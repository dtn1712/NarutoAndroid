package screen;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.BgItem;
import Objectgame.Char;
import Objectgame.Mob;
import Objectgame.TileMap;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.SmallImage;

public class WaitingScreen extends Screen{

	public static WaitingScreen me;
	public long timeStartOff;
	public int timeSleepLoadTile;
	public boolean isRemoveImg;
	public static boolean isChangeKhu;
	@Override
	public void switchToMe() {
		// TODO Auto-generated method stub
		timeStartOff = mSystem.currentTimeMillis();
		timeSleepLoadTile = 20;
		isRemoveImg = true;
		super.switchToMe();
	}
	public static WaitingScreen gI(){
		if(me==null) me = new WaitingScreen();
		return me;
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		if(isRemoveImg){
			if(!isChangeKhu){
			Mob.cleanImg();
			BgItem.cleanImg();
			}
			isChangeKhu = false;
			SmallImage.CleanImg();
			mSystem.my_Gc();
			isRemoveImg = false;
		}
		if(TileMap.imgMaptile==null)
		{
			timeSleepLoadTile++;
			if(timeSleepLoadTile>20){
				timeSleepLoadTile = 0;
				TileMap.loadimgTile(TileMap.tileID);
			}
		}
		if((mSystem.currentTimeMillis()-timeStartOff)/1000>3)
			
			if(GameCanvas.gameScreen !=null){
				Char.myChar().timeLastchangeMap= System.currentTimeMillis();
				Char.myChar().timedelay = 5000;
				GameCanvas.gameScreen.switchToMe();
				GameCanvas.cleanImgBg();
			}
		super.update();
	}

	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		g.setColor(0xff000000);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

		g.drawImage(LoadImageInterface.imgMap, GameCanvas.w/2, GameCanvas.h/2, mGraphics.VCENTER|mGraphics.HCENTER);

		int nCham = (GameCanvas.gameTick/10)%4;
		String text = "";
		for (int i = 0; i < nCham; i++) {
			text+=".";
		}
		for (int i = 0; i < 3-nCham; i++) {
			text+=" ";
		}
		mFont.tahoma_7.drawString(g, TileMap.mapName+text,GameCanvas.w/2, GameCanvas.h/2-4, 2);
//
//		mFont.tahoma_7.drawString(g, text, GameCanvas.w/2+mFont.tahoma_7.getWidth(TileMap.mapName)/2-2,GameCanvas.h/2-4, 0);
		g.drawRegion(LoadImageInterface.imgXinCho, 0, 16*((GameCanvas.gameTick/2)%3), 16, 16, 0,
				GameCanvas.w/2, 3*GameCanvas.h/4+20, mGraphics.VCENTER|mGraphics.HCENTER, false);
		
//		mFont.tahoma_7_white.drawString(g, "Xin chờ", GameCanvas.w/2, 3*GameCanvas.h/4+20, 2);

		super.paint(g);
	}

}
