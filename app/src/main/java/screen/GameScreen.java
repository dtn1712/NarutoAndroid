package screen;



import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Vector;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.FontSys;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.lib.Music;
import com.sakura.thelastlegend.lib.Rms;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.Util;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mHashtable;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.Arrowpaint;
import com.sakura.thelastlegend.domain.model.CRes;
import com.sakura.thelastlegend.domain.model.ChatManager;
import com.sakura.thelastlegend.domain.model.ChatPopup;
import com.sakura.thelastlegend.domain.model.ChatPopupCW;
import com.sakura.thelastlegend.domain.model.ChatTab;
import com.sakura.thelastlegend.domain.model.ChatTextField;
import com.sakura.thelastlegend.domain.model.DartInfo;
import com.sakura.thelastlegend.domain.model.Effect2;
import com.sakura.thelastlegend.domain.model.EffectCharPaint;
import com.sakura.thelastlegend.domain.model.EffectEnd;
import com.sakura.thelastlegend.domain.model.EffectInfoPaint;
import com.sakura.thelastlegend.domain.model.EffectKill;
//import com.sakura.thelastlegend.domain.model.Arrowpaint;
//import com.sakura.thelastlegend.domain.model.BuNhin;
//import com.sakura.thelastlegend.domain.model.ChatManager;
//import com.sakura.thelastlegend.domain.model.ChatTab;
//import com.sakura.thelastlegend.domain.model.Clan;
//import com.sakura.thelastlegend.domain.model.DunItem;
//import com.sakura.thelastlegend.domain.model.Effect;
//import com.sakura.thelastlegend.domain.model.EffectCharPaint;
//import com.sakura.thelastlegend.domain.model.EffectInfoPaint;
//import com.sakura.thelastlegend.domain.model.Friend;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.IChatable;
import com.sakura.thelastlegend.domain.model.Info;
import com.sakura.thelastlegend.domain.model.InfoDlg;
import com.sakura.thelastlegend.domain.model.Key;
import com.sakura.thelastlegend.domain.model.MainEffect;
import com.sakura.thelastlegend.domain.model.Member;
import com.sakura.thelastlegend.domain.model.MovePoint;
import com.sakura.thelastlegend.domain.model.NClass;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Part;
import com.sakura.thelastlegend.domain.model.PartImage;
import com.sakura.thelastlegend.domain.model.Party;
import com.sakura.thelastlegend.domain.model.QuickSlot;
import com.sakura.thelastlegend.domain.model.Res;
//import com.sakura.thelastlegend.domain.model.Party;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.domain.model.ServerEffect;
//import com.sakura.thelastlegend.domain.model.Scroll;
//import com.sakura.thelastlegend.domain.model.ScrollResult;
//import com.sakura.thelastlegend.domain.model.Skill;
//import com.sakura.thelastlegend.domain.model.SkillInfoPaint;
//import com.sakura.thelastlegend.domain.model.SkillOption;
//import com.sakura.thelastlegend.domain.model.SkillOptionTemplate;
//import com.sakura.thelastlegend.domain.model.SkillPaint;
//import com.sakura.thelastlegend.domain.model.SkillTemplate;
import com.sakura.thelastlegend.domain.model.SmallImage;
import com.sakura.thelastlegend.domain.model.Sprite;
import com.sakura.thelastlegend.domain.model.StaticObj;
import com.sakura.thelastlegend.domain.model.Type_Chat;
import com.sakura.thelastlegend.domain.model.Type_Party;
import com.sakura.thelastlegend.domain.model.Waypoint;
//import com.sakura.thelastlegend.domain.model.TaskOrder;
//import com.sakura.thelastlegend.domain.model.Waypoint;
import com.sakura.thelastlegend.domain.model.mResources;
import com.sakura.thelastlegend.network.Message;
import com.sakura.thelastlegend.real.Service;
import com.sakura.thelastlegend.gui.FatherChat;
import com.sakura.thelastlegend.gui.GuiChatClanWorld;
import com.sakura.thelastlegend.gui.GuiContact;
import com.sakura.thelastlegend.gui.GuiMain;
import com.sakura.thelastlegend.gui.Iconchat;
import com.sakura.thelastlegend.gui.MenuIcon;
import com.sakura.thelastlegend.gui.QShortQuest;
import com.sakura.thelastlegend.gui.QuestMain;
import com.sakura.thelastlegend.gui.ShopMain;
import com.sakura.thelastlegend.gui.TabBag;
import com.sakura.thelastlegend.gui.TabChat;
import com.sakura.thelastlegend.gui.TabInfoChar;
import com.sakura.thelastlegend.gui.TabMySeftNew;
import com.sakura.thelastlegend.gui.TabSkill;
import com.sakura.thelastlegend.gui.TradeGui;
import Objectgame.BgItem;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.Friend;
import Objectgame.Item;
import Objectgame.ItemMap;
import Objectgame.ItemOptionTemplate;
import Objectgame.ItemStands;
import Objectgame.ItemTemplates;
import Objectgame.Laroi;
import Objectgame.MenuObject;
import Objectgame.Mob;
import Objectgame.NodeChat;
import Objectgame.Npc;
import Objectgame.OtherChar;
import Objectgame.Quest;
import Objectgame.Skill;
import Objectgame.SkillInfoPaint;
import Objectgame.SkillPaint;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import Objectgame.TileMap;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.mGraphics;


public class GameScreen extends Screen implements IActionListener,IChatable  {

	//	public static Vector vSkillTemplate =  new Vector();
	public static boolean isTouchKey = true;
	public boolean isMenushow = false;
	public static ChatPopupCW chatPopupCW;
	public static DartInfo[] darts;
	public BgItem bi = new BgItem();
	public static int indexSizeFriend=50;
	public static GameScreen instance;
	public static int gW, gH, gW2, gssw, gssh, gH34, gW3, gH3, gH23, gW23, gH2, csPadMaxH, cmdBarH, gW34, gW6, gH6;
	// camera
	public static int cmx, cmy, cmdx, cmdy, cmvx, cmvy, cmtoX, cmtoY, cmxLim, cmyLim, gssx, gssy, gssxe, gssye;
	// char

	public Command cmdback, cmdBag, cmdSkill, cmdTiemnang, cmdtrangbi, cmdInfo, cmdFocus, cmdFire;
	public Command cmdAcceptParty, cmdNo , cmdComfirmFriend;
	public static int d;
	public static int mpPotion, hpPotion;
	public static mVector veffClient = new mVector();
	public static SkillPaint[] sks;
	public static Arrowpaint[] arrs;
	public static Part[] parts;
	public static EffectCharPaint[] efs;
	private static Scroll scrInfo = new Scroll();
	public static Scroll scrMain = new Scroll();
	int moveUp, moveDow, idTypeTask = 0;
	boolean isstarOpen, isChangeSkill = false, isShortcut = false;
	public static mVector vMobSoul = new mVector(), vClan = new mVector(), 
	/*vParty = new mVector(),*/ vPtMap = new mVector(), vFriend = new mVector(), 
	vList = new mVector(), vFriendWait = new mVector(), vEnemies = new mVector(), vNhatItemMap = new mVector(),
	vCharInMap = new mVector(), vItemMap = new mVector(), vMobAttack = new mVector(), vSet = new mVector(), 
	vMob = new mVector(), vNpc = new mVector(), vBuNhin = new mVector(), vLanterns = new mVector(),listInfoServer = new mVector(),listWorld = new mVector();
	public static mHashtable hCharInmap = new mHashtable(), hParty = new mHashtable();
	public static NClass[] nClasss;
	public static int indexSize = 28, indexTitle = 0, indexSelect = 0, indexRow = -1, indexRowMax, indexMenu = 0, indexCard = -1;
	public Item itemFocus;
	public static ItemOptionTemplate[] iOptionTemplates;
//	public static SkillOptionTemplate[] sOptionTemplates;

	public static Item[] arrItemNonNam, arrItemNonNu, arrItemAoNam, arrItemAoNu, arrItemGangTayNam, arrItemGangTayNu, arrItemQuanNam, arrItemQuanNu,
			arrItemGiayNam, arrItemGiayNu, arrItemLien, arrItemNhan, arrItemNgocBoi, arrItemPhu;
	public static Item[] arrItemWeapon, arrItemStack, arrItemStackLock, arrItemGrocery, arrItemGroceryLock, arrItemStore, arrItemElites,
			arrItemClanShop, arrItemBook, arrItemFashion;
	public static Item[] arrItemUpPeal, arrItemUpGrade, arrItemSplit, arrItemTradeMe, arrItemTradeOrder, arrItemConvert;
	public static ItemStands[] arrItemStands;
	public static short[] arrItemSprin;
	public int numSprinLeft;
	public static Item itemUpGrade, itemSplit, itemSell;
	public static mVector vItemUpGrade = new mVector();
	public static boolean isTypeXu; // true?xu:xukhoa
	public static boolean isViewNext, isViewClanMemOnline = false, isSortClanByPointWeek = false, isViewClanInvite = true, isChop,
			isMessageMenu = false;
	public static String titleInputText = "";

	public int indexKeyTouchAuto,timeDow,xStartAuto,yStartAuto,rangeAuto=240;
		
	public static boolean isShowFocus = false,isAutoDanh,isAutoNhatItem;
	public static int ypaintFocus=0;
	public static boolean isPaintAuctionSale = false, isPaintAlert = false, isPaintTask = false, isPaintTeam = false, isPaintFindTeam = false,
			isPaintFriend = false, isPaintList = false, isPaintEnemies = false, isPaintItemInfo = false, isPaintSelectSkill = false,
			isPaintInfoMe = false, isPaintStore = false, isPaintEliteShop = false, isPaintNonNam = false, isPaintNonNu = false, isPaintAoNam = false,
			isPaintAoNu = false, isPaintGangTayNam = false, isPaintGangTayNu = false, isPaintQuanNam = false, isPaintQuanNu = false,
			isPaintGiayNam = false, isPaintGiayNu = false, isPaintLien = false, isPaintNhan = false, isPaintNgocBoi = false, isPaintPhu = false,
			isPaintWeapon = false, isPaintStack = false, isPaintStackLock = false, isPaintGrocery = false, isPaintGroceryLock = false,
			isPaintUpGrade = false, isPaintConvert = false, isPaintUpGradeGold = false, isPaintUpPearl = false, isPaintBox = false,
			isPaintSplit = false, isPaintCharInMap = false, isPaintTrade = false, isPaintZone = false, isPaintAuto= false, isPaintMessage = false, isPaintClan = false,
			isRequestMember = false, isPaintLuckySpin = false, isPaintAuctionBuy = false, isPaintLuyenThach, isPaintTinhluyen, isPaintDichChuyen;
	public static Char currentCharViewInfo;
	public static long exps[];
	public static int[] crystals, upClothe, upAdorn, upWeapon, coinUpCrystals, coinUpClothes, coinUpAdorns, coinUpWeapons, maxPercents, goldUps;
	public int zoneCol = 6, zones[], pts[];

	public int typeTrade = 0, typeTradeOrder = 0, coinTrade = 0, coinTradeOrder = 0, timeTrade = 0, typeSortPrice = 0, typeSortLevel = 0,
			typeSortName = 0;

	public int indexItemUse = -1, cLastFocusID = -1, cPreFocusID = -1;
	public boolean isLockKey;
	public static byte[][] tasks, mapTasks;

	public Vector texts;
	public String textsTitle;
	public TField tfText = null;
	public static byte vcData, vcMap, vcSkill, vcItem, vsData, vsMap, vsSkill, vsItem;
	// private static boolean isShowControl = false;
	public static mBitmap imgTopBar, imgTransparent, imgArrow, imgArrow2, imgChat, imgMenu, imgFocus, imgHpp, imgMpp, imgSkill,imgFocusActor;
	public static mBitmap imgMapBorder, imgLbtn, imgLbtnFocus, imgSelect, imgMatcho, imgFiremoto, imgmoto, imgChattext;
	public static mBitmap imgxe1, imgxe2, imgxe3;

	public static mBitmap imgQuest;
	public String tradeName = "", tradeItemName = "";
	public int timeLengthMap, timeStartMap;
	public static byte typeViewInfo = 0, typeActive = 0;
	private int[] xM = new int[2], yM = new int[2];
	private int[] xMounts ,yMounts;
	public long timePoint;
	public int yenTemp, typeba;
	public String[] yenValue;
	public static mVector charnearByme =  new mVector();
	public static mBitmap imgtestMod;
	public int timeSleepLoadTile = 20;

	public static mBitmap[] imgCloudy = new mBitmap[3];
	public static int nbg,wcloudy1,ncloudy1,wcloudy0,ncloudy0,kcwcloudy0,wcloudy2,ncloudy2;
	public static int[] xpaintcloudy1,xpaintcloudy0,ypaintcloudy1,ypaintcloudy0,xpaintcloudy2,ypaintcloudy2;
	//icon chat
	public static Iconchat iconChat;
	//chat world
	public static boolean isPaintGuiChatWorld;
	
	//chat friend
	public static Command chat;
	public static Command bntIconChat;
	public static boolean ispaintChat = false;
	public static boolean isPaintQuest=false;
	
	//init menu
	public static Command mChatPrivate;//chat private
	public static Command mListFriend;//display friend list
	public static Command mBag;//display friend list
	public static Command mParty;//display friend list
	public static Command mChatWorld;//display friend list
	public static Command mGuiQuest;//display quest info
	
	//friend list com.sakura.thelastlegend.gui
	public static Command btnUnfriend;//display quest info
	public static Command btnChat;//display quest info
	
	//quest screen
	public QShortQuest qShortQuest= new QShortQuest();
	public static ShopMain guiQuest;//com.sakura.thelastlegend.gui main quest
	
	//main guis
	public GuiMain guiMain;
	
	//chat clan,world
	public GuiChatClanWorld guiChatClanWorld = new GuiChatClanWorld(-FatherChat.popw + 5,GameCanvas.h-FatherChat.poph+7,new String[]{"World","Clan"});
	
	//com.sakura.thelastlegend.gui Contact
	GuiContact guiContact= new GuiContact(GameCanvas.w/2,GameCanvas.h/2);
	
	public static boolean isBag = false;
	
	// trade command
	public static Command cmdAcceptTrade;
	
	// quest main 
	public static QuestMain questMain;
	
	public static TradeGui tradeGui;//test thu

	static {
		byte[] d1, d2, d3, d4;
		d1 = Rms.loadRMS("dataVersion");
		d2 = Rms.loadRMS("mapVersion");
		d3 = Rms.loadRMS("skillVersion");
		d4 = Rms.loadRMS("itemVersion");
		if (d1 != null)
			vcData = d1[0];
		if (d2 != null)
			vcMap = d2[0];
		if (d3 != null)
			vcSkill = d3[0];
		if (d4 != null)
			vcItem = d4[0];
	}

	public static long getMaxExp(int level) {
		long totalExp = 0;
		for (int i = 0; i <= level; i++) {
			totalExp += exps[i];
		}
		return totalExp;
	}

	public static void resetAllvector() {
		vCharInMap.removeAllElements();
		vItemMap.removeAllElements();
		vMobSoul.removeAllElements();
		Effect2.vEffect2.removeAllElements();
		Effect2.vAnimateEffect.removeAllElements();
		Effect2.vEffect2Outside.removeAllElements();
		vMobAttack.removeAllElements();
		vSet.removeAllElements();
		vMob.removeAllElements();
		vNpc.removeAllElements();
		vBuNhin.removeAllElements();
		Char.myChar().vMovePoints.removeAllElements();
	}

	static Skill[] keySkill = { null, null, null };
	static Skill[] onScreenSkill = { null, null, null, null, null };

	protected void doSetOnScreenSkill(SkillTemplate skillTemplate) {
		final Skill sk = Char.myChar().getSkill(skillTemplate);
		Vector menu = new Vector();
		for (int i = 0; i < 5; i++) {
			boolean isStop = false;
			if (onScreenSkill[i] == null)
				isStop = true;
			Object[] value = new Object[2];
			value[0] = sk;
			value[1] = i + "";
			menu.addElement(new Command(mResources.CELLS + " " + (i + 1), 11120, value));
			if (isStop)
				break;
		}
		GameCanvas.menu.startAt(menu, 0);
	}

	protected void doSetKeySkill(SkillTemplate skillTemplate) {
		final Skill sk = Char.myChar().getSkill(skillTemplate);
		String[] name = TField.isQwerty ? mResources.KEY_QWERTY : mResources.KEY_T9;
		Vector menu = new Vector();
		for (int i = 0; i < 3; i++) {
			Object[] value = new Object[2];
			value[0] = sk;
			value[1] = i + "";
			menu.addElement(new Command(name[i], 11121, value));
		}
		GameCanvas.menu.startAt(menu, 0);
	}


	@Override
	public void switchToMe() {
		// TODO Auto-generated method stub
		super.switchToMe();
		isBag = false;
	}
	

	protected void saveRMSCurrentSkill(byte id) {
// Rms.saveRMS("CSkill" + Char.myChar().cName, new byte[] { id });
// Service.gI().saveRms("CSkill", new byte[] { id });
	}

	

	public boolean isBagFull() {
		for (int i = Char.myChar().arrItemBag.length - 1; i >= 0; i--) {
			if ((Item) Char.myChar().arrItemBag[i] == null) {
				return false;
			}
		}
		return true;
	}

	public void doBag() {
		currentCharViewInfo = Char.myChar();
		indexMenu = 0;
//		doMiniMenuInforMe();
	}

	public void doskill() {
		currentCharViewInfo = Char.myChar();
		indexMenu = 1;
//		doMiniMenuInforMe();
	}

	public void doTiemnangMe() {
		currentCharViewInfo = Char.myChar();
		indexMenu = 2;
//		doMiniMenuInforMe();
	}

	public void doInfo() {
		currentCharViewInfo = Char.myChar();
		indexMenu = 3;
//		doMiniMenuInforMe();
	}

	public void doTrangbi() {
		currentCharViewInfo = Char.myChar();
		indexMenu = 4;
//		doMiniMenuInforMe();
	}
	
	public void doThucuoi() {
		currentCharViewInfo = Char.myChar();
		indexMenu = 5;
//		doMiniMenuInforMe();
		
	}

	public static void readPart() {
		DataInputStream file = null;
		try {
// file = new DataInputStream(this.getClass().getResourceAsStream("/img/nj_part"));
			file = new DataInputStream(new ByteArrayInputStream(Rms.loadRMS("nj_part")));
//			file = GameCanvas.readdatafile("file/1_part");
//			System.out.println("FILE -----> "+file);
			int sum = file.readShort();
//			System.out.println("SUM ----> "+sum);
			parts = new Part[sum];
			for (int i = 0; i < sum; i++) {
				int type = file.readByte();
				parts[i] = new Part(type);
				for (int j = 0; j < parts[i].pi.length; j++) {

					parts[i].pi[j] = new PartImage();
					parts[i].pi[j].id = file.readShort();
					parts[i].pi[j].dx = file.readByte();
					parts[i].pi[j].dy = file.readByte();
					

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				file.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}

	// =================
	public static void readEfect() {
		DataInputStream file = null;
		try {
// file = new DataInputStream(this.getClass().getResourceAsStream("/img/nj_effect"));
			file = new DataInputStream(new ByteArrayInputStream(Rms.loadRMS("nj_effect")));
//			file = GameCanvas.readdatafile("file/1_effect");
			int sum = file.readShort();
            efs = new EffectCharPaint[sum];
            Cout.println2222("readEfect sum  "+sum);
            for (int i = 0; i < sum; i++)
            {
                efs[i] = new EffectCharPaint();
                efs[i].indexEffect = i;
                efs[i].idEf = file.readShort();
                efs[i].arrEfInfo = new EffectInfoPaint[file.readByte()];
                for (int j = 0; j < efs[i].arrEfInfo.length; j++)
                {
                    efs[i].arrEfInfo[j] = new EffectInfoPaint();
                    efs[i].arrEfInfo[j].idImg = file.readShort();
                    efs[i].arrEfInfo[j].dx = file.readByte();
                    efs[i].arrEfInfo[j].dy = file.readByte();
                }
            }
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				file.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}

	public static void readArrow() {
		DataInputStream file = null;
		try {
// file = new DataInputStream(this.getClass().getResourceAsStream("/img/nj_arrow"));
			file = new DataInputStream(new ByteArrayInputStream(Rms.loadRMS("nj_arrow")));
//			file = GameCanvas.readdatafile("file/1_arrow");
			int sum = file.readShort();
//			System.out.println("READ ARROW SUM ----> "+sum);
			arrs = new Arrowpaint[sum];
			for (int i = 0; i < sum; i++) {
				arrs[i] = new Arrowpaint();
				arrs[i].id = file.readShort();
				arrs[i].imgId[0] = file.readShort();
				arrs[i].imgId[1] = file.readShort();
				arrs[i].imgId[2] = file.readShort();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				file.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}

	 public static void readSkill() {
		  DataInputStream file = null;
		  try {
		// file = new DataInputStream(this.getClass().getResourceAsStream("/img/nj_skill"));
		   file = new DataInputStream(new ByteArrayInputStream(Rms.loadRMS("nj_skill")));
		//   file = GameCanvas.readdatafile("file/1_skill");
		   int sum = file.readShort();
		   int size = 0;
		//   for (int i = 0; i < nClasss.length; i++) {
//		    size += nClasss[i].skillTemplates.length;
		//   }
		   
		   sks = new SkillPaint[sum + 1];
		// System.out.println("sks.length: " + sks.length);
		   for (int i = 1; i < sum; i++) {
		    int skillId = i;
		    sks[skillId] = new SkillPaint();
		    sks[skillId].id = file.readShort();
		    sks[skillId].effId = file.readShort();
		    sks[skillId].numEff = file.readByte();
		    sks[skillId].skillStand = new SkillInfoPaint[file.readByte()];
		    for (int j = 0; j < sks[skillId].skillStand.length; j++) {
		     sks[skillId].skillStand[j] = new SkillInfoPaint();
		     sks[skillId].skillStand[j].status = file.readByte();
		     sks[skillId].skillStand[j].effS0Id = file.readShort();
		     sks[skillId].skillStand[j].e0dx = file.readShort();
		     sks[skillId].skillStand[j].e0dy = file.readShort();

		     sks[skillId].skillStand[j].effS1Id = file.readShort();
		     sks[skillId].skillStand[j].e1dx = file.readShort();
		     sks[skillId].skillStand[j].e1dy = file.readShort();

		     sks[skillId].skillStand[j].effS2Id = file.readShort();
		     sks[skillId].skillStand[j].e2dx = file.readShort();
		     sks[skillId].skillStand[j].e2dy = file.readShort();

		     sks[skillId].skillStand[j].arrowId = file.readShort();
		     sks[skillId].skillStand[j].adx = file.readShort();
		     sks[skillId].skillStand[j].ady = file.readShort();
		    }

		    sks[skillId].skillfly = new SkillInfoPaint[file.readByte()];
		    for (int j = 0; j < sks[skillId].skillfly.length; j++) {
		     sks[skillId].skillfly[j] = new SkillInfoPaint();
		     sks[skillId].skillfly[j].status = file.readByte();
		     sks[skillId].skillfly[j].effS0Id = file.readShort();
		     sks[skillId].skillfly[j].e0dx = file.readShort();
		     sks[skillId].skillfly[j].e0dy = file.readShort();

		     sks[skillId].skillfly[j].effS1Id = file.readShort();
		     sks[skillId].skillfly[j].e1dx = file.readShort();
		     sks[skillId].skillfly[j].e1dy = file.readShort();

		     sks[skillId].skillfly[j].effS2Id = file.readShort();
		     sks[skillId].skillfly[j].e2dx = file.readShort();
		     sks[skillId].skillfly[j].e2dy = file.readShort();

		     sks[skillId].skillfly[j].arrowId = file.readShort();
		     sks[skillId].skillfly[j].adx = file.readShort();
		     sks[skillId].skillfly[j].ady = file.readShort();
		    }
		   }
		  } catch (Exception ex) {
		   ex.printStackTrace();
		  } finally {
		   try {
		    file.close();
		   } catch (IOException e) {

		    e.printStackTrace();
		   }
		  }
		 }

	// =======================
	public static long[] getLevelExp(long exp) {
		long expRemain = exp;
		int i = 0;
		for (i = 0; i < exps.length; i++) {

			if (expRemain >= exps[i])
				expRemain -= exps[i];
			else
				break;
		}

		return new long[] { i, expRemain };
	}

	public static void setLevel_Exp(long exp, boolean value) {
		long[] a = getLevelExp(exp);
		if (value)
			Char.myChar().clevel = (int) a[0];
		Char.myChar().cExpR = a[1];
	}

	public static GameScreen gI() {
		if (GameCanvas.gameScreen == null) {
			GameCanvas.gameScreen = new GameScreen();
		}
		return GameCanvas.gameScreen;
	}

	public static void clearGameScr() {
		instance = null;
		arrItemUpPeal = arrItemUpGrade = arrItemSplit = arrItemTradeMe = arrItemTradeOrder = null;
		itemUpGrade = itemSplit = null;
	}

	public void loadGameScr() {
		loadSplash();
		GuiMain.loadCmdBar();
		Res.init();
	}

	public Command menu;
	private Command cmdPotentialAdd;
	private Command cmdSkillUp;
	private Command cmdAddFriend;
	public void init() {
		  mVector v = new mVector();
		  
		  TabBag tabbag = new TabBag("Hành Trang");
		  v.addElement(tabbag);
		  
		  TabMySeftNew tabmyseft = new TabMySeftNew("Trang Bị");
		  v.addElement(tabmyseft);
		  
		  TabInfoChar tabinfochar = new TabInfoChar("Thông tin");
		  v.addElement(tabinfochar);

		  TabSkill tabskill = new TabSkill("Kỹ năng");
		  v.addElement(tabskill);
//		  TabQuest tabQuest = new TabQuest(Text.tabhethong);
//		  v.addElement(tabQuest);

//		  TabNangCap tabNc = new TabNangCap("Nâng cấp");
//		  v.addElement(tabNc);
		  GameCanvas.AllInfo.addMoreTab(v);
		  
		 }
	public GameScreen() {
		Service.gI().requestFriendList((byte)Friend.REQUEST_FRIEND_LIST, (short)Char.myChar().charID);
		//doShowFriendUI();
//		bi.loadImgmap();
//		bi.loadMaptable();
//		bi.loadMapItem();
//		hardcodeParty();
//		hardcodeFriendlist();
		loadGameScr();
		guiMain= new GuiMain();
		questMain  = new QuestMain();
		Service.gI().requestMenuShop();
		Service.gI().requestShop(0);
//		Service.gI().requestFriendList((byte)Friend.REQUEST_FRIEND_LIST, (short)Char.myChar().charID);
//		isPaintFriend = true;
		if (GameCanvas.w == 128 || GameCanvas.h <= 208)
			indexSize = 20;
//		cmdFocus.img = GameCanvas.loadImage("/u/fc.png");
//		chatPopupCW = new ChatPopupCW();
//		left = menu;
//		right = cmdFocus;
		init();
		popy = 50;
		popw = 100;
		poph = GameCanvas.h-50-40;
		widthGui = 240;

		xGui = GameCanvas.w/2-widthGui/2;

		popx = xGui-popw; 
		popx = (popx<0?0:popx);
		yGui = 50;
		NodeChat.wnode = widthGui - 10;
		heightGui=GameCanvas.h-50-40;
		initTfied();
		initCommand();
//		InitMenu();//init menu
	}

	private void InitMenu()
	{
//		mChatPrivate= new Command("Chat riêng", Constants.MENU_CHAT_PRIVATE);//chat private
//		mChatPrivate.setPos(2,30,imgLbtn,imgLbtn);
//		
		mListFriend=new Command("List friend", Constants.MENU_LIST_FRIEND);;//display friend list
		mListFriend.setPos(2,30*2, imgLbtn,imgLbtn);
//		
//	    mBag=new Command("Hành trang", Constants.MENU_BAG);//display bag
//	    mBag.setPos(2,30*3, imgLbtn,imgLbtn);
//	    
//		mParty=new Command("Party", Constants.MENU_LIST_PARTY);//display PARTY list
//		mParty.setPos(2,30*4, imgLbtn, imgLbtn);
//		
//		mChatWorld=new Command("Chat World", Constants.MENU_CHAT_WORLD);//display chat world
//		mChatWorld.setPos(2,30*5, imgLbtn, imgLbtn);
//		
		
		mGuiQuest=new Command("Nhiệm vụ", Constants.MENU_QUEST);//display chat world
		mGuiQuest.setPos(2,30*6, imgLbtn, imgLbtn);
		
	}
	private void initCommand() {

//		Service.gI().requestinventory();
		bntIconChat = new Command("", Constants.BUTTON_ICON_CHAT);
		bntIconChat.setPos( xGui+5+tfCharFriend.width+5, tfCharFriend.y, LoadImageInterface.imgEmo[7], LoadImageInterface.imgEmo[7]);
		bntIconChat.w = 40;
		bntIconChat.h = 20;
		//create button sen chat
		chat = new Command("Gửi", Constants.BUTTON_SEND);
		chat.setPos(bntIconChat.x+bntIconChat.w+5, tfCharFriend.y, GameScreen.imgSkill, GameScreen.imgSkill);

		chat.w = 60;
		chat.h = 20;
		
		cmdAddFriend = new Command(mResources.ACCEPT, 11002);
		cmdSkillUp = new Command(mResources.ADD, 11003);
		cmdGangTayNuView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11004);
		cmdGangTayNuBuy = new Command(mResources.BUY, 11005);
		cmdPotentialAdd = new Command(mResources.ADD, 11006);
		cmdGangTayNamView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11007);
		cmdGangTayNamBuy = new Command(mResources.BUY, 11008);
		cmdAoNuView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11009);
		cmdAoNuBuy = new Command(mResources.BUY, 11010);
		cmdAoNamView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11011);
		cmdAoNamBuy = new Command(mResources.BUY, 11012);
		cmdNonNuView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11013);
		cmdNonNuBuy = new Command(mResources.BUY, 11014);
		cmdNonNamView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11015);
		cmdNonNamBuy = new Command(mResources.BUY, 11016);
		cmdStoreLockView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11017);
		cmdStoreFashionView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 13001);
		cmdStoreLockBuy = new Command(mResources.BUY, 11018);
		cmdStoreFashionBuy = new Command(mResources.BUY, 13002);
		cmdStoreView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11019);
		cmdStoreBuy = new Command(mResources.BUY, 11020);
		cmdEliteShopBuy = new Command(mResources.BUY, 14022);
		cmdEliteShopView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 14023);
		cmdClanStoreView = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 14018);
		cmdClanStoreBuy = new Command(mResources.BUY, 14019);
		cmdCloseAll = new Command(mResources.CLOSE, 11021);
		cmdBagSelectItem = new Command(mResources.SELECT, 11022);
		cmdBagViewItemInfo = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11023);
		cmdBagThrowItem = new Command(mResources.THROW, 11024);
		cmdBagSplitItem = new Command(mResources.SPLIT, 110244);
		cmdItemInfoClose = new Command(mResources.CLOSE, 11025);
		cmdBagUseItem = new Command(mResources.USE, 11026);
		cmdBagSortItem = new Command(mResources.SORT, 110221);
		cmdTradeSelectInList = new Command(mResources.SELECT, 11027);
		cmdTradeSelectInBag = new Command(mResources.SELECT, 11028);
		cmdTradeSelectItem = new Command(mResources.SELECT, 11029);
		cmdTradeViewItemInfo = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11030);

		cmdback = new Command(mResources.BACK, 11021);
		menu = new Command(mResources.MENU, 11000);
		cmdFocus = new Command("Focus", 11001);

		cmdTradeLock = new Command(mResources.LOCK1, 11032);
		cmdTradeAccept = new Command(mResources.ACCEPT, 11033);
		cmdUpgradeMoveOut = new Command(mResources.MOVEOUT, 11034);
		cmdConvertMoveOut = new Command(mResources.MOVEOUT, 14014);
		cmdSplitMoveOut = new Command(mResources.MOVEOUT, 11035);
		cmdTradeSendMoney = new Command(mResources.SENDMONEY, 11036);
		cmdTradeMoveOut = new Command(mResources.MOVEOUT, 11037);
		cmdAcceptParty = new Command("Đồng ý", 111037);
		cmdComfirmFriend = new Command("Đồng ý", 111038);
		//////
		cmdAcceptTrade = new Command("Đồng ý",111039);
		

		// ----- INIT COMMAND FOR TOUCHSCREEN
		if (GameCanvas.isTouch && GameCanvas.isTouchControl) {
			menu.x = gW - 135;
			menu.y = 6;
			menu.img = imgMenu;

			cmdFocus.x = gW;
			cmdFocus.y = gH;

			if (GameCanvas.isTouchControlSmallScreen) {
				menu.x = gW / 2 - 38;
				menu.y = gH - 34;
			}
		}
		

	}

	

	public void doUpPotential() {
		if ((indexTitle <= 0 || indexTitle > 4) && !GameCanvas.isTouch)
			return;
		GameCanvas.inputDlg.show(mResources.INPUT_POINT, cmdPotentialAdd, TField.INPUT_TYPE_NUMERIC);
	}

	public void doUpSkill() {
		if (indexTitle <= 0 || indexTitle > 4)
			return;
		GameCanvas.inputDlg.show(mResources.INPUT_POINT, cmdSkillUp, TField.INPUT_TYPE_NUMERIC);
	}

	public void doAddFriend() {
		if (!isPaintFriend)
			return;
		GameCanvas.inputDlg.show(mResources.BE_FRIEND_WITH, cmdAddFriend, TField.INPUT_TYPE_ANY);
	}

	

	protected void doMenuInforMe() {
		scrMain.clear();
		scrInfo.clear();
		isViewNext = false;
		cmdBag = new Command(mResources.MENUME[0], 1100011);
		cmdSkill = new Command(mResources.MENUME[1], 1100012);
		cmdTiemnang = new Command(mResources.MENUME[2], 1100013);
		cmdInfo = new Command(mResources.MENUME[3], 1100014);
		cmdtrangbi = new Command(mResources.MENUME[4], 1100015);

		Vector menu = new Vector();
		menu.addElement(cmdBag);
		menu.addElement(cmdSkill);
		menu.addElement(cmdTiemnang);
		menu.addElement(cmdInfo);
		menu.addElement(cmdtrangbi);
		menu.addElement(new Command(mResources.MENUME[5], 1100016));
		GameCanvas.menu.startAt(menu, 3);
	}

	protected void doMenusynthesis() {
		mVector sub = new mVector();
		sub.addElement(new Command(mResources.SYNTHESIS[0], 110002));
		sub.addElement(new Command(mResources.SYNTHESIS[1], 1100032));
		sub.addElement(new Command(mResources.SYNTHESIS[2], 1100033));
		sub.addElement(new Command(mResources.CONFIG, LoginScreen.gI(), 1004, null));
		GameCanvas.menu.startAt(sub, 3);
	}

	protected void actMenu() {
		Command inforMe = new Command(mResources.MENUGAMESCR[1], 110001);
		Command cmdMap = new Command(mResources.MENUGAMESCR[2], 110002);
		Command cmdTask = new Command(mResources.MENUGAMESCR[3], 110003);
		Command exit = new Command(mResources.MENUGAMESCR[4], 110004);
		Command cmdStore = new Command(mResources.MENUGAMESCR[0], 110005);
		Command cmdOrder = new Command(mResources.MENUGAMESCR[6], 110006);
		Command cmdShopStackLock = new Command(mResources.MENUGAMESCR[7], 110007);
		Command cmdShopStack = new Command(mResources.MENUGAMESCR[8], 110008);
		Command cmdGroceryLock = new Command(mResources.MENUGAMESCR[9], 110009);
		Command cmdGrocery = new Command(mResources.MENUGAMESCR[10], 110010);
		Command cmdUpgrade = new Command(mResources.MENUGAMESCR[11], 110011);
		Command cmdUpPearl = new Command(mResources.MENUGAMESCR[12], 110012);
		Command cmdUpPearlLock = new Command(mResources.MENUGAMESCR[13], 110013);
		Command cmdBox = new Command(mResources.MENUGAMESCR[14], 110014);
		Command cmdSplit = new Command(mResources.MENUGAMESCR[15], 110015);
		Command cmdZone = new Command(mResources.MENUGAMESCR[16], 110016);
		Command cmdTrade = new Command(mResources.MENUGAMESCR[17], 110017);
		Command cmdMessage = new Command(mResources.MENUGAMESCR[18], 110018);

		Vector menu = new Vector();
// menu.addElement(new Command("Test menu", 999));
		// menu.addElement(new Command(mResources.LATQUA,909090));
		menu.addElement(inforMe);
		menu.addElement(cmdTask);
		menu.addElement(cmdOrder);
		menu.addElement(cmdMessage);
		menu.addElement(exit);
		GameCanvas.menu.startAt(menu, 3);
//		Session_ME.gI().isStopSend = true;
//		System.out.println("STOP SEND MESSAGE");
	}

	public void doShowTaskUI() {
		resetButton();
		isPaintTask = true;
		indexMenu = idTypeTask;
		isLockKey = true;
		setPopupSize(175, 200);
		right = cmdCloseAll;
		left = new Command(mResources.MENUGAMESCR[2], 110002);
		center = new Command(mResources.CHANGE, 110019);

	}

	public void doShowFindTeamUI() {
		resetButton();
		isPaintFindTeam = true;
		isLockKey = true;
		setPopupSize(175, 200);
		right = cmdCloseAll;
//		Service.gI().openFindParty();
		refreshFindTeam();
	}

	public void doShowCharInMap() {
		resetButton();
		if (cLastFocusID > 0)
			indexRow = Char.getIndexChar(cLastFocusID);
		else {
			indexRow = 0;
			cLastFocusID = -1;
		}
		isPaintCharInMap = true;
		isLockKey = true;
		setPopupSize(175, 200);
		right = cmdCloseAll;
	}

	public void doShowTeamUI() {
		resetButton();
		isPaintTeam = true;
		isLockKey = true;
		setPopupSize(175, 200);
		right = cmdCloseAll;
//		refreshTeam();
	}

	public void doShowListUI() {
		resetButton();
		isPaintList = true;
		isLockKey = true;
		setPopupSize(175, 200);
		right = cmdCloseAll;
		left = center = null;
		indexRow = 0;
	}

	public void doShowFriendUI() {
		resetButton();
		isPaintFriend = true;
		isLockKey = true;
		setPopupSize(175, 200);
//		right = cmdCloseAll;
//		left = new Command(mResources.LIST, 11044);
		center = null;
		indexRow = -1;
//		Service.gI().requestFriend();
	}

	public void doShowEnemiesUI() {
		resetButton();
		isPaintEnemies = true;
		isLockKey = true;
		setPopupSize(175, 200);
		right = cmdCloseAll;
		left = new Command(mResources.LIST, 14017);
		center = null;
		indexRow = 0;
//		Service.gI().requestEnemies();
	}

	public void refreshFindTeam() {
		if (isPaintFindTeam) {
			left = center = null;
			left = new Command(mResources.MENU, 11045);
			if (vPtMap.size() > 0 && indexRow >= 0 && indexRow < vPtMap.size()) {
				Party p = (Party) vPtMap.elementAt(indexRow);
				if (p != null) {
					if (!Char.myChar().cName.equals(p.name))
						center = new Command(mResources.SELECT, 11046);
				}
			}
		}
	}
	public void openUIZone(Message message) {
		InfoDlg.hide();
		try {
			zones = new int[message.reader().readByte()];
			pts = new int[zones.length];
			for (int i = 0; i < zones.length; i++) {
				zones[i] = message.reader().readByte();
				pts[i] = message.reader().readByte();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		isPaintZone = true;
		indexSelect = TileMap.zoneID;
		setPopupSize(175, 200);
		left = new Command(mResources.SELECT, 11067);
		center = new Command("", 11067);
		right = cmdCloseAll;
	}

	public void openUITrade() {
		try {
			resetButton();
			tradeItemName = "";
			typeTrade = 0;
			typeTrade = typeTradeOrder = coinTrade = coinTradeOrder = 0;
			isPaintTrade = true;
			arrItemTradeMe = new Item[12];
			arrItemTradeOrder = new Item[12];
			indexMenu = 0;
			setPopupSize(175, 200);
			right = cmdCloseAll;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public static int firstY;
	public static final void loadCamera(boolean fullScreen, int cx, int cy) {
		

		gW = GameCanvas.w;

		cmdBarH = 39;

		gH = GameCanvas.h;

		cmdBarW = gW;

		cmdBarX = 0;
//		cmdBarY = GameCanvas.h - GameCanvas.paintz.hTab - cmdBarH;
		girlHPBarY = 0;

		csPadMaxH = GameCanvas.h / 6;
		if (csPadMaxH < 48)
			csPadMaxH = 48;
		gW2 = gW >> 1;
		gH2 = gH >> 1;
		gW3 = gW / 3;
		gH3 = gH / 3;
		gW23 = gH - 120;
		gH23 = gH * 2 / 3;
		gW34 = 3 * gW / 4;
		gH34 = 3 * gH / 4;
		gW6 = gW / 6;
		gH6 = gH / 6;
		// Map --------------------
		gssw = gW / TileMap.size + 2;
		gssh = gH / TileMap.size + 2;
		if (gW % 24 != 0)
			gssw += 1;
		cmxLim = (TileMap.tmw ) * TileMap.size - gW;
		cmyLim = (TileMap.tmh ) * TileMap.size - gH;
		if (cx == -1 && cy == -1) {
			cmx = cmtoX = Char.myChar().cx - GameScreen.gW2 + gW6
					* Char.myChar().cdir;
			cmy = cmtoY = Char.myChar().cy - GameScreen.gH23;
		}

		else {
			cmx = cmtoX = Char.myChar().cx - gW23 + gW6 * Char.myChar().cdir;
			cmy = cmtoY = Char.myChar().cy - GameScreen.gH23;

		}
		firstY = cmy;
		if (cmx < 24)
			cmx = cmtoX = 24;
		if (cmx > cmxLim)
			cmx = cmtoX = cmxLim;
		if (cmy < 0)
			cmy = cmtoY = 0;
		if (cmy > cmyLim)
			cmy = cmtoY = cmyLim;
		//
	
		gssx = cmx / TileMap.size - 1;
		if (gssx < 0)
			gssx = 0;
		gssy = cmy / TileMap.size;
		gssxe = gssx + gssw;
		gssye = gssy + gssh;
		if (gssy < 0)
			gssy = 0;
		if (gssye > TileMap.tmh - 1)
			gssye = TileMap.tmh - 1;
		//
		TileMap.countx = (gssxe - gssx) * 4;
		if (TileMap.countx > TileMap.tmw)
			TileMap.countx = TileMap.tmw;
		TileMap.county = (gssye - gssy) * 4;
		if (TileMap.county > TileMap.tmh)
			TileMap.county = TileMap.tmh;
		//
		TileMap.gssx = (Char.myChar().cx - 2 * gW) / TileMap.size;
		if (TileMap.gssx < 0)
			TileMap.gssx = 0;
		TileMap.gssxe = TileMap.gssx + TileMap.countx;
		if (TileMap.gssxe > TileMap.tmw)
			TileMap.gssxe = TileMap.tmw;

		TileMap.gssy = (Char.myChar().cy - 2 * gH) / TileMap.size;
		if (TileMap.gssy < 0)
			TileMap.gssy = 0;

		TileMap.gssye = TileMap.gssy + TileMap.county;

		if (TileMap.gssye > TileMap.tmh)
			TileMap.gssye = TileMap.tmh;
		ChatTextField.gI().parentScreen = instance;

		ChatTextField.gI().tfChat.y = GameCanvas.h - 35 - ChatTextField.gI().tfChat.height;

//		if (!GameCanvas.isTouch || (GameCanvas.isTouch && !GameCanvas.isTouchControl))
//			TileMap.setPosMiniMap(GameCanvas.w - 51, cmdBarY - 4, 50, 40);
//		else {
//			TileMap.setPosMiniMap(GameCanvas.w - 60, 0, 60, 42);
//		}
		if (GameCanvas.isTouch) { // INIT TOUCH CONTROL POSITION

			yTouchBar = gH - 88;

			xC = gW - 100;
			yC = 2;

			if (GameCanvas.isTouchControlSmallScreen) {
				xC = gW / 2 - 2;
				yC = yTouchBar + 50;
			}
			int xtrungtam = 3*Image.getWidth(LoadImageInterface.imgMoveNormal)/2;
			
			GuiMain.xCenter = xtrungtam;
			GuiMain.yCenter = yTouchBar + 60;

			GuiMain.xL = 3*Image.getWidth(LoadImageInterface.imgMoveNormal)/2-Image.getWidth(LoadImageInterface.imgMoveNormal)/2;
			GuiMain.yL = GuiMain.yCenter; // left
			GuiMain.xR = xtrungtam+Image.getWidth(LoadImageInterface.imgMoveNormal)/2;
			GuiMain.yR = GuiMain.yCenter; // right

			xF = gW - 50;
			yF = yTouchBar + 35; // fire

			GuiMain.xU = xtrungtam;
			GuiMain.yU = GuiMain.yCenter-Image.getWidth(LoadImageInterface.imgMoveNormal)/2; // up
			
			
			GuiMain.gamePad = new GamePad();
			
			xHP = gW - 74;
			yHP = yTouchBar + 13; // hp

			xMP = gW - 85;
			yMP = yTouchBar + 50; // mp

			xTG = gW - 37;
			yTG = yTouchBar - 1;

//			if (GameCanvas.w >= 400) {
				yU -= 15;
//				GuiMain.xU += 28;
//				GuiMain.xR += 45;
//				GuiMain.xL += 10;
				yTG -= 12;
				yHP -= 7;
				xF -= 18;
				xTG -= 10;
				xHP -= 17;
				xMP -= 24;
//			} 
//			else if (GameCanvas.w >= 360) {
//				GuiMain.yU -= 5;
//				GuiMain.xU += 6;
//				GuiMain.xR += 12;
//				yTG -= 2;
//				yHP -= 2;
//				xHP -= 2;
//				xMP -= 2;
//			}

		}

		xS = new int[onScreenSkill.length];
		yS = new int[onScreenSkill.length];
		if (GameCanvas.isTouch) {
			if (GameCanvas.isTouchControlSmallScreen) {
				xSkill = 2;
				ySkill = 55;
				padSkill = 5;
				for (int i = 0; i < xS.length; i++) {
					xS[i] = i * (25 + padSkill);
					yS[i] = ySkill;
				}
			} else {
				if (GameCanvas.w <= 320)
					xSkill = gW2 - (onScreenSkill.length * 25) / 2 - 15;
				else
					xSkill = gW2 - (onScreenSkill.length * 25) / 2;
				ySkill = yTouchBar + 58;
				padSkill = 5;

				for (int i = 0; i < xS.length; i++) {
					xS[i] = i * (25 + padSkill);
					yS[i] = ySkill;
				}
			}
		} else {
			xSkill = 0;
			for (int i = 0; i < yS.length; i++) {
				xS[i] = 2;
				yS[i] = 2 + 25 * i;
			}
		}
	}

	static int shaking, count = 0;
	static int deltaY = 20;

	private static final void updateCamera() {
		if (cmx != cmtoX || cmy != cmtoY) {
			cmvx = (cmtoX - cmx) << 2;
			cmvy = (cmtoY - cmy) << 2;

			cmdx += cmvx;

			cmx += cmdx >> 4;

			cmdx = cmdx & 0xf;

			cmdy += cmvy;
			cmy += cmdy >> 4;
			cmdy = cmdy & 0xf;

			if (cmx < 24)
				cmx = 24;
			if (cmx > cmxLim)
				cmx = cmxLim;
			if (cmy < 0)
				cmy = 0;
			if (cmy > cmyLim)
				cmy = cmyLim;

		}

		gssx = cmx / TileMap.size - 1;
		if (gssx < 0)
			gssx = 0;
		gssy = cmy / TileMap.size;
		gssxe = gssx + gssw;
		gssye = gssy + gssh;
		if (gssy < 0)
			gssy = 0;
		if (gssye > TileMap.tmh - 1)
			gssye = TileMap.tmh - 1;
		//
		TileMap.gssx = (Char.myChar().cx - 2 * gW) / TileMap.size;
		if (TileMap.gssx < 0)
			TileMap.gssx = 0;
		TileMap.gssxe = TileMap.gssx + TileMap.countx;
		if (TileMap.gssxe > TileMap.tmw) {
			TileMap.gssxe = TileMap.tmw;
			TileMap.gssx = TileMap.gssxe - TileMap.countx;
		}

		TileMap.gssy = (Char.myChar().cy - 2 * gH) / TileMap.size;
		if (TileMap.gssy < 0)
			TileMap.gssy = 0;

		TileMap.gssye = TileMap.gssy + TileMap.county;

		if (TileMap.gssye > TileMap.tmh) {
			TileMap.gssye = TileMap.tmh;
			TileMap.gssy = TileMap.gssye - TileMap.county;
		}
		scrMain.updatecm();
//		scrInfo.updatecm();
//		if(iconChat!=null)//icon chat private
//		{
//			iconChat.scrMain.updatecm();
//		}
	
	
	
			
	}

	public boolean testAct() {
		for (byte i = 2; i < 9; i += 2)
			if (GameCanvas.keyHold[i])
				return false;
		return true;
	}
	private boolean checkSkillValid2() {
		if (Char.myChar().myskill != null && Char.myChar().cMP < Char.myChar().myskill.manaUse) {
			return false;
		}
		if (Char.myChar().myskill == null || (Char.myChar().myskill.template.maxPoint > 0 && Char.myChar().myskill.point == 0)) {
			return false;
		}
//		if (Char.myChar().arrItemBody[Item.TYPE_VUKHI] == null) {
//			return false;
//		}
		return true;
	}

	public void resetButton() {
		if (Char.myChar().arrItemBag != null) {
			if ((isPaintUpPearl || isPaintLuyenThach) && arrItemUpPeal != null) {
				for (int i = 0; i < arrItemUpPeal.length; i++) {
					if (arrItemUpPeal[i] != null) {
						Char.myChar().arrItemBag[arrItemUpPeal[i].indexUI] = arrItemUpPeal[i];
						arrItemUpPeal[i] = null;
					}
				}
			}
			if (isPaintUpGrade) {
				if (itemUpGrade != null) {
					Char.myChar().arrItemBag[itemUpGrade.indexUI] = itemUpGrade;
					itemUpGrade = null;
				}
				if (arrItemUpGrade != null) {
					for (int i = 0; i < arrItemUpGrade.length; i++) {
						if (arrItemUpGrade[i] != null) {
							Char.myChar().arrItemBag[arrItemUpGrade[i].indexUI] = arrItemUpGrade[i];
							arrItemUpGrade[i] = null;
						}
					}
				}
			}
			if (isPaintAuctionSale) {
				if (itemSell != null) {
					Char.myChar().arrItemBag[itemSell.indexUI] = itemSell;
					itemSell = null;
				}
			}
			if (isPaintConvert) {
				if (arrItemConvert != null) {
					for (int i = 0; i < arrItemConvert.length; i++) {
						if (arrItemConvert[i] != null) {
							Char.myChar().arrItemBag[arrItemConvert[i].indexUI] = arrItemConvert[i];
							arrItemConvert[i] = null;
						}
					}
				}
			}
			if ((isPaintSplit || isPaintDichChuyen || isPaintTinhluyen)) {
				if (itemSplit != null) {
					Char.myChar().arrItemBag[itemSplit.indexUI] = itemSplit;
					itemSplit = null;
				}
				if (arrItemSplit != null) {
					for (int i = 0; i < arrItemSplit.length; i++) {
						if (arrItemSplit[i] != null) {
							if (isPaintTinhluyen || isPaintDichChuyen) {
								Char.myChar().arrItemBag[arrItemSplit[i].indexUI] = arrItemSplit[i];
							}
							arrItemSplit[i] = null;
						}
					}
				}
			}
			if (isPaintTrade) {
				InfoDlg.hide();
				if (coinTrade > 0)
					Char.myChar().xu += coinTrade;
				if (arrItemTradeMe != null) {
					for (int i = 0; i < arrItemTradeMe.length; i++) {
						if (arrItemTradeMe[i] != null) {
							Char.myChar().arrItemBag[arrItemTradeMe[i].indexUI] = arrItemTradeMe[i];
							arrItemTradeMe[i] = null;
						}
					}
				}
				if (arrItemTradeOrder != null) {
					for (int i = 0; i < arrItemTradeOrder.length; i++) {
						arrItemTradeOrder[i] = null;
					}
				}
			}
		}
		
		// currentCharViewInfo = null;
		GameCanvas.menu.showMenu = false;
		ChatTextField.gI().close();
		ChatTextField.gI().center = null;
		if (!GameCanvas.isTouch)
			isPaintSelectSkill = false;
		isMessageMenu = false;
		isPaintUpGradeGold = false;
		isLockKey = false;
		isPaintZone = false;
		isPaintAuto = false;
		isPaintInfoMe = false;
		isPaintItemInfo = false;
		isPaintTask = false;
		isPaintTeam = false;
		isPaintMessage = false;
		isPaintClan = false;
		isPaintLuckySpin = false;
		isRequestMember = false;
		isPaintCharInMap = false;
		isPaintFindTeam = false;
		isPaintFriend = false;
		isPaintAuctionBuy = false;
		isPaintList = false;
		isPaintEnemies = false;
		isPaintAlert = false;
		typeTrade = 0;
		isPaintStore = false;
		isPaintEliteShop = false;
		isPaintNonNam = false;
		isPaintNonNu = false;
		isPaintAoNam = false;
		isPaintAoNu = false;
		isPaintGangTayNam = false;
		isPaintGangTayNu = false;
		isPaintQuanNam = false;
		isPaintQuanNu = false;
		isPaintGiayNam = false;
		isPaintGiayNu = false;
		isPaintLien = false;
		isPaintNhan = false;
		isPaintNgocBoi = false;
		isPaintPhu = false;
		isPaintWeapon = false;
		isPaintStack = false;
		isPaintStackLock = false;
		isPaintGrocery = false;
		isPaintGroceryLock = false;
		isPaintUpGrade = false;
		isPaintAuctionSale = false;
		isPaintConvert = false;
		isPaintSplit = isPaintTinhluyen = isPaintDichChuyen = false;
		isPaintTrade = false;
		isPaintUpPearl = isPaintLuyenThach = false;
		isPaintBox = false;
		indexMenu = 0;
		indexSelect = 0;
		indexItemUse = -1;
		indexRow = -1;
		indexRowMax = 0;
		indexTitle = 0;
		typeTrade = typeTradeOrder = 0;
		left = menu;
		right = cmdFocus;
		xMounts = yMounts = null;
		center = null;
//		if (Char.myChar().cHP <= 0 || Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY) {
//			if (GameCanvas.isTouchControlSmallScreen)
//				cmdDead.caption = "";
//			center = cmdDead;
//		}
		// System.out.println("RESET");
		scrMain.clear();
	}

	//execute event key press
	public void keyPress(int keyCode) {
		
		if (tfText != null && tfText.isFocus)
			tfText.keyPressed(keyCode);
		if (tfCharFriend != null && tfCharFriend.isFocus){
			tfCharFriend.keyPressed(keyCode);
		}
		//chat world
		if(guiChatClanWorld!=null)
			guiChatClanWorld.KeyPress(keyCode);
		
		super.keyPress(keyCode);
	}

	public void updateKey() {

        if(isAutoDanh&&(GameCanvas.keyPressed[2]||GameCanvas.keyPressed[6]||GameCanvas.keyPressed[4]))
            isAutoDanh = false;
		if(!GameCanvas.gameScreen.guiChatClanWorld.moveClose&&!GameCanvas.menu.showMenu)
			guiMain.UpdateKey();
		if(MenuIcon.isShowTab) return;
		guiChatClanWorld.updatePointer();
		guiChatClanWorld.UpdateKey();
		boolean isUpdatePhim = false;
		if(qShortQuest!=null)
			qShortQuest.UpdateKey();
		
		if (!MenuIcon.isShowTab&&Screen.getCmdPointerLast(guiMain.bntAttack)) {
				//doFire();
				doTouchQuickSlot(0);
				isUpdatePhim = true;
		}
		if (!MenuIcon.isShowTab&&Screen.getCmdPointerLast(guiMain.bntAttack_1)) {
			//doFire();
			doTouchQuickSlot(1);
			isUpdatePhim = true;
		}
		if (!MenuIcon.isShowTab&&Screen.getCmdPointerLast(guiMain.bntAttack_2)) {
			//doFire();
			doTouchQuickSlot(2);
			isUpdatePhim = true;
		}
		if (!MenuIcon.isShowTab&&Screen.getCmdPointerLast(guiMain.bntAttack_3)) {
			//doFire();
			doTouchQuickSlot(3);
			isUpdatePhim = true;
		}
		if (!MenuIcon.isShowTab&&Screen.getCmdPointerLast(guiMain.bntAttack_4)) {
			//doFire();
			doTouchQuickSlot(4);
			isUpdatePhim = true;
		}
//		if (GameCanvas.keyPressed[5]) {
//			Service.gI().requestFriendList((byte)Friend.REQUEST_FRIEND_LIST, (short)Char.myChar().charID);
//			GameCanvas.keyPressed[5] = false;
//			doFire();
//			
//		}
//		guiMain.UpdateKey();
//		if(tradeGui != null)
//			tradeGui.updatekey();
//		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.btnUnfriend)) {
//			if (GameCanvas.gameScreen.btnUnfriend != null) {
//				GameCanvas.isPointerJustRelease = false;
//				GameCanvas.keyPressed[5] = false;
//				Screen.keyTouch = -1;
//				if (GameCanvas.gameScreen.btnUnfriend != null)
//					GameCanvas.gameScreen.btnUnfriend.performAction();
//			}
//		}
//		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.cmdAcceptTrade)) {
//			if (GameCanvas.gameScreen.cmdAcceptTrade != null) {
//				GameCanvas.isPointerJustRelease = false;
//				GameCanvas.keyPressed[5] = false;
//				Screen.keyTouch = -1;
//				if (GameCanvas.gameScreen.cmdAcceptTrade != null)
//					GameCanvas.gameScreen.cmdAcceptTrade.performAction();
//			}
//		}
		if(isPaintFriend){
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.btnChat)) {
				if (GameCanvas.gameScreen.btnChat != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (GameCanvas.gameScreen.btnChat != null)
						GameCanvas.gameScreen.btnChat.performAction();
				}
			}
		}
		updateSelectList();
		if(ispaintChat){
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.chat)) {
				if (GameCanvas.gameScreen.chat != null) {
					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (GameCanvas.gameScreen.chat != null)
						GameCanvas.gameScreen.chat.performAction();
				}
			}
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.bntIconChat)) {
				if (GameCanvas.gameScreen.bntIconChat != null) {
					GameCanvas.isPointerJustRelease = false;		
//					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (GameCanvas.gameScreen.bntIconChat != null)
						GameCanvas.gameScreen.bntIconChat.performAction();
				}
			}
			if(Char.toCharChatSelected!=null&&!isPaintZone)
			{
				Char.toCharChatSelected.update();
			}
		}
		
		//chat private
		
		
		if(GameCanvas.menu.showMenu){
			return;
		}
		if (InfoDlg.isLock)
			return;
//		setFriendCommand();
//		guiMain.UpdateKey();
//		if(qShortQuest!=null)
//		{
//			qShortQuest.UpdateKey();
//		}
//		if(guiChatClanWorld!=null)
//			guiChatClanWorld.UpdateKey();
//		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.mChatWorld)) {
//			if (GameCanvas.gameScreen.mChatWorld != null) {
//				GameCanvas.isPointerJustRelease = false;
//				GameCanvas.keyPressed[5] = false;
//				Screen.keyTouch = -1;
//				if (GameCanvas.gameScreen.mChatWorld != null)
//					GameCanvas.gameScreen.mChatWorld.performAction();
//			}
//		}
//		guiMain.UpdateKey();
////		if (GameCanvas.isTouch && !ChatTextField.gI().isShow && !GameCanvas.menu.showMenu) {
////			doViewMessagebyTouch();
////			GuiMain.updateKeyTouchControl();
////			updateKeyTouchControl();
////		}
//	
//		
//	
//		showCharList();
//
//		if (ChatPopup.currentMultilineChatPopup != null) {
//			Command cmdNextLine = ChatPopup.currentMultilineChatPopup.cmdNextLine;
//			if (/*GameCanvas.keyPressed[5] || */Screen.getCmdPointerLast(cmdNextLine)) {
//				if (cmdNextLine != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (cmdNextLine != null)
//						cmdNextLine.performAction();
//				}
//			}
//		} else if (!ChatTextField.gI().isShow) {//not opening text field
//			
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.chat)) {
//				if (GameCanvas.gameScreen.chat != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (GameCanvas.gameScreen.chat != null)
//						GameCanvas.gameScreen.chat.performAction();
//				}
//			}
//			
//			//party
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.mParty)) {
//				if (GameCanvas.gameScreen.mParty != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (GameCanvas.gameScreen.mParty != null)
//						GameCanvas.gameScreen.mParty.performAction();
//				}
//			}
//			
//			//chat private
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.mChatPrivate)) {
//				if (GameCanvas.gameScreen.mChatPrivate != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (GameCanvas.gameScreen.mChatPrivate != null)
//						GameCanvas.gameScreen.mChatPrivate.performAction();
//				}
//			}
//			
//			//chat world
////			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.mChatWorld)) {
////				if (GameCanvas.gameScreen.mChatWorld != null) {
////					GameCanvas.isPointerJustRelease = false;
////					GameCanvas.keyPressed[5] = false;
////					Screen.keyTouch = -1;
////					if (GameCanvas.gameScreen.mChatWorld != null)
////						GameCanvas.gameScreen.mChatWorld.performAction();
////				}
////			}
//			
//			//bag
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.mBag)) {
//				if (GameCanvas.gameScreen.mBag != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (GameCanvas.gameScreen.mBag != null)
//						GameCanvas.gameScreen.mBag.performAction();
//				}
//			}
//			
//			//bag
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.mListFriend)) {
//				if (GameCanvas.gameScreen.mListFriend != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (GameCanvas.gameScreen.mListFriend != null)
//						GameCanvas.gameScreen.mListFriend.performAction();
//				}
//			}
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.mGuiQuest)) {
//				if (GameCanvas.gameScreen.mGuiQuest != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (GameCanvas.gameScreen.mGuiQuest != null)
//						GameCanvas.gameScreen.mGuiQuest.performAction();
//				}
//			}
//			
//			//icon chat
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.bntIconChat)) {
//				if (GameCanvas.gameScreen.bntIconChat != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (GameCanvas.gameScreen.bntIconChat != null)
//						GameCanvas.gameScreen.bntIconChat.performAction();
//				}
//			}
//			
//			//unfriend
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.btnUnfriend)) {
				if (GameCanvas.gameScreen.btnUnfriend != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (GameCanvas.gameScreen.btnUnfriend != null)
						GameCanvas.gameScreen.btnUnfriend.performAction();
				}
			}
			
			//chat private
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.gameScreen.btnChat)) {
//				if (GameCanvas.gameScreen.btnChat != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (GameCanvas.gameScreen.btnChat != null)
//						GameCanvas.gameScreen.btnChat.performAction();
//				}
//			}
////			
//			if (GameCanvas.keyPressed[12] || Screen.getCmdPointerLast(GameCanvas.currentScreen.left)) {
//
//			}
//			if (GameCanvas.keyPressed[13] || (Screen.getCmdPointerLast(GameCanvas.currentScreen.right))) {
//				isPaintFriend = false;//paint chat friend
//				ispaintChat=false;//paint chat
//				
//				center = left = right = null;
//				indexRow = -1;
//				if(guiChatClanWorld!=null)
//				{
//					guiChatClanWorld.Close();
//				}
//				isBag=false;//close paint bag
//				isPaintFriend=false;//close paint list friend
//				isPaintFriend=false;//close paint chat private
//				isPaintTeam=false;//close paint party
//				isPaintQuest=false;//close paint com.sakura.thelastlegend.gui quest
//				
//			}
//			
//			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(GameCanvas.currentScreen.center)) {
//				if (center != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (center != null)
//						center.performAction();
//				}
//			}
//			
//			//quest
////			if(qShortQuest!=null)
////			{
////				qShortQuest.UpdateKey();
////			}
//			
////			guiMain.UpdateKey();
////			
////			// tab
////			if(isBag)
////			{
////				GameCanvas.AllInfo.updatekey();
////			}
//			
//
//		} else {
//			//// update button ben trai
//			if (ChatTextField.gI().left != null) {
//				if (GameCanvas.keyPressed[12] || Screen.getCmdPointerLast(ChatTextField.gI().left)) { 
//					if (ChatTextField.gI().left != null)
//						ChatTextField.gI().left.performAction();
//				}
//			}
//			if (ChatTextField.gI().right != null) {
//				if (GameCanvas.keyPressed[13] || Screen.getCmdPointerLast(ChatTextField.gI().right)) {
//					if (ChatTextField.gI().right != null)
//						ChatTextField.gI().right.performAction();
//				}
//			}
//			if (ChatTextField.gI().center != null) {
//				if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(ChatTextField.gI().center)) {
//					if (ChatTextField.gI().center != null)
//						ChatTextField.gI().center.performAction();
//				}
//			}
//		}
	
		if (Char.myChar().currentMovePoint != null) {
			for (int i = 0; i < GameCanvas.keyPressed.length; i++) {
				if (GameCanvas.keyPressed[i]) {
					Char.myChar().currentMovePoint = null;
					break;
				}
			}
		}

		if (ChatTextField.gI().isShow && GameCanvas.keyAsciiPress != 0) {
			ChatTextField.gI().keyPressed(GameCanvas.keyAsciiPress);
			GameCanvas.keyAsciiPress = 0;
		}
//		if (isLockKey) {
//			GameCanvas.clearKeyHold();
//			GameCanvas.clearKeyPressed();
//			return;
//		}
		if (GameCanvas.menu.showMenu || isOpenUI() || Char.isLockKey){
			return;
		}

		// /------------------------------
		if (GameCanvas.keyPressed[10]) {
			GameCanvas.keyPressed[10] = false;
		
			GameCanvas.clearKeyPressed();
		}
		if (GameCanvas.keyPressed[11]) {
			GameCanvas.keyPressed[11] = false;
		
			GameCanvas.clearKeyPressed();
		}
		if (GameCanvas.keyAsciiPress != 0) {
			if (TField.isQwerty) {
				if (GameCanvas.keyAsciiPress == (char) ' ') {
					
					GameCanvas.keyAsciiPress = 0;
					GameCanvas.clearKeyPressed();
				} else if (GameCanvas.keyAsciiPress == (char) '@') {
				
					GameCanvas.keyAsciiPress = 0;
					GameCanvas.clearKeyPressed();
				} else if (GameCanvas.keyAsciiPress == (char) '0') {
					
					GameCanvas.keyAsciiPress = 0;
					GameCanvas.clearKeyPressed();
				} else if (GameCanvas.keyAsciiPress == (char) '?') {
					
					GameCanvas.keyAsciiPress = 0;
					GameCanvas.clearKeyPressed();
				}

			}
		}
		// /------------------------------
		if (Char.myChar().skillPaint != null)
			return;
		if(MenuIcon.isShowTab){//truong hop hien tab ko update key
			
		}else if (Char.myChar().statusMe == Char.A_STAND) {
			if (!MenuIcon.isShowTab&&(GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(guiMain.bntAttack))) {
				GameCanvas.keyPressed[5] = false;
				isUpdatePhim = true;
	            //doFire();
	            doTouchQuickSlot(0);
				
			} else if (GameCanvas.keyHold[2]) {
				Char.myChar().updateCharJump();
				isUpdatePhim = true;
				if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
					setCharJump(0);
				Char.myChar().updateCharJump();
				isAutoDanh = false;
			} else if (GameCanvas.keyHold[1]) {
				Char.myChar().cdir = -1;
				isUpdatePhim = true;
				if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
					setCharJump(-4);
				isAutoDanh = false;
			} else if (GameCanvas.keyHold[3]) {
				Char.myChar().cdir = 1;
				isUpdatePhim = true;
				if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
					setCharJump(4);
				isAutoDanh = false;
			} else if (GameCanvas.keyHold[4]) {
				Char.myChar().isAttack = false;
				isUpdatePhim = true;
				Char.myChar().updateCharRun();
				if (Char.myChar().cdir == 1) {
					Char.myChar().cdir = -1;
					
				} else if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
					if ((Char.myChar().cx - Char.myChar().cxSend) != 0)
						Service.gI().charMove();
						Char.myChar().updateCharRun();
					Char.myChar().statusMe = Char.A_RUN;
					Char.myChar().isStartSoundRun = true;
					Music.play(Music.RUN, 12);
					isAutoDanh = false;
					Char.myChar().cvx = -Char.myChar().getSpeed();
				}
			} else if (GameCanvas.keyHold[6]) {
				Char.myChar().isAttack = false;
				isUpdatePhim = true;
				Char.myChar().updateCharRun();
				if (Char.myChar().cdir == -1) {
					Char.myChar().cdir = 1;

				} else if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
					if ((Char.myChar().cx - Char.myChar().cxSend) != 0)
						Service.gI().charMove();
						Char.myChar().updateCharRun();
					Char.myChar().statusMe = Char.A_RUN;
					Char.myChar().isStartSoundRun = true;
					Music.play(Music.RUN, 12);
					isAutoDanh = false;
					Char.myChar().cvx = Char.myChar().getSpeed();
				}
			}
		} else if (Char.myChar().statusMe == Char.A_RUN) {
			if (!MenuIcon.isShowTab&&GameCanvas.keyPressed[5]) {
				GameCanvas.keyPressed[5] = false;
				isUpdatePhim = true;

	            //doFire();
	            doTouchQuickSlot(0);
			} else if (GameCanvas.keyHold[2]) {
				if ((Char.myChar().cx - Char.myChar().cxSend) != 0 || (Char.myChar().cy - Char.myChar().cySend) != 0)
					Service.gI().charMove();

				isUpdatePhim = true;
				Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
				Char.myChar().statusMe = Char.A_JUMP;
				isAutoDanh = false;
				Char.myChar().cp1 = 0;
			} else if (GameCanvas.keyHold[1]) {
				if ((Char.myChar().cx - Char.myChar().cxSend) != 0 || (Char.myChar().cy - Char.myChar().cySend) != 0)
					Service.gI().charMove();
				Char.myChar().cdir = -1;
				Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
				Char.myChar().cvx = -4;
				Char.myChar().statusMe = Char.A_JUMP;
				isAutoDanh = false;
				Char.myChar().cp1 = 0;
				isUpdatePhim = true;
			} else if (GameCanvas.keyHold[3]) {
				if ((Char.myChar().cx - Char.myChar().cxSend) != 0 || (Char.myChar().cy - Char.myChar().cySend) != 0)
					Service.gI().charMove();
				Char.myChar().cdir = 1;
				Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
				Char.myChar().cvx = 4;
				Char.myChar().statusMe = Char.A_JUMP;
				isAutoDanh = false;
				Char.myChar().cp1 = 0;
				isUpdatePhim = true;
			} else if (GameCanvas.keyHold[4]) {
				if (Char.myChar().cdir == 1) {
					Char.myChar().cdir = -1;
				} else {
					Char.myChar().cvx = -Char.myChar().getSpeed() + Char.myChar().cBonusSpeed;

				}
				isUpdatePhim = true;
			} else if (GameCanvas.keyHold[6]) {
				if (Char.myChar().cdir == -1) {
					Char.myChar().cdir = 1;
				} else {
					Char.myChar().cvx = Char.myChar().getSpeed() + Char.myChar().cBonusSpeed;
				}

				isUpdatePhim = true;
			}
		} else if (Char.myChar().statusMe == Char.A_JUMP) {
			if (!MenuIcon.isShowTab&&GameCanvas.keyPressed[5]) {
				GameCanvas.keyPressed[5] = false;
				isUpdatePhim = true;

	            //doFire();
	            doTouchQuickSlot(0);
			}
			if (GameCanvas.keyHold[4] || GameCanvas.keyHold[1]) {

				if (Char.myChar().cdir == 1) {
					Char.myChar().cdir = -1;
				} else {
					Char.myChar().cvx = -Char.myChar().getSpeed();
				}
				//			
				isAutoDanh = false;
			} else if (GameCanvas.keyHold[6] || GameCanvas.keyHold[3]) {

				if (Char.myChar().cdir == -1) {
					Char.myChar().cdir = 1;
				} else {
					Char.myChar().cvx = Char.myChar().getSpeed();
				}
				isAutoDanh = false;
			}
			if (GameCanvas.keyHold[2] || GameCanvas.keyHold[1] || GameCanvas.keyHold[3]) {

				if (Char.myChar().canJumpHigh && Char.myChar().cp1 == 0 && Char.myChar().cvy > -4) {
					Char.myChar().cp1++;
					Char.myChar().cvy = -7;

				}

				isAutoDanh = false;
			}

		} else if (Char.myChar().statusMe == Char.A_FALL) {
//			Char.myChar().updateCharFall();
			if (!MenuIcon.isShowTab&&GameCanvas.keyPressed[5]) {
				GameCanvas.keyPressed[5] = false;

	            //doFire();
	            doTouchQuickSlot(0);
				
			}
			if (GameCanvas.keyPressed[2]) {

				GameCanvas.clearKeyPressed();
				// Char.gI().cy -= 3;
			}
			if (GameCanvas.keyHold[4]) {

				if (Char.myChar().cdir == 1)
					Char.myChar().cdir = -1;
				else
					Char.myChar().cvx = -Char.myChar().getSpeed();

			} else if (GameCanvas.keyHold[6]) {
				if (Char.myChar().cdir == -1)
					Char.myChar().cdir = 1;
				else
					Char.myChar().cvx = Char.myChar().getSpeed();
			}
		} else if (Char.myChar().statusMe == Char.A_WATERRUN) {
			if (!MenuIcon.isShowTab&&GameCanvas.keyPressed[5]) {
				GameCanvas.keyPressed[5] = false;

	            //doFire();
	            doTouchQuickSlot(0);
				
			}
			if (GameCanvas.keyHold[2]) {
				if ((Char.myChar().cx - Char.myChar().cxSend) != 0 || (Char.myChar().cy - Char.myChar().cySend) != 0)
					Service.gI().charMove();
				Char.myChar().cvy = -10;
				Char.myChar().statusMe = Char.A_JUMP;
				Char.myChar().cp1 = 0;
			} else if (GameCanvas.keyHold[4]) {
				if (Char.myChar().cdir == 1)
					Char.myChar().cdir = -1;
				else {
					Char.myChar().cvx = -5;// -Char.cwspeed + Char.cBonusSpeed;
				}
			} else if (GameCanvas.keyHold[6]) {
				if (Char.myChar().cdir == -1)
					Char.myChar().cdir = 1;
				else {
					Char.myChar().cvx = 5;// Char.cwspeed + Char.cBonusSpeed;
				}
			}
		} else if (Char.myChar().statusMe == Char.A_ATTK) {
			if (!MenuIcon.isShowTab&&GameCanvas.keyPressed[5]) {
				GameCanvas.keyPressed[5] = false;

	            //doFire();
	            doTouchQuickSlot(0);
			}
			if (GameCanvas.keyHold[4]) {

				if (Char.myChar().cdir == 1)
					Char.myChar().cdir = -1;
				else {
					Char.myChar().cvx = -Char.myChar().getSpeed() + 2;
				}

				isAutoDanh = false;
			} else if (GameCanvas.keyHold[6]) {
				if (Char.myChar().cdir == -1)
					Char.myChar().cdir = 1;
				else {
					Char.myChar().cvx = Char.myChar().getSpeed() - 2;
				}
				isAutoDanh = false;
			}
		} else if (Char.myChar().statusMe == Char.A_WATERDOWN) {
			if (!MenuIcon.isShowTab&&GameCanvas.keyPressed[5]) {
				GameCanvas.keyPressed[5] = false;

	            //doFire();
	            doTouchQuickSlot(0);
			}
			if (GameCanvas.keyHold[2]) {
				Char.myChar().cvy = -10;
				Char.myChar().statusMe = Char.A_JUMP;
				Char.myChar().cp1 = 0;
			}
		}
		if (GameCanvas.keyPressed[Key.NUM8] && GameCanvas.keyAsciiPress != (char) '8') {
			GameCanvas.keyPressed[Key.NUM8] = false;
//			doChangeSkill();
		}

//		//execute com.sakura.thelastlegend.gui icon chat
//		if(iconChat!=null)
//		{
//			iconChat.Updatecm();	
//			iconChat.updateKeySelectIconChat();
//			if(iconChat.indexSelect>=0)
//			{
//				tfCharFriend.setText(tfCharFriend.getText()+NodeChat.maEmo[iconChat.indexSelect]) ; 
//				iconChat.indexSelect=-1;
//				GameScreen.isPaintZone =false;
//			}
//		}
//		else
//		{
//			updateSelectList();
//			updatekeychat();
//		}

		
		//update key chat world
//				if(guiChatClanWorld!=null)
//					guiChatClanWorld.UpdateKey();
				//update guiChatClanWorld
//				if(guiChatClanWorld!=null)
//				{
					guiChatClanWorld.updatePointer();
//				}		
				
				guiContact.UpdateKey();
				

				if(!isUpdatePhim&&GameCanvas.isPointerJustRelease){
					for (int i = 0; i < vNpc.size(); i++) {
						Npc c = (Npc)vNpc.elementAt(i);
						if(c!=null&&GameCanvas.isPointer(c.cx-20-cmx,c.cy-60-cmy,40,60))
						{
//							if(CRes.abs(Char.myChar().cx-c.cx)>20||CRes.abs(Char.myChar().cy-c.cy)>20)
//								Char.myChar().moveTo(c.cx, c.cy);	
							Char.myChar().clearAllFocus();
							Char.myChar().npcFocus = c;
							Cout.println(" npcFocus "+Char.myChar().npcFocus);
							GameCanvas.isPointerJustRelease = false;
							return;
						}
					}
					for (int i = 0; i < vCharInMap.size(); i++) {
						Char c = (Char)vCharInMap.elementAt(i);
						if(c!=null&&GameCanvas.isPointer(c.cx-20-cmx,c.cy-60-cmy,40,60))
						{
							if(c.charID==Char.myChar().charID&&(Char.myChar().statusMe==Char.A_DEAD||Char.myChar().statusMe==Char.A_DEADFLY)){
									GameCanvas.startCommandDlg("Bạn có muốn hồi sinh tại chỗ (1 gold)?", 
											new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
											new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
									return;
							}
							else if(c.charID!=Char.myChar().charID){
	//							if(CRes.abs(Char.myChar().cx-c.cx)>20||CRes.abs(Char.myChar().cy-c.cy)>20)
	//							Char.myChar().moveTo(c.cx, c.cy);
								Char.myChar().clearAllFocus();
								Char.myChar().charFocus = c;
								Service.gI().requestPlayerInfo((short)c.charID);
								GameCanvas.isPointerJustRelease = false;
								return;
							}
						}
					}
					for (int i = 0; i < vMob.size(); i++) {
						Mob c = (Mob)vMob.elementAt(i);
						if(c!=null&&GameCanvas.isPointer(c.x-c.getW()/2-cmx,c.y-c.getH()-cmy,c.getW(),c.getH()))
						{
							if(c.mobName==null||c.mobName.equals(""))
								Service.gI().requetsInfoMod((short) c.mobId);
							if(Char.myChar().mobFocus!=null&&c.mobId==Char.myChar().mobFocus.mobId){
								doTouchQuickSlot(0);
								if(!isAutoDanh){
									xStartAuto=Char.myChar.cx;
									yStartAuto=Char.myChar.cy;
								}
								byte[] listauto = Rms.loadRMS(Rms.rms_Auto);
								if(listauto==null)
								{
								}else {
									if(listauto[SettingScreen.AUTO_DANH]==1)
										isAutoDanh = true;
								}
								return;
							}
							if(c.isBoss&&!Mob.isBossAppear) continue;
							Char.myChar().clearAllFocus();
							Char.myChar().mobFocus = c;
							GameCanvas.isPointerJustRelease = false;
							if(Char.myChar().statusMe==Char.A_DEAD||Char.myChar().statusMe==Char.A_DEADFLY)
								return;
							int maxkcX = CRes.abs(Char.myChar().cx-c.x);
							int kc = 1,leng=0;
							if(maxkcX>20||CRes.abs(Char.myChar().cy-c.y)>10){
								
								for (int j = 0; j < maxkcX; j++) {
									
									leng +=j;
									if(leng>= maxkcX)
									{
										kc = j-1;
										break;
									}
								}
								if(Char.myChar().cx>c.x){
									Char.myChar().updateCharRun();
									if (Char.myChar().cdir == 1) {
										Char.myChar().cdir = -1;
										
									}
									if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
										if ((Char.myChar().cx - Char.myChar().cxSend) != 0)
											Service.gI().charMove();
											Char.myChar().updateCharRun();
										Char.myChar().statusMe = Char.A_RUN;
										
										Char.myChar().cvx = -kc;
									}
								}else{
									Char.myChar().updateCharRun();
									if (Char.myChar().cdir == -1) {
										Char.myChar().cdir = 1;
										
									}
									if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
										if ((Char.myChar().cx - Char.myChar().cxSend) != 0)
											Service.gI().charMove();
											Char.myChar().updateCharRun();
										Char.myChar().statusMe = Char.A_RUN;
										Char.myChar().cvx = kc;
									}
								}
							}
							return;
						}
					}
					for (int i = 0; i < vItemMap.size(); i++) {
						ItemMap c = (ItemMap)vItemMap.elementAt(i);
						if(c!=null&&GameCanvas.isPointer(c.x-14-cmx,c.y-28-cmy,14,28))
						{
//							if(CRes.abs(Char.myChar().cx-c.x)>20||CRes.abs(Char.myChar().cy-c.y)>20)
//								Char.myChar().moveTo(c.x, c.y);
							Char.myChar().clearAllFocus();
							Char.myChar().itemFocus = c;
							Cout.println(" itemFocus "+Char.myChar().itemFocus);
							GameCanvas.isPointerJustRelease = false;
							return;
						}
					}
					
				}
				GameCanvas.clearKeyPressed();
	}

	private void doFocusbyTouch() {
		if (isPaintPopup() || isPaintUI() || isOpenUI())
			return;
		int i;
		if (GameCanvas.isPointerClick) {
			for (i = 0; i < vMob.size(); i++) {
				Mob mob = (Mob) vMob.elementAt(i);
				if (!mob.isPaint())
					continue;
				if (GameCanvas.isPointerInGame(mob.x - mob.w / 2, mob.y - mob.h, mob.w, mob.h) && GameCanvas.isPointerJustRelease) {
					Service.gI().requetsInfoMod(mob.mobId);
					if(mob.isBoss&&!Mob.isBossAppear) continue;
					Char.myChar().mobFocus = mob;
					Char.myChar().deFocusNPC();
					Char.myChar().charFocus = null;
					Char.myChar().itemFocus = null;
					Char.isManualFocus = true;
					return;
				}
			}
			

			for (i = 0; i < GameScreen.vCharInMap.size(); i++) {
				Char c = (Char) GameScreen.vCharInMap.elementAt(i);
				if (!c.isPaint()||c.isNhanban())
					continue;

				if (GameCanvas.isPointerInGame(c.cx - c.cw / 2, c.cy - c.ch, c.cw, c.ch) && GameCanvas.isPointerJustRelease) {
					Char.myChar().mobFocus = null;
					Char.myChar().deFocusNPC();
					Char.myChar().charFocus = c;
					Char.myChar().itemFocus = null;
					Char.isManualFocus = true;
					return;
				}
			}
			
			for(i = 0; i < GameScreen.vItemMap.size(); i++){
				ItemMap item = (ItemMap) vItemMap.elementAt(i);
				if(GameCanvas.isPointerInGame(item.x - 24/2, item.y - 24, 24, 24)  && GameCanvas.isPointerJustRelease){
					Char.myChar().mobFocus = null;
					Char.myChar().deFocusNPC();
					Char.myChar().charFocus = null;
					Char.myChar().itemFocus = item;
					Char.isManualFocus = true;
					return;
				}
			}
		}

	}

	private void showCharList() {
		if (GameCanvas.isTouch) {
			if (!GameCanvas.isPointerDown || GameCanvas.isPointerJustRelease)
				return;
			if (GameCanvas.isPointerHoldIn(xTG, yTG, 34, 34) && isPaintCharInMap == false && GameCanvas.isPointerClick)
				if (GameCanvas.isHoldPress()) {
					doShowCharInMap();
				}
		} else {
			if (!GameCanvas.keyHold[13])
				return;
			if (isPaintCharInMap == false && GameCanvas.isHoldPress()) {
				doShowCharInMap();
			}
		}

	}

	long lastMove;

	long lastFire;
	int auto;
	boolean lockAutoMove = false;

	

	private void resetAuto() {
		auto = 0;
		lockAutoMove = Char.myChar().isLockMove = false;
	}

	

	private boolean isMobSameParty() {

		if (Char.myChar().mobFocus != null) {
			if ((Char.myChar().mobFocus.getTemplate().mobTemplateId == 142
					&& Char.myChar().cTypePk == Char.PK_PHE1) ||  (Char.myChar().mobFocus.getTemplate().mobTemplateId == 143
					&& Char.myChar().cTypePk == Char.PK_PHE2)){
				return true;
			}
			return false;
		}
		return false;
	}
	

	int nSkill = 0;
	int selectedIndexSkill = -1;

	

	

	

	
	public void sortSkill() {
		for (int i = 0; i < Char.myChar().vSkillFight.size() - 1; i++) {
			Skill skilli = (Skill) Char.myChar().vSkillFight.elementAt(i);
			for (int j = i + 1; j < Char.myChar().vSkillFight.size(); j++) {
				Skill skillj = (Skill) Char.myChar().vSkillFight.elementAt(j);
				if (skillj.template.id < skilli.template.id) {
					Skill temp = skillj;
					skillj = skilli;
					skilli = temp;
					Char.myChar().vSkillFight.setElementAt(skilli, i);
					Char.myChar().vSkillFight.setElementAt(skillj, j);
				}
			}
		}
	}

	

	private int messageType() {
		int t = -1;
		if (GameCanvas.isPointerClick) {
			for (int i = 0; i < xM.length; i++) {
				if (GameCanvas.isPointerHoldIn(xM[i], yM[i], 100, 12) && GameCanvas.isPointerJustRelease) {
					t = i;
					break;
				}
			}
		}
		return t;
	}

	long longPress = 0;

	

	public void setCharJumpAtt() {
		Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
		Char.myChar().statusMe = Char.A_JUMP;
		Char.myChar().cp1 = 0;
	}

	public void setCharJump(int cvx) {
		if ((Char.myChar().cx - Char.myChar().cxSend) != 0 || (Char.myChar().cy - Char.myChar().cySend) != 0)
			Service.gI().charMove();
		Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
		Char.myChar().cvx = cvx;
		Char.myChar().statusMe = Char.A_JUMP;
		Char.myChar().cp1 = 0;
	}

	// ===========================
	public void updateOpen() {
		if (!isstarOpen)
			return;

		if (moveUp > -3) {
			moveUp -= 4;
		} else
			moveUp = -2;
		if (moveDow < GameCanvas.h + 3) {
			moveDow += 4;
		} else
			moveDow = GameCanvas.h + 2;
		if (moveUp <= -2 && moveDow >= GameCanvas.h + 2) {
			isstarOpen = false;
		}
	}

	long lastSendUpdatePostion;

	public void update() {
//		updateCamera();
		Laroi.update_Laroi();
		updateCloudy();
		if(GameCanvas.imgBG != null)
		for(int i = 0; i < GameCanvas.imgBG.length; i++){
			if(GameCanvas.imgBG[i] == null){
				GameCanvas.loadBG(2);
				break;
			}
		}
		
		scrMain.updatecm();
		//Char.myChar().update();
//		//chat world
		if(guiChatClanWorld!=null)
			guiChatClanWorld.Update();
//		if(Char.myChar()!=null&&GameCanvas.gameTick%75==0){  //hardcode test
//			ChatPopup.addChatPopup("toi la "+Char.myChar().cName, 80,Char.myChar());
//		}
//		ChatTextField.gI().update();
//		// tab
		if(isBag)
		{
			GameCanvas.AllInfo.updatekey();
			GameCanvas.AllInfo.update();
		}
//		
		for (int i = 0; i < veffClient.size(); i++) {
			MainEffect eff = (MainEffect)veffClient.elementAt(i);
			eff.update();
		}
		//updateKey();
		guiMain.update();
//		
		updateCamera();
//
		ChatTextField.gI().update();
		for (int i = 0; i < vCharInMap.size(); i++) {
			((Char) vCharInMap.elementAt(i)).update();
		}
		for (int i = 0; i < vItemMap.size(); i++){
			// paint item
			((ItemMap) vItemMap.elementAt(i)).update();
		}
//		
		for(int i = 0; i < vMob.size(); i++){
			((Mob) vMob.elementAt(i)).update();
		}
//		
//		Char.myChar().update();
//
		for (int i = 0; i < vNpc.size(); i++) {
			((Npc) vNpc.elementAt(i)).update();
		}
//
		updateFlyText();
//
//		for (int i = 0; i < vItemMap.size(); i++)
//			((ItemMap) vItemMap.elementAt(i)).update();
//
		for (int i = 0; i < Effect2.vEffect2.size(); i++) {
			Effect2 l = (Effect2) Effect2.vEffect2.elementAt(i);
			l.update();
		}
//
//		
		autoFocus();
//		//bag
		if(isBag)
		{
			GameCanvas.AllInfo.updatePointer();
		}
//		
//		guiMain.updatePointer();
//			
//		//quest
		if(qShortQuest!=null)
			qShortQuest.Update();
//		
//		if(tradeGui!=null)
//		{
//			tradeGui.update();
//		}
		guiContact.Update();
		if(isAutoDanh)
			AutoDanh();
		if(isAutoNhatItem)
			AutoNhat();
	}
	
	public static void loadDataRMS(){
		byte[] d1, d2, d3, d4;
		d1 = Rms.loadRMS("dataVersion");
		d2 = Rms.loadRMS("mapVersion");
		d3 = Rms.loadRMS("skillVersion");
		d4 = Rms.loadRMS("itemVersion");
		if (d1 != null)
			vcData = d1[0];
		if (d2 != null)
			vcMap = d2[0];
		if (d3 != null)
			vcSkill = d3[0];
		if (d4 != null)
			vcItem = d4[0];
	}
	public static boolean isloadimgfocus = false;
	public static void loadImages(){
		imgSelect = GameCanvas.loadImage("/u/select.png");
		imgHpp = GameCanvas.loadImage("/hd/hpp.png");
		imgMpp = GameCanvas.loadImage("/hd/mpp.png");
		imgSkill = GameCanvas.loadImage("/hd/skill.png");
		imgLbtn = GameCanvas.loadImage("/hd/btnl.png");
		imgLbtnFocus = GameCanvas.loadImage("/hd/btnlf.png");
		imgArrow = GameCanvas.loadImage("/hd/arrow.png");
		imgArrow2 = GameCanvas.loadImage("/hd/arrow2.png");
		imgChat = GameCanvas.loadImage("/hd/chat.png");
		imgFocus = GameCanvas.loadImage("/hd/focus.png");
		imgFocusActor = GameCanvas.loadImage("/GuiNaruto/imgFocus.png");
		isloadimgfocus = true;
		imgMenu = GameCanvas.loadImage("/hd/menu.png");
		imgTopBar = GameCanvas.loadImage("/hd/topbar.png");
		imgMapBorder = GameCanvas.loadImage("/hd/mapborder.png");
		imgQuest = GameCanvas.loadImage("/imgNpcQuest.png");
		for (int i = 0; i < imgCloudy.length; i++) {
			imgCloudy[i] = GameCanvas.loadImage("/bg/may"+i+".png");;
		}
		wcloudy0 = mGraphics.getImageWidth(imgCloudy[0]);
		ncloudy0 = GameCanvas.w/wcloudy0+1;
		xpaintcloudy0 = new int[ncloudy0*3];
		ypaintcloudy0 = new int[ncloudy0*3];
		kcwcloudy0 = GameCanvas.w%wcloudy0;
		for (int i = 0; i < xpaintcloudy0.length; i++) {
			xpaintcloudy0[i] =(wcloudy0+kcwcloudy0)*i;
			ypaintcloudy0[i] =CRes.random(GameCanvas.h/2,GameCanvas.h);
		}
		wcloudy1 = mGraphics.getImageWidth(imgCloudy[1]);
		ncloudy1 = GameCanvas.w/wcloudy1+2;
		xpaintcloudy1 = new int[ncloudy1*3];
		ypaintcloudy1 = new int[ncloudy1*3];
		for (int i = 0; i < xpaintcloudy1.length; i++) {
			xpaintcloudy1[i] =wcloudy1*i;
			ypaintcloudy1[i] =CRes.random(GameCanvas.h/2,GameCanvas.h);
		}

		wcloudy1 = mGraphics.getImageWidth(imgCloudy[2]);
		ncloudy1 = GameCanvas.w/wcloudy1+2;
		xpaintcloudy2 = new int[ncloudy1*3];
		ypaintcloudy2 = new int[ncloudy1*3];
		for (int i = 0; i < xpaintcloudy2.length; i++) {
			xpaintcloudy2[i] =wcloudy1*i;
			ypaintcloudy2[i] =CRes.random(GameCanvas.h/2,GameCanvas.h);
		}
	}

	public int runArrow = 0;

	void PaintMenu(mGraphics g) 
	{
		mChatPrivate.paint(g);//chat private
		mListFriend.paint(g);//display friend list
	    mBag.paint(g);//display bag
		mParty.paint(g);//display PARTY list
		mChatWorld.paint(g);//paint chatworld
		mGuiQuest.paint(g);
		
		
	}
	public void paint(mGraphics g) {
		

		g.setColor(0xff3aadfe);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		GameCanvas.paintBGGameScr(g);
		g.translate(-cmx, -cmy);
		paintCloudy0(g);
		paintCloudy1(g);
		paintCloudy2(g);
		TileMap.paintMap(g);
		Laroi.paint_Laroi(g);
//		paintBgItem(g, 2);
		for (int i = 0; i < vNpc.size(); i++) // paint NPC
			((Npc) vNpc.elementAt(i)).paint(g);
		for (int i = 0; i < vMob.size(); i++){
			// paint quái
			
			((Mob) vMob.elementAt(i)).paint(g); 
		}
		if(Char.myChar().mobFocus != null){
			paintInfoMod(g);
		}
//		for (int i = 0; i < vNpc.size(); i++) // paint NPC
//			((Npc) vNpc.elementAt(i)).paint(g);

//		g.translate(0, GameCanvas.transY);
		if(Char.myChar().npcFocus!=null)
			g.drawImage(imgFocusActor, Char.myChar().npcFocus.cx, Char.myChar().npcFocus.cy-Char.myChar().npcFocus.ch-20+(GameCanvas.gameTick%20)/4, mGraphics.VCENTER|mGraphics.HCENTER,true);
		
		
		if(Char.myChar().charFocus!=null)
			g.drawImage(imgFocusActor, Char.myChar().charFocus.cx, Char.myChar().charFocus.cy-Char.myChar().charFocus.ch-20+(GameCanvas.gameTick%20)/4, mGraphics.VCENTER|mGraphics.HCENTER,true);
		
		if(Char.myChar().mobFocus!=null)
			g.drawImage(imgFocusActor, Char.myChar().mobFocus.x, Char.myChar().mobFocus.y-Char.myChar().mobFocus.h-35+(GameCanvas.gameTick%20)/4, mGraphics.VCENTER|mGraphics.HCENTER,true);
		if(Char.myChar().itemFocus!=null)
			g.drawImage(imgFocusActor, Char.myChar().itemFocus.x, Char.myChar().itemFocus.y-65+(GameCanvas.gameTick%20)/4, mGraphics.VCENTER|mGraphics.HCENTER,true);
		
		for (int i = 0; i < vCharInMap.size(); i++) { // paint char trong map
			Char c = null;
			try {
				c = (Char) vCharInMap.elementAt(i);
			} catch (Exception e) {
			}
//			if (c == null)
//				continue;
//			if (TileMap.mapID == 111 && i > 19) {
//				c.paintCharName_HP_MP_Overhead(g);
//			} else
				c.paint(g);
		}
		for (int i = 0; i < vCharInMap.size(); i++) {
			Char c = (Char)vCharInMap.elementAt(i);
			if(c!=null&&c.skillPaint!=null&&c.statusMe!=Char.A_DEAD&&c.statusMe!=Char.A_DEADFLY)
				c.paintCharWithSkill(g);
		}
//		for (int i = 0; i < vPtMap.size(); i++) { // paint party
//			Party p = (Party) vPtMap.elementAt(i);
//			if (p.c != null && p.c != Char.myChar())
//				p.c.paintNameInSameParty(g);
//		}
//		paintTeam(g);
		paintFlyText(g);

	//	Char.myChar().paint(g);
		for (int i = 0; i < veffClient.size(); i++) {
			MainEffect eff = (MainEffect)veffClient.elementAt(i);
			eff.paint(g);
		}
//		g.translate(0, -GameCanvas.transY);
		paintWaypointArrow(g);
//		GameCanvas.gI().paintDust(g);
		
	
		
		
		ChatTextField.gI().paint(g);

		for (int i = 0; i < vItemMap.size(); i++){
			// paint item
			((ItemMap) vItemMap.elementAt(i)).paint(g);
		}

		for (int i = 0; i < Effect2.vEffect2.size(); i++) {
			Effect2 l = (Effect2) Effect2.vEffect2.elementAt(i);
			l.paint(g);
		}

		for (int i = 0; i < Effect2.vEffect2Outside.size(); i++) {
			Effect2 l = (Effect2) Effect2.vEffect2Outside.elementAt(i);
			l.paint(g);
		}
		paintBgItem(g, 2);
		paintBgItem(g, 3);
		for (int a = 0; a < TileMap.vCurrItem.size(); a++) {
			BgItem bi = (BgItem) TileMap.vCurrItem.elementAt(a);
			if (bi.idImage != -1)
				if (bi.layer > 3&&GuiMain.isPaintOjectMap)
					bi.paint(g);
		}
		

		super.paint(g);
		if (GameCanvas.isTouch && GameCanvas.isTouchControl) {
//			paintTouchControl(g);
			isPaintSelectSkill = true;
		}
		
		ChatTextField.gI().paint(g);
		
//		paintTeam(g);
		
//		if(isMenushow){
//			paintTeam(g);
//		}
		//paint menu
//		PaintMenu(g);
		
//		if(questMain != null)
//			questMain.Paint(g);
		
		//paint short quest
		if(qShortQuest!=null)
			qShortQuest.Paint(g);
		

		paintCmdBar(g);
		guiMain.Paint(g);

		
		//paintAlert(g);//paint chat friend
		//paintFriend(g);// paint ds ban
//		
		//paint chat clan,world
		//guiChatClanWorld.Paint(g);
//		
//		paintFriend(g);//paint friend
//		paintTeam(g);//paint party
//		//tab inventory,info char
		if(isBag)
		{
			g.setColor(0xff000000,GameCanvas.opacityTab);
			g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
			g.disableBlending();
			GameCanvas.AllInfo.paint(g);
			for (int i = 0; i < veffClient.size(); i++) {
				MainEffect eff = (MainEffect)veffClient.elementAt(i);
				if(eff!=null&&eff.typeEffect==EffectKill.EFF_CUONG_HOA)
				eff.paint(g);
			}
		}
////		paintCmdBar(g);
//		//chat clan,world
		if(guiChatClanWorld!=null)
			if(!MenuIcon.isShowTab)
			guiChatClanWorld.Paint(g);
//		
////		trade
//		if(tradeGui != null)
//			tradeGui.paint(g);
//		
//		//com.sakura.thelastlegend.gui contact
//		guiContact.Paint(g);
		
		//paintTeam(g);
	}


	public static void resetTranslate(mGraphics g) {
		g.translate(-g.getTranslateX(), -g.getTranslateY());
		g.setClip(0, -200, GameCanvas.w, 200 + GameCanvas.h);
	}

	

	static int yTouchBar;
	static int xL, yL; // left
	static int xC, yC;
	static int xR, yR; // right
	static int xF, yF; // fire
	static int xU, yU; // up
	static int xHP, yHP; // HP
	static int xMP, yMP; // MP
	static int xTG, yTG; // target
	static int[] xS;
	static int[] yS;
	static int xSkill, ySkill, padSkill;

	

	// public void paintChat(mGraphics g) {
	// if (GameCanvas.showRunstory) {
	// GameCanvas.paintRunstory(g);
	//
	// }
	// }

	// FLYING TEXT
	public static String[] flyTextString;
	public static int[] flyTextX, flyTextY, flyTextDx, flyTextDy, flyTextState, flyTextColor;

	static {
		flyTextX = new int[5];
		flyTextY = new int[5];
		flyTextDx = new int[5];
		flyTextDy = new int[5];
		flyTextState = new int[5];
		flyTextString = new String[5];
		flyTextColor = new int[8];
		for (int i = 0; i < 5; i++)
			flyTextState[i] = -1;
	}

	public static final void startFlyText(String flyString, int x, int y, int dx, int dy, int color) {
		int n = -1;
		for (int i = 0; i < 5; i++)
			if (flyTextState[i] == -1) {
				n = i;
				break;
			}
		if (n == -1)
			return;
		flyTextColor[n] = color;
		flyTextString[n] = flyString;
		flyTextX[n] = x;
		flyTextY[n] = y;
		flyTextDx[n] = dx;
		flyTextDy[n] = dy;
		flyTextState[n] = 0;
	}
	
	public static final void updateFlyText() {
		for (int i = 0; i < 5; i++)
			if (flyTextState[i] != -1) {
				flyTextState[i] += Res.abs(flyTextDy[i]);
				if (flyTextState[i] > 30)
					flyTextState[i] = -1;
				flyTextX[i] += flyTextDx[i];
				flyTextY[i] += flyTextDy[i];
			}
	}



	public static final void paintFlyText(mGraphics g) {
		for (int i = 0; i < 5; i++)
			if (flyTextState[i] != -1) {
				if (!GameCanvas.isPaint(flyTextX[i], flyTextY[i]))
					continue;
				if (flyTextColor[i] == mFont.RED)
					mFont.number_red_hp.drawStringBorder(g, flyTextString[i], flyTextX[i], flyTextY[i], mFont.CENTER);
				else if (flyTextColor[i] == mFont.YELLOW)
					mFont.number_yellow_xp.drawStringBorder(g, flyTextString[i], flyTextX[i], flyTextY[i], mFont.CENTER);
				else if (flyTextColor[i] == mFont.GREEN)
					mFont.number_green_mp.drawStringBorder(g, flyTextString[i], flyTextX[i], flyTextY[i], mFont.CENTER);
				else if (flyTextColor[i] == mFont.FATAL)
					mFont.tahoma_7b_yellow.drawString(g, flyTextString[i], flyTextX[i], flyTextY[i], mFont.CENTER, mFont.tahoma_7b_blue);
				else if (flyTextColor[i] == mFont.FATAL_ME)
					mFont.tahoma_7b_white.drawString(g, flyTextString[i], flyTextX[i], flyTextY[i], mFont.CENTER, mFont.tahoma_7b_blue);
				else if (flyTextColor[i] == mFont.MISS)
					SmallImage.drawSmallImage(g, 1062, flyTextX[i], flyTextY[i], 0, mGraphics.VCENTER | mGraphics.HCENTER);
				else if (flyTextColor[i] == mFont.ORANGE)
					mFont.number_orange.drawString(g, flyTextString[i], flyTextX[i], flyTextY[i], mFont.CENTER);
				else if (flyTextColor[i] == mFont.ADDMONEY)
					mFont.tahoma_7_yellow.drawString(g, flyTextString[i], flyTextX[i], flyTextY[i], mFont.CENTER, mFont.tahoma_7_red);
				else if (flyTextColor[i] == mFont.MISS_ME)
					SmallImage.drawSmallImage(g, 655, flyTextX[i], flyTextY[i], 0, mGraphics.VCENTER | mGraphics.HCENTER);
			}
	}

	// LANTERNS



	// SPLASH
	public static int[] splashX, splashY, splashState, splashF, splashDir;
	public static mBitmap imgSplash[];

	public static final void loadSplash() {
		if (imgSplash == null) {
			imgSplash = new mBitmap[3];
			for (int i = 0; i < 3; i++)
				imgSplash[i] = GameCanvas.loadImage("/e/sp" + i + ".png");
		}
		splashX = new int[2];
		splashY = new int[2];
		splashState = new int[2];
		splashF = new int[2];
		splashDir = new int[2];
		splashState[0] = splashState[1] = -1;
	}

	public static final boolean startSplash(int x, int y, int dir) {
		int i = splashState[0] == -1 ? 0 : 1;
		if (splashState[i] != -1)
			return false;
		splashState[i] = 0;
		splashDir[i] = dir;
		splashX[i] = x;
		splashY[i] = y;
		return true;
	}

	public static final void updateSplash() {
		for (int i = 0; i < 2; i++) {
			if (splashState[i] != -1) {
				splashState[i]++;
				splashX[i] += (splashDir[i] << 2);
				splashY[i] -= 1;
				if (splashState[i] >= 6)
					splashState[i] = -1;
				else
					splashF[i] = (splashState[i] >> 1) % 3;
			}
		}
	}

	public static final void paintSplash(mGraphics g) {
		for (int i = 0; i < 2; i++) {
			if (splashState[i] != -1) {
				if (splashDir[i] == 1)
					g.drawImage(imgSplash[splashF[i]], splashX[i], splashY[i], 3);
				else
					g.drawRegion(imgSplash[splashF[i]], 0, 0, mGraphics.getImageWidth(imgSplash[splashF[i]]), mGraphics
							.getImageHeight(imgSplash[splashF[i]]), 2, splashX[i], splashY[i], 3);
			}
		}
	}

	public static int cmdBarX, cmdBarY, cmdBarW, cmdBarLeftW, cmdBarRightW, cmdBarCenterW, hpBarX, hpBarY, hpBarW, mpBarW, expBarW, lvPosX,
			moneyPosX, hpBarH, girlHPBarY;
	public static mBitmap[] imgCmdBar;


	

	public static void paintCmdBar(mGraphics g) {
	
		g.setClip(0, cmdBarY - 4, GameCanvas.w, 100);

		// // PAINT HP
		int hpWidth = Char.myChar().cHP * hpBarW / Char.myChar().cMaxHP;
		if (hpWidth > hpBarW)
			hpWidth = 0;
		g.setColor(0x770000);
		g.fillRect(hpBarX + 4, hpBarY - 2, hpWidth, 10);

		g.setColor(0xCC0000);
		g.fillRect(hpBarX + 4, hpBarY - 2 + 1, hpWidth, 10);
		// paint MP
		hpWidth = Char.myChar().cMP * hpBarW / Char.myChar().cMaxMP;
		if (hpWidth > hpBarW)
			hpWidth = 0;
		g.setColor(0x001188);
		g.fillRect(hpBarX + 4, hpBarY - 12, hpWidth, 10);

//		g.setColor(0x0011DD);
//		g.fillRect(hpBarX + 4, hpBarY + 14, hpWidth, hpBarH - 2);
		// paint MP
		// exp
		

//		g.setColor(0x90653b);
//		g.fillRect(0, cmdBarY + 35, GameCanvas.w, 1);
	}

	public static int popupY, popupX, isborderIndex, isselectedRow;
	static mBitmap imgNolearn;

	

	int indexMember = 0;
	String[] arrClanInfo = null, arrClanDongGop = null;

	

	private void paintNumCount(mGraphics g) {
		resetTranslate(g);
		int curRow = indexRow;
		if (isPaintAuctionBuy)
			curRow = indexSelect;
		if (curRow >= 0 && indexRowMax > 0) {
			int row = ((curRow + 1) < indexRowMax) ? (curRow + 1) : indexRowMax;
			
			mFont.tahoma_7b_white.drawString(g, row + "/" + indexRowMax, popx + popw / 2, popy + poph - 20, 2);
		}
	}

	public void updateSS() {
		if (indexMenu == -1)
			return;
		if (cmySK != cmtoYSK) {
			cmvySK = (cmtoYSK - cmySK) << 2;
			cmdySK += cmvySK;
			cmySK += cmdySK >> 4;
			cmdySK = cmdySK & 0xf;
		}
		if (Math.abs(cmtoYSK - cmySK) < 15 && cmySK < 0) {
			cmtoYSK = 0;

		}
		if (Math.abs(cmtoYSK - cmySK) < 15 && cmySK > cmyLimSK) {
			cmtoYSK = cmyLimSK;

		}

	}

	public int cmxp, cmvxp, cmdxp, cmxLimp, cmyLimp, cmyp, cmvyp, cmdyp;
	int indexTiemNang = 0;
	private Command cmdTradeLock;
	private Command cmdTradeAccept;
	protected Command cmdUpgradeMoveOut;
	protected Command cmdConvertMoveOut;
	protected Command cmdSplitMoveOut;
	private Command cmdTradeSendMoney;
	private Command cmdTradeMoveOut;
	private Command cmdTradeViewItemInfo;
	private Command cmdTradeSelectItem;
	private Command cmdTradeSelectInBag;
	private Command cmdTradeSelectInList;
	public Command cmdCloseAll;
	private Command cmdEliteShopBuy;
	private Command cmdEliteShopView;
	private Command cmdStoreBuy;
	private Command cmdStoreView;
	private Command cmdClanStoreBuy;
	private Command cmdClanStoreView;
	private Command cmdStoreLockBuy;
	private Command cmdStoreLockView;
	private Command cmdStoreFashionBuy;
	private Command cmdStoreFashionView;
	private Command cmdNonNamBuy;
	private Command cmdNonNamView;
	private Command cmdNonNuBuy;
	private Command cmdNonNuView;
	private Command cmdAoNamBuy;
	private Command cmdAoNamView;
	private Command cmdAoNuBuy;
	private Command cmdAoNuView;
	private Command cmdGangTayNamBuy;
	private Command cmdGangTayNamView;
	private Command cmdGangTayNuBuy;
	private Command cmdGangTayNuView;

	// ===============================

	
	
	public static boolean trans = false;

	String alertURL, fnick;

	

	private void setCharFocusCommand() {
		if (indexRow >= 0 && vCharInMap.size() > 0) {
			if (Char.getIndexChar(cLastFocusID) == indexRow) {
				left = new Command(mResources.UNSELECT, 14002);
			} else {
				left = new Command(mResources.SELECT, 14003);
				center = new Command("", 14003);
			}
		} else
			left = center = null;
	}

	private void setDunListCommand() {

		if (vList.size() > 0 && indexRow >= 0 && indexRowMax > 0) {
			center = new Command(mResources.SELECT, 14021);
		} else
			center = null;

	}

	private void setEnemiesCommand() {

		if (vEnemies.size() > 0 && indexRow >= 0 && indexRowMax > 0) {
			center = new Command(mResources.SELECT, 11078);
		} else
			center = null;

	}

	

	
	public boolean isPaintPopup() {
		if (isPaintLuckySpin || isPaintItemInfo || isPaintInfoMe || isPaintStore || isPaintEliteShop || isPaintAuctionBuy || isPaintWeapon
				|| isPaintNonNam || isPaintNonNu || isPaintAoNam || isPaintAoNu || isPaintGangTayNam || isPaintGangTayNu || isPaintQuanNam
				|| isPaintQuanNu || isPaintGiayNam || isPaintGiayNu || isPaintLien || isPaintNhan || isPaintNgocBoi || isPaintPhu || isPaintStack
				|| isPaintStackLock || isPaintGrocery || isPaintGroceryLock || isPaintUpGrade || isPaintAuctionSale || isPaintConvert || isPaintSplit || isPaintTinhluyen || isPaintDichChuyen
				|| isPaintUpPearl || isPaintLuyenThach || isPaintBox || isPaintTrade || ispaintChat || isPaintZone || isPaintAuto ||isPaintTeam || isPaintClan || isPaintFindTeam
				|| isPaintTask || isPaintFriend || isPaintList || isPaintEnemies || isPaintCharInMap || isPaintMessage)
			return true;
		return false;
	}

	

	public boolean isPaintUI() {
		if (isPaintAuto || isPaintStore || isPaintLuckySpin || isPaintEliteShop || isPaintWeapon || isPaintNonNam || isPaintNonNu || isPaintAoNam || isPaintAoNu
				|| isPaintGangTayNam || isPaintGangTayNu || isPaintQuanNam || isPaintQuanNu || isPaintGiayNam || isPaintGiayNu || isPaintLien
				|| isPaintNhan || isPaintNgocBoi || isPaintPhu || isPaintStack || isPaintStackLock || isPaintGrocery || isPaintGroceryLock
				|| isPaintUpGrade || isPaintAuctionBuy || isPaintAuctionSale || isPaintConvert || isPaintSplit || isPaintTinhluyen || isPaintDichChuyen || isPaintUpPearl || isPaintLuyenThach || isPaintBox
				|| isPaintTrade)
			return true;
		return false;
	}

	public boolean isOpenUI() {
		if (isPaintAuto || isPaintItemInfo || isPaintLuckySpin || isPaintInfoMe || isPaintStore || isPaintEliteShop || isPaintWeapon || isPaintNonNam
				|| isPaintNonNu || isPaintAoNam || isPaintAoNu || isPaintGangTayNam || isPaintGangTayNu || isPaintQuanNam || isPaintQuanNu
				|| isPaintGiayNam || isPaintGiayNu || isPaintLien || isPaintNhan || isPaintNgocBoi || isPaintPhu || isPaintStack || isPaintStackLock
				|| isPaintGrocery || isPaintGroceryLock || isPaintUpGrade || isPaintAuctionBuy || isPaintAuctionSale || isPaintConvert
				|| isPaintSplit || isPaintTinhluyen || isPaintDichChuyen|| isPaintUpPearl || isPaintLuyenThach || isPaintBox || isPaintTrade) {
			return true;
		}
		return false;
	}

	public static boolean isSmallUI() {
		if ((isPaintInfoMe && (indexMenu > 0 && indexMenu<5)) || isPaintAuto || isPaintZone || (isPaintClan && (indexMenu == 0 || indexMenu == 1 || indexMenu == 3))
				|| isPaintCharInMap || isPaintTeam || isPaintFindTeam || isPaintFriend || isPaintList || isPaintEnemies || isPaintTask
				|| isPaintMessage || isPaintAlert)
			return true;
		return false;
	}

	public void actionBuy(final Item item) {
		Command ok = new Command(mResources.ACCEPT, 11055, item);
		GameCanvas.inputDlg.show(mResources.INPUT_QUANTITY, ok, TField.INPUT_TYPE_NUMERIC);
	}

	public void actionSale(final Item item) {
		if (item.upgrade > 0 && item.isTypeBody()) {
			GameCanvas.msgdlg.setInfo(mResources.NOT_SALE_UPGRADE, null, new Command(mResources.CLOSE, 110561), null);
			GameCanvas.msgdlg.show();
			return;
		}
		GameCanvas.inputDlg.tfInput.getText();
		if (item.quantity > 1) {
			Command ok = new Command(mResources.ACCEPT, 110562, item);
			GameCanvas.inputDlg.show(mResources.INPUT_QUANTITY, ok, TField.INPUT_TYPE_NUMERIC);
		} else {
			GameCanvas.startYesNoDlg(mResources.CONFIRMSALEITEM, new Command(mResources.YES, 11061, item), new Command(mResources.NO, 0001));
		}
	}

	public void actionCoinOut() {
		Command ok = new Command(mResources.ACCEPT, 11042);
		GameCanvas.inputDlg.show(mResources.INPUT_COIN, ok, TField.INPUT_TYPE_NUMERIC);
	}

	public void actionCoinTrade() {
		Command ok = new Command(mResources.ACCEPT, 110361);
		GameCanvas.inputDlg.show(mResources.INPUT_COIN, ok, TField.INPUT_TYPE_NUMERIC);
	}

	public void actionCoinIn() {
		Command ok = new Command(mResources.ACCEPT, 11043);
		GameCanvas.inputDlg.show(mResources.INPUT_COIN, ok, TField.INPUT_TYPE_NUMERIC);
	}


	private int getMaxIndexForCurrentStore() {
		int maxIndex = 0;
		try {
			if (isPaintEliteShop) {
				if (arrItemElites.length % columns == 0)
					maxIndex = arrItemElites.length;
				else
					maxIndex = (arrItemElites.length / columns + 1) * columns;
			} else if (isPaintStore) {
				if (indexMenu == 0) {
					if (arrItemStore.length % columns == 0)
						maxIndex = arrItemStore.length;
					else
						maxIndex = (arrItemStore.length / columns + 1) * columns;
				} else if (indexMenu == 1) {
					if (arrItemBook.length % columns == 0)
						maxIndex = arrItemBook.length;
					else
						maxIndex = (arrItemBook.length / columns + 1) * columns;
				} else if (indexMenu == 2) {
					if (arrItemFashion.length % columns == 0)
						maxIndex = arrItemFashion.length;
					else
						maxIndex = (arrItemFashion.length / columns + 1) * columns;
				}
			} else if (isPaintNonNam) {
				if (arrItemNonNam.length % columns == 0)
					maxIndex = arrItemNonNam.length;
				else
					maxIndex = (arrItemNonNam.length / columns + 1) * columns;
			} else if (isPaintNonNu) {
				if (arrItemNonNu.length % columns == 0)
					maxIndex = arrItemNonNu.length;
				else
					maxIndex = (arrItemNonNu.length / columns + 1) * columns;
			} else if (isPaintAoNam) {
				if (arrItemAoNam.length % columns == 0)
					maxIndex = arrItemAoNam.length;
				else
					maxIndex = (arrItemAoNam.length / columns + 1) * columns;
			} else if (isPaintAoNu) {
				if (arrItemAoNu.length % columns == 0)
					maxIndex = arrItemAoNu.length;
				else
					maxIndex = (arrItemAoNu.length / columns + 1) * columns;
			} else if (isPaintGangTayNam) {
				if (arrItemGangTayNam.length % columns == 0)
					maxIndex = arrItemGangTayNam.length;
				else
					maxIndex = (arrItemGangTayNam.length / columns + 1) * columns;
			} else if (isPaintGangTayNu) {
				if (arrItemGangTayNu.length % columns == 0)
					maxIndex = arrItemGangTayNu.length;
				else
					maxIndex = (arrItemGangTayNu.length / columns + 1) * columns;
			} else if (isPaintQuanNam) {
				if (arrItemQuanNam.length % columns == 0)
					maxIndex = arrItemQuanNam.length;
				else
					maxIndex = (arrItemQuanNam.length / columns + 1) * columns;
			} else if (isPaintQuanNu) {
				if (arrItemQuanNu.length % columns == 0)
					maxIndex = arrItemQuanNu.length;
				else
					maxIndex = (arrItemQuanNu.length / columns + 1) * columns;
			} else if (isPaintGiayNam) {
				if (arrItemGiayNam.length % columns == 0)
					maxIndex = arrItemGiayNam.length;
				else
					maxIndex = (arrItemGiayNam.length / columns + 1) * columns;
			} else if (isPaintGiayNu) {
				if (arrItemGiayNu.length % columns == 0)
					maxIndex = arrItemGiayNu.length;
				else
					maxIndex = (arrItemGiayNu.length / columns + 1) * columns;
			} else if (isPaintLien) {
				if (arrItemLien.length % columns == 0)
					maxIndex = arrItemLien.length;
				else
					maxIndex = (arrItemLien.length / columns + 1) * columns;
			} else if (isPaintNhan) {
				if (arrItemNhan.length % columns == 0)
					maxIndex = arrItemNhan.length;
				else
					maxIndex = (arrItemNhan.length / columns + 1) * columns;
			} else if (isPaintNgocBoi) {
				if (arrItemNgocBoi.length % columns == 0)
					maxIndex = arrItemNgocBoi.length;
				else
					maxIndex = (arrItemNgocBoi.length / columns + 1) * columns;
			} else if (isPaintPhu) {
				if (arrItemPhu.length % columns == 0)
					maxIndex = arrItemPhu.length;
				else
					maxIndex = (arrItemPhu.length / columns + 1) * columns;
			} else if (isPaintWeapon) {
				if (arrItemWeapon.length % columns == 0)
					maxIndex = arrItemWeapon.length;
				else
					maxIndex = (arrItemWeapon.length / columns + 1) * columns;
			} else if (isPaintStack) {
				if (arrItemStack.length % columns == 0)
					maxIndex = arrItemStack.length;
				else
					maxIndex = (arrItemStack.length / columns + 1) * columns;
			} else if (isPaintStackLock) {
				if (arrItemStackLock.length % columns == 0)
					maxIndex = arrItemStackLock.length;
				else
					maxIndex = (arrItemStackLock.length / columns + 1) * columns;
			} else if (isPaintGrocery) {
				if (arrItemGrocery.length % columns == 0)
					maxIndex = arrItemGrocery.length;
				else
					maxIndex = (arrItemGrocery.length / columns + 1) * columns;
			} else if (isPaintGroceryLock) {
				if (arrItemGroceryLock.length % columns == 0)
					maxIndex = arrItemGroceryLock.length;
				else
					maxIndex = (arrItemGroceryLock.length / columns + 1) * columns;
			}
			if (isPaintBox)
				maxIndex = Char.myChar().arrItemBox.length;
			if (indexMenu == 1 && !isPaintStore)
				maxIndex = Char.myChar().arrItemBag.length;
		} catch (Exception e) {
			//e.printStackTrace();
		}

		if ((isPaintUpPearl || isPaintLuyenThach || isPaintSplit || isPaintTinhluyen|| isPaintDichChuyen) && indexMenu == 0)
			maxIndex = 24;
		else if ((isPaintUpGrade || isPaintConvert) && indexMenu == 0)
			maxIndex = 18;
		else if (maxIndex < 30)
			maxIndex = 30;

		return maxIndex;
	}

	

	

		// --------updateselectlist
// else if (isPaintAuctionBuy) {
// ScrollResult r = scrMain.updateKey();
// if (r.isDowning || r.isFinish) {
// indexSelect = r.selected;
// if (indexSelect >= arrItemStands.length)
// indexSelect = -1;
// if (arrItemStands.length > 0)
// indexTitle = 1;
// updateCommandForUI();
// }
// }



	private boolean isCanLuyenThach(){
		int count = 0;
		int count1 = 0;
		int count2 = 0;
		int levelCrysal = 0;
		for (int i = 0; i < arrItemUpPeal.length; i++) {
			Item item = arrItemUpPeal[i];
			if (item != null) {
				if (item.template.id == 455) // Tử tinh thạch sơ cấp
					count++;
				else if (item.template.id == 456) // Tử tinh thạch trung cấp
					count1++;
//				else if (item.template.type == Item.TYPE_CRYSTAL){
//					count2++;
//					levelCrysal = item.template.id;
//				}
			}
		}
		if (count >= 9 || count1 >= 9 || (levelCrysal >= 10 && count >= 3 && count2 ==1) || (levelCrysal >= 11 && count1 >= 3 && count2 ==1)) {
			return true;
		}
		return false;
	}
	


	private void doUPPOINT() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.UPPOINT, 11064));
		menu.addElement(new Command(mResources.UPPOINTS, 11065));
		GameCanvas.menu.startAt(menu, 0);
	}

	public void saleItem() {
		if (Char.myChar().arrItemBag[indexSelect].quantity > 1) {
			left = new Command(mResources.SALE, 11072);
		} else {
			left = new Command(mResources.SALE, 11073);
		}
	}

	public static int widthGui=237,heightGui=232,xGui,yGui;

	public static int xstart, ystart, popupW = 140, popupH = 160, cmySK, cmtoYSK, cmdySK, cmvySK, cmyLimSK;
	public static int columns = 6, rows;

	public static void setPopupSize(int w, int h) {
		if (GameCanvas.w == 128 || GameCanvas.h <= 208) {
			w = 126;
			h = 160;
		}

		popupW = w;
		popupH = h;
		popupX = gW2 - w / 2;
		popupY = gH2 - h / 2;

		if (GameCanvas.h <= 250)
			popupY -= 10;
		if (GameCanvas.isTouchControlLargeScreen && !isSmallUI() && GameCanvas.currentScreen instanceof GameScreen) {
			popupW = 310;
			popupX = gW / 2 - popupW / 2;
		}
		if (popupY < -10)
			popupY = -10;
		if (GameCanvas.h > 208)
			if (popupY < 0)
				popupY = 0;
		if (GameCanvas.h == 208) {
			if (popupY < 10)
				popupY = 10;
		}

	}

	int totalRowInfo;

	int ypaintKill = 0, ylimUp = 0, ylimDow;

	public void paintSkillInfo(mGraphics g, Skill skill) {
		if (Char.myChar().clevel >= skill.level)
			mFont.tahoma_7_white.drawString(g, mResources.LEVELNEED + " " + skill.level, xstart + 5, yPaint += 12, 0);
		else
			mFont.tahoma_7_red.drawString(g, mResources.LEVELNEED + " " + skill.level, xstart + 5, yPaint += 12, 0);

		if (skill.template.type != Skill.SKILL_AUTO_USE) {
			indexRowMax += 4;
			mFont.tahoma_7_white.drawString(g, mResources.MAX_FIGHT + ": " + skill.maxFight, xstart + 5, yPaint += 12, 0);
			mFont.tahoma_7_white.drawString(g, mResources.MANA_USE + ": " + skill.manaUse, xstart + 5, yPaint += 12, 0);
			mFont.tahoma_7_white.drawString(g, mResources.RANGE_ATTACK + ": " + skill.dx, xstart + 5, yPaint += 12, 0);
			mFont.tahoma_7_white.drawString(g, mResources.TIME_LOOP + ": " + skill.strTimeReplay() + " " + mResources.SECOND, xstart + 5,
					yPaint += 12, 0);
		}
		indexRowMax++;
	}

	

	int yPaint = 0;

	private void paintSelectHighlight(int x, int y, mGraphics g) {
		g.drawImage(imgSelect, x - 5, y - 5, 0);

	}

	int[] color = new int[] { 0, 0, 0, 0, 0x092B09, 0x092B09, 0x0A300A, 0x0A300A, 0x331200, 0x331100, 0x401500, 0x4D1A00, 0x320033, 0x3C0033,
			0x460033, 0x4D0008, 0x66000A, 0x334400 };

	int[][] colorBorder = new int[][] { //
	{ 0x0048FF, 0x0041E5, 0x003ACC, 0x0033B3, 0x002B99, 0x002480 }, //
			{ 0x00B300, 0x009900, 0x008000, 0x006600, 0x004D00, 0x003300 }, //
			{ 0xFF7F00, 0xE57300, 0xCC6600, 0xB35900, 0x994C00, 0x804000 }, // 
			{ 0xCE00FF, 0xB800E5, 0xA300CC, 0x8F00B3, 0x7A0099, 0x660080 }, //
			{ 0xFF0019, 0xE50017, 0xCC0014, 0xB30012, 0x99000F, 0x80000D }, //
	};
	int size[] = { 2, 1, 1, 1, 1, 1 };
	private void paintFineEffect(int x, int y, int fine, mGraphics g){ // paint effect độ tinh luyện	
//		int indexSize = GameScreen.indexSize - 2;
//		int start = 0;
//		int cl = fine < 4 ? 0 : fine < 8 ? 1 : fine < 12 ? 2 : fine <= 14 ? 3 : 4;
//		for (int i = start; i < size.length; i++) {
//			int xPos = x - (indexSize / 2) + upgradeEffectX(GameCanvas.gameTick * 1 - i * 4);
//			int yPos = y - (indexSize / 2) + upgradeEffectY(GameCanvas.gameTick * 1 - i * 4);
//			g.setColor(colorBorder[cl][i]);
//			g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
//		}
//		if (fine == 4 || fine == 8)
//			for (int i = start; i < size.length; i++) {
//				int xPos = x - (indexSize / 2) + upgradeEffectX((GameCanvas.gameTick - indexSize * 2) * 1 - i * 4);
//				int yPos = y - (indexSize / 2) + upgradeEffectY((GameCanvas.gameTick - indexSize * 2) * 1 - i * 4);
//				g.setColor(colorBorder[cl - 1][i]);
//				g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
//			}
//		if (fine != 1 && fine != 4 && fine != 8)
//			for (int i = start; i < size.length; i++) {
//				int xPos = x - (indexSize / 2) + upgradeEffectX((GameCanvas.gameTick - indexSize * 2) * 1 - i * 4);
//				int yPos = y - (indexSize / 2) + upgradeEffectY((GameCanvas.gameTick - indexSize * 2) * 1 - i * 4);
//				g.setColor(colorBorder[cl][i]);
//				g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
//			}
//		if (fine != 1 && fine != 4 && fine != 8 && fine != 12 && fine != 2 && fine != 5 && fine != 9)
//			for (int i = start; i < size.length; i++) {
//				int xPos = x - (indexSize / 2) + upgradeEffectX((GameCanvas.gameTick - indexSize) * 1 - i * 4);
//				int yPos = y - (indexSize / 2) + upgradeEffectY((GameCanvas.gameTick - indexSize) * 1 - i * 4);
//				g.setColor(colorBorder[cl][i]);
//				g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
//			}
//		if (fine != 1 && fine != 4 && fine != 8 && fine != 12 && fine != 2 && fine != 5 && fine != 9 && fine != 13
//				&& fine != 3 && fine != 6 && fine != 10 && fine != 15)
//			for (int i = start; i < size.length; i++) {
//				int xPos = x - (indexSize / 2) + upgradeEffectX((GameCanvas.gameTick - indexSize * 3) * 1 - i * 4);
//				int yPos = y - (indexSize / 2) + upgradeEffectY((GameCanvas.gameTick - indexSize * 3) * 1 - i * 4);
//				g.setColor(colorBorder[cl][i]);
//				g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
//			}
		int indexSize = GameScreen.indexSize - 2;
		int start = 0;
		int cl = fine < 4 ? 0 : fine < 8 ? 1 : fine < 12 ? 2 : fine <= 14 ? 3 : 4;
		//for (int i = start; i < size.length; i++) {
			int xPos = x - (indexSize / 2) + fineEffectX(GameCanvas.gameTick * 1 * 4);
			int yPos = y - (indexSize / 2) + fineEffectY(GameCanvas.gameTick * 1 * 4);
			int xPos1 = x  ;
			int yPos1 = y ;
			
			
			int	xPos2 = x - (indexSize / 2) + FineEffectX1(GameCanvas.gameTick * 1 * 4);
			int	yPos2 = y - (indexSize / 2) + FineEffectY1(GameCanvas.gameTick * 1 * 4);
		
			g.setColor(0xFFFFFF);
			g.drawLine(xPos, yPos, xPos2, yPos2);
			g.drawLine(xPos2, yPos2, xPos1, yPos1);
			
			
		//}
		
		
	}
	
	private int fineEffectY(int tick) {
		int indexSize = GameScreen.indexSize - 2;
		int k = tick % (4 * indexSize);
		if (0 <= k && k < indexSize)
			return 0;
		else if (indexSize <= k && k < indexSize * 2)
			return k % indexSize;
		else if (indexSize * 2 <= k && k < indexSize * 3)
			return indexSize;
		else
			return indexSize - (k % indexSize);
	}
	
	private int fineEffectX(int tick) {
		int indexSize = GameScreen.indexSize - 2;
		int k = tick % (4 * indexSize);
		if (0 <= k && k < indexSize)
			return k % indexSize;
		else if (indexSize <= k && k < indexSize * 2)
			return indexSize;
		else if (indexSize * 2 <= k && k < indexSize * 3)
			return indexSize - (k % indexSize);
		else
			return 0;
	}
	
	private int FineEffectX1(int tick){ 
		int indexSize = GameScreen.indexSize - 2;
		int k = tick % (4 * indexSize);
		if (0 <= k && k < indexSize)
			return k % indexSize + 6 ;
		else if (indexSize <= k && k < indexSize * 2)
			return indexSize - 6;
		else if (indexSize * 2 <= k && k < indexSize * 3)
			return indexSize - (k % indexSize) - 6;
		else
			return 0 + 6 ;
	}
	
	private int FineEffectY1(int tick){
		int indexSize = GameScreen.indexSize - 2;
		int k = tick % (4 * indexSize);
		if (0 <= k && k < indexSize)
			return 0 + 6;
		else if (indexSize <= k && k < indexSize * 2)
			return k % indexSize - 6;
		else if (indexSize * 2 <= k && k < indexSize * 3)
			return indexSize - 6;
		else
			return indexSize - (k % indexSize) + 6;
		
	}

	private void paintUpgradeEffect(int x, int y, int upgrade, mGraphics g) { // paint hiệu ứng nâng cấp
		//System.out.println("PAINT EFFECT ITEM UP");
		int indexSize = GameScreen.indexSize - 2;
		int start = 0;
		int cl = upgrade < 4 ? 0 : upgrade < 8 ? 1 : upgrade < 12 ? 2 : upgrade <= 14 ? 3 : 4;
		for (int i = start; i < size.length; i++) {
			int xPos = x - (indexSize / 2) + upgradeEffectX(GameCanvas.gameTick * 1 - i * 4);
			int yPos = y - (indexSize / 2) + upgradeEffectY(GameCanvas.gameTick * 1 - i * 4);
			g.setColor(colorBorder[cl][i]);
			g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
		}
		if (upgrade == 4 || upgrade == 8)
			for (int i = start; i < size.length; i++) {
				int xPos = x - (indexSize / 2) + upgradeEffectX((GameCanvas.gameTick - indexSize * 2) * 1 - i * 4);
				int yPos = y - (indexSize / 2) + upgradeEffectY((GameCanvas.gameTick - indexSize * 2) * 1 - i * 4);
				g.setColor(colorBorder[cl - 1][i]);
				g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
			}
		if (upgrade != 1 && upgrade != 4 && upgrade != 8)
			for (int i = start; i < size.length; i++) {
				int xPos = x - (indexSize / 2) + upgradeEffectX((GameCanvas.gameTick - indexSize * 2) * 1 - i * 4);
				int yPos = y - (indexSize / 2) + upgradeEffectY((GameCanvas.gameTick - indexSize * 2) * 1 - i * 4);
				g.setColor(colorBorder[cl][i]);
				g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
			}
		if (upgrade != 1 && upgrade != 4 && upgrade != 8 && upgrade != 12 && upgrade != 2 && upgrade != 5 && upgrade != 9)
			for (int i = start; i < size.length; i++) {
				int xPos = x - (indexSize / 2) + upgradeEffectX((GameCanvas.gameTick - indexSize) * 1 - i * 4);
				int yPos = y - (indexSize / 2) + upgradeEffectY((GameCanvas.gameTick - indexSize) * 1 - i * 4);
				g.setColor(colorBorder[cl][i]);
				g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
			}
		if (upgrade != 1 && upgrade != 4 && upgrade != 8 && upgrade != 12 && upgrade != 2 && upgrade != 5 && upgrade != 9 && upgrade != 13
				&& upgrade != 3 && upgrade != 6 && upgrade != 10 && upgrade != 15)
			for (int i = start; i < size.length; i++) {
				int xPos = x - (indexSize / 2) + upgradeEffectX((GameCanvas.gameTick - indexSize * 3) * 1 - i * 4);
				int yPos = y - (indexSize / 2) + upgradeEffectY((GameCanvas.gameTick - indexSize * 3) * 1 - i * 4);
				g.setColor(colorBorder[cl][i]);
				g.fillRect(xPos - size[i] / 2, yPos - size[i] / 2, size[i], size[i]);
			}
	}

	private int upgradeEffectY(int tick) {
		int indexSize = GameScreen.indexSize - 2;
		int k = tick % (4 * indexSize);
		if (0 <= k && k < indexSize)
			return 0;
		else if (indexSize <= k && k < indexSize * 2)
			return k % indexSize;
		else if (indexSize * 2 <= k && k < indexSize * 3)
			return indexSize;
		else
			return indexSize - (k % indexSize);
	}
	
	private int upgradeEffectX(int tick) {
		int indexSize = GameScreen.indexSize - 2;
		int k = tick % (4 * indexSize);
		if (0 <= k && k < indexSize)
			return k % indexSize;
		else if (indexSize <= k && k < indexSize * 2)
			return indexSize;
		else if (indexSize * 2 <= k && k < indexSize * 3)
			return indexSize - (k % indexSize);
		else
			return 0;
	}
	




	
	public static int indexEff = 0;
	

	

	public void paintConvert(mGraphics g, String[] titles) {
		try {
			rows = 3;
			GameCanvas.paint.paintFrame(popupX, popupY, popupW, popupH, g);
			paintTitle(g, titles[indexMenu], titles.length > 1);
			xstart = popupX + 3;
			ystart = popupY + 34 + indexSize;
			int xItemUpgrade = popupX + 45;
			int xItemUpgradeTo = popupX + 100;
			int yItemUpgrade = ystart - indexSize - 3;
			if (arrItemConvert[0] != null) {
				paintItem(g, arrItemConvert[0], xItemUpgrade, yItemUpgrade);
				g.setColor(0xBB6611);
				g.drawRect(xItemUpgrade, yItemUpgrade, indexSize, indexSize);
				mFont.tahoma_7_yellow.drawString(g, "(+" + arrItemConvert[0].upgrade + ")", xItemUpgrade - 5, yItemUpgrade + indexSize / 2 - 5,
						mFont.RIGHT);
			} else {
				g.setColor(0x001919);
				g.fillRect(xItemUpgrade - 1, yItemUpgrade - 1, indexSize + 3, indexSize + 3);
				SmallImage.drawSmallImage(g, 154, xItemUpgrade + indexSize / 2, yItemUpgrade + indexSize / 2, 0, mGraphics.VCENTER
						| mGraphics.HCENTER);
				g.setColor(0xBB6611);
				g.drawRect(xItemUpgrade, yItemUpgrade, indexSize, indexSize);
			}
			SmallImage.drawSmallImage(g, 942, xItemUpgrade + 43, ystart - 15, 0, StaticObj.VCENTER_HCENTER);
			if (arrItemConvert[1] != null) {
				Item itemTemp = arrItemConvert[1].clone();
				if (arrItemConvert[0] != null && arrItemConvert[0].template.type == itemTemp.template.type
						&& arrItemConvert[1].template.level >= arrItemConvert[0].template.level)
					itemTemp.upgrade = arrItemConvert[0].upgrade;
				paintItem(g, itemTemp, xItemUpgradeTo, yItemUpgrade);
				g.setColor(0xBB6611);
				g.drawRect(xItemUpgradeTo, yItemUpgrade, indexSize, indexSize);
				mFont.tahoma_7_yellow.drawString(g, "(+" + (itemTemp.upgrade) + ")", xItemUpgradeTo + indexSize + 10, yItemUpgrade + indexSize / 2
						- 5, mFont.LEFT);
			} else {
				g.setColor(0x001919);
				g.fillRect(xItemUpgradeTo - 1, yItemUpgrade - 1, indexSize + 3, indexSize + 3);
				SmallImage.drawSmallImage(g, 154, xItemUpgradeTo + indexSize / 2, yItemUpgrade + indexSize / 2, 0, mGraphics.VCENTER
						| mGraphics.HCENTER);
				g.setColor(0xBB6611);
				g.drawRect(xItemUpgradeTo, yItemUpgrade, indexSize, indexSize);
			}
			if (indexTitle == 1) {
				if (indexSelect == 0) {
					g.setColor(0xffffff);
					g.drawRect(xItemUpgrade, yItemUpgrade, indexSize, indexSize);
				}
				if (indexSelect == 1) {
					g.setColor(0xffffff);
					g.drawRect(xItemUpgradeTo, yItemUpgrade, indexSize, indexSize);
				}
			}

			g.setColor(0);
			g.fillRect(xstart - 1, ystart - 1, columns * indexSize + 3, rows * indexSize + 3);

			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < columns; j++) {
					SmallImage.drawSmallImage(g, 154, xstart + (j * indexSize) + indexSize / 2, ystart + (i * indexSize) + indexSize / 2, 0,
							mGraphics.VCENTER | mGraphics.HCENTER);
					g.setColor(0xBB6611);
					g.drawRect(xstart + (j * indexSize), ystart + (i * indexSize), indexSize, indexSize);
				}
			}
			Item item = arrItemConvert[2];
			if (item != null) {
				int r = 0 / columns;
				int c = 0 - (r * columns);
				if (!item.isLock) {
					g.setColor(0x002f33);
					g.fillRect(xstart + (c * indexSize) + 1, ystart + (r * indexSize) + 1, indexSize - 1, indexSize - 1);

				}
				SmallImage.drawSmallImage(g, item.template.iconID, xstart + (c * indexSize) + indexSize / 2,
						ystart + (r * indexSize) + indexSize / 2, 0, mGraphics.VCENTER | mGraphics.HCENTER);
			}
			mFont.tahoma_7_white.drawString(g, "- " + mResources.UPGRADE_CONDITION[0], xstart, ystart + (rows) * indexSize + 10, mFont.LEFT);
			mFont.tahoma_7_white.drawString(g, "  " + mResources.UPGRADE_CONDITION[1], xstart, ystart + (rows) * indexSize + 22, mFont.LEFT);
			mFont.tahoma_7_white.drawString(g, "- " + mResources.UPGRADE_CONDITION[2], xstart, ystart + (rows) * indexSize + 34, mFont.LEFT);

			if (indexTitle == 2) {
				int r = indexSelect / columns;
				int c = indexSelect - (r * columns);
				g.setColor(0xffffff);
				g.drawRect(xstart + (c * indexSize), ystart + (r * indexSize), indexSize, indexSize);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void paintItem(mGraphics g, Item item, int x, int y) {
		paintItem(g, item, x, y, 0, 0);
	}

	private void paintItem(mGraphics g, Item item, int x, int y, int dUpgrade, int dR) {
		
	}

	private void paintItemBackground(mGraphics g, Item item, int x, int y, int dR) {
		if (!item.isLock)
			g.setColor(0x002f33);
		else
			g.setColor(0x001919);
		g.fillRect(x + 1 + dR, y + 1 + dR, indexSize - 2 - 2 * dR, indexSize - 2 - 2 * dR);
		SmallImage.drawSmallImage(g, 154, x + indexSize / 2, y + indexSize / 2, 0, mGraphics.VCENTER | mGraphics.HCENTER);

	}


	public void paintTrade(mGraphics g, String[] titles) {
		try {
			GameCanvas.paint.paintFrame(popupX, popupY, popupW, popupH, g);
			paintTitle(g, titles[indexMenu], titles.length > 1);

			xstart = popupX + 3;
			ystart = popupY + 45;

			rows = 4;
			mFont.tahoma_7_yellow.drawString(g, Char.myChar().cName, xstart + 1, ystart - 12, FontSys.LEFT);

			int xStatus = xstart;
			for (int i = 0; i < 3; i++) {
				if (i == typeTrade)
					mFont.tahoma_7_blue1
							.drawString(g, String.valueOf(i + 1), xStatus + 2 + (i * 20), ystart + rows * (indexSize + 3) + 8, FontSys.LEFT);

				else
					mFont.tahoma_7_grey.drawString(g, String.valueOf(i + 1), xStatus + 2 + (i * 20), ystart + rows * (indexSize + 3) + 8, FontSys.LEFT);
				if (i < 2)
					SmallImage.drawSmallImage(g, 942, xStatus + 14 + (i * 20), ystart + rows * (indexSize + 3) + 13, 0, StaticObj.VCENTER_HCENTER);

			}
			mFont.tahoma_7_white.drawString(g, Util.numberToString(String.valueOf(coinTrade)) + " " + mResources.XU, xstart, ystart + rows
					* indexSize + 4, FontSys.LEFT);

			if (typeTrade == 0)
				g.setColor(0);
			if (typeTrade == 1)
				g.setColor(0x03382a);
			if (typeTrade == 2)
				g.setColor(0x0c4b3a);

			g.fillRect(xstart - 1, ystart - 1, indexSize * 3 + 3, indexSize * 4 + 3);
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < 3; j++) {
					SmallImage.drawSmallImage(g, 154, xstart + (j * indexSize) + indexSize / 2, ystart + (i * indexSize) + indexSize / 2, 0,
							mGraphics.VCENTER | mGraphics.HCENTER);
					g.setColor(0xBB6611);
					g.drawRect(xstart + (j * indexSize), ystart + (i * indexSize), indexSize, indexSize);
				}
			}
			if (indexTitle == 1) {
				int r = indexSelect / 3;
				int c = indexSelect - (r * 3);
				g.setColor(0xffffff);
				g.drawRect(xstart + (c * indexSize), ystart + (r * indexSize), indexSize, indexSize);
			}
			if (arrItemTradeMe != null) {
				for (int i = 0; i < arrItemTradeMe.length; i++) {
					Item item = arrItemTradeMe[i];
					if (item != null) {
						int r = i / 3;
						int c = i - (r * 3);
						if (!item.isLock) {
							g.setColor(0x002f33);
							g.fillRect(xstart + (c * indexSize) + 1, ystart + (r * indexSize) + 1, indexSize - 1, indexSize - 1);

						}
						SmallImage.drawSmallImage(g, item.template.iconID, xstart + (c * indexSize) + indexSize / 2, ystart + (r * indexSize)
								+ indexSize / 2, 0, mGraphics.VCENTER | mGraphics.HCENTER);
						if (item.quantity > 1)
							mFont.number_yellow.drawString(g, String.valueOf(item.quantity), xstart + (c * indexSize) + indexSize, ystart
									+ (r * indexSize) + indexSize - FontSys.number_yellow.getHeight(), 1);
						if (item.quantity > 1)
							mFont.number_yellow.drawString(g, String.valueOf(item.quantity), xstart + (c * indexSize) + indexSize, ystart
									+ (r * indexSize) + indexSize - mFont.number_yellow.getHeight(), 1);
					}
				}
			}

			xstart = popupX + popupW - 2 - indexSize * 3;
			rows = 4;
			mFont.tahoma_7_yellow.drawString(g, tradeName, popupX + popupW - 2, ystart - 12, FontSys.RIGHT);

			xStatus = popupX + popupW - 3 - 60;

			for (int i = 0; i < 3; i++) {
				if (i == typeTradeOrder)

					mFont.tahoma_7_blue1
							.drawString(g, String.valueOf(i + 1), xStatus + 2 + (i * 20), ystart + rows * (indexSize + 3) + 8, FontSys.LEFT);

				else
					mFont.tahoma_7_grey.drawString(g, String.valueOf(i + 1), xStatus + 2 + (i * 20), ystart + rows * (indexSize + 3) + 8, FontSys.LEFT);

				if (i < 2)

					SmallImage.drawSmallImage(g, 942, xStatus + 14 + (i * 20), ystart + rows * (indexSize + 3) + 13, 0, StaticObj.VCENTER_HCENTER);

			}

			mFont.tahoma_7_white.drawString(g, Util.numberToString(String.valueOf(coinTradeOrder)) + " " + mResources.XU, popupX + popupW - 2,
					ystart + rows * indexSize + 4, FontSys.RIGHT);
			if (typeTradeOrder == 0)
				g.setColor(0);
			if (typeTradeOrder == 1)
				g.setColor(0x03382a);
			if (typeTradeOrder == 2)
				g.setColor(0x0c4b3a);
			g.fillRect(xstart - 1, ystart - 1, indexSize * 3 + 3, indexSize * 4 + 3);

			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < 3; j++) {
					SmallImage.drawSmallImage(g, 154, xstart + (j * indexSize) + indexSize / 2, ystart + (i * indexSize) + indexSize / 2, 0,
							mGraphics.VCENTER | mGraphics.HCENTER);
					g.setColor(0xBB6611);
					g.drawRect(xstart + (j * indexSize), ystart + (i * indexSize), indexSize, indexSize);
				}
			}
			if (indexTitle == 2) {
				int r = indexSelect / 3;
				int c = indexSelect - (r * 3);
				g.setColor(0xffffff);
				g.drawRect(xstart + (c * indexSize), ystart + (r * indexSize), indexSize, indexSize);
			}
			if (arrItemTradeOrder != null) {
				for (int i = 0; i < arrItemTradeOrder.length; i++) {
					Item item = arrItemTradeOrder[i];
					if (item != null) {
						int r = i / 3;
						int c = i - (r * 3);
						if (!item.isLock) {

							g.setColor(0x002f33);
							g.fillRect(xstart + (c * indexSize) + 1, ystart + (r * indexSize) + 1, indexSize - 1, indexSize - 1);

						}
						SmallImage.drawSmallImage(g, item.template.iconID, xstart + (c * indexSize) + indexSize / 2, ystart + (r * indexSize)
								+ indexSize / 2, 0, mGraphics.VCENTER | mGraphics.HCENTER);
						if (item.quantity > 1)
							mFont.number_yellow.drawString(g, String.valueOf(item.quantity), xstart + (c * indexSize) + indexSize, ystart
									+ (r * indexSize) + indexSize - FontSys.number_yellow.getHeight(), 1);
						if (item.quantity > 1)
							mFont.number_yellow.drawString(g, String.valueOf(item.quantity), xstart + (c * indexSize) + indexSize, ystart
									+ (r * indexSize) + indexSize - FontSys.number_yellow.getHeight(), 1);
					}
				}
			}

			int timeNow = (int) (System.currentTimeMillis() / 1000);
			if (timeTrade - timeNow > 0 && typeTrade == 1 && typeTradeOrder == 1) {
				mFont.tahoma_7_white.drawString(g, mResources.WAIT + " " + (timeTrade - timeNow) + " " + mResources.SECOND, popupX + popupW / 2,
						popupY + popupH - 13, 2);
			} else if (typeTrade == 0) {
				mFont.tahoma_7_white.drawString(g, mResources.TRADEHELP, popupX + popupW / 2, popupY + popupH - 13, 2);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	

	public void paintUILuckySprin(mGraphics g) {
		if (isPaintLuckySpin) {

			resetTranslate(g);
			GameCanvas.paint.paintFrame(popupX, popupY, popupW, popupH, g);

			int w = popupW;
			if (GameCanvas.isTouchControlLargeScreen)
				w = popupW / 2 + 20;

			g.setColor(0);// màu đen
			g.fillRect(popupX + 7, popupY + 31, w - 14, popupH - 58);// 120
			g.setColor(0xFFcf9f38);// viền nâu
			g.drawRect(popupX + 8, popupY + 32, w - 16, popupH - 60);
			g.setColor(Paint.COLORBACKGROUND);// nền nâu trong
			g.fillRect(popupX + 9, popupY + 33, w - 18, popupH - 62);
			paintTitle(g, mResources.LATQUA, false);

			int dis = 40, cellW = 35;
			int num = 3;
			xstart = popupX + 33;
			ystart = popupY + 40;
			for (int i = 0; i < num; i++) {
				for (int j = 0; j < num; j++) {
					g.setColor(Paint.COLORDARK);
					g.fillRect(xstart + j * dis, ystart + 10 + i * dis, cellW - 6, cellW - 6);
					g.setColor(0xFF9C6531);
					g.drawRect(xstart + j * dis, ystart + 10 + i * dis, cellW - 6, cellW - 6);
					g.setColor(0xFF993300);
					g.fillRect(xstart + j * dis + 2, ystart + 12 + i * dis, cellW - 9, cellW - 9);
					g.setColor(Paint.COLORDARK);
					g.fillRect(xstart + j * dis + 4, ystart + 14 + i * dis, cellW - 13, cellW - 13);
					SmallImage.drawSmallImage(g, 1414, xstart + j * dis + dis / 2 - 5, ystart + i * dis + dis / 2 + 4, 0, StaticObj.VCENTER_HCENTER);
				}
			}

			for (int i = 0; i < 9; i++) {
				int r = i / num;
				int c = i - (r * num);
				if (arrItemSprin != null) {
					g.setColor(0xFF001919);
					g.fillRect(xstart + c * dis + 4, ystart + 14 + r * dis, cellW - 13, cellW - 13);
					g.setColor((i == indexSelect) ? 0xFFffffff : 0xFF9C6531);
					g.drawRect(xstart + c * dis + 4, ystart + 14 + r * dis, cellW - 14, cellW - 14);
					SmallImage.drawSmallImage(g, 154, xstart + c * dis + cellW / 2 - 3, ystart + 7 + r * dis + cellW / 2, 0, mGraphics.VCENTER
							| mGraphics.HCENTER);
					if (System.currentTimeMillis() - timePoint < 1000) {
						if (i == indexCard)
							SmallImage.drawSmallImage(g, ItemTemplates.getIcon(arrItemSprin[indexCard]), xstart + c * dis + cellW / 2 - 3, ystart + 7
									+ r * dis + cellW / 2, 0, mGraphics.VCENTER | mGraphics.HCENTER);
						else
							SmallImage.drawSmallImage(g, 1414, xstart + c * dis + cellW / 2 - 2, ystart + 7 + r * dis + cellW / 2, 0, StaticObj.VCENTER_HCENTER);
					} else{
						if(arrItemSprin[i]< 0 || arrItemSprin[i] >= ItemTemplates.itemTemplates.size())
							SmallImage.drawSmallImage(g, ItemTemplates.getIcon((short) 242), xstart + c * dis + cellW / 2 - 3, ystart + 7 + r * dis + cellW / 2, 0, mGraphics.VCENTER | mGraphics.HCENTER);
						else
							SmallImage.drawSmallImage(g, ItemTemplates.getIcon(arrItemSprin[i]), xstart + c * dis + cellW / 2 - 3, ystart + 7 + r * dis + cellW / 2, 0, mGraphics.VCENTER | mGraphics.HCENTER);
					}
						
							

					if (indexCard == i && yenTemp > 0)
						yenValue[i] = yenTemp + "";

					if (indexTitle == 1) {
						if (indexCard == i && GameCanvas.gameTick % 10 > 4) {
							g.setColor(0xFFcc9933);
							g.drawRect(xstart + c * dis, ystart + 10 + r * dis, cellW - 6, cellW - 6);
						} else {
							if (i == indexSelect) {
								g.setColor(0xFFffffff);
								g.drawRect(xstart + c * dis, ystart + 10 + r * dis, cellW - 6, cellW - 6);
							} else {
								g.setColor(Paint.COLORLIGHT);
								g.drawRect(xstart + c * dis, ystart + 10 + r * dis, cellW - 6, cellW - 6);
							}
						}
					}
				} else {
					if (indexTitle == 1) {
						g.setColor((i == indexSelect) ? 0xFFffffff : Paint.COLORLIGHT);
						g.drawRect(xstart + c * dis, ystart + 10 + r * dis, cellW - 6, cellW - 6);
					}
				}
			}

			mFont.tahoma_7_yellow.drawString(g, mResources.LUOTLATQUA + numSprinLeft, popupX + popupW / 2, popupY + popupH - 20, FontSys.CENTER);
		}

	}

	

	

	public void paintTiemNang(mGraphics g) {
		if (indexMenu != 2)
			return;
		g.translate(-g.getTranslateX(), -g.getTranslateY());
		GameCanvas.paint.paintFrame(popupX, popupY, popupW, popupH, g);
		paintTitle(g, mResources.MENUME[indexMenu], true);

		mFont.tahoma_8b.drawString(g, mResources.POTENTIAL_POINT, popupX + 10, popupY + 33, 0);
		mFont.tahoma_8b.drawString(g, "" + Char.myChar().pPoint, popupX + popupW - 10, popupY + 33, FontSys.RIGHT);
		int rowHeight = (popupH - 80) / 5;

		for (int i = 0; i < Char.myChar().potential.length; i++) {
			g.setColor(Paint.COLORBORDER);
			if (indexTitle > 0 && indexTitle - 1 == i) {
				g.setColor(Paint.COLORDARK);
				g.fillRect(popupX + 5, popupY + 52 + i * (rowHeight + 4), popupW - 10, rowHeight);
				g.setColor(Paint.COLORFOCUS);
			}

			g.drawRect(popupX + 5, popupY + 52 + i * (rowHeight + 4), popupW - 10, rowHeight);
			mFont.tahoma_7b_white.drawString(g, "" + Char.myChar().potential[i], popupX + popupW - 10, popupY + 52 + (rowHeight - 10) / 2 + i
					* (rowHeight + 4), FontSys.RIGHT);
			mFont.tahoma_7b_white
					.drawString(g, mResources.NAMEPOTENTIAL[i], popupX + 10, popupY + 52 + (rowHeight - 10) / 2 + i * (rowHeight + 4), 0);
		}
		if (indexTitle > 0) {
			switch (Char.myChar().nClass.classId) {
			case 0:

				mFont.tahoma_7_green.drawString(g, mResources.HELPPOTENTIAL1[0], popupX + 10, popupY + 52 + (rowHeight - 10) / 2 + 4
						* (rowHeight + 4), 0);
				break;
			case 1:
			case 3:
			case 5:
				mFont.tahoma_7_green.drawString(g, mResources.HELPPOTENTIAL2[indexTitle - 1], popupX + 10, popupY + 52 + (rowHeight - 10) / 2 + 4
						* (rowHeight + 4), 0);
				break;
			case 2:
			case 4:
			case 6:
				mFont.tahoma_7_green.drawString(g, mResources.HELPPOTENTIAL3[indexTitle - 1], popupX + 10, popupY + 52 + (rowHeight - 10) / 2 + 4
						* (rowHeight + 4), 0);
				break;
			}
		}
	}

	public static Item getItemFocus(int typeUI) {
		try {
			if (indexSelect < 0)
				return null;
			switch (typeUI) {
			case Item.UI_BAG:
				return Char.myChar().arrItemBag[indexSelect];
			case Item.UI_BOX:
				return Char.myChar().arrItemBox[indexSelect];
			case Item.UI_BODY:
				return currentCharViewInfo.arrItemBody[indexSelect];
			case Item.UI_NONNAM:
				return arrItemNonNam.length > indexSelect ? arrItemNonNam[indexSelect] : null;
			case Item.UI_NONNU:
				return arrItemNonNu.length > indexSelect ? arrItemNonNu[indexSelect] : null;
			case Item.UI_AONAM:
				return arrItemAoNam.length > indexSelect ? arrItemAoNam[indexSelect] : null;
			case Item.UI_AONU:
				return arrItemAoNu.length > indexSelect ? arrItemAoNu[indexSelect] : null;
			case Item.UI_GANGTAYNAM:
				return arrItemGangTayNam.length > indexSelect ? arrItemGangTayNam[indexSelect] : null;
			case Item.UI_GANGTAYNU:
				return arrItemGangTayNu.length > indexSelect ? arrItemGangTayNu[indexSelect] : null;
			case Item.UI_QUANNAM:
				return arrItemQuanNam.length > indexSelect ? arrItemQuanNam[indexSelect] : null;
			case Item.UI_QUANNU:
				return arrItemQuanNu.length > indexSelect ? arrItemQuanNu[indexSelect] : null;
			case Item.UI_GIAYNAM:
				return arrItemGiayNam.length > indexSelect ? arrItemGiayNam[indexSelect] : null;
			case Item.UI_GIAYNU:
				return arrItemGiayNu.length > indexSelect ? arrItemGiayNu[indexSelect] : null;
			case Item.UI_LIEN:
				return arrItemLien.length > indexSelect ? arrItemLien[indexSelect] : null;
			case Item.UI_NHAN:
				return arrItemNhan.length > indexSelect ? arrItemNhan[indexSelect] : null;
			case Item.UI_NGOCBOI:
				return arrItemNgocBoi.length > indexSelect ? arrItemNgocBoi[indexSelect] : null;
			case Item.UI_PHU:
				return arrItemPhu.length > indexSelect ? arrItemPhu[indexSelect] : null;
			case Item.UI_WEAPON:
				return arrItemWeapon.length > indexSelect ? arrItemWeapon[indexSelect] : null;
			case Item.UI_STACK:
				return arrItemStack.length > indexSelect ? arrItemStack[indexSelect] : null;
			case Item.UI_STACK_LOCK:
				return arrItemStackLock.length > indexSelect ? arrItemStackLock[indexSelect] : null;
			case Item.UI_GROCERY:
				return arrItemGrocery.length > indexSelect ? arrItemGrocery[indexSelect] : null;
			case Item.UI_GROCERY_LOCK:
				return arrItemGroceryLock.length > indexSelect ? arrItemGroceryLock[indexSelect] : null;
			case Item.UI_STORE:
				return arrItemStore.length > indexSelect ? arrItemStore[indexSelect] : null;
			case Item.UI_ELITES:
				return arrItemElites.length > indexSelect ? arrItemElites[indexSelect] : null;
			case Item.UI_BOOK:
				return arrItemBook.length > indexSelect ? arrItemBook[indexSelect] : null;
			case Item.UI_FASHION:
				return arrItemFashion.length > indexSelect ? arrItemFashion[indexSelect] : null;
			case Item.UI_UPPEARL:
				return arrItemUpPeal[indexSelect];
			case Item.UI_LUYEN_THACH:
				return arrItemUpPeal[indexSelect];
			case Item.UI_TINH_LUYEN_AO:
				return arrItemSplit[indexSelect];
			case Item.UI_TINH_LUYEN_THU:
				return arrItemSplit[indexSelect];
			case Item.UI_UPGRADE:
				return arrItemUpGrade[indexSelect];
			case Item.UI_CLANSHOP:
				return arrItemClanShop.length > indexSelect ? arrItemClanShop[indexSelect] : null;
			}
		} catch (Exception e) {
		}
		return null;
	}

//	public static void loadImg() {
//		TileMap.loadMainTile();
//	}

	public void paintTitle(mGraphics g, String title, boolean arrow) {
		int xPaint = 0;
		if(isPaintFriend)
			xPaint = gW / 2;
		else
			xPaint = gW / 2;
		g.setColor(Paint.COLORDARK);
		g.fillRoundRect(xPaint - mFont.tahoma_8b.getWidth(title) / 2 - 12, popupY + 4, mFont.tahoma_8b.getWidth(title) + 22, 24, 6, 6);

		if (indexTitle == 0 || GameCanvas.isTouch) {

			if (arrow) {
				SmallImage.drawSmallImage(g, 989, xPaint - mFont.tahoma_8b.getWidth(title) / 2 - 15 - 7 - (GameCanvas.gameTick % 8 <= 3 ? 2 : 0),
						popupY + 16, 2, StaticObj.VCENTER_HCENTER);
				SmallImage.drawSmallImage(g, 989, xPaint + mFont.tahoma_8b.getWidth(title) / 2 + 15 + 5 + (GameCanvas.gameTick % 8 <= 3 ? 2 : 0),
						popupY + 16, 0, StaticObj.VCENTER_HCENTER);
			}
		}
		if (indexTitle == 0)
			g.setColor(Paint.COLORFOCUS);
		else
			g.setColor(Paint.COLORBORDER);
		g.drawRoundRect(xPaint - FontSys.tahoma_8b.getWidth(title) / 2 - 12, popupY + 4, FontSys.tahoma_8b.getWidth(title) + 22, 24, 6, 6);
		mFont.tahoma_8b.drawString(g, title, xPaint, popupY + 9, 2);
	}

	
	public static int inforX, inforY, inforW, inforH;
	//public Command cmdDead = new Command(mResources.DIES[0], 11038);
	protected Command cmdBagUseItem;
	protected Command cmdBagSortItem;
	private Command cmdItemInfoClose;
	protected Command cmdBagThrowItem;
	protected Command cmdBagSplitItem;
	private Command cmdBagViewItemInfo;
	private Command cmdBagSelectItem;


	
	
	

	

	

	

	
	public void showAlert(String title, String str, boolean withMenuShow) {
		// str =
		// "Mac dinh neu vo khong co gi la mau trang tinh khoi\nc1http://my.teamobi.com\nc0Tiep theo la noi dung mau trang mau trang mau trang\nc8Day la mot font mau khac nua do nha chua nghe ro chua\nc0Quay lai mau trang ne. Day la mau trang!";
		InfoDlg.hide();
		isPaintAlert = true;
		isLockKey = true;
		indexRow = 0;
		setPopupSize(175, 200);
		if (withMenuShow)
			popupH = popupH - 60;
		right = new Command(mResources.CLOSE, 0003);
		left = center = null;
		textsTitle = title;
		texts = mFont.tahoma_7.splitFontVector(str, popupW - 30);
	}

	public void doCloseAlert() {
		isPaintAlert = false;
		textsTitle = null;
		texts = null;
		center = null;
		resetButton();
	}

	public String[] splitMultiLine(FontSys f, String str) {
		String[] arr = f.splitFontArray(str, popupW - 20);
		return arr;
	}
	private void paintInforFrame(mGraphics g) {
		resetTranslate(g);
		g.setColor(0);// màu đen
		g.fillRect(inforX - 2, inforY - 2, inforW + 5, inforH + 5);// 120
		g.setColor(0xcf9f38);// viền nâu
		g.drawRect(inforX - 1, inforY - 1, inforW + 2, inforH + 2);
		g.setColor(Paint.COLORBACKGROUND);// nền nâu trong
		g.fillRect(inforX, inforY, inforW, inforH);

	}

	private void setInfoFrameForLargeScreen() {
		if (!GameCanvas.isTouchControlLargeScreen || (isPaintClan && indexMenu == 0))
			return;
		inforX = popupX + 175;
		inforW = popupW - 179;
		inforY = popupY + 33;
		inforH = 138;
		if (isPaintTrade && indexMenu == 0) {
			inforX = popupX + 6 + 3 * indexSize;
			inforW = popupW - (11 + 6 * indexSize);
		}
		if (isPaintInfoMe) {
			if(indexMenu ==4){
			inforX = popupX + 33;
			inforY = popupY + 87;
			inforW = popupW - 67;
			inforH = 75;
			}else if(indexMenu == 5){
				inforH = 161;
			}
		}
	}
	public static Char findCharInMap(short charId) {
		for (int i = 0; i < vCharInMap.size(); i++) {
			Char c = (Char) vCharInMap.elementAt(i);
			if (c.charID == charId)
				return c;
		}
		return null;
	}
	public static Mob findMobInMap(short charId) {
		for (int i = 0; i < vMob.size(); i++) {
			Mob c = (Mob) vMob.elementAt(i);
			if (c.mobId == charId)
				return c;
		}
		return null;
	}
//	public static BuNhin findBuNhinInMap(int index) {
//		if (vBuNhin.size() > 0)
//			return (BuNhin) vBuNhin.elementAt(index);
//		return null;
//	}

	

	

	public void openWeb(String strLeft, String strRight, final String url, String title, String str) {
		isPaintAlert = true;
		isLockKey = true;
		indexRow = 0;
		setPopupSize(175, 200);
		textsTitle = title;
//		texts = FontSys.tahoma_7.splitFontVector(str, popupW - 30);
		center = null;
		left = new Command(strLeft, 11068, url);
		right = new Command(strRight, 11069);
	}

	

	public static final int INFO = 0;
	public static final int STORE = 1;
	public static final int ZONE = 2;
	public static final int UPGRADE = 3;

	int Hitem = 30, maxSizeRow = 5, isTranKyNang = 0;
	boolean isTran = false;
	int cmY_Old, cmX_Old;

	/*
	 * execute action click button
	 */
	public void actionPerform(int idAction, Object p) {
		Cout.println(getClass(), "-----------------idaction "+idAction);
		Item item = null;
		String strTemp = "";
		mVector menu = null;
		switch (idAction) {
		case 111039:// dong y trao doi 
			Service.gI().acceptTrade(Constants.ACCEPT_INVITE_TRADE,(short) Char.myChar().partnerTrade.charID);
			tradeGui = new TradeGui(45, 0);
			Service.gI().requestinventory();
			GameCanvas.endDlg();
			break;
		case Constants.BUTTON_SEND:
			Cout.println(getClass(), " BUTTON_SEND  "+Char.toCharChat);
			if(TabChat.gI().tfCharFriend.getText().toLowerCase().trim().length()>0&&Char.toCharChat!=null)
				Service.gI().chatPrivate(Type_Chat.CHAT_FRIEND, Char.toCharChat.CharidDB, TabChat.gI().tfCharFriend.getText());
			TabChat.gI().tfCharFriend.setText("");
			TabChat.gI().tfCharFriend.isFocus = false;
			break;
			
		case Constants.BUTTON_ICON_CHAT:
			// xy ly button icon o cho nay
			TabChat.gI().iconChat= new Iconchat(popupX,popupY);
			isPaintZone=true;
			TabChat.gI().isPaintListIcon = true;
			break;
		case Constants.CHAT_PRIVATE: // mo giao dien chat rieng
//			ChatTab currentTab = ChatManager.gI().getCurrentChatTab();
//			currentTab.type = 2;
//			indexRow = -1;
//			isPaintFriend = false;
//			left = null;
//			center = null;
//			openUIChatTab();

			if(indexRow>=0){
				Char charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
					ChatPrivate.AddNewChater((short)Char.myChar().charID, charF.cName);
				}
			indexRow = -1;
			isPaintFriend = false;
			left = null;
			center = null;
			
			openUIChatTab();
			
			break;
			
//		case Constants.BUTTON_SEND_CHAT_WORLD: // event click button [send] in com.sakura.thelastlegend.gui chat world
//			if(guiChatWorld!=null)
//				guiChatWorld.ActionPerformSend();
//			
//			break;
//			
//		case Constants.BUTTON_ICON_CHAT_WORLD: // event click button [icon] in com.sakura.thelastlegend.gui chat world
//			if(guiChatWorld!=null)
//				guiChatWorld.ActionPerformIconChat();
//			
//			break;
			
		case Constants.UN_FRIEND:
			Char charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
			Service.gI().deleteFriend((byte)Friend.UNFRIEND,(short)Char.myChar().charID ,(short)charF.CharidDB);
			break;
			
		case Constants.MENU_BAG://info invetory
			isBag=true;
			break;	
			
		case Constants.MENU_LIST_PARTY://info party
			doShowTeamUI();
			break;	
		
		case Constants.MENU_CHAT_PRIVATE://info party
//			initTfied();

			openUIChatTab();
//			ispaintChat=true;
			break;
			
		case Constants.MENU_CHAT_WORLD://chat world
			break;
		
		case Constants.MENU_LIST_FRIEND://chat world
			//show list friend //tam thoi commment lai
			initTfied();
			Service.gI().requestFriendList((byte)Friend.REQUEST_FRIEND_LIST, (short)Char.myChar().charID);
			isPaintFriend = true;
			break;
			
		case Constants.MENU_QUEST://chat world
			//show list friend //tam thoi commment lai
			guiQuest=new ShopMain(GameCanvas.wd6-20, 20);
			isPaintQuest = true;
			break;
		case 155555555:// xin vao nhom
			Char ch = (Char) charnearByme.elementAt(indexRow);
			Service.gI().requestjoinParty(Type_Party.REQUEST_JOIN_PARTY, (short)ch.charID);
			break;
		case 166666666:
			ch = (Char) charnearByme.elementAt(indexRow);
			Service.gI().inviteParty((short)ch.charID, Type_Party.INVITE_PARTY);
			break;
		case 191210: // giai tan
			Service.gI().removeParty(Type_Party.DISBAND_PARTY);
			break;
		case 270192: // kich ra khoi nhom
			Party party = (Party) hParty.get(Char.myChar().idParty+"");
			if (((Char) party.vCharinParty.elementAt(0)).charID == Char.myChar().charID) {
				final Char c = (Char) party.vCharinParty.elementAt(indexRow);
				if (c.charID != Char.myChar().charID) {
					Service.gI().kickPlayeleaveParty((byte)Type_Party.KICK_OUT_PARTY, (short)c.charID);
//					center = new Command(mResources.SELECT, 11080);
				}
			}
			break;
		case 231291: // tu roi nhom
			Service.gI().leaveParty((byte)Type_Party.OUT_PARTY,(short)Char.myChar().charID);
			break;
		case 111037:
			Service.gI().AcceptParty(Type_Party.ACCEPT_INVITE_PARTY, Party.gI().charId);
			GameCanvas.endDlg();
			break;
		case 111038:
			Service.gI().AcceptFriend((byte)Friend.ACCEPT_ADD_FRIEND, (short)Char.myChar().idFriend);
			Char.myChar().idFriend = -1;
			GameCanvas.endDlg();
			break;
		case 909090:
//			doOpenUI(Item.UI_LUCKY_SPIN);
			break;
		case 0001:
			GameCanvas.endDlg();
			break; // CLOSE DIALOG
		case 0002: // UPDATE COMMAND FOR UI
			GameCanvas.endDlg();
			left = center = null;
			updateCommandForUI();
			break;
		case 0003: // CLOSE ARLET
			doCloseAlert();
			break;
		case 1000:
//			Service.gI().rewardPB();
			resetButton();
			break;
		case 2000:
//			Service.gI().rewardCT();
			resetButton();
			break;
		case 11000:
			actMenu();
			break; // LEFT MAIN MENU
		case 110001:
			doMenuInforMe();
			break;
		case 1100011:
			doBag();
			break;
		case 1100012:
			doskill();
			break;
		case 1100013:
			doTiemnangMe();
			break;
		case 1100014:
			doInfo();
			break;
		case 1100015:
			doTrangbi();
			break;
		case 1100016:
			doThucuoi();
			break;
		case 110002:
//			doShowMap();
			break;
		case 110003:
			doMenusynthesis();
			break;
		case 1100032:
			doShowTaskUI();
			break;
		case 1100033:
			domenuClan();
			break;
		case 110004:
			actMenu4();
			break;
		case 1100041:
//			actMenu41();
			break;
		case 110005:
//			doOpenUI(Item.UI_STORE);
			break;
		case 110006:
			actOrder();
			break;
		case 1100061:
			doShowFindTeamUI();
			break;
		case 1100062:
			doShowTeamUI();
			break;
		case 1100063:
			doShowFriendUI();
			break;
		case 1100064:
			doShowEnemiesUI();
			break;
		case 1100065:
			actsubMenuOrder();
			break;
		case 11000651:
//			actsubMenuOrder_PK(1);
			break;
		case 11000652:
//			actsubMenuOrder_PK(2);
			break;
		case 11000653:
//			actsubMenuOrder_PK(3);
			break;
		case 1100067:
			domenuPrivateLock();
			break;
		case 11000671:
			GameCanvas.startYesNoDlg(mResources.ACTIVE_PROTECT_ACC, 88836, null, 8882, null);
			break;
		case 11000672:
			GameCanvas.inputDlg.tfInput.setMaxTextLenght(6);
			GameCanvas.inputDlg.show(mResources.INPUT_PRIVATE_PASS, new Command(mResources.ACCEPT, GameCanvas.instance, 88837, null),
					TField.INPUT_TYPE_NUMERIC);
			break;
		case 11000673:
			GameCanvas.input2Dlg.setTitle(mResources.OLD_PASS, mResources.NEW_PASS);
			GameCanvas.input2Dlg.tfInput.setMaxTextLenght(6);
			GameCanvas.input2Dlg.tfInput2.setMaxTextLenght(6);
			GameCanvas.input2Dlg.show(mResources.INPUT_PRIVATE_PASS, new Command(mResources.CLOSE, GameCanvas.instance, 8882, null), new Command(
					mResources.ACCEPT, GameCanvas.instance, 88838, null), TField.INPUT_TYPE_NUMERIC, TField.INPUT_TYPE_NUMERIC);
			break;
		case 11000674:
			GameCanvas.inputDlg.tfInput.setMaxTextLenght(6);
			GameCanvas.inputDlg.show(mResources.INPUT_PRIVATE_PASS, new Command(mResources.ACCEPT, GameCanvas.instance, 88839, null),
					TField.INPUT_TYPE_NUMERIC);
			break;
		case 1100068:
//				doOpenUI(Item.UI_AUTO);
			break;
		case 11000661:
			indexMenu = 0;
//			doShowClan();
			break;
		case 11000662:
			indexMenu = 1;
//			doShowClan();
//			Service.gI().requestClanMember();
			break;
		case 11000663:
			indexMenu = 2;
//			doShowClan();
//			Service.gI().requestClanItem();
			break;
		case 11000664:
			indexMenu = 3;
//			doShowClan();
//			Service.gI().requestClanLog();
			break;
		case 11000665:
			isViewClanInvite = !isViewClanInvite;
			if(isViewClanInvite)
				Rms.saveRMSInt(Char.myChar().cName+"vci", 1);
			else
				Rms.saveRMSInt(Char.myChar().cName+"vci", 0);
			break;
		case 110007:
//			doOpenUI(Item.UI_STACK_LOCK);
			break;
		case 110008:
//			doOpenUI(Item.UI_STACK);
			break;
		case 110009:
//			doOpenUI(Item.UI_GROCERY_LOCK);
			break;
		case 110010:
//			doOpenUI(Item.UI_GROCERY);
			break;
		case 110011:
//			doOpenUI(Item.UI_UPGRADE);
			break;
		case 110012:
//			doOpenUI(Item.UI_UPPEARL);
			break;
		case 110013:
//			doOpenUI(Item.UI_UPPEARL_LOCK);
			break;
		case 110014:
//			doOpenUI(Item.UI_BOX);
			break;
		case 110015:
//			doOpenUI(Item.UI_SPLIT);
			break;
		case 110016:
//			Service.gI().openUIZone();
			break;
		case 110017:
			openUITrade();
			break;
		case 110018:
			doShowListChatTab();
			break;
		case 110019:
			changeTaskInfo();
			break;

		case 11001:
			Char.myChar().findNextFocusByKey();
			break;// RIGHT SOFT CHANGE FOCUS
		case 11002:
			Service.gI().AcceptFriend((byte)0,(short) Char.myChar().idFriend);
//			actAddFriendAccept();
			break;// ADD FRIEND ACCEPT
		case 11003:
//			actSkillUpPoint();
			break;// SKILL UP POINT

		case 11004:
//			actView(Item.UI_GANGTAYNU);
			break;// GANGTAYNU VIEW
		case 11005:
			actGangTayNuBuy();
			break;// GANGTAYNU BUY
		case 110051:
//			actBuy(Item.UI_GANGTAYNU);
			break;// GANGTAYNU BUY
		case 110052:
			actBuys(Item.UI_GANGTAYNU);
			break;// GANGTAYNU BUYS

		case 11006:
//			actAddPotential();
			break;

		case 11007:
//			actView(Item.UI_GANGTAYNAM);
			break; // GANG TAY NAM
		case 11008:
			actGangTayNamBuy();
			break;
		case 110081:
//			actBuy(Item.UI_GANGTAYNAM);
			break;
		case 110082:
			actBuys(Item.UI_GANGTAYNAM);
			break;

		case 11009:
//			actView(Item.UI_AONU);
			break;// AO NU
		case 11010:
			actAoNuBuy();
			break;
		case 110101:
//			actBuy(Item.UI_AONU);
			break;
		case 110102:
			actBuys(Item.UI_AONU);
			break;

		case 11011:
//			actView(Item.UI_AONAM);
			break; // AO NAM
		case 11012:
			actAoNamBuy();
			break;
		case 110121:
//			actBuy(Item.UI_AONAM);
			break;
		case 110122:
			actBuys(Item.UI_AONAM);
			break;

		case 11013:
//			actView(Item.UI_NONNU);
			break;
		case 11014:
			actNonNuBuy();
			break;
		case 110141:
//			actBuy(Item.UI_NONNU);
			break;
		case 110142:
			actBuys(Item.UI_NONNU);
			break;

		case 11015:
//			actView(Item.UI_NONNAM);
			break;
		case 11016:
			actNonNamBuy();
			break;
		case 110161:
//			actBuy(Item.UI_NONNAM);
			break;
		case 110162:
			actBuys(Item.UI_NONNAM);
			break;

		case 11017:
//			actView(Item.UI_BOOK);
			break;
		case 11018:
			actStoreLockBuy();
			break;
		case 110181:
//			actBuy(Item.UI_BOOK);
			break;
		case 110182:
			actBuys(Item.UI_BOOK);
			break;
		case 11019:
//			actView(Item.UI_STORE);
			break;

		case 11020:
			actStoreBuy();
			break;
		case 110201:
//			actBuy(Item.UI_STORE);
			break;
		case 110202:
			actBuys(Item.UI_STORE);
			break;

		case 11021:
			resetButton();
			break;
		case 11022:
			actBagSelectItem();
			break;
		case 110221:
//			actBagSortItem();
			break;
		case 11023:
			actBagViewItemInfo();
			break;
		case 11024:
//			actBagThrowItem();
			break;
		case 110244:
			actBagSplitItem();
			break;
		case 11025:
//			doCloseItemInfo();
			break;
		case 11026:
//			actBagUseItem();
			break;
		case 11027:
			actTradeSelectInList();
			break;
		case 11028:
			actTradeSelectInBag();
			break;
		case 11029:
//			actTradeSelectItem();
			break;
		case 11030:
//			actTradeViewItemInfo();
			break;

		case 11032:
//			actTradeLock();
			break;
		case 11033:
//			actTradeAccept();
			break;
		case 11034:
//			actUpgradeMoveOut();
			break;
		case 11035:
//			actSplitMoveOut();
			break;
		case 11036:
			actionCoinTrade();
			break;
		case 110361:
			actionCoinTradeAccept();
			break;
		case 11037:
//			actTradeMoveOut();
//			break;

		case 11038:
			actDead();
			break;
		case 110382:
//			Service.gI().returnTownFromDead();
			break;// RETURN TOWN FROM DEAD
		case 110383:
//			Service.gI().wakeUpFromDead();
			break;// WAKEUP FROM DEAD
		case 110391:
//			actmenuAttack(1);
			break;
		case 110392:
//			actmenuAttack(2);
			break;
		case 110393:
//			actmenuAttack(3);
			break;
		case 110394:
//			actmenuAttack(4);
			break;
		case 110395:
//			actmenuAttack(5);
			break;
		case 110396:
//			actmenuAttack(6);
			break;
		case 110397:
//			actmenuAttack(7);
			break;
		case 110398:
//			actmenuAttack(8);
			break;
		case 110399:
//			actmenuAttack(9);
			break;
		case 1103991:
//			actmenuAttack(10);
			break;
		case 11040:
//			actleftItemInfo();
			break; // DO ITEM INFO LEFT ACTION
		case 11041:
//			actrightItemInfo();
			break; // DO ITEM INFO RIGHT ACTION
		case 11042:
//			actCoinOut();
			break;
		case 11043:
//			actCoinIn();
			break;
		case 11044:
			domenuFriendLeft();
			break;
		case 110441:
			doAddFriend();
			break;
		case 11045:
			domenuFindTeam();
			break;
		case 110451:
//			actInputTeam();
			break;
		case 110452:
//			actRefreshFindTeam();
			break;
		case 11046:
//			domenuFindTeamCenter();
			break;
		case 11047:
			domenuTeamLeft();
			break;
		case 110471:
//			actCreateTeam();
			break;
		case 11048:
//			actBoxSort();
			break;
		case 11049:
			actionCoinOut();
			break;
		case 11050:
			actionCoinIn();
			break;
		case 11051:
//			actUseItem();
			break;
		case 11052:
			Item itemBag = (Item) p;
//			Service.gI().useItemChangeMap(itemBag.indexUI, GameCanvas.menu.menuSelectedItem);
			break;
		case 11053:
			Item itemBag3 = (Item) p;
			doItemChangeMap(itemBag3);
			break;
		case 110531:
			Item itemBag31 = (Item) p;
//			Service.gI().useItemChangeMap(itemBag31.indexUI, GameCanvas.menu.menuSelectedItem + 3);
			break;
		case 11054:
//			doOpenUIZone();
			break;
		case 11055:
			Item itemBuy = (Item) p;
//			doActionBuy(itemBuy);
			break;
		case 110561:
			GameCanvas.endDlg();
//			updateCommandForUI();
			break;
		case 110562:
			Item itemSaleOk = (Item) p;
			doActionSaleOk(itemSaleOk);
			break;
		case 11057:
//			Npc npcConfirm = (Npc) p;
//			Service.gI().getTask(npcConfirm.template.npcTemplateId, GameCanvas.menu.menuSelectedItem, -1);
			break;
		case 11058:
			Item itemConfirmSale = (Item) p;
			GameCanvas.endDlg();
//			Service.gI().saleItem(itemConfirmSale.indexUI, Integer.parseInt(GameCanvas.inputDlg.tfInput.getText()));
			break;
		case 11059:
//			actdoChooseSkill();
			break;
		case 11060:
			actdoMiniInfo();
			break;
		case 11061:
			Item itemSaleConfirm = (Item) p;
//			actSaleConfirm(itemSaleConfirm);
			break;
		case 11062:
//			upPearl();
			break;
		case 11063:
//			actConfirmUpgrade();
			break;
		case 11064:
//			Service.gI().upPotential(indexTitle - 1, 1);
			setLCR();
			break;
		case 11065:
			doUpPotential();
			break;
		case 11066:
			doCloseAlert();
			isPaintMessage = false;
			isMessageMenu = false;
			ChatTextField.gI().center = null;
			break;
		case 11067:
			if (TileMap.zoneID != indexSelect) {
//				Service.gI().requestChangeZone(indexSelect, indexItemUse);
				InfoDlg.showWait();
			} else
//				InfoMe.addInfo(mResources.ZONE_HERE);

			break;
		case 11068:
			strTemp = (String) p;
			actOpenWeb(strTemp);
			break;
		case 11069:
			actOpenWebCancel();
			break;
		case 11070:
			Party ptLeader = (Party) p;
//			actRefresh_TeamLeader(ptLeader);
			break;
		case 110701:
//			Service.gI().outParty();
			break;
		case 110702:
//			Service.gI().lockParty(true);
			break;
		case 110703:
//			Service.gI().lockParty(false);
			break;
		case 11071:
//			Service.gI().outParty();
			break;
		case 11072:
//			actSaleItem();
			break;
		case 110721:
//			Service.gI().saleItem(indexSelect, 1);
			break;
		case 110722:
			actionSale(Char.myChar().arrItemBag[indexSelect]);
			break;
		case 110723:
//			Service.gI().saleItem(indexSelect, Char.myChar().arrItemBag[indexSelect].quantity);
			break;
		case 11073:
			actionSale(Char.myChar().arrItemBag[indexSelect]);
			break;
		case 11074:
			mVector valueSMS = (mVector) p;
			short port = Short.parseShort(String.valueOf(valueSMS.elementAt(0)));
			String syntax = String.valueOf(valueSMS.elementAt(1));
//			actSendSMSLeft(port, syntax);
			break;
		case 11075:
//			actSendSMSRight();
			break;
		case 11076:
			strTemp = (String) p;
//			Service.gI().addParty(strTemp);
			break;
		case 11077:
			strTemp = (String) p;
//			actConfirmRemoveFriend(strTemp);
			break;
		case 110771:
			strTemp = (String) p;
			GameCanvas.endDlg();
//			Service.gI().removeFriend(strTemp);
			break;

		case 11078:
//			actSetEnemiesCommand();
			break;
		case 11079:
			strTemp = (String) p;
			actSetFriendCommand();
			break;
		case 110791:
			strTemp = (String) p;
//			Service.gI().addParty(strTemp);
			break;
		case 110792:
			strTemp = (String) p;
			actSetDeleteFriend(strTemp);
			break;
		case 1107921:
			strTemp = (String) p;
			GameCanvas.endDlg();
//			Service.gI().removeFriend(strTemp);
//			actRemoveWaitAcceptFriend(strTemp);
			break;
		case 1107931:
			strTemp = (String) p;
//			Service.gI().addFriend(strTemp);
			break;
		case 1107932: // remove friend accept waitting list
			strTemp = (String) p;
//			actRemoveWaitAcceptFriend(strTemp);
			break;
		case 11080: // set command giao diện party
			strTemp = (String) p;
			actSetPartyCommand(strTemp);
			break;
		case 110801:
//			Service.gI().moveMember(GameScreen.indexRow);
			break;
		case 110802:
//			Service.gI().changeTeamLeader(GameScreen.indexRow);
			break;
		case 110803:
			strTemp = (String) p;
//			Service.gI().addFriend(strTemp);
			break;
		case 110804:
//			viewMemberInfo();
			break;
		case 1108041:
			strTemp = (String) p;
//			Service.gI().viewInfo(strTemp, 0);
			GameScreen.gI().resetButton();
			break;
		case 110805:
//			viewMemberClanInfo();
			break;
		case 11081:
			actdoGan();
			break;
		case 110811:
			SkillTemplate skillTemplate = Char.myChar().nClass.skillTemplates[indexSelect];
			doSetKeySkill(skillTemplate);
			break;
		case 110812:
			SkillTemplate skillTemplate2 = Char.myChar().nClass.skillTemplates[indexSelect];
			doSetOnScreenSkill(skillTemplate2);
			break;
		case 11082:
			actTrangBiSelect();
			break;
		case 110821:
//			Service.gI().itemBodyToBag(indexSelect);
			break;
		case 11083:
//			actView(Item.UI_BODY);
			break;
		case 11084:
			actdoUpPoint();
			break;
		case 110841:
//			Service.gI().upPotential(indexTitle - 1, 1);
			setLCR();
			break;
		case 110842:
			doUpPotential();
			break;
		case 11085:
			Item itemQuanNamBuy = (Item) p;
			actBuyQuanNam(itemQuanNamBuy);
			break;
		case 110851:
			Item buyItemQuanNam = (Item) p;
//			Service.gI().buyItem(buyItemQuanNam.typeUI, buyItemQuanNam.indexUI, 1);
			break;
		case 110852:
			Item itemQuanNamBuys = (Item) p;
			actionBuy(itemQuanNamBuys);
			break;
		case 110854:
//			actTrangBiCenter();
			break;
		case 11086:
			actTrangBiRight();
			break;
		case 11087:
			Item itemS = (Item) p;
			GameCanvas.endDlg();
//			Service.gI().splitItem(itemS);
			break;
		case 11088:
//			actView(Item.UI_QUANNAM);
			break;
		case 11089:
//			actView(Item.UI_QUANNU);
			break;
		case 11090:
//			actView(Item.UI_GIAYNAM);
			break;
		case 11091:
//			actView(Item.UI_GIAYNU);
			break;
		// TRANG SỨC
		case 11092:
			Item itemTrangSuc = (Item) p;
			actBuyLeft(itemTrangSuc);
			break;
		case 110921:
			final Item itemTSBuy = (Item) p;
//			Service.gI().buyItem(itemTSBuy.typeUI, itemTSBuy.indexUI, 1);
			break;
		case 110922:
			final Item itemTSBuys = (Item) p;
			actionBuy(itemTSBuys);
			break;
		case 110923:
//			actView(Item.UI_LIEN);
			break;
		case 110924:
//			actView(Item.UI_NHAN);
			break;
		case 110925:
//			actView(Item.UI_NGOCBOI);
			break;
		case 110926:
//			actView(Item.UI_PHU);
			break;

		case 11093:
//			actView(Item.UI_WEAPON);
			break;
		case 11094:
//			actView(Item.UI_STACK);
			break;
		case 11095:
//			actView(Item.UI_STACK_LOCK);
			break;
		case 11096:
//			actView(Item.UI_GROCERY);
			break;
		case 11097:
//			actView(Item.UI_GROCERY_LOCK);
			break;
		case 11098:
			actUpgradeLeft();
			break;
		case 110981:
//			upGrade();
			break;
		case 11099:
			isViewNext = false;
			updateItemInfo(Item.UI_BAG, itemUpGrade);
			break;
		case 110991:
			isViewNext = true;
			updateItemInfo(Item.UI_BAG, itemUpGrade);
			break;
		case 11100:
			actUpgradeBag();
			break;
		case 111001:
//			actItemUpgradeMoveOut();
			break;
		case 11101:
			item = getItemFocus(Item.UI_UPGRADE);
			updateItemInfo(Item.UI_BAG, item);

			break;
		case 11102:
//			actUpgrade_HanhTrang();
			break;
		case 11103:
			actSplitItem();
			break;
		case 111031:
//			if(isPaintTinhluyen)
//				Service.gI().tinhluyen(itemSplit, arrItemSplit);
//			else if(isPaintDichChuyen)
//				Service.gI().dichchuyen(itemSplit, arrItemSplit);
			break;
		case 11104:
			updateItemInfo(Item.UI_BAG, ((Item)p));
			break;
		case 11105:
//			split();
			break;
		case 11106:
//			actSplitItemBag();
			break;
		case 11107:
			actUpPearlSelect();
			break;
		case 111071:
//			actUpPearlMoveOut();
			break;
		case 11108:
//			actView(Item.UI_BAG);
			break;
		case 11109:
//			actUpPearlItems();
			break;
		case 11110:
			item = arrItemTradeOrder[indexSelect];
			updateItemInfo(Item.UI_TRADE, item);
			break;
		case 111101:
			item = getItemFocus(Item.UI_BOX);
//			Service.gI().itemBoxToBag(item.indexUI);
			break;
		case 11111:
			item = getItemFocus(Item.UI_BOX);
			updateItemInfo(Item.UI_BOX, item);
			break;
		case 11112:
//			Service.gI().boxSort();
			break;
		case 11113:
//			Service.gI().itemBagToBox(Char.myChar().arrItemBag[indexSelect].indexUI);
			break;
		case 11114:
			updateItemInfo(Item.UI_BAG, Char.myChar().arrItemBag[indexSelect]);
			break;
		case 11115:
			actBuyItemUILeft();
			break;
		case 11116:
			actBuyItemLeft2();
			break;
		case 11120:
			Object[] objOnScreen = (Object[]) p;
			Skill skOnScreen = (Skill) objOnScreen[0];
			int indexOnScreen = Integer.parseInt((String) objOnScreen[1]);
			onScreenSkill[indexOnScreen] = skOnScreen;
//			saveOnScreenSkillToRMS();
			break;
		case 11121:
			Object[] objOnKey = (Object[]) p;
			Skill skOnKey = (Skill) objOnKey[0];
			int indexOnKey = Integer.parseInt((String) objOnKey[1]);
			keySkill[indexOnKey] = skOnKey;
//			saveKeySkillToRMS();
			break;

		case 12000:
//			actOpenAlertURL();
			break;
		case 12001:
			ChatManager.gI().switchToTab(((Integer) p).intValue());
			openUIChatTab();
			break;
		case 12002:
		case 12004:
			strTemp = (String) p;
			ChatTab t = ChatManager.gI().findTab(strTemp);
			if (t == null) {
				ChatManager.gI().addNewTab(strTemp);
				ChatManager.gI().switchToLastTab();
			} else {
				ChatManager.gI().switchToTab(t);
			}

			openUIChatTab();
			isPaintTeam = isPaintFriend = isPaintEnemies = isPaintClan = isPaintFindTeam = false;
			ChatTextField.gI().center = null;
			break;
		case 12003:
			doShowFriendUI();
			break;
		case 12005:
			doShowChatTextFieldInMessage();
			break;
		case 120051:

			ChatTab curChattab = (ChatTab) p;
			ChatManager.gI().chatTabs.removeElement(curChattab);
			if (ChatManager.gI().currentTabIndex > ChatManager.gI().chatTabs.size() - 1)
				ChatManager.gI().switchToPreviousTab();
			curChattab = ChatManager.gI().getCurrentChatTab();
			if (curChattab != null)
				openUIChatTab();
			else {
				ChatTextField.gI().isShow = false;
				resetButton();
			}

			break;
		case 12006:// chặn tin nhắn
//			doShowBlockMessageMenu();
			break;
		case 120061:
			ChatManager.blockGlobalChat = !ChatManager.blockGlobalChat;
			GameCanvas.startOKDlg(mResources.BLOCK_GLOBAL_CHAT + (ChatManager.blockGlobalChat ? mResources.ON : mResources.OFF));
			break;
		case 120062:
			ChatManager.blockPrivateChat = !ChatManager.blockPrivateChat;
			GameCanvas.startOKDlg(mResources.BLOCK_PRIVATE_CHAT + (ChatManager.blockPrivateChat ? mResources.ON : mResources.OFF));
			break;
		case 12007:
			step = 1;
			strErrCard = "";
			doShowErrorInput();
			break;
		case 120071:
			step = 2;
			if (GameCanvas.input2Dlg.tfInput.getText().equals("")) {
				GameCanvas.startOKDlg(mResources.NOT_INPUT_CARDINFO);
			} else if (GameCanvas.input2Dlg.tfInput2.getText().equals("")) {
				GameCanvas.startOKDlg(mResources.NOT_INPUT_CARDMONEY);
			} else {
				strErrCard = "Loại thẻ: " + GameCanvas.input2Dlg.tfInput.getText();
				strErrCard += ", Mệnh giá: " + GameCanvas.input2Dlg.tfInput2.getText();

				GameCanvas.endDlg();
				doShowErrorInput();
			}
			break;
		case 120072:
			if (GameCanvas.input2Dlg.tfInput.getText().equals("")) {
				GameCanvas.startOKDlg(mResources.NOT_INPUT_CARDSERI);
			} else if (GameCanvas.input2Dlg.tfInput2.getText().equals("")) {
				GameCanvas.startOKDlg(mResources.NOT_INPUT_CARDTIME);
			} else {
				strErrCard += ", Số seri: " + GameCanvas.input2Dlg.tfInput.getText();
				strErrCard += ", Khoảng thời gian nạp: " + GameCanvas.input2Dlg.tfInput2.getText();
//				Service.gI().adminChat(strErrCard);
				GameCanvas.endDlg();
			}
			break;
		case 12008:
			domenuErrorAdim();
			break;
		case 120081:
			GameCanvas.inputDlg.tfInput.setMaxTextLenght(11);
			GameCanvas.inputDlg.show(mResources.INPUT_REG_NUMPHONE, new Command("OK", null, 120082, null), TField.INPUT_TYPE_NUMERIC);
			break;
		case 120082:
			strTemp = GameCanvas.inputDlg.tfInput.getText();
			if (strTemp.equals("")) {
				GameCanvas.startOKDlg(mResources.NOT_INPUT_NUMPHONE);
			} else {
//				Service.gI().adminChat("Số điện thoại đăng ký: " + strTemp);
				GameCanvas.endDlg();
			}
			break;
		case 12009:
			mVector subPermission = new mVector();
			subPermission.addElement(new Command("Xin vào nhóm", 155555555));
			subPermission.addElement(new Command("Mời vào nhóm", 166666666));
//			subPermission.addElement(new Command("Rời", 231291));
//			subPermission.addElement(new Command("Giải tán", 191210));
//			subPermission.addElement(new Command(mResources.TEAMLEADER_SELECT[0], 110801));
//			subPermission.addElement(new Command(mResources.TEAMLEADER_SELECT[1], 110802));
//			subPermission.addElement(new Command(mResources.TEAMLEADER_SELECT[2], 110803, partyName));
//			subPermission.addElement(new Command(mResources.CHAR_ORDER[7], 12002, partyName));
//			subPermission.addElement(new Command(mResources.CHAR_ORDER[6], 110804));
			
			GameCanvas.menu.startAt(subPermission, 3);
			break;
		case 130011:
//			Npc npc1 = (Npc) p;
//			Service.gI().getTask(npc1.template.npcTemplateId, 0, -1);
//			npc1.chatPopup = null;
			resetButton();
			break;
		case 130012:
//			Npc npc2 = (Npc) p;
//			npc2.chatPopup = null;
			resetButton();
			break;

		case 13001:
//			actView(Item.UI_FASHION);
			break;
		case 13002:
			actStoreFashionBuy();
			break;
		case 130021:
//			actBuy(Item.UI_FASHION);
			break;
		case 130022:
			actBuys(Item.UI_FASHION);
			break;

		case 14001:
			actdoKyNang();
			break;
		case 140011:
//			Service.gI().upSkill(Char.myChar().nClass.skillTemplates[indexSelect].id, 1);
			setLCR();
			break;
		case 140012:
			doUpSkill();
			break;
		case 14002:
			doUnfocusChar();
			break;
		case 14003:
			doCharFocusList();
			break;
		case 14004:
//			domenuClanNotice();
			break;
		case 140041:
			GameCanvas.inputDlg.tfInput.setMaxTextLenght(180);
			GameCanvas.inputDlg.show(mResources.INPUT_CLAN_TEXT, new Command(mResources.ACCEPT, GameCanvas.instance, 88832, null),
					TField.INPUT_TYPE_ANY);
			break;
		case 140042:
//			Service.gI().clanUpLevel();
			break;
		case 140043:
			GameCanvas.inputDlg.show(mResources.INPUT_CLAN_MONEY, new Command(mResources.ACCEPT, GameCanvas.instance, 88834, null),
					TField.INPUT_TYPE_ANY);
			break;
		case 140044:
//			Service.gI().unlockClanItem();
			break;
		case 14005:
//			domenuClanLeader();
			break;
		case 14006:
			strTemp = (String) p;
			actSetClanCommand(strTemp);
			break;
		case 14007:
			domenuClanConfig();
			break;
		case 140071:
			GameScreen.indexRow = 0;
			GameScreen.indexSelect = 0;
			scrMain.clear();
			isViewClanMemOnline = !isViewClanMemOnline;
//			sortClan();
			break;
		case 140072:
			GameScreen.indexRow = 0;
			GameScreen.indexSelect = 0;
			scrMain.clear();
			isSortClanByPointWeek = !isSortClanByPointWeek;
//			sortClan();
			break;
		case 14008:
			GameCanvas.startYesNoDlg(mResources.MOVE_OUT_CLAN1, new Command(mResources.YES, 140081), new Command(mResources.NO, 0001));
			break;
		case 140081:
//			Service.gI().outClan();
			GameCanvas.endDlg();
			break;
		case 14009:
//			domenuClanPermission();
			break;
		case 140091:
//			Service.gI().changeClanType(((Member) vClan.elementAt(indexRow)).name, Clan.TYPE_TOCPHO);
			break;
		case 140092:
//			Service.gI().changeClanType(((Member) vClan.elementAt(indexRow)).name, Clan.TYPE_TRUONGLAO);
			break;
		case 140093:
			GameCanvas.startYesNoDlg(mResources.CLEAR_CLAN_TYPE, new Command(mResources.YES, 1400931), new Command(mResources.NO, 0001));
			break;
		case 140094: // Trục xuất
			GameCanvas.startYesNoDlg(mResources.MOVE_OUT_CLAN, new Command(mResources.YES, 1400941), new Command(mResources.NO, 0001));
			break;
		case 1400931:
//			Service.gI().changeClanType(((Member) vClan.elementAt(indexRow)).name, Clan.TYPE_NORMAL);
			GameCanvas.endDlg();
			break;
		case 1400941:
//			Service.gI().moveOutClan(((Member) vClan.elementAt(indexRow)).name);
			GameCanvas.endDlg();
			break;
		case 140095:
			Member m = (Member) vClan.elementAt(indexRow);
//			Service.gI().inviteClanDun(m.name);
			break;

		case 140096:
			menu = new mVector();
			menu.addElement(new Command(mResources.INVITE_THIS_PERSON, 1400961));
			menu.addElement(new Command(mResources.INVITE_ALL, 1400962));
			GameCanvas.menu.startAt(menu, 0);
			break;
		case 1400961:
			Member m1 = (Member) vClan.elementAt(indexRow);
//			Service.gI().inviteClanBattlefield(m1.name);
			break;
		case 1400962:
//			Service.gI().inviteClanBattlefieldAll();
			break;
		case 14010:
			GameCanvas.inputDlg.show(mResources.INPUT_CLAN_CONTRIBUTE, new Command(mResources.ACCEPT, GameCanvas.instance, 88833, null),
					TField.INPUT_TYPE_NUMERIC);
			break;
		case 140101:
			item = new Item();
			item.template = ItemTemplates.get((short) 0);
			item.expires = -1;
			updateItemInfo(Item.UI_CLAN, item);
			break;
		case 14011:
			isMessageMenu = false;
			break;
		case 14012:
//			actConvert_HanhTrang();
			break;
		case 14013:
			actConvertLeft();
			break;

		case 140131:
			GameCanvas.startYesNoDlg(mResources.CONFIRMCONVERT, new Command(mResources.YES, 140132), new Command(mResources.NO, 0001));
			break;
		case 140132:
//			Convert();
			break;
		case 14014:
//			actConvertMoveOut();
			break;
		case 14015:
//			actItemConvertMoveOut();
			break;
		case 140151:
//			actItemConvertMoveOut2();
			break;
		case 14016:
			updateItemInfo(Item.UI_BAG, arrItemConvert[indexSelect]);
			break;
		case 140161:
			updateItemInfo(Item.UI_BAG, arrItemConvert[2]);
			break;
		case 14017:
			domenuEnemieLeft();
			break;
		case 14018: // CLAN STORE VIEW
//			actView(Item.UI_CLANSHOP);
			break;
		case 14019: // CLAN STORE BUY
			actClanStoreBuy();
			break;
		case 140191:
//			actBuy(Item.UI_CLANSHOP);
			break;
		case 140192:
			actBuys(Item.UI_CLANSHOP);
			break;
		case 14020:
//			if (isHaveVanBienLenh()) {
//				
//				strTemp = (String) p;
//				//for(int i = 0; i < vFriend.size(); i++){
//					//Friend f = (Friend) vFriend.elementAt(i);
//				Service.gI().textBoxId((short) 1, strTemp);
//					//Service.gI().textBoxId((short) 1, f.friendName);
//				//}
//			
//			} else {
//				GameCanvas.startOKDlg(mResources.ALERT_MOVETO);
//			}
			break;
		case 14021:
//			actSetDunListCommand();
			break;
		case 14022:
			actEliteShopBuy();
			break;
		case 140221:
//			actBuy(Item.UI_ELITES);
			break;
		case 140222:
			actBuys(Item.UI_ELITES);
			break;
		case 14023:
//			actView(Item.UI_ELITES);
			break;

		case 1500:
			menu = new mVector();
			menu.addElement(new Command(mResources.MOVEOUT, 15001));
			if (Char.myChar().xu >= 5000)
				menu.addElement(new Command(mResources.SALE, 15002));
			GameCanvas.menu.startAt(menu, 1);
			break;
		case 15001:
			doStandToBag();
			break;
		case 15002:
//			int price;
//			try {
//				price = Integer.parseInt(tfText.getText());
//				if (price <= 0) {
//					GameCanvas.startOKDlg(mResources.INVALID_NUM);
//				}
//				GameCanvas.startYesNoDlg(mResources.replace(mResources.SALE_ASK, NinjaUtil.numberToString(price + "")), new Command(mResources.YES,
//						150021), new Command(mResources.NO, 0001));
//
//			} catch (Exception e) {
//				GameCanvas.startOKDlg(mResources.INVALID_NUM);
//			}
			break;
		case 150021:
			GameCanvas.startWaitDlg();
//			price = 0;
			try {
//				price = Integer.parseInt(tfText.getText());
			} catch (Exception e) {
			}
//			Service.gI().sendToSaleItem(itemSell, price);
			break;
		case 1501:
			updateItemInfo(Item.UI_BAG, itemSell);
			break;
		case 1502:
			tfText.doChangeToTextBox();
			break;
		case 1503:
			doBagToStand();
			break;
		case 1504:
			domenuItemStand();
			break;
		case 15041:
			domenuSortItemStand();
			break;
		case 150411:
			typeSortLevel = 0;
			typeSortName = 0;
			if (typeSortPrice == 0)
				typeSortPrice = 1;
			else if (typeSortPrice == 1)
				typeSortPrice = 2;
			else if (typeSortPrice == 2)
				typeSortPrice = 1;
			sortItemStand();
			break;
		case 150412:
			typeSortLevel = 0;
			typeSortPrice = 0;
			if (typeSortName == 0)
				typeSortName = 1;
			else if (typeSortName == 1)
				typeSortName = 2;
			else if (typeSortName == 2)
				typeSortName = 1;
			sortItemStand();
			break;
		case 150413:
			typeSortPrice = 0;
			typeSortName = 0;
			if (typeSortLevel == 0)
				typeSortLevel = 1;
			else if (typeSortLevel == 1)
				typeSortLevel = 2;
			else if (typeSortLevel == 2)
				typeSortLevel = 1;

			sortItemStand();
			break;
		case 15042:
// Service.gI().buyItemAuction(arrItemStands[indexSelect].item.itemId);
			GameCanvas.startYesNoDlg(mResources.replace(mResources.BUY_ASK, Util.numberToString(arrItemStands[indexSelect].price + "")),
					new Command(mResources.YES, 150421), new Command(mResources.NO, 0001));
			break;
		case 150421:
			GameCanvas.endDlg();
//			Service.gI().buyItemAuction(arrItemStands[indexSelect].item.itemId);
			break;
		case 1505:
			updateItemInfo(Item.UI_BAG, arrItemStands[indexSelect].item);
			break;
		case 1506:
			if (arrItemSprin != null) {
				yenTemp = 0;
				isPaintItemInfo = false;
				indexCard = -1;
				arrItemSprin = null;
				GameScreen.gI().left = new Command(mResources.SELECT, 1506);
			} else {
				indexCard = indexSelect;
//				Service.gI().selectCard();
				GameCanvas.startWaitDlgWithoutCancel();
			}
			break;
		case 1507:
//			doviewCardsInfo();
			break;
		case 1508:
			menu = new mVector();
			item = Char.clan.items[indexSelect];
			if(item!=null){
				if (item.template.id == 281)
					menu.addElement(new Command(mResources.USE, 15081));
				else
					menu.addElement(new Command(mResources.CLAN_BOX, 15082));
				GameCanvas.menu.startAt(menu, 0);
			}
			break;
		case 15081:
//			Service.gI().useClanItem();
			break;
		case 15082:
			GameCanvas.inputDlg.show(mResources.INPUT_MEM_NAME, new Command(mResources.OK, GameCanvas.instance, 88843, new Integer(indexSelect)),
					TField.INPUT_TYPE_ANY);
			break;
		case 1509:
			if (indexSelect >= 0 && Char.clan != null)
				updateItemInfo(Item.UI_CLAN, Char.clan.items[indexSelect]);
			else
				isPaintItemInfo = false;
			break;
		case 1510:
//			doAutoSelect();
			break;
		case 1511:
			strTemp = GameCanvas.inputDlg.tfInput.getText();
			GameCanvas.endDlg();
			try {
				if(strTemp.equals(""))
					GameCanvas.startOKDlg(mResources.INVALID_NUM);
				else{
					int value = Integer.valueOf(strTemp).intValue();
					
					if(value< 10 || value > 90)
						GameCanvas.startOKDlg(mResources.INVALID_NUM);
					else{
						Char.aHpValue = value;
					}
				}
			} catch (Exception e) {
				GameCanvas.startOKDlg(mResources.INVALID_NUM);
				// TODO: handle exception
			}
			break;
		case 1512:
			strTemp = GameCanvas.inputDlg.tfInput.getText();
			GameCanvas.endDlg();
			try {
				if(strTemp.equals(""))
					GameCanvas.startOKDlg(mResources.INVALID_NUM);
				else{
					int value = Integer.valueOf(strTemp).intValue();
					
					if(value< 10 || value > 90)
						GameCanvas.startOKDlg(mResources.INVALID_NUM);
					else{
						Char.aMpValue = value;
					}
				}
			} catch (Exception e) {
				GameCanvas.startOKDlg(mResources.INVALID_NUM);
				// TODO: handle exception
			}
			break;
		case 151301:
//			Service.gI().sendCatkeo(Char.myChar().mobFocus.getTemplate().mobTemplateId);
			break;
		case 15130:
			Char.aFoodValue = 1;
			break;
		case 15131:
			Char.aFoodValue = 10;
			break;
		case 15132:
			Char.aFoodValue = 20;
			break;
		case 15133:
			Char.aFoodValue = 30;
			break;
		case 15134:
			Char.aFoodValue = 40;
			break;
		case 15135:
			Char.aFoodValue = 50;
			break;
		case 15136:
			Char.aFoodValue = 60;
			break;
		case 15137:
			Char.aFoodValue = 70;
			break;
		case 1515:
			updateItemInfo(Item.UI_MON, currentCharViewInfo.arrItemMounts[indexSelect]);
			break;
		case 1516:
//			Service.gI().itemMonToBag(indexSelect);
			break;
			
		case 1600:
//			doLuyenThach();
			break;
		case 1601:
			actLuyenThachSelect();
			break;
		case 1602:
			item = getItemFocus(Item.UI_LUYEN_THACH);
			updateItemInfo(Item.UI_BAG, item);
			break;
		case 1603:
//			actgetLuyenThachItems();
			break;
		case 1604:
			menu = new mVector();
			if(arrItemSplit[indexSelect] != null)
				menu.addElement(new Command(mResources.MOVEOUT, 1605));
			menu.addElement(new Command(mResources.BEGIN, 11105));
			GameCanvas.menu.startAt(menu, 0);
			break;
		case 1605:
//			actTinhLuyenMoveOut();
			break;
		case 1606:
//			actDichChuyenTrangBiLeft();
			break;
		case 999:
//			doOpenUI(Item.UI_ELITES);
			break;
		}
	}
	private void sortItemStand() {
		if (typeSortLevel == 0 && typeSortPrice == 0 && typeSortName == 0)
			return;
		for (int i = 0; i < arrItemStands.length - 1; i++) {
			for (int j = i + 1; j < arrItemStands.length; j++) {
				if (typeSortPrice == 1) {
					if (arrItemStands[i].price < arrItemStands[j].price) {
						ItemStands tItem = arrItemStands[i];
						arrItemStands[i] = arrItemStands[j];
						arrItemStands[j] = tItem;
					}
				} else if (typeSortPrice == 2) {
					if (arrItemStands[i].price > arrItemStands[j].price) {
						ItemStands tItem = arrItemStands[i];
						arrItemStands[i] = arrItemStands[j];
						arrItemStands[j] = tItem;
					}
				}

				if (typeSortName == 1) {
					if (!arrItemStands[i].item.template.name.equals(arrItemStands[j].item.template.name)) {
						if (arrItemStands[i].item.template.name.compareTo(arrItemStands[j].item.template.name) > 0) {

							ItemStands tItem = arrItemStands[i];
							arrItemStands[i] = arrItemStands[j];
							arrItemStands[j] = tItem;
						}
					}
				} else if (typeSortName == 2) {
					if (!arrItemStands[i].item.template.name.equals(arrItemStands[j].item.template.name)) {
						if (arrItemStands[i].item.template.name.compareTo(arrItemStands[j].item.template.name) < 0) {

							ItemStands tItem = arrItemStands[i];
							arrItemStands[i] = arrItemStands[j];
							arrItemStands[j] = tItem;
						}
					}
				}

				if (typeSortLevel == 1) {
					if (arrItemStands[i].item.template.level < arrItemStands[j].item.template.level) {
						ItemStands tItem = arrItemStands[i];
						arrItemStands[i] = arrItemStands[j];
						arrItemStands[j] = tItem;
					}
				} else if (typeSortLevel == 2) {
					if (arrItemStands[i].item.template.level > arrItemStands[j].item.template.level) {
						ItemStands tItem = arrItemStands[i];
						arrItemStands[i] = arrItemStands[j];
						arrItemStands[j] = tItem;
					}
				}
			}
		}

	}

	private void domenuSortItemStand() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.SORT_BY_PRICE, 150411));
		menu.addElement(new Command(mResources.SORT_BY_NAME, 150412));
		menu.addElement(new Command(mResources.SORT_BY_LEVEL, 150413));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void domenuItemStand() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.SORT, 15041));
		menu.addElement(new Command(mResources.BUY, 15042));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void doStandToBag() {
		Char.myChar().arrItemBag[itemSell.indexUI] = itemSell;
		itemSell = null;
		left = center = null;
	}

	private void doBagToStand() {
		Item item = Char.myChar().arrItemBag[indexSelect];
		if (item == null)
			return;
		if (item.isLock || item.isExpires) {
			GameCanvas.startOKDlg(mResources.ONLY_NO_EXPIRE);
		} else {
			if (itemSell == null) {
				itemSell = Char.myChar().arrItemBag[indexSelect];
				Char.myChar().arrItemBag[indexSelect] = null;
			} else {
				item = Char.myChar().arrItemBag[indexSelect];
				Char.myChar().arrItemBag[indexSelect] = null;
				Char.myChar().arrItemBag[itemSell.indexUI] = itemSell;
				itemSell = item;
			}
		}

	}

	private void actClanStoreBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 140191));
		menu.addElement(new Command(mResources.BUYS, 140192));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void domenuErrorAdim() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BAG_CODE, null, 120081, null));
		menu.addElement(new Command(mResources.CARD, null, 12007, null));
		GameCanvas.menu.startAt(menu, 0);

	}

	int step = 0;
	String strErrCard = "";

	private void doShowErrorInput() {
		if (step == 1) {
			GameCanvas.input2Dlg.setTitle(mResources.CARD_TYPE, mResources.CARD_MONEY);
			GameCanvas.input2Dlg.show(mResources.INPUT_CARDTYPE_MONEY, new Command(mResources.CLOSE, GameCanvas.gI(), 8882, null), new Command(
					mResources.NEXT, null, 120071, null), TField.INPUT_TYPE_ANY, TField.INPUT_TYPE_ANY);
		} else {
			GameCanvas.input2Dlg.setTitle(mResources.CARD_SERI, mResources.CARD_TIME);
			GameCanvas.input2Dlg.show(mResources.INPUT_CARDSERI_TIME, new Command(mResources.CLOSE, GameCanvas.gI(), 8882, null), new Command(
					mResources.OK, null, 120072, null), TField.INPUT_TYPE_ANY, TField.INPUT_TYPE_ANY);
		}
	}

	

	private void domenuClanConfig() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.CLAN_MENU[0] + ": " + (isViewClanMemOnline ? mResources.OFF : mResources.ON), 140071));
		menu.addElement(new Command(mResources.CLAN_SORTTYPE, 140072));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void domenuEnemieLeft() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.CLAN_MENU[0] + ": " + (isViewClanMemOnline ? mResources.OFF : mResources.ON), 140071));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void domenuFriendLeft() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.ADDNEW, 110441));
		menu.addElement(new Command(mResources.CLAN_MENU[0] + ": " + (isViewClanMemOnline ? mResources.OFF : mResources.ON), 140071));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void domenuPrivateLock() {
		mVector mnuPrivateLock = new mVector();
		if (typeActive == 0)
			mnuPrivateLock.addElement(new Command(mResources.ENABLE, 11000671));
		else if (typeActive == 1)
			mnuPrivateLock.addElement(new Command(mResources.UNLOCK, 11000672));
		if (typeActive == 1 || typeActive == 2) {
			mnuPrivateLock.addElement(new Command(mResources.CANCEL_PRIVATE_PROTECT, 11000674));
			mnuPrivateLock.addElement(new Command(mResources.CHANGE_PRIVATE_PASS, 11000673));
		}

		GameCanvas.menu.startAt(mnuPrivateLock, 0);

	}

	private void domenuTeamLeft() {
		mVector menu = new mVector();

		menu.addElement(new Command(mResources.CREATE_TEAM, 110471));
		menu.addElement(new Command(mResources.ACTIONS[4], 1100061));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void domenuFindTeam() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.REFRESH, 110452));
		if (vPtMap.size() > 0)
			menu.addElement(new Command(mResources.INPUT_TEAM, 110451));
		GameCanvas.menu.startAt(menu, 3);
	}
	

	
	
	private boolean isHaveChucTet(){ // item chúc tết
		for(int i = 0; i < Char.myChar().arrItemBag.length; i++){
			Item item = (Item) Char.myChar().arrItemBag[i];
			if(item != null){
				if(item.template.id == 214 || item.template.id == 215)
					return true;
			}
		}
		return false;
	}

	private void actSetClanCommand(String mName) {

		mVector subPermission = new mVector();
		subPermission.addElement(new Command(mResources.CHAR_ORDER[6], 110805));
		subPermission.addElement(new Command(mResources.CHAT, 12002, mName));
		subPermission.addElement(new Command(mResources.PT, 110791, mName));
		subPermission.addElement(new Command(mResources.MOVETO, 14020, mName));
		subPermission.addElement(new Command(mResources.TEAMLEADER_SELECT[2], 110803, mName));
		GameCanvas.menu.startAt(subPermission, 3);

	}

	

	private void domenuClan() {
		mVector subMenu = new mVector();
		subMenu.addElement(new Command(mResources.CLAN[0], 11000661));
		subMenu.addElement(new Command(mResources.CLAN[1], 11000662));
		subMenu.addElement(new Command(mResources.CLAN[2], 11000663));
		subMenu.addElement(new Command(mResources.CLAN[3], 11000664));

		GameCanvas.menu.startAt(subMenu, 3);
	}

	private void doUnfocusChar() {
		Char.myChar().charFocus = null;
		Char.isManualFocus = false;
		cLastFocusID = -1;
		isPaintCharInMap = false;
		resetButton();

	}

	private void autoFocus() {

		if (cLastFocusID >= 0 && vCharInMap.size() > 0) {
			int cIndex = Char.getIndexChar(cLastFocusID);
			if (cIndex >= 0 && cIndex < vCharInMap.size()) {
				Char cFocus = (Char) vCharInMap.elementAt(cIndex);
				if (cFocus != null) {
					if (Char.isCharInScreen(cFocus)/*&&!cFocus.isNhanban()*/) {
						Char.myChar().mobFocus = null;
						Char.myChar().deFocusNPC();
						Char.myChar().itemFocus = null;
						Char.myChar().isManualFocus = true;
						Char.myChar().charFocus = cFocus;
					}
				}
			} else {
				cLastFocusID = -1;
				Char.myChar().charFocus = null;
			}
		} else {
			cLastFocusID = -1;
		}
	}

	private void doCharFocusList() {
		Char cFocus = (Char) vCharInMap.elementAt(indexRow);
		if(!cFocus.isNhanban()){
			cLastFocusID = cFocus.charID;
			Char.myChar().mobFocus = null;
			Char.myChar().deFocusNPC();
			Char.myChar().itemFocus = null;
			Char.myChar().isManualFocus = true;
			isPaintCharInMap = false;
			Char.myChar().charFocus = cFocus;
		}
		resetButton();
	}

	private void changeTaskInfo() {
		if (indexMenu == 0)
			indexMenu = 1;
		else
			indexMenu = 0;
		indexRow = 0;
		idTypeTask = indexMenu;
	}

	

	private void actBuyItemLeft2() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.SORT, 110221));
		menu.addElement(new Command(mResources.GETINMONEY, 11050));
		GameCanvas.menu.startAt(menu, 3);
	}

	private void actBuyItemUILeft() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.SORT, 11048));
		menu.addElement(new Command(mResources.GETOUTMONEY, 11049));
		GameCanvas.menu.startAt(menu, 3);
	}
	
	

	

	
	private void actLuyenThachSelect(){
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.MOVEOUT, 111071));
		for (int i = 0; i < arrItemUpPeal.length; i++) {
			if (arrItemUpPeal[i] != null) {
				menu.addElement(new Command(mResources.BEGIN, 1600));
				break;
			}
		}
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actUpPearlSelect() {

		mVector menu = new mVector();
		menu.addElement(new Command(mResources.MOVEOUT, 111071));
		for (int i = 0; i < arrItemUpPeal.length; i++) {
			if (arrItemUpPeal[i] != null) {
				menu.addElement(new Command(mResources.BEGIN, 11062));
				break;
			}
		}
		GameCanvas.menu.startAt(menu, 0);

	}


	private void actSplitItem() {
		mVector menu = new mVector();
		menu.addElement(cmdSplitMoveOut);
		if (itemSplit != null)
			menu.addElement(new Command(mResources.BEGIN, 111031));
		GameCanvas.menu.startAt(menu, 0);
	}


	

	

	

	private void actUpgradeBag() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.MOVEOUT, 111001));
		if (itemUpGrade != null)
			for (int i = 0; i < arrItemUpGrade.length; i++) {
				if (arrItemUpGrade[i] != null) {
					menu.addElement(new Command(mResources.BEGIN, 110981));
					break;
				}
			}
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actConvertLeft() {
		mVector menu = new mVector();
		menu.addElement(cmdConvertMoveOut);
		for (int i = 0; i < arrItemConvert.length; i++) {
			if (((Item) arrItemConvert[i]) == null) {
				left = null;
				break;
			}
			if (i == arrItemConvert.length - 1)
				menu.addElement(new Command(mResources.BEGIN, 140131));
		}
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actUpgradeLeft() {
		mVector menu = new mVector();
		menu.addElement(cmdUpgradeMoveOut);
		for (int i = 0; i < arrItemUpGrade.length; i++) {
			if (arrItemUpGrade[i] != null) {
				menu.addElement(new Command(mResources.BEGIN, 110981));
				break;
			}
		}
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actBuyLeft(Item itemBuy) {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110921, itemBuy));
		menu.addElement(new Command(mResources.BUYS, 110922, itemBuy));
		GameCanvas.menu.startAt(menu, 0);

	}



	private void actTrangBiRight() {
		indexMenu = 0;
		isPaintInfoMe = false;
		left = menu;
		right = cmdFocus;
		center = null;
		System.gc();
		resetButton();
		doMenuInforMe();
	}

	private void actBuyQuanNam(final Item item) {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110851));
		menu.addElement(new Command(mResources.BUYS, 110852));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void actdoUpPoint() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.UPPOINT, 110841));
		menu.addElement(new Command(mResources.UPPOINTS, 110842));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void actTrangBiSelect() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.MOVETOBAG, 110821));
		GameCanvas.menu.startAt(menu, 3);
	}

	private void actdoGan() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.ASSIGN_KEY[0], 110811));
		menu.addElement(new Command(mResources.ASSIGN_KEY[1], 110812));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actSetPartyCommand(final String partyName) {
		mVector subPermission = new mVector();
		subPermission.addElement(new Command("Xin vào nhóm", 155555555));
		subPermission.addElement(new Command("Mời vào nhóm", 166666666));
//		subPermission.addElement(new Command("Rời", 231291));
//		subPermission.addElement(new Command("Giải tán", 191210));
//		subPermission.addElement(new Command(mResources.TEAMLEADER_SELECT[0], 110801));
//		subPermission.addElement(new Command(mResources.TEAMLEADER_SELECT[1], 110802));
//		subPermission.addElement(new Command(mResources.TEAMLEADER_SELECT[2], 110803, partyName));
//		subPermission.addElement(new Command(mResources.CHAR_ORDER[7], 12002, partyName));
//		subPermission.addElement(new Command(mResources.CHAR_ORDER[6], 110804));
		
		GameCanvas.menu.startAt(subPermission, 3);

	}

	private void actdoKyNang() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.UPPOINT, 140011));
		menu.addElement(new Command(mResources.UPPOINTS, 140012));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actSetDeleteFriend(final String friendName) {
		GameCanvas
				.startYesNoDlg(mResources.CONFIRM_REMOVE_FRIEND, new Command(mResources.YES, 1107921, friendName), new Command(mResources.NO, 0001));

	}

	private void actOpenWebCancel() {
		isPaintAlert = false;
		textsTitle = null;
		texts = null;
		center = null;
		resetButton();
	}

	private void actOpenWeb(String url) {
//		Util.downloadGame(url);
		isPaintAlert = false;
		textsTitle = null;
		texts = null;
		center = null;
		resetButton();
	}



	private void actdoMiniInfo() {
		indexMenu = 0;
		isPaintInfoMe = false;
		resetButton();
		if (currentCharViewInfo == Char.myChar())
			doMenuInforMe();

	}


	private void doActionSaleOk(final Item item) {
		String text = GameCanvas.inputDlg.tfInput.getText();
		if (text.trim().equals("")) {

			return;
		}
		int quantity = 0;
		try {
			quantity = Integer.parseInt(text);
		} catch (Exception e) {
			GameCanvas.inputDlg.hide();
			return;
		}
		if (quantity <= 0) {
			GameCanvas.inputDlg.hide();
			return;
		}
		if (quantity > item.quantity) {
			GameCanvas.startOKDlg(mResources.NOT_ENOUGH_QUANTITY);
			return;
		}
		GameCanvas.inputDlg.hide();
		GameCanvas.startYesNoDlg(mResources.CONFIRMSALEITEM, new Command(mResources.YES, 11058, item), new Command(mResources.NO, 0001));
	}

	

	
	private void doItemChangeMap(final Item itemBag) {
		mVector vsub = new mVector();
		for (int i = 1; i < mResources.TELEPORT[3].length; i++) {
			vsub.addElement(new Command(mResources.TELEPORT[3][i], 110531, itemBag));
		}
		GameCanvas.menu.startAt(vsub, 3);
	}

	


	
//	private void actOpenAlertURL() {
//		try {
////			GameMidlet.instance.platformRequest(alertURL);
//		} catch (ConnectionNotFoundException e) {
//			e.printStackTrace();
//		}
//	}





	

	

	private void actionCoinTradeAccept() {
		String text = GameCanvas.inputDlg.tfInput.getText();
		if (text.trim().equals("")) {
			GameCanvas.inputDlg.hide();
			return;
		}
		int coin = 0;
		try {
			coin = Integer.parseInt(text);
		} catch (Exception e) {
			GameCanvas.inputDlg.hide();
			return;
		}
		if (coin <= 0) {
			GameCanvas.inputDlg.hide();
			return;
		}
		if (Char.myChar().xu == 0 || coin > Char.myChar().xu) {
			GameCanvas.startOKDlg(mResources.NOT_ENOUGH_COIN_IN1);
			return;
		}
		coinTrade += coin;
		Char.myChar().xu -= coin;
		GameCanvas.inputDlg.hide();

	}

	

	private void actsubMenuOrder() {
		mVector subPK = new mVector();
		subPK.addElement(new Command(mResources.PKS[0], 11000651));
		subPK.addElement(new Command(mResources.PKS[1], 11000652));
		subPK.addElement(new Command(mResources.PKS[3], 11000653));
		GameCanvas.menu.startAt(subPK, 3);

	}

	private void actOrder() {
		mVector sub = new mVector();
		sub.addElement(new Command(mResources.ACTIONS[6], 1100067));
		sub.addElement(new Command(mResources.ACTIONS[3], 1100062));
		sub.addElement(new Command(mResources.ACTIONS[1], 1100063));
		sub.addElement(new Command(mResources.ACTIONS[2], 1100064));
		sub.addElement(new Command(mResources.ACTIONS[0], 1100065));
		sub.addElement(new Command(mResources.ACTIONS[7], 1100068));

		GameCanvas.menu.startAt(sub, 0);

	}

	

	

	

	

	private void actMenu4() {
		GameCanvas.startYesNoDlg(mResources.DOYOUWANTEXIT2, new Command(mResources.YES, 1100041), new Command(mResources.NO, 0001));
	}

	private void actDead() {
		mVector deadMenu = new mVector();
		deadMenu.addElement(new Command(mResources.DIES[1], 110381));
		deadMenu.addElement(new Command(mResources.DIES[2], 110382));
		deadMenu.addElement(new Command(mResources.DIES[3], 110383));
		GameCanvas.menu.startAt(deadMenu, 3);
	}

	

	

	private void actTradeSelectInBag() {
		mVector v = new mVector();
		v.addElement(cmdTradeSelectItem);
		v.addElement(cmdTradeSendMoney);
		GameCanvas.menu.startAt(v, 0);

	}

	private void actTradeSelectInList() {
		mVector v = new mVector();
		v.addElement(cmdTradeMoveOut);
		if (typeTrade == 0) {
			v.addElement(cmdTradeLock);
		} else if (typeTrade == 1 && typeTradeOrder >= 1 && timeTrade - System.currentTimeMillis() / 1000 <= 0) {
			v.addElement(cmdTradeAccept);
		}
		GameCanvas.menu.startAt(v, 0);

	}



	private void actBagSplitItem() {
		if (Char.myChar().arrItemBag[indexSelect] == null || Char.myChar().arrItemBag[indexSelect].quantity <= 1)
			return;
		GameCanvas.inputDlg.show(mResources.INPUT_NUMSPLIT, new Command(mResources.OK, GameCanvas.instance, 88835, indexSelect + ""),
				TField.INPUT_TYPE_NUMERIC);
	}
	private void actBagSelectItem() {
		mVector menu = new mVector();
		menu.addElement(cmdBagUseItem);
		menu.addElement(cmdBagThrowItem);
		if (Char.myChar().arrItemBag[indexSelect] != null && Char.myChar().arrItemBag[indexSelect].quantity > 1)
			menu.addElement(cmdBagSplitItem);
		menu.addElement(new Command(mResources.SORT, 110221));
		GameCanvas.menu.startAt(menu, 3);

	}

	private void actEliteShopBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 140221));
		menu.addElement(new Command(mResources.BUYS, 140222));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actStoreBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110201));
		menu.addElement(new Command(mResources.BUYS, 110202));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actStoreLockBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110181));
		menu.addElement(new Command(mResources.BUYS, 110182));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void actStoreFashionBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 130021));
		menu.addElement(new Command(mResources.BUYS, 130022));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actNonNamBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110161));
		menu.addElement(new Command(mResources.BUYS, 110162));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actNonNuBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110141));
		menu.addElement(new Command(mResources.BUYS, 110142));
		GameCanvas.menu.startAt(menu, 0);

	}

	private void actAoNamBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110121));
		menu.addElement(new Command(mResources.BUYS, 110122));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void actAoNuBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110101));
		menu.addElement(new Command(mResources.BUYS, 110102));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void actGangTayNamBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110081));
		menu.addElement(new Command(mResources.BUYS, 110082));
		GameCanvas.menu.startAt(menu, 0);
	}

	private void actGangTayNuBuy() {
		mVector menu = new mVector();
		menu.addElement(new Command(mResources.BUY, 110051));
		menu.addElement(new Command(mResources.BUYS, 110052));
		GameCanvas.menu.startAt(menu, 0);
	}



	private void actBuys(byte itembuys) {
		Item item = getItemFocus(itembuys);
		actionBuy(item);
	}
	

	public void perform(int idAction, Object p) {
		Cout.println2222(idAction+" GameScreen perform "+p);
		switch (idAction) {
		case 1000:
			try {

				String[] _contend = ((String)p).split(",");
				short[] contend = new short[_contend.length];
				for (int i = 0; i < _contend.length; i++) {
					contend[i] = Short.parseShort(_contend[i]);
				}
				if(contend.length>=3)
				Service.gI().ActionDialogYesNo((byte)1,contend[0],(byte)contend[1],contend[2],(byte) 0);
			} catch (Exception e) {
				// TODO: handle exception
			}
			GameCanvas.endDlg();
			break;
		case 1001:
			try {

				String[] _contend = ((String)p).split(",");
				short[] contend = new short[_contend.length];
				for (int i = 0; i < _contend.length; i++) {
					contend[i] = Short.parseShort(_contend[i]);
				}
				if(contend.length>=2)
				Service.gI().ActionDialogYesNo((byte)1,contend[0],(byte)contend[1],contend[2],(byte)1);
			} catch (Exception e) {
				// TODO: handle exception
			}
			GameCanvas.endDlg();
			break;
		case 10:
			MenuObject menu3=(MenuObject)p;
			Npc npc = null;
			for (int i = 0; i < vNpc.size(); i++) {
				Npc dem = (Npc)vNpc.elementAt(i);
				if(dem!=null&&menu3!=null&&dem.npcId==menu3.idActor)
					npc = dem;
			}
			if(npc!=null){
				npc.NhiemVu(false);
			}
			Cout.println(getClass(), "Chon nhiem vu");
			
			break;
		}
		if (idAction == 9999)
			GameCanvas.instance.resetToLoginScr();
		
	}
	public void onCancelChat() {
		// TODO Auto-generated method stub
		
	}

	//chat world
	public void onChatFromMe(String text, String to) {
		if (!isPaintMessage || GameCanvas.isTouch) {
			ChatTextField.gI().isShow = false;
		}
		if (text.equals(""))
			return;
		if (to.equals(mResources.PUBLICCHAT[0]))
			Service.gI().chat(text,(byte) 0);
//		else if (to.equals(mResources.PARTYCHAT[0])) {
//			if (vParty.size() == 0)
//				ChatManager.gI().getCurrentChatTab().addInfo(mResources.NOT_IN_PARTY);
//			else
//				Service.gI().chatParty(text);
		 else if (to.equals(mResources.GLOBALCHAT[0]))
			Service.gI().chatGlobal(text,(byte) 1);
//		else if (to.equals(mResources.CLANCHAT[0])) {
//			if (Char.myChar().cClanName.equals(""))
//				ChatManager.gI().getCurrentChatTab().addInfo(mResources.NOT_IN_CLAN);
//			else
//				Service.gI().chatClan(text);
//		} else {
//			ChatManager.gI().addChat(to, Char.myChar().cName, text);
//			Service.gI().chatPrivate(to, text);
//		}

	}
	
	private void paintWaypointArrow(mGraphics g) {

		int x, y, a = 10;
		for (int i = 0; i < TileMap.vGo.size(); i++) {
			Waypoint way = (Waypoint) TileMap.vGo.elementAt(i);
//			Cout.println(" ----> "+way.minX+" "+way.minY+" "+(way.maxX - way.minX));
//			g.setColor(0xffffff);
//			g.drawRect(way.minX, way.minY, way.maxX - way.minX, 20);
//			if (way.minY == 0 || way.maxY >= TileMap.pxh - 24) {
//				if (way.maxY <= TileMap.pxh / 2) {
//					x = way.minX + (way.maxX - way.minX) / 2;
//					y = way.minY + (way.maxY - way.minY) / 2 + runArrow;
//					if (GameCanvas.isTouch)
//						y = way.maxY + (way.maxY - way.minY) + runArrow + a;
//					SmallImage.drawSmallImage(g, 1213, x, y, 6, StaticObj.VCENTER_HCENTER);
//				} else if (way.minY >= TileMap.pxh / 2) {
//					SmallImage.drawSmallImage(g, 1213, way.minX + (way.maxX - way.minX) / 2, way.minY - 12 - runArrow, 4, StaticObj.VCENTER_HCENTER);
//
//				}
//			} else {
//				if (way.maxX <= TileMap.pxw / 2)
//					if (!GameCanvas.isTouch)
//						SmallImage.drawSmallImage(g, 1213, way.maxX + 12 + runArrow, way.maxY - 12, 2, StaticObj.VCENTER_HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 1213, way.maxX + 12 + runArrow, way.maxY - 32, 2, StaticObj.VCENTER_HCENTER);
//				else if (way.minX >= TileMap.pxw / 2) {
//					if (!GameCanvas.isTouch)
//						SmallImage.drawSmallImage(g, 1213, way.minX - 12 - runArrow, way.maxY - 12, 0, StaticObj.VCENTER_HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 1213, way.minX - 12 - runArrow, way.maxY - 32, 0, StaticObj.VCENTER_HCENTER);
//				}
//			}

//			g.drawRegion(LoadImageInterface.imgXinCho, 0, 16*((GameCanvas.gameTick/2)%3), 16, 16, 0,
//					way.minX+(way.maxX - way.minX)/2, way.minY+10, mGraphics.VCENTER|mGraphics.HCENTER, false);
//			g.drawImage(LoadImageInterface.imgMap, way.minX+(way.maxX - way.minX)/2, way.minY+10, mGraphics.VCENTER|mGraphics.HCENTER);
			if(way.name!=null)
				mFont.tahoma_7.drawString(g, way.name, way.minX+(way.maxX - way.minX)/2, way.minY+2+ LoadImageInterface.imgXinCho.getHeight()/3, 2);
		}
	}
	
	private void paintItemFrame(mGraphics g, String[] titles, boolean paintMoney) {
		GameCanvas.paint.paintFrame(popupX, popupY, popupW, popupH, g);
		if (paintMoney) {

			mFont.tahoma_7_white.drawString(g, mResources.XU + ": " + Util.numberToString(String.valueOf(Char.myChar().xu)), popupX + 6, popupY
					+ popupH - 26, 0);
			mFont.tahoma_7_white.drawString(g, mResources.YEN + ": " + Util.numberToString(String.valueOf(Char.myChar().yen)), popupX + popupW
					- 6, popupY + popupH - 26, 1);
			if (isPaintTrade) {
				if (GameCanvas.gameTick % 10 > 4)
					mFont.tahoma_7_yellow.drawString(g, mResources.SELECTTRADE, popupX + popupW / 2, popupY + popupH - 14, 2);
			} else if (isPaintUpPearl) {
				if (GameCanvas.gameTick % 10 > 4)
					mFont.tahoma_7_yellow.drawString(g, mResources.SELECTPEARL, popupX + popupW / 2, popupY + popupH - 14, 2);
// } else if (isPaintUpGrade) {
// if (GameCanvas.gameTick % 10 > 4)
// Font.tahoma_7_yellow.drawString(g, Resources.SELECT_ITEM_AND_PEARL, popupX + popupW / 2, popupY + popupH - 14, 2);
			} else if (isPaintSplit) {
				if (GameCanvas.gameTick % 10 > 4)
					mFont.tahoma_7_yellow.drawString(g, mResources.SELECT_ITEM, popupX + popupW / 2, popupY + popupH - 14, 2);
			} else
				mFont.tahoma_7_yellow.drawString(g, mResources.LUONG + ": " + Util.numberToString(String.valueOf(Char.myChar().luong)), popupX
						+ popupW / 2, popupY + popupH - 14, 2);
		}
		paintTitle(g, titles[indexMenu], titles.length > 1);
		xstart = popupX + 3;
		ystart = popupY + 32;
		g.setColor(0x001919);
		g.fillRect(xstart - 1, ystart - 1, columns * indexSize + 3, 5 * indexSize + 3);
		//
	}
	
	public void paintHanhTrang(mGraphics g) {
		if (indexMenu != 0)
			return;
		paintBag(g, mResources.MENUME);
	}
	
	

	public void paintBag(mGraphics g, String[] titles) {
		try {
			resetTranslate(g);
			paintItemFrame(g, titles, true);
			paintBagItem(g, Char.myChar().arrItemBag);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void paintBagItem(mGraphics g, Item[] items) {
		rows = items.length / columns;

		scrMain.setStyle(rows, indexSize, xstart, ystart, columns * indexSize, 5 * indexSize, true, 6);
		scrMain.setClip(g, xstart, ystart, scrMain.width + 2, scrMain.height + 2);

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				SmallImage.drawSmallImage(g, 154, xstart + (j * indexSize) + indexSize / 2, ystart + (i * indexSize) + indexSize / 2, 0,
						mGraphics.VCENTER | mGraphics.HCENTER);
				g.setColor(0xBB6611);
				g.drawRect(xstart + (j * indexSize), ystart + (i * indexSize), indexSize, indexSize);
			}
		}

		for (int i = 0; i < items.length; i++) {
			Item item = items[i];
			if (item == null)
				continue;
			int r = item.indexUI / columns;
			int c = item.indexUI - (r * columns);
			paintItem(g, item, xstart + (c * indexSize), ystart + (r * indexSize));
			if (item.quantity > 1)
				mFont.number_yellow.drawString(g, String.valueOf(item.quantity), xstart + (c * indexSize) + indexSize, ystart + (r * indexSize)
						+ indexSize - mFont.number_yellow.getHeight(), 1);
		}
		if (indexTitle > 0 && indexSelect >= 0) {
			int r = indexSelect / columns;
			int c = indexSelect - (r * columns);
			g.setColor(0xffffff);
			g.drawRect(xstart + (c * indexSize), ystart + (r * indexSize), indexSize, indexSize);
			paintSelectHighlight(xstart + (c * indexSize), ystart + (r * indexSize), g);
		}
	}
	
	public void paintTrangbi(mGraphics g) {
		try{
			if (indexMenu != 4)
				return;
			g.translate(-g.getTranslateX(), -g.getTranslateY());
			GameCanvas.paint.paintFrame(popupX, popupY, popupW, popupH, g);
			g.setColor(Paint.COLORBACKGROUND);// nền nâu trong
			paintTitle(g, mResources.MENUME[indexMenu], true);
	
			if (currentCharViewInfo.arrItemBody == null) // Wait for other char load trang bi
			{
//				GameCanvas.paintShukiren(popupX + 90, popupY + 75, g, false);
				mFont.tahoma_7b_white.drawString(g, mResources.PLEASEWAIT, popupX + popupW / 2, popupY + 90, 2);
				return;
			}
	
			// g.fillRect(popupX + 33, popupY + (GameCanvas.isTouchControlLargeScreen ? 87 : 34), popupW - 67, (GameCanvas.isTouchControlLargeScreen ? 76 : 128));// viền
			g.setColor(0xcf9f38);// viền nâu
			g.drawRect(popupX + 33, popupY + (GameCanvas.isTouchControlLargeScreen ? 87 : 34), popupW - 67, (GameCanvas.isTouchControlLargeScreen ? 76
					: 128));// viền
			int blockSize = indexSize - 2;
	
			int t = 0;
			for (int i = 0; i < 13; i++) {
				if (i == 0 || i == 2 || i == 4 || i == 6 || i == 8) {
					g.setColor(0);
					g.fillRect(popupX + 4 + 1, popupY + 35 + i / 2 * blockSize + 1, blockSize - 1, blockSize - 1);
					if (mResources.ITEMNAME[i].length > 1) {
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][0], popupX + 7 + 22 / 2, popupY + 36 + i / 2 * blockSize + 2, 2);
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][1], popupX + 7 + 22 / 2, popupY + 36 + i / 2 * blockSize + 2 + 9, 2);
					} else
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][0], popupX + 7 + 22 / 2, popupY + 36 + i / 2 * blockSize + 2 + 5, 2);
				} else if (i == 1 || i == 3 || i == 5 || i == 7 || i == 9) {
					g.setColor(0);
					g.fillRect(popupX + popupW - blockSize - 4, popupY + 35 + i / 2 * blockSize + 1, blockSize - 1, blockSize - 1);
					if (mResources.ITEMNAME[i].length > 1) {
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][0], popupX + popupW - blockSize / 2 - 4, popupY + 36 + i / 2 * blockSize
								+ 2, 2);
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][1], popupX + popupW - blockSize / 2 - 4, popupY + 36 + i / 2 * blockSize
								+ 2 + 9, 2);
					} else
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][0], popupX + popupW - blockSize / 2 - 4, popupY + 36 + i / 2 * blockSize
								+ 2 + 5, 2);
				} else if (i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14 || i == 15) {
					int x = popupX + 4 + 1 + t * (blockSize + 2);
					int y = popupY + 35 + 5 * blockSize + 1;
					g.setColor(0);
					g.fillRect(x, popupY + 35 + 5 * blockSize + 1, blockSize - 1, blockSize - 1);
					if (mResources.ITEMNAME[i].length > 1) {
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][0], x + blockSize / 2, y + 2, 2);
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][1], x + blockSize / 2, y + 2 + 9, 2);
					} else
						mFont.tahoma_7_grey.drawString(g, mResources.ITEMNAME[i][0], x + blockSize / 2, y + 2 + 5, 2);
					t++;
				}
			}
			for (int i = 0; i < currentCharViewInfo.arrItemBody.length; i++) {
				Item item = currentCharViewInfo.arrItemBody[i];
				if (item != null) {
					if (item.eff == null)
						item.eff = GameScreen.efs[56];
					if (item.indexUI == 0 || item.indexUI == 2 || item.indexUI == 4 || item.indexUI == 6 || item.indexUI == 8 || item.indexUI == 10) {
						int x = popupX + 4;
						int y = popupY + 34 + item.indexUI / 2 * blockSize;
						// g.setColor(0);
						// g.fillRect(popupX + 4 + 1, popupY + 35 + i / 2 * blockSize + 1, blockSize - 1, blockSize - 1);
						paintItem(g, item, x - 1, y, 0, 1);
					} else if (item.indexUI == 1 || item.indexUI == 3 || item.indexUI == 5 || item.indexUI == 7 || item.indexUI == 9) {
						int x = popupX + popupW - blockSize - 5;
						int y = popupY + 35 + item.indexUI / 2 * blockSize;
	
						// g.setColor(0);
						// g.fillRect(popupX + popupW - blockSize - 4, popupY + 35 + i / 2 * blockSize + 1, blockSize - 1, blockSize - 1);
						paintItem(g, item, x - 1, y - 1, 0, 1);
					} else if (item.indexUI == 11 || item.indexUI == 12 || item.indexUI == 13 || item.indexUI == 14 || item.indexUI == 15) {
						if (item.indexUI == 10)
							t = 1;
						else if (item.indexUI == 11)
							t = 2;
						else if (item.indexUI == 12)
							t = 3;
						else if (item.indexUI == 13)
							t = 4;
						else if (item.indexUI == 14)
							t = 5;
						else if (item.indexUI == 15)
							t = 6;
						int x = popupX + 2 + 1 + t * (blockSize + 2) - blockSize;
						int y = popupY + 35 + 5 * blockSize;
						// g.setColor(0);
						// g.fillRect(popupX + 4 + ((item.indexUI - 10) * blockSize), popupY + 35 + 5 * blockSize + 1, blockSize - 1, blockSize - 1);
						paintItem(g, item, x - 2, y - 1, 0, 1);
					}
					if (GameCanvas.gameTick % 4 == 0) {
						item.indexEff++;
						if (item.indexEff >= item.eff.arrEfInfo.length)
							item.indexEff = 0;
					}
				}
			}
			for (int i = 0; i < 13; i++) {
				if (indexTitle == 1 && i == indexSelect) {
					if (i == 0 || i == 2 || i == 4 || i == 6 || i == 8) {
						g.setColor(0xffffff);
						g.drawRect(popupX + 4, popupY + 35 + i / 2 * blockSize, blockSize, blockSize);
						paintSelectHighlight(popupX + 5 - 2, popupY + 35 + i / 2 * blockSize - 1, g);
					} else if (i == 1 || i == 3 || i == 5 || i == 7 || i == 9) {
						g.setColor(0xffffff);
						g.drawRect(popupX + popupW - blockSize - 4 - 1, popupY + 35 + i / 2 * blockSize, blockSize, blockSize);
						paintSelectHighlight(popupX + popupW - blockSize - 4 - 2, popupY + 35 + i / 2 * blockSize - 1, g);
					} else if (i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14 || i == 15) {
						if (i == 9)
							t = 0;
						else if (i == 10)
							t = 1;
						else if (i == 11)
							t = 2;
						else if (i == 12)
							t = 3;
						else if (i == 13)
							t = 4;
						else if (i == 14)
							t = 5;
						else if (i == 15)
							t = 6;
						int x = popupX + 2 + 1 + t * (blockSize + 2) - blockSize;
						int y = popupY + 35 + 5 * blockSize;
						g.setColor(0xffffff);
						g.drawRect(x - 1, y, blockSize, blockSize);
						paintSelectHighlight(x - 2, y - 1, g);
					}
				}
			}
			int a = GameCanvas.isTouchControlLargeScreen ? -25 : 16;
			// Font.tahoma_7_yellow.drawString(g, Char.myChar().cName, gW2, gH2 + a - 45, 2);
			Part ph = GameScreen.parts[currentCharViewInfo.head], pl = GameScreen.parts[currentCharViewInfo.leg], pb = GameScreen.parts[currentCharViewInfo.body], pw = GameScreen.parts[currentCharViewInfo.wp];
//			if (currentCharViewInfo.arrItemBody != null && currentCharViewInfo.arrItemBody[Item.TYPE_MATNA] != null) {
//				ph = GameScreen.parts[currentCharViewInfo.arrItemBody[Item.TYPE_MATNA].template.part];
//			}
			if (ph.pi == null || ph.pi.length < 8) {
				ph = Char.myChar().getDefaultHead(Char.myChar().cgender);
			} else {
				for (int j = 0; j < ph.pi.length; j++) {
					if (ph.pi[j] == null || !SmallImage.isExitsImage(ph.pi[j].id)) {
						ph = Char.myChar().getDefaultHead(Char.myChar().cgender);
						break;
					}
				}
			}
			SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].id, gW2 + Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][1] + ph.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].dx, gH2 + a - Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][2] + ph.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].dy, 0, 0);
			SmallImage.drawSmallImage(g, pl.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].id, gW2 + Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][1] + pl.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dx, gH2 + a - Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][2] + pl.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dy, 0, 0);
			SmallImage.drawSmallImage(g, pb.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].id, gW2 + Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][1] + pb.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dx, gH2 + a - Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][2] + pb.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dy, 0, 0);
	//		currentCharViewInfo.paintClanEffect(g, gW2 + Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][1] + pb.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dx + 5, gH2 + a - Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][2] + pl.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dy + 5);
	//		currentCharViewInfo.paintClanEffect2(g,  gW2 + Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][1] + pb.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dx  + 22, gH2 + a - Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][2] + pl.pi[Char.CharInfo[currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dy + 5);
		}	catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
	}
	
	private void updateKeyUIInforMeTouch() { // update
		if (GameCanvas.menu.showMenu || GameCanvas.currentDialog != null)
			return;
		if (GameCanvas.isPointerJustRelease) {
			if (GameCanvas.isPointerHoldIn(popupX, popupY, popupW, Hitem) && (!isPaintItemInfo || GameCanvas.w >= 320)) {
				if (GameCanvas.isPointerClick) {
					if (GameCanvas.isPointerHoldIn(gW2 - 90, popupY + 5, 60, 40)) {
						indexSelect = 0;
						indexMenu--;

					}
					if (GameCanvas.isPointerHoldIn(gW2 + 20, popupY + 5, 60, 40)) {
						indexSelect = 0;
						indexMenu++;

					}
					isPaintItemInfo = false;
					scrMain.clear();
					scrInfo.clear();
					if (currentCharViewInfo != Char.myChar()) {
						if (indexMenu < 3)
							indexMenu = mResources.MENUME.length - 1;
						if (indexMenu > mResources.MENUME.length - 1)
							indexMenu = 3;
					} else {
						if (indexMenu < 0)
							indexMenu = mResources.MENUME.length - 1;
						if (indexMenu > mResources.MENUME.length - 1)
							indexMenu = 0;
					}
					indexTitle = 1;
					indexSelect = -1;
					doMiniMenuInforMe();
				}
			}
		}
		
//		if (GameCanvas.isPointerJustRelease) {
//			if (GameCanvas.isPointerHoldIn(popx, popx, popw, Hitem) && (!isPaintItemInfo || GameCanvas.w >= 320)) {
//				System.out.println("Update DDDDDDDDDDDDDDDDDDD");
//				if (GameCanvas.isPointerClick) {
//					if (GameCanvas.isPointerHoldIn(gW2 - 90, popy + 5, 60, 40)) {
//						indexSelect = 0;
//						indexMenu--;
//
//					}
//					if (GameCanvas.isPointerHoldIn(gW2 + 20, popy + 5, 60, 40)) {
//						indexSelect = 0;
//						indexMenu++;
//
//					}
//					isPaintItemInfo = false;
//					scrMain.clear();
//					scrInfo.clear();
//					if (currentCharViewInfo != Char.myChar()) {
//						if (indexMenu < 3)
//							indexMenu = mResources.MENUME.length - 1;
//						if (indexMenu > mResources.MENUME.length - 1)
//							indexMenu = 3;
//					} else {
//						if (indexMenu < 0)
//							indexMenu = mResources.MENUME.length - 1;
//						if (indexMenu > mResources.MENUME.length - 1)
//							indexMenu = 0;
//					}
//					indexTitle = 1;
//					indexSelect = -1;
//					doMiniMenuInforMe();
//				}
//			}
//		}

		if (isPaintItemInfo) { // ITEM INFO POPUP

			ScrollResult r = scrInfo.updateKey();
			if (r.isDowning || r.isFinish) {
				indexRow = r.selected;
				indexTitle = 1;
			}
			if (GameCanvas.isTouchControlSmallScreen)
				return;
		}

		if (indexMenu == 0) {// HÀNH TRANG
			ScrollResult r = scrMain.updateKey();
			if (r.isDowning || r.isFinish) {
				if (indexSelect != r.selected) {
					indexSelect = r.selected;
					left = center = null;
					if (GameCanvas.isTouchControlSmallScreen)
						setLCR();
					else {
						Item itemBag = getItemFocus(Item.UI_BAG);
						if (itemBag != null) {
							actBagViewItemInfo();
						} else {
							isPaintItemInfo = false;
							left = cmdBagSortItem;
						}
					}

				}
				indexTitle = 1;
			}

		} else if (indexMenu == 1) {// KỸ NĂNG

			ScrollResult r = scrMain.updateKey();
			if (r.isDowning || r.isFinish) {
				if (indexSelect != r.selected) {
					indexSelect = r.selected;
					if (indexSelect >= Char.myChar().nClass.skillTemplates.length)
						indexSelect = -1;
					left = center = null;

					setLCR();
					scrInfo.clear();
					indexRow = 0;
				}
				indexTitle = 1;
			} else {
				ScrollResult r2 = scrInfo.updateKey();
				if (r2.isDowning || r2.isFinish) {
					if (indexRow != r2.selected) {
						indexRow = r2.selected;
					}
				}
			}
		} else if (indexMenu == 2) {// TIỀM NĂNG

			if (GameCanvas.isPointerJustRelease) {

				if (GameCanvas.isPointerHoldIn(popupX + 5, popupY + 52, popupW - 10, 130)) {
					if (GameCanvas.isPointerClick) {
						int aa = (GameCanvas.py - (popupY + 52)) / 32;
						aa += 1;

						if (aa == indexTiemNang) {
							doUPPOINT();
						}
						indexTitle = aa;
						indexTiemNang = aa;
						setLCR();
					}
				}
			}

		} else if (indexMenu == 3) {// THÔNG TIN
			ScrollResult r = scrMain.updateKey();
			if (r.isDowning || r.isFinish) {
				indexRow = r.selected;
				indexTitle = 1;
			}

		} else if (indexMenu == 4) {// TRANG BỊ
			if (GameCanvas.isPointerJustRelease) {
				indexTitle = 1;
				if (GameCanvas.isPointerHoldIn(popupX + 4, popupY + 35, indexSize, 130)) {
					int aa = ((GameCanvas.py - (popupY + 35)) / indexSize);
					aa = aa * 2;
					indexSelect = aa;
					left = center = null;
					setLCR();
				}
				if (GameCanvas.isPointerHoldIn(popupX + popupW - 30, popupY + 35, indexSize, 130)) {
					int aa = ((GameCanvas.pyLast - (popupY + 35)) / indexSize);
					aa = aa * 2 + 1;
					indexSelect = aa;
					left = center = null;
					setLCR();
				}
				if (GameCanvas.isPointerHoldIn(popupX + 4, popupY + 165, popupW - 8, indexSize)) {
					int aa = ((GameCanvas.pxLast - (popupX + 4)) / indexSize);
					aa = 10 + aa;
					indexSelect = aa;
					left = center = null;
					setLCR();
				}
			}
		} else if (indexMenu == 5) {
			if (GameCanvas.isPointerJustRelease) {
				for (int i = 0; i < xMounts.length; i++) {
					if (i == 4) {
						if (GameCanvas.isPointerHoldIn(xMounts[i], yMounts[i], 84, 75) && GameCanvas.isPointerClick) {
							indexTitle = 1;
							indexSelect = 4;
							setLCR();
							if (!GameCanvas.isTouchControlSmallScreen && center!=null){
								actionPerform(center.idAction,  center.p);
							}
						}
					} else {
						if (GameCanvas.isPointerHoldIn(xMounts[i], yMounts[i], indexSize, indexSize) && GameCanvas.isPointerClick) {
							indexTitle = 1;
							indexSelect = i;
							setLCR();
							if (!GameCanvas.isTouchControlSmallScreen) {
								if (currentCharViewInfo.arrItemMounts[indexSelect] != null)
									actionPerform(center.idAction, center.p);
								else
									isPaintItemInfo = false;
							}
						}
					}
				}
			}
		}
	}
	
	public void doMiniMenuInforMe() {
		isPaintInfoMe = true;

		setPopupSize(175, 200);
		setLCR();
//		if (indexMenu == 3 && currentCharViewInfo.equals(Char.myChar())) {
//			Service.gI().viewInfo(currentCharViewInfo.cName, 0);
//		} 
		if (indexMenu == 5) {
			xMounts = new int[5];
			yMounts = new int[5];

			xstart = popupX + 5;
			ystart = popupY + 35;

			xMounts[0] = xstart + 5;
			yMounts[0] = ystart + 35;

			xMounts[1] = xstart + 5;
			yMounts[1] = ystart + 70;

			xMounts[2] = xstart + 131;
			yMounts[2] = ystart + 35;

			xMounts[3] = xstart + 131;
			yMounts[3] = ystart + 70;
			
			xMounts[4] = xMounts[0] + indexSize + 7;
			yMounts[4] = yMounts[0] - 5;
 		}
		right = new Command(mResources.BACK, 11060);
	}
	
	public void setLCR() {

		center = null;
		if (indexTitle == 0 && (indexMenu == 1 || indexMenu == 3 || indexMenu == 4)) {
			left = null;
			return;
		}
		switch (indexMenu) {
		case 0: // hành trang
			if (indexTitle == 1) {
				Item itemBag = getItemFocus(Item.UI_BAG);
				if (itemBag != null) {
					left = cmdBagSelectItem;
					if ((GameCanvas.isTouch && GameCanvas.w < 320) || !GameCanvas.isTouch)
						center = cmdBagViewItemInfo;
				} else {
					isPaintItemInfo = false;
					left = cmdBagSortItem;
				}
			}
			break;
		case 1: // Kỹ năng
//			System.out.println("indexTile ---> "+indexTitle);
//			System.out.println("index Select ---> "+indexSelect);
			if (indexTitle == 1) {
				left = null;
				if (indexSelect >= 0) {
					SkillTemplate skillTemplate = Char.myChar().nClass.skillTemplates[indexSelect];
					Skill skill = Char.myChar().getSkill(skillTemplate);
					if (skill != null) {
						if (skill.point < skillTemplate.maxPoint) {
							left = new Command(mResources.UPPOINT, 14001);
						}
						if (skill.template.type == Skill.SKILL_CLICK_USE_ATTACK || skill.template.type == Skill.SKILL_CLICK_LIVE
								|| skill.template.type == Skill.SKILL_CLICK_USE_BUFF || skill.template.type == Skill.SKILL_CLICK_NPC) {
							center = new Command(mResources.SHORCUT, 11081);
						} else
							center = null;
					}
				}
			}
			break;
		case 2: // Tiá»m nÄƒng
			if (indexTitle >= 1) {
				left = new Command(mResources.UPPOINT, 11084);
				center = new Command("", 11084);
			}
			break;
		case 3: // ThÃ´ng tin
			left = null;
			center = new Command(mResources.CHANGE, 110854);
			break;
		case 4: // Trang bá»‹
			if (indexTitle == 1) {
				left = null;
				final Item itemBody = getItemFocus(Item.UI_BODY);
				if (itemBody != null) {
					if (currentCharViewInfo == Char.myChar()) {
						left = new Command(mResources.SELECT, 11082);
						if (GameCanvas.isTouchControlLargeScreen) {
							updateItemInfo(Item.UI_BODY, itemBody);
						} else {
							center = new Command(mResources.VIEW, 11083);
						}
					} else {
						if (GameCanvas.isTouchControlLargeScreen) {
							updateItemInfo(Item.UI_BODY, itemBody);
						} else {
							center = new Command(mResources.VIEW, 11083);
						}
					}
				} else
					isPaintItemInfo = false;
			}
			break;
		case 5:
			left = null;
			if (indexTitle == 1 && indexSelect >=0 && currentCharViewInfo.arrItemMounts[indexSelect] != null) {
				if (Char.myChar().equals(currentCharViewInfo))
					left = new Command(mResources.MOVEOUT, 1516);
				center = new Command(GameCanvas.isTouchControlLargeScreen?"":mResources.VIEW, 1515);
			}
			break;
		}
		if (currentCharViewInfo == Char.myChar())
			right = new Command(mResources.BACK, 11086);
		else
			right = cmdCloseAll;
	}
	
	
	public void updateItemInfo(int typeUI, Item item) { // thong tin item
		if (item == null)
			return;
		this.itemFocus = item;
		inforW = 120;
		inforH = 120;
		if (GameCanvas.isTouch && !GameCanvas.isTouchControlSmallScreen)
			inforH += 18;
		isPaintItemInfo = true;
		scrInfo.clear();
		indexRow = 0;
		if (item.expires == 0) {
//			if(isPaintTinhluyen || isPaintDichChuyen)
//				Service.gI().requestItemInfo(item.typeUI, item.indexUI);
//			if (isPaintAuctionBuy)
//				Service.gI().requestItemAuction(item.itemId);
//			else if (currentCharViewInfo == Char.myChar())
//				Service.gI().requestItemInfo(typeUI, item.indexUI);
//			else
//				Service.gI().requestItemPlayer(currentCharViewInfo.charID, item.indexUI);
			
		}
		if (typeUI == Item.UI_BODY)
			Char.myChar().updateKickOption();

		if (!GameCanvas.isTouch || (GameCanvas.isTouch && GameCanvas.isTouchControlSmallScreen) || (isPaintInfoMe && indexMenu > 0 && indexMenu < 4)
				|| (isPaintClan && indexMenu == 0)) {
			center = cmdItemInfoClose;
			right = null;
			left = null;
		}
		GameCanvas.clearKeyHold();
		GameCanvas.clearKeyPressed();
	}
	
	private void actBagViewItemInfo() {
		if (indexTitle == 1) {
			Item itemBag = getItemFocus(Item.UI_BAG);
			if (GameCanvas.isTouchControlLargeScreen)
				updateItemInfo(Item.UI_BAG, itemBag, cmdBagUseItem, null);
			else
				updateItemInfo(Item.UI_BAG, itemBag, null, null);
		}

	}
	
	public void updateItemInfo(int typeUI, Item item, final Command left, final Command right) { // item su dung command
		updateItemInfo(typeUI, item);
		if (left != null)
			this.left = new Command(left.caption, 11040);
		if (right != null)
			this.right = new Command(right.caption, 11041);
	}
	
	public void updateKeyUIInforMe() {
		if (!isPaintInfoMe || indexMenu == -1 || GameCanvas.currentDialog != null)
			return;
		if ((isselectedRow >= 1 && indexMenu == 1))
			return;

		if (indexTitle == 0) {
			left = center = null;
			if (indexMenu == 0) {
				left = new Command(mResources.SORT, 110221);
			}
			if (GameCanvas.keyPressed[Key.NUM8]) {
				indexTitle = 1;
				indexSelect = 0;
				indexRow = -1; // bang 0
				scrMain.clear();
				scrInfo.clear();
			}
			if (GameCanvas.keyPressed[Key.NUM4]) {
				indexSelect = 0;
				indexRow = -1;
				indexMenu--;
				scrMain.clear();
				scrInfo.clear();

				if (currentCharViewInfo != Char.myChar()) {
					if (indexMenu < 3)
						indexMenu = 5;
				} else {
					if (indexMenu < 0)
						indexMenu = mResources.MENUME.length - 1;
				}
				doMiniMenuInforMe();
			}
			if (GameCanvas.keyPressed[Key.NUM6]) {
				indexSelect = 0;
				indexRow = -1;
				indexMenu++;
				scrMain.clear();
				scrInfo.clear();

				if (currentCharViewInfo != Char.myChar()) {
					if (indexMenu > 5)
						indexMenu = 3;
				} else {
					if (indexMenu > mResources.MENUME.length - 1)
						indexMenu = 0;
				}
				doMiniMenuInforMe();
			}
			setLCR();
		} else {
			if (isPaintItemInfo) // PAINT ITEMINFO POPUP
			{
				if (GameCanvas.keyPressed[Key.NUM2]) {
					indexRow--;
					if (indexRow < 0)
						indexRow = indexRowMax - 1;
					scrInfo.moveTo(indexRow * scrInfo.ITEM_SIZE);
				} else if (GameCanvas.keyPressed[Key.NUM8]) {
					indexRow++;
					if (indexRow >= indexRowMax)
						indexRow = 0;
					scrInfo.moveTo(indexRow * scrInfo.ITEM_SIZE);
				}

			} else if (indexMenu == 0) // HÀNH TRANG
			{
				if (GameCanvas.keyPressed[Key.NUM4]) {
					indexSelect--;
					if (indexSelect < 0)
						indexSelect = Char.myChar().arrItemBag.length - 1;
					left = center = null;
					setLCR();
					scrMain.moveTo((indexSelect / columns) * scrMain.ITEM_SIZE);
				} else if (GameCanvas.keyPressed[Key.NUM6]) {
					indexSelect++;
					if (indexSelect >= Char.myChar().arrItemBag.length)
						indexSelect = 0;
					left = center = null;
					setLCR();
					scrMain.moveTo((indexSelect / columns) * scrMain.ITEM_SIZE);
				} else if (GameCanvas.keyPressed[Key.NUM8]) {
					if (indexSelect + columns <= Char.myChar().arrItemBag.length - 1)
						indexSelect += columns;
					left = center = null;
					setLCR();
					scrMain.moveTo((indexSelect / columns) * scrMain.ITEM_SIZE);
				} else if (GameCanvas.keyPressed[Key.NUM2]) {
					if (indexSelect >= 0 && indexSelect < columns) {
						indexTitle = 0;
						indexSelect = 0;
					} else if (indexSelect - columns >= 0)
						indexSelect -= columns;
					left = center = null;
					setLCR();
					scrMain.moveTo((indexSelect / columns) * scrMain.ITEM_SIZE);
				}

			} else if (indexMenu == 1) // KỸ NĂNG
			{
				if (GameCanvas.keyPressed[Key.NUM2]) {
					if (indexTitle == 1 && indexRow == -1)
						indexTitle--;
					else if (indexTitle == 1 && indexRow >= 0)
						indexRow--;
					scrInfo.moveTo(indexRow * scrInfo.ITEM_SIZE);

				} else if (GameCanvas.keyPressed[Key.NUM8]) {
					if (indexTitle == 0)
						indexTitle++;
					else if (indexTitle == 1) {
						indexRow++;
						if (indexRow >= indexRowMax)
							indexRow = 0;
						scrInfo.moveTo(indexRow * scrInfo.ITEM_SIZE);
					}
					left = center = null;
					setLCR();

				} else if (GameCanvas.keyPressed[Key.NUM4]) {
					indexRow = -1;
					if (indexTitle == 1) {
						indexSelect--;
						if (indexSelect < 0)
							indexSelect = Char.myChar().nClass.skillTemplates.length - 1;
					}
					left = center = null;
					setLCR();
					scrMain.moveTo(indexSelect * scrMain.ITEM_SIZE);
					scrInfo.clear();
					indexRow = 0;

				} else if (GameCanvas.keyPressed[Key.NUM6]) {
					indexRow = -1;
					if (indexTitle == 1) {
						indexSelect++;

						if (indexSelect >= Char.myChar().nClass.skillTemplates.length)
							indexSelect = 0;
					}
					left = center = null;
					setLCR();
					scrMain.moveTo(indexSelect * scrMain.ITEM_SIZE);
					scrInfo.clear();
					indexRow = 0;
				}
			} else if (indexMenu == 2) // TIỀM NĂNG
			{
				if (GameCanvas.keyPressed[Key.NUM2]) {
					indexTitle--;
				} else if (GameCanvas.keyPressed[Key.NUM8]) {
					indexTitle++;
					if (indexTitle >= 5)
						indexTitle = 1;
					left = center = null;
					setLCR();
				}
			}

			else if (indexMenu == 3) // THÔNG TIN
			{
				if (indexRow < 0)
					indexRow = 0;
				if (GameCanvas.keyPressed[Key.NUM2]) {
					if (indexRow == 0) {
						indexTitle--;
						indexRow = -1;
					} else
						indexRow--;
					scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
				} else if (GameCanvas.keyPressed[Key.NUM8]) {
					indexRow++;
					if (indexRow >= indexRowMax)
						indexRow = 0;
					scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
				}
			} else if (indexMenu == 4) // TRANG BỊ
			{
				int oldIndex = indexSelect;
				if (indexSelect == 11 || indexSelect == 12 || indexSelect == 13 || indexSelect == 14) {
					if (GameCanvas.keyPressed[Key.NUM2] || (GameCanvas.keyPressed[Key.NUM4])) {
						indexSelect--;
					} else if (GameCanvas.keyPressed[Key.NUM6] || (GameCanvas.keyPressed[Key.NUM8])) {
						indexSelect++;
					}
				} else if (indexSelect == 9) {
					if (GameCanvas.keyPressed[Key.NUM2])
						indexSelect -= 2;
					else if (GameCanvas.keyPressed[Key.NUM8])
						indexSelect = 15;
					else if (GameCanvas.keyPressed[Key.NUM4])
						indexSelect--;
					else if (GameCanvas.keyPressed[Key.NUM6])
						indexSelect++;
				} else if (indexSelect == 10) {
					if (GameCanvas.keyPressed[Key.NUM2])
						indexSelect -= 2;
					else if (GameCanvas.keyPressed[Key.NUM4])
						indexSelect--;
					else if (GameCanvas.keyPressed[Key.NUM6] || GameCanvas.keyPressed[Key.NUM8])
						indexSelect++;
				} else if (indexSelect == 15) {
					if (GameCanvas.keyPressed[Key.NUM2])
						indexSelect = 9;
					else if (GameCanvas.keyPressed[Key.NUM4])
						indexSelect--;
					else if (GameCanvas.keyPressed[Key.NUM8] || GameCanvas.keyPressed[Key.NUM6])
						indexSelect = 0;
				} else {
					if (GameCanvas.keyPressed[Key.NUM2]) {
						if (indexSelect <= 1) {
							indexSelect = 0;
							indexTitle = 0;
						} else
							indexSelect -= 2;
					} else if (GameCanvas.keyPressed[Key.NUM8]) {
						indexSelect += 2;
						if (indexSelect > Item.TYPE_BODY_MAX)
							indexSelect = 0;
					} else if (GameCanvas.keyPressed[Key.NUM4]) {
						indexSelect--;
						if (indexSelect < 0)
							indexSelect = Item.TYPE_BODY_MAX;
					} else if (GameCanvas.keyPressed[Key.NUM6]) {
						indexSelect++;
						if (indexSelect > 11)
							indexSelect = 0;
					}
				}
				if (oldIndex != indexSelect) {
					left = center = null;
					setLCR();
				}
			} else if (indexMenu == 5) { // THÚ CƯỠI
					if (GameCanvas.keyPressed[Key.NUM2]) {
						if (indexSelect == 4) {
							indexSelect = 0;
							indexTitle--;
						} else {
							indexSelect--;
							if (indexSelect < 0) {
								indexSelect = 0;
								indexTitle--;
							}
						}
						setLCR();
					} else if (GameCanvas.keyPressed[Key.NUM4]) {
						if (indexSelect >= 2 && indexSelect !=4)
							indexSelect = 4;
						else 
							indexSelect = 0;
						setLCR();
					} else if (GameCanvas.keyPressed[Key.NUM6]) {
						if (indexSelect < 2)
							indexSelect = 4;
						else 
							indexSelect = 2;
						setLCR();
					} else if (GameCanvas.keyPressed[Key.NUM8]) {
						indexSelect++;
						if (indexSelect >= 4)
							indexSelect = 0;
						setLCR();
					}
			}
		}
		// ------------TOUCHSCREEN

		if (GameCanvas.isTouch)
			updateKeyUIInforMeTouch();
		GameCanvas.clearKeyHold();
		GameCanvas.clearKeyPressed();
	}
	
//	public void paintItemInfo(mGraphics g) {
//		if (GameCanvas.isTouchControlLargeScreen && !isSmallUI()) {
//			if (isOpenUI() || isPaintPopup() || isPaintUI()) {
//				setInfoFrameForLargeScreen();
//				paintInforFrame(g);
//				resetTranslate(g);
//				paintMultiLine(g, mFont.tahoma_7_white, mResources.CHOOSE_ITEM, inforX + inforW / 2, inforY + inforH / 2 - 20, 2);
//			}
//		}
//		if (!isPaintItemInfo || itemFocus == null || itemFocus.template == null) {
//			return;
//		}
//
//		Item itemInfo = itemFocus;
//		if (isViewNext && !itemFocus.isUpMax() && indexMenu == 0)
//			itemInfo = itemFocus.viewNext(itemFocus.upgrade + 1);
//		if (isPaintConvert && indexMenu == 0 && indexTitle == 1 && itemInfo.isTypeBody() && itemInfo.upgrade == 0 && arrItemConvert[0] != null
//				&& arrItemConvert[0].template.type == arrItemConvert[1].template.type
//				&& arrItemConvert[1].template.level >= arrItemConvert[0].template.level) {
//			itemInfo = itemFocus.viewNext(arrItemConvert[0].upgrade);
//		}
//		resetTranslate(g);
//		int maxW;
//		if (itemInfo.expires != 0 && itemInfo.options != null && itemInfo.options.size() > 0) {
//			for (int i = 0; i < itemInfo.options.size(); i++) {
//				ItemOption option = (ItemOption) itemInfo.options.elementAt(i);
//				maxW = option.getOptionString().length() * 5;
//				if (maxW > inforW && !GameCanvas.isTouchControlLargeScreen)
//					inforW = maxW;
//			}
//		}
//
//		maxW = mFont.tahoma_7b_white.getWidth(itemInfo.template.name) + 10;
//		if (maxW > inforW && !GameCanvas.isTouchControlLargeScreen)
//			inforW = maxW;
//
//		if (inforW > GameCanvas.w - 4)
//			inforW = GameCanvas.w - 4;
//		if (inforH > GameCanvas.h - 4)
//			inforW = GameCanvas.h - 4;
//
//		inforX = gW / 2 - inforW / 2;
//		inforY = gH / 2 - inforH / 2;
//
//		setInfoFrameForLargeScreen();
//
//		if (inforX < 2)
//			inforX = 2;
//		if (inforY < 2)
//			inforY = 2;
//
//		paintInforFrame(g);
//
//		if (isPaintClan && indexMenu == 0) {
//			if (Char.clan == null)
//				return;
//			yPaint = inforY - 9;
//			indexRowMax = 2;
//			scrInfo.setClip(g, inforX, inforY + 2, inforW, inforH - 2);
//			inforW = mFont.tahoma_7_white.getWidth(mResources.CLAN_ITEM[Char.clan.itemLevel][1]) + 10;
//			for (int i = 0; i < 2; i++) {
//				mFont.tahoma_7_white.drawString(g, mResources.CLAN_ITEM[Char.clan.itemLevel][i], inforX + 8, yPaint += 12, mFont.LEFT);
//			}
//			if (indexRow >= 0 && (!GameCanvas.isTouch || (GameCanvas.isTouch && GameCanvas.w < 320)))
//				SmallImage.drawSmallImage(g, 942, inforX + 1, inforY + 5 + indexRow * 12, 0, StaticObj.TOP_LEFT);
//			scrInfo.setStyle(indexRowMax, 12, inforX, inforY + 2, inforW, inforH - 4, true, 1);
//			return;
//		}
//
//		scrInfo.setClip(g, inforX, inforY + 2, inforW, inforH - 2);
//		indexRowMax = 3;
//		yPaint = inforY + 3;
//		mFont f = mFont.tahoma_7b_white;
//
//		if(itemInfo.isTypeMounts()){}
//		else if (itemInfo.upgrade >= 1 && itemInfo.upgrade < 4)
//			f = mFont.tahoma_7b_blue;
//		else if (itemInfo.upgrade >= 4 && itemInfo.upgrade < 8)
//			f = mFont.tahoma_7b_green;
//		else if (itemInfo.upgrade >= 8 && itemInfo.upgrade < 12)
//			f = mFont.tahoma_7b_yellow;
//		else if (itemInfo.upgrade >= 12 && itemInfo.upgrade < 15)
//			f = mFont.tahoma_7b_purple;
//		else if (itemInfo.upgrade >= 15)
//			f = mFont.tahoma_7b_red;
//
//		if (itemInfo.img != null) {
//			g.drawRegion(itemInfo.img, 0, 0, mGraphics.getImageWidth(itemInfo.img), mGraphics.getImageHeight(itemInfo.img), 0, inforX + inforW / 2,
//					yPaint + inforH - 10, Graphics.BOTTOM | Graphics.HCENTER);
//		}
//		if(itemInfo.isTypeMounts()){
//			paintMultiLine(g, f, itemInfo.template.name, inforX + 8, yPaint, mFont.LEFT);
//		}
//		else
//			paintMultiLine(g, f, itemInfo.template.name + (itemInfo.upgrade > 0 ? (" +" + itemInfo.upgrade) : ""), inforX + 8, yPaint, mFont.LEFT);
//		if (itemInfo.upgrade >= 15 && !isChop && !itemInfo.isTypeMounts()) {
//
//			String[] arr = f.splitFontArray(itemInfo.template.name + (itemInfo.upgrade > 0 ? (" +" + itemInfo.upgrade) : ""), inforW - ((GameCanvas.isTouch && GameCanvas.w >= 320) ? 20 : 10));
//			if (arr.length > 1)
//				yPaint -= 12;
//			if(itemInfo.isTypeMounts())
//				paintMultiLine(g, mFont.tahoma_7b_white, itemInfo.template.name, inforX + 8, yPaint, mFont.LEFT);
//			else
//				paintMultiLine(g, mFont.tahoma_7b_white, itemInfo.template.name + (itemInfo.upgrade > 0 ? (" +" + itemInfo.upgrade) : ""), inforX + 8,
//					yPaint, mFont.LEFT);
//		}
//		if (itemInfo.isTypeBody() && itemInfo.isTypeMounts()) {
//			yPaint += 12;
//			indexRowMax++;
//			if (isChop && GameCanvas.gameTick % 5 == 0)
//				isChop = !isChop;
//			else if (!isChop && GameCanvas.gameTick % 5 == 0)
//				isChop = !isChop;
//
//			int countStar = itemInfo.upgrade / 2 + 1;
//			if (itemInfo.upgrade == 0 || itemInfo.isTypeMounts()) {
//				for (int i = 0; i < countStar; i++) {
//					SmallImage.drawSmallImage(g, 633, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//			} else if (itemInfo.upgrade >= 1 && itemInfo.upgrade < 4) {
//				for (int i = 0; i < countStar; i++) {
//					SmallImage.drawSmallImage(g, 625, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//				if (itemInfo.upgrade == 3)
//					SmallImage.drawSmallImage(g, 635, inforX + 12 + countStar * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//			} else if (itemInfo.upgrade >= 4 && itemInfo.upgrade < 8) {
//				for (int i = 0; i < countStar; i++) {
//					SmallImage.drawSmallImage(g, 626, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//				if (itemInfo.upgrade % 2 != 0)
//					SmallImage.drawSmallImage(g, 636, inforX + 12 + countStar * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//			} else if (itemInfo.upgrade >= 8 && itemInfo.upgrade < 12) {
//				for (int i = 0; i < countStar; i++) {
//					if (isChop)
//						SmallImage.drawSmallImage(g, 627, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 628, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//				if (itemInfo.upgrade % 2 != 0) {
//					if (isChop)
//						SmallImage.drawSmallImage(g, 637, inforX + 12 + countStar * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 638, inforX + 12 + countStar * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//			} else if (itemInfo.upgrade >= 12 && itemInfo.upgrade < 15) {
//				for (int i = 0; i < countStar; i++) {
//					if (isChop)
//						SmallImage.drawSmallImage(g, 629, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 630, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//				if (itemInfo.upgrade % 2 != 0) {
//					if (isChop)
//						SmallImage.drawSmallImage(g, 639, inforX + 12 + countStar * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 640, inforX + 12 + countStar * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//			} else {
//				for (int i = 0; i < countStar; i++) {
//					if (isChop)
//						SmallImage.drawSmallImage(g, 631, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 632, inforX + 12 + i * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//				if (itemInfo.upgrade % 2 != 0) {
//					if (isChop)
//						SmallImage.drawSmallImage(g, 641, inforX + 12 + countStar * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//					else
//						SmallImage.drawSmallImage(g, 642, inforX + 12 + countStar * 10, yPaint + 5, 0, StaticObj.VCENTER_HCENTER);
//				}
//			}
//		}
//		mFont.tahoma_7_white.drawString(g, itemInfo.getLockString(), inforX + 8, yPaint += 12, mFont.LEFT);
////		if (itemInfo.isTypeBody() || itemInfo.isTypeMounts()) {
////			String strUp = itemInfo.getUpgradeString();
////			if (strUp != null) {
////				paintMultiLine(g, mFont.tahoma_7_white, strUp, inforX + 8, yPaint += 12, mFont.LEFT);
////				indexRowMax++;
////			}
////		}
//		if (itemInfo.template.gender == 0 || itemInfo.template.gender == 1) {
//			if (itemInfo.template.gender == Char.myChar().cgender) {
//				mFont.tahoma_7_white.drawString(g, mResources.GENDER[itemInfo.template.gender], inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			} else {
//				mFont.tahoma_7_red.drawString(g, mResources.GENDER[itemInfo.template.gender], inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			}
//		}
//		if (Char.myChar().clevel >= itemInfo.template.level)
//			paintMultiLine(g, mFont.tahoma_7_white, mResources.LEVELNEED + " " + itemInfo.template.level, inforX + 8, yPaint += 12, mFont.LEFT);
//		else
//			paintMultiLine(g, mFont.tahoma_7_red, mResources.LEVELNEED + " " + itemInfo.template.level, inforX + 8, yPaint += 12, mFont.LEFT);
//
//		if ((itemInfo.template.id >= 40 && itemInfo.template.id <= 48) || itemInfo.template.id == 311 || itemInfo.template.id == 375
//				|| itemInfo.template.id == 397) {
//			int classId = 1;
//			if (Char.myChar().nClass.classId == classId)
//				mFont.tahoma_7_white.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			else
//				mFont.tahoma_7_red.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		} else if ((itemInfo.template.id >= 49 && itemInfo.template.id <= 57) || itemInfo.template.id == 312 || itemInfo.template.id == 376
//				|| itemInfo.template.id == 398) {
//			int classId = 2;
//			if (Char.myChar().nClass.classId == classId)
//				mFont.tahoma_7_white.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			else
//				mFont.tahoma_7_red.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		} else if ((itemInfo.template.id >= 58 && itemInfo.template.id <= 66) || itemInfo.template.id == 313 || itemInfo.template.id == 377
//				|| itemInfo.template.id == 399) {
//			int classId = 3;
//			if (Char.myChar().nClass.classId == classId)
//				mFont.tahoma_7_white.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			else
//				mFont.tahoma_7_red.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		} else if ((itemInfo.template.id >= 67 && itemInfo.template.id <= 75) || itemInfo.template.id == 314 || itemInfo.template.id == 378
//				|| itemInfo.template.id == 400) {
//			int classId = 4;
//			if (Char.myChar().nClass.classId == classId)
//				mFont.tahoma_7_white.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			else
//				mFont.tahoma_7_red.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		} else if ((itemInfo.template.id >= 76 && itemInfo.template.id <= 84) || itemInfo.template.id == 315 || itemInfo.template.id == 379
//				|| itemInfo.template.id == 401) {
//			int classId = 5;
//			if (Char.myChar().nClass.classId == classId)
//				mFont.tahoma_7_white.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			else
//				mFont.tahoma_7_red.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		} else if ((itemInfo.template.id >= 85 && itemInfo.template.id <= 93) || itemInfo.template.id == 316 || itemInfo.template.id == 380
//				|| itemInfo.template.id == 402) {
//			int classId = 6;
//			if (Char.myChar().nClass.classId == classId)
//				mFont.tahoma_7_white.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			else
//				mFont.tahoma_7_red.drawString(g, mResources.CLASSNEED + " " + nClasss[classId].name, inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		}
//
//		if(itemInfo.isTypeMounts()){}
//		else{
//			if (itemInfo.template.id == 420) {
//				if (Char.myChar().nClass.classId == 1 || Char.myChar().nClass.classId == 2)
//					mFont.tahoma_7_white.drawString(g, mResources.SYSITEM[1], inforX + 8, yPaint += 12, mFont.LEFT);
//				else
//					mFont.tahoma_7_red.drawString(g, mResources.SYSITEM[1], inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			} else if (itemInfo.template.id == 421) {
//				if (Char.myChar().nClass.classId == 3 || Char.myChar().nClass.classId == 4)
//					mFont.tahoma_7_white.drawString(g, mResources.SYSITEM[2], inforX + 8, yPaint += 12, mFont.LEFT);
//				else
//					mFont.tahoma_7_red.drawString(g, mResources.SYSITEM[2], inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			} else if (itemInfo.template.id == 422) {
//				if (Char.myChar().nClass.classId == 5 || Char.myChar().nClass.classId == 6)
//					mFont.tahoma_7_white.drawString(g, mResources.SYSITEM[3], inforX + 8, yPaint += 12, mFont.LEFT);
//				else
//					mFont.tahoma_7_red.drawString(g, mResources.SYSITEM[3], inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			}
//		}
//		if (itemInfo.expires > 0) {
//			if (itemInfo.isTypeUIShop() || itemInfo.isTypeUIShopLock() || itemInfo.isTypeUIStore() || itemInfo.isTypeUIBook()
//					|| itemInfo.isTypeUIFashion() || itemInfo.isTypeUIClanShop() || itemInfo.isTypeUIClan()) {
//				maxW = mFont.tahoma_7.getWidth(mResources.EXPIRES + ": " + itemInfo.getExpiresShopString()) + 10;
//				if (maxW > inforW && !GameCanvas.isTouchControlLargeScreen)
//					inforW = maxW;
//				paintMultiLine(g, mFont.tahoma_7_yellow, mResources.EXPIRES + ": " + itemInfo.getExpiresShopString(), inforX + 8, yPaint += 12,
//						mFont.LEFT);
//			} else {
//				maxW = mFont.tahoma_7.getWidth(mResources.EXPIRES + ": " + itemInfo.getExpiresString()) + 10;
//				if (maxW > inforW && !GameCanvas.isTouchControlLargeScreen)
//					inforW = maxW;
//				paintMultiLine(g, mFont.tahoma_7_yellow, mResources.EXPIRES + ": " + itemInfo.getExpiresString(), inforX + 8, yPaint += 12,
//						mFont.LEFT);
//
//			}
//			indexRowMax++;
//		}
//		if (!itemInfo.template.description.equals("")) {
//			paintMultiLine(g, mFont.tahoma_7_white, itemInfo.template.description, inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		}
//	
//		if (itemInfo.isTypeUIMe() || itemInfo.typeUI == Item.UI_AUCTION_BUY) {
//			mFont.tahoma_7_yellow.drawString(g, mResources.replace(mResources.COIN_LOCK_SALE, Util.numberToString(String.valueOf(itemInfo.saleCoinLock))), inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		} else if (itemInfo.isTypeUIShop() || itemInfo.isTypeUIShopLock() || itemInfo.isTypeUIStore() || itemInfo.isTypeUIBook()
//				|| itemInfo.isTypeUIFashion() || itemInfo.isTypeUIClanShop()) {
//			if (itemInfo.buyCoin > 0) {
//				if (itemInfo.isTypeUIClanShop()) {
//					maxW = mFont.tahoma_7.getWidth(mResources.replace(mResources.COIN_BUY_CLAN, Util.numberToString(String.valueOf(itemInfo.buyCoin)))) + 10;
//					if (maxW > inforW && !GameCanvas.isTouchControlLargeScreen)
//						inforW = maxW;
//						//paintMultiLine(g, mFont.tahoma_7_yellow, mResources.replace(mResources.COIN_BUY_CLAN, NinjaUtil.numberToString(String.valueOf(itemInfo.buyCoin))), inforX + 8, yPaint += 12, mFont.LEFT);
//					mFont.tahoma_7_yellow.drawString(g, mResources.replace(mResources.COIN_BUY_CLAN, Util.numberToString(String.valueOf(itemInfo.buyCoin))), inforX + 8, yPaint += 12, mFont.LEFT);
//				} else
//					mFont.tahoma_7_yellow.drawString(g, mResources.replace(mResources.COIN_BUY, Util.numberToString(String
//							.valueOf(itemInfo.buyCoin))), inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			} else if (itemInfo.buyCoinLock > 0) {
//				mFont.tahoma_7_yellow.drawString(g, mResources.replace(mResources.COIN_LOCK_BUY, Util.numberToString(String
//						.valueOf(itemInfo.buyCoinLock))), inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			} else if (itemInfo.buyGold > 0) {
//				mFont.tahoma_7_yellow.drawString(g, mResources.replace(mResources.GOLD_BUY, Util
//						.numberToString(String.valueOf(itemInfo.buyGold))), inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			}
//		}
//		if (itemInfo.template.type == Item.TYPE_MON4) {
//			mFont.tahoma_7_yellow.drawString(g, mResources.Level + ": " + (itemInfo.upgrade + 1), inforX + 8, yPaint += 12, mFont.LEFT);
//			indexRowMax++;
//		}
//		if (itemInfo.isTypeBody()) {
//			if (itemInfo.sys != 0) {
//				mFont.tahoma_7_blue1.drawString(g, mResources.SYSITEM[itemInfo.sys], inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//			}
//		}
//		if (itemInfo.expires != 0 && itemInfo.options != null && itemInfo.options.size() > 0) {
//			boolean isKick = false, isUpgrade = false;
//			for (int i = 0; i < itemInfo.options.size(); i++) {
//				ItemOption option = (ItemOption) itemInfo.options.elementAt(i);
//				if (!isKick && option.optionTemplate.type == 2) {
//					isKick = true;
//					String titleHide = mResources.LINE[0] + ": ";
//					if (itemInfo.template.type == Item.TYPE_VUKHI) {
//						titleHide += mResources.BODY[itemInfo.template.type] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_NON) {
//						titleHide += mResources.BODY[Item.TYPE_QUAN] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_NHAN] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_QUAN) {
//						titleHide += mResources.BODY[Item.TYPE_NON] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_NHAN] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_NHAN) {
//						titleHide += mResources.BODY[Item.TYPE_NON] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_QUAN] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_AO) {
//						titleHide += mResources.BODY[Item.TYPE_GIAY] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_NGOCBOI] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_GIAY) {
//						titleHide += mResources.BODY[Item.TYPE_AO] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_NGOCBOI] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_NGOCBOI) {
//						titleHide += mResources.BODY[Item.TYPE_AO] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_GIAY] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_GANGTAY) {
//						titleHide += mResources.BODY[Item.TYPE_LIEN] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_PHU] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_LIEN) {
//						titleHide += mResources.BODY[Item.TYPE_GANGTAY] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_PHU] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					} else if (itemInfo.template.type == Item.TYPE_PHU) {
//						titleHide += mResources.BODY[Item.TYPE_GANGTAY] + "(" + mResources.KICKSYS[itemFocus.sys] + "), "
//								+ mResources.BODY[Item.TYPE_LIEN] + "(" + mResources.KICKSYS[itemFocus.sys] + ")";
//					}
//
//					maxW = mFont.tahoma_7_white.getWidth(titleHide) + 15;
//
//					if (maxW > inforW && !GameCanvas.isTouchControlLargeScreen) {
//						inforW = maxW;
//
//					}
//
//					paintMultiLine(g, mFont.tahoma_7_white, titleHide, inforX + 8, yPaint += 12, mFont.LEFT);
//					indexRowMax++;
//				}
//				if (!isUpgrade && option.optionTemplate.type > 2 && option.optionTemplate.type < 8) {
//					isUpgrade = true;
//					mFont.tahoma_7_white.drawString(g, mResources.LINE[1], inforX + 8, yPaint += 12, mFont.LEFT);
//					indexRowMax++;
//				}
//				if (option.optionTemplate.id == 65 ) {
//						paintMultiLine(g, mFont.tahoma_7_blue, (itemInfo.template.id == 485)? Util.replace(option.getOptionString(), mResources.EXP, mResources.MACHINE):option.getOptionString(), inforX + 8, yPaint += 12, mFont.LEFT);
//				} else 	if (option.optionTemplate.id == 66 ) {
//						paintMultiLine(g, mFont.tahoma_7_blue1, (itemInfo.template.id == 485)? Util.replace(option.getOptionString(), mResources.VITALITY, mResources.POWER):option.getOptionString(), inforX + 8, yPaint += 12, mFont.LEFT);
//				} else if (option.optionTemplate.type == 0)
//					paintMultiLine(g, mFont.tahoma_7_blue1, itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionString(),
//							inforX + 8, yPaint += 12, mFont.LEFT);
//				else if (option.optionTemplate.type == 1)
//					paintMultiLine(g, mFont.tahoma_7_green, itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionString(),
//							inforX + 8, yPaint += 12, mFont.LEFT);
//				else if (option.optionTemplate.type == 8){
//					if(option.optionTemplate.id == 85)
//						paintMultiLine(g, mFont.tahoma_7_yellow, itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionShopString1(),
//								inforX + 8, yPaint += 12, mFont.LEFT);
//					else
//						paintMultiLine(g, mFont.tahoma_7b_blue, itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionShopString(),
//								inforX + 8, yPaint += 12, mFont.LEFT);
//				}
//				else if ((option.optionTemplate.type == 2 && itemInfo.typeUI == Item.UI_BODY && option.active == 1)
//						|| (option.optionTemplate.type == 3 && itemInfo.upgrade >= 4) || (option.optionTemplate.type == 4 && itemInfo.upgrade >= 8)
//						|| (option.optionTemplate.type == 5 && itemInfo.upgrade >= 12) || (option.optionTemplate.type == 6 && itemInfo.upgrade >= 14)
//						|| (option.optionTemplate.type == 7 && itemInfo.upgrade >= 16))
//					paintMultiLine(g, mFont.tahoma_7_green, itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionString(), inforX + 8, yPaint += 12, mFont.LEFT);
//				else
//					paintMultiLine(g, mFont.tahoma_7_grey, itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionString(),
//							inforX + 8, yPaint += 12, mFont.LEFT);
//				indexRowMax++;
//
//			}
//		}
//		if (isPaintLuckySpin) {
//			if (itemInfo.template.id == 12)
//				mFont.tahoma_7_red.drawString(g, Util.numberToString(yenValue[indexSelect]) + " " + mResources.YEN, inforX + 8, yPaint += 12,
//						mFont.LEFT);
//			if (itemInfo.template.type >= 0 && itemInfo.template.type <= 9)
//				mFont.tahoma_7_yellow.drawString(g, mResources.MAX_OPTIONS, inforX + 8, yPaint += 12, mFont.LEFT);
//
//			indexRowMax++;
//		}
//
//		if (indexRow >= 0 && (!GameCanvas.isTouch || (GameCanvas.isTouch && GameCanvas.w < 320)))
//			SmallImage.drawSmallImage(g, 942, inforX + 1, inforY + 5 + indexRow * 12, 0, StaticObj.TOP_LEFT);
//		scrInfo.setStyle(indexRowMax, 12, inforX, inforY + 2, inforW, inforH - 4, true, 1);
//	}
	
	public void paintMultiLine(mGraphics g, mFont f, String arr[], int x, int y, int align) {
		int yTemp = y;
		for (int i = 0; i < arr.length; i++) {
			String s = arr[i];
			if (s.startsWith("c")) {
				if (s.startsWith("c0")) {
					s = s.substring(2);
					f = mFont.tahoma_7_white;
				} else if (s.startsWith("c1")) {
					s = s.substring(2);
					f = mFont.tahoma_7_yellow;
				} else if (s.startsWith("c2")) {
					s = s.substring(2);
					f = mFont.tahoma_7_green;
				}
			}
			if (i == 0)
				f.drawString(g, s, x, y, align);
			else {
// if(i < indexRow + 30 && i > indexRow - 30)
				if (i * scrMain.ITEM_SIZE + yTemp >= (scrMain.cmy - 12) && i * scrMain.ITEM_SIZE < scrMain.cmy + popupH - 44)
					f.drawString(g, s, x, y += 12, align);
				else
					y += 12;
				yPaint += 12;
				indexRowMax++;
			}
		}
	}

	public void paintMultiLine(mGraphics g, mFont f, String str, int x, int y, int align) {
		int a = (GameCanvas.isTouch && GameCanvas.w >= 320) ? 20 : 10;
		int yTemp = y;
		String[] arr = f.splitFontArray(str, inforW - a);
		for (int i = 0; i < arr.length; i++) {
			if (i == 0)
				f.drawString(g, arr[i], x, y, align);
			else {
				if (i * scrMain.ITEM_SIZE + yTemp >= (scrMain.cmy - 12) && i * scrMain.ITEM_SIZE < scrMain.cmy + popupH - 44) {
					f.drawString(g, arr[i], x, y += 12, align);
					yPaint += 12;
				} else
					y += 12;
				indexRowMax++;
			}
		}
	}

	public void paintMultiLine(mGraphics g, mFont f, String str, int x, int y, int align, int width) {
		int yTemp = y;
		String[] arr = f.splitFontArray(str, width);
		for (int i = 0; i < arr.length; i++) {
			if (i == 0)
				f.drawString(g, arr[i], x, y, align);
			else {
				if (i * scrMain.ITEM_SIZE + yTemp >= (scrMain.cmy - 12) && i * scrMain.ITEM_SIZE < scrMain.cmy + popupH - 44) {
					f.drawString(g, arr[i], x, y += 12, align);
					yPaint += 12;
				} else
					y += 12;
				indexRowMax++;
			}
		}
	}
	
	public static void paintTouchControl(mGraphics g) {
		try{
			resetTranslate(g);
			// ---CHAT
			g.drawImage(imgChat, xC + 17, yC + 17, mGraphics.HCENTER | mGraphics.VCENTER);
			
			
			// ---LEFT
//			g.drawImage(imgButton, xL, yL, 0);
			g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, mGraphics.getImageWidth(LoadImageInterface.imgMoveNormal), mGraphics.getImageHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_ROT180, xL + 21, yL + 16,
					mGraphics.HCENTER | mGraphics.VCENTER);
			if (keyTouch == 4) {
//				g.drawImage(imgButton2, xL, yL, 0);
				g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0, mGraphics.getImageWidth(LoadImageInterface.imgMoveNormal), mGraphics.getImageHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_ROT180, xL + 21, yL + 16, mGraphics.HCENTER | mGraphics.VCENTER);
			}

			// ----RIGHT
//			g.drawImage(imgButton, xR, yR, 0);
			g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, mGraphics.getImageWidth(LoadImageInterface.imgMoveNormal), mGraphics.getImageHeight(LoadImageInterface.imgMoveNormal), 0, xR + 30, yR + 16, mGraphics.HCENTER
					| mGraphics.VCENTER);
			if (keyTouch == 6) {
//				g.drawImage(imgButton2, xR, yR, 0);
				g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0, mGraphics.getImageWidth(LoadImageInterface.imgMoveFocus), mGraphics.getImageHeight(LoadImageInterface.imgMoveFocus), 0, xR + 30, yR + 16,
						mGraphics.HCENTER | mGraphics.VCENTER);
			}

			// ----UP
//			g.drawImage(imgButton, xU, yU, 0);
			g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, mGraphics.getImageWidth(LoadImageInterface.imgMoveNormal), mGraphics.getImageHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR_ROT90, xU + 24,
					yU + 20, mGraphics.HCENTER | mGraphics.VCENTER);
			if (keyTouch == 3) {
//				g.drawImage(imgButton2, xU, yU, 0);
				g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0, mGraphics.getImageWidth(LoadImageInterface.imgMoveFocus), mGraphics.getImageHeight(LoadImageInterface.imgMoveFocus), Sprite.TRANS_MIRROR_ROT90, xU + 24,
						yU + 20, mGraphics.HCENTER | mGraphics.VCENTER);
			}
			
			// CENTER
			g.drawImage(LoadImageInterface.imgMoveCenter, xL  + 44, yL + 14,  mGraphics.HCENTER | mGraphics.VCENTER);

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
//		if (!GameCanvas.isTouch || (GameCanvas.menu.showMenu && GameCanvas.isTouchControlSmallScreen))
//			return;
//		if (GameCanvas.currentDialog != null || ChatPopup.currentMultilineChatPopup != null || GameCanvas.menu.showMenu || isPaintPopup())
//			return;
		
	}
	
	private void updateKeyTouchControl() {

		boolean isPress = false;
		keyTouch = -1;
//		if (GameCanvas.isPointerHoldIn(TileMap.posMiniMapX, TileMap.posMiniMapY, TileMap.wMiniMap, TileMap.hMiniMap)) {
//			if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease) {
//				doShowMap();
//				isPress = true;
//			}
//		}
		if (!GameCanvas.isTouch ||MenuIcon.isShowTab|| (GameCanvas.menu.showMenu && GameCanvas.isTouchControlSmallScreen))
			return;
		if (GameCanvas.currentDialog != null || ChatPopup.currentMultilineChatPopup != null || GameCanvas.menu.showMenu || isPaintPopup())
			return;
		if (GameCanvas.isPointerHoldIn(xC, yC, 34, 34)) {
			keyTouch = 15;
			if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease) {
				ChatTextField.gI().startChat(this, mResources.PUBLICCHAT[0]);
				isPress = true;
			}
		}

//		if ( isNotPaintTouchControl())
//			return;
		if (GameCanvas.isPointerHoldIn(xU, yU, 34, 34)) {
			keyTouch = 3;
			GameCanvas.keyHold[2] = true;
			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown) {
			GameCanvas.keyHold[2] = false;
		}

		if (GameCanvas.isPointerHoldIn(xU - 30, yU, 30, 34)) {

			GameCanvas.keyHold[1] = true;
			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown)
			GameCanvas.keyHold[1] = false;

		if (GameCanvas.isPointerHoldIn(xU + 34, yU, 30, 34)) {

			GameCanvas.keyHold[3] = true;
			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown)
			GameCanvas.keyHold[3] = false;

		if (GameCanvas.isPointerHoldIn(xL, yL, 34, 34)) {
			keyTouch = 4;
			GameCanvas.keyHold[4] = true;
			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown)
			GameCanvas.keyHold[4] = false;

		if (GameCanvas.isPointerHoldIn(xR - 5, yR, 50, 34)) {
			keyTouch = 6;
			GameCanvas.keyHold[6] = true;
			resetAuto();
			isPress = true;
		} else if (GameCanvas.isPointerDown)
			GameCanvas.keyHold[6] = false;


		if (GameCanvas.isPointerHoldIn(xF, yF, 54, 54)) {
			keyTouch = 5;
			if (GameCanvas.isPointerJustRelease) {
				GameCanvas.keyPressed[5] = true;
				isPress = true;
			}
		}
	
		

		if (GameCanvas.isPointerJustRelease) {
			GameCanvas.keyHold[1] = false;
			GameCanvas.keyHold[2] = false;
			GameCanvas.keyHold[3] = false;
			GameCanvas.keyHold[4] = false;
			GameCanvas.keyHold[6] = false;

			// GameCanvas.keyHold[5] = false;

		}
		if (!isPress)
			doFocusbyTouch();

	}
	
	private void doViewMessagebyTouch() {
		int messageType = messageType();
		if (messageType == -1 || isPaintPopup() || isPaintUI() || isOpenUI())
			return;
		int pos;
		if (messageType == 0) {
			if (ChatManager.gI().waitList.size() > 0) {
				pos = ChatManager.gI().postWaitPerson();
				ChatManager.gI().switchToTab(pos);
				openUIChatTab();
				xM[0] = yM[0] = -1;
			}
		} else {
			if (ChatManager.isMessagePt) {
				ChatManager.gI().switchToTab(1);
			} else if (ChatManager.isMessageClan) {
				ChatManager.gI().switchToTab(3);
			}
			openUIChatTab();
			xM[1] = yM[1] = -1;
		}
	}
	
	private void openUIChatTab() { // mở tab chat
//		ChatTab currentTab = ChatManager.gI().getCurrentChatTab();
//		currentTab.type = 2;
//		ispaintChat = true;
//		isPaintMessage = true;
		TabChat.gI().switchToMe();
		isLockKey = true;
		setPopupSize(175, 200);
		left = center = null;
		//add to list chater(hien tai comment)
		
		
//		if(Char.toCharChat!=null)
//		{
//			boolean exitChater = false;
//			for(int i=0;i<ChatPrivate.vOtherchar.size();i++)
//			{
//				OtherChar otherChar=(OtherChar)ChatPrivate.vOtherchar.elementAt(i);
//				if(otherChar.id==Char.toCharChat.CharidDB)//co ton tai trong list chater
//				{
//					indexRow=i;
//					exitChater=true;
//				}
//			}
//			if(!exitChater)//truong hop khong ton tai trong danh list chater
//			{
//				ChatPrivate.AddNewChater(Char.toCharChat.CharidDB,Char.toCharChat.cName);
//				indexRow=ChatPrivate.vOtherchar.size()-1;
//			}
//		}
		
		//create button icon chat
//		bntIconChat = new Command("",Constants.BUTTON_ICON_CHAT);
//		bntIconChat.setPos( 55+ popupW, GameScreen.popupY + popupH - 33, LoadImageInterface.imgEmo[7],LoadImageInterface.imgEmo[7]);
//		
//		//create button sen chat
//		chat = new Command("Gửi",Constants.BUTTON_SEND);
//		chat.setPos(58+ popupW+bntIconChat.w  /*- GameScreen.imgLbtn.getWidth()*/ , GameScreen.popupY + popupH - 33, GameScreen.imgSkill,GameScreen.imgSkill);

		
		
		//		if (GameCanvas.h - popupH < 40 && !GameCanvas.isTouch)
//			popupH -= 52;
//		right = new Command(mResources.CLOSE, 11066);
//		left = center = null;
//
//		if (!GameCanvas.isTouch) {
//			doShowChatTextFieldInMessage();
//		} else {
//			left = new Command(mResources.CHAT, 12005);
//		}
//
//		if (currentTab.type == 2) {
//			center = new Command(mResources.HIDE_TAB, 120051, currentTab);
//		}
//
//		ChatTextField.gI().center = null;
//		textsTitle = currentTab.ownerName;
//		texts = currentTab.contents;
//		ChatManager.gI().removeFromWaitList(currentTab.ownerName);
//		if (currentTab.type == 1) { // CLAN CHAT
//			ChatManager.isMessagePt = false;
//		}
//		if (currentTab.type == 4) { // CLAN CHAT
//			ChatManager.isMessageClan = false;
//		}
//		scrollDownAlert(); // mới đóng 
		
	}
	
	public void doShowChatTextFieldInMessage() {
		ChatTab currentTab = ChatManager.gI().getCurrentChatTab();
		if (currentTab.type == 0)// public
			ChatTextField.gI().startChat(this, mResources.PUBLICCHAT[0]);
		if (currentTab.type == 1)// party
			ChatTextField.gI().startChat(this, mResources.PARTYCHAT[0]);
		if (currentTab.type == 2)// private
			ChatTextField.gI().startChat(this, currentTab.ownerName);
		if (currentTab.type == 3)// global
			ChatTextField.gI().startChat(this, mResources.GLOBALCHAT[0]);
		if (currentTab.type == 4)// clan
			ChatTextField.gI().startChat(this, mResources.CLANCHAT[0]);
	}
	
	public void scrollDownAlert() {
		indexRowMax = texts.size();
		scrMain.setStyle(indexRowMax, 12, popupX, ystart + 12, popupW, popupH - 42 - (textsTitle != null ? 10 : 0), true, 1);
		indexRow = texts.size() - 1;
		scrMain.moveTo((indexRow) * scrMain.ITEM_SIZE);
	}
	public void loadCmdBar() {
//		if (imgCmdBar == null) {
//			imgCmdBar = new Image[2];
//			for (int i = 0; i < 2; i++)
//				imgCmdBar[i] = GameCanvas.loadImage("/u/c" + i + ".png");
//		}
//		cmdBarLeftW = mGraphics.getImageWidth(imgCmdBar[0]);
//		cmdBarRightW = mGraphics.getImageWidth(imgCmdBar[1]);
//		cmdBarCenterW = gW - cmdBarLeftW - cmdBarRightW + 1;

		hpBarX = 78 - 15 + 10;
		hpBarY = cmdBarY;
		hpBarW = gW - 84 - 30;
		expBarW = gW - 44 - 4;
		hpBarH = 20;

//		if (GameCanvas.w > 176) {
//			cmdBarCenterW -= 50;
//			hpBarW -= 50;
//			expBarW -= 50;
//			hpBarX += 15;
//			hpBarW -= 15;
//		}
		loadInforBar();
	}
	
	private void loadInforBar() {
		if (!GameCanvas.isTouch)
			return;
		hpBarW = 82;
		mpBarW = 57;
		hpBarX = 52;
		hpBarY = 10 + Info.hI;
		expBarW = gW - 61;

		if (GameCanvas.isTouchControlSmallScreen) {
			xC = gW / 2 - 2;
			yC = yTouchBar + 50;

		} else {
			menu.y = 6 + Info.hI;
			xC = gW - 100;
			yC = 2 + Info.hI;
		}
//		TileMap.setPosMiniMap(GameCanvas.w - 60, Info.hI, 60, 42);

	}
	
	public boolean isNotPaintTouchControl() {
		if (!GameCanvas.isTouch || Char.ischangingMap || isPaintZone || isPaintAuto)
			return true;
		if (ChatTextField.gI().isShow)
			return true;
//		if (center == cmdDead && GameCanvas.isTouchControlLargeScreen)
//			return true;
		if (GameCanvas.currentDialog != null || ChatPopup.currentMultilineChatPopup != null || GameCanvas.menu.showMenu || isPaintPopup())
			return true;
		return false;
	}
	
	public void updateCommandForUI() {
		left = center = null;
		if (indexSelect < 0)
			return;
		
		if(isPaintAuto){
			if(indexTitle == 1 && !GameCanvas.isTouch){
				left = new Command(mResources.SELECT, 1510);
			}
			return;
		}
		if (isPaintLuckySpin) {
			if (indexTitle == 1) {
				left = new Command(mResources.SELECT, 1506);
				center = new Command("", 1507);
			}
			return;
		}
		if (isPaintEliteShop) {
			if (indexMenu == 0) {
				Item item = getItemFocus(Item.UI_ELITES);
				if (item != null) {
					left = cmdEliteShopBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdEliteShopView;
//					else
//						actView(Item.UI_ELITES);
				}
			}
			return;
		} else if (isPaintStore) {
			if (indexMenu == 0) {
				Item item = getItemFocus(Item.UI_STORE);
				if (item != null) {
					left = cmdStoreBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdStoreView;
//					else
//						actView(Item.UI_STORE);
				}
			} else if (indexMenu == 1) {
				Item item = getItemFocus(Item.UI_BOOK);
				if (item != null) {
					left = cmdStoreLockBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdStoreLockView;
//					else
//						actView(Item.UI_BOOK);
				}
			} else if (indexMenu == 2) {
				Item item = getItemFocus(Item.UI_FASHION);
				if (item != null) {
					left = cmdStoreFashionBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdStoreFashionView;
//					else
//						actView(Item.UI_FASHION);
				}
			} else if (indexMenu == 3) {
				Item item = getItemFocus(Item.UI_CLANSHOP);
				if (item != null) {
					left = cmdClanStoreBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdClanStoreView;
//					else
//						actView(Item.UI_CLANSHOP);
				}
			}
			return;
		} else if (isPaintNonNam) {
			if (indexMenu == 0) {
				Item item = getItemFocus(Item.UI_NONNAM);
				if (item != null) {
					left = cmdNonNamBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdNonNamView;
//					else
//						actView(Item.UI_NONNAM);
				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintNonNu) {
			if (indexMenu == 0) {
				Item item = getItemFocus(Item.UI_NONNU);
				if (item != null) {
					left = cmdNonNuBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdNonNuView;
//					else
//						actView(Item.UI_NONNU);
				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintAoNam) {
			if (indexMenu == 0) {
				Item item = getItemFocus(Item.UI_AONAM);
				if (item != null) {
					left = cmdAoNamBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdAoNamView;
//					else
//						actView(Item.UI_AONAM);
				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintAoNu) {
			if (indexMenu == 0) {
				Item item = getItemFocus(Item.UI_AONU);
				if (item != null) {
					left = cmdAoNuBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdAoNuView;
//					else
//						actView(Item.UI_AONU);
				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintGangTayNam) {
			if (indexMenu == 0) {
				Item item = getItemFocus(Item.UI_GANGTAYNAM);
				if (item != null) {
					left = cmdGangTayNamBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdGangTayNamView;
//					else
//						actView(Item.UI_GANGTAYNAM);
				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintGangTayNu) {
			if (indexMenu == 0) {
				Item item = getItemFocus(Item.UI_GANGTAYNU);
				if (item != null) {
					left = cmdGangTayNuBuy;
					if (!GameCanvas.isTouchControlLargeScreen)
						center = cmdGangTayNuView;
//					else
//						actView(Item.UI_GANGTAYNU);
				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintQuanNam) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_QUANNAM);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (!GameCanvas.isTouchControlLargeScreen) {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11088, item);
					} else
						updateItemInfo(Item.UI_QUANNAM, item);

				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintQuanNu) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_QUANNU);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (!GameCanvas.isTouchControlLargeScreen) {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11089);
					} else
						updateItemInfo(Item.UI_QUANNU, item);

				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintGiayNam) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_GIAYNAM);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (!GameCanvas.isTouchControlLargeScreen) {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11090);
					} else
						updateItemInfo(Item.UI_GIAYNAM, item);
				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintGiayNu) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_GIAYNU);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (!GameCanvas.isTouchControlLargeScreen) {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11091);
					} else
						updateItemInfo(Item.UI_GIAYNU, item);
				}
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintLien) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_LIEN);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_LIEN, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 110923);
					}
				} else
					isPaintItemInfo = false;
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintNhan) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_NHAN);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_NHAN, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 110924);
					}
				} else
					isPaintItemInfo = false;
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintNgocBoi) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_NGOCBOI);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_NGOCBOI, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 110925);
					}
				} else
					isPaintItemInfo = false;
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintPhu) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_PHU);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_PHU, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 110926);
					}
				} else
					isPaintItemInfo = false;
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintWeapon) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_WEAPON);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_WEAPON, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11093);
					}
				} else
					isPaintItemInfo = false;
			} else if (indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
				saleItem();
			}
		} else if (isPaintStack) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_STACK);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_STACK, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11094);
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null)
					saleItem();
				else
					left = cmdBagSortItem;
			}
		} else if (isPaintStackLock) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_STACK_LOCK);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_STACK_LOCK, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11095);
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null)
					saleItem();
				else
					left = cmdBagSortItem;
			}
		} else if (isPaintGrocery) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_GROCERY);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_GROCERY, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11096);
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null)
					saleItem();
				else {
					left = cmdBagSortItem;
				}
			}

		} else if (isPaintGroceryLock) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_GROCERY_LOCK);
				if (item != null) {
					left = new Command(mResources.BUY, 11092, item);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_GROCERY_LOCK, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11097);
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null)
					saleItem();
				else {
					left = cmdBagSortItem;

				}
			}
		} else if (isPaintUpGrade) {
			if (indexMenu == 0) {
				if (indexTitle == 1) {
					if (itemUpGrade != null) {
						if (indexSelect == 0) {
							left = new Command(mResources.SELECT, 11098);
							if (GameCanvas.isTouchControlLargeScreen) {
								isViewNext = false;
								updateItemInfo(Item.UI_BAG, itemUpGrade);
							} else {
								center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11099);
							}
						} else if (indexSelect == 1 && !itemUpGrade.isUpMax()) {
							if (GameCanvas.isTouchControlLargeScreen) {
								isViewNext = true;
								updateItemInfo(Item.UI_BAG, itemUpGrade);
							} else {
								center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 110991);
							}
						}
					} else
						isPaintItemInfo = false;
				} else if (indexTitle == 2) {
					final Item item = getItemFocus(Item.UI_UPGRADE);
					isViewNext = false;
					if (item != null) {
						left = new Command(mResources.SELECT, 11100);
						if (GameCanvas.isTouchControlLargeScreen)
							updateItemInfo(Item.UI_BAG, item);
						else {
							center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11101);
						}
					} else {
						left = null;
						isPaintItemInfo = false;
						if (itemUpGrade != null)
							for (int i = 0; i < arrItemUpGrade.length; i++) {
								if (arrItemUpGrade[i] != null) {
									left = new Command(mResources.BEGIN, 110981);
									break;
								}
							}
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null) {
					left = new Command(mResources.SELECT, 11102);
				} else {
					left = null;

				}
			}
		} else if (isPaintConvert) {
			if (indexMenu == 0) {
				if (indexTitle == 1) {
					if (indexSelect == 0) {
						if (arrItemConvert[0] != null) {
							left = new Command(mResources.SELECT, 14013);
							if (GameCanvas.isTouchControlLargeScreen)
								updateItemInfo(Item.UI_BAG, arrItemConvert[indexSelect]);
							else
								center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 14016);
						}
					} else if (indexSelect == 1) {
						if (arrItemConvert[1] != null) {
							left = new Command(mResources.SELECT, 14013);
							if (GameCanvas.isTouchControlLargeScreen) {
								updateItemInfo(Item.UI_BAG, arrItemConvert[indexSelect]);
							} else {
								center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 14016);
							}
						}
					} else
						isPaintItemInfo = false;
				} else if (indexTitle == 2) {
					Item item = null;
					int temp = indexSelect + 2;
					if (temp <= arrItemConvert.length - 1)
						item = arrItemConvert[temp];
					if (item != null) {
						left = new Command(mResources.MOVEOUT, 140151);
						if (GameCanvas.isTouchControlLargeScreen)
							updateItemInfo(Item.UI_BAG, item);
						else
							center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 140161);
					} else {
						left = new Command(mResources.BEGIN, 140131);
						for (int i = 0; i < arrItemConvert.length; i++) {
							if (((Item) arrItemConvert[i]) == null) {
								left = null;
								break;
							}
						}
						isPaintItemInfo = false;
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null) {
					left = new Command(mResources.SELECT, 14012);
				} else {
					left = null;
				}
			}
		} else if (isPaintAuctionSale) {
			if (indexMenu == 0) {
				if (indexTitle == 2) {
					tfText.isFocus = true;
					right = tfText.cmdClear;
				} else {
					tfText.isFocus = false;
					right = cmdCloseAll;
				}

				int price = 0;
				try {
					price = Integer.parseInt(tfText.getText());
				} catch (Exception e) {
				}

				if (itemSell != null && price > 0 && Char.myChar().xu >= 5000) {
					left = new Command(mResources.SALE, 15002);
				}
				if (indexTitle == 1) {
					if (itemSell != null) {
						left = new Command(mResources.SELECT, 1500);
						if (GameCanvas.isTouchControlLargeScreen)
							updateItemInfo(Item.UI_BAG, itemSell);
						else {
							center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 1501);
						}
					}
				}

			} else if (indexMenu == 1) {
				right = cmdCloseAll;
				if (Char.myChar().arrItemBag[indexSelect] != null) {
					left = new Command(mResources.SELECT, 1503);
				} else {
					left = null;
					isPaintItemInfo = false;
				}
			}
		} else if (isPaintAuctionBuy) {
			if (indexTitle == 1) {
				if (arrItemStands != null) {
					if (indexSelect >= 0 && indexSelect < arrItemStands.length) {
						ItemStands iStand = arrItemStands[indexSelect];
						if (iStand != null) {
							left = new Command(mResources.SELECT, 1504);
							if (GameCanvas.isTouchControlLargeScreen)
								actionPerform(1505, null);
							else {
								center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 1505);
							}
						}
					}
				}
			}
			return;
		} else if (isPaintSplit) {
			if (indexMenu == 0) {
				if (indexTitle == 1) {
					if (itemSplit != null && itemSplit.upgrade > 0) {
						left = new Command(mResources.SELECT, 11103);
					} else if (itemSplit != null) {
						left = cmdSplitMoveOut;
					} else
						isPaintItemInfo = false;

					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_BAG, itemSplit);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11104, itemSplit);
					}
				} else if (indexTitle == 2) {
					final Item item = arrItemSplit[indexSelect];
					if (item != null) {
						if (GameCanvas.isTouchControlLargeScreen)
							updateItemInfo(Item.UI_BAG, item);
						else {
							center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11104, item);
						}
					} else
						isPaintItemInfo = false;
					if (itemSplit != null && itemSplit.upgrade > 0) {
						left = new Command(mResources.BEGIN, 11105);
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] == null){
					left = null;
					isPaintItemInfo = false;
				}
				else {
					left = new Command(mResources.SELECT, 11106);
				}
			}
		}
		else if (isPaintTinhluyen) {
			try{
				if (indexMenu == 0) {
					if (indexTitle == 1) {
						if (itemSplit != null) {
							left = new Command(mResources.SELECT, 11103);
//						} else if (itemSplit != null) {
//							left = cmdSplitMoveOut;
						} else
							isPaintItemInfo = false;

						if (GameCanvas.isTouchControlLargeScreen)
							updateItemInfo(Item.UI_BAG, itemSplit);
						else {
							center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11104, itemSplit);
						}
					} else if (indexTitle == 2) {
						final Item item = arrItemSplit[indexSelect];
						if (item != null) {
							if (GameCanvas.isTouchControlLargeScreen)
								updateItemInfo(Item.UI_BAG, item);
							else {
								center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11104, item);
							}
							left = new Command(mResources.MOVEOUT, 1605);
						} else
							isPaintItemInfo = false;
						if (itemSplit != null) {
							left = new Command(mResources.SELECT, 1604);
						}
					}
				} else if (indexMenu == 1) {
					if (Char.myChar().arrItemBag[indexSelect] == null){
						left = null;
						isPaintItemInfo = false;
					}
					else {
						left = new Command(mResources.SELECT, 11106);
					}
				}
				}catch (Exception e) {
				}
		}
		
		 else if (isPaintDichChuyen) {
			if (indexMenu == 0) {
				if (indexTitle == 1) {
					if (itemSplit != null && itemSplit.upgrade > 13) {
						left = new Command(mResources.SELECT, 11103);
					} else if (itemSplit != null) {
						left = cmdSplitMoveOut;
					} else
						isPaintItemInfo = false;

					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_BAG, itemSplit);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11104, itemSplit);
					}
				} else if (indexTitle == 2) {
					final Item item = arrItemSplit[indexSelect];
					if (item != null) {
						if (GameCanvas.isTouchControlLargeScreen)
							updateItemInfo(Item.UI_BAG, item);
						else {
							center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11104, item);
						}
					} else
						isPaintItemInfo = false;
					if (itemSplit != null && itemSplit.upgrade > 13) {
						left = new Command(mResources.SELECT, 1604);
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] == null){
					left = null;
					isPaintItemInfo = false;
				}
				else {
					left = new Command(mResources.SELECT, 1606);
				}
			}
		}
		
		else if (isPaintUpPearl) {
			if (indexMenu == 0) {
				int count = 0;
				for (int i = 0; i < arrItemUpPeal.length; i++) {
					if (arrItemUpPeal[i] != null) {
						count++;
						if (count >= 2) {
							break;
						}
					}
				}
				final Item item = getItemFocus(Item.UI_UPPEARL);
				if (item != null) {
					if (count >= 2) {
						left = new Command(mResources.SELECT, 11107);
					} else {
						left = new Command(mResources.MOVEOUT, 111071);
					}
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_BAG, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11108);
					}
				} else {
					isPaintItemInfo = false;
					if (count >= 2) {
						left = new Command(mResources.BEGIN, 11062);
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null) {
					left = new Command(mResources.SELECT, 11109);
				} else {
					isPaintItemInfo = false;
					left = null;
				}
			}
		}
		
		else if(isPaintLuyenThach){
			if (indexMenu == 0) {
				int count = 0;
				int count1 = 0;
				int count2 = 0;
				int levelCrysal = 0;
				for (int i = 0; i < arrItemUpPeal.length; i++) {
					Item item = arrItemUpPeal[i];
					if (item != null) {
						if (item.template.id == 455) // Tử tinh thạch sơ cấp
							count++;
						else if (item.template.id == 456) // Tử tinh thạch trung cấp
							count1++;
						
					}
					if (count >= 9 || count1 >= 9 || (levelCrysal == 10 && count >= 3) || (levelCrysal == 11 && count1 >= 3)) 
						break;
				}
				
				final Item item = getItemFocus(Item.UI_LUYEN_THACH);
				if (item != null) {
					if (count == 9 || count1 == 9 || (levelCrysal == 10 && count == 3 && count2 == 1) || (levelCrysal == 11 && count1 == 3 && count2 == 1)) {
						left = new Command(mResources.SELECT, 1601);
					} else {
						left = new Command(mResources.MOVEOUT, 111071);
					}
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_LUYEN_THACH, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 1602);
					}
				} else {
					isPaintItemInfo = false;
					if (count >= 9 || count1 >= 9 || (levelCrysal >= 10 && (count >= 3 || count1 >= 3))) {
						left = new Command(mResources.BEGIN, 1600);
					}
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null) {
					left = new Command(mResources.SELECT, 1603);
				} else {
					isPaintItemInfo = false;
					left = null;
				}
			}
		}
		
		else if (isPaintTrade) {
			if (indexMenu == 0) {
				if (indexTitle == 1) {
					Item item = arrItemTradeMe[indexSelect];
					if (item != null) {
						if (typeTrade == 0) {
							left = cmdTradeSelectInList;
						} else {
							if (typeTrade == 1 && typeTradeOrder >= 1 && timeTrade - System.currentTimeMillis() / 1000 <= 0) {
								left = cmdTradeAccept;
							}
						}
						if (GameCanvas.isTouchControlLargeScreen) {
							Item item_ = arrItemTradeMe[indexSelect];
							updateItemInfo(Item.UI_BAG, item_);
						} else
							center = cmdTradeViewItemInfo;
					} else {
						isPaintItemInfo = false;
						if (typeTrade == 0) {
							left = cmdTradeLock;
						} else if (typeTrade == 1 && typeTradeOrder >= 1 && timeTrade - System.currentTimeMillis() / 1000 <= 0) {
							left = cmdTradeAccept;
						}
					}
				}
				if (indexTitle == 2) {
					Item item = arrItemTradeOrder[indexSelect];
					if (item != null) {
						if (GameCanvas.isTouchControlLargeScreen) {
							Item item_ = arrItemTradeOrder[indexSelect];
							updateItemInfo(Item.UI_TRADE, item_);
						} else {
							center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11110);
						}
					} else
						isPaintItemInfo = false;
				}
			} else if (indexMenu == 1 && typeTrade == 0) {
				if (Char.myChar().arrItemBag[indexSelect] != null)
					left = cmdTradeSelectInBag;
				else
					left = cmdTradeSendMoney;
			}
		} else if (isPaintBox) {
			if (indexMenu == 0) {
				final Item item = getItemFocus(Item.UI_BOX);
				if (item != null) {
					left = new Command(mResources.GETOUT, 111101);
					if (GameCanvas.isTouchControlLargeScreen)
						updateItemInfo(Item.UI_BOX, item);
					else {
						center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11111);
					}
				} else {
					left = new Command(mResources.SORT, 11112);
				}
			} else if (indexMenu == 1) {
				if (Char.myChar().arrItemBag[indexSelect] != null) {
					left = new Command(mResources.GETIN, 11113);
				} else
					left = cmdBagSortItem;
			}
		}
		if (isPaintUI() && indexMenu == 1 && Char.myChar().arrItemBag[indexSelect] != null) {
			if (GameCanvas.isTouchControlLargeScreen)
				updateItemInfo(Item.UI_BAG, Char.myChar().arrItemBag[indexSelect]);
			else {
				center = new Command(GameCanvas.isTouch ? mResources.VIEW : "", 11114);
			}
		}
	}
	
	private void doShowListChatTab() {
		mVector v = new mVector();
		for (int i = 0; i < ChatManager.gI().chatTabs.size(); i++) {
			ChatTab tab = (ChatTab) ChatManager.gI().chatTabs.elementAt(i);
			v.addElement(new Command(tab.ownerName, 12001, new Integer(i)));
		}
		// v.addElement(new Command(Resources.LIST_FRIEND, 12003));
		v.addElement(new Command(mResources.BLOCK_MESSAGE, 12006));
		v.addElement(new Command(mResources.CHAT_ADMIN, 12008));
		GameCanvas.menu.startAt(v, 0);
		isMessageMenu = true;
	}
	
	public void paintInfoMod(mGraphics g){
		
		if(Char.myChar().mobFocus.mobName != null){
			mFont.tahoma_7_white.drawStringShadown(g, Char.myChar().mobFocus.mobName, Char.myChar().mobFocus.x, Char.myChar().mobFocus.y
					-Char.myChar().mobFocus.h-20 ,2);
//			mFont.tahoma_7_white.drawString(g, "Char.myChar().mobFocus.hp+"/"+Char.myChar().mobFocus.maxHp" )
		}
		
	}
	public static int changeKillID = 1,idEff;
	public static long delayPressAtt = 500, lastpress;
	public static boolean isSendMove;
	private void doFire(/*boolean isFireByShortCut*/) {

//		if (Char.myChar().statusMe != Char.A_DEAD && isAttack() && Char.myChar().mobFocus != null) {
////			Service.gI().requestNPC((short)Char.myChar().npcFocus.npcId);
////			Char.myChar().currentFireByShortcut = isFireByShortCut;
//			
//		}

			if(Char.myChar().statusMe != Char.A_DEAD && Char.myChar().mobFocus != null){
				if(mSystem.currentTimeMillis() - (lastpress + delayPressAtt) > 0){
					Char.myChar().cdir = (Char.myChar().cx-Char.myChar().mobFocus.x)>0?-1:1;
					if(GuiMain.isTestSkill)
					{

//						SkillTemplate skill=(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+changeKillID);
//						if(skill!=null)
						idEff = changeKillID;//skill.ideff;
					}
					if(changeKillID==0)
						Music.play(Music.ATTACK_0, 0.5f);
					else Music.play(Music.SKILL2, 0.5f);
					Char.myChar().setSkillPaint(sks[idEff], Skill.ATT_STAND);
					mVector ds = new mVector();
					ds.add(Char.myChar().mobFocus);
					Char.myChar().mobFocus.setInjure();
					Char.myChar().mobFocus.injureBy = Char.myChar();
					Char.myChar().mobFocus.status = Mob.MA_INJURE;
					
					lastpress = mSystem.currentTimeMillis();
//					isShowFocus = true;
					if(!isSendMove)
						Service.gI().charMove();
					Service.gI().sendPlayerAttack(ds, vCharInMap,1,changeKillID,true);
				}
				
//				Cout.println(" veff "+veffClient.size());
			}
			else if(Char.myChar().statusMe != Char.A_DEADFLY && Char.myChar().statusMe != Char.A_DEAD&&Char.myChar().charFocus!=null
					&&Char.myChar().charFocus.statusMe != Char.A_DEADFLY && Char.myChar().charFocus.statusMe != Char.A_DEAD
					){
				mVector dSs = new mVector();
				dSs.add(Char.myChar().charFocus);
				Char.myChar().cdir = (Char.myChar().cx-Char.myChar().charFocus.cx)>0?-1:1;
				if(Char.myChar().typePk==0||Char.myChar().charFocus.typePk==0
						||(Char.myChar().typePk>0&&Char.myChar().charFocus.typePk>0
								&&Char.myChar().typePk!=Char.myChar().charFocus.typePk)){

					if(!isSendMove)
						Service.gI().charMove();
					Char.myChar().charFocus.doInjure(1, 0,false,1 );
					Service.gI().sendPlayerAttack(dSs, dSs,0,changeKillID,false);
					ServerEffect.addServerEffect(25, Char.myChar().charFocus.cx, Char.myChar().charFocus.cy-20, 1);
					Char.myChar().setSkillPaint(sks[idEff], Skill.ATT_STAND);
				}else{
					Char.myChar().cdir = (Char.myChar().cx-Char.myChar().charFocus.cx)>0?-1:1;
					guiMain.menuIcon.indexpICon = Constants.ICON_GIAOTIEP;
					guiMain.menuIcon.paintButtonClose=true;
					guiMain.menuIcon.contact = new GuiContact(GameCanvas.hw, 20);
					guiMain.menuIcon.contact.SetPosClose(guiMain.menuIcon.cmdClose);
					MenuIcon.lastTab.add(""+ Constants.ICON_GIAOTIEP);
					MenuIcon.isShowTab = true;
				}
				
			}
			else if(Char.myChar().statusMe != Char.A_DEAD && Char.myChar().npcFocus != null){
//				isShowFocus = true;

				Char.myChar().cdir = (Char.myChar().cx-Char.myChar().npcFocus.cx)>0?-1:1;
				if(Char.myChar().npcFocus.template.typeKhu==-1){
					//focus khu
				}
				else if(Char.myChar().npcFocus.typeNV>-1){
					mVector menu = new mVector();
//					menu.addElement(new Command("cmd 1", GameCanvas.instance,GameCanvas.cMenuNpc,1+""));
//					menu.addElement(new Command("cmd 2", GameCanvas.instance,GameCanvas.cMenuNpc,2+""));
//					menu.addElement(new Command("cmd 3", GameCanvas.instance,GameCanvas.cMenuNpc,3+""));
					menu.addElement(new Command("Nhiệm vụ",this,10,new MenuObject(0,0,Char.myChar().npcFocus.npcId)));
//					menu.addElement(new Command("Nhiệm vụ 2",this,10,new MenuObject(0,0,Char.myChar().npcFocus.npcId)));
//					menu.addElement(new Command("Nhiệm vụ 3",this,10,new MenuObject(0,0,Char.myChar().npcFocus.npcId)));
//					GameCanvas.menu.startAt(menu, 0);
					menu.addElement(new Command("Menu", GameCanvas.instance,GameCanvas.cMenuNpc,(-1)+""));
					
					GameCanvas.menu.startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
				}else{
					Cout.println(getClass(),Quest.listUnReceiveQuest.size()+ " idnpc focus "+Char.myChar().npcFocus.npcId);
					for (int j = 0; j < Quest.listUnReceiveQuest.size(); j++) {
						Quest npc = (Quest)Quest.listUnReceiveQuest.elementAt(j);
						if(npc!=null&&npc.idNPC_From==Char.myChar().npcFocus.npcId){
							Char.myChar().npcFocus.typeNV = 0;
							mVector menu = new mVector();
							menu.addElement(new  Command("Nhiệm vụ",this,10,new MenuObject(0,0,Char.myChar().npcFocus.npcId)));

							GameCanvas.menu.startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
//							GameCanvas.menu.startAt(menu, 0);
							break;
						}
					}
					if(Char.myChar().npcFocus.typeNV==-1)
						for (int j = 0; j < Quest.vecQuestDoing_Main.size(); j++) {
							Quest npc = (Quest)Quest.vecQuestDoing_Main.elementAt(j);
							if(npc!=null&&npc.idNPC_From==Char.myChar().npcFocus.npcId){
								Char.myChar().npcFocus.typeNV = 1;
								mVector menu = new mVector();
								menu.addElement(new  Command("Nhiệm vụ",this,10,new MenuObject(0,0,Char.myChar().npcFocus.npcId)));
//								GameCanvas.menu.startAt(menu, 0);
								GameCanvas.menu.startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
								break;
							}
						}
					if(Char.myChar().npcFocus.typeNV==-1)
						for (int j = 0; j < Quest.vecQuestDoing_Sub.size(); j++) {
							Quest npc = (Quest)Quest.vecQuestDoing_Sub.elementAt(j);
							if(npc!=null&&npc.idNPC_From==Char.myChar().npcFocus.npcId){
								Char.myChar().npcFocus.typeNV = 1;
								mVector menu = new mVector();
								menu.addElement(new  Command("Nhiệm vụ",this,10,new MenuObject(0,0,Char.myChar().npcFocus.npcId)));
								GameCanvas.menu.startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
//								GameCanvas.menu.startAt(menu, 0);
								break;
							}
						}
					if(Char.myChar().npcFocus.typeNV==-1)
						for (int j = 0; j < Quest.vecQuestFinish.size(); j++) {
							Quest npc = (Quest)Quest.vecQuestFinish.elementAt(j);
							if(npc!=null&&npc.idNPC_From==Char.myChar().npcFocus.npcId){
								Char.myChar().npcFocus.typeNV = 1;
								mVector menu = new mVector();
								menu.addElement(new  Command("Nhiệm vụ",this,10,new MenuObject(0,0,Char.myChar().npcFocus.npcId)));
//								GameCanvas.menu.startAt(menu, 0);
								GameCanvas.menu.startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
								break;
							}
						}
					if(true){
						Service.gI().MenuNpc(Char.myChar().npcFocus.template.npcTemplateId, (byte)-1);
						mVector menu = new mVector();
//						menu.addElement(new  Command("",this,1,new MenuObject(0,0,Char.myChar().npcFocus.npcId),true));
						GameCanvas.menu.startAtNPC(menu, 1, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus,Char.myChar().npcFocus.template.contendChat );
					return;
					}
				}
			}
			else if(Char.myChar().statusMe != Char.A_DEAD && Char.myChar().itemFocus != null){

				Service.gI().itemPick(Char.myChar().itemFocus.type, (short)Char.myChar().itemFocus.itemMapID);//.itemMapID
			}
//		if(Char.myChar().statusMe != Char.A_DEAD && Char.myChar().npcFocus != null){
//			Service.gI().requestNPC((short)Char.myChar().npcFocus.npcId);
//		}
//		if(Char.myChar().charFocus != null && Char.myChar().statusMe != Char.A_DEAD){
//			Service.gI().inviteParty((short)Char.myChar().charFocus.charID, (byte)Type_Party.INVITE_PARTY);
//			Service.gI().requestjoinParty((byte)Type_Party.REQUEST_JOIN_PARTY, (short)Char.myChar().charFocus.charID);
//			Service.gI().requestAddfriend((byte)Friend.INVITE_ADD_FRIEND, (short)Char.myChar().charFocus.charID);
//			Service.gI().inviteTrade(Constants.INVITE_TRADE, (short) Char.myChar().charFocus.charID);
//		}
//		if(!isFireByShortCut)
//			Char.myChar().lastNormalSkill = Char.myChar().myskill;
	}
	
	
	public boolean isAttack() {
//		if (ChatTextField.gI().isShow)
//			return false;
		try{
			
			if (Char.myChar().mobFocus != null) {
				if (Char.myChar().cx < Char.myChar().mobFocus.x)
					Char.myChar().cdir = 1;
				else
					Char.myChar().cdir = -1;
				int dx = Math.abs(Char.myChar().cx - Char.myChar().mobFocus.x);
				int dy = Math.abs(Char.myChar().cy - Char.myChar().mobFocus.y);
				Char.myChar().cvx = 0;
			} else if (Char.myChar().npcFocus != null) {
				if (Char.myChar().cx < Char.myChar().npcFocus.cx)
					Char.myChar().cdir = 1;
				else
					Char.myChar().cdir = -1;
				if (Char.myChar().cx < Char.myChar().npcFocus.cx)
					Char.myChar().npcFocus.cdir = -1;
				else
					Char.myChar().npcFocus.cdir = 1;
				
				int dx = Math.abs(Char.myChar().cx - Char.myChar().npcFocus.cx);
				int dy = Math.abs(Char.myChar().cy - Char.myChar().npcFocus.cy);
				if (dx < 60 && dy < 40) {
					GameCanvas.clearKeyHold();
					GameCanvas.clearKeyPressed();
				} else {
					Char.myChar().currentMovePoint = new MovePoint(Char.myChar().npcFocus.cx, Char.myChar().cy);
					GameCanvas.clearKeyHold();
					GameCanvas.clearKeyPressed();
				}
				return false;
			} 
			else if (Char.myChar().itemFocus != null) {
				if (Char.myChar().statusMe != Char.A_STAND) {
					return false;
				}
				if (Char.myChar().cx < Char.myChar().itemFocus.x)
					Char.myChar().cdir = 1;
				else
					Char.myChar().cdir = -1;
				int dx = Math.abs(Char.myChar().cx - Char.myChar().itemFocus.x);
				int dy = Math.abs(Char.myChar().cy - Char.myChar().itemFocus.y);
				if ((dx <= 35 && dy < 35) || (auto != 0 && dx <= 48 && dy <= 48)) {
					GameCanvas.clearKeyHold();
					GameCanvas.clearKeyPressed();
					Service.gI().itemPick((byte)3,(short)Char.myChar().itemFocus.itemMapID);
				} else {
					Char.myChar().currentMovePoint = new MovePoint(Char.myChar().itemFocus.x, Char.myChar().cy);
					GameCanvas.clearKeyHold();
					GameCanvas.clearKeyPressed();
				}
				return false;
			}
			return true;
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public static void showChatPopup(String contendChat,  int iYes,Object pYes,int iNo,Object pNo){
		chatPopupCW.setInfo(contendChat, new Command("Giao Tiếp", instance,iYes,pYes), null, new Command("Hủy", instance,iYes,pYes));
		chatPopupCW.show();
		
	}
	
	
	
	private boolean isPaintSelect()
	{
		if (!isPaintTeam && !isPaintFindTeam && !isPaintFriend && !isPaintEnemies && !isPaintCharInMap && !isPaintList && !isPaintAuctionBuy && !ispaintChat && !isPaintMessage)
			return false;
		return true;
	}
	public void updateSelectList() {
		try{
		if(!isPaintSelect())
			return;
		if (isPaintTeam) {
//			if (hParty.size() == 0)
//				Service.gI().requestinfoPartynearME(Type_Party.GET_INFOR_NEARCHAR, (short)Char.myChar().charID);
			if (GameCanvas.keyPressed[Key.NUM8]) {
				indexRow++;
				if (indexRow >= hParty.size())
					indexRow = hParty.size() - 1;
				scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
			} else if (GameCanvas.keyPressed[Key.NUM2]) {
				indexRow--;
				if (indexRow < 0)
					indexRow = 0;
				scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
			}
			setPartyCommand();
		} else if (isPaintFriend) {
			if (GameCanvas.keyPressed[Key.NUM8]) {
				indexRow++;
				if (indexRow >= Char.myChar().vFriend.size())
					indexRow = Char.myChar().vFriend.size() - 1;
				scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
			} else if (GameCanvas.keyPressed[Key.NUM2]) {
				indexRow--;
				if (indexRow < 0)
					indexRow = 0;
				scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
			}
			setFriendCommand();

		} 
		else if(ispaintChat) {
			if (GameCanvas.keyPressed[Key.NUM8]) {
				indexRow++;
				if (indexRow >= ChatPrivate.vOtherchar.size())
					indexRow = ChatPrivate.vOtherchar.size() - 1;
				scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
			} else if (GameCanvas.keyPressed[Key.NUM2]) {
				indexRow--;
				if (indexRow < 0)
					indexRow = 0;
				scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
			}
			if(!isPaintZone)
			tfCharFriend.update();
			setFriendChatCommand();

		}
		if (GameCanvas.isTouch) {
				if (GameCanvas.currentDialog == null && !GameCanvas.menu.showMenu) {
					Vector v = null;
					Hashtable h = null;
					if (isPaintTeam)
						h = hParty;
					else if (isPaintFriend)
						v = Char.myChar().vFriend;
					else if (ispaintChat)
					{
						v =ChatPrivate.vOtherchar;
					}
					ScrollResult r = scrMain.updateKey();
					if (r.isDowning || r.isFinish) {
						indexRow = r.selected;
						if (indexRow >= v.size())
							indexRow = -1;
					}
				}
		}

		GameCanvas.clearKeyHold();
		GameCanvas.clearKeyPressed();
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	//send chat comand
	private void setFriendChatCommand(){
		if (ChatPrivate.vOtherchar.size() > 0 && indexRow!=-1 ) {
			Char.toCharChatSelected = (OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow);
		}
	}
	
	private void setPartyCommand() {
		
		Party party = (Party)hParty.get(Char.myChar().idParty+"");
//		if(party == null)
//			return;
		if (indexRow == -1 )
			return;
		if(party != null){
			center = null;
					if(Char.myChar().charID == party.idleader){
						final Char c = (Char) party.vCharinParty.elementAt(indexRow);
						if(c != null){
							left = new Command("Kích", 270192);
							left.setPos(GameScreen.popupX + GameScreen.popupW, GameScreen.popupY, GameScreen.imgLbtn, GameScreen.imgLbtn);
							right = new Command("Giải tán", 111111);
							right.setPos(GameScreen.popupX + GameScreen.popupW, GameScreen.popupY + (Screen.cmdH * 2), GameScreen.imgLbtn, GameScreen.imgLbtn);
							center = new Command("Rời", 231291);
							center.setPos(GameScreen.popupX + GameScreen.popupW, GameScreen.popupY + Screen.cmdH, GameScreen.imgLbtn, GameScreen.imgLbtn);
						}
					}else{
						left = new Command("Kích", 270192);
						left.setPos(GameScreen.popupX + GameScreen.popupW, GameScreen.popupY, GameScreen.imgLbtn, GameScreen.imgLbtn);
						right = new Command("Giải tán", 111111);
						right.setPos(GameScreen.popupX + GameScreen.popupW, GameScreen.popupY + (Screen.cmdH * 2), GameScreen.imgLbtn, GameScreen.imgLbtn);
						center = new Command("Rời", 231291);
						center.setPos(GameScreen.popupX + GameScreen.popupW, GameScreen.popupY + Screen.cmdH, GameScreen.imgLbtn, GameScreen.imgLbtn);
					}
				//}
				
			} 
			else {
				final Char c = (Char) charnearByme.elementAt(indexRow);
//				if (c.charID != Char.myChar().charID) {
					center = new Command(mResources.SELECT, 12009);
//				}
//			}
		}
		
	}
	
	public void paintTeam(mGraphics g) {
//		if (isPaintTeam) {
			
			GameCanvas.paint.paintFrameNaruto(popupX, popupY,widthGui,heightGui, g);

			xstart = popupX + 5;
			ystart = popupY + 40;

			if (hParty.size() == 0){
				if(charnearByme.size() == 0){
					mFont.tahoma_7_white.drawString(g, mResources.NOT_TEAM, popupX + widthGui / 2, popupY + 40, FontSys.CENTER);
				}else{
//					g.setColor(0x001919);
//					g.fillRect(xstart - 2, ystart - 2, widthGui - 6, indexSize * 5 + 8);

					resetTranslate(g);
					scrMain.setStyle(hParty.size(), indexSize, xstart, ystart, widthGui - 3, indexSize * 5 + 4, true, 1);
					scrMain.setClip(g, xstart, ystart, widthGui - 3, indexSize * 5 + 6);
					indexRowMax = charnearByme.size();
					for(int i = 0; i < charnearByme.size(); i++){
						Char c = (Char) charnearByme.elementAt(i);
						if (indexRow == i) {
							g.setColor(Paint.COLORLIGHT);
							g.fillRect(xstart + 2, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
							g.setColor(0xffffff);
							g.drawRect(xstart + 2, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
						} else {
							g.setColor(Paint.COLORBACKGROUND);
							g.fillRect(xstart + 2, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
							g.setColor(0xd49960);
							g.drawRect(xstart + 2, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
						}
						if(c.idParty == -1){
							mFont.tahoma_7_yellow.drawString(g, c.cName, xstart + 22, ystart + i * indexSize + indexSize / 2 - 6, 0);
						}else{
							mFont.tahoma_7_red.drawString(g, c.cName, xstart + 22, ystart + i * indexSize + indexSize / 2 - 6, 0);
						}
					}
				}
			}
			else {
				g.setColor(0x001919);
				g.fillRect(xstart - 2, ystart - 2, widthGui - 6, indexSize * 5 + 8);

				resetTranslate(g);
				scrMain.setStyle(hParty.size(), indexSize, xstart, ystart, widthGui - 3, indexSize * 5 + 4, true, 1);
				scrMain.setClip(g, xstart, ystart, widthGui - 3, indexSize * 5 + 6);
//				Party party = (Party) hParty.get(Char.myChar().idParty+"");
//				if(party != null){
					indexRowMax = Party.vCharinParty.size();
					for(int i = 0; i < Party.vCharinParty.size(); i++){
						Char c = (Char) Party.vCharinParty.elementAt(i); 
						if (indexRow == i) {
							g.setColor(Paint.COLORLIGHT);
							g.fillRect(xstart + 2, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
							g.setColor(0xffffff);
							g.drawRect(xstart + 2, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
						} else {
							g.setColor(Paint.COLORBACKGROUND);
							g.fillRect(xstart + 2, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
							g.setColor(0xd49960);
							g.drawRect(xstart + 2, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
						}
						
						if(c != null){
							if(c.isLeader)
								mFont.tahoma_7_yellow.drawString(g, "Hoahoa"/*c.cName*/, xstart + 22, ystart + i * indexSize + indexSize / 2 - 6, 0);
							else
								mFont.tahoma_7_red.drawString(g, "Hoahoa"/*c.cName*/, xstart + 22, ystart + i * indexSize + indexSize / 2 - 6, 0);
						}
					}
				}
				
			paintNumCount(g);
			
//		}
		
			GameCanvas.resetTrans(g);
			//paint name box 
			Paint.PaintBoxName("DANH SÁCH NHÓM",popupX+widthGui/2 - 50,popupY+10,100,g);
//		}
		
	}
	public void paintFriend(mGraphics g) {

		if (isPaintFriend) {
			String str =  mResources.FRIENDS[0] ;
			
			GameCanvas.paint.paintFrameNaruto(popupX, popupY, widthGui,heightGui, g);
			if (Char.myChar().vFriend.size() > 0) {
				xstart = popupX + 5;
				ystart = popupY + 40;
				
				resetTranslate(g);
				scrMain.setStyle(Char.myChar().vFriend.size(), indexSize, xstart, ystart,  widthGui - 3, 180, true, 1);
				scrMain.setClip(g, xstart, ystart-10,  widthGui - 3, 180);

				indexRowMax = Char.myChar().vFriend.size();
				int friendCount = 0;
				int yBGFriend=0;
				for (int i = 0; i < Char.myChar().vFriend.size(); i++) {
					Char c = (Char) Char.myChar().vFriend.elementAt(i);
//					
//						if (indexRow == i) {
//							g.setColor(Paint.COLORLIGHT);
//							g.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
//							g.setColor(0xffffff);
//							g.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
//							btnChat.paint(g);
//							btnUnfriend.paint(g);
//						
//							
//						} else {
//							g.setColor(Paint.COLORBACKGROUND);
//							g.fillRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
//							g.setColor(0xd49960);
//							g.drawRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
//						}
						
					   
						Paint.PaintBGListQuest(xstart +35,ystart+yBGFriend,160,g);//new quest	
						 g.drawImage(LoadImageInterface.charPic,xstart +45,ystart+yBGFriend+20,g.VCENTER|g.HCENTER);
						if(c.isOnline)
						{
							g.drawImage(LoadImageInterface.imgName,xstart+100,ystart+yBGFriend+20,g.VCENTER|g.HCENTER);
							mFont.tahoma_7_white.drawString(g, c.cName, xstart + 73, ystart+yBGFriend+13 , 0);
						}
							
						else 
						{
							g.drawImage(LoadImageInterface.imgName,xstart+100,ystart+yBGFriend+20,g.VCENTER|g.HCENTER);
							mFont.tahoma_7_white.drawString(g, c.cName, xstart + 73, ystart +yBGFriend+13, 0);	
						}
						
				
						friendCount++;
						yBGFriend+=50;
				}
				resetTranslate(g);
				if(btnChat!=null)
				{
					btnChat.paint(g);
					btnUnfriend.paint(g);
				}
				
				indexRowMax = friendCount;
				scrMain.setStyle(friendCount, indexSize, xstart, ystart, widthGui - 3, indexSize * 5 + 4, true, 1);
			} else {
				mFont.tahoma_7_white.drawString(g, mResources.NO_FRIEND, popupX + widthGui / 2, popupY + 40,
						mFont.CENTER);
			}
			paintNumCount(g);
			
			GameScreen.resetTranslate(g);
			//paint name box 
			Paint.PaintBoxName("DANH SÁCH BẠN BÈ",popupX+55,popupY,130,g);
		}
	}
	
	private void setFriendCommand() {
		if(isPaintFriend){
			if(indexRow == -1)
				return;
			if (Char.myChar().vFriend.size() > 0) {
				Char c = (Char) Char.myChar().vFriend.elementAt(indexRow);
				Char.toCharChat=c;
//				yMenu+(5+LoadImageInterface.btnTab.getHeight())*i
				btnUnfriend = new Command("Xóa bạn", Constants.UN_FRIEND);
				btnUnfriend.setPos(GameScreen.popupX + GameScreen.popupW+Image.getWidth(LoadImageInterface.btnTab)-10, GameScreen.popupY+40, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
				btnChat = new Command("Chat riêng", Constants.CHAT_PRIVATE);
				btnChat.setPos(GameScreen.popupX + GameScreen.popupW+Image.getWidth(LoadImageInterface.btnTab)-10, GameScreen.popupY + Screen.cmdH+40+5, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
			}
		}else{
			btnUnfriend = null;
			btnChat = null;
		}
	}
	
	private void actSetFriendCommand() {
		final  Char ch = (Char) Char.myChar().vFriend.elementAt(indexRow);
		mVector subPermission = new mVector();
		subPermission.addElement(new Command("Xóa Bạn", 11));
		GameCanvas.menu.startAt(subPermission, 0);

	}
	public OtherChar c;
	public int popx, popy, popw, poph;
	public void paintListFiendChat(mGraphics g) {
		if (ispaintChat) {
//			GameCanvas.paint.paintFrame(popx + 70, popy +30, popw, poph + 20, g);//paint list chater
			GameCanvas.paint.paintFrameNaruto(popx, popy, popw, poph, g);//paint list chater
			GameCanvas.paint.paintFrameNaruto(xGui, yGui,widthGui,heightGui, g);//paint frame chat
			if (ChatPrivate.vOtherchar.size() > 0) {
//				xstart = popupX + 5;
//				ystart = popupY + 40;
				xstart = popx+5;
				ystart = popy+5;
//				g.setColor(0x001919);
//				g.fillRect(xstart - 2, ystart - 2, popw - 6, indexSize * 5 + 8);
	
				resetTranslate(g);
				
				scrMain.setStyle(ChatPrivate.vOtherchar.size(), indexSize, popx, popy, popw , poph, true, 1);
				scrMain.setClip(g, popx, popy, popw, poph);
	
				indexRowMax = ChatPrivate.vOtherchar.size();
				int friendCount = 0;
				for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++)
				{
					OtherChar c = (OtherChar) ChatPrivate.vOtherchar.elementAt(i);
//	
					if (indexRow == i) 
					{
						g.setColor(Paint.COLORLIGHT);
						g.fillRect(xstart + 2, ystart + (indexRow * indexSize) + 2, popw - 15, indexSize - 4);
						g.setColor(0xffffff);
						g.drawRect(xstart + 2, ystart + (indexRow * indexSize) + 2, popw - 15, indexSize - 4);
								
					} 
//					else
//					{
//						g.setColor(Paint.COLORBACKGROUND);
//						g.fillRect(xstart + 2, ystart + (i * indexSize) + 2, popw - 15, indexSize - 4);
//						g.setColor(0xd49960);
//						g.drawRect(xstart + 2, ystart + (i * indexSize) + 2, popw - 15, indexSize - 4);
//					}
							
					mFont.tahoma_7_red.drawString(g, c.name, xstart + 8, ystart + i * indexSize + indexSize / 2 - 6, 0);
		
					friendCount++;
				}
				indexRowMax = friendCount;
				scrMain.setStyle(friendCount, indexSize, xstart, ystart, popw - 3, indexSize * 5 + 4, true, 1);
				
				paintNumCount(g);
			} 

		
		}
	}
	public static TField tfCharFriend;
	private void initTfied(){
		
		  tfCharFriend = new TField();
//		  tfCharFriend.name = "Chat riêng";
		  tfCharFriend.width = popupW - 30;
		  tfCharFriend.height = ITEM_HEIGHT + 2;
		  tfCharFriend.x = xGui + 5;
		  tfCharFriend.y = yGui+heightGui- 25;
		  tfCharFriend.isFocus =false;
		  tfCharFriend.setIputType(TField.INPUT_TYPE_ANY);
//		  tfCharFriend.m = GameMidlet.instance;
//		  tfCharFriend.c = MotherCanvas.instance;
//		  tfCharFriend.color = 0xffffff;
	}
	
	/*
	 * Using to paint com.sakura.thelastlegend.gui chat friend
	 */
	public mVector contendChat;
	public void paintAlert(mGraphics g) {
		
		if (!ispaintChat )
			return;
		resetTranslate(g);
		paintListFiendChat(g);
		resetTranslate(g);
		tfCharFriend.paint(g);
		//paint content of chat friend
		if(Char.toCharChatSelected!=null)
		{
			Char.toCharChatSelected.Paint(g,xGui+5, yGui+5,widthGui-20,tfCharFriend.y-yGui-7);
		}
		resetTranslate(g);
		chat.paint(g);
		bntIconChat.paint(g);
		if(iconChat!=null)
		{
			 iconChat.paint(g);
		}
		
	}
	
	public void updatekeychat(){
		if(!ispaintChat)
			return;
		if (GameCanvas.isTouch) {
			   ScrollResult r = scrMain.updateKey();
			   if (r.isDowning || r.isFinish) {
			    indexRow = r.selected;
			   }
		}
	}
	
public static mBitmap[] imgBgIt;
	
	public static void loadMapItem(){ // đọc dữ liệu data Map item
//		TileMap.vItemBg.removeAllElements();
//		DataInputStream dis;
		InputStream iss = null;
		DataInputStream dis;

		try{
//			dis = new DataInputStream(MyStream.readFile("/mapitem/mapItem"));
			iss  = GameMidlet.asset.open("mapitem/mapItem");
			dis = new DataInputStream(iss);
			short nMapItem = dis.readShort();
			//imgBgIt = new Image[nMapItem];
			
			for (int i = 0; i < nMapItem; i++) {
				//imgBgIt[i] = GameCanvas.loadImage("/mapobject/"+i+".png");
				BgItem biSe = new BgItem();
				biSe.id = i;
					biSe.idImage = dis.readShort();// id hình
//					if(biSe.idImage < 129){
//					BgItem.imgNew.put(biSe.idImage+"", imgBgItem[biSe.idImage]);
//					byte nTileNotMove = msg.reader().readByte();
					biSe.layer = dis.readByte(); // layer vẽ trước vẽ sau
					biSe.dx = dis.readShort(); // tâm của item trong map
					biSe.dy = dis.readShort();
					byte nTileNotMove = dis.readByte();
//					if(nTileNotMove > 0){
						biSe.tileX = new int[nTileNotMove];
						biSe.tileY = new int[nTileNotMove];
						for (int j = 0; j < nTileNotMove; j++) {
							biSe.tileX[j] = dis.readByte();
							biSe.tileY[j] = dis.readByte();
							
						}
					
					
					
					TileMap.vItemBg.addElement(biSe);
//					}
				}
				
//			}

		}catch(Exception e){
			Cout.println("loadMapItem error  sizeBgItemmmmm======= "+TileMap.vItemBg.size());
			
			e.printStackTrace();
		}
	}
	
	public static void loadMapTable(int mapId){
//		TileMap.vCurrItem.removeAllElements();
		InputStream iss = null;
		DataInputStream dis = null;
		try{
//			dis = new DataInputStream(MyStream.readFile("/mapitem/mapTable"/*+mapId*/));
//			dis = new DataInputStream(GameMidlet.asset.open("mapitem/mapTable"/*+mapId*/));
			iss  = GameMidlet.asset.open("mapitem/mapTable"+mapId);
			dis = new DataInputStream(iss);
			short Count = dis.readShort();
			for(int i = 0; i < Count; i++){
				BgItem biMap = new BgItem();
				short id = dis.readShort();
				BgItem currBi = TileMap.getBIById(id);
				biMap.id = currBi.id;
				biMap.x = dis.readShort() * TileMap.size;
				biMap.y = dis.readShort() * TileMap.size;
				biMap.idImage = currBi.idImage;
				biMap.dx = currBi.dx;
				biMap.dy = currBi.dy;
				biMap.layer = currBi.layer;
				TileMap.vCurrItem.addElement(biMap);
					
				
			}
			Service.gI().requestinventory();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
//	public static mBitmap[] imgBgItem;
//	public static void loadImageBgItem(){
//		imgBgItem = new mBitmap[400];
//		for(int i = 0; i < imgBgItem.length; i++){
//			imgBgItem[i] = GameCanvas.loadImage("/mapobject/"+i+".png");
//		}
//	}
	
	
	public void paintBgItem(mGraphics g, int layer) {
		for (int a = 0; a < TileMap.vCurrItem.size(); a++) {
			BgItem bi = (BgItem) TileMap.vCurrItem.elementAt(a);
			if (bi.idImage != -1){
				if (bi.layer == layer&&GuiMain.isPaintOjectMap)
					bi.paint(g);
			}
		}
	}
	
	
	public void hardcodeFriendlist(){
		
		Char.myChar().vFriend.removeAllElements();
		byte sizeFriendList = 8;
		for(int i = 0; i < sizeFriendList; i++){
			Char ch = new Char();
			ch.isOnline = true;
			if(ch.isOnline){
				ch.charID = 11111; // không online defaut là 0							
			}
			ch.CharidDB = 32100;
			ch.cName = "naruto";
			ch.clevel = 10;
			Char.myChar().vFriend.addElement(ch);
		}
		
	}
	
	public void hardcodeParty(){
		Party.vCharinParty.removeAllElements();
		for(int i = 0; i < 9; i++){
//			Char cMem = GameScreen.findCharInMap((short)11111);
			Char cMem = new Char();
			if(cMem != null){
				cMem.idParty = 1;
				if(cMem.charID == 1111)
					cMem.isLeader = true;
				else
					cMem.isLeader = false;
				Party.vCharinParty.addElement(cMem);
			}
//			else{
//				cMem = Char.myChar();
//				cMem.idParty = 1;
//				if(cMem.charID == 11111)
//					cMem.isLeader = true;
//				else
//					cMem.isLeader = false;
//				vCharinParty.addElement(cMem);
//			}
		}
	}
	
	public static void addEffectKillMobAttack(Mob m,Char c, int idEffect,byte ideffsub)
	{
		EffectKill eff = new EffectKill(idEffect, m, c, ideffsub);
		veffClient.add(eff);
	}
	
	public static void addEffectEnd(byte type, int x, int y){
		EffectEnd end = new EffectEnd(type, x, y);
		veffClient.add(end);
	}
	public static void addEffectEnd(byte type, int x, int y, int xTo, int toY){
		EffectKill end = new EffectKill(type, x, y,xTo, toY);
		veffClient.add(end);
		Cout.println2222("addEffct veffClient "+veffClient.size());
	}
	public void doTouchQuickSlot(int id)
	{
		if(Char.myChar().statusMe==Char.A_DEAD||Char.myChar().statusMe==Char.A_DEADFLY){
			GameCanvas.startCommandDlg("Bạn có muốn hồi sinh tại chỗ (1 gold)?", 
					new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
					new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
			return;
		}
		if(Char.myChar().npcFocus!=null&&Char.myChar().npcFocus.template!=null&&Char.myChar().npcFocus.template.typeKhu==-1){
			Service.gI().requestDanhKhu();
			return;
		}
		if(Char.myChar().npcFocus!=null) doFire();
		if(GuiMain.isTestSkill){
			changeKillID = GuiMain.indexSkillTest;
			doFire();
			return;
		}
		
		if(GameCanvas.isTouch){
			QuickSlot ql=  Char.myChar().mQuickslot[id];
			boolean isAction = false;
			if(ql.canfight()){
				//timeDow = 40;
				boolean isNgoaiRange = false;
				if(Char.myChar().mobFocus!=null){
					SkillTemplate skill=(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+ ql.idSkill);
					int range = CRes.getDistance(Char.myChar().cx, Char.myChar().cy, Char.myChar().mobFocus.x, Char.myChar().mobFocus.y);
					isNgoaiRange = (range>skill.rangelv[skill.level>=skill.rangelv.length?0:skill.level]?true:false);
				}
				changeKillID = ql.idSkill;
				idEff = ql.ideff;
				if(Char.myChar().mobFocus!=null&&isNgoaiRange){

					GameCanvas.StartDglThongBao("Mục tiêu ở quá xa");
					int maxkcX = CRes.abs(Char.myChar().cx-Char.myChar().mobFocus.x);
					int kc = 1,leng=0;
					if(maxkcX>20||CRes.abs(Char.myChar().cy-Char.myChar().mobFocus.y)>10){
						
						for (int j = 0; j < maxkcX; j++) {
							
							leng +=j;
							if(leng>= maxkcX)
							{
								kc = j-1;
								break;
							}
						}
						if(Char.myChar().cx>Char.myChar().mobFocus.x){
							Char.myChar().updateCharRun();
							if (Char.myChar().cdir == 1) {
								Char.myChar().cdir = -1;
								
							}
							if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
								if ((Char.myChar().cx - Char.myChar().cxSend) != 0)
									Service.gI().charMove();
									Char.myChar().updateCharRun();
								Char.myChar().statusMe = Char.A_RUN;
								
								Char.myChar().cvx = -kc;
							}
						}else{
							Char.myChar().updateCharRun();
							if (Char.myChar().cdir == -1) {
								Char.myChar().cdir = 1;
								
							}
							if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {
								if ((Char.myChar().cx - Char.myChar().cxSend) != 0)
									Service.gI().charMove();
									Char.myChar().updateCharRun();
								Char.myChar().statusMe = Char.A_RUN;
								Char.myChar().cvx = kc;
							}
						}
					}
					return;
				}else{
						if(Char.myChar().mobFocus!=null){
							if(ql.isEnoughtMp()){
								for (int i = 0; i < Char.myChar().mQuickslot.length; i++) {
									QuickSlot qll=  Char.myChar().mQuickslot[i];
									if(qll.idSkill==ql.idSkill&&!isNgoaiRange){
										Char.myChar().mQuickslot[i].startCoolDown();
									}
								}
								if(!isAutoDanh){
									xStartAuto=Char.myChar.cx;
									yStartAuto=Char.myChar.cy;
								}
								byte[] listauto = Rms.loadRMS(Rms.rms_Auto);
								if(listauto==null)
								{
								}else {
									if(listauto[SettingScreen.AUTO_DANH]==1)
										isAutoDanh = true;
								}
								doFire();
							}else 
								GameCanvas.StartDglThongBao("Không đủ charka");
						}
						else if(Char.myChar().npcFocus!=null||Char.myChar().itemFocus!=null||Char.myChar().charFocus!=null)
						{
							if(Char.myChar().charFocus!=null){
								if(Char.myChar().typePk==Char.myChar().charFocus.typePk){
									doFire();
								}
							}else doFire();
						}
				}
				if(Char.myChar().npcFocus==null&&
						(Char.myChar().charFocus!=null&&Char.myChar().charFocus.statusMe!=Char.A_DEAD&&Char.myChar().typePk!=Char.myChar().charFocus.typePk)) // ko start cooldow
					if(ql.isEnoughtMp()){
						for (int i = 0; i < Char.myChar().mQuickslot.length; i++) {
							QuickSlot qll=  Char.myChar().mQuickslot[i];
							if(qll.idSkill==ql.idSkill&&!isNgoaiRange){
								Char.myChar().mQuickslot[i].startCoolDown();
							}
						}
						doFire();
					}else 
						GameCanvas.StartDglThongBao("Không đủ charka");
//				Char.myChar().mQuickslot[id].startCoolDown();
			}
		}
/*			if(coolDown-mSystem.currentTimeMillis()<0&&ql.quickslotType==QuickSlot.TYPE_SKILL && Char.myChar().state!=Char.A_DEAD)
			{
			
			/*	coolDown=mSystem.currentTimeMillis()+Timecooldowm*1000;
			}else if(ql.quic kslotType==QuickSlot.TYPE_ITEM)
			{
				doUseItem(ql.ItemType);
			}*/
	}
	public static void updateCloudy(){
		for (int i = 0; i < ncloudy1; i++) {
			if(xpaintcloudy1[i]>TileMap.pxw)
			{
				ypaintcloudy1[i]=CRes.random(80, TileMap.pxh+GameCanvas.h);
			}
			else xpaintcloudy1[i]+=1;
		}
		for (int i = 0; i < ncloudy1; i++) {
			if(xpaintcloudy1[i]>TileMap.pxw){
				if(i==ncloudy1-1){
					xpaintcloudy1[i] =0 - wcloudy1;
					ypaintcloudy1[i]=CRes.random(80, TileMap.pxh+GameCanvas.h);
				}
				else{
					xpaintcloudy1[i] = 0 - wcloudy1;
					ypaintcloudy1[i]=CRes.random(80, TileMap.pxh+GameCanvas.h);
				}
			}
		}
		for (int i = 0; i < ncloudy2; i++) {
			if(xpaintcloudy2[i]>TileMap.pxw)
			{
				ypaintcloudy2[i]=CRes.random(80, TileMap.pxh+GameCanvas.h);
			}
			else xpaintcloudy2[i]+=2;
		}
		for (int i = 0; i < ncloudy2; i++) {
			if(xpaintcloudy2[i]>TileMap.pxw){
				if(i==ncloudy2-1){
					xpaintcloudy2[i] =0 - wcloudy2;
					ypaintcloudy2[i]=CRes.random(80, TileMap.pxh+GameCanvas.h);
				}
				else{
					xpaintcloudy2[i] = 0 - wcloudy2;
					ypaintcloudy2[i]=CRes.random(80, TileMap.pxh+GameCanvas.h);
				}
			}
		}
		for (int i = 0; i < ncloudy0; i++) {
			if(xpaintcloudy0[i]>TileMap.pxw)
			{
				ypaintcloudy0[i]=CRes.random(80, TileMap.pxh);
			}
			else xpaintcloudy0[i]+=3;
		}
		for (int i = 0; i < ncloudy0; i++) {
			if(xpaintcloudy0[i]>TileMap.pxw){
				if(i==ncloudy0-1){
					xpaintcloudy0[i] = 0- wcloudy0 -kcwcloudy0 ;
					ypaintcloudy0[i]=CRes.random(80, TileMap.pxh+GameCanvas.h);
				}
				else{
					xpaintcloudy0[i] =0- wcloudy0 - kcwcloudy0;
					ypaintcloudy0[i]=CRes.random(80, TileMap.pxh+GameCanvas.h);
				}
			}
		}
	}
	public static void paintCloudy1(mGraphics g){
		if(imgCloudy[1]!=null)
		for (int i = 0; i < ncloudy1; i++) {
			g.drawImage(imgCloudy[1], xpaintcloudy1[i], ypaintcloudy1[i],  g.BOTTOM|g.LEFT);
		}
	}
	public static void paintCloudy0(mGraphics g){
		if(imgCloudy[0]!=null)
		for (int i = 0; i < ncloudy0; i++) {
			g.drawImage(imgCloudy[0], xpaintcloudy0[i], ypaintcloudy0[i],  g.BOTTOM|g.LEFT);
		}
	}
	public static void paintCloudy2(mGraphics g){
		if(imgCloudy[2]!=null)
		for (int i = 0; i < ncloudy2; i++) {
			g.drawImage(imgCloudy[2], xpaintcloudy2[i], ypaintcloudy2[i],  g.BOTTOM|g.LEFT);
		}
	}
		public void AutoDanh(){
			if(Char.myChar().statusMe == Char.A_DEAD||Char.myChar().statusMe==Char.A_ATTK
					||Char.myChar().statusMe==Char.A_RUN
					||Char.myChar().skillPaint!=null)
				return;
			if(Char.myChar().statusMe!=Char.A_ATTK&&Char.myChar().statusMe!=Char.A_RUN){
				timeDow--;
				if(timeDow<=0&&Char.myChar().mobFocus!=null&&!Char.myChar().mobFocus.injureThenDie){
					int[] key = {0,1,2,3,4,5};
					indexKeyTouchAuto =(indexKeyTouchAuto+1)%5;
					QuickSlot ql=  Char.myChar().mQuickslot[indexKeyTouchAuto];
					if(ql.idSkill==-1) return;
					int range = -1;
					SkillTemplate skill=(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+ql.idSkill);
					range = skill.rangelv[skill.level];
					if(Util.distance(Char.myChar().cx,Char.myChar().cy, Char.myChar().mobFocus.x,Char.myChar().mobFocus.y)<=range){
						if(ql.quickslotType==QuickSlot.TYPE_SKILL)
							doTouchQuickSlot(GameCanvas.isTouch==true?key[indexKeyTouchAuto]:indexKeyTouchAuto);
					}else{
						timeDow = 100;
					}
				}
				else if(timeDow<=0&&(Char.myChar().mobFocus==null||Char.myChar().mobFocus.injureThenDie)) { // tim con quai trong range auTo
					Char.myChar().clearAllFocus();
					if(Char.myChar().statusMe==Char.A_STAND){
					for(int i=0;i<vMob.size();i++){
						Mob ac= (Mob)vMob.elementAt(i);
						if(ac!=null&&!ac.injureThenDie&&CRes.abs(ac.y-Char.myChar().cy)<30&&
								Util.distance(xStartAuto,yStartAuto, ac.x,ac.y)<=rangeAuto){
							int dis = Util.distance(Char.myChar().cx,Char.myChar().cy, ac.x,ac.y);
							if(dis<160){
								int maxkcX = CRes.abs(Char.myChar().cx-ac.x);
								int kc = 1,leng=0;
								if(maxkcX>20||CRes.abs(Char.myChar().cy-ac.y)>10){
									
									for (int j = 0; j < maxkcX; j++) {
										
										leng +=j;
										if(leng>= maxkcX)
										{
											kc = j-1;
											break;
										}
									}
									if(Char.myChar().cx>ac.x){
										Char.myChar().updateCharRun();
										if (Char.myChar().cdir == 1) {
											Char.myChar().cdir = -1;
											
										}
										Cout.println2222(GameCanvas.gameTick+"  gametick  "+(GameCanvas.gameTick%4==0));
										if (!Char.myChar().isLockMove && !Char.myChar().isBlinking&&GameCanvas.gameTick%4==0) {

											if(ac.isBoss&&!Mob.isBossAppear) continue;
//											if ((Char.myChar().cx - Char.myChar().cxSend) != 0)
//												Service.gI().charMove();
												Char.myChar().updateCharRun();
											Char.myChar().statusMe = Char.A_RUN;

											Char.myChar().mobFocus = ac;
											Char.myChar().cvx = -kc;
											break;
										}
									}else{
										Cout.println2222(GameCanvas.gameTick+"  gametick 22  "+(GameCanvas.gameTick%4==0));
										
										Char.myChar().updateCharRun();
										if (Char.myChar().cdir == -1) {
											Char.myChar().cdir = 1;
											
										}
										if (!Char.myChar().isLockMove && !Char.myChar().isBlinking) {

											if(ac.isBoss&&!Mob.isBossAppear) continue;
//											if ((Char.myChar().cx - Char.myChar().cxSend) != 0)
//												Service.gI().charMove();
												Char.myChar().updateCharRun();
											Char.myChar().statusMe = Char.A_RUN;
											Char.myChar().mobFocus = ac;
											Char.myChar().cvx = kc;
											break;
										}
									}
								}
							}
						}
						if(i==vMob.size()-1){
							timeDow = 120; // sleep thoi gian tim quai
						}
					}
					}
				}
			}
		}
		public void AutoNhat()
		{
			
			for (int i = 0; i < vNhatItemMap.size(); i++) {
				ItemMap item = (ItemMap)vNhatItemMap.elementAt(i);
				if(GameCanvas.gameTick%33==0)
				{
					Cout.println(item.timeTonTai+"   isSendNhat  "+item.isSendNhat);

				}
				if(item.timeTonTai>=3&&!item.isSendNhat){
					item.isSendNhat = true;
					Service.gI().itemPick(item.type, (short)item.itemMapID);//.

					vNhatItemMap.removeElementAt(i);
					i--;
				}
			}
		}
}
