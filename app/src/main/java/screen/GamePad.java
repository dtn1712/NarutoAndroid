package screen;


import com.sakura.thelastlegend.gui.GuiMain;
import com.sakura.thelastlegend.lib.LoadImageInterface;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.domain.model.CRes;
import com.sakura.thelastlegend.domain.model.Res;

public class GamePad {
    int xC, yC, xM, yM, xMLast, yMLast, R, r, d, xTemp, yTemp;
	int deltaX, deltaY, delta, angle;
	boolean isGamePad = false;
	public GamePad() {
		R = 18;
		xC = xM = 60;
		yC = GameCanvas.h-52;
//		xM = (GuiMain.xL+GuiMain.xR)/2;
//		yM = (GuiMain.yU+GuiMain.yU)/2+16;
	}
	
	public void update() {

		if (GameCanvas.isPointerDown && !GameCanvas.isPointerJustRelease)
        {
            if (!isGamePad)
            {
                xM = xC;
                yM = yC;
            }

            if (xC <= (100 ) && 
            		yC >= (GameCanvas.h - 100 ) && GameCanvas.px < 100  && GameCanvas.py >= GameCanvas.h - 100  && GameCanvas.py < GameCanvas.h )
            {

                isGamePad = true;
                delta = (int)(CRes.pow(deltaX, 2) + CRes.pow(deltaY, 2));
                d = CRes.sqrt(delta);

                if (Math.abs(deltaX) > 4 || Math.abs(deltaY) > 4)
                {
//                	if(MainChar.isAutoAttack)
//                    {
//                    	MainChar.isSleepStopAutoAttack = true;                        
//                    }
//                	if(GameCanvas.mainChar.action==Actor.AC_DIE){
//            			GameCanvas.startDlgMainCharDie();
//            			GameCanvas.isPointerJustRelease = false;
//                		 return;
//                	 }
                    angle = CRes.angle(deltaX, deltaY);

        			if (!GameCanvas.isPointerHoldIn(xC - R, yC - R, 2 * R, 2 * R))
                    {
                        if (d != 0)
                        {
                            yM = (deltaY * R) / d;
                            xM = (deltaX * R) / d;
                            xM += xC;
                            yM += yC;
                            if (!Res.inRect(xC - R, yC - R, 2 * R, 2 * R, xM,
                                    yM))
                            {
                                xM = xMLast;
                                yM = yMLast;
                            }
                            else
                            {
                                xMLast = xM;
                                yMLast = yM;
                            }
                        }
                        else
                        {
                            xM = xMLast;
                            yM = yMLast;
                        }
                    }
                    else
                    {
                        xM = GameCanvas.px;
                        yM = GameCanvas.py;
                    }
                    resetHold();
                   
                    if (checkPointerMove(2))
                    {
                        //                    	short speed = GameCanvas.mainChar.speed;
                        //                    	short xmove = (short)GameCanvas.mainChar.x;
                        //                    	short ymove = (short)GameCanvas.mainChar.y;
                        //                    	boolean isMove = false;

                        
                        	
                    	int key = 0;
                        if ((angle <= 360 && angle > 340) || (angle > 0 && angle <= 25))
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[6] = true;
                            GameCanvas.keyPressed[6] = true;
                            key = 6;
                        }
                        else if (angle > 25 && angle <= 70)
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[6] = true;
                            GameCanvas.keyPressed[6] = true;
                            key = 9;
//                            if(!MainChar.isAutoAttack)
//                            GameCanvas.mainChar.dir = Actor.DIR_RIGHT;
                        }

                        else if (angle > 70 && angle <= 115)
                        {
//                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[8] = true;
                            GameCanvas.keyPressed[8] = true;
//                            key = 8;
//                            if(!MainChar.isAutoAttack)
//                            GameCanvas.mainChar.dir = Actor.DIR_DOWN;
                        }
                        else if (angle > 115 && angle <= 160)
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[4] = true;
                            GameCanvas.keyPressed[4] = true;
                            key = 7;
//                            if(!MainChar.isAutoAttack)
//                            GameCanvas.mainChar.dir = Actor.DIR_DOWN;
                        }

                        else if ((angle > 160 && angle <= 205))
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[4] = true;
                            GameCanvas.keyPressed[4] = true;
                            key =4;
//                            if(!MainChar.isAutoAttack)
//                            GameCanvas.mainChar.dir = Actor.DIR_LEFT;
                        }
                        else if ((angle > 205 && angle <= 250))
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[1] = true;
                            GameCanvas.keyPressed[1] = true;
//                            GameCanvas.keyHold[4] = true;
//                            GameCanvas.keyPressed[4] = true;
                            key = 1;
//                            if(!MainChar.isAutoAttack)
//                            GameCanvas.mainChar.dir = Actor.DIR_LEFT;
                        }
                        else if (angle > 250 && angle <= 295)
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[2] = true;
                            GameCanvas.keyPressed[2] = true;
                            key = 2;
//                            if(!MainChar.isAutoAttack)
//                            GameCanvas.mainChar.dir = Actor.DIR_UP;
                        }
                        else if (angle > 295 && angle <= 340)
                        {
                            GameScreen.gI().auto = 0;
                            key = 3;
                            GameCanvas.keyHold[3] = true;
                            GameCanvas.keyPressed[3] = true;
//                            if(!MainChar.isAutoAttack)
//                            GameCanvas.mainChar.dir = Actor.DIR_UP;
                        }
                    }
                    else
                    {
                        resetHold();
                    }

                }
            }
        }
        else
        {
           ResetMove();
        }
        if (GameCanvas.isPointerDown &&
        		!GameCanvas.isPointerJustRelease &&
        		GameCanvas.pxFirst < xC+50  &&
        		GameCanvas.pyFirst >= yC-50  &&
        		GameCanvas.pyFirst < GameCanvas.h )
        {

			if (GameCanvas.pxFirst < xC+50  && GameCanvas.pyFirst >=yC-50 )
            {
                xTemp = GameCanvas.pxFirst;
                yTemp = GameCanvas.pyFirst;
            }
            if (xTemp <= (100 ) && yTemp >= (GameCanvas.h - 100 ))
            {
                if (!isGamePad)
                {
                    xC = xM = xTemp;
                    yC = yM = yTemp;
                }
                isGamePad = true;

                deltaX = GameCanvas.px - xC;
                deltaY = GameCanvas.py - yC;

                delta = (int)(CRes.pow(deltaX, 2) + CRes.pow(deltaY, 2));
                d = CRes.sqrt(delta);

                if (Math.abs(deltaX) > 4 || Math.abs(deltaY) > 4)
                {
                    angle = CRes.angle(deltaX, deltaY);

                    if (!GameCanvas.isPointerHoldIn(xC - R, yC - R, 2 * R, 2 * R))
                    {

                        if (d != 0)
                        {
                            yM = (deltaY * R) / d;
                            xM = (deltaX * R) / d;

                            // xM = (int)R* Res.cos(angle);
                            // yM = (int)R*Res.sin(angle);
                            xM += xC;
                            yM += yC;
                            if (!Res.inRect(xC - R, yC - R, 2 * R, 2 * R, xM,
                              yM))
                            {
                                xM = xMLast;
                                yM = yMLast;
                            }
                            else
                            {
                                xMLast = xM;
                                yMLast = yM;
                            }

                        }
                        else
                        {
                            xM = xMLast;
                            yM = yMLast;
                        }

                    }
                    else
                    {
                        xM = GameCanvas.px;
                        yM = GameCanvas.py;
                    }
                    resetHold();
                   
                    if (checkPointerMove(2))
                    {
                    	
                    	int key =0;
                        if ((angle <= 360 && angle > 340) || (angle > 0 && angle <= 25))
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[6] = true;
                            GameCanvas.keyPressed[6] = true;
                        }
                        else if (angle > 25 && angle <= 70)
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[6] = true;
                            GameCanvas.keyPressed[6] = true;
                            key =9;
                        }

                        else if (angle > 70 && angle <= 115)
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[8] = true;
                            GameCanvas.keyPressed[8] = true;
                            key =8;
                        }
                        else if (angle > 115 && angle <= 160)
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[4] = true;
                            GameCanvas.keyPressed[4] = true;
                            key =7;
                        }

                        else if ((angle > 160 && angle <= 205))
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[4] = true;
                            GameCanvas.keyPressed[4] = true;
                            key =4;
                        }
                        else if ((angle > 205 && angle <= 250))
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[1] = true;
                            GameCanvas.keyPressed[1] = true;
                            key =1;
                        }
                        else if (angle > 250 && angle <= 295)
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[2] = true;
                            GameCanvas.keyPressed[2] = true;
                            key =2;
                        }
                        else if (angle > 295 && angle <= 340)
                        {
                            GameScreen.gI().auto = 0;
                            GameCanvas.keyHold[3] = true;
                            GameCanvas.keyPressed[3] = true;
                            key =3;
                        }
                        
                    }
                    else
                    {
                        resetHold();
                    }
                }
            }

        }
        else
        {
            ResetMove();
        }
	}
	public void ResetMove()
	{
		 xC = (GuiMain.xL + GuiMain.xR) / 2 ;
         yC = (GuiMain.yU + GuiMain.yU) / 2 ;
         xM = xC;
         yM = yC;
         isGamePad = false;
         resetHold();
	}
	private boolean checkPointerMove(int distance) {
		if(true) return true;
//        if (Main.isPC) return false;
//		if((Char.getMyChar().statusMe == Char.A_JUMP))
//			return true;
		if(GameCanvas.arrPos==null||GameCanvas.arrPos.length<2)
			return false;
		for (int i = 2; i >0; i--) {
			if(GameCanvas.arrPos[i]==null||GameCanvas.arrPos[i-1]==null){
				return false;
			}
			int mx = GameCanvas.arrPos[i].x - GameCanvas.arrPos[i-1].x;
			int my = GameCanvas.arrPos[i].y - GameCanvas.arrPos[i-1].y;
			if (CRes.abs(mx) >distance  && CRes.abs(my) >distance ){
				return false;
			}
		}
		return true;
	}
	private void resetHold(){
//		GameCanvas.keyHold[1] = false;
//        if (!GameCanvas.keyHold[2] && (GameCanvas.keyHold[4] || GameCanvas.keyHold[6] || GameCanvas.keyHold[8]))
//        GameCanvas.keyHold[2] = false;
//        GameCanvas.keyHold[3] = false;
//
//        if (!GameCanvas.keyHold[4] &&( GameCanvas.keyHold[2] || GameCanvas.keyHold[6] || GameCanvas.keyHold[8]))
//        GameCanvas.keyHold[4] = false;
//
//
//        if (!GameCanvas.keyHold[6]&&(GameCanvas.keyHold[4] || GameCanvas.keyHold[2] || GameCanvas.keyHold[8]))
//        GameCanvas.keyHold[6] = false;
//        GameCanvas.keyHold[7] = false;
//
//        if (!GameCanvas.keyHold[8] && (GameCanvas.keyHold[4] || GameCanvas.keyHold[6] || GameCanvas.keyHold[2]))
//        GameCanvas.keyHold[8] = false;
//        GameCanvas.keyHold[9] = false;

        GameCanvas.keyPressed[1] = false;
        GameCanvas.keyPressed[2] = false;
        GameCanvas.keyPressed[3] = false;
        GameCanvas.keyPressed[4] = false;
        GameCanvas.keyPressed[6] = false;
        GameCanvas.keyPressed[7] = false;
        GameCanvas.keyPressed[8] = false;
        GameCanvas.keyPressed[9] = false;
        
        GameCanvas.keyHold[1] = false;
        GameCanvas.keyHold[2] = false;
        GameCanvas.keyHold[3] = false;
        GameCanvas.keyHold[4] = false;
        GameCanvas.keyHold[6] = false;
        GameCanvas.keyHold[7] = false;
        GameCanvas.keyHold[8] = false;
        GameCanvas.keyHold[9] = false;
	}
	
	public void paint(mGraphics g) {
//		g.drawRegion(LoadImageInterface.imgHotKey[0], 0, 0, g.getImageWidth(LoadImageInterface.imgHotKey[0]), g.getImageHeight(LoadImageInterface.imgHotKey[0]), 0, xC, yC, mGraphics.HCENTER | mGraphics.VCENTER);
		g.drawRegion(LoadImageInterface.imgHotKey[0], 0,
				(isGamePad==false?0:g.getImageHeight(LoadImageInterface.imgHotKey[0])/2),
				g.getImageWidth(LoadImageInterface.imgHotKey[0]),
				g.getImageHeight(LoadImageInterface.imgHotKey[0])/2, 0, xC, yC, mGraphics.HCENTER | mGraphics.VCENTER);
		
		g.drawRegion(LoadImageInterface.imgHotKey[1], 0, (isGamePad==false?0:g.getImageHeight(LoadImageInterface.imgHotKey[1])/2), g.getImageWidth(LoadImageInterface.imgHotKey[1]), g.getImageHeight(LoadImageInterface.imgHotKey[1])/2, 0, xM, yM, mGraphics.HCENTER | mGraphics.VCENTER);
	}
}
