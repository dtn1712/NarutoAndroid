package screen;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.real.Service;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.OtherChar;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

public class ClanScreen extends Screen implements IActionListener{

	public static ClanScreen me ;

	public int wkhung,hKhung,xpaint,ypaint;
	Scroll scr_listUser;
	public Command cmdClose,cmdKich,cmdRoi,CmdGiaiTan;
	public Command cmdClanLocal,cmdClanGobal;
	public boolean isBoss,isBossGobal;
	public boolean isTabLocal;
	public mVector listUser_local = new mVector();
	public mVector listUser_gobal = new mVector();
	
	public static final int CREAT_CLAN_GLOBAL = 0; // tạo clan global
	 public static final int CREAT_CLAN_LOCAL = 1; // tạo clan local
	 public static final int LEAVE_CLAN = 2; // rời bỏ clan 
	 public static final int DELETE_CLAN = 3; // xóa clan dành cho ban chủ 
	 public static final int INVITA_CLAN = 4; // mời vào clan 
	 public static final int KICK_CLAN = 5; // kích khỏi clan
	 public static final int REQUEST_CLAN = 6; // yêu cầu danh sách clan
	 public static final int REQUETS_NAME = 7;
	 public static final int GET_LIST_LOCAL = 8;
	 public static final int GET_LIST_GOBAL = 9;
	 public static final int INVITE_CLAN_GLOBAL = 11;
	 
	 mVector listcmd = new mVector();
	 
	
	public ClanScreen()
	{
		wkhung = 240;
		hKhung = 200;

		xpaint = GameCanvas.w/2-wkhung/2;
		ypaint = GameCanvas.h/2-hKhung/2-10;
		cmdClose = new Command("", this, 2, null);
		cmdKich = new Command("Kích", this, 3, null);
		cmdRoi = new Command("Rời", this, 4, null);
		CmdGiaiTan = new Command("Giải tán", this, 5, null);
		cmdClanLocal = new Command("Clan Local", this, 6, null);
		cmdClanGobal = new Command("Clan Gobal", this, 7, null);
		listcmd.addElement(cmdKich);
		listcmd.addElement(cmdRoi);
		listcmd.addElement(CmdGiaiTan);
		
		int wbutton = Image.getWidth(LoadImageInterface.btnTab);
		int hbutton = Image.getHeight(LoadImageInterface.btnTab);
		for (int i = 0; i < listcmd.size(); i++) {
			Command cmd = (Command)listcmd.elementAt(i);
			if(cmd!=null)
			{
				cmd.setPos(xpaint + wkhung/2-(wbutton/2)*listcmd.size()-(wbutton/4)*(listcmd.size()-1)/2+i*(5*wbutton/4),
						ypaint+hKhung-10,
						LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
			}
		}
//		cmdKich.setPos(xpaint + wkhung/2-7*wbutton/4,
//				ypaint+hKhung-10, LoadImageInterface.img_use,LoadImageInterface.img_use_focus);
//		cmdRoi.setPos(xpaint + wkhung/2-wbutton/2,
//				ypaint+hKhung-10,LoadImageInterface.img_use,LoadImageInterface.img_use_focus);
//		CmdGiaiTan.setPos(xpaint + wkhung/2+3*wbutton/4,
//				ypaint+hKhung-10,LoadImageInterface.img_use,LoadImageInterface.img_use_focus);

		cmdClose.setPos(xpaint+wkhung- LoadImageInterface.closeTab.width/2,
				ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
		cmdClanLocal.setPos(xpaint + wkhung-10,
				ypaint+hKhung/2-hbutton*2, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
		cmdClanGobal.setPos(xpaint + wkhung-10,
				ypaint+hKhung/2-hbutton/2, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
		
		scr_listUser = new Scroll();
	}
	
	public static ClanScreen gI()
	{
		if(me==null)
			return me = new ClanScreen();
		return me;
	}
	
	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		Cout.println2222("perform id  "+idAction);
		switch (idAction) {
		case 2:
			GameScreen.gI().switchToMe();
			break;
		case 3:
			if(scr_listUser.selectedItem>-1)
			{
				if(isTabLocal&&scr_listUser.selectedItem<listUser_local.size())
				{
					OtherChar other = (OtherChar)listUser_local.elementAt(scr_listUser.selectedItem);
					if(other!=null)
					{
						Service.gI().Clan_KichKhoiBang((short)other.id);
					}
				}else if(!isTabLocal&&scr_listUser.selectedItem<listUser_gobal.size())
				{
					OtherChar other = (OtherChar)listUser_gobal.elementAt(scr_listUser.selectedItem);
					if(other!=null)
					{
						Service.gI().Clan_KichKhoiBang((short)other.id);
					}
				}
			}
			break;
		case 4:
			Service.gI().Clan_RoiBang();
			break;
		case 5:
			Service.gI().Clan_XoaBang();
			break;
		case 6:
			isTabLocal = true;
			if(isBoss)
			{
				listcmd.removeAllElements();
				listcmd.addElement(cmdKich);
				listcmd.addElement(cmdRoi);
				listcmd.addElement(CmdGiaiTan);
			}else {
				listcmd.removeAllElements();
				listcmd.addElement(cmdRoi);
			}
			break;
		case 7:
			isTabLocal = false;
			if(isBossGobal)
			{
				listcmd.removeAllElements();
				listcmd.addElement(cmdKich);
				listcmd.addElement(cmdRoi);
				listcmd.addElement(CmdGiaiTan);
			}else {
				listcmd.removeAllElements();
				listcmd.addElement(cmdRoi);
			}
			break;
		}
	}

	@Override
	public void switchToMe() {
		// TODO Auto-generated method stub
		isTabLocal = true;
		if(isBoss)
		{
			listcmd.removeAllElements();
			listcmd.addElement(cmdKich);
			listcmd.addElement(cmdRoi);
			listcmd.addElement(CmdGiaiTan);
		}else {
			listcmd.removeAllElements();
			listcmd.addElement(cmdRoi);
		}
		super.switchToMe();
	}

	@Override
	public void keyPress(int keyCode) {
		// TODO Auto-generated method stub
		super.keyPress(keyCode);
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		super.update();
		GameScreen.gI().update();
	}

	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		super.updateKey();
		if (GameCanvas.keyPressed[13] || Screen.getCmdPointerLast(cmdClose)) {
			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
				{
					cmdClose.performAction();
					GameCanvas.clearPointerEvent();
				}
			}
		}
		for (int i = 0; i < listcmd.size(); i++) {
			Command cmd = (Command)listcmd.elementAt(i);
			if(cmd!=null)
			{
				if (Screen.getCmdPointerLast(cmd)) {
					if (cmd != null) {
						GameCanvas.isPointerJustRelease = false;
						GameCanvas.keyPressed[5] = false;
						Screen.keyTouch = -1;
						if (cmd != null)
						{
							cmd.performAction();
							GameCanvas.clearPointerEvent();
						}
					}
				}
			}
		}
//		if (Screen.getCmdPointerLast(cmdKich)) {
//			if (cmdKich != null) {
//				GameCanvas.isPointerJustRelease = false;
//				GameCanvas.keyPressed[5] = false;
//				Screen.keyTouch = -1;
//				if (cmdKich != null)
//				{
//					cmdKich.performAction();
//					GameCanvas.clearPointerEvent();
//				}
//			}
//		}
//		if (Screen.getCmdPointerLast(cmdRoi)) {
//			if (cmdRoi != null) {
//				GameCanvas.isPointerJustRelease = false;
//				GameCanvas.keyPressed[5] = false;
//				Screen.keyTouch = -1;
//				if (cmdRoi != null)
//				{
//					cmdRoi.performAction();
//					GameCanvas.clearPointerEvent();
//				}
//			}
//		}
//		if (Screen.getCmdPointerLast(CmdGiaiTan)) {
//			if (CmdGiaiTan != null) {
//				GameCanvas.isPointerJustRelease = false;
//				GameCanvas.keyPressed[5] = false;
//				Screen.keyTouch = -1;
//				if (CmdGiaiTan != null)
//				{
//					CmdGiaiTan.performAction();
//					GameCanvas.clearPointerEvent();
//				}
//			}
//		}
		if (Screen.getCmdPointerLast(cmdClanLocal)) {
			if (cmdClanLocal != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClanLocal != null)
				{
					cmdClanLocal.performAction();
					GameCanvas.clearPointerEvent();
				}
			}
		}
		if (Screen.getCmdPointerLast(cmdClanGobal)) {
			if (cmdClanGobal != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClanGobal != null)
				{
					cmdClanGobal.performAction();
					GameCanvas.clearPointerEvent();
				}
			}
		}
		ScrollResult s1 = scr_listUser.updateKey();
		scr_listUser.updatecm();
	}

	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
		GameScreen.gI().paint(g);
		g.setColor(0xff000000,GameCanvas.opacityTab);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		
		Paint.paintFrameNaruto(xpaint,ypaint,wkhung,hKhung+2,g);
		Paint.PaintBoxName("Bang hội",xpaint+wkhung/2-40,ypaint,80,g);
		if(scr_listUser.selectedItem>-1)
		{

//			cmdRoi.paint(g);
//			cmdKich.paint(g);
//			CmdGiaiTan.paint(g);
			for (int i = 0; i < listcmd.size(); i++) {
				Command cmd = (Command)listcmd.elementAt(i);
				if(cmd!=null)
					cmd.paint(g);
			}
		}
		cmdClanGobal.paint(g);
		cmdClanLocal.paint(g);
		cmdClose.paint(g);
		if(scr_listUser.selectedItem>-1)
		{
			if(cmdKich.isFocusing())
				cmdKich.paint(g);
			if(cmdRoi.isFocusing())
				cmdRoi.paint(g);
			if(CmdGiaiTan.isFocusing())
				CmdGiaiTan.paint(g);
		}
		scr_listUser.setStyle(isTabLocal==true?listUser_local.size():listUser_gobal.size(), 24, xpaint+20,ypaint+40,wkhung-40,hKhung-50, true, 1);
		scr_listUser.setClip(g,xpaint+20,ypaint+40,wkhung-40,hKhung-50);
		if(isTabLocal){
			for (int i = 0; i < listUser_local.size(); i++) {
				OtherChar user = (OtherChar)listUser_local.elementAt(i);
				if(user!=null&&user.name!=null)
					mFont.tahoma_7_white.drawString(g, user.name, xpaint+wkhung/2, ypaint+40+i*24, 2);
			}
		}else {
			for (int i = 0; i < listUser_gobal.size(); i++) {
				OtherChar user = (OtherChar)listUser_gobal.elementAt(i);
				if(user!=null&&user.name!=null)
					mFont.tahoma_7_white.drawString(g, user.name, xpaint+wkhung/2, ypaint+40+i*24, 2);
			}
		}
		GameCanvas.resetTrans(g);
		super.paint(g);
	}

	@Override
	public void updatePointer() {
		// TODO Auto-generated method stub
		super.updatePointer();
	}

}
