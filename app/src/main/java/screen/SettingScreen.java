package screen;

import com.sakura.thelastlegend.gui.GuiMain;
import com.sakura.thelastlegend.lib.LoadImageInterface;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Music;
import com.sakura.thelastlegend.lib.Rms;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;

public class SettingScreen extends Screen implements IActionListener{

	public static SettingScreen instance;
	public int wkhung,hKhung,xpaint,ypaint;
	public boolean[] isListAuto = new boolean[5];

	public static byte[] defautAuto = new byte[]{1,1,0,1,0};
	public static final int MUSIC     = 0;
	public static final int SOUND	   = 1;
	public static final int TYPE_MOVE = 2;
	public static final int AUTO_DANH = 3;
	public static final int AUTO_NHAT = 4;
	public Command cmdClose;

	public String[] listText = {"Âm nhạc nền","Âm thanh game","Analog di chuyển","Tự động đánh",/*"Tự động nhặt vật phẩm"*/};
	public SettingScreen()
	{
		wkhung =200;
		hKhung = 150;

		xpaint = GameCanvas.w/2-wkhung/2;
		ypaint = GameCanvas.h/2-hKhung/2-10;
		cmdClose = new Command("", this, 2, null);
		cmdClose.setPos(xpaint+wkhung- LoadImageInterface.closeTab.width/2,
				ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}
	public static SettingScreen gI()
	{
		if(instance==null) instance = new SettingScreen();
		return instance;
	}
	@Override
	public void switchToMe() {
		// TODO Auto-generated method stub
		super.switchToMe();
		byte[] listauto = Rms.loadRMS(Rms.rms_Auto);
		if(listauto==null|| listauto.length < SettingScreen.defautAuto.length)
		{
			Rms.saveRMS(Rms.rms_Auto, defautAuto);
		}else {
			for (int i = 0; i < listauto.length; i++) {
				if(listauto[i]==1)
					isListAuto[i] = true;
				else isListAuto[i] = false;
			}
		}
	}
	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		super.updateKey();
		if (GameCanvas.keyPressed[13] || Screen.getCmdPointerLast(cmdClose)) {
			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
				{
					cmdClose.performAction();
				}
			}
		}
		for (int i = 0; i < listText.length; i++) {
			if(GameCanvas.isPointerJustRelease&&
					GameCanvas.isPoint(xpaint+20, ypaint+31+20*i, wkhung-40, 18))
			{
				GameCanvas.isPointerJustRelease = false;
				if(i==MUSIC)
				{
					isListAuto[MUSIC] = !isListAuto[MUSIC];
					Music.isplayMusic = isListAuto[MUSIC];
					if(isListAuto[MUSIC])
					{
						Music.resumeMusic();
					}else Music.pauseMusic();
				}else if(i==SOUND){
					isListAuto[SOUND] = !isListAuto[SOUND];
					Music.isplaySound = isListAuto[SOUND];
					if(isListAuto[SOUND])
					{
					}else Music.stopSound(Music.currentSound);;
				}else if(i==AUTO_DANH){
					isListAuto[AUTO_DANH] = !isListAuto[AUTO_DANH];
					GameScreen.isAutoDanh = isListAuto[AUTO_DANH];
				}
				else if(i==AUTO_NHAT){
					isListAuto[AUTO_NHAT] = !isListAuto[AUTO_NHAT];
					GameScreen.isAutoNhatItem = isListAuto[AUTO_NHAT];
				}else if(i==TYPE_MOVE){
					isListAuto[TYPE_MOVE] = !isListAuto[TYPE_MOVE];
					GuiMain.isAnalog = isListAuto[TYPE_MOVE];
				}
				byte[] listauto = new byte[]{0,0,0,0,0};
				for (int j = 0; j < listauto.length; j++) {
					if(isListAuto[j])
						listauto[j] = 1;
				}
				Rms.saveRMS(Rms.rms_Auto, listauto);
				break;
				
			}
		}
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		super.update();
		GameScreen.gI().update();
	}
	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
		GameScreen.gI().paint(g);
		g.setColor(0xff000000,GameCanvas.opacityTab);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		Paint.paintFrameNaruto(xpaint,ypaint,wkhung,hKhung+2,g);
		Paint.PaintBoxName("Cài Đặt",xpaint+wkhung/2-40,ypaint,80,g);
		for (int i = 0; i < listText.length; i++) {
			mFont.tahoma_7.drawString(g, listText[i], xpaint+20, ypaint+40+20*i, 0);
			
			g.drawRegion(LoadImageInterface.imgCheckSetting, 0, (isListAuto[i]==true?1:0)* LoadImageInterface.imgCheckSetting.getHeight()/2,
					LoadImageInterface.imgCheckSetting.getWidth(), LoadImageInterface.imgCheckSetting.getHeight()/2,
					0,xpaint+wkhung-30, ypaint+45+20*i, mGraphics.VCENTER|mGraphics.HCENTER);
		}
		cmdClose.paint(g);
	}
	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 2:
			GameScreen.gI().switchToMe();
			break;

		default:
			break;
		}
	}

}
