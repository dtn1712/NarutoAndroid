package screen;

import com.sakura.thelastlegend.real.Service;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Rms;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.SmallImage;

public class DownloadImageScreen extends Screen{

	public long timeCoolDownNextImage = 15000,timeStart;
	public static boolean isOKNext,isDownLoadedSuccess;
	public static int idNext,idTo;
	public boolean isTheoList;
	public int idAdd;
	public int indexPhanTram,indexpaintload;
	public int[] listId;
	public mBitmap imgBgLoading,frameLoading,ongLoading;
	public mBitmap[] imgFrameChaLoadingr = new mBitmap[6];;
	@Override
	public void switchToMe() {
		// TODO Auto-generated method stub
		isDownLoadedSuccess = isTheoList = false;
		idNext = 0;
		idTo=0;
		indexPhanTram = indexpaintload = 0;
		super.switchToMe();
	}
	public void switchToMe(int idScreenTo) {
		// TODO Auto-generated method stub
		isDownLoadedSuccess = isTheoList = false;
		idNext = 0;
		indexPhanTram = indexpaintload = 0;
		this.idTo = idScreenTo;
		super.switchToMe();
	}
	public void switchToMe(int idAdd,int[] listId) {
		// TODO Auto-generated method stub
		this.idAdd = idAdd;
		this.listId = listId;
		isDownLoadedSuccess = false;
		 isTheoList = true;
		idNext = 0;
		indexPhanTram = indexpaintload = 0;
		super.switchToMe();
	}
	public boolean loadingImg(){
		if(imgBgLoading==null)
			imgBgLoading = GameCanvas.loadImage("/GuiNaruto/loading/imgBgLoading.png");
		if(frameLoading==null)
			frameLoading = GameCanvas.loadImage("/GuiNaruto/loading/ongLoading.png");
		if(ongLoading==null)
			ongLoading = GameCanvas.loadImage("/GuiNaruto/loading/frameLoading.png");
		if(imgFrameChaLoadingr[0]==null){
			for (int i = 0; i < imgFrameChaLoadingr.length; i++) {
				if(imgFrameChaLoadingr[i]==null)
				imgFrameChaLoadingr[i] = GameCanvas.loadImage("/GuiNaruto/loading/image-"+(i+1)+".png");
			}
		}
		return false;
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		loadingImg();
		if(idNext<(isTheoList==false?SmallImage.nBigImage:listId.length)){
			indexPhanTram = (idNext)*frameLoading.getWidth()/(isTheoList==false?SmallImage.nBigImage:listId.length);
			indexpaintload += (indexPhanTram-indexpaintload)/2;
			if(indexpaintload>=frameLoading.getWidth()) indexpaintload = frameLoading.getWidth();
		}else indexpaintload = frameLoading.getWidth();
		if(!isTheoList&&idNext<SmallImage.nBigImage){
			byte[] data = Rms.loadRMS(mSystem.getPathRMS(SmallImage.getPathImage(idNext))+""+idNext);
			if(data!=null){
				idNext++;
				if(idNext>=SmallImage.nBigImage){
					isDownLoadedSuccess = true;
					Rms.saveRMSData(SmallImage.keyOKDownloaded, new byte[]{1});
				}
				data = null;
			}
			else if(isOKNext||(mSystem.currentTimeMillis()-timeStart)>timeCoolDownNextImage){
				timeStart = mSystem.currentTimeMillis();
				Service.gI().requestImage(idNext,(byte)0);
				isOKNext = false;
				idNext++;
				if(idNext>=SmallImage.nBigImage){
					isDownLoadedSuccess = true;
					Rms.saveRMSData(SmallImage.keyOKDownloaded, new byte[]{1});
				}
			}
		}else if(isTheoList&&idNext<listId.length){
			byte[] data = Rms.loadRMS(mSystem.getPathRMS(SmallImage.getPathImage(listId[idNext]+idAdd)+""+(listId[idNext]+idAdd)));
			if(listId!=null)
			if(data!=null){
				idNext++;
				if(idNext>=listId.length){
					isDownLoadedSuccess = true;
					Rms.saveRMSData(SmallImage.keyOKDownloaded, new byte[]{1});
					//WaitingScreen.gI().switchToMe();
				}
				data = null;
			}
			else if(isOKNext||(mSystem.currentTimeMillis()-timeStart)>timeCoolDownNextImage){
				timeStart = mSystem.currentTimeMillis();
				Service.gI().requestImage(listId[idNext]+idAdd,(byte)0);
				isOKNext = false;
				idNext++;
				Rms.saveRMSData(SmallImage.keyOKDownloaded, new byte[]{0});
				if(idNext>=listId.length){
					//WaitingScreen.gI().switchToMe();
					isDownLoadedSuccess = true;
				}
			}
		}
		if(isDownLoadedSuccess&&GameCanvas.gameTick%7==0){
			if(!isTheoList){
				if(idTo==0)
				SelectCharScreen.gI().switchToMe();
				else if(idTo==1)
					CreateCharScreen.gI().switchToMe();
			}else WaitingScreen.gI().switchToMe();
		}
		super.update();
	}

	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		g.setColor(0xff000000);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
		//g.drawImage(imgBgLoading, GameCanvas.w/2, GameCanvas.h/2, mGraphics.VCENTER|mGraphics.HCENTER);
		g.drawRegionScalse(imgBgLoading, 0, 0, imgBgLoading.getWidth(), imgBgLoading.getHeight(), 0,
				GameCanvas.w/2, GameCanvas.h/2, mGraphics.VCENTER|mGraphics.HCENTER, false, (GameCanvas.w*100)/imgBgLoading.getWidth());
		g.setColor(0xff3c3a39);
		g.fillRect(GameCanvas.w/2-frameLoading.getWidth()/2,7*GameCanvas.h/8-frameLoading.getHeight()/2,
				frameLoading.getWidth(), frameLoading.getHeight());
		g.drawRegion(frameLoading, 0, 0, indexpaintload, frameLoading.getHeight(),
				0,GameCanvas.w/2-frameLoading.getWidth()/2, 7*GameCanvas.h/8-frameLoading.getHeight()/2, 
				mGraphics.TOP|mGraphics.LEFT);
//		g.drawImage(frameLoading, GameCanvas.w/2, 7*GameCanvas.h/8, mGraphics.VCENTER|mGraphics.HCENTER);
		g.drawImage(ongLoading, GameCanvas.w/2, 7*GameCanvas.h/8, mGraphics.VCENTER|mGraphics.HCENTER);
//		mFont.tahoma_7_white.drawString(g, idNext+"/"+(isTheoList==false?SmallImage.nBigImage:listId.length),
//				GameCanvas.w/2, GameCanvas.h/2, 2);
		if(imgFrameChaLoadingr!=null&&imgFrameChaLoadingr[(GameCanvas.gameTick/2)%6]!=null)
			g.drawImage(imgFrameChaLoadingr[(GameCanvas.gameTick/2)%6],
					GameCanvas.w/2-frameLoading.getWidth()/2+(indexpaintload),
					7*GameCanvas.h/8-18, mGraphics.VCENTER|mGraphics.HCENTER);
		
		super.paint(g);
	}

	public static DownloadImageScreen intansce;
	
	public static DownloadImageScreen gI(){
		if(intansce==null) intansce = new DownloadImageScreen();
		intansce.loadingImg();
		return intansce;
	}
}
