package screen;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.real.Service;
import Objectgame.Char;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

public class FlagScreen extends Screen implements IActionListener{
	
	public static FlagScreen instance;
	
	public static final byte LOW_LAYER_REGION = 0;
    public static final byte MED_PLAYER_REGION = 1;
    public static final byte HIGH_PLAYER_REGION = 2;
    
	public byte[] listKhu=new byte[]{0,1,2,3,4,5,6,7,8,9,-1};
	Command cmdClose;
	public int wKhung=170,hKhung=150;
	public int xpaint,ypaint;
	public Scroll srclist =new Scroll();
	public int coutFc,lastSelect;
	public int minKhu = 15;
	
	public int day,hour,minute,second,value;
	public static long time;
	public long timeRemove=180000,timeOld; // mili giấy server trả về là s;
	public String sday = "";
	
	public FlagScreen(){
		xpaint = GameCanvas.w/2-wKhung/2;
		ypaint = GameCanvas.h/2-hKhung/2;
		cmdClose = new Command("", this, 2, null);
		cmdClose.setPos(xpaint+wKhung- LoadImageInterface.closeTab.width/2,
				ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}
	
	public static FlagScreen gI(){
		if(instance==null) instance = new FlagScreen();
		return instance;
	}
	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdClose)) {
			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
				{
					cmdClose.performAction();
				}
			}
		}
		ScrollResult s1 = srclist.updateKey();
		srclist.updatecm();
		if(GameCanvas.isPointerJustRelease&&srclist.selectedItem!=-1&&srclist.selectedItem<listKhu.length){
			if(listKhu[srclist.selectedItem]!=Char.myChar().typePk){
				if(lastSelect==srclist.selectedItem){
					Service.gI().ChangeFlag((byte)listKhu[srclist.selectedItem]);
					GameScreen.gI().switchToMe();
				}
				lastSelect = srclist.selectedItem;
				GameCanvas.isPointerJustRelease = false;
			}
		}

		super.updateKey();
	}
	@Override
	public void updatePointer() {
		// TODO Auto-generated method stub
		super.updatePointer();
		
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		super.update();
		GameScreen.gI().update();
		if(GameCanvas.gameTick%4 ==0){
			coutFc++;
			if(coutFc>2)
				coutFc =0;
		}
		if(Char.myChar().typePk>-1){
			long currtime = mSystem.currentTimeMillis();
			if (currtime-time>=timeRemove) {
				sday="";
			}
			else{
				int secon = second;
				if((timeRemove-(currtime-time))/1000<=60){
					second = (int)(timeRemove-(currtime-time))/1000;
					day =minute =hour= 0;
				}else if((timeRemove-(currtime-time))/1000<=3600){
					second = (int)(timeRemove-(currtime-time))/1000;
					minute = second/60;
					second = second%60;
					second = hour=day= 0;
				}else if((timeRemove-(currtime-time))/1000<=86400){
					second = (int)(timeRemove-(currtime-time))/1000;
					hour = second/3600;
					second =minute=day= 0;
				}
				else{
					second = (int)(timeRemove-(currtime-time))/1000;
					day = second/86400;
					second =minute =hour= 0;
				}
			}

			sday = (day<=0?"":(day<10?"0"+day:day+"")+"d")+
					(hour<=0?"":(hour<10?"0"+hour:hour+"")+"h")+
					(minute<=0?"":(minute<10?"0"+minute:minute+"")+"'")+
					(second<=0?"":(second<10?"0"+second:second+"")+"s")+"";
		}else sday="";
	}
	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
		GameScreen.gI().paint(g);
		g.setColor(0xff000000,GameCanvas.opacityTab);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		Paint.paintFrameNaruto(xpaint,ypaint,wKhung,hKhung+2,g);
		Paint.PaintBoxName("Cờ",xpaint+wKhung/2-40,ypaint,80,g);
		cmdClose.paint(g);
		if(listKhu!=null){
			srclist.setStyle((listKhu.length>minKhu?listKhu.length:minKhu)/5, Image.getWidth(LoadImageInterface.ImgItem),
					xpaint+15,ypaint+22+Image.getHeight(LoadImageInterface.ImgItem)/4,
					wKhung-30,hKhung-32, true, 5);
			srclist.setClip(g, xpaint+15,ypaint+22+Image.getHeight(LoadImageInterface.ImgItem)/4,
					wKhung-30,hKhung-32);
			for (int i = 0; i < (listKhu.length>minKhu?listKhu.length:minKhu)/5; i++) {
				for(int j=0;j<5;j++)
				{
					g.drawImage(LoadImageInterface.ImgItem,xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j,ypaint+30+(Image.getHeight(LoadImageInterface.ImgItem))*i+2, 0,true);
					if((i*5+j)<listKhu.length)
					{
						if(listKhu[i*5+j]>-1){
							g.drawRegion(LoadImageInterface.iconpk, 0, 12*(listKhu[i*5+j] * 3 + (GameCanvas.gameTick / 3) % 3), 12, 12, 0,
									xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
									ypaint+30+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getWidth(LoadImageInterface.ImgItem)/2,
									mGraphics.VCENTER|mGraphics.HCENTER);
							if(listKhu[i*5+j]==Char.myChar().typePk){
								mFont.tahoma_7_white.drawString(g,sday, xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
										ypaint+32+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getWidth(LoadImageInterface.ImgItem)/2, 2);
							}
						}
						else{
							mFont.tahoma_7_white.drawString(g,"Tháo", xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
									ypaint+22+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getWidth(LoadImageInterface.ImgItem)/2, 2);
						}
					}
//					if(listKhu[i*5+j][0]==0)
//						mFont.tahoma_7_green.drawString(g, (i*5+j+1)+"",
//								xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
//								ypaint+23+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getHeight(LoadImageInterface.ImgItem)/2, 2);
//					else if(listKhu[i*5+j][0]==1)
//						mFont.tahoma_7_yellow.drawString(g, (i*5+j+1)+"",
//								xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
//								ypaint+23+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getHeight(LoadImageInterface.ImgItem)/2, 2);
//					else 
//						mFont.tahoma_7_red.drawString(g, (i*5+j+1)+"",
//							xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
//							ypaint+23+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getHeight(LoadImageInterface.ImgItem)/2, 2);
				
				}
			}
			if(srclist.selectedItem>=0&&srclist.selectedItem<listKhu.length)
			Paint.paintFocus(g,
					 (xpaint+15 + ((srclist.selectedItem % 5)*(Image.getWidth(LoadImageInterface.ImgItem))))+11- LoadImageInterface.ImgItem.getWidth()/4,
					 ypaint+30+ (srclist.selectedItem / 5) * (Image.getHeight(LoadImageInterface.ImgItem) ) +13- LoadImageInterface.ImgItem.getWidth()/4
					, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
			GameCanvas.resetTrans(g);
		}
		
	}
	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 2:
			GameScreen.gI().switchToMe();
			break;
		}
	}
}
