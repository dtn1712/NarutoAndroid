package screen;


import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.Rms;

public class SplashScreen extends Screen{

	public static mBitmap imgLogo;
	public static int splashScrStat;
	public static SplashScreen instance;

	static int time = 0;
	public SplashScreen() {
		instance = this;
	}

	public static void loadSplashScr() {
		splashScrStat = 0;
		time = 80;

	}

	public void update() {
//
		if (splashScrStat >= time) {
			if (Rms.loadRMSInt("indLanguage") >= 0) {
//				GameScreen.gI().switchToMe();
				GameCanvas.loginScreen.switchToMe();
			} else {
				GameCanvas.loginScreen.switchToMe();
			}
		}
		splashScrStat++;

	}

	public void paint(mGraphics g) {
		g.setColor(0xffffff);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

		if (imgLogo != null)
		{
			g.drawImage(imgLogo, GameCanvas.w >> 1, GameCanvas.h >> 1, mGraphics.HCENTER | mGraphics.VCENTER);
		}
	}
}
