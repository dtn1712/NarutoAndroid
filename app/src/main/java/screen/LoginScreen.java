package screen;


import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.mGraphics;

import java.util.Vector;

import com.sakura.thelastlegend.gui.GuiMain;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Char;
import Objectgame.Mob;
import Objectgame.TileMap;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Music;
import com.sakura.thelastlegend.lib.Rms;
import com.sakura.thelastlegend.lib.Session_ME;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.Util;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Menu;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.mResources;
import com.sakura.thelastlegend.real.Service;

public class LoginScreen extends Screen implements IActionListener{
	
	public TField tfIp;
	public TField tfUser;
	public TField tfPass;
	public TField tfRegPass;
	int focus;
	int wC, yL, defYL;
	public boolean isCheck = false, isRes = false;
	Command cmdLogin, cmdCheck, cmdFogetPass, cmdRes, cmdMenu,cmdLoginFB,cmdLoginGoogle;
	public String listFAQ = "", titleFAQ, subtitleFAQ;
	String numSupport = "";
	String strUser, strPass;
	public static boolean  isAutoLogin = false,isLogout,isTest=false;
	public static int indexLocal = 0;
	public static mBitmap imgTitle = GameCanvas.loadImage("/screen/title.png");
	public static LoginScreen gI ;
	public Mob test;
//	public static CreateCharScreen creatCharScr;
//	public static SelectCharScreen selectCharScr;

	public void switchToMe() {
		resetLogo();
		GameScreen.gH = GameCanvas.h;
//		if(GameCanvas.typeBg == 2)
			GameCanvas.loadBG(0);
//		else
//			GameCanvas.loadBG(TileMap.bgID);
		super.switchToMe();

		
		if (GameScreen.instance != null)
			GameScreen.instance = null;
		if (GameCanvas.menu != null)
			GameCanvas.menu = new Menu();
		
//		int indexLanguage = Rms.loadRMSInt("indLanguage");
//		if (indexLanguage <= 0)
//			mResources.languageID = mResources.Lang_VI;
//		else
//			mResources.languageID = mResources.Lang_EN;
		
		int isSoftKey = Rms.loadRMSInt("isSoftKey");
		if (isSoftKey <= 0) {
			Rms.saveRMSInt("isSoftKey", 1);
			GameScreen.isTouchKey = true;
		} else if (isSoftKey == 1) {
			GameScreen.isTouchKey = true;
		} else if (isSoftKey == 2)
			GameScreen.isTouchKey = false;
//		if(isAutoLogin){
//			String strLastServer = Rms.loadRMSString("lastServer");
//			if(strLastServer!=null)
//				GameMidlet.IP = strLastServer;
//			doLogin();
//		
//		}
		
		int sound = Rms.loadRMSInt("isSound");
		if (sound < 0) {
			Rms.saveRMSInt("isSound", 1);
			Music.isSound = true;
		} else if (sound == 1) {
			Music.isSound = true;
		} else if (sound == 2) {
			Music.isSound = false;
			Music.releaseAll();
		}
		Music.init();
		Music.play(Music.MLogin, 5);
		if(isLogout){
			isLogout = false;
			GameCanvas.gameScreen = null;
		}
	}
	int yt;
	public LoginScreen() {

		gI = this;
		// ==============================
		TileMap.bgID = (byte) (System.currentTimeMillis() % 9);
		if (TileMap.bgID == 5 || TileMap.bgID == 6)
			TileMap.bgID = 4;
		GameScreen.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
		GameScreen.cmx = 100;
		// ==============================
		String ip = Rms.loadRMSString("ipgame");
		if(ip!=null) GameMidlet.IP = ip;
		if (GameCanvas.h > 200) {
			defYL = GameCanvas.hh - 80;
		} else {
			defYL = GameCanvas.hh - 65;
		}
		resetLogo();
		wC = GameCanvas.w - 30;
		if (wC < 70)
			wC = 70;
		if (wC > 99)
			wC = 99;

//		yt = GameCanvas.hh - ITEM_HEIGHT - 5;
		yt = (GameCanvas.h-150);
		yt = (GameCanvas.h/2-100+60>0?GameCanvas.h/2-100+60:0);
		if (GameCanvas.h <= 160)
			yt = 20;
		tfUser = new TField();

		tfUser.x = GameCanvas.hw - 75;
		tfUser.y = yt + 20;
		tfUser.width = wC + 50;
		tfUser.height = ITEM_HEIGHT + 2;
		tfUser.isFocus = false;
		tfUser.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);
		TField.m = GameMidlet.instance;
		TField.c = GameCanvas.instance;

		tfPass = new TField();
		tfPass.x = GameCanvas.hw - 75;
		tfPass.y = (yt + 60);
		tfPass.width = wC + 50;
		tfPass.height = ITEM_HEIGHT + 2;
		tfPass.isFocus = false;
		tfPass.setIputType(TField.INPUT_TYPE_PASSWORD);
		
		tfIp = new TField();
		tfIp.x = GameCanvas.hw - 75;
		tfIp.y = (yt + 100);
		tfIp.width = wC + 50;
		tfIp.height = ITEM_HEIGHT + 2;
		tfIp.isFocus = false;
		tfIp.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);

		tfRegPass = new TField();
		tfRegPass.x = GameCanvas.hw - 20;
		tfRegPass.y = (yt+=35);
		tfRegPass.width = wC;
		tfRegPass.height = ITEM_HEIGHT + 2;
		tfRegPass.isFocus = false;
		tfRegPass.setIputType(TField.INPUT_TYPE_PASSWORD);
		
		isCheck = true;

		byte[] listauto = Rms.loadRMS(Rms.rms_Auto);
		if(listauto==null||listauto.length<SettingScreen.defautAuto.length)
		{
			Rms.saveRMS(Rms.rms_Auto, SettingScreen.defautAuto);
		}else {
			if(listauto[SettingScreen.AUTO_DANH]==1) GameScreen.isAutoDanh = true;
			else GameScreen.isAutoDanh = false;

            if (listauto[SettingScreen.AUTO_NHAT] == 1) GameScreen.isAutoNhatItem = true;
            else GameScreen.isAutoNhatItem = false;
            
			if(listauto[SettingScreen.MUSIC]==1) Music.isplayMusic = true;
			else Music.isplayMusic = false;

			if(listauto[SettingScreen.SOUND]==1) Music.isplaySound = true;
			else Music.isplaySound = false;

			if(listauto[SettingScreen.TYPE_MOVE]==1)GuiMain.isAnalog = true;
			else GuiMain.isAnalog = false;
		}
		int a = Rms.loadRMSInt("check");
		if (a == 1) {
			isCheck = true;
//			isAutoLogin = true;
		} else if (a == 2)
			isCheck = false;
		tfUser.setText(Rms.loadRMSString("acc"));//
		tfPass.setText(Rms.loadRMSString("pass"));
//		tfUser.setText(strUser);
//		tfPass.setText(strPass);

//		tfUser.setText("1111");
//		tfPass.setText("12345");
		focus = 0;
		cmdLogin = new Command(GameCanvas.w > 200 ? mResources.LOGIN1 : mResources.LOGIN2, this, 2000, null);
		cmdLogin.setPos(tfPass.x - 2 +80, tfPass.y + 30+(isTest?35:0), LoadImageInterface.btnLogin0 , LoadImageInterface.btnLogin1);
		cmdCheck = new Command(mResources.REMEMBER, this, 2001, null);
		cmdRes = new Command(mResources.REGISTER, this, 2002, null);
//		cmdRes.setPos(GameCanvas.w-LoadImageInterface.img_use.getWidth()-5,
//				GameCanvas.h-LoadImageInterface.img_use.getHeight()-5,LoadImageInterface.img_use ,LoadImageInterface.img_use_focus);
		cmdRes.setPos(tfPass.x - 2+38, tfPass.y + 30+(isTest?35:0), LoadImageInterface.btnLogin0 , LoadImageInterface.btnLogin1);
		cmdFogetPass = new Command(mResources.FORGETPASS, this, 2004, null); 
		left = cmdRes;
		cmdMenu = new Command(mResources.MENU, this, 2003, null);
		cmdMenu.setPos(tfPass.x - 2+40, tfPass.y + 30+(isTest?35:0), LoadImageInterface.btnLogin0 , LoadImageInterface.btnLogin1);
	
		cmdLoginFB = new Command("", this, 2010, null);
		cmdLoginFB.setPos(tfPass.x - 2, tfPass.y + 30+(isTest?35:0), LoadImageInterface.imgIconFb0 , LoadImageInterface.imgIconFb1);
		
		cmdLoginGoogle = new Command("", this, 2011, null);
		cmdLoginGoogle.setPos(tfPass.x + 120, tfPass.y + 30+(isTest?35:0), LoadImageInterface.imgIconGoogle0 , LoadImageInterface.imgIconGoogle1);
		
		
		if (GameCanvas.isTouch) {
			center = null;
			right = cmdLogin;
		} else {
			center = cmdLogin;
			right = tfUser.cmdClear;

		}
//		selectCharScr = new SelectCharScreen();
//		creatCharScr = new CreateCharScreen();

		// =================
		// GameCanvas.startDialogChain(t, -1, 100, 100);
		// ====================
		test = new Mob((short)0,100,100,0);
	}
	
//	private void doAskForGPRS() {
//		GameCanvas.msgdlg.setInfo("Bạn đang sử dụng mạng gì? (Bạn có thể chọn lại từ Menu)", new Command("3G/Wifi", new IAction() {
//			public void perform() {
//				doSetGPRS(false);
//				GameCanvas.endDlg();
//			}
//		}), null, new Command("GPRS (chậm)", new IAction() {
//			public void perform() {
//				doSetGPRS(true);
//				GameCanvas.endDlg();
//			}
//		}));
//		GameCanvas.currentDialog = GameCanvas.msgdlg;
//	}
//
//
//	protected void doSetGPRS(boolean isGPRS) {
//		System.out.println("ISGPRS=" + isGPRS);
//		GameCanvas.isGPRS = isGPRS;
//		Rms.saveRMSInt("isGPRS", isGPRS ? 1 : 2);
//	}
//	
	public static LoginScreen gI() {
		if (gI == null)
			gI = new LoginScreen();
		return gI;
	}
	
	protected void doMenu() {
		
	
		//Command update = new Command("Cập nhật game", this, 1000, null);
		//Command cskh = new Command("Gọi Hotline", this, 1001, null);
		Vector menu = new Vector();
		
		menu.addElement(new Command(mResources.NEWREGISTER, this, 1002, null));
		menu.addElement(cmdFogetPass);
		menu.addElement(new Command(mResources.FORUM, this, 1003, null));
		menu.addElement(new Command(mResources.CONFIG, this, 1006, null));
		
		menu.addElement(new Command(mResources.EXIT, GameCanvas.instance, 8885,null));
		GameCanvas.menu.startAt(menu, 0);
	}

	protected void doRegister() {
		if (tfUser.getText().equals("")) {
			GameCanvas.startOKDlg(mResources.NOT_INPUT_USERNAME);
			return;
		}
		char[] ch = tfUser.getText().toCharArray();
		for (int i = 0; i < ch.length; i++) {
			if (!TField.setNormal(ch[i])) {
				GameCanvas.startOKDlg(mResources.NOT_SPEC_CHARACTER);
				return;
			}
		}
		if (tfPass.getText().equals("")) {
			GameCanvas.startOKDlg(mResources.NOT_INPUT_PASS1);
			return;
		}
//		if (tfIp.getText().equals("")) {
//			GameCanvas.startOKDlg(mResources.NOT_INPUT_PASS2);
//			return;
//		}
		if (tfUser.getText().length() < 8) {
			GameCanvas.startOKDlg(mResources.USERNAME_LENGHT);
			return;
		}
//		if (!tfPass.getText().equals(tfIp.getText())) {
//			GameCanvas.startOKDlg(mResources.WRONG_PASSWORD);
//			return;
//		}
		GameCanvas.msgdlg.setInfo(mResources.REGISTER_TEXT[0]+ " " + tfUser.getText() + " với mật khẩu "+tfPass.getText()+" "+ mResources.REGISTER_TEXT[1], new Command(mResources.ACCEPT, this, 4000, null), null,new Command(mResources.NO, GameCanvas.instance, 8882, null));
		GameCanvas.currentDialog = GameCanvas.msgdlg;
	}

	protected void doRegister(String user) {
		isFAQ = false;
		//GameMidlet.IP = GameMidlet.IPS1;
		GameCanvas.startWaitDlg(mResources.CONNECTING);
		GameCanvas.connect();
		GameCanvas.startWaitDlg(mResources.REGISTERING);
//		Service.gI().setClientType();
		passRe = tfPass.getText();
		Service.gI().requestRegister(user, tfPass.getText(), GameMidlet.VERSION);
	}

	protected void doGetForgetPass(String user) {
		isFAQ = false;
		GameMidlet.IP = GameMidlet.IPS1;
		GameCanvas.startWaitDlg(mResources.CONNECTING);
		GameCanvas.connect();
		GameCanvas.startWaitDlg(mResources.PLEASEWAIT);
		//java
//		Service.gI().requestForgetPass(user);
		//iphone
		//Service.gI().requestForgetPass(user, imei, newpass);
	}

	String passRe = "";
	public boolean isFAQ = false;

	public void doViewFAQ() {
		if (listFAQ.equals(""))
			// loadFAQ();
			if (!listFAQ.equals("")) {
				// ViewMsg.gI().setInfo(listFAQ, titleFAQ, subtitleFAQ);
				// ViewMsg.gI().switchToMe(this);
			}
		if (!Session_ME.gI().connected) {
			isFAQ = true;
			GameCanvas.connect();
		}
		GameCanvas.startWaitDlg();
		// Service.gI().doViewRequest(0, listFAQ.hashCode());

	}
	protected void doSelectServer(){
		Vector vServer = new Vector();
		if (indexLocal == 1)
			vServer.addElement(new Command("LOCAL 44", this, 200041, null));
		else if (indexLocal == 2)
			vServer.addElement(new Command("LOCAL 46", this, 200042, null));
		if(GameMidlet.indexClient == 0){
			vServer.addElement(new Command("Server Bokken", this, 20001, null));
			vServer.addElement(new Command("Server Shuriken", this, 20002, null));
			vServer.addElement(new Command("Server Tessen", this, 20003, null));
			vServer.addElement(new Command("Server Kunai ("+mResources.NEW+")", this, 20004, null));
			vServer.addElement(new Command(mResources.LIEN_DAU, this, 20005, null));
			GameCanvas.menu.startAt(vServer, 0);
		}else{
			GameCanvas.menu.showMenu = false;
			GameMidlet.IP = GameMidlet.IPS1;
			saveLastServer(GameMidlet.IP);
			doLogin();
		}
	}
	protected void saveLastServer(String serverInfo){
		Rms.saveRMSString("lastServer", serverInfo);
		
	}
	
	
	String[] currentTip;
	int tipid=-1;
	protected void doLogin() {// can duoc sua lai
//		 tipid = GameCanvas.gameTick%mResources.tips.length;
//		currentTip = mFont.tahoma_7_white.splitFontArray(mResources.tips[tipid], GameCanvas.w-40);
		isFAQ = true;
//		doViewFAQ();
//		if(true){
//			CreateCharScreen.gI().switchToMe();
//			return;
//		}
//		if(tfUser.getText().toLowerCase().trim().equals("testtrue"))
//		{
//			tfUser.setText("");
//			isTest = true;
//			cmdRes.setPos(tfPass.x - 2, tfPass.y + 35+(isTest?35:0),LoadImageInterface.img_use ,LoadImageInterface.img_use_focus);
//			cmdLogin.setPos(tfPass.x + 90, tfPass.y + 35+(isTest?35:0),LoadImageInterface.img_use ,LoadImageInterface.img_use_focus);
//			return;
//		}
		GameCanvas.startWaitDlg("Đang đăng nhập");
		String user = tfUser.getText().trim();//.toLowerCase()
		String pass = tfPass.getText().trim();//.toLowerCase()

		if(tfIp.getText()!=null&&tfIp.getText().trim().length()>0){
			String ip = tfIp.getText();

			if(ip.contains("shutdown")) SelectCharScreen.isShutdown = true;
			
			if(ip.contains("@"))
			{
				try {
					String[] text_ip = ip.split("@");
					if(text_ip.length>1)
					{

						Rms.saveRMSString("ipgame", text_ip[0]);
						GameMidlet.IP = text_ip[0].toLowerCase().trim();
						GameMidlet.VERSION = text_ip[1];
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}else {
				Rms.saveRMSString("ipgame", tfIp.getText());
				GameMidlet.IP = tfIp.getText().toLowerCase().trim();
			}
		}
		savePass(user, pass);
//		//-------------------init value
//		user="duigaluoc";
//		pass ="njgaga";
		if (user.equals("a") && pass.equals("a"))
			indexLocal = 1;
		else if (user.equals("b") && pass.equals("b"))
			indexLocal = 2;
		if (user == null || pass == null || GameMidlet.VERSION == null ||  user.equals("")) {
			return;
		} 
		else if (user.equals("")) {
			return;
		} else if (pass.equals("")) {
			focus = 1;
			tfUser.isFocus = false;
			tfPass.isFocus = true;
			right = tfPass.cmdClear;
			return;
		}
//		if(!isAutoLogin)
//			GameCanvas.startWaitDlg(mResources.CONNECTING);
		
		GameCanvas.connect();
		
//		Service.gI().setClientType();
//		Service.gI().login(user, pass, GameMidlet.VERSION);
		
		
		byte[] listauto = Rms.loadRMS(Rms.rms_Auto);
		if(listauto==null|| listauto.length < SettingScreen.defautAuto.length)
		{
			
			Rms.saveRMS(Rms.rms_Auto, SettingScreen.defautAuto);
		}else {
			if(listauto[SettingScreen.AUTO_DANH]==1) GameScreen.isAutoDanh = true;
			else GameScreen.isAutoDanh = false;

            if (listauto[SettingScreen.AUTO_NHAT] == 1) GameScreen.isAutoNhatItem = true;
            else GameScreen.isAutoNhatItem = false;
		}
		if(GameMidlet.VERSION.equals("0.0.5"))
		Cout.println2222(GameScreen.isAutoNhatItem+" GameScreen.isAutoDanh  "+ GameScreen.isAutoDanh);
		Service.gI().doLogin(user, pass,(byte)mGraphics.zoomLevel);//
		isLoggingIn = true;
		savePass(user, pass);
		focus = 0;
	}

	public void savePass(String user, String pass) {
//		if (isCheck) {
			Rms.saveRMSInt("check", 1);
			Rms.saveRMSString("acc", user);
			Rms.saveRMSString("pass", pass);
//		} else {
//			Rms.saveRMSInt("check", 2);
//			Rms.saveRMSString("acc", "");
//			Rms.saveRMSString("pass", "");
//		}
	}


	public void update() {
//		if(isAutoLogin)
//			return;
//		test.update();
		if(GameCanvas.imgCloud==null&&GameCanvas.gameTick%2==0&&GameCanvas.imgBG!=null&&GameCanvas.imgBG[0]==null) 
			GameCanvas.loadBG(0);
		GameScreen.cmx++;
		if (GameScreen.cmx > GameCanvas.w * 3 + 100)
			GameScreen.cmx = 100;

		tfUser.update();
		tfPass.update();
		if(isTest){
		tfIp.update();
		if (isRes)
			tfRegPass.update();
		}
		updateLogo();
		if (GameCanvas.isTouch) {
			center =null;
			if (isRes)
				right = cmdRes;
			else
				right = cmdLogin;
		} else {
			if (isRes)
				center = cmdRes;
			else if (focus == 2) {
				center = cmdCheck;
				if (isCheck)
					center.caption = mResources.UNCHECK;
				else
					center.caption = mResources.REMEMBER;

			} else
				center = cmdLogin;
		}

		//
		if (g >= 0) {
			ylogo += dir * g;
			g += dir * v;
			if (g <= 0) {
				dir *= -1;
			}
			if (ylogo > 0) {
				dir *= -1;
				g -= 2 * v;
			}
		}
		if (tipid >= 0 && GameCanvas.gameTick % 100 == 0)
		{
			tipid++;
			if(tipid>=mResources.tips.length)tipid=0;
			currentTip = mFont.tahoma_7_white.splitFontArray(mResources.tips[tipid], GameCanvas.w-40);
		}
	}

	int v = 2;
	int g = 0;
	int ylogo = -40;
	int dir = 1;

	public void updateLogo() {
		if (defYL != yL)
			yL += (defYL - yL) >> 1;
	}

	public void keyPress(int keyCode) {

		if (tfUser.isFocus)
			tfUser.keyPressed(keyCode);

		else if (tfPass.isFocus)

			tfPass.keyPressed(keyCode);

		else if (isRes && tfRegPass.isFocus)

			tfRegPass.keyPressed(keyCode);

		super.keyPress(keyCode);
	}

	public void unLoad() {
		super.unLoad();
	}

	public void paint(mGraphics g) {
		g.setColor(0xff3aadfe);
		g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
//		if (isAutoLogin){
//			g.drawImage(SplashScreen.imgLogo, GameCanvas.w >> 1, GameCanvas.h >> 1, mGraphics.HCENTER | mGraphics.VCENTER);
//			String s = GameMidlet.VERSION;
//			if (isLoggingIn)
//				s=Session_ME.gI().strRecvByteCount;
//			mFont.tahoma_7_yellow.drawString(g,s , GameCanvas.w - 5, 5, 1);
//			GameCanvas.paintShukiren(15, GameCanvas.h-15, g, false);
//			return;
//		}

//		g.drawImage(LoadImageInterface.img_use,100, 100, 0);
//		g.drawRegionScalse(LoadImageInterface.img_use, 0, 0,
//				LoadImageInterface.img_use.getWidth(),
//				LoadImageInterface.img_use.getHeight(), 0, 100,200, 0,false,200);
//		if(true) return;
		GameCanvas.paintBGGameScr(g);
		GameCanvas.resetTrans(g);
		// TileMap.paintTilemap(g);
		int yTT = tfUser.y - 45;
		if (GameCanvas.h <= 220)
			yTT += 5;
		if (GameCanvas.currentDialog == null) {
//			GameCanvas.paint.paintFrame(GameCanvas.hw - 85, tfUser.y - 15, 170, 115, g);
			GameCanvas.paint.SubFrame(GameCanvas.hw - 85, yt-60/*tfUser.y - 30*/, 170, 160+(isTest?40:0), g);

			if (imgTitle != null)
//
//				g.drawRegionScalse(imgTitle, 0, 0, imgTitle.getWidth(), imgTitle.getHeight(), 0, 
//						 GameCanvas.hw, yt-66,  mGraphics.HCENTER | mGraphics.VCENTER,false, 125);
				g.drawImage(imgTitle, GameCanvas.hw, yt-66/*yTT + 10*/, 3);
			mFont.tahoma_7_white.drawString(g, "Email or Phone", tfUser.x, tfUser.y-12, 0);
			mFont.tahoma_7_white.drawString(g, "Password", tfPass.x, tfPass.y-12, 0);
			tfUser.paint(g);
			tfPass.paint(g);
			if(isTest){
			tfIp.paint(g);
			if (isRes)
				tfRegPass.paint(g);
			}
			g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
			int a = 0;
//			if (GameCanvas.w > 200) {
//				if(Rms.loadRMS("acc") == null && Rms.loadRMS("pass") == null){
//					mFont.tahoma_7b_white.drawString(g, mResources.USERNAME, tfUser.x, tfUser.y + 10, 0);
//					mFont.tahoma_7b_white.drawString(g, mResources.PASSWORD, tfPass.x, tfPass.y + 10, 0);
//					
//				}
//
//				if (isRes) {
//
//					mFont.tahoma_7b_white.drawString(g, mResources.REPASS, tfRegPass.x - 58, tfRegPass.y - 1, 0);
//					mFont.tahoma_7b_white.drawString(g, mResources.PASSWORD, tfRegPass.x - 58, tfRegPass.y + 13, 0);
//
//				}
//				a = 10;
//			} else {
//				if(Rms.loadRMS("acc") == null && Rms.loadRMS("pass") == null){
//					mFont.tahoma_7b_white.drawString(g, mResources.USER, tfUser.x, tfUser.y + 7, 0);
//					mFont.tahoma_7b_white.drawString(g, mResources.PASS, tfPass.x, tfPass.y + 7, 0);
//					
//				}
//
//				if (isRes) {
//
//					mFont.tahoma_7b_white.drawString(g, mResources.RE, tfRegPass.x - 35, tfRegPass.y - 1, 0);
//					mFont.tahoma_7b_white.drawString(g, mResources.PASS, tfRegPass.x - 35, tfRegPass.y + 13, 0);
//
//				}
//				a = 0;
//			}
//			if (!isRes) {
//				GameCanvas.paint.paintCheckPass(g, tfUser.x - 20 + a, yt + 7, isCheck, focus == 2);
//				mFont.tahoma_8b.drawString(g, mResources.SAVE_INFO, tfUser.x + a, yt + 4, 0);
//			}
		}
		else{
			if (currentTip != null)
				for (int i = 0; i < currentTip.length; i++)
					mFont.tahoma_7_white.drawString(g, currentTip[i], GameCanvas.w / 2, tfUser.y - 10 + 10 * i, 2);
		}
		
		//Font.tahoma_7_grey.drawString(g, GameMidlet.VERSION, GameCanvas.w - 5, 5, 1);
		String s = GameMidlet.VERSION;
		if (isLoggingIn)
			s=Session_ME.gI().strRecvByteCount;
		mFont.tahoma_7_grey.drawString(g,s , GameCanvas.w - 5, 5, 1);
		if(GameCanvas.currentDialog==null){
			cmdLoginFB.paint(g);
			cmdLoginGoogle.paint(g);
		}
//		left.x = tfPass.x - 33;
//		right.x = tfPass.x + 67;
//		left.y = right.y = tfPass.y + 80;
//		test.paint(g);
		super.paint(g);
		
	}

	public static boolean isLoggingIn;
	public void updateKey() {
		if(getCmdPointerLast(cmdLoginFB)){
			cmdLoginFB.performAction();
			GameCanvas.clearPointerEvent();
		}
		if(getCmdPointerLast(cmdLoginGoogle)){
			cmdLoginGoogle.performAction();
			GameCanvas.clearPointerEvent();
		}
//		if(isAutoLogin)
//			return;
		// //================
		// if(GameCanvas.showDlg){
		// if(GameCanvas.keyPressed[5]){
		// GameCanvas.keyPressed[5]=false;
		// GameCanvas.nextDlgStep();
		// if (GameCanvas.currentDlgStep == GameCanvas.nStepToShow)
		// GameCanvas.showDlg = false;
		// return;
		// }
		// }
		//		
		// //=================
		// if (GameCanvas.isPointerJustRelease) {
		// int a = 0;
		// if (GameCanvas.w > 200)
		// a = 10;
		// if (GameCanvas.isPointerHoldIn(xC - 30 + a, GameCanvas.hh + 31, 93,
		// 12)) {
		// if (!isRes && focus == 2)
		// cmdCheck.action.perform();
		// else
		// focus = 2;
		// } else
		// focus = 1;
		// }
		if(GameCanvas.keyPressed[5])
			doLogin();
		if (GameCanvas.keyPressed[2]) {
			focus--;
			if (focus < 0)
				focus = 2;
		} else if (GameCanvas.keyPressed[8]) {
			focus++;
			if (focus > 2)
				focus = 0;
		}
		if (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[8]) {
			GameCanvas.clearKeyPressed();
			if (focus == 1) {
				tfUser.isFocus = false;
				tfPass.isFocus = true;
				tfRegPass.isFocus = false;
				tfIp.isFocus = false;
				right = tfPass.cmdClear;
			} else if (focus == 0) {
				tfUser.isFocus = true;
				tfPass.isFocus = false;
				tfRegPass.isFocus = false;
				tfIp.isFocus = false;
				right = tfUser.cmdClear;
			} else {
				tfUser.isFocus = false;
				tfPass.isFocus = false;
				if (isRes) {
					tfRegPass.isFocus = true;
					right = tfRegPass.cmdClear;
				}
			}
		}
		if(GameCanvas.keyPressed[14]){
			currentTip = null;
			GameCanvas.msgdlg.setInfo(mResources.DOYOUWANTEXIT2, new Command(mResources.YES, this, 5001, null), null, new Command(mResources.NO, this, 5002, null));
			GameCanvas.currentDialog = GameCanvas.msgdlg;
					
			
		}
		if (GameCanvas.isPointerJustRelease) {
			if (GameCanvas.isPointerHoldIn(tfUser.x, tfUser.y, tfUser.width, tfUser.height)) {
//				tfUser.isFocus = true;
				tfPass.isFocus =false;
				tfRegPass.isFocus = false;
				tfIp.isFocus = false;
			} else if (GameCanvas.isPointerHoldIn(tfPass.x, tfPass.y, tfPass.width, tfPass.height)) {
				tfUser.isFocus = false;
//				tfPass.isFocus = true;
				tfRegPass.isFocus = false;
				tfIp.isFocus = false;
			} else if(GameCanvas.isPointerHoldIn(tfIp.x, tfIp.y, tfIp.width, tfIp.height)){
//				tfIp.isFocus = true;
				tfUser.isFocus = false;
				tfPass.isFocus = false;
				tfRegPass.isFocus = false;
				
				
			}else {
				if (isRes) {
					if (GameCanvas.isPointerHoldIn(tfRegPass.x, tfRegPass.y, tfRegPass.width, tfRegPass.height)){
						tfUser.isFocus = false;
						tfPass.isFocus = false;
						tfRegPass.isFocus = true;
					}
				} else if (GameCanvas.isPointerHoldIn(tfUser.x - 20, GameCanvas.hh + 40, 200, 50)){
					isCheck = !isCheck;
					focus = 2;
				}
//				else{
//					tfUser.isFocus = false;
//					tfPass.isFocus = false;
//					tfRegPass.isFocus = false;
//					myEditText.instance.end();
//				}
			}

		}
		super.updateKey();
		GameCanvas.clearKeyPressed();

	}

	public void resetLogo() {
		yL = -50;
	}
	String strNick = "";
	@Override
	public void perform(int idAction, Object p) {
		Cout.println("perrrrrrrrrrr "+idAction);
		switch (idAction) {
		case 2010:
			GameCanvas.StartDglThongBao("Chức năng đang phát triển");
			break;
		case 2011:
			GameCanvas.StartDglThongBao("Chức năng đang phát triển");
			break;
		case 1000:
			break;
		case 1001:
			actHotline();
			break;
		case 1002:
			actRegister();
			break;
		case 10021:
			actRegisterLeft();
			break;
		case 1003:
			Util.openUrl("http://ninjaschool.vn");
			break;
		case 10041:
			Rms.saveRMSInt("lowGraphic", 0);
			GameCanvas.startOK(mResources.RESTART, 8885 ,null);
			break;
		case 10042:
			Rms.saveRMSInt("lowGraphic", 1);
			GameCanvas.startOK(mResources.RESTART, 8885 ,null);
			break;
//		case 1005:
//			doAskForGPRS();
//			break;
		case 1006:
			domenuConfig();
			break;
		case 10061:
			Rms.saveRMSInt("isSoftKey", 1);
			GameScreen.isTouchKey = true;
			break;
		case 10062:
			Rms.saveRMSInt("isSoftKey", 2);
			GameScreen.isTouchKey = false;
			break;
		case 1007:
			Music.isSound = !Music.isSound;
			if(Music.isSound){
				Rms.saveRMSInt("isSound", 1);
				Music.init();
			}
			else{
				Rms.saveRMSInt("isSound", 2);
				Music.releaseAll();
			}
			break;
		case 1008:
			GameCanvas.startYesNoDlg(mResources.SELECT_LANGUAGE, new Command( mResources.ACCEPT, this, 10081, null), new Command(mResources.NO, GameCanvas.gI(),8882, null));
			break;
		case 10081:
			doResetToSelectLanguage();
			break;
		case 1009:
			Rms.clearRMS();
			break;
		case 2000:
			GameCanvas.menu.showMenu = false;
//			GameMidlet.IP = GameMidlet.IPS1;
//			saveLastServer(GameMidlet.IP);
			doLogin();
			//doSelectServer();
			break;
		case 20001:
			GameCanvas.menu.showMenu = false;
//			GameMidlet.IP = GameMidlet.IPS1;
//			saveLastServer(GameMidlet.IP);
			doLogin();
			break;
		case 20002:
			GameCanvas.menu.showMenu = false;
//			GameMidlet.IP = GameMidlet.IPS2;
//			saveLastServer(GameMidlet.IP);
			doLogin();
			break;
		case 20003:
			GameCanvas.menu.showMenu = false;
//			GameMidlet.IP = GameMidlet.IPS3;
//			saveLastServer(GameMidlet.IP);
			doLogin();
			break;
		case 20004:
			GameCanvas.menu.showMenu = false;
//			GameMidlet.IP = GameMidlet.IPS4;
//			saveLastServer(GameMidlet.IP);
			doLogin();
			break;
		case 20005:
			GameCanvas.menu.showMenu = false;
//			GameMidlet.IP = GameMidlet.IPS5;
//			saveLastServer(GameMidlet.IP);
			doLogin();
			break;
		case 200041:
			GameCanvas.menu.showMenu = false;
//			GameMidlet.IP = GameMidlet.IPS0;
//			GameMidlet.PORT = 14444;
//			saveLastServer(GameMidlet.IP);
			doLogin();
			break;
		case 200042:
			GameCanvas.menu.showMenu = false;
//			GameMidlet.IP = GameMidlet.IPS0;
//			GameMidlet.PORT = 14446;
//			saveLastServer(GameMidlet.IP);
			doLogin();
			break;
		case 2001:
			if (isCheck)
				isCheck = false;
			else
				isCheck = true;
			break;
		case 2002:
			doRegister();
			break;
		case 2003:
			doMenu();
			break;
		case 2004:
			GameCanvas.inputDlg.show(mResources.INPUT_NICK, new Command(mResources.OK, this, 20041, null), TField.INPUT_TYPE_ANY);
			break;
		case 20041:
			strNick = GameCanvas.inputDlg.tfInput.getText().toString();
			GameCanvas.endDlg();
			if (strNick.equals("")) {
				GameCanvas.startOKDlg(mResources.NOT_INPUT_USERNAME);
			} else
				GameCanvas.startYesNoDlg(mResources.ASK_REG_NUM, new Command( mResources.YES, this, 200421, null), new Command(mResources.NO, this, 200422, null));
			break;
		case 200421:
			GameCanvas.endDlg();
			doGetForgetPass(strNick);
			break;
		case 200422:
			GameCanvas.startOKDlg(mResources.replace(mResources.GETPASS_BY_NUMPHONE, strNick));
			break;
//		case 3000:
//			doSetGPRS(false);
//			GameCanvas.endDlg();
//			break;
//		case 3001:
//			doSetGPRS(true);
//			GameCanvas.endDlg();
//			break;
			
		case 4000:
			doRegister(tfUser.getText());
			break;
		case 5001:
			GameMidlet.instance.exit();
			break;
		case 5002:
			GameCanvas.currentDialog= null;
			break;
		}
	}

	private void doResetToSelectLanguage() {
		Rms.saveRMSInt("indLanguage", -1);
		
	}
	
	private void saveLanguageID(int languageID) {
		Rms.saveRMSInt("indLanguage", languageID);
	}


	private void domenuConfig() {
		Vector menu = new Vector();
		menu.addElement(new Command(mResources.SOFT_KEY, this, 10061, null));
		menu.addElement(new Command(mResources.ANALOG_KEY, this, 10062, null));
		menu.addElement(new Command(mResources.SOUND +": " + (!Music.isSound ? mResources.ON : mResources.OFF), this, 1007, null));
			menu.addElement(new Command(mResources.LANGUAGE, this, 1008, null));
		if (GameCanvas.currentScreen == this)
			menu.addElement(new Command(mResources.RMS, this, 1009, null));
		GameCanvas.menu.startAt(menu, 0);
		
	}

	public void actRegisterLeft() {
		 isRes = false;
		 tfRegPass.isFocus = false;
		 tfPass.isFocus = false;
		 tfUser.isFocus = true;
		 right = tfUser.cmdClear;
		 left = cmdMenu;
	}

	private void actRegister() {
		isRes = true;
		tfRegPass.isFocus = false;
		tfPass.isFocus = false;
		tfUser.isFocus = true;
		right = tfUser.cmdClear;
		left = new Command(mResources.CANCEL, this, 10021, null);

		left.setPos(5,
				GameCanvas.h- LoadImageInterface.img_use.getHeight()-5, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		
	}

	private void actHotline() {
		if (!numSupport.equals("")) {
		} else {
			if (!Session_ME.gI().connected) {
				GameCanvas.startWaitDlg(mResources.CONNECTING);
				GameCanvas.connect();
			} else {
				GameCanvas.startWaitDlg();
			}
			// GlobalService.gI().requestService((byte) 5, null);
		}
	}

}
