package screen;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.real.Service;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;

public class KhuScreen extends Screen implements IActionListener{
	
	public static KhuScreen instance;
	
	public static final byte LOW_LAYER_REGION = 0;
    public static final byte MED_PLAYER_REGION = 1;
    public static final byte HIGH_PLAYER_REGION = 2;
    
	public byte[][] listKhu;
	Command cmdClose;
	public int wKhung=170,hKhung=150;
	public int xpaint,ypaint;
	public Scroll srclist =new Scroll();
	public int coutFc;
	public int minKhu = 15;
	
	public KhuScreen(){
		xpaint = GameCanvas.w/2-wKhung/2;
		ypaint = GameCanvas.h/2-hKhung/2;
		cmdClose = new Command("", this, 2, null);
		cmdClose.setPos(xpaint+wKhung- LoadImageInterface.closeTab.width/2,
				ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}
	
	public static KhuScreen gI(){
		if(instance==null) instance = new KhuScreen();
		return instance;
	}
	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdClose)) {
			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
				{
					cmdClose.performAction();
				}
			}
		}
		ScrollResult s1 = srclist.updateKey();
		srclist.updatecm();
		if(GameCanvas.isPointerJustRelease&&srclist.selectedItem!=-1&&srclist.selectedItem<listKhu.length){
			Service.gI().requestChangeKhu((byte)srclist.selectedItem);
			GameCanvas.isPointerJustRelease = false;
		}
		super.updateKey();
		
	}
	@Override
	public void updatePointer() {
		// TODO Auto-generated method stub
		super.updatePointer();
		
	}
	@Override
	public void update() {
		// TODO Auto-generated method stub
		super.update();
		GameScreen.gI().update();
		if(GameCanvas.gameTick%4 ==0){
			coutFc++;
			if(coutFc>2)
				coutFc =0;
		}
	}
	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
		GameScreen.gI().paint(g);
		Paint.paintFrameNaruto(xpaint,ypaint,wKhung,hKhung+2,g);
		Paint.PaintBoxName("Khu",xpaint+wKhung/2-40,ypaint,80,g);
		cmdClose.paint(g);
		if(listKhu!=null){
			srclist.setStyle((listKhu.length>minKhu?listKhu.length:minKhu)/5, Image.getWidth(LoadImageInterface.ImgItem),
					xpaint+15,ypaint+22+Image.getHeight(LoadImageInterface.ImgItem)/4,
					wKhung-30,hKhung-32, true, 5);
			srclist.setClip(g, xpaint+15,ypaint+22+Image.getHeight(LoadImageInterface.ImgItem)/4,
					wKhung-30,hKhung-32);
			for (int i = 0; i < (listKhu.length>minKhu?listKhu.length:minKhu)/5; i++) {
				for(int j=0;j<5;j++)
				{
					g.drawImage(LoadImageInterface.ImgItem,xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j,ypaint+30+(Image.getHeight(LoadImageInterface.ImgItem))*i+2, 0,true);
					if((i*5+j)<listKhu.length)
					if(listKhu[i*5+j][0]==0)
						mFont.tahoma_7_green.drawString(g, (i*5+j+1)+"",
								xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
								ypaint+23+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getHeight(LoadImageInterface.ImgItem)/2, 2);
					else if(listKhu[i*5+j][0]==1)
						mFont.tahoma_7_yellow.drawString(g, (i*5+j+1)+"",
								xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
								ypaint+23+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getHeight(LoadImageInterface.ImgItem)/2, 2);
					else 
						mFont.tahoma_7_red.drawString(g, (i*5+j+1)+"",
							xpaint+15+(Image.getWidth(LoadImageInterface.ImgItem))*j+Image.getWidth(LoadImageInterface.ImgItem)/2,
							ypaint+23+(Image.getHeight(LoadImageInterface.ImgItem))*i+2+Image.getHeight(LoadImageInterface.ImgItem)/2, 2);
				
				}
			}
			if(srclist.selectedItem>0&&srclist.selectedItem<listKhu.length)
			Paint.paintFocus(g,
					 (xpaint+15 + ((srclist.selectedItem % 5)*(Image.getWidth(LoadImageInterface.ImgItem))))+11- LoadImageInterface.ImgItem.getWidth()/4,
					 ypaint+30+ (srclist.selectedItem / 5) * (Image.getHeight(LoadImageInterface.ImgItem) ) +13- LoadImageInterface.ImgItem.getWidth()/4
					, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
			GameCanvas.resetTrans(g);
		}
		
	}
	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 2:
			GameScreen.gI().switchToMe();
			break;
		}
	}
}
