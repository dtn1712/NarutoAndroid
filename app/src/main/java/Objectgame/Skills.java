package Objectgame;

import java.util.Hashtable;

import com.sakura.thelastlegend.lib.mHashtable;

public class Skills {
	public static mHashtable skills = new mHashtable();

	public static void add(Skill skill) {
		skills.put(new Short((short) skill.skillId), skill);
	}
	public static Skill get(short skillId)
	{
		return (Skill) skills.get(new Short(skillId));
	}
}
