package Objectgame;



import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.MyStream;
import com.sakura.thelastlegend.lib.mHashtable;
import com.sakura.thelastlegend.lib.mVector;



import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Vector;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.mGraphics;


import screen.GameScreen;


//import com.sakura.thelastlegend.domain.model.Party;


public class TileMap {
	public static final int T_EMPTY = 0;
	public static final int T_TOP = 2;
	public static final int T_LEFT = 2 << 1;
	public static final int T_RIGHT = 2 << 2;
	public static final int T_TREE = 2 << 3;
	public static final int T_WATERFALL = 2 << 4;
	public static final int T_WATERFLOW = 2 << 5;
	public static final int T_TOPFALL = 2 << 6;
	public static final int T_OUTSIDE = 2 << 7;
	public static final int T_DOWN1PIXEL = 2 << 8;
	public static final int T_BRIDGE = 2 << 9;
	public static final int T_UNDERWATER = 2 << 10;
	public static final int T_SOLIDGROUND = 2 << 11;
	public static final int T_BOTTOM = 2 << 12;
	public static final int T_DIE = 2 << 13;
	public static final int T_HEBI = 2 << 14;
	public static final int T_BANG = 2 << 15;
	public static final int T_JUM8 = 2 << 16;
	public static final int T_NT0 = 2 << 17;
	public static final int T_NT1 = 2 << 18;
	public static final int T_CENTER = 1;

	public static int tmw, tmh, pxw, pxh, tileID, lastTileID = -1;
	public static int[] maps;
	public static int[] types;
	// Tileo
	public static mBitmap imgMaptile;
	public static mBitmap[] imgTile;
	public static mBitmap imgTileSmall, imgMiniMap;
	public static mBitmap imgWaterfall;
	public static mBitmap imgTopWaterfall;
	public static mBitmap imgWaterflow, imgWaterlowN, imgWaterlowN2, imgWaterF;

	public static mBitmap imgLeaf;
	public static byte size = 24;
	private static int bx, dbx, fx, dfx;
	//
	public static String[] instruction;
	public static int[] iX, iY, iW;
	public static int iCount;

	public static String mapName = "";
	public static byte versionMap = 1;
	public static short mapID;
	public static byte lastBgID = -1, zoneID, bgID,bgType,lastType=-1, typeMap;
	public static byte planetID, lastPlanetId = -1;
	public static long timeTranMini;
	public static Vector vGo = new Vector();
	public static Vector vItemBg = new Vector();
	public static Vector vCurrItem = new Vector();
	// public static int nItemBg;
	// public static int nItemGet;
	public static String[] mapNames;
	public static final byte MAP_NORMAL = 0;
	public static mBitmap bong;
	public static final int TRAIDAT_DOINUI = 0;
	public static final int TRAIDAT_RUNG = 1;
	public static final int TRAIDAT_DAORUA = 2;
	public static final int TRAIDAT_DADO = 3;

	public static final int NAMEK_DOINUI = 4;
	public static final int NAMEK_RUNG = 6;
	public static final int NAMEK_DAO = 7;
	public static final int NAMEK_THUNGLUNG = 5;

	public static final int SAYAI_DOINUI = 8;
	public static final int SAYAI_RUNG = 9;
	public static final int SAYAI_CITY = 10;
	public static final int SAYAI_NIGHT = 11;
	public static final int KAMISAMA=12;
	public static final int TIME_ROOM=13;
	
	public static final int ARENA=14;
	public static final int DOCNHAN_1=15;
	public static final int DOCNHAN_3=16;
	public static final int DOCNHAN_2=17;
	public static final int FIZE=18;
	public static final int FIZE2=19;
	public static final int FIZE3=20;
	public static mHashtable listNameAllMap = new mHashtable();
	public static mBitmap[] bgItem = new mBitmap[8];
	static {

		for (int i = 0; i < 3; i++) {

		}

//		bong = GameCanvas.loadImage("/hd/bong.png");

	};

	public static boolean isTrainingMap() {
		if (mapID == 39 || mapID == 40 || mapID == 41)
			return true;
		return false;
	}

	public static mVector vObject = new mVector();

	public static BgItem getBIById(int id) {
		for (int i = 0; i < vItemBg.size(); i++) {
			BgItem bi = (BgItem) vItemBg.elementAt(i);
			if (bi.id == id)
				return bi;
		}
		return null;
	}

	public static int[] offlineId = new int[] { 21, 22, 23, 39, 40, 41 };
	public static int[] highterId = { 21, 22, 23, 24, 25, 26 };

	public static boolean isOfflineMap() {
		for (int i = 0; i < offlineId.length; i++)
			if (mapID == offlineId[i])
				return true;
		return false;
	}

	public static boolean isHighterMap()

	{
		for (int i = 0; i < offlineId.length; i++)
			if (mapID == highterId[i])
				return true;
		return false;
	}

	public static int[] toOfflineId = new int[] { 0, 7, 14 };

	public static boolean isToOfflineMap() {
		for (int i = 0; i < toOfflineId.length; i++)
			if (mapID == toOfflineId[i])
				return true;
		return false;
	}

	public static void freeTilemap() {
		imgTile = null;
		System.gc();
	}

	public static boolean isExistMoreOne(int id) {
		if(id==156)
			return false;
		if(mapID==54 || mapID==55 || mapID==56 || mapID==57 || mapID==58 || mapID==59)
			return false;
		int dem = 0;
		for (int i = 0; i < vCurrItem.size(); i++) {
			BgItem b = (BgItem) vCurrItem.elementAt(i);
			if (b.id == id)
				dem++;
		}
		if (dem > 2)
			return true;
		return false;
	}

//	static final void loadTileImage() {
//		
//		if (imgWaterfall == null)
//			imgWaterfall = GameCanvas.loadImageRMS("/tWater/wtf.png");
//		if (imgTopWaterfall == null)
//			imgTopWaterfall = GameCanvas.loadImageRMS("/tWater/twtf.png");
//		if (imgWaterflow == null)
//			imgWaterflow = GameCanvas.loadImageRMS("/tWater/wts.png");
//		if (imgWaterlowN == null)
//			imgWaterlowN = GameCanvas.loadImageRMS("/tWater/wtsN.png");
//		if (imgWaterlowN2 == null)
//			imgWaterlowN2 = GameCanvas.loadImageRMS("/tWater/wtsN2.png");
//		System.gc();
//	}

	public static void setTile(int index, int mapsArr[], int type) {
//		System.out.println("MAP ARR ----> "+mapsArr.length+" ---- "+type);
		for (int i = 0; i < mapsArr.length; i++) {
			if (maps[index] == mapsArr[i]) {
//				if(maps[index] != -1){ // sửa để test
//					types[index] |= T_TOP;
//				}else
					types[index] |= type;
//				types[index] |= 2;
				
//				types[index] |= T_TOP;
				return;
			}
		}
	}
	public static int[][] tileType;
	public static int[][][] tileIndex;
	
	static int defaultSolidTile;
	
	
		// for (int j = 0; j < types.length; j++) {
		// System.out.println(types.length + "," + maps.length + "," + tileID +
		// "," + types[j]);
		// }
	public static void loadMap(int tileId) {
//		tileId = 2;
		pxh = tmh * size;
		pxw = tmw * size;
		
		int tile = tileId - 1;
//		int tile = tileId;
		
		try {
			for (int i = 0; i <tmw * tmh; i++) {
				for(int a=0;a<tileType[tile].length;a++){
					setTile(i, tileIndex[tile][a], tileType[tile][a]);
				}
			}

		} catch (Exception e) {
			System.out.println("Error Load Map");
			e.printStackTrace();
			GameMidlet.instance.exit();
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		
		
		
	}
	public static boolean isInAirMap(){
		if(mapID==45 || mapID==46 || mapID==48)
			return true;
		return false;
	}
	public static boolean isDoubleMap(){
		if(mapID==45 || mapID==46 || mapID==48 || mapID==51 || mapID==52)
			return true;
		return false;
	}
//	public static void getTile(){
//	{
//			Bitmap img=GameCanvas.loadImageRMS("/t/" + TileMap.tileID + "$1.png");
//			if(img!=null){
//				Rms.DeleteStorage("x"+mGraphics.zoomLevel+"t"+tileID);
//				imgTile=new Image[100];
//				for(int i=0;i<imgTile.length;i++)
//					imgTile[i]=GameCanvas.loadImageRMS("/t/" + TileMap.tileID + "$"+(i+1)+ ".png");
//				
//			}
//			else{
//				img=GameCanvas.loadImageRMS("/t/" + TileMap.tileID + ".png");
//				if(img!=null){
//					Rms.DeleteStorage("$");
//					imgTile=new Image[1];
//					imgTile[0]=img;
//				}
//			}
//			
//		}
//		
//	}
	public static final void paintTile(mGraphics g,int frame,int indexX,int indexY){
		
		if(imgTile==null)
			return;
		if(imgTile.length==1)
			g.drawRegion(imgTile[0], 0, frame * size, size, size, 0, indexX* size, indexY * size, 0);
		else
			g.drawImage(imgTile[frame], indexX*size, indexY*size,0);
	}
	public static final void paintTile(mGraphics g,int frame,int x,int y,int w,int h){
		
		if(imgTile==null)
			return;
		if(imgTile.length==1)
			g.drawRegion(imgTile[0], 0, frame * w, w, w, 0, x, y , 0);
		else
			g.drawImage(imgTile[frame], x, y,0);
	}
	public static final void paintTilemapLOW(mGraphics g) {

		for (int a = GameScreen.gssx; a < GameScreen.gssxe; a++) {
			for (int b = GameScreen.gssy; b < GameScreen.gssye; b++) {

				int t = maps[b * tmw + a] - 1;
				if (t != -1)
					//g.drawRegion(imgTile, 0, t * size, 24, 24, 0, a * size, b * size, 0);
					paintTile(g, t, a, b);
				if ((tileTypeAt(a, b) & T_WATERFALL) == T_WATERFALL) {
					g.drawRegion(imgWaterfall, 0, 24 * (GameCanvas.gameTick % 4), 24, 24, 0, a * size, b * size, 0);
				} else if ((tileTypeAt(a, b) & T_WATERFLOW) == T_WATERFLOW) {
					if ((tileTypeAt(a, b - 1) & T_WATERFALL) == T_WATERFALL)
						g.drawRegion(imgWaterfall, 0, 24 * (GameCanvas.gameTick % 4), 24, 24, 0, a * size, b * size, 0);
					else if ((tileTypeAt(a, b - 1) & T_SOLIDGROUND) == T_SOLIDGROUND)
					//	g.drawRegion(imgTile, 0, 24 * 21, 24, 24, 0, a * size, b * size, 0);
						paintTile(g, 21, a, b);
					mBitmap imgWater = null;
					if (tileID == 5)
						imgWater = imgWaterlowN;
					else if (tileID == 8)
						imgWater = imgWaterlowN2;
					else
						imgWater = imgWaterflow;
					g.drawRegion(imgWater, 0, ((GameCanvas.gameTick % 8) >> 2) * 24, 24, 24, 0, a * size, b * size, 0);
				}
				if ((tileTypeAt(a, b) & T_UNDERWATER) == T_UNDERWATER) {
					if ((tileTypeAt(a, b - 1) & T_WATERFALL) == T_WATERFALL)
						g.drawRegion(imgWaterfall, 0, 24 * (GameCanvas.gameTick % 4), 24, 24, 0, a * size, b * size, 0);
					else if ((tileTypeAt(a, b - 1) & T_SOLIDGROUND) == T_SOLIDGROUND)
						//g.drawRegion(imgTile, 0, 24 * 21, 24, 24, 0, a * size, b * size, 0);
						paintTile(g, 21, a, b);
					//g.drawRegion(imgTile, 0, (maps[b * tmw + a] - 1) * size, 24, 24, 0, a * size, b * size, 0);
					paintTile(g, (maps[b * tmw + a] - 1), a, b);
				}
			}
		}
	}
//	public static Bitmap imgLight=GameCanvas.loadImage("/bg/light.png");
	public static final void paintTilemap(mGraphics g) {
	
		int replaceTile;
//		if(Char.isLoadingMap)
//			return;
		// GameCanvas.debug("map " + mapID + " tile " + tileID, 1);
		GameScreen.gI().paintBgItem(g, 1);
		
		
		
		for (int a = GameScreen.gssx; a < GameScreen.gssxe; a++) {
			for (int b = GameScreen.gssy; b < GameScreen.gssye; b++) {
				if(a==0)continue;
				if(a==tmw-1)continue;
				int t = maps[b * tmw + a] - 1;

				if ((tileTypeAt(a, b) & T_OUTSIDE) == T_OUTSIDE)
					continue;
				if ((tileTypeAt(a, b) & T_WATERFALL) == T_WATERFALL) {
					g.drawRegion(imgWaterfall, 0, 24 * ((GameCanvas.gameTick % 8) >> 1), 24, 24, 0, a * size, b * size,
							0);
					continue;
				} else if ((tileTypeAt(a, b) & T_TOPFALL) == T_TOPFALL) {
					g.drawRegion(imgTopWaterfall, 0, 24 * ((GameCanvas.gameTick % 8) >> 1), 24, 24, 0, a * size, b
							* size, 0);
					continue;
				}
				
				if (tileID == 13 ) {
					if(!GameCanvas.lowGraphic)
						return;
					else{
						if(t!=-1){}
							//g.drawRegion(imgTile, 0, 24, 24, 24, 0, a * size, b * size, 0);
							paintTile(g, 0, a, b);
						continue;
					}
				}
				if (tileID == 2) {
				
					if ((tileTypeAt(a, b) & T_DOWN1PIXEL) == T_DOWN1PIXEL) {
						if (t != -1) {
							//g.drawRegion(imgTile, 0, t * size, 24, 1, 0, a * size, b * size, 0);
							//g.drawRegion(imgTile, 0, t * size, 24, 24, 0, a * size, b * size + 1, 0);
							paintTile(g, t, a * size, b * size, 24, 1);
							paintTile(g, t, a * size, b * size+1, 24, 24);
						}
					}
				}
				if (tileID == 3) {
				}
               System.out.println(GameScreen.cmx+"  paint tile map>>>>>>  "+(fx + GameScreen.cmx));
				if ((tileTypeAt(a, b) & T_TREE) == T_TREE) {
					bx = a * size - GameScreen.cmx;
					dbx = bx - GameScreen.gW2;
					dfx = ((size - 2) * dbx) / size;
					fx = dfx + GameScreen.gW2;
					//g.drawRegion(imgTile, 0, t * size, 24, 24, 0, fx + GameScreen.cmx, b * size, 0);
					paintTile(g, t,fx + GameScreen.cmx, b * size, 24, 24);
				} else if ((tileTypeAt(a, b) & T_DOWN1PIXEL) == T_DOWN1PIXEL) {
					if (t != -1) {
						paintTile(g, t, a * size, b * size, 24, 1);
						paintTile(g, t, a * size, b * size+1, 24, 24);
					}
				} else if (t != -1) {
					paintTile(g, t, a, b);
				}
			}
		}
		// paint outside camera when drag mouse (cmy<0 and cmy>cmyLim)
		if(GameScreen.cmx<24)
		{
			for (int b = GameScreen.gssy; b < GameScreen.gssye; b++) {
				int t = maps[b * tmw + 1] - 1;
				if(t!=-1)
				{
					//g.drawRegion(imgTile, 0, t * size, 24, 24, 0, 0, b * size, 0);
					paintTile(g, t, 0, b);
				}
			}
		}
		if(GameScreen.cmx > GameScreen.cmxLim)
		{
			int a = tmw-2;
			for (int b = GameScreen.gssy; b < GameScreen.gssye; b++) {
				int t = maps[b * tmw + a] - 1;
				if(t!=-1)
				{
				//	g.drawRegion(imgTile, 0, t * size, 24, 24, 0, (a+1)*size , b * size, 0);
					paintTile(g, t, a+1, b);
				}
			}
		}		
	}

	public static int sizeMiniMap = 2, gssx, gssxe, gssy, gssye, countx, county;
	private static int[] colorMini = new int[] { 0x503a0a, 0x866318 };
	public static int yWater = 0;
	public static boolean isWaterEff(){
		if(mapID==54 || mapID==55 || mapID==56 || mapID==57)
			return false;
		return true;
	}
	
	public static final void paintOutTilemap(mGraphics g) {
		if (GameCanvas.lowGraphic)
			return;

		for (int a = GameScreen.gssx; a < GameScreen.gssxe; a++) {
			for (int b = GameScreen.gssy; b < GameScreen.gssye; b++) {

				if ((tileTypeAt(a, b) & T_WATERFLOW) == T_WATERFLOW ) {
					mBitmap imgWater = null;
					if (tileID == 5)
						imgWater = imgWaterlowN;
					else if (tileID == 8)
						imgWater = imgWaterlowN2;
					else
						imgWater = imgWaterflow;
					if(!isWaterEff()){
						g.drawRegion(imgWater, 0, 0* 24, 24, 24, 0, a * size,
								b * size-1 , 0);
						g.drawRegion(imgWater, 0, 0* 24, 24, 24, 0, a * size,
								b * size-3 , 0);
					}
					g.drawRegion(imgWater, 0, ((GameCanvas.gameTick % 8) >> 2) * 24, 24, 24, 0, a * size,
							b * size - 12, 0);
					if (yWater == 0&& isWaterEff()) {
						yWater = b * size - 12;
						int waterColor = 0xffffff;
						if (GameCanvas.typeBg == TRAIDAT_DAORUA)
							waterColor = 0xa5e1f7;
						else if (GameCanvas.typeBg == NAMEK_DOINUI)
							waterColor = 0x7bc56e;
						else if (GameCanvas.typeBg == NAMEK_DAO)
							waterColor = 0x56dec5;
//						BackgroudEffect.addWater(waterColor, yWater + 15);
					}
				}

			}
		}

//		BackgroudEffect.paintWaterAll(g);
	}

	public static void loadMapFromResource(int mapID) throws Exception {
		InputStream iss = null;
		DataInputStream is = null;
		Cout.println("TileMap loadma "+("map/" + TileMap.mapID));
		iss =  GameMidlet.asset.open("map/" + TileMap.mapID);
		is = new DataInputStream(iss);
		TileMap.tmw = (char) is.read();
		TileMap.tmh = (char) is.read();

		Cout.println(TileMap.tmw+ "  TileMap loadma "+TileMap.tmh);
		TileMap.maps = new int[is.available()];
		for (int i = 0; i < TileMap.tmw * TileMap.tmh; i++){
			TileMap.maps[i] = (char) is.read();
		}
		TileMap.types = new int[TileMap.maps.length];
	}

	public static final int tileAt(int x, int y) {
		try {
			return maps[y * tmw + x];
		} catch (Exception ex) {
			return 1000;
		}
	}

	public static final int tileTypeAt(int x, int y) {
		try {
			return types[y * tmw + x];
		} catch (Exception ex) {
			return 1000;
		}
	}

	public static final int tileTypeAtPixel(int px, int py) {
		try {

			return types[(py / size) * tmw + (px / size)];
		} catch (Exception ex) {
			return 1000;
		}
	}

	public static final boolean tileTypeAt(int px, int py, int t) {
		try {

			return (types[(py / size) * tmw + (px / size)] & t) == t;
		} catch (Exception ex) {
			return false;
		}
	}

	public static final void setTileTypeAtPixel(int px, int py, int t) {

		types[(py / size) * tmw + (px / size)] |= t;
	}

	public static final void setTileTypeAt(int x, int y, int t) {
		types[y * tmw + x] = t;
	}

	public static final void killTileTypeAt(int px, int py, int t) {
		types[(py / size) * tmw + (px / size)] &= ~t;
	}

	public static final int tileYofPixel(int py) {
		return (py / size) * size;
	}

	public static final int tileXofPixel(int px) {
		return (px / size) * size;
	}


	public static void loadMainTile() {
		// imgTile = null;
		System.gc();
		if (lastTileID != tileID) {
			//imgTile = GameCanvas.loadImageRMS("/t/" + TileMap.tileID + ".png");
//			getTile();
			lastTileID = tileID;
		}

		// TileMap.imgTileSmall = GameCanvas.loadImage("/t/mini_" +
		// TileMap.tileID + ".png");
	}
	
	public static void loadMapFromResource() throws Exception {
		InputStream iss = null;
		DataInputStream is = null;
		iss = MyStream.readFile("/map/0");
//		System.out.println("iss ---> "+iss);
		is = new DataInputStream(iss);

		Cout.println("TileMap loadma222 "+iss);
//		System.out.println("is ---> "+is);
//		TileMap.tmw = (char) is.read();
//		TileMap.tmh = (char) is.read();
		TileMap.tmw = (char) is.read();
		TileMap.tmh = (char) is.read();
		pxh = tmh * size;
		pxw = tmw * size;
		TileMap.maps = new int[is.available()];
		TileMap.types = new int[TileMap.maps.length];
		for (int i = 0; i < TileMap.tmw * TileMap.tmh; i++)
			{TileMap.maps[i] = (char) is.read();
			TileMap.types[i]=0;// chi co trai dat
			}
		
	}

	public static void loadMapfile(int MapID){
		try{
			TileMap.vCurrItem.removeAllElements();
			TileMap.vItemBg.removeAllElements();
			loadMapFromResource(MapID);

			
						
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public static void loadimgTile(int TileID){
		TileID = TileID - 1;
		if(imgMaptile!=null&&imgMaptile.image!=null)
			imgMaptile.cleanImg();
		imgMaptile = null;
		imgMaptile = GameCanvas.loadImage("/tile"+TileID+".png");
//		System.out.println("MapImage ---> "+imgMaptile);
		
		
	}
	
	public static void paintMap(mGraphics g){
		
		GameScreen.gI().paintBgItem(g, 1);
		GameScreen.gssw = GameCanvas.w / TileMap.size + 2;
		GameScreen.gssh = GameCanvas.h / TileMap.size + 2;
		if ( GameCanvas.w  % 24 != 0)
			GameScreen.gssw += 1;
		
		GameScreen.gssx = GameScreen.cmx / TileMap.size - 1;
		if (GameScreen.gssx < 0)
			GameScreen.gssx = 0;
		GameScreen.gssy = GameScreen.cmy  / TileMap.size;
		GameScreen.gssxe = GameScreen.gssx + GameScreen.gssw;
		GameScreen.gssye = GameScreen.gssy + GameScreen.gssh;
		if (GameScreen.gssy < 0)
			GameScreen.gssy = 0;
		if (GameScreen.gssye > TileMap.tmh - 1)
			GameScreen.gssye = TileMap.tmh - 1;
//		loadMap(1);
//		positionY=GameScreen.gssy*size;
		if(imgMaptile!=null)
		for (int a = GameScreen.gssx; a < GameScreen.gssxe; a++) {
			for (int b = GameScreen.gssy; b < GameScreen.gssye; b++){
				
				try {
					
					int t = maps[b * tmw + a] - 1;
					if((t+1)*size>imgMaptile.getHeight()){
						return;
					}
					if (t >= 0){
						g.drawRegion(imgMaptile, 0, t * size, size, size, 0, a * size, (b * size), 0);
					}
				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}
			}
		}
	}
	
}

