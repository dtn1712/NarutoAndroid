package Objectgame;

import java.util.Hashtable;

import com.sakura.thelastlegend.lib.mHashtable;

public class ItemTemplates {
	public static mHashtable itemTemplates = new mHashtable();


	public static void add(ItemTemplate it) {
		itemTemplates.put(new Short(it.id), it);
	}
	public static ItemTemplate get(short id)
	{
		return (ItemTemplate) itemTemplates.get(new Short(id));
	}
	public static short getPart(short itemTemplateID) {
		return get(itemTemplateID).part;
	}
	public static short getIcon(short itemTemplateID) {
		return get(itemTemplateID).iconID;
	}
}
