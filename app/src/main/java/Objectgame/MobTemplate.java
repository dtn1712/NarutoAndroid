package Objectgame;


import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.domain.model.EffectData;

import com.sakura.thelastlegend.domain.model.Frame;
import com.sakura.thelastlegend.domain.model.ImageInfo;

public class MobTemplate {
	public byte rangeMove, speed, type;
	public short mobTemplateId,idloadimage;
	public int hp;
	public String name;
	public mBitmap imgs[] = new mBitmap[8];
	public ImageInfo[] imginfo;
	public Frame[] frameBoss;
	public byte[] frameBossMove;
	public byte[][] frameBossAttack;
	public  EffectData data;
	public mBitmap imgTool;
	
	public ImageInfo getImgInfo(byte frame){
		return imginfo[frame];
	}

}
