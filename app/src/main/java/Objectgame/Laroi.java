package Objectgame;

import java.util.Random;
import java.util.Vector;

import com.sakura.thelastlegend.lib.LoadImageInterface;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.domain.model.Sprite;

public class Laroi {
	int X, Y;
	int id_frame;
	// int dirX, dirY;
	int vx = 0, vy = 0;
	public static Image imgEff;
	int type;
	public static Random rd = new Random(System.currentTimeMillis());
	public static Vector la = new Vector();
	int tickk;

	public Laroi(int x, int y) {

		X = x;
		Y = y;
		vx = vy = 0;

	}

	static Laroi sn = null;

	public static void load() {
		la.removeAllElements();
		int kc = (TileMap.pxw)/50;
		int kch = (TileMap.pxh)/20;
		for (int i = 0; i < 50; i++) {
			
			sn = new Laroi(-20 + (random(-5, 45)*kc) * kc, kch* random(2, 18));
			la.addElement(sn);
		}
	}

	public static int absb(int x) {
		if (x < 0)
			return -x;
		return x;

	}

	public void update() {
		tickk = (tickk + 1) % 100;
		if (tickk % 4 == 0) {
			id_frame++;
			if (id_frame > 3)
				id_frame = 0;
		}
		int a[] = { 1, 2, 3 };
			X += random(1, 3);
			Y += random(1, 3);
			if ( Y > ((GameCanvas.h +TileMap.pxh)/ TileMap.size + 2) * 24 + 240
					|| X > ((GameCanvas.w +TileMap.pxw)/ TileMap.size + 1) * 24 + 240
					|| X < -600) {
				X = random(-5, TileMap.pxh/50-5)* 50;
				Y = -random(0, 20)*50;
			}
	}

	public static int random(int imin, int imax) {
		int result = 0;
		int delta = absb(imax - imin);
		result = absb(rd.nextInt(delta)) + imin;
		return result;
	}

	public static void update_Laroi() {
		for (int i = 0; i < la.size(); i++) {
			Laroi obj = (Laroi) la.elementAt(i);
			obj.update();
		}
	}

	public static void paint_Laroi(mGraphics g) {
		for (int i = 0; i < la.size(); i++) {
			Laroi obj = (Laroi) la.elementAt(i);
			obj.paint(g);
		}
	}

	public void paint(mGraphics g) {
		// g.drawImage(Res.imgLa, X, Y, StaticObj.BOTTOM_HCENTER);
		g.drawRegion(LoadImageInterface.imgLa, 0, id_frame * 14, 20, 14, Sprite.TRANS_NONE, X,
				Y, mGraphics.VCENTER|mGraphics.BOTTOM);

	}
}
