package Objectgame;


import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import screen.GameScreen;

import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;

public class OtherChar {
	public String name;
	public short id,x,y;
	public int nTinChuaDoc;
	short lvUser;
	boolean isHostClan;
	public mVector listChat = new mVector();
	public static Scroll scrMain = new Scroll();
	public  int indexSize = 28, indexTitle = 0, indexSelect = 0, indexRow = -1, indexRowMax, indexMenu = 0, indexCard = -1;
	
	public OtherChar(short idd, String namee)
	{
		
		this.id = idd;
		this.name = namee;
	}
	public OtherChar(short idd, String namee,short lv, boolean isHostC)
	{
		this.lvUser = lv;
		this.isHostClan = isHostC;
		this.id = idd;
		this.name = namee;
	}
	public OtherChar(short idd, String namee,mFont f, int w)
	{
		this.id = idd;
		String[] chuoiname = f.splitFontArray(namee, w-2);
		if(chuoiname!=null&&chuoiname.length>1)
		this.name = chuoiname[0]+"...";
		else if(chuoiname!=null&&chuoiname.length==1)
			this.name = namee;
	}
	public OtherChar(String namee,mFont f,int w, short xx,short yy)
	{
		this.x = xx;
		this.y = yy;
		String[] chuoiname = f.splitFontArray(namee, w-2);
		if(chuoiname!=null&&chuoiname.length>1)
		this.name = chuoiname[0]+"...";
		else if(chuoiname!=null&&chuoiname.length==1)
			this.name = namee;
	}
	
	public void Paint(mGraphics g,int xTab,int yTab,int wND,int hND )
	{
		GameScreen.resetTranslate(g);
		int notehdem = 0;
		this.hChat = hND;
		scrMain.setStyle(nItemChat, NodeChat.HSTRING, xTab, yTab, wND - 3, hND, true, 1);
		scrMain.setClip(g, xTab, yTab,wND - 3, hND);
		for (int i = this.listChat.size()-1; i >=0; i--) 
		{
		    NodeChat node = (NodeChat)this.listChat.elementAt(i);
		    if(node!=null){
		    	node.paint(g, xTab+NodeChat.HSTRING,yTab+ NodeChat.HSTRING/2+ (notehdem),node.isMe,false);
				    
//		     if(!node.isMe){
////		      if(node.type!=NodeChat.t_EMO){
////		    	  node.paintNodeChat(g, xTab+name.length()+NodeChat.HSTRING/2, yTab+ NodeChat.HSTRING/2+ (notehdem), node.wnode+NodeChat.HSTRING/2, node.hnode+7,0);
////		      }
//		      node.paint(g, xTab+NodeChat.HSTRING,yTab+ NodeChat.HSTRING/2+ (notehdem));
//		     }
//		     else{
////		      if(node.type!=NodeChat.t_EMO){
////		    	  node.paintNodeChat(g, xTab-5*NodeChat.HSTRING/4+wND-node.wnode, yTab+ NodeChat.HSTRING/2+ (notehdem), node.wnode+NodeChat.HSTRING/2, node.hnode+7,1);
////		      }
//		      node.paint(g, xTab+NodeChat.HSTRING, yTab+ NodeChat.HSTRING/2+ (notehdem));
//		     }
		     notehdem+=node.hnode+NodeChat.HSTRING/2;
		     
		    }
		   
		 }
		GameScreen.resetTranslate(g);
//		 GameScreen.scrMain.setStyle(nItem, ITEM_SIZE, xPos, yPos, width, height, styleUPDOWN, ITEM_PER_LINE)(this.listChat.size(),0,xTab,yTab)
		
	}
	public int nItemChat = 0;
	public int hChat = 10;
	public boolean isKhoa = false;;
	public void update(){
		int dem = 0;
		for (int i = this.listChat.size()-1; i >=0; i--) {
			NodeChat node = (NodeChat)this.listChat.elementAt(i);
			if(node!=null){
				dem+=node.hnode+NodeChat.HSTRING/2;
			}
		}
//		if(dem<hChat)
			scrMain.cmtoY = -hChat+dem;
//		if(scrMain.cmtoY>=(dem-hChat)){
//			isKhoa = false;
//		}
//		else isKhoa = true;
		nItemChat = (dem)/NodeChat.HSTRING + 1;
		if (GameCanvas.isTouch) {
			   ScrollResult r = scrMain.updateKey();
			   scrMain.updatecm();
			   if (r.isDowning || r.isFinish) {
			    indexRow = r.selected;
			   // isPress = true;
			   }
		}
	}
	
}
