package Objectgame;

import com.sakura.thelastlegend.lib.mFont;


public class InfoItem {
	
	public String s;
	public mFont f;
	public int speed;
	public static int wcat = -1;
	public boolean isNhiemVuServer = false;

	public InfoItem(String s) {
		f = mFont.tahoma_7_white;
		this.s = s;
		speed = 20;
	}
	public InfoItem(String s,boolean isServer) {
		f = mFont.tahoma_7_white;
		this.s = s;
		speed = 20;
		this.isNhiemVuServer = isServer;
	}

	public InfoItem(String s, mFont f, int speed) {
		this.f = f;
		this.s = s;
		this.speed = speed;
	}
}
