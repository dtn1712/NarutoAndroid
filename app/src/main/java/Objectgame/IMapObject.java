package Objectgame;

// có x,y, paint theo lối BOTTOM-CENTER (MapItem, Mob, Char)
public interface IMapObject {
	public int getX();
	public int getY();
	public int getW();
	public int getH();
	public void stopMoving();
	public boolean isInvisible();
}
