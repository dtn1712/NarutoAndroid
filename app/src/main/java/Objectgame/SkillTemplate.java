package Objectgame;

import com.sakura.thelastlegend.lib.mFont;

public class SkillTemplate {
	public byte id;
	public int classId,ideff;
	public String name;
	public int maxPoint;
	public int type;
	public int iconId,iconTron;
	public String[] description;
	public Skill[] skills;
	public byte typeskill,ntarget,typebuffSkill,subeff;
	public int range,level=-1;
	public short[] levelChar,mphao,tExistSubEff,usehp,usemp,rangelv,dame;
	public int[] cooldown,timebuffSkill;
	public byte[] pcSubEffAppear,ntargetlv;
	public byte nlevelSkill;
	
	public SkillTemplate(byte id, String name, int idIcon,byte typeskilll,byte ntargett,
			byte typebuffSkilll, byte subEfff,short rangee,String des){ // moi them vo để add vào vector template lấy thông tin
		this.id = id;
		this.name = name;
		this.iconId = idIcon;
		this.typeskill = typeskilll;
		this.ntarget = ntargett;
		this.typebuffSkill = typebuffSkilll;
		this.subeff = subEfff;
		this.range = rangee;
		this.description = mFont.tahoma_7_white.splitFontArray(des, 149);
	}
	public void khoitaoLevel(byte nlevelSkill){
		this.nlevelSkill =nlevelSkill;
		levelChar = new short[nlevelSkill];
		mphao= new short[nlevelSkill];
		tExistSubEff= new short[nlevelSkill];
		usehp= new short[nlevelSkill];
		usemp= new short[nlevelSkill];
		rangelv= new short[nlevelSkill];
		cooldown= new int[nlevelSkill];
		timebuffSkill= new int[nlevelSkill];
		pcSubEffAppear= new byte[nlevelSkill];
		ntargetlv= new byte[nlevelSkill];
		dame = new short[nlevelSkill];
		
	}
	public int getCoolDown(byte level) {
		// TODO Auto-generated method stub
		if(level>=cooldown.length){
			return cooldown[0];
		}
		return cooldown[level];
	}
}
