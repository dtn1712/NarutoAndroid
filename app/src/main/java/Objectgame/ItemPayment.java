package Objectgame;

import com.sakura.thelastlegend.lib.Cout;

public class ItemPayment {
	private String iditem;
	private String money;
	private String name_money;
	private String value_xu;
	private String value_luong;
	
	private int gameMoneyAmount;
	private String gameMoneyUnit;
	private int realMoneyAmount;
	private String realMoneyUnit;
	
	public static final String KEY_ID = "";
	public static final String KEY_VALUE_MONEY_AMOUNT = "gameMoneyAmount";
	public static final String KEY_NAME_MONEY_AMOUNT = "gameMoneyUnit";
	public static final String KEY_VALUE_MONEY_REAL = "realMoneyAmount";
	public static final String KEY_NAME_MONEY_REAL = "realMoneyUnit";
	
	public ItemPayment()
	{
		
	}
	
	public ItemPayment(String iditem,String money,String namemoney,String valuexu,String valueluong)
	{
		this.setIditem(iditem);
		this.setMoney(money);
		this.setName_money(namemoney);
		this.setValue_luong(valueluong);
		this.setValue_xu(valuexu);
	}
	public ItemPayment(int id,int gamemoney, String gamemoneyUtil,int realmoney, String realmoneyUtil)
	{
		this.iditem = id+"";
		this.setGameMoneyAmount(gamemoney);
		this.setGameMoneyUnit(gamemoneyUtil);
		this.setRealMoneyAmount(realmoney);
		this.setRealMoneyUnit(realmoneyUtil);
		Cout.println2222(id+" moneyUtil "+gamemoney+" name "+gamemoneyUtil+ "com/sakura/thelastlegend/real  " +realmoney+" name "+realmoneyUtil);
	}
	public String getIditem() {
		return iditem;
	}

	public void setIditem(String iditem) {
		this.iditem = iditem;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getName_money() {
		return name_money;
	}

	public void setName_money(String name_money) {
		this.name_money = name_money;
	}

	private String getValue_xu() {
		return value_xu;
	}

	public void setValue_xu(String value_xu) {
		this.value_xu = value_xu;
	}

	public String getValue_luong() {
		return value_luong;
	}

	public void setValue_luong(String value_luong) {
		this.value_luong = value_luong;
	}

	public int getGameMoneyAmount() {
		return gameMoneyAmount;
	}

	private void setGameMoneyAmount(int gameMoneyAmount) {
		this.gameMoneyAmount = gameMoneyAmount;
	}

	public String getGameMoneyUnit() {
		return gameMoneyUnit;
	}

	private void setGameMoneyUnit(String gameMoneyUnit) {
		this.gameMoneyUnit = gameMoneyUnit;
	}

	public int getRealMoneyAmount() {
		return realMoneyAmount;
	}

	private void setRealMoneyAmount(int realMoneyAmount) {
		this.realMoneyAmount = realMoneyAmount;
	}

	public String getRealMoneyUnit() {
		return realMoneyUnit;
	}

	private void setRealMoneyUnit(String realMoneyUnit) {
		this.realMoneyUnit = realMoneyUnit;
	}
}
