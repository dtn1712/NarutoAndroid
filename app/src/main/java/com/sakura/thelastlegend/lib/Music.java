package com.sakura.thelastlegend.lib;



import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;

import com.sakura.thelastlegend.GameMidlet;


public class Music  {
	public static int volume = 0;
	private final static int MAX_VOLUME = 10;
	public static MediaPlayer[] music;
	public static SoundPool[] sound;
	public static int[] soundID ;
	public static int idMusicPlaying = 0;

	public static int currentMusic = 0;
	public static int currentSound = 50;
	public static boolean isplaySound = true;
	public static boolean isplayMusic = true;
	
	//MUSIC

	

	// SOUND
	 public final static int ATTACK_0 = 50;
	 public final static int ATTACK_1 = 51;
	 public final static int ATTACK_2 = 52;
	 public final static int RUN = 53;
	 public final static int LENLV = 54;
	 public final static int SKILL2 = 55;

	static final int nMusic = 5;
	static final int nSound = 6;
	public static final int MLogin = 0;
	public static final int MGameScr = 1;
	

	public static void init(int[] musicID,int[] sID) {
		if(musicID!=null){
			music=new MediaPlayer[musicID.length];
			for (int i = 0; i < music.length; i++) {
				music[i] = getMediaSoundFile(i+"");
			}
		}
		if(sID!=null){
			soundID=new int[sID.length];
			sound=new SoundPool[sID.length];
			for (int i = 0; i < sound.length; i++) {
				sound[i] = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
				sound[i].setOnLoadCompleteListener(new OnLoadCompleteListener() {
				public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {	}});
				soundID[i] = getSoundPoolSource(i,i+"");
			}
		}
		System.gc();
	}
	
	public static int getSoundPoolSource(int index, String fileName) {
		AssetFileDescriptor descriptor = null;
		try {
			try {
				descriptor = GameMidlet.asset.openFd("sound/"+fileName+".wav");
			} catch (Exception e) {
				// TODO: handle exception
				descriptor = GameMidlet.asset.openFd("sound/"+fileName+".mp3");
			}
			Cout.println("load am thanh thanh cong");
			return sound[index].load(descriptor.getFileDescriptor(),descriptor.getStartOffset(),descriptor.getLength(),0);

		} catch (IOException e) {
			System.out.println("tieng dong loi: "+fileName);
		}
		return -1;
	}

	public static MediaPlayer getMediaSoundFile(String fileName) {
		AssetFileDescriptor descriptor = null;
		MediaPlayer m = new MediaPlayer();

		try {
			try {				
				descriptor =GameMidlet.asset.openFd("music/" + fileName+ ".mp3");
			} catch (Exception e) {
				// TODO: handle exception
				descriptor =GameMidlet.asset.openFd("music/" + fileName+ ".wav");
			}
			m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			m.prepare();
		} catch (IOException e) {
			System.out.println("am thanh loi: " + fileName);
			e.printStackTrace();
		} finally {
			System.gc();
		}
		return m;
	}

	
	public static boolean stopAll;
	public static boolean isNotPlay=false;
	public static boolean isSound;
	public static boolean isPlayingMusic;;
	public static void playSound(int id, float volume) {
		if(sound==null || isNotPlay ){
			
		}
		else
			try {
				sound[id].play(soundID[id], volume,volume, 0, 0, 1f);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}

	}
	public static void playMus(int id, float volume,boolean isLoop) {
		
		if(music!=null)
			for (int i = 0; i < music.length; i++) {
				if (music[i] != null && music[i].isPlaying())
						music[i].pause();
			}
		try {
			music[id].setVolume(volume, volume);
			music[id].setLooping(isLoop);
			music[id].seekTo(0);
			music[id].start();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
	}
	
	public static void pauseMusic(int id) {
		if (music[id] != null) {
			if (music[id].isPlaying())
				music[id].pause();
		}
	}

	public static void resumeMusic(int id) {
		if (music[id] != null) {
			if (music[id].getCurrentPosition() == 0
					|| music[id].getCurrentPosition() >= 33332) {
				return;
			}
			if (volume > 0)
				music[id].start();
		}
	}


	public static void resumeAll() {
		for (int i = 0; i < music.length; i++) {
			for (int j = 0; j < 3; j++) {
				int curLength = music[i].getCurrentPosition();
				music[i].seekTo(curLength);
				music[i].start();
			}
		}
	}
	
	public static void releaseAll() {
		int i;
		for ( i = 0; i < music.length; i++) {
			if (music[i] != null) {
				music[i].release();
				music[i] = null;
			}
		}
		
		System.gc();
	}
	public static void stopAll() {
		if(music!=null)
			for (int i = 0; i < music.length; i++) {
				if (music[i] != null && music[i].isPlaying())
						music[i].pause();
			}
		if(sound!=null)
			for (int i = 0; i < sound.length; i++) {
				if (sound[i] != null) {
					sound[i].stop(volume);
				}
			}
		System.gc();
	}
	public static void setVolume(int id, int index, int soundVolume) {
		final float volume = (float) (1 - (Math.log(MAX_VOLUME - soundVolume) / Math.log(MAX_VOLUME)));
		if (music[id] != null)
			music[id].setVolume(volume, volume);
	}

	public static void init() {
		// TODO Auto-generated method stub
		if(music!=null||sound!=null)
			return;
		music = new MediaPlayer[nMusic];
		sound = new SoundPool[nSound];

		soundID=new int[nSound];

		for (int i = 0; i < music.length; i++) {
			music[i] = getMediaSoundFile(i+"");
		}
		for (int i = 0; i < sound.length; i++) {
			sound[i] = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
			sound[i].setOnLoadCompleteListener(new OnLoadCompleteListener() {
			public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {	}});
			soundID[i] = getSoundPoolSource(i,i+"");
		}
		System.gc();
	
	}

	public static void play(int id, float volume) {
		// TODO Auto-generated method stub
//		if(true) return;
		try {
			if (!Music.isSound || id == -1 || music == null || sound == null)
				return;

			if (id < 50) {
				currentMusic  = id;
				if(isplayMusic){
					music[id].setVolume(volume, volume);
	//				music[id].seekTo(0);
	//				music[id].start();
					if(isPlayingMusic)
						stopAll();
					isPlayingMusic = true;
					idMusicPlaying = id;
					playMus(id, volume, true);
				}
//				music[id].play(volume);
			} else {

				currentSound  = id;
				if(isplaySound){
					id -= 50;
	//				sound[id].play(soundID[id], volume, volume, 0, 0, 1f);
	//				sound[id].play(volume);
					playSound(id, volume);
				}
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}

	}

	public static void stopSound(int id) {
		// TODO Auto-generated method stub
		id-=50;
		if(id>=0&&id<sound.length){
			sound[id].stop(volume);
		}
	}
	public static void ResumMusic(){

//		Cout.println("onResume MMMMMMMMMMMMMMMMMMisPlayingMusic  "+isPlayingMusic);
//		Cout.println("onResume MMMMMMMMMMMMMMMMMMidMusicPlaying  "+idMusicPlaying);
//		Cout.println("onResume MMMMMMMMMMMMMMMMMMvolume  "+volume);
		if(isPlayingMusic){
			stopAll();
			playMus(idMusicPlaying, 0.5f, true);
		}
	}

	public static void resumeMusic() {
		// TODO Auto-generated method stub
		if(isPlayingMusic){
			try {
				playMus(currentMusic, 0.5f, true);
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void pauseMusic() {
		// TODO Auto-generated method stub
		music[currentMusic].pause();
	}
}
