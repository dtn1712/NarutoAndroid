package com.sakura.thelastlegend.lib;



import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.mGraphics;


public class FontSys {
	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	public static final int CENTER = 2;
	public static final int RED = 0;
	
	public static final int YELLOW	= 1;
	public static final int GREEN	= 2;
	public static final int FATAL	= 3;
	public static final int MISS	= 4;
	public static final int ORANGE	= 5;
	public static final int ADDMONEY= 6;
	public static final int MISS_ME	= 7;
	public static final int FATAL_ME= 8;
	public Paint p;
	public int color;
	
	public static mFont number_yellow,tahoma_8b;

	public FontSys(String name, int size, int color) {
		p = new Paint();
		p.setTypeface(Typeface.createFromAsset(GameMidlet.asset,name));
		p.setTextSize(size * mGraphics.zoomLevel);
		p.setColor(color);
		p.setAntiAlias(true);
		this.color = color;
	}
	
	public int getHeight() {
		Rect bounds = new Rect();
		p.getTextBounds("Âg", 0, 1, bounds);
		return bounds.height() / mGraphics.zoomLevel;
	}
	
	public int getWidth(String st) {		
		return (int) (p.measureText(st) / mGraphics.zoomLevel);
	}
	
	
	public void drawString(mGraphics g, String st, int x, int y, int align) {
		g.drawString(st, x, y+10, align,p);
	}
	public void drawStringBoder(mGraphics g, String st, int x, int y, int align) {
		g.drawStringBoder(st, x, y+10*mGraphics.zoomLevel, align,p);
	}
	public void drawStringStroke(mGraphics g, String st, int x, int y, int align) {
		g.drawStringStroke(st, x, y+10, align,p);
	}

	public String[] splitFontArray(String str, int lineWidth) {
		// TODO Auto-generated method stub
		mVector lines = splitFontVector(str, lineWidth);
		String[] arr = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			arr[i] = lines.elementAt(i).toString();
		}
		return arr;
	}

	private mVector splitFontVector(String src, int lineWidth) {
		// TODO Auto-generated method stub
		mVector lines = new mVector();
		String line = "";
		for (int i = 0; i < src.length(); i++) {
			if (src.charAt(i) == '\n'|| src.charAt(i)=='\b') {
				lines.addElement(line);
				line = "";
			} else {
				line += src.charAt(i);
				if (getWidth(line) > lineWidth) {
					// System.out.println(line);
					int j = 0;
					for (j = line.length() - 1; j >= 0; j--) {
						if (line.charAt(j) == ' ') {
							break;
						}
					}
					if (j < 0)
						j = line.length() - 1;
					lines.addElement(line.substring(0, j));
					i = i - (line.length() - j) + 1;
					line = "";
				}
				if (i == src.length() - 1 && !line.trim().equals(""))
					lines.addElement(line);
			}
		}
		return lines;
	}
}