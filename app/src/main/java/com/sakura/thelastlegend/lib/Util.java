package com.sakura.thelastlegend.lib;


import java.io.DataInputStream;
import java.util.Random;

import com.sakura.thelastlegend.network.Message;
import android.content.Intent;
import android.net.Uri;

import com.sakura.thelastlegend.GameCanvas;

public class Util {
	static Random random = new Random();

	public static void onLoadMapComplete() {
		GameCanvas.endDlg();
	}

	public void onLoading() {
		GameCanvas.startWaitDlg("Đang tải dữ liệu");
	}

	public static int randomNumber(int max) {
		return random.nextInt(max);
	}
	
	public static int random(int a, int b) {
		return a + random.nextInt(b - a);
		
	}

	public static byte[] readByteArray(Message msg) {
		try {
			byte[] data = new byte[msg.reader().available()];
			msg.reader().read(data);
			return data;
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return null;
	}
	public static byte[] readByteArray(Message msg,int leng) {
		try {
			byte[] data = new byte[leng];
			msg.reader().read(data);
			return data;
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return null;
	}
	public static byte[] readByteArray(DataInputStream dos) {
		try {
			short lengh = dos.readShort();
			byte[] data = new byte[lengh];
			dos.read(data);
			return data;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static String replace(String text, String regex, String replacement) {
		StringBuffer sBuffer = new StringBuffer();
		int pos = 0;
		while ((pos = text.indexOf(regex)) != -1) {
			sBuffer.append(text.substring(0, pos) + replacement);
			text = text.substring(pos + regex.length());
		}
		sBuffer.append(text);
		return sBuffer.toString();
	}

	public static String numberToString(String number) {
		String value = "", value1 = "";
		if(number.equals(""))
			return value;
		
		if(number.charAt(0) == '-'){
			value1 = "-";
			number = number.substring(1);
		}
		for (int i = number.length() - 1; i >= 0; i--) {
			if ((number.length() - 1 - i) % 3 == 0
					&& (number.length() - 1 - i) > 0)
				value = number.charAt(i) + "." + value;
			else
				value = number.charAt(i) + value;
		}
		return value1 + value;
	}

	public static void sendMsDK(String syntax, short port) {
//		GameMidlet.sendSMS(syntax, "sms://" + port, new Command("",GameCanvas.gI(),88827, null), new Command("", GameCanvas.gI(),88828, null));
//		ThreadManager.sendSMS(syntax, "sms://" + port, new Command("",GameCanvas.gI(),88827, null), new Command("", GameCanvas.gI(),88828, null));
	}

//	public static void downloadGame(String url) {
//		try {
//			GameMidlet.instance.platformRequest(url);
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			GameMidlet.instance.notifyDestroyed();
//		}
//	}


	public static String getTime(int timeRemainS){
		int timeRemainM = 0;
		if (timeRemainS > 60) {
			timeRemainM = timeRemainS / 60;
			timeRemainS = timeRemainS % 60;
		}
		int timeRemainH = 0;
		if (timeRemainM > 60) {
			timeRemainH = timeRemainM / 60;
			timeRemainM = timeRemainM % 60;
		}
		int timeRemainD = 0;
		if (timeRemainH > 24) {
			timeRemainD = timeRemainH / 24;
			timeRemainH = timeRemainH % 24;
		}
		String s = "";
		if (timeRemainD > 0) {
			s += timeRemainD;
			s += "d";
			s += timeRemainH+"h";
					
		}
		else if (timeRemainH > 0) {
			s += timeRemainH;
			s += "h";
			s += timeRemainM+"'";
		}
		else
		{		
			if (timeRemainM > 9)
				s += timeRemainM;
			else
				s += "0" + timeRemainM;
			s += ":";
			
			if (timeRemainS > 9)
				s += timeRemainS;
			else
				s += "0" + timeRemainS;
		}
		return s;
	}

	public static void sleep(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {}
	}
	
	public static String[] split(String original,String separator) {
	    mVector nodes = new mVector();
	    int index = original.indexOf(separator);
	    while(index >= 0) {
	        nodes.addElement( original.substring(0, index) );
	        original = original.substring(index+separator.length());
	        index = original.indexOf(separator);
	    }
	    nodes.addElement( original );
	    String[] result = new String[ nodes.size() ];
	    if( nodes.size() > 0 ) {
	        for(int loop = 0; loop < nodes.size(); loop++)
	        {
	            result[loop] = (String)nodes.elementAt(loop);
	        }

	    }
	    return result;
	}
	
	public static void openUrl(String url) {
		try {
			Intent i = new Intent(Intent.ACTION_VIEW);
			System.out.println("openurl  "+url);
			i.setData(Uri.parse(url));
			Cout.println("--------gCanvas  "+GameCanvas.gCanvas);
			GameCanvas.gCanvas.startActivity(i);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	public static int distance(int x1, int y1, int x2, int y2) {
		return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}

	public static int sqrt(int a) {
		int x, x1;

		if (a <= 0)
			return 0;
		x = (a + 1) / 2;
		do {
			x1 = x;
			x = x / 2 + a / (2 * x);
		} while (Math.abs(x1 - x) > 1);
		return x;
	}
	
	
}
