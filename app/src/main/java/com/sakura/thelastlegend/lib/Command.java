
package com.sakura.thelastlegend.lib;

import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Screen;
import screen.GameScreen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;


public class Command {

	public String caption;
    public String[] subCaption;
    public IActionListener actionListener;
    public int idAction;
    public mBitmap back , focus , img,imgFocus;
	public int x = 0, y = 0, w = Screen.cmdW, h = Screen.cmdH;
	int lenCaption = 0;
	private boolean isFocus = false;
	public boolean isNoPaintImage;
	public Object p;
    /**
     * @param caption
     * @param action
     */
	 public Command(String caption, IActionListener actionListener,int action, Object p, int x, int y) {
		super();
		this.caption = caption;
		this.idAction = action;
        this.actionListener=actionListener;
        this.p=p;
		this.x = x;
		this.y = y;
		this.w = Screen.cmdW;
		this.h = Screen.cmdH;
		this.back = null;
		this.focus = null;
	}
	 public Command(String caption, IActionListener actionListener,int action, Object p,boolean ispaintImg) {
	        super();

	        if(caption.contains("#")){
	        	subCaption = mFont.split(caption,"#");
	        }
	        this.isNoPaintImage = ispaintImg;
	        this.caption = caption;
	        this.idAction = action;
	        this.actionListener=actionListener;
	        this.p=p;
	    }
    public Command(String caption, IActionListener actionListener,int action, Object p) {
        super();
        if(caption.contains("#")){
        	subCaption = mFont.split(caption,"#");
        }
        this.caption = caption;
        this.idAction = action;
        this.actionListener=actionListener;
        this.p=p;
    }
    public Command(String caption, int action, Object p) {
        super();
        this.caption = caption;
        this.idAction = action;
        this.p=p;
    }
    public Command(String caption, int action) {
        super();
        this.caption = caption;
        this.idAction = action;
    }
    public Command(String caption, int action, int x, int y) {
        super();
        this.caption = caption;
        this.idAction = action;
        this.x = x;
        this.y = y;
    }
    
	public void performAction()
	{
		if (idAction > 0){
			if(actionListener!=null)
				actionListener.perform(idAction, p);
			else{
				GameScreen.gI().actionPerform(idAction,p);
			}
		}
	}

    public void paint(mGraphics g) {

//		if(GameScreen.isPaintTeam){
//			x = GameScreen.popupX + GameScreen.popupW;
//    		y = GameScreen.popupY;
//		}
		if(back != null&&!isNoPaintImage)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus&&!isNoPaintImage )
			g.drawImage(focus, x, y, 0);    	
		if (img != null) {
			if(isFocus&&!isNoPaintImage){
				g.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);
			}
			else if(!isNoPaintImage)
				g.drawImage(img, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 - 8 */
							, y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+4,2);
				}else{
					if(subCaption==null)
						mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 */,
								y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight(), 2);
					else {
						for (int i = 0; i < subCaption.length; i++) {
							mFont.tahoma_7b_white.drawString(g, subCaption[i],x  + this.w/2 /*- Image.getHeight(img)/2 */,
									y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()-5+i*10, 2);
						
						}
					}
				}
			}
			else{
				if(subCaption==null)
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2,
						y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+4, 2);
				else {
					for (int i = 0; i < subCaption.length; i++) {
						mFont.tahoma_7b_white.drawString(g, subCaption[i], x  + this.w/2,
								y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+4-5+i*10, 2);
						
					}
				}
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//			}else{
				
			if(subCaption==null)
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 , y + 6, 2);
			else {
				for (int i = 0; i < subCaption.length; i++) {
					mFont.tahoma_7b_white.drawString(g, subCaption[i], x  + this.w/2 , y + 6-5+i*10, 2);
					
				}
			}
//			}
		}
    }
    public void paintW(mGraphics g) {

//		if(GameScreen.isPaintTeam){
//			x = GameScreen.popupX + GameScreen.popupW;
//    		y = GameScreen.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);  
		if (img != null) {
			if(isFocus){
				g.drawRegion(img, 0, 0, w/2-2, img.getHeight(), 0,
						x + w/4, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);
				g.drawRegion(img, 0, 0, w/2-2, img.getHeight(), 2,
						x + 3*w/4-2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);

//				g.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);
			}
			else{
				g.drawRegion(img, 0, 0, w/2-2, img.getHeight(), 0,
						x + w/4, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);
				g.drawRegion(img, 0, 0, w/2-2, img.getHeight(), 2,
						x + 3*w/4-2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);

			}
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2-1 /*- Image.getHeight(img)/2 - 8 */
							, y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2,2);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2-1 /*- Image.getHeight(img)/2 */,
							y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2-1,
						y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2-1 , y + 6, 2);
//			}
		}
		

    }
    public void paintFocus(mGraphics g,boolean isFocus) {

//		if(GameScreen.isPaintTeam){
//			x = GameScreen.popupX + GameScreen.popupW;
//    		y = GameScreen.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);    	
		if (img != null) {
			if(isFocus){
				g.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);
			}
			else
				g.drawImage(img, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 - 8 */
							, y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2,2);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 */,
							y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2,
						y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight()+2, 2);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 , y + 6, 2);
//			}
		}
		

    }
    public void paint(mGraphics g,int transform) { //lat hinh
//		if(GameScreen.isPaintTeam){
//			x = GameScreen.popupX + GameScreen.popupW;
//    		y = GameScreen.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);    	
		if (img != null) {
			if(isFocus){
				g.drawRegion(imgFocus, 0, 0, Image.getWidth(img), Image.getHeight(img), transform,x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER, false);
				//g.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER);
			}
			else
				g.drawRegion(img, 0, 0, Image.getWidth(img), Image.getHeight(img), transform,x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER, false);
			
//				g.drawImage(img, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2 , mGraphics.HCENTER | mGraphics.VCENTER);
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2, y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//			}
		}
		
}
    public void paint(mGraphics g,Boolean isUclip) {
//		if(GameScreen.isPaintTeam){
//			x = GameScreen.popupX + GameScreen.popupW;
//    		y = GameScreen.popupY;
//		}
		if(back != null)
			g.drawImage(back, x, y, 0);
		if(focus != null && isFocus )
			g.drawImage(focus, x, y, 0);    	
		if (img != null) {
			if(isFocus){
				g.drawImage(imgFocus, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2, mGraphics.HCENTER | mGraphics.VCENTER,true);
			}
			else
				g.drawImage(img, x + Image.getWidth(img)/2, y + Image.getHeight(img)/2 , mGraphics.HCENTER | mGraphics.VCENTER,true);
			if(img!=null){
				if(caption.equals("Đăng nhập")){
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 - 8 */
							, y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight(),2,true);
				}else
					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 /*- Image.getHeight(img)/2 */,
							y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight(), 2,true);
			}
			else{
				
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2,
						y + Image.getHeight(img)/2- mFont.tahoma_7b_white.getHeight(), 2,true);
			}
		}
		else
		{
//			if(img!=null){
//				if(caption.equals("Đăng nhập")){
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 - 8 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//				}else
//					mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 - Image.getHeight(img)/2 , y + 6, mGraphics.VCENTER|mGraphics.HCENTER);
//			}else{
				
					
				mFont.tahoma_7b_white.drawString(g, caption, x  + this.w/2 , y + 6, 2);
//			}
		}
		
}

    public boolean input() {
    	isFocus = false;
    	if (GameCanvas.isPointerHoldIn(x, y, w, h)){
			if (GameCanvas.isPointerDown) 
				isFocus = true;

//			else if ( GameCanvas.isPointerClick) {
//				GameCanvas.isPointerClick = false;
//				return true;
//			}
    	}
    	if (GameCanvas.isPoint(x, y, w, h)) {
    		if(GameCanvas.isPointerJustRelease){
    			GameCanvas.isPointerJustRelease = false;
    			return true;
    		}
		}
		return false;
	}
    
    public void setPos(int x, int y, mBitmap img, mBitmap imgFocus) {
    	  this.img = img;
    	  this.imgFocus=imgFocus;
    	  this.x = x;
    	  this.y = y;
    	  if (img != null) {
    	   this.w = Image.getWidth(img)<20?20:Image.getWidth(img);
    	   this.h = Image.getHeight(img)<20?20:Image.getHeight(img);
    	  }
    }
    public boolean isFocusing()
    {
    	return isFocus;
    }
}
