package com.sakura.thelastlegend.lib;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Vector;

import com.sakura.thelastlegend.domain.model.Cmd;
import com.sakura.thelastlegend.network.IMessageHandler;
import com.sakura.thelastlegend.network.ISesion;
import com.sakura.thelastlegend.network.Message;

public class Session_ME implements ISesion {
	protected static Session_ME instance = new Session_ME();

	public static Session_ME gI() {
		return instance;
	}

	private DataOutputStream dos;
	public DataInputStream dis;
	public IMessageHandler messageHandler;
	public Socket sc;
	public boolean connected, connecting;
	private final Sender sender = new Sender();
	public Thread initThread;
	public Thread collectorThread;
	public int sendByteCount;
	public int recvByteCount;
	boolean getKeyComplete;
	public byte[] key = null;
	private byte curR, curW;
	long timeConnected;
	public String strRecvByteCount = "";

	public boolean isConnected() {
		return connected;
	}

	public void setHandler(IMessageHandler messageHandler) {
		this.messageHandler = messageHandler;
	}

	public void connect(String host, int port) {
		if (connected || connecting) {
			return;
		} else {
			getKeyComplete = false;
			sc = null;
			initThread = new Thread(new NetworkInit(host, port));
			initThread.start();
		}
	}//

	public static boolean isCancel;

	class NetworkInit implements Runnable {
		private final String host;
		private final int port;
		
		NetworkInit(String host, int port) {
			this.host = host;
			this.port = port;
		}


		public void run() {
			isCancel = false;
			new Thread(new Runnable() {
				public void run() {
					try {
						Thread.sleep(20000);
					} catch (InterruptedException e) {
					}
					if (connecting) {
						try {
							sc.close();
						} catch (Exception e) {
						}
						isCancel = true;
						connecting = false;
						connected = false;
						messageHandler.onConnectionFail();
					}
				}
			}).start();
			connecting = true;
			Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
			connected = true;
			try {
				doConnect(host,port);
				messageHandler.onConnectOK();
			} catch (Exception ex) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
				if (isCancel)
					return;
				if (messageHandler != null) {
					close();
					messageHandler.onConnectionFail();
				}
			}
		}

		public void doConnect(String host, int port) throws Exception {
			System.out.println("doConnect " + host+" : "+port);
			/*
			 * sc = (SocketConnection) Connector.open(host); dos =
			 * sc.openDataOutputStream(); dis = sc.openDataInputStream();
			 */

			sc = new Socket(host, port);
			sc.setKeepAlive(true);
			dos = new DataOutputStream(sc.getOutputStream());
			dis = new DataInputStream(sc.getInputStream());

			new Thread(sender).start();
			collectorThread = new Thread(new MessageCollector());
			collectorThread.start();
			timeConnected = System.currentTimeMillis();
			//
			doSendMessage(new Message((byte) -27));
			connecting = false;
			// messageHandler.onConnectOK(); MOVE TO GET KEY
		}
	}

	public void sendMessage(Message message) {
//		System.out.println(">>>>>>>>>>>>>>>>>SEND MSG: " + message.command);
		sender.AddMessage(message);
	}

	private synchronized void doSendMessage(Message m) throws IOException {
//		System.out.println(">>>>>>>>>>>>>>>>>DO SEND MSG: " + m.command);
		byte[] data = m.getData();
		try {
			if (getKeyComplete) {
				byte b = (writeKey(m.command));
				dos.writeByte(b);
			} else
				dos.writeByte(m.command);
			if (data != null) {
				int size = data.length;
				if (getKeyComplete) {
					int byte1 = writeKey((byte) (size >> 8));
					dos.writeByte(byte1);
					int byte2 = writeKey((byte) (size & 0xFF));
					dos.writeByte(byte2);
				} else
					dos.writeShort(size);
				//
				if (getKeyComplete)
					for (int i = 0; i < data.length; i++)
						data[i] = writeKey(data[i]);
				dos.write(data);
				sendByteCount += (5 + data.length);
			} else {
				dos.writeShort(0);
				sendByteCount += 5;
			}
			dos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private byte readKey(byte b) {
		byte i = (byte) ((key[curR++] & 0xff) ^ (b & 0xff));
		if (curR >= key.length)
			curR %= key.length;
		return i;
	}

	private byte writeKey(byte b) {
		byte i = (byte) ((key[curW++] & 0xff) ^ (b & 0xff));
		if (curW >= key.length)
			curW %= key.length;
		return i;
	}

	private  Vector sendingMessage;
	private class Sender implements Runnable {

		public Sender() {
			sendingMessage = new Vector();
		}

		public void AddMessage(Message message) {
	
			sendingMessage.addElement(message);
		}

			public void run() {
				while (connected) {
					try {
						if (getKeyComplete)
							while (sendingMessage.size() > 0) {
								Message m = (Message) sendingMessage.elementAt(0);
								sendingMessage.removeElementAt(0);
								doSendMessage(m);
							}
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

	class MessageCollector implements Runnable {
		public void run() {
			com.sakura.thelastlegend.network.Message message;
			try {
				while (isConnected()) {
					message = readMessage();
					if (message != null) {
						try {
							if (message.command == -27) {
								getKey(message);
							} else{
//								GameCanvas.debug("READ MSG: "+message.command);
								messageHandler.onMessage(message);
								}
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						break;
					}
				}
			} catch (Exception ex) {
			}
			if (connected) {
				if (messageHandler != null) {
					if (System.currentTimeMillis() - timeConnected > 1000)
						messageHandler.onDisconnected();
					else
						messageHandler.onConnectionFail();
				}
				if (sc != null)
					cleanNetwork();
			}
		}

		private void getKey(Message message) throws IOException {
			byte keySize = message.reader().readByte();
			key = new byte[keySize];
			for (int i = 0; i < keySize; i++) {
				key[i] = message.reader().readByte();
			}
			for (int i = 0; i < key.length - 1; i++)
				key[i + 1] ^= key[i];
			getKeyComplete = true;
		}

		private Message readMessage() throws Exception {
			
			// read message command
			byte cmd = dis.readByte();
			if (getKeyComplete)
				cmd = readKey(cmd);
//			System.out.println("cmd Read ---> "+cmd);
			// Res.out("................cmd= " + cmd);
//			if (cmd == -32 || cmd == -66 || cmd == 11 || cmd == -67 || cmd==-74 || cmd==-87) {
//				return readMessage2(cmd);
//			}
			// read size of data
			int size;
			if (getKeyComplete) {
				if(cmd == Cmd.LOGIN || cmd == Cmd.REQUEST_IMAGE){
					 byte b1 = readKey(dis.readByte());
						byte b2 = readKey(dis.readByte());
						byte b3 = readKey(dis.readByte());
						byte b4 = readKey(dis.readByte());
						size = ((b1 & 0xff) << 24) | ((b2 & 0xff) << 16)  | ((b3 & 0xff) << 8)  | (b4 & 0xff);
					
				}else{ // khac -27
					byte b1 = dis.readByte();
					byte b2 = dis.readByte();
					size = (readKey(b1) & 0xff) << 8 | readKey(b2) & 0xff;	
					
				}
			} else
				size = dis.readUnsignedShort();
			byte data[] = new byte[size];
			int len = 0;
			int byteRead = 0;
			while (len != -1 && byteRead < size) {
				len = dis.read(data, byteRead, size - byteRead);
				if (len > 0) {
					byteRead += len;
					recvByteCount += (5 + byteRead);
					int Kb = (Session_ME.gI().recvByteCount + Session_ME.gI().sendByteCount);
					strRecvByteCount = Kb / 1024 + "." + Kb % 1024 / 102 + "Kb";
				}
			}
			if (getKeyComplete)
				for (int i = 0; i < data.length; i++) {
					data[i] = readKey(data[i]);
				}
			Message msg = new Message(cmd, data);
			return msg;
		}
	}

	public void close() {
		cleanNetwork();
	}

	private void cleanNetwork() {
		key = null;
		curR = 0;
		curW = 0;
		try {
			connected = false;
			connecting = false;
			if (sc != null) {
				sc.close();
				sc = null;
			}
			if (dos != null) {
				dos.close();
				dos = null;
			}
			if (dis != null) {
				dis.close();
				dis = null;
			}
			collectorThread = null;
			// if (initThread != null && initThread.isAlive()) {
			// initThread.interrupt();
			// initThread = null;
			// }
			mSystem.my_Gc();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void connect(String host, String port) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connect(String host) {
		// TODO Auto-generated method stub
		
	}
}
