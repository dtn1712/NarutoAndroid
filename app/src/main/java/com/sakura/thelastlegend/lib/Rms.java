package com.sakura.thelastlegend.lib;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import android.app.Activity;
import android.content.Context;

import com.sakura.thelastlegend.GameCanvas;

public class Rms {

	public static String rms_Auto = "autosetting";
	public static void saveRMS(String filename, byte[] data) {
		saveRMSData(filename, data, GameCanvas.gCanvas);
	}


	public static byte[] loadRMS(String filename) {
		return loadRMSData(filename, GameCanvas.gCanvas);
	}

	public static final byte[] loadRMSData(String name, Activity Act) {

		FileInputStream fis = null;
		
		try {
			fis = Act.openFileInput(name);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] b = new byte[1024];
			int bytesRead;
			while ((bytesRead = fis.read(b)) != -1) {
				bos.write(b, 0, bytesRead);
			}
			byte[] data = bos.toByteArray();

			if (fis != null) {
				fis.close();
				fis = null;
				bos.close();
				bos = null;
//				System.out.println("load rms '" + name + "' suscess!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				return data;
			}
		} catch (Exception ex) {
//			System.out.println("load rms '" + name + "' error!");
//			ex.printStackTrace();
			return null;
		}
		return null;

	}

	public static void saveRMSInt(String file, int x) {
		try {
			saveRMS(file, new byte[] { (byte) x });
		} catch (Exception e) {
		}
	}

	public static void saveRMSString(String filename, String s) {
		try {
			saveRMS(filename, s.getBytes("UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String loadRMSString(String filename) {
		byte[] data = loadRMS(filename);
		if (data == null)
			return null;
		else
			try {
				String s = new String(data, "UTF-8");
				return s;
			} catch (Exception e) {
				return new String(data);
			}
	}

	public static final void saveRMSData(String name, byte[] data, Activity Act) {
		try {
			FileOutputStream fos = Act.openFileOutput(name, Context.MODE_WORLD_WRITEABLE);
			fos.write(data);
			if (fos != null) {
				data = null;
				fos.close();
				Cout.println("save rms '" + name + "' suscess!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}
		} catch (Exception ex) {
			data = null;
			Cout.println("save rms '" + name + "' error!");
		}

	}
	public static final void saveRMSData(String filename, byte[] data) {
		
		saveRMSData(filename, data, GameCanvas.gCanvas);
	}

	public static int loadRMSInt(String file) {
		byte[] b = loadRMS(file);
		return b == null ? -1 : b[0];
	}
	
	
	public static void deleteRecord(String fileName) {
		GameCanvas.gCanvas.deleteFile(fileName);
	}
	
	public static void clearRMS() {
		deleteRecord("nj_arrow");
		deleteRecord("nj_effect");
		deleteRecord("nj_image");
		deleteRecord("nj_part");
		deleteRecord("nj_skill");
		deleteRecord("data");
		deleteRecord("dataVersion");
		deleteRecord("map");
		deleteRecord("mapVersion");
		deleteRecord("skill");
		deleteRecord("killVersion");
		deleteRecord("item");
		deleteRecord("itemVersion");
	}


}
