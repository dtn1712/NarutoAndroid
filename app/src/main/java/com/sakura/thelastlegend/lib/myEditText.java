package com.sakura.thelastlegend.lib;


import com.sakura.thelastlegend.domain.model.ChatTextField;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.mGraphics;

public class myEditText extends EditText {
	public static int x = 0, y = 0, width = 100 * mGraphics.zoomLevel, height = 20 * mGraphics.zoomLevel;

	public static boolean isVisible = false;
	public static myEditText instance;

	public int ID;
	public RelativeLayout.LayoutParams myParames;

	public myEditText(Context context) {
		super(context);
		instance = this;
		setFocusable(true);
		setFocusableInTouchMode(true);
		init();
	}

	public myEditText(Context context, int width, int height) {
		super(context);
		instance = this;
		// TODO Auto-generated constructor stub
		setFocusable(true);
		setFocusableInTouchMode(true);
		init(width, height);
		
	}

	public void init() {
		init(width, height);
	}

	public void init(int width, int height) {
		// if (!GameMidlet.isChaCha) {
		   setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
		   OnEditorActionListener cmd = new OnEditorActionListener() {
		    public boolean onEditorAction(TextView v, int actionId,
		      KeyEvent event) {
		    	action_Next_Done(v, actionId, event);
		    	
		     return true;
		    }
		   };
		   this.setOnEditorActionListener(cmd);
	  //}
		myParames = new RelativeLayout.LayoutParams(width, height);
		myParames.leftMargin = 0;
		myParames.topMargin = 0;
		
		this.setLayoutParams(myParames);

	
		this.setBackgroundColor(Color.TRANSPARENT);

		//this.setTextColor(0xFFefe310);
		this.setTextColor(Color.YELLOW);
		this.setTypeface(Typeface.DEFAULT_BOLD);
		this.setSingleLine();
		// this.setVisibility(View.INVISIBLE);
		
		if (GameCanvas.h < 400)
			this.setTextSize(14f);
		else
			this.setTextSize(16f);

		// for galaxy tab 10.1 in
//		if (Build.VERSION.SDK_INT >= 11)
//			this.setTextSize(20);

		// GameCanvas.CanvasAct.getWindow().setSoftInputMode(
		// WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

		// this.requestFocus();
		// if ( this.isFocused())

	}

	public void action_Next_Done(TextView v, int actionId, KeyEvent event) {
		//Cout.println( "action_Next_Done  "+  actionId+ " actionId "+EditorInfo.IME_ACTION_DONE);
		//tren bluestack done = 0// trên đt done = 6

		Cout.println(KeyEvent.KEYCODE_BACK+"  action_Next_Done   "+actionId);
		if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT || actionId==KeyEvent.KEYCODE_BACK) {
			//Cout.println( "action_Next_Done  "+  EditorInfo.IME_ACTION_DONE+ " actionId ");
			//			if(this.ID==3){
//				for (int i = 0; i < GameMidlet.allField.size(); i++) {
//					if (GameMidlet.allField.elementAt(i).ID == this.ID) {
//						GameMidlet.allField.elementAt(i).setText(this.getString());
//						GameMidlet.allField.elementAt(i).isFocus = false;
//						GameMidlet.allField.elementAt(i+1).isFocus = true;
//						LoginScreen.gI().tfUser.isFocus = false;
//						LoginScreen.gI().tfPass.isFocus = true;
//						LoginScreen.gI().tfRegPass.isFocus = false;
//						LoginScreen.gI().right = LoginScreen.gI().tfPass.cmdClear;
//						this.ID++;
//						GameMidlet.allField.elementAt(i+1).setText("");
//						break;
//					}
//				}
//			}else{
			hideSoftKeyboard();
			for (int i = 0; i < GameMidlet.allField.size(); i++) {
				if (GameMidlet.allField.elementAt(i).ID == this.ID) {
					GameMidlet.allField.elementAt(i).setText(this.getString());
					GameMidlet.allField.elementAt(i).isFocus = false;
					if(actionId == EditorInfo.IME_ACTION_DONE)
					GameMidlet.allField.elementAt(i).isOKReturn = true;
					((GameMidlet) GameCanvas.gCanvas).update(2, this);
					if(ChatTextField.gI().isShow)
						GameCanvas.keyPressed[12] = true;
					break;
				}
//			}
			}
		}
	}
		   
	@Override
	public void onDraw(Canvas canvas) {
		// myCanvas = canvas;

//		 canvas.translate(xPosition, yPosition);
//		 this.layout(xPosition, yPosition, xPosition + 100, yPosition + 30);
		if (strchange.compareTo(getString()) != 0) {
			strchange = getString();
			for (int i = 0; i < GameMidlet.allField.size(); i++) {
				if (GameMidlet.allField.elementAt(i).ID == this.ID) {
					GameMidlet.allField.elementAt(i).setText(this.getString());
				}
			}
		}
		//Font.tahoma_8b.drawString(g, text, xText, yText, 0);;
	}

	public String getString() {
		Editable e = this.getText();

		return e.toString();
	}

	String strchange = "";

	public void setText(String t) {
		this.setText(t, TextView.BufferType.EDITABLE);
		strchange = t;
	}

	
	public void clear() {
		this.setText("", TextView.BufferType.EDITABLE);
	}

	public void setMaxTextInput(int maxText) {
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(maxText);
		this.setFilters(FilterArray);
	}

	public void setWidthHeight(int w, int h) {
		myParames.width = w;
		myParames.height = h;

		this.setWidth(w);
		this.setHeight(h);
	}

	public void changePosition(int dx, int dy) {
		/*
		 * xPosition += dx; yPosition += dy; this.invalidate();
		 */

		// invisible before change position
		int tempVisible = this.getVisibility();
		this.setVisibility(View.INVISIBLE);

		x += dx;
		y += dy;

		myParames.leftMargin += dx;
		myParames.topMargin += dy;
		this.setLayoutParams(myParames);

		this.setVisibility(tempVisible);

	}

	public void setPosition(int dx, int dy) {
		/*
		 * xPosition = x; yPosition = y; //this.invalidate();
		 * //this.postInvalidate();
		 */

		// invisible before change position
		// int tempVisible = this.getVisibility();
		// this.setVisibility(View.INVISIBLE);

		x = dx;
		y = dy;
		myParames.leftMargin = x;
		myParames.topMargin = y;
		this.setLayoutParams(myParames);

		// this.setVisibility(tempVisible);
	}

	public void setmyInputType(int type) {
		switch (type) {
		case TField.INPUT_TYPE_ANY:
			setInputType(InputType.TYPE_CLASS_TEXT);
			break;
		case TField.INPUT_TYPE_NUMERIC:
			setInputType(InputType.TYPE_CLASS_NUMBER);
			break;
		case TField.INPUT_TYPE_PASSWORD:
			setInputType(InputType.TYPE_CLASS_TEXT);
			setTransformationMethod(PasswordTransformationMethod.getInstance());
			break;
		case TField.INPUT_ALPHA_NUMBER_ONLY:
			setInputType(InputType.TYPE_CLASS_TEXT);
			break;
		}
	}

	public void setVisible(boolean v) {
		isVisible = v;
		if (v) {

			this.setVisibility(View.VISIBLE);
		} else
			this.setVisibility(View.INVISIBLE);
	}

	public void OkiTest() {
		hideSoftKeyboard();
		Cout.println(getClass(), EditorInfo.IME_ACTION_DONE+ " OkiTestactionId ");
		
		for (int i = 0; i < GameMidlet.allField.size(); i++) {
			if (GameMidlet.allField.elementAt(i).ID == this.ID) {
				GameMidlet.allField.elementAt(i).setText(this.getString());
			}
		}
		myEditText.isVisible = false;
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode== KeyEvent.KEYCODE_BACK){
			 GameCanvas.keyPressed[14] = true;
		}
		if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode== KeyEvent.KEYCODE_BACK) {
			hideSoftKeyboard();
			
			for (int i = 0; i < GameMidlet.allField.size(); i++) {
				GameMidlet.allField.elementAt(i).isFocus = false;
				if (GameMidlet.allField.elementAt(i).ID == this.ID) {
					GameMidlet.allField.elementAt(i).setText(this.getString());
					((GameMidlet) GameCanvas.gCanvas).update(2, this);
					GameMidlet.allField.elementAt(i).isOKReturn = true;
					if(ChatTextField.gI().isShow)
						GameCanvas.keyPressed[12] = true;
//					if(!GameMidlet.Model.contains("HTC ChaCha")){
//						ChatTextField.gI().tfChat.justReturnFromTextBox = true;
//					}
				
				}
			}

			this.setVisible(false);
		}

	
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onFocusChanged(boolean f, int direction,
			Rect previouslyFocusedRect) {

		if (!f) {
			hideSoftKeyboard();

			for (int i = 0; i < GameMidlet.allField.size(); i++) {
				if (GameMidlet.allField.elementAt(i).ID == this.ID) {
					GameMidlet.allField.elementAt(i).setText(
							this.getString());
					((GameMidlet) GameCanvas.gCanvas).update(2, this);
				}
			}

			this.setVisible(false);
		} else {
			showSoftKeyboard();
		}

		super.onFocusChanged(f, direction, previouslyFocusedRect);
	}

	private void hideSoftKeyboard() {
		InputMethodManager imm = (InputMethodManager) GameCanvas.Context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(this.getWindowToken(), 0);
	}

	private void showSoftKeyboard() {
		InputMethodManager imm = (InputMethodManager) GameCanvas.Context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(this, 0);
	}

	public void end() {
		hideSoftKeyboard();
		for (int i = 0; i < GameMidlet.allField.size(); i++) {
			if (GameMidlet.allField.elementAt(i).ID == this.ID) {
				if (GameMidlet.allField.elementAt(i).isFocus) {
					GameMidlet.allField.elementAt(i).setText("");
					GameMidlet.allField.elementAt(i).isFocus = false;
//					GameMidlet.allField.elementAt(i).editText.
					((GameMidlet) GameCanvas.gCanvas).update(2, this);
				}

			}
		}

	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public boolean onKeyPreIme(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
	        //keyboard will be hidden
			for (int i = 0; i < GameMidlet.allField.size(); i++) {
				if (GameMidlet.allField.elementAt(i).ID == this.ID) {
					if (GameMidlet.allField.elementAt(i).isFocus) {
						GameMidlet.allField.elementAt(i).isFocus = false;
						((GameMidlet) GameCanvas.gCanvas).update(2, this);}

				}
			}
	    }
		
		return super.onKeyPreIme(keyCode, event);
	}
}
