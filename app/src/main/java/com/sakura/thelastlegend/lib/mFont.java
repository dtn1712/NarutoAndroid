package com.sakura.thelastlegend.lib;

import java.io.DataInputStream;
import java.io.IOException;

import android.graphics.Color;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;


public class mFont {

	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	public static final int CENTER = 2;
	public static final int RED = 0;

	public static final int YELLOW = 1;
	public static final int GREEN = 2;
	public static final int FATAL = 3;
	public static final int MISS = 4;
	public static final int ORANGE = 5;
	public static final int ADDMONEY = 6;
	public static final int MISS_ME = 7;
	public static final int FATAL_ME = 8;
	public static final int HP = 9;
	public static final int MP = 10;
	public static final int XP = 11;
	public static final int WHITE = 12;
	public static final int NAMEITEMXANH = 13;
	public static final int NAMEITEMTRANG = 14;

	private int space;
	private int height;
	private mBitmap imgFont;
	private String strFont;
	private int fImages[][];

	// ===============================================
	public static String str = " 0123456789+-*='_?.,<>/[]{}!@#$%^&*():aÃ¡Ã áº£Ã£áº¡Ã¢áº¥áº§áº©áº«áº­Äƒáº¯áº±áº³áºµáº·bcdÄ‘eÃ©Ã¨áº»áº½áº¹Ãªáº¿á»�á»ƒá»…á»‡fghiÃ­Ã¬á»‰Ä©á»‹jklmnoÃ³Ã²á»�Ãµá»�Ã´á»‘á»“á»•á»—á»™Æ¡á»›á»�á»Ÿá»¡á»£pqrstuÃºÃ¹á»§Å©á»¥Æ°á»©á»«á»­á»¯á»±vxyÃ½á»³á»·á»¹á»µzwAÃ�Ã€áº¢Ãƒáº Ä‚áº°áº®áº²áº´áº¶Ã‚áº¤áº¦áº¨áºªáº¬BCDÄ�EÃ‰Ãˆáººáº¼áº¸ÃŠáº¾á»€á»‚á»„á»†FGHIÃ�ÃŒá»ˆÄ¨á»ŠJKLMNOÃ“Ã’á»ŽÃ•á»ŒÃ”á»�á»’á»”á»–á»˜Æ á»šá»œá»žá» á»¢PQRSTUÃšÃ™á»¦Å¨á»¤Æ¯á»¨á»ªá»¬á»®á»°VXYÃ�á»²á»¶á»¸á»´ZW";
	public static mFont tahoma_7b_redsmall;
	public static mFont tahoma_7b_whitesmall; 
	public static mFont tahoma_7b_whitesmall_size6; 
	public static mFont tahoma_7b_red; // 1
	public static mFont tahoma_7b_bluesmall,tahoma_7b_violetsmall;
	public static mFont tahoma_7b_blue ; // 2
	public static mFont tahoma_7b_white;// 3
	public static mFont tahoma_7b_yellow ; // 4
	public static mFont tahoma_7b_yellowMainTab ; // 4
	public static mFont tahoma_7b_yellowsmall ; // 4
	public static mFont tahoma_7b_dark ; // 5
	public static mFont tahoma_7b_dark_small ; // 5
	public static mFont tahoma_7b_popup ; // 5
	public static mFont tahoma_7b_yellowsmall_size6 ; // 4

	public static mFont tahoma_7b_green2 ;// 6
	public static mFont tahoma_7b_green ; // 7
	public static mFont tahoma_7_button;
	public static mFont tahoma_7b_greensmall;
	public static mFont tahoma_7b_focus ;//8
	public static mFont tahoma_7b_unfocus; //9
	public static mFont tahoma_7; //10
	public static mFont tahoma_7_blue1 ; //11
	public static mFont tahoma_7_green2 ;//12
	public static mFont tahoma_7_yellow;//13
	public static mFont tahoma_7_yellow_xu;//13
	public static mFont tahoma_7_grey;//14
	public static mFont tahoma_7_red ;//15
	public static mFont tahoma_7_blue ;//16
	public static mFont tahoma_7_green ;//17
	public static mFont tahoma_7_white ;//18
	public static mFont tahoma_7_white_size6 ;//18
	public static mFont tahoma_7_white_size7 ;//18
	public static mFont tahoma_8b;//19
	public static mFont  tahoma_8btf,tahoma_8btf_unfocus;
	public static mFont number_yellow_7b_size6;
	public static mFont number_yellow ;//20
	public static mFont number_red_7b_size6;
	public static mFont number_red ;//21
	public static mFont number_green;//22
	public static mFont number_gray;//23
	public static mFont number_orange ;//24
	
	public static mFont number_yellow_xp;
	public static mFont number_red_hp;
	public static mFont number_green_mp;
	
	
	public static mFont bigNumber_red;//25
	public static mFont bigNumber_While; // 26
	public static mFont bigNumber_yellow ;//27	
	public static mFont bigNumber_green ;//28
	public static mFont bigNumber_orange;//29
	public static mFont bigNumber_blue ; //30
	
	public static mFont nameFontRed=tahoma_7b_red;
	public static mFont nameFontYellow=tahoma_7_yellow;
	public static mFont nameFontGreen=tahoma_7_green;
	public static mFont  tahoma_7_redsmall;
	public static mFont tahoma_7_whitesmall;// 3
	public static mFont tahoma_7_bluesmall;
	public static mFont tahoma_7_yellowsmall;
	public static mFont tahoma_6_white;
	public static mFont tahoma_10b;
	    
	public static void loadBegin(){
		  tahoma_7b_red = new mFont(str, "/font/tahoma_7b_red", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",11,0xffff0000); // 1
		  tahoma_7b_whitesmall = new mFont(str, "/font/tahoma_7b_white", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xffffffff); // 1
		  tahoma_7b_whitesmall_size6 = new mFont(str, "/font/tahoma_7b_white", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",6,0xffffffff); // 1
		  
		  tahoma_7b_blue = new mFont(str, "/font/tahoma_7b_blue", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",9,0xff637dff); // 2
		  tahoma_7b_white = new mFont(str, "/font/tahoma_7b_white", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",9,0xffffffff); // 3
		  tahoma_7b_yellow = new mFont(str, "/font/tahoma_7b_yellow", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",10,0xffffaa00); // 4
		  tahoma_7b_yellowsmall = new mFont(str, "/font/tahoma_7b_yellow", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xffffe21e); // 4
		  tahoma_7b_yellowsmall_size6 = new mFont(str, "/font/tahoma_7b_yellow", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",6,0xffffe21e); // 4
		  
		  tahoma_7b_dark = new mFont(str, "/font/tahoma_7b_brown", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",10,0xff532905); // 5
		  tahoma_7b_dark_small = new mFont(str, "/font/tahoma_7b_brown", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xff532905); // 5
		  tahoma_7b_bluesmall= new mFont(str, "/font/tahoma_7b_blue", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xff637dff); // 2
		  tahoma_7b_redsmall = new mFont(str, "/font/tahoma_7b_red", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xffff0000); // 1
		  tahoma_7b_yellowMainTab = new mFont(str, "/font/tahoma_7b_yellow", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",9,0xffffaa00); // 4
		  tahoma_7b_popup = new mFont(str, "/font/tahoma_7b_brown", "/font/tahoma_7b", 0,"font/UVNTHANGVU_2.ttf",11,0xff000000); // 5
		  
		  
		  tahoma_7b_green2 = new mFont(str, "/font/tahoma_7b_green2", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",11,0xff005325); // 6
		  tahoma_7b_green = new mFont(str, "/font/tahoma_7b_green", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",9,0xff00cc00); // 7
		  tahoma_7b_focus = new mFont(str, "/font/tahoma_7b_focus", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xff000000); //8
		  tahoma_7b_greensmall = new mFont(str, "/font/tahoma_7b_green", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xff00cc00); // 7
		  tahoma_7_button = new mFont(str, "/font/tahoma_7", "/font/tahoma_7", 0,"font/tahoma_7.ttf",9,0xff000000); //10
			  
		  tahoma_7b_unfocus = new mFont(str, "/font/tahoma_7b_unfocus", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xff000000); //9
		  tahoma_7 = new mFont(str, "/font/tahoma_7", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xff000000); //10
		  tahoma_7_blue1 = new mFont(str, "/font/tahoma_7_blue1", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xff00ffff); //11
		  tahoma_7_green2 = new mFont(str, "/font/tahoma_7_green2", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xff005325);//12
		  tahoma_7_yellow = new mFont(str, "/font/tahoma_7_yellow", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xffffff00);//13
		  tahoma_7_yellow_xu = new mFont(str, "/font/tahoma_7_yellow", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xffffff00);//13
		  tahoma_7_grey = new mFont(str, "/font/tahoma_7_grey", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xff555555);//14
		  tahoma_7_red = new mFont(str, "/font/tahoma_7_red", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xffff7777);//15
		  tahoma_7_blue = new mFont(str, "/font/tahoma_7_blue", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xff0080ff);//16
		  tahoma_7_green = new mFont(str, "/font/tahoma_7_green", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xff20eb5f);//17 20eb5f
		  tahoma_7_white = new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",8,0xffefebef);//18
		  tahoma_7_white_size7 = new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",7,0xffefebef);//18
		  tahoma_7_white_size6 = new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",6,0xffead69a);//18
		  tahoma_8b = new mFont(str, "/font/tahoma_8b", "/font/tahoma_8b", -1,"font/tahoma_8b.ttf",9,0xffeecc66);//19
		  tahoma_8btf = new mFont(str, "/font/tahoma_8b", "/font/tahoma_8b", -1,"font/tahoma_8b.ttf",8,0xff000000);//19
		  tahoma_10b= new mFont(" 0123456789+-", "/font/number_yellow", "/font/number", 0,"font/tahoma_8b.ttf",14,0xffffdf2f);//20
		  
		  tahoma_7_bluesmall = new mFont(str, "/font/tahoma_7_blue", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",7,0xff0080ff);//16
		  tahoma_7_yellowsmall= new mFont(str, "/font/tahoma_7_yellow", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",7,0xffffff00);//16
		  
		  tahoma_7_whitesmall = new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",7,0xffefebef);//18
		  tahoma_7_redsmall = new mFont(str, "/font/tahoma_7_red", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",7,0xffff7777);//15
		  number_yellow_7b_size6 = new mFont(" 0123456789+-", "/font/number_yellow", "/font/number", 0,"font/UVNTHANGVU_2.ttf",16,0xffffdf2f);//20
		  tahoma_7b_violetsmall= new mFont(str, "/font/tahoma_7b_red", "/font/tahoma_7b", 0,"font/tahoma_7b.ttf",8,0xffd105e9); // 1
		  
		  tahoma_8btf_unfocus   = new mFont(str, "/font/tahoma_8b", "/font/tahoma_8b", -1,"font/tahoma_8b.ttf",8,0xff000000);//19
		  tahoma_6_white =  new mFont(str, "/font/tahoma_7_white", "/font/tahoma_7", 0,"font/tahoma_7b.ttf",7,0xffefebef);//18
		  
		  number_yellow = new mFont(" 0123456789+-", "/font/number_yellow", "/font/number", 0,"font/tahoma_7b.ttf",8,0xffffdf2f);//20
		  number_red = new mFont(" 0123456789+-", "/font/number_red", "/font/number", 0,"font/tahoma_7b.ttf",16,0xffe44a55);//21
		  number_green = new mFont(" 0123456789+-", "/font/number_green", "/font/number", 0,"font/tahoma_7.ttf",8,0xff3ef0e3);//22
		  number_red_7b_size6 = new mFont(" 0123456789+-", "/font/number_red", "/font/number", 0,"font/UVNTHANGVU_2.ttf",12,0xffff0012);//21
		  number_gray = new mFont(" 0123456789+-", "/font/number_gray", "/font/number", 0,"font/tahoma_7.ttf",8,0xff474747);//23
		  number_orange = new mFont(" 0123456789+-", "/font/number_orange", "/font/number", 0,"font/tahoma_7.ttf",8,0xfff59c38);//24
		  
		  number_yellow_xp = new mFont(" 0123456789+-", "/font/number_yellow", "/font/number", 0,"font/UTM Azuki.ttf",14,0xffffdf2f);//20
		  number_red_hp = new mFont(" 0123456789+-", "/font/number_red", "/font/number", 0,"font/UTM Azuki.ttf",14,0xffDF0101);//21
		  number_green_mp = new mFont(" 0123456789+-", "/font/number_green", "/font/number", 0,"font/UTM Azuki.ttf",14,0xff0000FF);//22
		 
		
		  bigNumber_red = new mFont(" 0123456789+-", "/font/number_red", "/font/number", 0,"font/tahoma_8b.ttf",12,0xffff0000);//25
		  bigNumber_While = new mFont(str, "/font/tahoma_7b_white", "/font/tahoma_7b", 0,"font/tahoma_8b.ttf",12,0xffffffff); // 26
		  bigNumber_yellow = new mFont(" 0123456789+-", "/font/number_yellow", "/font/number", 0,"font/tahoma_8b.ttf",12,0xffffaa00);//27	
		  bigNumber_green = new mFont(" 0123456789+-", "/font/number_green", "/font/number", 0,"font/tahoma_8b.ttf",12,0xff00cc00);//28
		  bigNumber_orange = new mFont(" 0123456789+-", "/font/number_orange", "/font/number", 0,"font/tahoma_8b.ttf",12,0xfff59c38);//29
		  bigNumber_blue = new mFont(str, "/font/tahoma_7_blue1", "/font/tahoma_7", 0,"font/tahoma_8b.ttf",12,0xff637dff); //30
		  FontSys.number_yellow = number_yellow;
		  FontSys.tahoma_8b =   tahoma_8b;
		  nameFontRed=tahoma_7b_red;
		  nameFontYellow=tahoma_7_yellow;
		  nameFontGreen=tahoma_7_green;
	}
	String pathImage;
	public FontSys fontSys;
	public String name;

	public mFont(String strFont, String pathImage, String pathData, int space, String pathSysFontFile, int SysFontSize, int SysFontColor) {
		name = pathImage;
		pathSysFontFile = pathSysFontFile;
//		SysFontSize -=1;
		if(mGraphics.zoomLevel>0)
		{
			if(mGraphics.zoomLevel==1)
				SysFontSize+=  SysFontSize/4;
			fontSys = new FontSys(pathSysFontFile, SysFontSize, SysFontColor);
			return;
		}
		try {
			this.strFont = strFont;
			this.space = space;
			this.pathImage = pathImage;
			DataInputStream dis = null;
			reloadImage();
			try {
				dis = new DataInputStream(MyStream.readFile((pathData)));
				fImages = new int[dis.readShort()][];
				for (int i = 0; i < fImages.length; i++) {
					fImages[i] = new int[4];
					fImages[i][0] = dis.readShort(); // x
					fImages[i][1] = dis.readShort(); // y
					fImages[i][2] = dis.readShort(); // width
					fImages[i][3] = dis.readShort(); // height
					height = fImages[i][3];
				}
			} catch (Exception e) {
				try {
					dis.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Font load error:" + pathImage);
		}
	}

	public void reloadImage() {
		this.imgFont = GameCanvas.loadImage(pathImage+".png");
	}

	public void freeImage() {
		this.imgFont = null;
	}

	public int getHeight() {
		if(fontSys!=null)return fontSys.getHeight();
		return this.height;
	}

	public int getWidth(String st) {
		if(fontSys!=null)return fontSys.getWidth(st);
		int pos;
		int len = 0;
		return len;
	}
	
	public void drawString(mGraphics g, String st, int x, int y, int align, mFont font) {
		if(st==null) return;
		if(font!=null)
		{
			font.drawString(g, st, x+1, y, align);
			font.drawString(g, st, x, y+1, align);
		}
		drawString(g, st, x, y, align);
	}
	public void drawString(mGraphics g, String st, int x, int y, int align, mFont font,boolean isUclip) {
		if(font!=null)
		{
			font.drawString(g, st, x+1, y, align);
			font.drawString(g, st, x, y+1, align);
		}
		drawString(g, st, x, y, align);
	}
	
	public void drawString(mGraphics g, String st, int x, int y, int align) {
		if(fontSys!=null)
		{
			fontSys.drawString(g, st, x, y, align);
			return;
		}
		int pos;
		int x1;
		int len = st.length();
		if (align == 0)
			x1 = x;
		else if (align == 1)
			x1 = x - getWidth(st);
		else
			x1 = x - (getWidth(st) >> 1);
		for (int i = 0; i < len; i++) {
			pos = strFont.indexOf(st.charAt(i));
			if (pos == -1)
				pos = 0;
			if (pos > -1) {
				g.drawRegion(imgFont, fImages[pos][0], fImages[pos][1], fImages[pos][2], fImages[pos][3], 0, x1, y, 20);
			}
			x1 += fImages[pos][2] + space;
		}
	}

	public void drawString(mGraphics g, String st, int x, int y, int align,boolean isUclip) {
		if(fontSys!=null)
		{
			fontSys.drawString(g, st, x, y, align);
			return;
		}
		int pos;
		int x1;
		int len = st.length();
		if (align == 0)
			x1 = x;
		else if (align == 1)
			x1 = x - getWidth(st);
		else
			x1 = x - (getWidth(st) >> 1);
		for (int i = 0; i < len; i++) {
			pos = strFont.indexOf(st.charAt(i));
			if (pos == -1)
				pos = 0;
			if (pos > -1) {
				g.drawRegion(imgFont, fImages[pos][0], fImages[pos][1], fImages[pos][2], fImages[pos][3], 0, x1, y, 20);
			}
			x1 += fImages[pos][2] + space;
		}
	}

	public mVector splitFontVector(String src, int lineWidth) {
		mVector lines = new mVector();
		String line = "";
		for (int i = 0; i < src.length(); i++) {
			if (src.charAt(i) == '\n'|| src.charAt(i)=='\b') {
				lines.addElement(line);
				line = "";
			} else {
				line += src.charAt(i);
				if (getWidth(line) > lineWidth) {
					// System.out.println(line);
					int j = 0;
					for (j = line.length() - 1; j >= 0; j--) {
						if (line.charAt(j) == ' ') {
							break;
						}
					}
					if (j < 0)
						j = line.length() - 1;
					lines.addElement(line.substring(0, j));
					i = i - (line.length() - j) + 1;
					line = "";
				}
				if (i == src.length() - 1 && !line.trim().equals(""))
					lines.addElement(line);
			}
		}
		return lines;
	}

	public String splitFirst(String str) {
		String line = "";
		boolean isSplit = false;
		for (int i = 0; i < str.length(); i++) {
			if (!isSplit) {
				String strEnd = str.substring(i, str.length());
				if (compare(strEnd, " "))
					line += str.charAt(i) + "-";
				else
					line += strEnd;
				isSplit = true;
			} else {
				if (str.charAt(i) == ' ')
					isSplit = false;
			}
		}
		return line;
	}

	public String[] splitFontArray(String src, int lineWidth) {
		mVector lines = splitFontVector(src, lineWidth);
		String[] arr = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			arr[i] = lines.elementAt(i).toString();
		}
		return arr;
	}

	public boolean compare(String strSource, String str) {
		for (int i = 0; i < strSource.length(); i++) {
			if (String.valueOf(strSource.charAt(i)).equals(str))
				return true;
		}
		return false;
	}
	public void drawStringBorder(mGraphics g, String st, int xx, int yy, int align) {
		if(fontSys!=null)
		{
			//float[] hsv = new float[3];
			//Color.colorToHSV(fontSys.color, hsv);
			//hsv[2] *= 0.4f;
			int colorDark = 0xff1C1C1C;//Color.(hsv);
			fontSys.p.setColor(colorDark);
			
			int x = xx*mGraphics.zoomLevel;
			int y = yy*mGraphics.zoomLevel;
			fontSys.drawStringBoder(g, st, x-1, y-1, align);
			fontSys.drawStringBoder(g, st, x, y-1, align);
			fontSys.drawStringBoder(g, st, x+1, y-1, align);
			fontSys.drawStringBoder(g, st, x+1, y, align);
			fontSys.drawStringBoder(g, st, x+1, y+1, align);
			fontSys.drawStringBoder(g, st, x, y+1, align);
			fontSys.drawStringBoder(g, st, x-1, y+1, align);
			fontSys.drawStringBoder(g, st, x-1, y, align);
			
			//fontSys.drawString(g, st, x, y-1, align);
			//fontSys.drawStringBoder(g, st, x, y, align);
			
			fontSys.p.setColor(fontSys.color);
			fontSys.drawString(g, st, xx, yy, align);
		}
		else drawString( g,  st,  xx,  yy,  align);
	}
	public void drawStringShadown(mGraphics g, String st, int xx, int yy, int align) {
		if(fontSys!=null)
		{
			//float[] hsv = new float[3];
			//Color.colorToHSV(fontSys.color, hsv);
			//hsv[2] *= 0.4f;
			int colorDark = 0xff1C1C1C;//Color.(hsv);
			fontSys.p.setColor(colorDark);
			
			int x = xx*mGraphics.zoomLevel;
			int y = yy*mGraphics.zoomLevel;
			fontSys.drawStringBoder(g, st, x+1, y+1, align);
			fontSys.drawStringBoder(g, st, x, y+1, align);
			
			//fontSys.drawString(g, st, x, y-1, align);
			//fontSys.drawStringBoder(g, st, x, y, align);
			
			fontSys.p.setColor(fontSys.color);
			fontSys.drawString(g, st, xx, yy, align);
		}
		else drawString( g,  st,  xx,  yy,  align);
	}
	public void drawStringStroke(mGraphics g, String st, int x, int y, int align,int color,boolean isUClip) {
		if(fontSys!=null)
		{
			fontSys.drawStringStroke(g, st, x, y, align);
		}
		else drawString( g,  st,  x,  y,  align);
	}
	public void drawStringBorder(mGraphics g, String st, int x, int y, int align,int color) {
		if(fontSys!=null)
		{
			float[] hsv = new float[3];
			Color.colorToHSV(fontSys.color, hsv);
			hsv[2] *= 0.4f;
			int colorDark = color;//Color.(hsv);
			fontSys.p.setColor(colorDark);
			
//			fontSys.drawString(g, st, x-1, y-1, align);
//			fontSys.drawString(g, st, x-1, y+1, align);
//			fontSys.drawString(g, st, x+1, y-1, align);
//			fontSys.drawString(g, st, x+1, y+1, align);
			
//			fontSys.drawString(g, st, x, y-1, align);
			fontSys.drawString(g, st, x+1*mGraphics.zoomLevel, y+1*mGraphics.zoomLevel, align);
			fontSys.drawString(g, st, x+1*mGraphics.zoomLevel, y, align);
//			fontSys.drawString(g, st, x-1, y, align);
			
			fontSys.p.setColor(fontSys.color);
			fontSys.drawString(g, st, x, y, align);
		}
		else drawString( g,  st,  x,  y,  align);
		
	}
	public void drawStringBorder(mGraphics g, String st, int x, int y, int align,int color,boolean isUClip) {
		
		if(fontSys!=null)
		{
			float[] hsv = new float[3];
			Color.colorToHSV(fontSys.color, hsv);
			hsv[2] *= 0.4f;
			int colorDark = color;//Color.(hsv);
			int colorOld = fontSys.color;
			fontSys.p.setColor(colorDark);
			//sdfsdf
//			fontSys.drawString(g, st, x-1, y-1, align);
//			fontSys.drawString(g, st, x-1, y+1, align);
//			fontSys.drawString(g, st, x+1, y-1, align);
//			fontSys.drawString(g, st, x+1, y+1, align);
			
//			fontSys.drawString(g, st, x, y-1, align);
			fontSys.drawString(g, st, x+1*mGraphics.zoomLevel, y+1*mGraphics.zoomLevel, align);
			fontSys.drawString(g, st, x+1*mGraphics.zoomLevel, y, align);
//			fontSys.drawString(g, st, x-1, y, align);
			
			fontSys.p.setColor(colorOld);
			fontSys.drawString(g, st, x, y, align);
		}
		else drawString( g,  st,  x,  y,  align);
		
	}
	public void drawStringBorder(mGraphics g, String st, int x, int y, int align, mFont font2) {
		drawStringBorder( g,  st,  x,  y,  align);		
	}
	public static String[] split(String original, String separator) {
		mVector nodes = new mVector();
		int index = original.indexOf(separator);
		while (index >= 0) {
			nodes.addElement(original.substring(0, index));
			original = original.substring(index + separator.length());
			index = original.indexOf(separator);
		}
		nodes.addElement(original);
		String[] result = new String[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = (String) nodes.elementAt(loop);

			}
		}
		return result;
	}
}
