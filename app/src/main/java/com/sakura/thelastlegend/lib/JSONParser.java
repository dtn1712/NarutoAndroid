package com.sakura.thelastlegend.lib;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.res.AssetManager;

import com.sakura.thelastlegend.GameMidlet;

import Objectgame.ItemPayment;



public class JSONParser {

	public static JSONObject jObj = null;
	static String json = "";
	
	public static String textDemo = "{numbers:[{class:Objectgame.PhoneNumber,number:3,name:Home},{class:Objectgame.PhoneNumber,number:4,name:Work}],name:Nate,age:31}";

	//Hàm xây dựng khởi tạo đối tượng
	public JSONParser() {

	}
	public static ArrayList<ItemPayment> parseItemPayment(String text)
	{
		ArrayList<ItemPayment> list = new ArrayList<ItemPayment>();
		try {
			if(text.trim().length()>0)
				jObj = new JSONObject(text);
			else {
				json = mSystem.loadFile("file.json");
				Cout.println2222("load json2222 ==  "+json);
				jObj = new JSONObject(json);
			}
			JSONArray component = jObj.getJSONArray("data");
			//array objects in json if you would have more components
			int id = 0;
			for (int i = 0; i < component.length(); i++) {
				JSONObject component1 = component.getJSONObject(i);
				ItemPayment item = new ItemPayment(id,component1.getInt(ItemPayment.KEY_VALUE_MONEY_AMOUNT),
						component1.getString(ItemPayment.KEY_NAME_MONEY_AMOUNT),
						component1.getInt(ItemPayment.KEY_VALUE_MONEY_REAL),
						component1.getString(ItemPayment.KEY_NAME_MONEY_REAL));
				list.add(item);
				id++;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		return list;
//		
	}
	public static ArrayList<ItemPayment> parseItemPayment()
	{
		ArrayList<ItemPayment> list = new ArrayList<ItemPayment>();
		try {
			json = mSystem.loadFile("file.json");
			Cout.println2222("load json2222 ==  "+json);
			jObj = new JSONObject(json);
			JSONArray component = jObj.getJSONArray("data");
			//array objects in json if you would have more components
			int id = 0;
			for (int i = 0; i < component.length(); i++) {
				JSONObject component1 = component.getJSONObject(i);
				ItemPayment item = new ItemPayment(id,component1.getInt(ItemPayment.KEY_VALUE_MONEY_AMOUNT),
						component1.getString(ItemPayment.KEY_NAME_MONEY_AMOUNT),
						component1.getInt(ItemPayment.KEY_VALUE_MONEY_REAL),
						component1.getString(ItemPayment.KEY_NAME_MONEY_REAL));
				list.add(item);
				id++;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		return list;
//		
	}
}
