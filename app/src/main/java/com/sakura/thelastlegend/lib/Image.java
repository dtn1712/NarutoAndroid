package com.sakura.thelastlegend.lib;

import java.io.IOException;
import java.io.InputStream;

import com.sakura.thelastlegend.domain.model.FilePack;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.mGraphics;



public class Image {
	public static FilePack filePack = null;

	public static mHashtable listImgLoad = new mHashtable();
	
	public static mBitmap createImage(String path) {
	     
		mBitmap img = null;
	     
		img = (mBitmap)listImgLoad.get(path);

		if(img!=null) return img;
		AssetManager assetManager = GameMidlet.asset;
		try {
			InputStream inputStream = assetManager.open(path);
			  BitmapFactory.Options opt = new BitmapFactory.Options(); 
			  opt.inPreferredConfig = Bitmap.Config.ARGB_8888; 
			  opt.inPurgeable = true; 
			  opt.inInputShareable = true; 
			  //Access to resources 
			  Bitmap example = BitmapFactory.decodeStream(inputStream,null,opt);
			  if(example!=null){
				  inputStream.close();
				  inputStream = null;
				  img = new mBitmap();
				  img.image = example;
				  if(img.image!=null)
				  {
					  listImgLoad.put(path, img);
				      img.key = path;
					  img.width = img.image.getWidth()/*/mGraphics.zoomLevel*/;
					  img.height = img.image.getHeight()/*/mGraphics.zoomLevel*/;
				  }
				  return img;
			  }
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		if(img==null){
			 byte[] data = Rms.loadRMS(mSystem.getPathRMS(path));
			  if(data!=null){
				  try {
					  img = createImage(data, 0, data.length);
					  listImgLoad.put(path, img);
					  data = null;
					  return img;
				} catch (Exception e) {
					// TODO: handle exception
				}
				  
			  }else {
//					Cout.println("loadRMS data "+path);
			  }
		}
		return img;
	}
	
	public static String loiloadanh = "";
	public static mBitmap createImage2(String path) throws IOException {
		
		return null;
		
	}
	public static mBitmap createImage(byte[] data, int w, int h) {
		if(data==null) return null;
		mBitmap img = new mBitmap();
		  BitmapFactory.Options opt = new BitmapFactory.Options(); 
		  opt.inPreferredConfig = Bitmap.Config.ARGB_8888; 
		  opt.inPurgeable = true; 
		  opt.inInputShareable = true; 
		  //Access to resources 
		  img.image = BitmapFactory.decodeByteArray(data, 0, data.length,opt);
		  if(img.image!=null){
			  img.width=img.image.getWidth()/*/mGraphics.zoomLevel*/;
			  img.height=img.image.getHeight()/*/mGraphics.zoomLevel*/;
		  }
		  data = null;
//		if(mGraphics.zoomLevel==3)
//			try {
//				img.image = getResizedBitmap(img.image,3*mGraphics.getRealImageWidth(img)/2,
//						  3*mGraphics.getRealImageHeight(img)/2);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		
		return img;
	}
	public static mBitmap createImage(int width, int height) {
		
		mBitmap bmp = null;
		return bmp;
	}

	public static mBitmap loadImageFromAsset(String strPath)
    {
		return createImage(strPath);
    }
	
	public static mBitmap loadImagePack(String filepackName) {
		mBitmap bmp = null;
		filePack = new FilePack(filepackName);
		bmp = filePack.loadImage(filepackName + ".png");
		filePack = null;
		return bmp;
	}
	public static mBitmap loadImagePack(String filepackName, String fileName) {
		mBitmap bmp = null;
		filePack = new FilePack(filepackName);
		bmp = filePack.loadImage(fileName);
		filePack = null;
		return bmp;
	}

	
	public static String convert(String str) {
		String strimg = str;
		if (str.charAt(0) == '/')
			strimg = strimg.substring(1);
		strimg = strimg.replaceAll("/", "_");
		if (strimg.indexOf('.') > -1)
			strimg = strimg.substring(0, strimg.indexOf('.'));

		return strimg.toLowerCase();
	}

	public static mBitmap getResizedBitmap(mBitmap bm) {
		if (bm == null)
			return null;

		return null;
//		int width, height;
//		float scaleWidth, scaleHeight;
//		
//		width = bm.getWidth();
//		height = bm.getHeight();
//
//		scaleWidth = (float) GameCanvas.rw / (float) width;
//		scaleHeight = (float) GameCanvas.rh / (float) height;
//
//		if (scaleWidth < scaleHeight)
//			scaleWidth = scaleHeight;
//		else
//			scaleHeight = scaleWidth;
//		
//		// CREATE A MATRIX FOR THE MANIPULATION
//		Matrix matrix = new Matrix();
//		
//		// RESIZE THE BIT MAP
//		matrix.postScale(scaleWidth, scaleHeight);
//		
//		// RECREATE THE NEW BITMAP
//		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,matrix, false);
//		
//		return resizedBitmap;
	}
	

	public static int getDrawableID(String filename) {
		return 0;
	}
	
	public static int getWidth(mBitmap image) {
		if(image==null) return 1;
		return image.getWidth() /*/ GameCanvas.zoomLevel*/;
	}

	public static int getHeight(mBitmap image) {
		if(image==null) return 1;
		return image.getHeight() /*/ GameCanvas.zoomLevel*/;
	}
}
