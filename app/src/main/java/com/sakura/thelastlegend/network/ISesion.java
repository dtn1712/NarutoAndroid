package com.sakura.thelastlegend.network;

public interface ISesion {
	
	  public abstract boolean isConnected();

	  public abstract void setHandler(IMessageHandler messageHandler);

	  public abstract void connect(String host);

	  public abstract void sendMessage(Message message);

	  public abstract void close();

}
