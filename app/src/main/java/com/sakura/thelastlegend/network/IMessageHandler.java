package com.sakura.thelastlegend.network;

public interface IMessageHandler {
	
	 public void onMessage(Message message);

	    public abstract void onConnectionFail();

	    public abstract void onDisconnected();

	    public abstract void onConnectOK();

}
