/* 
 * DrawRegion class
 * Author : Giang.LC
 * create: 08-03-2011
 * 
 * How to use :
 * in class need DrawRegion, import DrawRegion class and insert code:
 * "private DrawRegion  g = new DrawRegion(canvas);"
 * when use it just right like j2me, delete anchor parameter:
 * g.drawRegion( Bitmap img, int x_src, int y_src, int width, int height, int x_dest, int y_dest);
 * or
 * g.drawRegion( Bitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest);
 * 
 * g.drawChar(char character, int x, int y);
 * 
 * g.drawString(String str, int x, int y);
 */

package com.sakura.thelastlegend;

import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mBitmap;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;

public class mGraphics {

	public static final int TRANS_NONE = 0;
	public static final int TRANS_ROT90 = 5;
	public static final int TRANS_ROT180 = 3;
	public static final int TRANS_ROT270 = 6;

	public static final int TRANS_MIRROR = 2;
	public static final int TRANS_MIRROR_ROT90 = 7;
	public static final int TRANS_MIRROR_ROT180 = 1;
	public static final int TRANS_MIRROR_ROT270 = 4;

	// for graphics anchor
	public static int TOP = 16;
	public static int BOTTOM = 32;
	public static int VCENTER = 2;

	public static int LEFT = 4;
	public static int RIGHT = 8;
	public static int HCENTER = 1;

	public static int BASELINE = 64;
	public static int DOTTED = 1;
	public static int SOLID = 0;

	public static Canvas canvas;
	public int translateX = 0, translateY = 0;
	public static int zoomLevel = 1;
	public Paint paint;
	
	// public Bitmap bitmap;
	// public Canvas myCanvas;

	public mGraphics() {
		paint = new Paint();
		

	}

	public void setCanvas(Canvas cv) {
		canvas = cv;
	}

	// draw without transform
	public void drawRegion(mBitmap img, int x_src, int y_src, int width, int height, int x_dest, int y_dest) {
		// create bitmap in area draw
		x_src *= zoomLevel;
		y_src *= zoomLevel;
		width *= zoomLevel;
		height *= zoomLevel;
		x_dest *= zoomLevel;
		y_dest *= zoomLevel;
		mBitmap image = mBitmap.createBitmap(img, x_src, y_src, width, height); 

		if(image.image!=null)
		canvas.drawBitmap(image.image, x_dest, y_dest, null);
	}
	public void _drawRegionScale(mBitmap img, int x_src, int y_src, int width,
			int height, int transform, int x_dest, int y_dest, int Anchor,int tileScale) {
		
		if (img == null)
			return;
		canvas.save();
		width =(width*tileScale)/100;
		height =(height*tileScale)/100;
		canvas.save();
		int wt = width, ht = height;
		switch (transform) {
		case TRANS_ROT90: // 90
		case TRANS_ROT270: // 270
		case TRANS_MIRROR_ROT90: // M90 j2me<-->android 270
		case TRANS_MIRROR_ROT270: // M270 j2me<-->android 90
			wt = height;
			ht = width;
			break;
		}
		int ixA = 0, iyA = 0;

		switch (Anchor) {
		// Graphics.TOP | Graphics.LEFT
		case 20:
			ixA = 0;
			iyA = 0;
			break;
		// Graphics.TOP | Graphics.HCENTER
		case 17:
			ixA = wt / 2;
			iyA = 0;
			break;
		// Graphics.TOP | Graphics.RIGHT
		case 24:
			ixA = wt;
			iyA = 0;
			break;
		// Graphics.VCENTER | Graphics.LEFT
		case 6:
			ixA = 0;
			iyA = ht / 2;
			break;
		// Graphics.VCENTER | Graphics.HCENTER
		case 3:
			ixA = wt / 2;
			iyA = ht/ 2;
			break;
		// Graphics.VCENTER | Graphics.RIGHT
		case 10:
			ixA = wt;
			iyA = ht / 2;
			break;
		// Graphics.BOTTOM | Graphics.LEFT
		case 36:
			ixA = 0;
			iyA = ht;
			break;
		// Graphics.BOTTOM | Graphics.HCENTER
		case 33:
			ixA = wt / 2;
			iyA = ht;
			break;
		// Graphics.BOTTOM | Graphics.RIGHT
		case 40:
			ixA = wt;
			iyA = ht;
			break;
		}

		x_dest -= ixA;
		y_dest -= iyA;
		int ix = 0, iy = 0;

		switch (transform) {
		case TRANS_NONE: // 0
			break;
		case TRANS_ROT90: // 90
			canvas.rotate(90, x_dest, y_dest);
			 iy = height;
//			iy = width;
			break;
		case TRANS_ROT180: // 180
			canvas.rotate(180, x_dest, y_dest);
			iy = height;
			ix = width;
			break;
		case TRANS_ROT270: // 270
			canvas.rotate(270, x_dest, y_dest);
			ix = width;
			break;
		case TRANS_MIRROR: // M
			canvas.scale(-1, 1, x_dest, y_dest);// ??
			ix = width;
			break;
		case TRANS_MIRROR_ROT90: // M90 j2me<-->android 270
			canvas.scale(-1, 1, x_dest, y_dest);// ??
			canvas.rotate(270, x_dest, y_dest);
			ix = width;
			iy = height;
			break;
		case TRANS_MIRROR_ROT180: // M180
			canvas.scale(-1, 1, x_dest, y_dest);// ??
			canvas.rotate(180, x_dest, y_dest);
			iy = height;
			break;
		case TRANS_MIRROR_ROT270: // M270 j2me<-->android 90
			canvas.scale(-1, 1, x_dest, y_dest);// ??90
			canvas.rotate(90, x_dest, y_dest);

			break;
		}

//		canvas.clipRect(x_dest - ix, y_dest - iy, x_dest - ix + width, y_dest
//				- iy + height);
//		canvas.drawBitmap(img.image, x_dest - ix - x_src, y_dest - iy - y_src, null);
//		canvas.restore();
		
		canvas.clipRect(x_dest - ix, y_dest - iy, x_dest - ix + width, y_dest
				- iy + height);
	    float startX = x_dest - ix - x_src; //the left
	    float startY =  y_dest - iy - y_src; //and top corner (place it wherever you want)
	    float endX = startX + width; //right
	    float endY = startY + height; //and bottom corner

//		Cout.println(x_dest+" drawRegionScalse  "+ix+"   x  "+(x_src )+" startX "+startX);
		canvas.drawBitmap(img.image, null, new RectF(x_dest,y_dest, endX, endY), null);
		canvas.restore();
	}
	public void _drawRegionRotate(Bitmap img, int x_src, int y_src, int width,
			int height, int transform, int x_dest, int y_dest, int Anchor,int rotate) {
		if (img == null)
			return;

		canvas.save();
		int ix = 0, iy = 0;
//
		switch (transform) {
		
		}

		x_dest -=width/2;
		y_dest -=height/2;
		switch (Anchor) {
//		case leftVCenter:
//			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
//			break;
		// Graphics.TOP | Graphics.LEFT
		case 20:
			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
		
			break;
		// Graphics.TOP | Graphics.HCENTER
		case 17:
			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
		
			break;
		// Graphics.TOP | Graphics.RIGHT
		case 24:
			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
			
			break;
		// Graphics.VCENTER | Graphics.LEFT
		case 6:
//
//			y_dest -=height/2;
			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
			x_dest+=width/2;
			break;
		// Graphics.VCENTER | Graphics.HCENTER
		case 3:
			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
			
			break;
		// Graphics.VCENTER | Graphics.RIGHT
		case 10:
			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
			
			break;
		// Graphics.BOTTOM | Graphics.LEFT
		case 36:
			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
			break;
		// Graphics.BOTTOM | Graphics.HCENTER
		case 33:
			canvas.rotate(rotate, x_dest+width/2, y_dest+height/2); //goc vs vi tri xoay hinh.//xoay ngay giua
			break;
		// Graphics.BOTTOM | Graphics.RIGHT
		case 40:
			break;
		}
		if(transform==TRANS_MIRROR_ROT180){
			canvas.scale(-1, 1, x_dest, y_dest);// ??
			canvas.rotate(180, x_dest, y_dest);
			iy = height;
		}
		canvas.clipRect(x_dest - ix, y_dest - iy, x_dest - ix + width, y_dest
				- iy + height);
		canvas.drawBitmap(img, x_dest - ix - x_src, y_dest - iy - y_src, null);
//
		canvas.restore();
	}
	// draw with transform
	public void drawRegion(mBitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest, int Anchor) {
		if(img==null||img.image==null) return;
		x_src *= zoomLevel;
		y_src *= zoomLevel;
		width *= zoomLevel;
		height *= zoomLevel;
		x_dest *= zoomLevel;
		y_dest *= zoomLevel;
		drawRegion(img.image, x_src, y_src, width, height, transform, x_dest, y_dest, Anchor);
	}
	// draw with transform
		public void drawRegionRotate(mBitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest, int Anchor,int rotate) {
			if(img==null||img.image==null) return;
			x_src *= zoomLevel;
			y_src *= zoomLevel;
			width *= zoomLevel;
			height *= zoomLevel;
			x_dest *= zoomLevel;
			y_dest *= zoomLevel;
			_drawRegionRotate(img.image, x_src, y_src, width, height, transform, x_dest, y_dest, Anchor,rotate);
		}
	// draw with transform
		public void drawRegion(mBitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest, int Anchor, boolean isUclip) {
			if(img==null||img.image==null) return;
			x_src *= zoomLevel;
			y_src *= zoomLevel;
			width *= zoomLevel;
			height *= zoomLevel;
			x_dest *= zoomLevel;
			y_dest *= zoomLevel;
			drawRegion(img.image, x_src, y_src, width, height, transform, x_dest, y_dest, Anchor);
		}
	public void drawRegion(Bitmap img, int x_src, int y_src, int width, int height, int transform, int x_dest, int y_dest, int Anchor) {
		if (img == null)
			return;

		canvas.save();
		int wt = width, ht = height;
		switch (transform) {
		case TRANS_ROT90: // 90
		case TRANS_ROT270: // 270
		case TRANS_MIRROR_ROT90: // M90 j2me<-->android 270
		case TRANS_MIRROR_ROT270: // M270 j2me<-->android 90
			wt = height;
			ht = width;
			break;
		}
		int ixA = 0, iyA = 0;

		switch (Anchor) {
		// Graphics.TOP | Graphics.LEFT
		case 20:
			ixA = 0;
			iyA = 0;
			break;
		// Graphics.TOP | Graphics.HCENTER
		case 17:
			ixA = wt / 2;
			iyA = 0;
			break;
		// Graphics.TOP | Graphics.RIGHT
		case 24:
			ixA = wt;
			iyA = 0;
			break;
		// Graphics.VCENTER | Graphics.LEFT
		case 6:
			ixA = 0;
			iyA = ht / 2;
			break;
		// Graphics.VCENTER | Graphics.HCENTER
		case 3:
			ixA = wt / 2;
			iyA = ht/ 2;
			break;
		// Graphics.VCENTER | Graphics.RIGHT
		case 10:
			ixA = wt;
			iyA = ht / 2;
			break;
		// Graphics.BOTTOM | Graphics.LEFT
		case 36:
			ixA = 0;
			iyA = ht;
			break;
		// Graphics.BOTTOM | Graphics.HCENTER
		case 33:
			ixA = wt / 2;
			iyA = ht;
			break;
		// Graphics.BOTTOM | Graphics.RIGHT
		case 40:
			ixA = wt;
			iyA = ht;
			break;
		}

		x_dest -= ixA;
		y_dest -= iyA;
		int ix = 0, iy = 0;

		switch (transform) {
		case TRANS_NONE: // 0
			break;
		case TRANS_ROT90: // 90
			canvas.rotate(90, x_dest, y_dest);
			 iy = height;
//			iy = width;
			break;
		case TRANS_ROT180: // 180
			canvas.rotate(180, x_dest, y_dest);
			iy = height;
			ix = width;
			break;
		case TRANS_ROT270: // 270
			canvas.rotate(270, x_dest, y_dest);
			ix = width;
			break;
		case TRANS_MIRROR: // M
			canvas.scale(-1, 1, x_dest, y_dest);// ??
			ix = width;
			break;
		case TRANS_MIRROR_ROT90: // M90 j2me<-->android 270
			canvas.scale(-1, 1, x_dest, y_dest);// ??
			canvas.rotate(270, x_dest, y_dest);
			ix = width;
			iy = height;
			break;
		case TRANS_MIRROR_ROT180: // M180
			canvas.scale(-1, 1, x_dest, y_dest);// ??
			canvas.rotate(180, x_dest, y_dest);
			iy = height;
			break;
		case TRANS_MIRROR_ROT270: // M270 j2me<-->android 90
			canvas.scale(-1, 1, x_dest, y_dest);// ??90
			canvas.rotate(90, x_dest, y_dest);

			break;
		}
		canvas.clipRect(x_dest - ix, y_dest - iy, x_dest - ix + width, y_dest
				- iy + height);
		canvas.drawBitmap(img, x_dest - ix - x_src, y_dest - iy - y_src, null);
		canvas.restore();
	}
	public void drawImage(Bitmap image, int x, int y) {
		// TODO Auto-generated method stub
		x *= zoomLevel;
		y *= zoomLevel;

		if(image!=null)
		canvas.drawBitmap(image, x, y, null);
	}

	// draw image with anchor
	public void drawImage(mBitmap image, int x, int y, int anchor) {
		// TODO Auto-generated method stub
		
		x *= zoomLevel;
		y *= zoomLevel;
		
		int ixA = 0, iyA = 0, width = Image.getWidth(image), height = Image.getHeight(image);
		width *= zoomLevel;
		height *= zoomLevel;
		switch (anchor) {
		// Graphics.TOP | Graphics.LEFT
		case 20:
			ixA = 0;
			iyA = 0;
			break;
		// Graphics.TOP | Graphics.HCENTER
		case 17:
			ixA = width / 2;
			iyA = 0;
			break;
		// Graphics.TOP | Graphics.RIGHT
		case 24:
			ixA = width;
			iyA = 0;
			break;
		// Graphics.VCENTER | Graphics.LEFT
		case 6:
			ixA = 0;
			iyA = height / 2;
			break;
		// Graphics.VCENTER | Graphics.HCENTER
		case 3:
			ixA = width / 2;
			iyA = height / 2;
			break;
		// Graphics.VCENTER | Graphics.RIGHT
		case 10:
			ixA = width;
			iyA = height / 2;
			break;
		// Graphics.BOTTOM | Graphics.LEFT
		case 36:
			ixA = 0;
			iyA = height;
			break;
		// Graphics.BOTTOM | Graphics.HCENTER
		case 33:
			ixA = width / 2;
			iyA = height;
			break;
		// Graphics.BOTTOM | Graphics.RIGHT
		case 40:
			ixA = width;
			iyA = height;
			break;
		}
		if(image!=null&&image.image!=null){
				canvas.drawBitmap(image.image, x - ixA, y - iyA, null);
		}

	}
	public void drawImageScale(mBitmap image, int x, int y, int anchor,int per) {
		// TODO Auto-generated method stub
		drawRegionScalse(image, 0, 0, image.getWidth(), image.getHeight(), 0, x, y, anchor,false, per);

	}
	// for draw char, string. change anchor by canvas
	public void drawChar(char character, int x, int y) {
		char[] tempChar = { '0' };
		tempChar[0] = character;
		x *= zoomLevel;
		y *= zoomLevel;
		canvas.drawText(tempChar, 0, 1, x, y, paint);
	}

	public void drawChar(char[] character, int offset, int length, int x, int y) {
		x *= zoomLevel;
		y *= zoomLevel;
		canvas.drawText(character, offset, length, x, y, paint);
	}

	public void drawString(String str, int x, int y) {
		x *= zoomLevel;
		y *= zoomLevel;
		canvas.drawText(str, x, y, paint);
	}

	public void drawString(String str, int x, int y, int Anchor, Paint p) {

		str.trim();
		x *= zoomLevel;
		y *= zoomLevel;
		
		if (str.endsWith("\n"))
			str = str.substring(0, str.length() - 1);

		switch (Anchor) {
		case 0:
			p.setTextAlign(Align.LEFT);
			break;
		case 1:
			p.setTextAlign(Align.RIGHT);
			break;
		case 2:
			p.setTextAlign(Align.CENTER);
			break;
		default:
			p.setTextAlign(Align.LEFT);
			break;
		}
		p.setAntiAlias(true);
		canvas.drawText(str, x, y, p);

	}

	public void drawString(String str, int x, int y, int Anchor) {

		str.trim();
		x *= zoomLevel;
		y *= zoomLevel;
		switch (Anchor) {
		case 0:
			paint.setTextAlign(Align.LEFT);
			break;
		case 1:
			paint.setTextAlign(Align.RIGHT);
			break;
		case 2:
			paint.setTextAlign(Align.CENTER);
			break;
		default:
			paint.setTextAlign(Align.LEFT);
			break;
		}

		paint.setStyle(Paint.Style.FILL);
		paint.setAntiAlias(true);

		canvas.drawText(str, x, y, paint);

		
	}

	public void fillRect(int xLeft, int yTop, int width, int height) {
		
		xLeft *= zoomLevel;
		yTop *= zoomLevel;
		width *= zoomLevel;
		height *= zoomLevel;
		int right = xLeft + width;
		int bottom = yTop + height;

		paint.setStyle(Paint.Style.FILL);
		canvas.drawRect(xLeft, yTop, right, bottom, paint);
	}

	public void fillTransparent(int xLeft, int yTop, int width, int height,	int alpha) {
		xLeft *= zoomLevel;
		yTop *= zoomLevel;
		width *= zoomLevel;
		height *= zoomLevel;
		
		int right = xLeft + width;
		int bottom = yTop + height;
		paint.setColor(Color.BLACK);
		paint.setAlpha(alpha);
		paint.setStyle(Paint.Style.FILL);
		canvas.drawRect(xLeft, yTop, right, bottom, paint);
		paint.setAlpha(255);
	}

	public void setPaintSize(float a) {
		a *= zoomLevel;
		paint.setStrokeWidth(a);
	}

	public void drawRect(int xLeft, int yTop, int width, int height) {
		xLeft *= zoomLevel;
		yTop *= zoomLevel;
		width *= zoomLevel;
		height *= zoomLevel;
		
		int right = xLeft + width;
		int bottom = yTop + height;
		paint.setStrokeWidth((float)mGraphics.zoomLevel);
		paint.setStyle(Paint.Style.STROKE);
		canvas.drawRect(xLeft, yTop, right, bottom, paint);

	}

	public void setColor(int color) {
		if (color == 0) {
			color = 0xff000000;
		} else if (color == 1) {
			color = 0xffffffff;
		}

		paint.setColor(0xff000000 | color);
	}
	public void setColor(int color,int opacity) {
		paint.setColor(color);
		paint.setAlpha(opacity*255/100);
	}
	public void setAlpha(int a) {
		paint.setAlpha(a);
	}

	public void setColor(int r, int g, int b) {
		paint.setARGB(0, r, g, b);
	}

	public void drawLine(int startX, int startY, int stopX, int stopY) {
		startX *= zoomLevel;
		startY *= zoomLevel;
		stopX *= zoomLevel;
		stopY *= zoomLevel;

	    paint.setStyle(Paint.Style.STROKE);
	    paint.setStrokeJoin(Paint.Join.ROUND);
	    paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStrokeWidth((float)mGraphics.zoomLevel);
	    paint.setAntiAlias(true);
		drawLine(canvas, paint, new PointF(startX, startY),  new PointF(stopX, stopY));
//		canvas.drawLine(startX, startY, stopX, stopY, paint);
	}
	public void drawLine(int startX, int startY, int stopX, int stopY,boolean isUclip) {
		startX *= zoomLevel;
		startY *= zoomLevel;
		stopX *= zoomLevel;
		stopY *= zoomLevel;

	    paint.setStyle(Paint.Style.STROKE);
	    paint.setStrokeJoin(Paint.Join.ROUND);
	    paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStrokeWidth((float)mGraphics.zoomLevel);
	    paint.setAntiAlias(true);
		drawLine(canvas, paint, new PointF(startX, startY),  new PointF(stopX, stopY));
//		canvas.drawLine(startX, startY, stopX, stopY, paint);
	}
	private void drawLine(Canvas canvas,  Paint paint, PointF start, PointF end)
	{
	    PointF mid2 = midPoint(end, start);
	    Path path = new Path();
	    path.reset();
	    paint.setStrokeWidth((float)mGraphics.zoomLevel);
	    path.moveTo(start.x, start.y);
	    path.quadTo(start.x, start.y, mid2.x, mid2.y);
	    canvas.drawPath(path, paint);
	    

	    Path path2 = new Path();
	    path2.reset();
	    paint.setStrokeWidth((float)mGraphics.zoomLevel);
	    path2.moveTo(mid2.x, mid2.y);
	    path2.quadTo(mid2.x, mid2.y, end.x, end.y);
	    canvas.drawPath(path2, paint);
	    //canvas.
	    //paintSize -= 1;
	}

	private PointF  midPoint(PointF p1, PointF p2)
	{
	    return new PointF((p1.x + p2.x) / 2.0f ,  (p1.y + p2.y) * 0.5f);
	}
	public void drawArc(int x, int y, int Width, int Height, int startAngle, int sweepAngle) {
		x *= zoomLevel;
		y *= zoomLevel;
		Width *= zoomLevel;
		Height *= zoomLevel;
		paint.setStyle(Paint.Style.FILL);
		RectF oval = new RectF(x, y, x + Width, y + Height);
		canvas.drawArc(oval, startAngle, sweepAngle, false, paint);
	}

	public void fillArc(int x, int y, int Width, int Height, int startAngle, int sweepAngle) {
		
		x *= zoomLevel;
		y *= zoomLevel;
		Width *= zoomLevel;
		Height *= zoomLevel;
		paint.setAntiAlias(true);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		RectF oval = new RectF(x, y, x + Width, y + Height);

		canvas.drawArc(oval, startAngle, sweepAngle, true, paint);

	}

	public void translate(int x, int y) {
		x *= zoomLevel;
		y *= zoomLevel;
		canvas.translate((float) x, (float) y);
		translateX += x;
		translateY += y;

	}

	public int getTranslateX() {
		return translateX / zoomLevel;
	}

	public int getTranslateY() {
		return translateY / zoomLevel;
	}

	public static class gFont {
		public static int NORMAL = 0;
		public static int BOLD_ITALIC = 1;
		public static int ITALIC = 2;
		public static int BOLD = 3;

		public static int STYLE_PLAIN = 0;
		public static int STYLE_BOLD = 3;
		public static float SIZE_MEDIUM = 10;

		public int myStyle;
		public float mySize;

		public gFont(int style, float size) {
			myStyle = style;
			mySize = size;
		}
	}

	public void setFont(gFont font) {
		paint.setTextSize(font.mySize);
	}

	public void setClip(int x, int y, int Width, int Height) {
		// TODO Auto-generated method stub
		x *= zoomLevel;
		y *= zoomLevel;
		Width *= zoomLevel;
		Height *= zoomLevel;
		canvas.clipRect(x, y, x + Width, y + Height, Op.REPLACE);
		// canvas.clipRect(x, y, x + With, y + Height);
	}

	public int getClipX() {
		Rect r = canvas.getClipBounds();
		if (r != null) {
			return r.left;
		}
		return 0;
	}

	public int getClipY() {
		Rect r = canvas.getClipBounds();
		if (r != null) {
			return r.top;
		}
		return 0;
	}

	public int getClipWidth() {
		Rect r = canvas.getClipBounds();
		if (r != null) {
			return r.width();
		}
		return 0;
	}

	public int getClipHeight() {
		Rect r = canvas.getClipBounds();
		if (r != null) {
			return r.height();
		}
		return 0;
	}



	
	public void drawRoundRect(int x, int y, int Width, int Height, int rX, int rY) {
		// TODO Auto-generated method stub
		x *= zoomLevel;
		y *= zoomLevel;
		Width *= zoomLevel;
		Height *= zoomLevel;
		paint.setStyle(Paint.Style.STROKE);
		RectF rect = new RectF(x, y, x + Width, y + Height);
		canvas.drawRoundRect(rect, rX, rY, paint);
	}

	public void fillRoundRect(int x, int y, int Width, int Height, int rX, int rY) {
		// TODO Auto-generated method stub
		x *= zoomLevel;
		y *= zoomLevel;
		Width *= zoomLevel;
		Height *= zoomLevel;
		paint.setAntiAlias(true);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStyle(Paint.Style.FILL);
		RectF rect = new RectF(x, y, x + Width, y + Height);
		canvas.drawRoundRect(rect, rX, rY, paint);
	}

	public void drawRGB(int[] rgbData, int offset, int scanlength, int x,int y, int width, int height, boolean processAlpha) {
		x *= zoomLevel;
		y *= zoomLevel;
		width *= zoomLevel;
		height *= zoomLevel;
		canvas.drawBitmap(rgbData, offset, width, x, y, width, height, processAlpha, null);

	}

	public void fillTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
		x1 *= zoomLevel;
		x2 *= zoomLevel;
		x3 *= zoomLevel;
		y1 *= zoomLevel;
		y2 *= zoomLevel;
		y3 *= zoomLevel;
		Path path = new Path();
		path.moveTo(x1 + translateX, y1 + translateY);
		path.lineTo(x2 + translateX, y2 + translateY);
		path.lineTo(x3 + translateX, y3 + translateY);
		path.close();

		paint.setStyle(Paint.Style.FILL);
		canvas.drawPath(path, paint);
	}

	public static int getImageHeight(mBitmap image) {
		// TODO Auto-generated method stub
		return Image.getHeight(image);
	}

	public static int getImageWidth(mBitmap image) {
		// TODO Auto-generated method stub
		return Image.getWidth(image);
	}

	public void drawImage(mBitmap bitmap, int i, int y, int j, boolean b) {
		// TODO Auto-generated method stub
		drawImage(bitmap, i, y, j);
	}

	public void disableBlending() {
		// TODO Auto-generated method stub
		
	}

	public void drawStringBoder(String str, int x, int y, int Anchor, Paint p) {
		// TODO Auto-generated method stub
		str.trim();
		
		if (str.endsWith("\n"))
			str = str.substring(0, str.length() - 1);

		switch (Anchor) {
		case 0:
			p.setTextAlign(Align.LEFT);
			break;
		case 1:
			p.setTextAlign(Align.RIGHT);
			break;
		case 2:
			p.setTextAlign(Align.CENTER);
			break;
		default:
			p.setTextAlign(Align.LEFT);
			break;
		}
		p.setAntiAlias(true);
		canvas.drawText(str, x, y, p);
	}

	public void drawStringStroke(String st, int x, int i, int align, Paint p) {
		// TODO Auto-generated method stub
		
	}

	public void fillRect(int i, int j, int gW, int gH, boolean uclip) {
		// TODO Auto-generated method stub
		fillRect(i, j, gW, gH);
	}

	public void drawRegion(mBitmap img, int x_src, int y_src, int width, int height,
			int transform, int x_dest, int y_dest, int anchor, Boolean isUclip,
			int opacity) {
		// TODO Auto-generated method stub
		x_src *= zoomLevel;
		y_src *= zoomLevel;
		width *= zoomLevel;
		height *= zoomLevel;
		x_dest *= zoomLevel;
		y_dest *= zoomLevel;

		paint.setAlpha(opacity*255/100);
		drawRegion(img.image, x_src, y_src, width, height, transform, x_dest, y_dest, anchor);
		paint.setAlpha(255);
	}

	public void drawRegionScalse(mBitmap img, int x_src, int y_src, int width, int height,
			int transform, int x_dest, int y_dest, int anchor, Boolean isUclip,
			int perScale) {

		x_src *= zoomLevel;
		y_src *= zoomLevel;
		width *= zoomLevel;
		height *= zoomLevel;
		x_dest *= zoomLevel;
		y_dest *= zoomLevel;
		//drawRegion(img.image, x_src, y_src, width, height, transform, x_dest, y_dest, anchor);
		_drawRegionScale(img, x_src, y_src, width, height, transform, x_dest, y_dest, anchor, perScale);
	}

	
}
