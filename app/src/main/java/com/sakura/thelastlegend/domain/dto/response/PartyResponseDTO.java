package com.sakura.thelastlegend.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class PartyResponseDTO {
    private byte type;
    private long id;
    private String content;
    private int idParty;
    private long idCharLeader;
    private List<PartyMemberInfoDTO> partyMemberInfo;

}
