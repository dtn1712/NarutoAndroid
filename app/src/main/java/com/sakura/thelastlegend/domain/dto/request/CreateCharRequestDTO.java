package com.sakura.thelastlegend.domain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CreateCharRequestDTO {

    private byte country;
    private byte charClass;
    private byte gender;
    private String charName;
}
