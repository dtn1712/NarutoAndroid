package com.sakura.thelastlegend.domain.dto.clan;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ClanMemberDTO {
    private long id;
    private String name;
    private String position;
    private short level;
    private short idHead;
    private boolean online;
}
