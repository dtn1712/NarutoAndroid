package com.sakura.thelastlegend.domain.dto.response;

import com.sakura.thelastlegend.domain.dto.minigame.MiniGameTemplateDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class JoinMiniGameResponseDTO {
    private boolean inMiniGame;
    private MiniGameTemplateDTO miniGameTemplate;
}
