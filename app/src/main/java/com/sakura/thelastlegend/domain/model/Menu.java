package com.sakura.thelastlegend.domain.model;


import java.util.Vector;





import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.real.Service;
import screen.GameScreen;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.lib.mVector;

//import com.sakura.thelastlegend.domain.model.ChatManager;
//import com.sakura.thelastlegend.domain.model.ChatTab;
import Objectgame.Npc;

public class Menu implements IActionListener {
	public boolean showMenu;
	public Vector menuItems;
	public int menuSelectedItem;
	public static int[] menuTemY;
	public int menuX, menuY, menuW, menuH/*, menuTemY*/;
	public int menuXNPC, menuYNPC, menuWNPC, menuHNPC/*, menuTemY*/;
	public static int cmtoX, cmx, cmdy, cmvy, cmxLim, xc,wTextPaintNpc;
	private boolean wantUpdateList;
	Command left = new Command(mResources.SELECT, 0);
	Command right = GameCanvas.isTouch?null:new Command(mResources.CLOSE, 0, GameCanvas.w - 71, GameCanvas.h - Screen.cmdH + 1);
	Command center = null;
	private boolean touch, close;
	public byte typeMenu;
	public final byte MENULIST = 0;
	public final byte NPC = 1;
	public String[] textNpc;
	public long timeCownDowrequestImgNPC = 5000;
	public long timeCurrent;
	
	public Npc npc;
	public static mBitmap nameNpc,decor,npctest,imgRightArrow,imgRightArrowFocus;
	public static mBitmap[] icn_focus = new mBitmap[6];
	Command next;

	
//	public static mBitmap imgMenu1 =   GameCanvas.loadImage("/hd/btnlBig0.png");
//	public static mBitmap imgMenu2 =   GameCanvas.loadImage("/hd/btnlBig1.png");
	
	boolean disableClose;
	public void startWithoutCloseButton(mVector menuItems, int pos) {
		startAt(menuItems, pos);
		disableClose = true;
	}
	public int tDelay;
	public void startAt(Vector menuItems, int pos) {
		if (showMenu)
			return;
		npc = null;
		npctest = null;
		typeMenu = MENULIST;
		isClose = false;
		touch = false;
		close = false;
		isNotClose = new boolean[menuItems.size()];
		for (int i = 0; i < isNotClose.length; i++)
			isNotClose[i] = false;
		disableClose = false;
		ChatPopup.currentMultilineChatPopup = null;
		InfoDlg.hide();

		tDelay = 0;
		if (menuItems.size() == 0)
			return;
		this.menuItems = menuItems;
		menuW = 64;

		menuH = 25;

		for (int i = 0; i < menuItems.size(); i++) {
			Command c = (Command) menuItems.elementAt(i);
			int w = mFont.tahoma_7_white.getWidth(c.caption);
			if (w > menuW - 8) {
				c.subCaption = mFont.tahoma_7_yellow.splitFontArray(c.caption, menuW - 8);
			}
		}
		menuTemY = new int[menuItems.size()];
		menuX = (GameCanvas.w - menuItems.size() * menuW) / 2;
		if (menuX < 1)
			menuX = 1;
		menuY = GameCanvas.h - menuH - (Paint.hTab+1);
		if (GameCanvas.isTouch) {
			// menuW = GameCanvas.w / menuItems.size();
			menuY -= 3;
		}
		//menuTemY = menuY;// GameCanvas.h;
		menuY += 27;
		for (int i = 0; i < menuTemY.length; i++)
			menuTemY[i] = GameCanvas.h;
		showMenu = true;
		menuSelectedItem = 0;

		cmxLim = this.menuItems.size() * menuW - GameCanvas.w;
	
		if (cmxLim < 0)
			cmxLim = 0;
		cmtoX = 0;
		cmx = 0;
		xc = 50;

		w = menuItems.size() * menuW - 1;
		if (w > GameCanvas.w - 2)
			w = GameCanvas.w - 2;

		if (GameCanvas.isTouch)
			menuSelectedItem = -1;
		Screen.keyTouch = -1;
	}
	public void startAtNPC(Vector menuItems, int pos,int idiconNpc,Npc npcc,String text) {
		if (showMenu)
			return;
		Cout.println("start NPC "+text);

		timeCurrent = 0;
		npc = null;
		npc = npcc;
		npctest = null;
		typeMenu = NPC;
		isClose = false;
		touch = false;
		close = false;
		isNotClose = new boolean[menuItems.size()];
		for (int i = 0; i < isNotClose.length; i++)
			isNotClose[i] = false;
		disableClose = false;
		ChatPopup.currentMultilineChatPopup = null;
		InfoDlg.hide();
		menuXNPC =  40;

		wTextPaintNpc = GameCanvas.w-menuXNPC*2;
		tDelay = 0;
		if(next!=null)
		next.isNoPaintImage = false;
//		if (menuItems.size() == 0)
//			return;
		this.menuItems = menuItems;
		menuW = 60;
		menuH = 60;
		menuXNPC = 30;
		menuYNPC = GameCanvas.h-50;
		menuWNPC = GameCanvas.w-60;
		menuHNPC = 70;
		for (int i = 0; i < menuItems.size(); i++) {
			Command c = (Command) menuItems.elementAt(i);
			c.x = menuXNPC+menuWNPC - 20;
			c.y = menuYNPC+menuHNPC/2-20;
			if(c.caption.length()==0){
				c.img = imgRightArrow;
				c.imgFocus = imgRightArrowFocus;
				next = c;
				
			}
			else{
				int wCmd = LoadImageInterface.img_use.getWidth();
				c.x = menuXNPC+menuWNPC/2
						-(this.menuItems.size()*(wCmd))/2 - ((this.menuItems.size()-1)*wCmd/2)/2
						+i*(wCmd+wCmd/2);
				c.y = menuYNPC+menuHNPC/2-20+(c.caption.equals("OK")==true?10:0);
				c.img = LoadImageInterface.img_use;
				c.imgFocus = LoadImageInterface.img_use_focus;
				c.w = mGraphics.getImageWidth(LoadImageInterface.img_use);
				next = c;
				if(!next.caption.equals("Nhiệm vụ")&&!next.caption.equals("OK")){
//					next.isNoPaintImage =true;
					c.h = mGraphics.getImageHeight(LoadImageInterface.img_use);
				}else if(next.caption.equals("OK")){
					next = null;
				}
			}
			
//			int w = mFont.tahoma_7_yellow.getWidth(c.caption);
//			if (w > menuW - 8) {
//				c.subCaption = mFont.tahoma_7_yellow.splitFontArray(c.caption, menuW - 8);
//			}
		}
		if(menuItems.size()==0&&pos==0){
			next = new Command("", this, 0, null);
			next.x = menuXNPC+menuWNPC - 20;
			next.y = menuYNPC+menuHNPC/2-20;
			if(next.caption.length()==0){
				next.img = imgRightArrow;
				next.imgFocus = imgRightArrowFocus;
			}
		}
		textNpc =  mFont.tahoma_7_white.splitFontArray(text,wTextPaintNpc);
		
		menuTemY = new int[menuItems.size()];
		menuX = (GameCanvas.w - menuItems.size() * menuW) / 2;
		if (menuX < 1)
			menuX = 1;
		menuY = GameCanvas.h - menuH - (Paint.hTab+1);
		if (GameCanvas.isTouch) {
			// menuW = GameCanvas.w / menuItems.size();
			menuY -= 3;
		}
		//menuTemY = menuY;// GameCanvas.h;
		menuY += 27;
		for (int i = 0; i < menuTemY.length; i++)
			menuTemY[i] = GameCanvas.h;
		showMenu = true;
		menuSelectedItem = 0;

		cmxLim = this.menuItems.size() * menuW - GameCanvas.w;
	
		if (cmxLim < 0)
			cmxLim = 0;
		cmtoX = 0;
		cmx = 0;
		xc = 50;

		w = menuItems.size() * menuW - 1;
		if (w > GameCanvas.w - 2)
			w = GameCanvas.w - 2;

		if (GameCanvas.isTouch)
			menuSelectedItem = -1;
		Screen.keyTouch = -1;
	}
	int w;
	int pa = 0;
	boolean trans = false;

	private int pointerDownTime;
	private int pointerDownFirstX;
	private int[] pointerDownLastX = new int[3];
	private boolean pointerIsDowning, isDownWhenRunning;
	private int waitToPerform, cmRun;


	
	public void updateMenuKey() {
	
		if (!showMenu)
			return;
		
		if (isScrolling())
			return;
		boolean changeFocus = false;
		if (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[4]) {
			changeFocus = true;
			menuSelectedItem--;
			if (menuSelectedItem < 0)
				menuSelectedItem = menuItems.size() - 1;
		} else if (GameCanvas.keyPressed[8] || GameCanvas.keyPressed[6]) {
			changeFocus = true;
			menuSelectedItem++;
			if (menuSelectedItem > menuItems.size() - 1)
				menuSelectedItem = 0;
		} else if (GameCanvas.keyPressed[5]) {
			if (center != null) {
				if (center.idAction > 0) {
					if (center.actionListener == GameScreen.gI()) {
						GameScreen.gI().actionPerform(center.idAction, center.p);
					} else
						perform(center.idAction, center.p);
				}
			} else {

				waitToPerform = 2;
			}
			// fire = true;
			// isClose = true;
		} else if (GameCanvas.keyPressed[12]) {
			if (isScrolling())
				return;
			if (left.idAction > 0) {
				perform(left.idAction, left.p);
			} else
				waitToPerform = 2;
			//SoundMn.gI().buttonClose();
			
			// close = true;
			// fire = true;
		} else if (!disableClose && (GameCanvas.keyPressed[13] || Screen.getCmdPointerLast(right))) {
			if (isScrolling())
				return;

			if (!close)
				close = true;
			isClose = true;
			//SoundMn.gI().buttonClose();
			// doCloseMenu();
		}
		//

		
		//
		if (changeFocus) {
			cmtoX = menuSelectedItem * menuW + menuW - GameCanvas.w / 2;
			if (cmtoX > cmxLim)
				cmtoX = cmxLim;
			if (cmtoX < 0)
				cmtoX = 0;
			if (menuSelectedItem == menuItems.size() - 1 || menuSelectedItem == 0)
				cmx = cmtoX;
		}

		if (!disableClose && GameCanvas.isPointerJustRelease && !GameCanvas.isPointer(menuX, menuY, w, menuH)
				&& !pointerIsDowning) {
			if (isScrolling())
				return;
			pointerDownTime = pointerDownFirstX = 0;
			pointerIsDowning = false;
			//GameCanvas.clearAllPointerEvent();
			//Res.out("menu select= " + menuSelectedItem);
			isClose = true;
			close = true;
			//SoundMn.gI().buttonClose();
			return;

		}
		
		if (GameCanvas.isPointerDown) {
			if (!pointerIsDowning && GameCanvas.isPointer(menuX, menuY, w, menuH)) {
				for (int i = 0; i < pointerDownLastX.length; i++)
					pointerDownLastX[0] = GameCanvas.px;
				pointerDownFirstX = GameCanvas.px;
				pointerIsDowning = true;
				isDownWhenRunning = cmRun != 0;
				cmRun = 0;
			} else if (pointerIsDowning) {
				pointerDownTime++;
				if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.px && !isDownWhenRunning) {
					pointerDownFirstX = -1000;
					menuSelectedItem = (cmtoX + GameCanvas.px - menuX) / menuW;
				}
				int dx = GameCanvas.px - pointerDownLastX[0];
				if (dx != 0 && menuSelectedItem != -1) {
					menuSelectedItem = -1;
				}
				for (int i = pointerDownLastX.length - 1; i > 0; i--)
					pointerDownLastX[i] = pointerDownLastX[i - 1];
				pointerDownLastX[0] = GameCanvas.px;

				cmtoX -= dx;
				if (cmtoX < 0)
					cmtoX = 0;
				if (cmtoX > cmxLim)
					cmtoX = cmxLim;
				if (cmx < 0 || cmx > cmxLim)
					dx /= 2;
				cmx -= dx;

				if (cmx < -(GameCanvas.h / 3))
					wantUpdateList = true;
				else
					wantUpdateList = false;
			}

		}
		if (GameCanvas.isPointerJustRelease && pointerIsDowning) {
			int dx = GameCanvas.px - pointerDownLastX[0];
			GameCanvas.isPointerJustRelease = false;

			if (Res.abs(dx) < 20 && Res.abs(GameCanvas.px - pointerDownFirstX) < 20 && !isDownWhenRunning) {
				cmRun = 0;
				cmtoX = cmx;
				pointerDownFirstX = -1000;
				menuSelectedItem = (cmtoX + GameCanvas.px - menuX) / menuW;
				
				pointerDownTime = 0;
				waitToPerform = 10;
			} else if (menuSelectedItem != -1 && pointerDownTime > 5) {
				pointerDownTime = 0;
				waitToPerform = 1;
			} else if (menuSelectedItem == -1 && !isDownWhenRunning) {
				if (cmx < 0)
					cmtoX = 0;
				else if (cmx > cmxLim)
					cmtoX = cmxLim;
				else {
					int s = ((GameCanvas.px - pointerDownLastX[0]) + (pointerDownLastX[0] - pointerDownLastX[1]) + (pointerDownLastX[1] - pointerDownLastX[2]));
					if (s > 10)
						s = 10;
					else if (s < -10)
						s = -10;
					else
						s = 0;
					cmRun = -s * 100;
				}

			}
			pointerIsDowning = false;
			pointerDownTime = 0;
			GameCanvas.isPointerJustRelease = false;
		}

		GameCanvas.clearKeyPressed();
		GameCanvas.clearKeyHold();
	}

	int cmvx, cmdx;

	public void moveCamera() {
		if (cmRun != 0 && !pointerIsDowning) {
			cmtoX += cmRun / 100;
			if (cmtoX < 0)
				cmtoX = 0;
			else if (cmtoX > cmxLim)
				cmtoX = cmxLim;
			else
				cmx = cmtoX;
			cmRun = cmRun * 9 / 10;
			if (cmRun < 100 && cmRun > -100)
				cmRun = 0;

		}
		if (cmx != cmtoX && !pointerIsDowning) {
			cmvx = (cmtoX - cmx) << 2;
			cmdx += cmvx;
			cmx += cmdx >> 4;
			cmdx = cmdx & 0xf;
		}
	}
	public void paintMenu(mGraphics g) {
		
		g.translate(-g.getTranslateX(), -g.getTranslateY());
		//g.setClip(-1000, -1000, 2000, 2000);
		// GameCanvas.paint.paintFrameSimple(menuX, menuTemY, w, menuH, g);
		// GameCanvas.paint.paintFrameInside(menuX, menuTemY, w, menuH, g);
		//g.setClip(0, menuY, GameCanvas.w, GameCanvas.h - menuY+1);
		if(typeMenu==NPC){
			paintMenuNpc(g);
			return;
		}
		g.translate(-cmx, 0);
		if(GameCanvas.isTouch){
			for (int i = 0; i < menuItems.size(); i++) {
				if (i == menuSelectedItem) {
					// g.setColor(Paint.COLORLIGHT);
					// g.fillRect(menuX + i * menuW + 1, menuTemY + 1, menuW - 3,
					// menuH - 2);
					g.drawImage(LoadImageInterface.img_use_focus, menuX + i * menuW + 1, menuTemY[i] + 1, 0,true);
				} else
					g.drawImage(LoadImageInterface.img_use, menuX + i * menuW + 1, menuTemY[i] + 1, 0,true);
				String sc[] = ((Command) menuItems.elementAt(i)).subCaption;
				if (sc == null)
					sc = new String[] { ((Command) menuItems.elementAt(i)).caption };
				int yCaptionStart = menuTemY[i] + (menuH - (sc.length * 14)) / 2 + 1;
				for (int k = 0; k < sc.length; k++) {
//				//menuW = 65;
						if (GameScreen.isMessageMenu) {
							if (ChatManager.gI().findWaitPerson(sc[k])) {
								if (GameCanvas.gameTick % 10 > 5)
									mFont.tahoma_7_red.drawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2, yCaptionStart + k * 14 , 2);
								else
									mFont.tahoma_7_white.drawString(g, sc[k], menuX + i * menuW + menuW / 2 , yCaptionStart + k * 14 +1, 2);
							} else
								mFont.tahoma_7_white.drawString(g, sc[k], menuX + i * menuW + menuW / 2 , yCaptionStart + k * 14 +1, 2);
						} else
							mFont.tahoma_7_white.drawString(g, sc[k], menuX + i * menuW + menuW / 2 , yCaptionStart + k * 14 +1, 2);
					}

			}
		}else{
			for (int i = 0; i < menuItems.size(); i++) {
				if (i == menuSelectedItem) {
					// g.setColor(Paint.COLORLIGHT);
					// g.fillRect(menuX + i * menuW + 1, menuTemY + 1, menuW - 3,
					// menuH - 2);
					g.drawImage(LoadImageInterface.img_use_focus, menuX + i * menuW + 1, menuTemY[i] + 1 - 23, 0);
				} else
					g.drawImage(LoadImageInterface.img_use, menuX + i * menuW + 1, menuTemY[i] + 1 - 23, 0);
				String sc[] = ((Command) menuItems.elementAt(i)).subCaption;
				if (sc == null)
					sc = new String[] { ((Command) menuItems.elementAt(i)).caption };
				int yCaptionStart = menuTemY[i] + (menuH - (sc.length * 14)) / 2 + 1 - 23;
				for (int k = 0; k < sc.length; k++) {
//				//menuW = 65;
						if (GameScreen.isMessageMenu) {
							if (ChatManager.gI().findWaitPerson(sc[k])) {
								if (GameCanvas.gameTick % 10 > 5)
									mFont.tahoma_7_red.drawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2, yCaptionStart + k * 14 , 2);
								else
									mFont.tahoma_7_yellow.drawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2, yCaptionStart + k * 14 , 2);
							} else
								mFont.tahoma_7_yellow.drawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2, yCaptionStart + k * 14 , 2);
						} else
							mFont.tahoma_7_yellow.drawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2, yCaptionStart + k * 14 , 2);
					}

			}
		}
		g.translate(-g.getTranslateX(), -g.getTranslateY());
		//g.setClip(-1000, -1000, 2000, 2000);

		// GameCanvas.paint.paintCmdBar(g, null, center, right);
	}
	public void paintMenuNpc(mGraphics g){
//		if(npc!=null&&nameNpc!=null)
//			npc.paint(g, 30+nameNpc.getWidth()/2, GameCanvas.h-40-nameNpc.getHeight());

		g.setColor(0xff000000,70);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		if(npctest!=null){
			g.drawRegion(npctest, 0, 0, npctest.getWidth(), npctest.getHeight(), 2,
					menuXNPC+10+nameNpc.getWidth()/2, menuYNPC-npctest.getHeight()/2, mGraphics.VCENTER|mGraphics.HCENTER);// GameCanvas.w-40, GameCanvas.h-decor.getHeight()
			
		}
//			g.drawImage(npctest,30+nameNpc.getWidth()/2, GameCanvas.h-40-npctest.getHeight()/2, mGraphics.VCENTER|mGraphics.HCENTER);
//		g.fillRect(30, GameCanvas.h-40, GameCanvas.w-60, 40);
		if(nameNpc!=null)
			g.drawImage(nameNpc, menuXNPC+10, menuYNPC-nameNpc.getHeight(), 0);
		Paint.SubFrame(menuXNPC, menuYNPC, menuWNPC, menuHNPC, g,0xff037ddb,80);
//		g.setColor(0xffff00ff);
//		g.fillRect(menuXNPC+10, menuYNPC, wTextPaintNpc, 100);
		for (int i = 0; i < textNpc.length; i++) {
			mFont.tahoma_7_white.drawString(g, textNpc[i], menuXNPC+10, menuYNPC+10+i*14, 0);
		}
		if(npc!=null&&npc.template.name!=null&&npc.template.name!=null&&nameNpc!=null)
		{
			mFont.tahoma_7.drawString(g, npc.template.name, menuXNPC+10+nameNpc.getWidth()/2,menuYNPC-4-nameNpc.getHeight()/2, 2);
		}
		for (int i = 0; i <menuItems.size(); i++) {
			Command cmd = (Command)menuItems.elementAt(i);
			cmd.paint(g);
		}
		if(decor!=null){
			g.drawImage(decor,menuXNPC-decor.getWidth(), GameCanvas.h-decor.getHeight(), 0);
			g.drawRegion(decor, 0, 0, decor.getWidth(), decor.getHeight(), 2, GameCanvas.w-30, GameCanvas.h-decor.getHeight(),mGraphics.TOP|mGraphics.LEFT);// GameCanvas.w-40, GameCanvas.h-decor.getHeight()
		}
		if(next!=null) next.paint(g);
		if(next!=null&&next.isNoPaintImage&&icn_focus!=null)
		{
			
			g.drawImage(icn_focus[(GameCanvas.gameTick/4)%5], next.x-mFont.tahoma_7_white.getWidth(next.caption+"   ")/2+next.img.getWidth()/2, next.y+next.img.getHeight()/2, mGraphics.VCENTER|mGraphics.HCENTER);
		}
	}
	boolean isClose;
	public boolean[] isNotClose;

	public void doCloseMenu(){
		Cout.println("doCloseMenu");
		isClose = false;
		showMenu = false;
		InfoDlg.hide();
		if(close){
			GameScreen.isMessageMenu =false;
//			if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.chatPopup != null)
//				Char.myChar().npcFocus.chatPopup = null;
		}
		else if (touch) {
			
			if (menuSelectedItem >= 0) {
				Command c = ((Command) menuItems.elementAt(menuSelectedItem));
				if (c != null){
					//SoundMn.gI().buttonClose();
					c.performAction();
				}
			}
		}
	}
	public boolean isScrolling() {
		
		if (menuItems.size()>0&&(!isClose &&(menuTemY.length - 1>=0)&& menuTemY[menuTemY.length - 1] > menuY)
				|| (isClose &&(menuTemY.length - 1>=0)&& menuTemY[menuTemY.length - 1] < GameCanvas.h))
			return true;
		return false;
	}



	public void updateMenu(){
			if(typeMenu==NPC&&nameNpc==null){
				if(nameNpc==null) nameNpc = GameCanvas.loadImage("/GuiNaruto/nameNPC.png");
				if(decor==null) decor = GameCanvas.loadImage("/GuiNaruto/decor.png");//
				imgRightArrow=GameCanvas.loadImage("/GuiNaruto/Trade/right_arrow.png");
				imgRightArrowFocus=GameCanvas.loadImage("/GuiNaruto/Trade/right_arrow2.png");
				if(next!=null&&next.caption.trim().length()==0){
					next.img = imgRightArrow;
					next.imgFocus = imgRightArrowFocus;
				}
				
			}
			if(next!=null&&typeMenu==NPC&&icn_focus!=null&&icn_focus[0]==null&&next.isNoPaintImage){
				for (int i = 0; i < icn_focus.length; i++) {
					icn_focus[i] = GameCanvas.loadImage("/GuiNaruto/icn_focus_"+i+".png");
				}
			}
			if(typeMenu==NPC&&npctest==null&&npc!=null&&npc.template!=null&&npc.template.idavatar!=-1){
				npctest = GameCanvas.loadImage("/npc/"+(npc.template.idavatar+SmallImage.ID_ADD_AVATARNPC)+".png");
				if(mSystem.currentTimeMillis()-timeCurrent>timeCownDowrequestImgNPC){
					timeCurrent = mSystem.currentTimeMillis();
					if(npctest==null) 
						Service.gI().requestImage(npc.template.idavatar+SmallImage.ID_ADD_AVATARNPC,(byte)0);
				}
			}
			
			if(typeMenu==NPC){
				for (int i = 0; i < menuItems.size(); i++) {
					Command cmd = (Command)menuItems.elementAt(i);
					if(Screen.getCmdPointerLast(cmd))
					{
						Cout.println2222("menuItem npc  "+i);
						doCloseMenu();
						cmd.performAction();
						return;
					}
				}
//				if(next!=null)
//				    if(Screen.getCmdPointerLast(next)||(GameCanvas.isPointerJustRelease&&GameCanvas.isPoint(next.x-3*next.w/2, next.y-15, next.w*2, 30))){
//						GameCanvas.isPointerJustRelease = false;
//						menuSelectedItem = 0;
//						if (next != null){
//							//SoundMn.gI().buttonClose();
//							doCloseMenu();
//							next.performAction();
//							return;
//						}
//				    }
			    if(GameCanvas.isPointerJustRelease&&!GameCanvas.isPoint(menuXNPC+10, menuYNPC, wTextPaintNpc, 100))
			    	doCloseMenu();
			    return;
			}
			moveCamera();
			if (!isClose) {
				tDelay++;
				for (int i = 0; i < menuTemY.length; i++)
					if (menuTemY[i] > menuY) {
						int delta = ((menuTemY[i] - menuY) >> 1);
						if (delta < 1)
							delta = 1;
						if (tDelay > i) {
							menuTemY[i] -= delta;
						}

					}
				if (menuTemY.length>0&&menuTemY[menuTemY.length - 1] <= menuY)
					tDelay = 0;

			} else {
				tDelay++;
				for (int i = 0; i < menuTemY.length; i++)
					if (menuTemY[i] < GameCanvas.h) {
						int delta;
						if(GameCanvas.isTouch)
							delta = ((GameCanvas.h - menuTemY[i]) >> 1) + 2;
						else
							delta = ((GameCanvas.h - menuTemY[i]) >> 1) + 2;
						if (delta < 1)
							delta = 1;
						if (tDelay > i)
							menuTemY[i] += delta;
					}

				if ((menuTemY.length - 1>=0)&&menuTemY[menuTemY.length - 1] >= GameCanvas.h) {
					tDelay = 0;
					Cout.println("doCloseMenu  2222 ");
					
					doCloseMenu();
				}
			}
			if (xc != 0) {
				xc >>= 1;
				if (xc < 0)
					xc = 0;
			}
			if (isScrolling())
				return;
			if (waitToPerform > 0) {
				waitToPerform--;
			
				if (waitToPerform == 0) {
					Cout.println(getClass(),isNotClose.length+ " updateMenu   menuSelectedItem  "+menuSelectedItem);
					if (menuSelectedItem!=-1&&menuSelectedItem<isNotClose.length&& !isNotClose[menuSelectedItem]) {
						isClose = true;
						touch = true;
						//GameCanvas.panel.cp = null;
						
					} 
					else
						performSelect();
				}

			}
		
	}
	public void perform(int idAction, Object p) {

		Cout.println(" perform action menu "+idAction);
		if(idAction==0) doCloseMenu();
		if (idAction == 1000) {
			ChatTab c = (ChatTab) p; 
			menuItems.removeAllElements();
			ChatManager.gI().removeFromWaitList(c.ownerName);
			ChatManager.gI().chatTabs.removeElement(c);
			for(int i=0;i<ChatManager.gI().chatTabs.size();i++)
			{
				ChatTab tab = (ChatTab) ChatManager.gI().chatTabs.elementAt(i);
				menuItems.addElement(new Command(tab.ownerName,null, 12001,new Integer(i)));
			}		
			menuItems.addElement(new Command(mResources.BLOCK_MESSAGE, null, 12006,null));
			menuItems.addElement(new Command(mResources.CHAT_ADMIN, null, 12008,null));
			for (int i = 0; i < menuItems.size(); i++) {
				Command c1 = (Command) menuItems.elementAt(i);
				int w = mFont.tahoma_7_yellow.getWidth(c1.caption);
				if (w > menuW - 8) {
					c1.subCaption = mFont.tahoma_7_yellow.splitFontArray(c1.caption, menuW - 8);
				}
			}
			
			cmxLim = this.menuItems.size() * menuW - GameCanvas.w;
			cmtoX = menuSelectedItem * menuW + menuW - GameCanvas.w / 2;
			if (cmtoX > cmxLim)
				cmtoX = cmxLim;
			if (cmtoX < 0)
				cmtoX = 0;
			if (menuSelectedItem == menuItems.size() - 1
					|| menuSelectedItem == 0)
				cmx = cmtoX;
		}
	
	}
	public void performSelect() {
		InfoDlg.hide();
		Cout.println(" performSelect  kakak ");
		if (menuSelectedItem >= 0&&menuSelectedItem<menuItems.size()) {
			Command c = ((Command) menuItems.elementAt(menuSelectedItem));
			Cout.println(" performSelect ccc  kakak ");
			if (c != null){
				c.performAction();
			}
		}
	}
	public void perform() {
		// TODO Auto-generated method stub
		
	}
}
