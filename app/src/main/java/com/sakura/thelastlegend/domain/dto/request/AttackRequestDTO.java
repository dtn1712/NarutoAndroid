package com.sakura.thelastlegend.domain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class AttackRequestDTO {
    private byte skillAttackId;
    private byte targetType;
    private long targetId;
}
