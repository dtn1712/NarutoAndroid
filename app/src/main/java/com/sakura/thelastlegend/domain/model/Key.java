package com.sakura.thelastlegend.domain.model;

public class Key {
	
	public static final int NUM0 = 0;
    public static final int NUM1 = 1;
    public static final int NUM2 = 2;
    public static final int NUM3 = 3;
    public static final int NUM4 = 4;
    public static final int NUM5 = 5;
    public static final int NUM6 = 6;
    public static final int NUM7 = 7;
    public static final int NUM8 = 8;
    public static final int NUM9 = 9;
    public static final int STAR = 10;
    public static final int BOUND = 11;
    public static final int UP = 12;
    public static final int DOWN = 13;
    public static final int LEFT = 14;
    public static final int RIGHT = 15;
    public static final int FIRE = 16;
    public static final int LEFT_SOFTKEY = 17;
    public static final int RIGHT_SOFTKEY = 18;
    public static final int CLEAR = 19;
    public static final int BACK = 20;
}
