package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.gui.ImageEffect;

import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mBitmap;



/*
 Written by Nguyen Van Minh
 Date: 28/02/2006
 */




//import javax.microedition.lcdui.game.*;

//quan ly cac frame anh duoc xep theo chieu doc
public class FrameImage {
	public int frameWidth;
	public int frameHeight;
	public int nFrame;

	private mBitmap imgFrame;
	private int pos[];
	private int totalHeight;

	public int Id;
	private mBitmap[] imgList;
	private boolean isRotate;

	public FrameImage(mBitmap img, int width, int height) {
		imgFrame = img;
		frameWidth = width ;
		frameHeight = height ;
		totalHeight = Image.getHeight(img);
		nFrame = totalHeight / height;
		pos = new int[nFrame];
		for (int i = 0; i < nFrame; i++){
			pos[i] = i * height;
		}
	}

	public FrameImage(int ID, int width, int height) {
		// TODO Auto-generated constructor stub
		this.Id = ID;
		imgFrame = ImageEffect.setImage(ID);
		frameWidth = width;
		frameHeight = height;
		nFrame = Image.getHeight(imgFrame) / height;
		pos = new int[nFrame];
		for (int i = 0; i < nFrame; i++){
			pos[i] = i * height;
		}
	}

	public void drawFrame(int idx, int x, int y, int trans, int anchor, mGraphics g) {
			if (idx >= 0 && idx < nFrame){
				g.drawRegion(imgFrame, 0, pos[idx], frameWidth, frameHeight, trans, x, y, anchor);
			}
	}

	public void unload() {
		imgFrame = null;
		pos = null;
	}

	
}
