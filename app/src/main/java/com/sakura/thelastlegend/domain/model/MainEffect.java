package com.sakura.thelastlegend.domain.model;

import screen.GameScreen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;



public class MainEffect extends MainObject {
	boolean isPaint = true;
	public int typeEffect = 0,valueEffect;
	int fRemove;
	public int levelPaint = 0;
	long timeBegin;
	public FrameImage fraImgEff, fraImgSubEff, fraImgSub2Eff, fraImgSub3Eff;
	public FrameImage fraKhoiThianThach, fraLuaThienThach;
	

	// Direction
	public static final int DIR_UP = 1;
	public static final int DIR_DOWN = 0;
	public static final int DIR_LEFT = 2;
	public static final int DIR_RIGHT = 3;
	
	public void paint(mGraphics g) {
	}

	public void update() {
		super.update();
	}

	public void setPosition(int x, int y, int xto, int yto) {
		this.x = x;
		this.y = y;
		this.toX = xto;
		this.toY = yto;
	}

	public static boolean isInScreen(int x, int y, int w, int h) {
		if (x < GameScreen.cmx - w
				|| x > GameScreen.cmx + GameCanvas.w + w
				|| y < GameScreen.cmy - h
				|| y > GameScreen.cmy + GameCanvas.h + h)
			return false;
		return true;
	}
}
