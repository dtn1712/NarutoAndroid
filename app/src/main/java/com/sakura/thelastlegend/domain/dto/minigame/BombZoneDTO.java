package com.sakura.thelastlegend.domain.dto.minigame;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BombZoneDTO {
    private short x;
    private short y;
    private short width;
    private short height;
}
