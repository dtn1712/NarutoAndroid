package com.sakura.thelastlegend.domain.model;


import java.util.Vector;

import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.mVector;

public abstract class Effect2 {
	public static mVector vEffect2 = new mVector();
	public static mVector vRemoveEffect2 = new mVector();
	public static mVector vEffect2Outside = new mVector();
	public static mVector vAnimateEffect = new mVector();
	
	public abstract void update();
	public abstract void paint(mGraphics g);
}
