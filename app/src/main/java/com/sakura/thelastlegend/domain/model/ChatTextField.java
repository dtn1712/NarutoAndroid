package com.sakura.thelastlegend.domain.model;


import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.mFont;

public class ChatTextField implements IActionListener{
	private static ChatTextField instance;
	public TField tfChat;
	public boolean isShow = false;
	public IChatable parentScreen;
	long lastChatTime = 0;

	public Command left = new Command(mResources.CHAT, this,8000,null, 1 , GameCanvas.h - Screen.cmdH + 1);
	public Command right = new Command(mResources.DELETE, this,8001,null ,GameCanvas.w - 53, GameCanvas.h - Screen.cmdH + 1);
	public Command center = null;

	public void keyPressed(int keyCode) {
		if (isShow)
			tfChat.keyPressed(keyCode);
		if (tfChat.getText().equals("")) {
			right.caption = mResources.CLOSE;
		}
		else right.caption=mResources.DELETE;
	}

	public static ChatTextField gI() {
		return instance == null ? instance = new ChatTextField() : instance;
	}

	protected ChatTextField() {
		tfChat = new TField();
//		tfChat.name = "chat";
		tfChat.x = 16;
		tfChat.width = GameCanvas.w - 32;
		tfChat.height = Screen.ITEM_HEIGHT + 2;
		tfChat.isFocus = true;
		tfChat.setMaxTextLenght(40);
	}
	String to;
	public void startChat(int firstCharacter, IChatable parentScreen,String to) {
		right.caption = mResources.CLOSE;
		this.to=to;
		tfChat.keyPressed(firstCharacter);
		if (!tfChat.getText().equals("") && GameCanvas.currentDialog==null) {
			this.parentScreen = parentScreen;
			isShow = true;
		}
	}

	public void startChat(IChatable parentScreen,String to) {
		right.caption = mResources.CLOSE;
		this.to=to;
		 if(GameCanvas.currentDialog==null){
			isShow = true;
			if(GameCanvas.isTouch){
				tfChat.doChangeToTextBox();
			}
		}
	}

	public void update() {
		if (!isShow)
			return;
		tfChat.update();
		if(tfChat.justReturnFromTextBox)
		{
			tfChat.justReturnFromTextBox=false;
			parentScreen.onChatFromMe(tfChat.getText(),to);
			tfChat.setText("");
			right.caption=mResources.CLOSE;
			
		}
	}

	public void close(){
		tfChat.setText("");
		isShow= false;
	}
	public void paint(mGraphics g) {
		if (!isShow)
			return;
		
		GameCanvas.paint.paintFrame(tfChat.x-14, tfChat.y-18, tfChat.width+28, tfChat.height+26, g);
		mFont.tahoma_7b_white.drawString(g,"Chat "+to ,tfChat.x, tfChat.y-13, 0);
		tfChat.paint(g);
	}

	public void perform(int idAction, Object p) {
		switch (idAction) {
		case Constants.BUTTON_SEND_CHAT_WORLD://chat world
			if (parentScreen != null) {
				long now = System.currentTimeMillis();
				if (now - lastChatTime < 1000)
					return;
				lastChatTime = now;
			
				parentScreen.onChatFromMe(tfChat.getText(),to);
				tfChat.setText("");
				right.caption=mResources.CLOSE;
			}
			break;
		case 8001:
			tfChat.clear();
			if (tfChat.getText().equals("")) {
				isShow = false;
				parentScreen.onCancelChat();
			}
			break;
		default:
			break;
		}
		
	}

	public void perform() {
		// TODO Auto-generated method stub
		
	}
}
