package com.sakura.thelastlegend.domain.dto.response;
import com.sakura.thelastlegend.domain.dto.game.ItemAttributeDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ItemTemplateInfoDTO {
    private short id;
    private byte type;
    private byte gender;
    private String name;
    private String description;
    private short levelRequired;
    private short icon;
    private long price;
    private byte charClass;
    private byte country;
    private short idPartBody;
    private short idPartLeg;
    private short idPartHead;
    private boolean sellByGold;
    private ItemAttributeDTO baseAttribute;
}
