package com.sakura.thelastlegend.domain.dto.clan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ClanInfoDTO {
    private long id;
    private String name;
    private String clanType;
    private List<ClanMemberDTO> members;    // Host o vi tri dau tien
}
