package com.sakura.thelastlegend.domain.model;





import Objectgame.Char;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Rms;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mSystem;


public class QuickSlot {
	public byte quickslotType = -1;// Skill or Potion
	public byte idSkill, ItemType, indexPotion;// Must set
	public static byte idSkillCoBan=-1;
	public int ideff;
	public static final byte TYPE_SKILL = 1;
	public static final byte TYPE_ITEM = 2;
	public static final byte TYPE_POTION = 2;
	public static final byte []WEAPON_OF_CLAZZ =  new byte[]{ 0, 1, 2, 3, 4, 0, 1, 2, 3, 4 };
	public static mBitmap img = null;
	public boolean isBuff = false;
	public short idicon;
	public int cooldown,mp;
	private long timewait,timecoolDown;
	public static byte[] idSkillGan = new byte[]{-1,-1,-1,-1,-1};
	public static void getImg() {
		img =GameCanvas.loadImage("/interface/delayskill.png");
	}
	public QuickSlot(){
		idSkill=-1;
	}
	public void setIsSkill(int skillType, boolean isBuff) {
		SkillTemplate skill=(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+skillType);
		quickslotType = TYPE_SKILL;
		this.idSkill = (byte)skillType;
		this.ideff = skill.ideff;
		this.isBuff = isBuff;
		if(skill.level<skill.mphao.length)
		this.mp = skill.mphao[skill.level];
		else this.mp = skill.mphao[0];
		this.idicon=(short)skill.iconId;
		cooldown=skill.getCoolDown((byte)skill.level);
		
	}
	public static void SaveRmsQuickSlot(){
		for (int i = 0; i < Char.myChar().mQuickslot.length; i++) {
			QuickSlot ql = Char.myChar().mQuickslot[i];
			if(ql!=null&&idSkillGan!=null)
			idSkillGan[i] = ql.idSkill;
		}
		Rms.saveRMS("quickslot", idSkillGan);
	}
	public static void loadRmsQuickSlot(){
		idSkillGan = Rms.loadRMS("quickslot");
//		Cout.println(idSkillGan+" loadRmsQuickSlot idSkillCoBan "+idSkillCoBan);
		if(idSkillGan!=null){
			boolean isMyOldChar = false;
			for (int i = 0; i < idSkillGan.length; i++) {
				SkillTemplate skill=(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+idSkillGan[i]);
				if(skill!=null&&skill.level>=0){
					isMyOldChar = true;
					Char.myChar().mQuickslot[i].setIsSkill(idSkillGan[i], false);
				}
			}
			if(!isMyOldChar){
				idSkillGan = new byte[]{-1,-1,-1,-1,-1};
				SkillTemplate skill =(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+(idSkillCoBan>-1?idSkillCoBan:0));
				if(skill!=null&&skill.level>=0){
					idSkillGan[0] = (byte)skill.id;
					Char.myChar().mQuickslot[0].setIsSkill(0, false);
				}
				Rms.saveRMS("quickslot", idSkillGan);
			}
		}else {

			idSkillGan = new byte[]{-1,-1,-1,-1,-1};
			SkillTemplate skill =(SkillTemplate)SkillTemplates.hSkilltemplate.get(""+(idSkillCoBan>-1?idSkillCoBan:0));
			if(skill!=null&&skill.level>=0){
				idSkillGan[0] = (byte)skill.id;
				Char.myChar().mQuickslot[0].setIsSkill(0, false);
			}
			Rms.saveRMS("quickslot", idSkillGan);
		}
	}
	public void setIsSkill_introgame(int skillType, int clazz, boolean isBuff, int idicon) {
		quickslotType = TYPE_SKILL;
		this.idSkill = (byte) skillType;
		this.isBuff = isBuff;
		this.idicon=(short)idicon;
		cooldown=500;
	}
	public void paint(mGraphics g, int x, int y) {
		try {
			if (quickslotType == -1)
				return;
		} catch (Exception e) {
			Cout.println("ERROR QUICKSLOT " + e.toString());
		}
	}

	public byte getSkillType() {
		return idSkill;
	}
	public boolean getBuffType() {
		return isBuff;
	}
	public void startCoolDown(){
		timewait=(long)(mSystem.currentTimeMillis()+cooldown*1000);
	}
	public boolean canfight(){
		return (timewait-mSystem.currentTimeMillis())<0&&quickslotType==TYPE_SKILL;
	}
	public boolean isEnoughtMp(){
		return (Char.myChar().cMP>=mp?true:false);
	}
	public boolean canUse(){
		return (timewait-mSystem.currentTimeMillis())<0;
	}
	/*
	 * public void update() { timePaintM_H = mSystem.currentTimeMillis_() -
	 * Canvas.gameScreen.mainChar.timeLastUseSkills[skillType];
	 * 
	 * timePaintSkill = mSystem.currentTimeMillis_() -
	 * Canvas.gameScreen.mainChar.potionLastTimeUse[potionType]; }
	 */
	public void update() {
		
	}
	public int getPotionType(){
		return 0;
	}
	public byte getItemType() {
		return ItemType;
	}
	public String gettimeCooldown(){
		long time=(timewait-mSystem.currentTimeMillis())/1000;
		long t=0;
		if(time==0){
			t = ((timewait-mSystem.currentTimeMillis())%1000)/100;
			if(t>0)
			return "0."+t;
		}else if(time>0) return ""+time;
		return "";
	}
	public void setIsIteam(int potionType) {
		quickslotType = TYPE_ITEM;
		this.ItemType = (byte) potionType;
	}

	public void setIsNothing() {
		quickslotType = -1;
	}
}
