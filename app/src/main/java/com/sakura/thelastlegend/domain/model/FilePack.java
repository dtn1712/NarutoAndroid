package com.sakura.thelastlegend.domain.model;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.sakura.thelastlegend.GameCanvas;

import com.sakura.thelastlegend.lib.mBitmap;



public class FilePack {
	public static FilePack instance;
	private String fname[];
	private int fpos[];
	private int flen[];
	private byte fullData[];
	private int nFile;
	private int hSize;
	private String name;
	private byte[] code = { 78, 103, 117, 121, 101, 110, 86, 97, 110, 77, 105,
			110, 104 };
	private int codeLen = code.length;

	private DataInputStream file;

	public FilePack(byte[] DATA) {

		ByteArrayInputStream array = new ByteArrayInputStream(DATA);
		file = new DataInputStream(array);
		int lname;
		byte data[];
		int pos = 0, size = 0;
		hSize = 0;
		try {
			nFile = encode(file.readUnsignedByte());
			// System.out.println("xor " + (100^78));
			hSize += 1;
			fname = new String[nFile];
			fpos = new int[nFile];
			flen = new int[nFile];
			for (int i = 0; i < nFile; i++) {
				lname = encode(file.readByte());
				data = new byte[lname];
				file.read(data);
				encode(data);
				fname[i] = new String(data);
				fpos[i] = pos;
				flen[i] = encode(file.readUnsignedShort());
				pos += flen[i];
				size += flen[i];
				hSize += lname + 3; // filenamelen + filename + filesize
			}
			// cap phat bo nho va doc mang data
			fullData = new byte[size];
			file.readFully(fullData);
			encode(fullData);
		} catch (IOException e) {
		}
		close();
	}

	public FilePack(String name) {
		int lname;
		byte data[];
		int pos = 0, size = 0;
		this.name = name;
		hSize = 0;
		open();
		try {
			nFile = encode(file.readUnsignedByte());
			// System.out.println("xor " + (100^78));
			hSize += 1;
			fname = new String[nFile];
			fpos = new int[nFile];
			flen = new int[nFile];
			for (int i = 0; i < nFile; i++) {
				lname = encode(file.readByte());
				data = new byte[lname];
				file.read(data);
				encode(data);
				fname[i] = new String(data);
				fpos[i] = pos;
				flen[i] = encode(file.readUnsignedShort());
				pos += flen[i];
				size += flen[i];
				hSize += lname + 3; // filenamelen + filename + filesize
			}
			// cap phat bo nho va doc mang data
			fullData = new byte[size];
			file.readFully(fullData);
			encode(fullData);
		} catch (IOException e) {
		}
		close();
	}

	public static mBitmap getImg(String path) {
		return instance.loadImage(path + ".png");
	}

	public static void reset() {
		instance = null;
	}

	public static void init(String path) {
		instance = null;
		instance = new FilePack(path);
	}

	private int encode(int i) {
		// return i^0xffff;
		return i;
	}

	private void encode(byte[] bytes) {
		int len = bytes.length;
		for (int i = 0; i < len; i++) {
			bytes[i] = (byte) (bytes[i] ^ code[i % codeLen]);
		}
	}

	private void open() {
	}

	private void close() {
		try {
			if (file != null)
				file.close();
		} catch (IOException e) {
		}
	}

	public byte[] loadFile(String fileName) throws Exception {
		for (int i = 0; i < nFile; i++) {
			if (fname[i].compareTo(fileName) == 0) {
				byte[] bytes = new byte[flen[i]];
				System.arraycopy(fullData, fpos[i], bytes, 0, flen[i]);
				return bytes;
			}
		}
		throw new Exception("File '" + fileName + "' not found!");
	}
	public String loadFileString(String fileName) throws Exception {
		for (int i = 0; i < nFile; i++) {
			if (fname[i].compareTo(fileName) == 0) {
				byte[] bytes = new byte[flen[i]];
				System.arraycopy(fullData, fpos[i], bytes, 0, flen[i]);
				return bytes.toString();
			}
		}
		throw new Exception("File '" + fileName + "' not found!");
	}

	public mBitmap loadImage(String fileName) {
		return null;
	}
}
