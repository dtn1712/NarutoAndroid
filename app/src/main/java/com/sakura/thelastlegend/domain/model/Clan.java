package com.sakura.thelastlegend.domain.model;

import java.util.Vector;


import com.sakura.thelastlegend.lib.Util;
import com.sakura.thelastlegend.lib.mVector;

import Objectgame.Char;
import Objectgame.Item;



public class Clan {
	

	public static final int CREATE_CLAN			= 0;
	public static final int MOVE_OUT_MEM		= 1;
	public static final int MOVE_INPUT_MONEY	= 2;
	public static final int MOVE_OUT_MONEY		= 3;
	public static final int FREE_MONEY			= 4;
	public static final int UP_LEVEL			= 5;
	
	public static final int TYPE_NORMAL			= 0;
	public static final int TYPE_UUTU			= 1;
	public static final int TYPE_TRUONGLAO		= 2;
	public static final int TYPE_TOCPHO			= 3;
	public static final int TYPE_TOCTRUONG		= 4;
	
	public String name = "";
	public int exp, expNext;
	public int level, itemLevel;
	public int icon;
	public int openDun;
	public int coin, freeCoin, coinUp;
	public String main_name = "";
	public String assist_name = "";
	public String elder1_name = "";
	public String elder2_name = "";
	public String elder3_name = "";
	public String elder4_name = "";
	public String elder5_name = "";
	public String reg_date = "";
	public String log = "";
	public String alert = "";
	public int total, use_card;
	public mVector members = new mVector();
	public Item[] items;

	public void writeLog(String data){
		String strs[] = Util.split(data, "\n");
		log = "";
		try{
			for (int i = 0; i < strs.length; i++) {
				String str = strs[i].trim();
				if(!str.equals("")){
					try{
						String[] datas = Util.split(str, ",");
						String value = datas[0];
						int type = Integer.parseInt(datas[1]);
						if(type == CREATE_CLAN){
							value = "c0" + value;
							value += mResources.CLAN_ACTIVITY[1]+" " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == MOVE_OUT_MEM){
							value = "c1" + value;
							value += " " + mResources.CLAN_ACTIVITY[2] + " " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == MOVE_INPUT_MONEY){
							value = "c2" + value;
							value += " "+mResources.CLAN_ACTIVITY[3]+" " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == MOVE_OUT_MONEY){
							value = "c1" + value;
							value += " " + mResources.CLAN_ACTIVITY[4]+ " " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == FREE_MONEY){
							value = "c1" + value;
							value += mResources.CLAN_ACTIVITY[5]+" " + Util.numberToString(datas[2]) + " "+mResources.CLAN_ACTIVITY[0]+" " + datas[3];
						}
						else if(type == UP_LEVEL){
							value = "c2" + value;
							value += " " + mResources.CLAN_ACTIVITY[6] + " " + Util.numberToString(datas[2]) + " " + mResources.CLAN_ACTIVITY[0] + " " + datas[3];
						}
						
						log += value + "\n";
					}catch (Exception e) {}
				}
			}
		}catch (Exception e) {}
	}
}