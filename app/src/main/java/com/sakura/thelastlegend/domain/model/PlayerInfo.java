package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.lib.Image;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;



public class PlayerInfo {
	public String name;
	public String showName;
	public String status;
	public int IDDB;
	private int exp;
//	public Icon avatar=new Icon(1,0);
	public boolean isReady;
	public int xu,gold;
	public String strMoney = "";
	public byte finishPosition;
//	public LevelDetail level = new LevelDetail();
//	public Icon icon=new Icon(2,0);
	public boolean isMaster;
	public static Image[] imgStart;
	public byte[] indexLv;
	public int onlineTime;
//	static {
//		imgStart = new Image[3];
//		FilePack.init(Text.pMain);
//		for (int i = 0; i < 3; i++)
//			imgStart[i] = FilePack.getImg("star" + (i + 1));
//		FilePack.reset();
//	}

	public String getName() {
		return name;
	}

	public void setMoney(int m) {
		xu = m;
		strMoney = GameCanvas.getMoneys(xu);
	}

	public void setName(String name) {
		this.name = name;
		if (name.length() > 9)
			this.showName = name.substring(0, 8);
		else
			this.showName = name;
	}

	public void paint(mGraphics g, int x, int y) {		
//		avatar.paint(g, x, y, Graphics.TOP | Graphics.HCENTER);			
//		if (indexLv != null&&avatar!=null&&avatar.img!=null)
//			for (int i = 0; i < indexLv.length; i++)
//				g.drawImage(imgStart[indexLv[i]], x - 15, y + avatar.img.getHeight() / 2 + i * 6
//						- (indexLv.length - 1) * 6 / 2, Graphics.VCENTER| Graphics.HCENTER);		
	}

//	public void paintName(Graphics g, String name, int x, int y, int anthor,boolean is) {
//		int a = 0;
//		if (icon.id >0) {
//			int w = 0;
//			if(is)
//				w=Font.normalBFont.getWidth(name);
//			else
//				w = Font.normalFont.getWidth(name);
//			if (anthor == 0)
//				w = 0;
//			else if (anthor == 2)
//				w /= 2;			
//			this.icon.paint(g,  x - w - 2, y + 7, Graphics.VCENTER| Graphics.RIGHT);			
//		} else{
//			a = 12;
//			if(anthor==1)
//				a=0;
//		}
//		if(is)
//			Font.normalBFont.drawString(g, name, x - a, y, anthor);
//		else
//			Font.normalFont.drawString(g, name, x - a, y, anthor);
//	}

	

//	public void setExp(int exp) {
//		this.exp = exp;
//		setExp(exp, level);
//		setIconLevel();
//	}
	
//	public void setIconLevel() {		
//		int lv=level.lv-3;
//		if(lv<0)
//			lv=0;		
//		int num=(lv/4)%4;
//		int color=lv/16;			
//		if(num>3)
//			num=3;				
//		if(color>2){
//			num=3;
//			color=2;
//		}
//		num++;
//		if(lv==0)
//			num=0;
//		indexLv=new byte[num];
//		for(int i=0;i<num;i++)
//			indexLv[i]=(byte)color;		
//	}

//	public static LevelDetail setExp(int exp, LevelDetail level) {
//		int lv = 1;
//		int lastExpRemain = exp;
//		while (true) {
//			int upLevelRequire = lv * 100;
//			lastExpRemain = exp;
//			int expRemain = exp - upLevelRequire;
//			if (expRemain >= 0) {
//				lv++;
//				exp = expRemain;
//			} else
//				break;
//		}
//		level.expRemain = lastExpRemain;
//		level.lv = lv;
//		level.expRequire = lv * 100;
//		level.percent = lastExpRemain * 100 / level.expRequire;
//		return level;
//	}

	public int getExp() {
		return exp;
	}
}
