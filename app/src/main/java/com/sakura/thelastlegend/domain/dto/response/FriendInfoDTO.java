package com.sakura.thelastlegend.domain.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class FriendInfoDTO {
    private long id;
    private boolean online;
    private String charName;
    private short charLevel;
    private short idHead;
}
