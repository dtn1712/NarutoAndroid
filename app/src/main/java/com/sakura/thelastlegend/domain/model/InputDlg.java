package com.sakura.thelastlegend.domain.model;


import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.FontSys;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.mResources;


public class InputDlg extends Dialog {
	protected String[] info;
	public TField tfInput;
	int padLeft;

	public InputDlg() {
		padLeft = 40;
		if (GameCanvas.w <= 176)
			padLeft = 10;
		tfInput = new TField();
		tfInput.x = padLeft+10;
		tfInput.y = GameCanvas.h - Screen.ITEM_HEIGHT - 43;

		tfInput.width = GameCanvas.w-2*(padLeft+10);
		tfInput.height = Screen.ITEM_HEIGHT + 2;
		tfInput.isFocus = true;
		right = tfInput.cmdClear;
	}

	public void show(String info, Command ok, int type) {
		tfInput.setText("");
		tfInput.setIputType(type);
		this.info = mFont.tahoma_8b.splitFontArray(info, GameCanvas.w  - (padLeft*2));
		left = new Command(mResources.CLOSE, GameCanvas.gI(), 8882, null);
		center = ok;
		show();

	}

	public void paint(mGraphics g) {
		GameCanvas.paint.paintInputDlg(g, padLeft, GameCanvas.h - 77 - Screen.cmdH, GameCanvas.w - padLeft*2, 69, info);
		tfInput.paint(g);
		super.paint(g);
	}

	public void keyPress(int keyCode) {
		tfInput.keyPressed(keyCode);
		super.keyPress(keyCode);
	}

	public void update() {
		tfInput.update();
		super.update();
	}

	public void show() {
		if(left != null){
			left.x = GameCanvas.w/2 - 160;
			left.y = GameCanvas.h - 26;
		}
		if(center != null){
			center.x = GameCanvas.w/2 - 35;
			center.y = GameCanvas.h - 26;
		}
		
		if(right!= null){
			right.x = GameCanvas.w/2 + 88;
			right.y = GameCanvas.h - 26;
		}
		GameCanvas.currentDialog = this;
	}

	public void hide() {
		GameCanvas.endDlg();
	}

}
