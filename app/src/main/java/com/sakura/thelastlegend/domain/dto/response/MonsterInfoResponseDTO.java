package com.sakura.thelastlegend.domain.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class MonsterInfoResponseDTO {
    private long id;
    private String name;
    private int maxHp;
    private int hp;
}
