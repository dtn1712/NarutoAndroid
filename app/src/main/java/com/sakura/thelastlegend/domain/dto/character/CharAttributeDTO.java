package com.sakura.thelastlegend.domain.dto.character;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CharAttributeDTO {
    private byte id;
    private int value;
}

