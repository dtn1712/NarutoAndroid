package com.sakura.thelastlegend.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CharPositionResponseDTO {
    private long id;
    private short x;
    private short y;
    private String name;
    private int statusPK;
}
