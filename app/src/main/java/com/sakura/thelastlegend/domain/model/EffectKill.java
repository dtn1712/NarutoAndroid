package com.sakura.thelastlegend.domain.model;

import screen.GameScreen;
import Objectgame.Char;
import Objectgame.Mob;

import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.mVector;


public class EffectKill extends MainEffect {
	public static final byte EFF_NORMAL = 0;
	public static final byte EFF_NORMAL_BAYNGANG = 1;
	public static final byte EFF_CUONG_HOA = 2;
	long time;
	//
	public int timeRemove, subType;
	// int hpShow, hpLast;
	int vXTam, vYTam;
	int x1000, y1000, vX1000, vY1000, xEff, yEff;
	int mTamgiac[][];
	int lT_Arc, gocT_Arc;
	static int[][] colorStar = {
			new int[] { 0xfff8e020, 0xfff8b048, 0xffffffff },
			new int[] { 0xff6b8000, 0xffc0ee78, 0xffffffff } };
	int[] colorpaint;
	static int[][] colorBuffMain = {
			new int[] { 0xffb54200, 0xfff8b048, 0xffffffff, 0xffffffff },
			new int[] { 0xfff7de29, 0xfff8b048, 0xffffffff, 0xffffffff },
			new int[] { 0xff90b0f8, 0xfff8b048, 0xffffffff, 0xffffffff },
			new int[] { 0xffb088a8, 0xfff8b048, 0xffffffff, 0xffffffff } };
	int colorBuff, indexColorStar;
	//
	byte timeAddNum;
	// byte typeNext = -1;
	//
	mVector VecEff = new mVector(), VecSubEff = new mVector();

	// one_three
	int[] mpaintone_three = { 4, 3, 2, 1, 0, 7, 6, 5 };
	// Arrow
	int[] mpaintone_Bullet = { 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 23,
			22, 21, 20, 19, 18, 17, 16, 15, 14, 13 };
	int[] mImageBullet = { 0, 0, 2, 1, 1, 2, 0, 0, 2, 1, 1, 2, 0, 0, 2, 1, 1,
			2, 0, 0, 2, 1, 1, 2 };
	int[] mXoayBullet = { 2, 2, 3, 3, 3, 4, 5, 5, 5, 5, 5, 1, 0, 0, 0, 0, 0, 7,
			6, 6, 6, 6, 6, 2 };
	int gocArrow = 0;
	int frameArrow = 0;
	// Crack_earth
	int xline, yline;
	//
	int yFly, ydArchor, yArchor, indexLan;
	MainObject objKill, objBeKillMain;
	Char charbeAttack;
	public mVector vecObjBeKill;
	boolean isEff = false;

	public EffectKill(int typeKill, Mob mobAttack,
			Char charbeAttack,
			int subtype) {
		this.subType = subtype;
		set_New_Effect(typeKill, mobAttack, charbeAttack);
	}
	public EffectKill(int typeKill, int x, int y,int xTo,int yTo)
	{
		this.typeEffect = typeKill;
		switch (this.typeEffect) {
		case EFF_CUONG_HOA:
			fraImgEff = new FrameImage(104, 11, 11);
			this.x = x;
			this.y = y;
			this.toX = xTo;
			this.toY = yTo;

			int dx = toX - x, dy = toY - y;
			gocT_Arc = CRes.angle(dx, dy);
			
			vMax = 4 * 1000;
			frame = CRes.random(0, 3);
			break;

		default:
			break;
		}
	}
	private void set_New_Effect(int typeKill, Mob mobAttack,
			Char charbeAttack) {
		// try {
		this.typeEffect = typeKill;
		switch (typeKill) {
		case EFF_NORMAL:
		case EFF_NORMAL_BAYNGANG:
			fRemove = 60;
			MainObject mob = new MainObject();
			mob.x = mobAttack.x;
			mob.y = mobAttack.y;
			mob.dir = mobAttack.dir;
			mob.hOne = mobAttack.getH();
			
			this.charbeAttack = charbeAttack;
			MainObject charr = new MainObject();
			this.objBeKillMain = charr;
			this.objBeKillMain.x = charbeAttack.cx;
			this.objBeKillMain.y = charbeAttack.cy;
			this.objBeKillMain.dir = charbeAttack.cdir;
			this.objBeKillMain.hOne = 60; 
			this.objKill = mob;
			this.x = mobAttack.x;
			this.y = mobAttack.y-mobAttack.getH()/2-5;
			switch (subType) {
			case 0:
				fraImgEff = new FrameImage(101, 14, 14);
				break;
			case 1:
				fraImgEff = new FrameImage(98, 14, 14);
				break;
			case 2:
				fraImgEff = new FrameImage(100, 14, 14);
				break;
			case 3:
				fraImgEff = new FrameImage(99, 14, 14);
				break;
			case 4:
				fraImgEff = new FrameImage(102, 14, 14);
				break;
			case 5:
				fraImgEff = new FrameImage(32, 14, 14);
				break;
			default:
				fraImgEff = new FrameImage(32, 14, 14);
				break;
			}
			vMax = 8 * 1000;
			createNormal();
			this.y-=10;
			if(typeEffect==EFF_NORMAL_BAYNGANG){
				gocT_Arc = (objKill.x-objBeKillMain.x)>=0?180:0;
				this.xold = x;
				this.yold = mobAttack.y-mobAttack.h/2-5;
				this.xold1 = objBeKillMain.x;
				this.yold1 = objBeKillMain.y-objBeKillMain.hOne/2;
				this.x +=CRes.xetVX(gocT_Arc, 6);
				this.y +=CRes.xetVY(gocT_Arc, 6);
			}
			break;

		}
	}
	public int xold,yold;
	public int xold1,yold1;

	public void paint(mGraphics g) {
		try {
			if (isRemove)
				return;
			switch (typeEffect) {
			case EFF_NORMAL:	
			case EFF_NORMAL_BAYNGANG:
			
				if (fraImgEff == null)
					return;
				fraImgEff.drawFrame(f / 2 % fraImgEff.nFrame, x, y, 0,
						mGraphics.VCENTER | mGraphics.HCENTER, g);
				break;
			case EFF_CUONG_HOA:
				if (fraImgEff == null)
					return;
				fraImgEff.drawFrame(frame % fraImgEff.nFrame, x, y, 0,
						mGraphics.VCENTER | mGraphics.HCENTER, g);
				break;

			}
		} catch (Exception e) {
			removeEff();
		}
	}

	public void update() {
		if (isRemove)
			return;
		f++;
		// try {
		// mSystem.outz("update Eff " + typeEffect);
		if (objBeKillMain != null) {
			xEff = objBeKillMain.x;
			yEff = objBeKillMain.y - objBeKillMain.hOne / 2;
		}
		boolean isRe = true;
		switch (typeEffect) {
		case EFF_NORMAL:
			updateAngleNormal();
			break;
		case EFF_NORMAL_BAYNGANG:
			
			x += CRes.xetVX(gocT_Arc, 6);
			y += CRes.xetVY(gocT_Arc, 6);
			if(CRes.abs(objBeKillMain.x-x)<8||f>fRemove){
				ServerEffect.addServerEffect(25, x, y, 1);
				//charbeAttack.doInjure(0, 0,false,1 );
				charbeAttack.doInjure(1, 0,false,1 );
//				((Char)charbeAttack).statusMe = Char.A_INJURE;
				//GameScreen.addEffectEnd(EffectEnd.END_NORMAL, objBeKillMain!=null?objBeKillMain.x:x,y);
				removeEff();
			}
			break;
		case EFF_CUONG_HOA:
			int dx = toX - x, dy = toY - y;
			if ((CRes.abs(dx) < 16 && CRes.abs(dy) < 16))
			{
//				Cout.println2222("remove EFF_CUONG_HOA ");
				removeEff();
				return ;
			}
			int a = CRes.angle(dx, dy);
			if (Math.abs(a - gocT_Arc) < 90 || dx * dx + dy * dy > 64 * 64) {
				if (Math.abs(a - gocT_Arc) < 15)
					gocT_Arc = a;
				else if (a - gocT_Arc >= 0 && a - gocT_Arc < 180
						|| a - gocT_Arc < -180)
					gocT_Arc = CRes.fixangle(gocT_Arc + 15);
				else
					gocT_Arc = CRes.fixangle(gocT_Arc - 15);
			}
			if (!isSpeedUp)
				if (va < 8 << 10)
					va += 2048;
			vX1000 = (va * CRes.cos(gocT_Arc)) >> 10;
			vY1000 = (va * CRes.sin(gocT_Arc)) >> 10;
			//
			dx += vX1000;
			int deltaX = dx >> 10;
			x += deltaX;
			dx = dx & 0x3ff;
			dy += vY1000;
			int deltaY = dy >> 10;
			y += deltaY;
			dy = dy & 0x3ff;
			break;
		}
		// } catch (Exception e) {
		// mSystem.outloi("Loi update Eff " + typeEffect);
		// e.printStackTrace();
		// removeEff();
		// }
		super.update();
		if (f > 200) {
			removeEff();
		}
	}

	public void endUpdate() {
	}

	int life = 0, va;
	public boolean isSpeedUp = false;

	public static int setDirection(MainObject idFrom, MainObject idTo) {
		int dir;
		int xdich = idFrom.x - idTo.x;
		int ydich = idFrom.y - idTo.y;
		if (CRes.abs(xdich) > CRes.abs(ydich)) {
			if (xdich > 0)
				dir = DIR_LEFT;
			else
				dir = DIR_RIGHT;
		} else {
			if (ydich > 0)
				dir = DIR_UP;
			else
				dir = DIR_DOWN;
		}
		return dir;
	}

	public int setDirection(int dx, int dy) {
		int dir;
		if (CRes.abs(dx) > CRes.abs(dy)) {
			if (dx > 0)
				dir = DIR_LEFT;
			else
				dir = DIR_RIGHT;
		} else {
			if (dy > 0)
				dir = DIR_UP;
			else
				dir = DIR_DOWN;
		}
		return dir;
	}



	public int setFrameAngle(int goc) {
		int fra;
		if (goc <= 15 || goc > 345)
			fra = 12;
		else {
			int t = (goc - 15) / 15 + 1;
			if (t > 24)
				t = 24;
			fra = mpaintone_Bullet[t];
		}
		return fra;
	}


	public void removeEff() {
		GameScreen.veffClient.remove(this);
		if (VecEff.size() > 0)
			VecEff.removeAllElements();
		if (VecSubEff.size() > 0)
			VecSubEff.removeAllElements();
	}


	// ////////////// create skill

	public void createNormal() {
		if (objKill == null) {
			gocT_Arc = 0;
		} else {
			switch (objKill.dir) {
			case 0:// d
				gocT_Arc = 90;
				break;
			case 1:// u
				gocT_Arc = 270;
				break;
			case 2:// l
				gocT_Arc = 180;
				break;
			case 3:// r
				gocT_Arc = 0;
				break;
			}
			if(CRes.abs(objBeKillMain.y-objKill.y)<=28){
				typeEffect = EFF_NORMAL_BAYNGANG;
			}
		}
		va = (short) (256 * 16);
		vx = 0;
		vy = 0;
		life = 0;
		vX1000 = (va * CRes.cos(gocT_Arc)) >> 10;
		vY1000 = (va * CRes.sin(gocT_Arc)) >> 10;
	}


	// / update more
	public void updateAngleNormal() {
		int dx, dy;
		int a;
		if (charbeAttack == null) {
			removeEff();
			return;
		}
		dx = (charbeAttack.cx - x);
		dy = (charbeAttack.cy - (objBeKillMain.hOne >> 1) - y+objKill.hOne/2);
		life++;
		if ((CRes.abs(dx) < 16 && CRes.abs(dy) < 16) || life > fRemove) {

//			((Char)charbeAttack).statusMe = Char.A_INJURE;
			charbeAttack.doInjure(1, 0,false,1 );
			ServerEffect.addServerEffect(25, x, y, 1);
				//ServerEffect.addServerEffect(25, objBeKillMain.x, objBeKillMain.y, 1);
				//GameScreen.addEffectEnd(EffectEnd.END_NORMAL, objBeKillMain!=null?objBeKillMain.x:x,y);
				removeEff();
			return;
		}
		a = CRes.angle(dx, dy);
		if (Math.abs(a - gocT_Arc) < 90 || dx * dx + dy * dy > 64 * 64) {
			if (Math.abs(a - gocT_Arc) < 15)
				gocT_Arc = a;
			else if (a - gocT_Arc >= 0 && a - gocT_Arc < 180
					|| a - gocT_Arc < -180)
				gocT_Arc = CRes.fixangle(gocT_Arc + 15);
			else
				gocT_Arc = CRes.fixangle(gocT_Arc - 15);
		}
		if (!isSpeedUp)
			if (va < 8 << 10)
				va += 2048;
		vX1000 = (va * CRes.cos(gocT_Arc)) >> 10;
		vY1000 = (va * CRes.sin(gocT_Arc)) >> 10;
		//
		dx += vX1000;
		int deltaX = dx >> 10;
		x += deltaX;
		dx = dx & 0x3ff;
		dy += vY1000;
		int deltaY = dy >> 10;
		y += deltaY;
		dy = dy & 0x3ff;
	}



	public void setSendMove(int x, int xend, int y, int yend) {
		
	}

}
