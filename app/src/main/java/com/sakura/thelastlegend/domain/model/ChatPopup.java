package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Char;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.mFont;
import screen.GameScreen;

public class ChatPopup extends Effect2 implements IActionListener {
	public int sayWidth = 100, delay, sayRun;
	public String[] says;
	int cx, cy, ch;
	public Char c;
	boolean outSide = false;

	// =====================
	int currentLine;
	String[] lines;
	public Command cmdNextLine;
	public static ChatPopup currentMultilineChatPopup;

	public static void addChatPopupMultiLine(String chat, int howLong, Char c) {
		String[] lines = Res.split(chat, "\n", 0);
		if (lines.length == 1) {
			addChatPopup(lines[0], howLong, c);
			return;
		}
		currentMultilineChatPopup = addChatPopup(lines[0], howLong, c);
		currentMultilineChatPopup.currentLine = 0;
		currentMultilineChatPopup.lines = lines;
		currentMultilineChatPopup.cmdNextLine = new Command(mResources.NEXT, currentMultilineChatPopup,8000,null);
	}

	public static ChatPopup addChatPopupOutSide(String chat, int howLong, Char c) {
		ChatPopup cp = new ChatPopup();
		if (chat.length() < 10)
			cp.sayWidth = 64;
		if (GameCanvas.w == 128)
			cp.sayWidth = 128;
		cp.says = mFont.tahoma_7_red.splitFontArray(chat, cp.sayWidth - 10);
		cp.delay = howLong;
		cp.c = c;
		cp.cx = c.cx;
		cp.cy = c.cy;
		c.chatPopup = cp;
		cp.sayRun = 7;
		cp.outSide = true;
		if (c != null) {
			cp.cx = c.cx;
			cp.cy = c.cy;
			cp.ch = c.ch -15;
		}
		vEffect2Outside.addElement(cp);
		return cp;
	}

	public static ChatPopup addChatPopup(String chat, int howLong, Char c) {
		ChatPopup cp = new ChatPopup();
		if (chat.length() < 10)
			cp.sayWidth = 64;
		if (GameCanvas.w == 128)
			cp.sayWidth = 128;
		cp.says = mFont.tahoma_7_red.splitFontArray(chat, cp.sayWidth - 10);
		cp.delay = howLong;
		cp.c = c;
		cp.cx = c.cx;
		cp.cy = c.cy;
		c.chatPopup = cp;
		cp.sayRun = 7;
		if (c != null) {
			cp.cx = c.cx;
			cp.cy = c.cy;
			cp.ch = c.ch -15;
		}
		vEffect2.addElement(cp);
		return cp;
	}

	public static void addChatPopup(String chat, int howLong, int x, int y) {
		ChatPopup cp = new ChatPopup();
		if (chat.length() < 10)
			cp.sayWidth = 60;
		if (GameCanvas.w == 128)
			cp.sayWidth = 128;
		cp.says = mFont.tahoma_7_red.splitFontArray(chat, cp.sayWidth - 10);
		cp.delay = howLong;
		cp.c = null;
		cp.cx = x;
		cp.cy = y;
		cp.sayRun = 7;
		vEffect2.addElement(cp);
	}

	public void update() {
		if (delay > 0) {
			delay--;
		}
		if (c != null) {
			cx = c.cx;
			cy = c.cy;
			ch = c.ch - 10;
		}
		if (sayRun > 1)
			sayRun--;

		if ((c != null && c.chatPopup != null && c.chatPopup != this) || (c != null && c.chatPopup == null) || delay == 0) {
			vEffect2Outside.removeElement(this);
			vEffect2.removeElement(this);
		}
	}

	public void paint(mGraphics g) {
		int cx = this.cx;
		int cy = this.cy;
		if (outSide) {
			cx -= GameScreen.cmx;
			cy -= GameScreen.cmy;
			cy += 35;
		}
		g.setColor(0xff000000);
		g.fillRect(cx - sayWidth / 2+2 , cy - ch - 15 + sayRun - says.length * 12 - 10, sayWidth-4 , (says.length + 1) * 12 - 1+2);
		g.fillRect(cx - sayWidth / 2-1 , cy - ch - 15 + sayRun - says.length * 12 -7, sayWidth+2 , (says.length + 1) * 12 - 5);
		
		g.setColor(0xffFFFFFF);
		g.fillRect(cx - sayWidth / 2 , cy - ch - 15 + sayRun - says.length * 12 - 9, sayWidth , (says.length + 1) * 12 - 1);
//		g.setColor(0xff000000);
//		g.drawLine(cx - sayWidth / 2+1 , cy - ch - 15 + sayRun - says.length * 12 - 10,
//		cx - sayWidth / 2+1+ sayWidth -2,cy - ch - 15 + sayRun - says.length * 12 - 10,true);
//		g.drawLine(cx - sayWidth / 2 , cy - ch - 15 + sayRun - says.length * 12 - 9+1,
//				cx - sayWidth / 2,cy - ch - 15 + sayRun - says.length * 12 - 9+ (says.length + 1) * 12 - 3,true);
//		g.drawLine(cx - sayWidth / 2+1 , cy - ch - 15 + sayRun - says.length * 12 - 9+(says.length + 1) * 12 - 1,
//				   cx - sayWidth / 2+1 + sayWidth-2 ,cy - ch - 15 + sayRun - says.length * 12 - 9+ (says.length + 1) * 12 - 1,true);
//		g.drawLine(cx - sayWidth / 2+3+ sayWidth-2  , cy - ch - 15 + sayRun - says.length * 12 - 8, 
//				cx - sayWidth / 2+3 + sayWidth-2 , cy - ch - 15 + sayRun - says.length * 12 - 11+ (says.length + 1) * 12 - 1,true);
		
//		g.drawRoundRect(cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.length * 12 - 10, sayWidth + 1, (says.length + 1) * 12, 12, 12);
//		SmallImage.drawSmallImage(g, 941, cx - 3, cy - ch - 15 + sayRun + 2, 0, mGraphics.TOP | mGraphics.HCENTER);
		
		
		g.drawImage(LoadImageInterface.imgGocChat,cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.length * 12 - 10, mGraphics.TOP|mGraphics.LEFT,true);
		g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.getWidth(), LoadImageInterface.imgGocChat.getHeight(), 2,
				cx - sayWidth / 2 - 1+sayWidth + 2- LoadImageInterface.imgGocChat.getWidth(), cy - ch - 15 + sayRun - says.length * 12 - 10, mGraphics.TOP|mGraphics.LEFT, true);
		g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.getWidth(), LoadImageInterface.imgGocChat.getHeight(), 6,
				cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.length * 12 - 10+(says.length + 1) * 12- LoadImageInterface.imgGocChat.getHeight(), mGraphics.TOP|mGraphics.LEFT, true);
		g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.getWidth(), LoadImageInterface.imgGocChat.getHeight(), 3,
				cx - sayWidth / 2 - 1+sayWidth + 2- LoadImageInterface.imgGocChat.getWidth(), cy - ch - 15 + sayRun - says.length * 12 - 10+(says.length + 1) * 12- LoadImageInterface.imgGocChat.getHeight()+1, mGraphics.TOP|mGraphics.LEFT, true);
		
		g.drawImage(LoadImageInterface.imgGoc,  cx - sayWidth/4, cy - ch - 15 + sayRun+ LoadImageInterface.imgGoc.getHeight()/2-1, mGraphics.TOP|mGraphics.LEFT,true);
		for (int i = 0; i < says.length; i++) {
			mFont.tahoma_7.drawString(g, says[i], cx, cy - ch - 15 + sayRun + i * 12 - says.length * 12 - 4, 2);
		}
	}

	public void updateKey() {
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(GameCanvas.currentScreen.center)) {
			GameCanvas.keyPressed[5] = false;
			Screen.keyTouch = -1;
			cmdNextLine.performAction();
		}
	}

	public void paintCmd(mGraphics g) {
		g.translate(-g.getTranslateX(), -g.getTranslateY());
		g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
		GameCanvas.paint.paintTabSoft(g);
		GameCanvas.paint.paintCmdBar(g, null, cmdNextLine, null);

	}

	public void perform(int idAction, Object p) {
		if(idAction==8000)
		{
			int currentLine = currentMultilineChatPopup.currentLine;
			currentLine++;
			if (currentLine >= currentMultilineChatPopup.lines.length) {
				currentMultilineChatPopup.c.chatPopup = null;
				currentMultilineChatPopup = null;
				return; // END
			}
			ChatPopup cp = addChatPopup(currentMultilineChatPopup.lines[currentLine], currentMultilineChatPopup.delay, currentMultilineChatPopup.c);
			cp.currentLine = currentLine;
			cp.lines = currentMultilineChatPopup.lines;
			cp.cmdNextLine = currentMultilineChatPopup.cmdNextLine;
			currentMultilineChatPopup = cp;
		}
		
	}

	public void perform() {
		// TODO Auto-generated method stub
		
	}

	
}