package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mBitmap;

public class TiledLayer extends Layer{

	private int cellHeight; // = 0;

    private int cellWidth; // = 0;

    private int rows; // = 0;

    private int columns; // = 0;

    private int[][] cellMatrix; // = null;

    mBitmap sourceImage; // = null;

    private int numberOfTiles; // = 0;

    int[] tileSetX;

    int[] tileSetY;
    
    private int[] anim_to_static; // = null;

    private int numOfAnimTiles; // = 0
    
    private Paint mPaint;
    private Rect mSrcRect;
    private Rect mDestRect;

    public TiledLayer(int columns, int rows, mBitmap image, int tileWidth, int tileHeight) {
        super(columns < 1 || tileWidth < 1 ? -1 : columns * tileWidth, rows < 1 || tileHeight < 1 ? -1 : rows * tileHeight);

        mPaint = new Paint();
        mSrcRect = new Rect();
        mDestRect = new Rect();

        if (((Image.getWidth(image) % tileWidth) != 0) || ((Image.getHeight(image) % tileHeight) != 0)) {
             throw new IllegalArgumentException();
        }
        this.columns = columns;
        this.rows = rows;

        cellMatrix = new int[rows][columns];

        int noOfFrames = (Image.getWidth(image) / tileWidth) * (Image.getHeight(image) / tileHeight);
        createStaticSet(image,  noOfFrames + 1, tileWidth, tileHeight, true);        
    }

    public int createAnimatedTile(int staticTileIndex) {
        // checks static tile 
        if (staticTileIndex < 0 || staticTileIndex >= numberOfTiles) { 
            throw new IndexOutOfBoundsException();
        }

        if (anim_to_static == null) {
            anim_to_static = new int[4];
            numOfAnimTiles = 1;
        } else if (numOfAnimTiles == anim_to_static.length) {
            // grow anim_to_static table if needed 
            int new_anim_tbl[] = new int[anim_to_static.length * 2];
            System.arraycopy(anim_to_static, 0, new_anim_tbl, 0, anim_to_static.length);
            anim_to_static = new_anim_tbl;
        }
        anim_to_static[numOfAnimTiles] = staticTileIndex;
        numOfAnimTiles++;
        
        return (-(numOfAnimTiles - 1));
    }

    public void setAnimatedTile(int animatedTileIndex, int staticTileIndex) {
        // checks static tile
        if (staticTileIndex < 0 || staticTileIndex >= numberOfTiles) {  
            throw new IndexOutOfBoundsException();
        }
        // do animated tile index check
        animatedTileIndex = - animatedTileIndex;
        if (anim_to_static == null || animatedTileIndex <= 0 
            || animatedTileIndex >= numOfAnimTiles) { 
            throw new IndexOutOfBoundsException();
        }
        anim_to_static[animatedTileIndex] = staticTileIndex;
    }


    public int getAnimatedTile(int animatedTileIndex) {
        animatedTileIndex = - animatedTileIndex;
        if (anim_to_static == null || animatedTileIndex <= 0 || animatedTileIndex >= numOfAnimTiles) { 
            throw new IndexOutOfBoundsException();
        }
    
        return anim_to_static[animatedTileIndex];
    }

    public void setCell(int col, int row, int tileIndex) {

        if (col < 0 || col >= this.columns || row < 0 || row >= this.rows) {
            throw new IndexOutOfBoundsException();
        }

    if (tileIndex > 0) {
            // do checks for static tile 
            if (tileIndex >= numberOfTiles) { 
            throw new IndexOutOfBoundsException();
        }
    } else if (tileIndex < 0) {
            // do animated tile index check
        if (anim_to_static == null ||
                (-tileIndex) >= numOfAnimTiles) { 
            throw new IndexOutOfBoundsException();
            }
    }

        cellMatrix[row][col] = tileIndex;
 
    }

    public int getCell(int col, int row) {
        if (col < 0 || col >= this.columns || row < 0 || row >= this.rows) {
            throw new IndexOutOfBoundsException();
        }
        return cellMatrix[row][col];
    }

    public void fillCells(int col, int row, int numCols, int numRows,
                          int tileIndex) {


    if (numCols < 0 || numRows < 0) {
            throw new IllegalArgumentException();
    }

        if (col < 0 || col >= this.columns || row < 0 || row >= this.rows ||
        col + numCols > this.columns || row + numRows > this.rows) {
            throw new IndexOutOfBoundsException();
        }

    if (tileIndex > 0) {
            // do checks for static tile 
            if (tileIndex >= numberOfTiles) { 
            throw new IndexOutOfBoundsException();
        }
    } else if (tileIndex < 0) {
            // do animated tile index check
        if (anim_to_static == null || 
                (-tileIndex) >= numOfAnimTiles) { 
                throw new IndexOutOfBoundsException();
            }
    }

        for (int rowCount = row; rowCount < row + numRows; rowCount++) {
            for (int columnCount = col; 
                     columnCount < col + numCols; columnCount++) {
                cellMatrix[rowCount][columnCount] = tileIndex;
            }
        }
    }


    public final int getCellWidth() {
        return cellWidth;
    }

    public final int getCellHeight() {
        return cellHeight;
    }

    public final int getColumns() {
        return columns;
    }

    public final int getRows() {
        return rows;
    }

    public void setStaticTileSet(mBitmap image, int tileWidth, int tileHeight) {

        // if img is null img.getWidth() will throw NullPointerException
        if (tileWidth < 1 || tileHeight < 1 ||
        ((Image.getWidth(image) % tileWidth) != 0) || ((Image.getHeight(image) % tileHeight) != 0)) {
             throw new IllegalArgumentException();
        }
        setWidthImpl(columns * tileWidth);
        setHeightImpl(rows * tileHeight);

        int noOfFrames = (Image.getWidth(image) / tileWidth) * (Image.getHeight(image) / tileHeight);

        // the zero th index is left empty for transparent tile
        // so it is passed in  createStaticSet as noOfFrames + 1

		if (noOfFrames >= (numberOfTiles - 1)) {
			// maintain static indices
        createStaticSet(image, noOfFrames + 1, tileWidth, tileHeight, true);
		} else {
            createStaticSet(image, noOfFrames + 1, tileWidth, tileHeight, false);
		}
	}

  public final void paint() {
	  
  }

  private void createStaticSet(mBitmap image, int noOfFrames, int tileWidth, 
                      int tileHeight, boolean maintainIndices) {

        cellWidth = tileWidth;
        cellHeight = tileHeight;

    int imageW = Image.getWidth(image);
    int imageH = Image.getHeight(image);

    sourceImage = image;

    numberOfTiles = noOfFrames;
    tileSetX = new int[numberOfTiles];
    tileSetY = new int[numberOfTiles];
    
    if (!maintainIndices) {
            // populate cell matrix, all the indices are 0 to begin with
            for (rows = 0; rows < cellMatrix.length; rows++) {
                int totalCols = cellMatrix[rows].length;
                for (columns = 0; columns < totalCols; columns++) {
                    cellMatrix[rows][columns] = 0;
                }
            }
        // delete animated tiles
        anim_to_static = null;
    } 

        int currentTile = 1;

        for (int locY = 0; locY < imageH; locY += tileHeight) {
            for (int locX = 0; locX < imageW; locX += tileWidth) {

        tileSetX[currentTile] = locX;
        tileSetY[currentTile] = locY;

                currentTile++;
            }
        }
    }

}
