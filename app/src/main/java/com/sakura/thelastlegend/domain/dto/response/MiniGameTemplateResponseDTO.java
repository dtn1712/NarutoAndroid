package com.sakura.thelastlegend.domain.dto.response;

import com.sakura.thelastlegend.domain.dto.minigame.MiniGameTemplateDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class MiniGameTemplateResponseDTO {

    List<MiniGameTemplateDTO> miniGameTemplates;
}
