package com.sakura.thelastlegend.domain.model;



import com.sakura.thelastlegend.mGraphics;

import Objectgame.Char;
import com.sakura.thelastlegend.domain.model.Res;

public class Arrow {
	public int life = 0, ax, ay, axTo, ayTo, avx, avy, adx, ady;
	public Char charBelong;
	public Arrowpaint arrp = null;

	public Arrow(Char charBelong, Arrowpaint arrp){
		this.charBelong = charBelong;
		this.arrp = arrp;
	}
	
	public void update() {
		if (charBelong.mobFocus == null && charBelong.charFocus == null) {
			endMe();
		} else {
			if (charBelong.mobFocus != null) {
				axTo = charBelong.mobFocus.x;
				ayTo = charBelong.mobFocus.y - charBelong.mobFocus.h / 4;
			} else if (charBelong.charFocus != null) {
				axTo = charBelong.charFocus.cx;
				ayTo = charBelong.charFocus.cy - charBelong.charFocus.ch / 4;
			}
			int dx = axTo - ax;
			int dy = ayTo - ay;
			int space = 5;
			int sp = 4;
			if(dx+dy<60)sp=3;
			else if(dx+dy<30)sp=2;
			if (ax != axTo) {
				if (dx > 0 && dx < space)
					ax = axTo;
				else if (dx < 0 && dx > -space)
					ax = axTo;
				else {
					avx = (axTo - ax) << 2;
					adx += avx;
					ax += adx >> sp;
					adx = adx & 0xf;
				}
			}
			if (ay != ayTo) {
				if (dy > 0 && dy < space)
					ay = ayTo;
				else if (dy < 0 && dy > -space)
					ay = ayTo;
				else {
					avy = (ayTo - ay) << 2;
					ady += avy;
					ay += ady >> sp;
					ady = ady & 0xf;
				}
			}
			int minx=0,miny=0,maxx=0,maxy=0;
			if (charBelong.mobFocus != null) {
				minx = axTo - charBelong.mobFocus.w / 4;
				maxx = axTo + charBelong.mobFocus.w / 4;
				miny = ayTo - charBelong.mobFocus.h / 4;
				maxy = ayTo + charBelong.mobFocus.h / 4;
			}
			else if (charBelong.charFocus != null) {
				minx = axTo - charBelong.charFocus.cw / 4;
				maxx = axTo + charBelong.charFocus.cw / 4;
				miny = ayTo - charBelong.charFocus.ch / 4;
				maxy = ayTo + charBelong.charFocus.ch / 4;
			}
			if (life > 0)
				life--;
			if ((life == 0) || (ax >= minx && ax <= maxx && ay >= miny && ay <= maxy)) {
				endMe();
			}
		}
	}

	private void endMe() {
		charBelong.arr = null;
		ax = ay = axTo = ayTo = avx = avy = adx = ady = 0;
		charBelong.setAttack();
		if(charBelong.me)charBelong.saveLoadPreviousSkill();
	}

	public void paint(mGraphics g) {
		int dx = axTo - ax;
		int dy = ayTo - ay;
		int d = findDirIndexFromAngle(Res.angle(dx, -dy));
		SmallImage.drawSmallImage(g, arrp.imgId[FRAME[d]], ax, ay, TRANSFORM[d], mGraphics.VCENTER | mGraphics.HCENTER);

	}

	public static final byte FRAME[] = { 0, 1, 2, 1, 0, 1, 2, 1, 0, 1, 2, 1, 0, 1, 2, 1, 0, 1, 2, 1, 0, 1, 2, 1, 0 };
	public static final int ARROWINDEX[] = { 0, 30 / 2, (45 + 30) / 2, (60 + 45) / 2, (90 + 60) / 2, (120 + 90) / 2, (135 + 120) / 2, (150 + 135) / 2, (180 + 150) / 2, (210 + 180) / 2, (225 + 210) / 2, (240 + 225) / 2, (270 + 240) / 2, (300 + 270) / 2, (315 + 300) / 2, (330 + 315) / 2, (360 + 330) / 2, 370 };
	public static final int TRANSFORM[] = { 0, 0, 0, Sprite.TRANS_MIRROR_ROT90,//
			Sprite.TRANS_ROT270, Sprite.TRANS_ROT270, Sprite.TRANS_ROT270, Sprite.TRANS_MIRROR, Sprite.TRANS_MIRROR, Sprite.TRANS_ROT180, Sprite.TRANS_ROT180, Sprite.TRANS_MIRROR_ROT270, Sprite.TRANS_ROT90, Sprite.TRANS_ROT90, Sprite.TRANS_ROT90, Sprite.TRANS_MIRROR_ROT180, };

	public static int findDirIndexFromAngle(int angle) {
		for (int i = 0; i < ARROWINDEX.length - 1; i++) {
			if (angle >= ARROWINDEX[i] && angle <= ARROWINDEX[i + 1]) {
				if (i >= 16)
					return 0;
				return i;
			}
		}
		return 0;
	}
}
