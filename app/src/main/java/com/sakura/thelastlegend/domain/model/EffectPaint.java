package com.sakura.thelastlegend.domain.model;

import Objectgame.Char;
import Objectgame.Mob;

public class EffectPaint {

	public int index;
	public Mob eMob;
	public Char eChar;
	public EffectCharPaint effCharPaint; 
	public boolean isFly;
	
	public int getImgId(){
		return effCharPaint.arrEfInfo[index].idImg;
	}
}