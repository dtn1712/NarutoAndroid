package com.sakura.thelastlegend.domain.model;

public class EffectTemplate {

	public byte id, type;
	public int iconId;
	public String name;
}
