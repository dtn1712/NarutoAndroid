package com.sakura.thelastlegend.domain.dto.response;

import com.sakura.thelastlegend.domain.dto.minigame.BombZoneDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BombZoneResponseDTO {
    List<BombZoneDTO> bombZones;
}
