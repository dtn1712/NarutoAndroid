package com.sakura.thelastlegend.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ChatResponseDTO {

    private byte type;
    private long id;
    private String playerSendName;
    private long playerReceiveId;
    private String playerReceiveName;
    private String text;
}
