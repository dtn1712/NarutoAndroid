package com.sakura.thelastlegend.domain.dto.character;

import com.sakura.thelastlegend.domain.dto.game.ItemTypeDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CharConfigDTO {

    private ItemTypeDTO[] equipmentPositions;

}
