package com.sakura.thelastlegend.domain.dto.minigame;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MiniGameTemplateDTO {

    private short id;
    private String name;
    private short totalPlayers;
    private short groupPlayers;
    private String gameType;
    private String description;
    private byte[] iconImage;
    private String feeType;
    private int feeAmount;
}
