package com.sakura.thelastlegend.domain.model;

public class Position {
	public int x, y;
	public int g;
	public int v;
	public int w;
	public int h;
	public int color = 0;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}


}
