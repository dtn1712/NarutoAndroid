package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.mGraphics;



public class StaticObj {
	public static int TOP_CENTER = mGraphics.TOP | mGraphics.HCENTER;
	public static int TOP_LEFT = mGraphics.TOP | mGraphics.LEFT;
	public static int TOP_RIGHT = mGraphics.TOP | mGraphics.RIGHT;
	public static int BOTTOM_HCENTER = mGraphics.BOTTOM | mGraphics.HCENTER;
	public static int BOTTOM_LEFT = mGraphics.BOTTOM | mGraphics.LEFT;
	public static int BOTTOM_RIGHT = mGraphics.BOTTOM | mGraphics.RIGHT;
	public static int VCENTER_HCENTER = mGraphics.VCENTER | mGraphics.HCENTER;
	public static int VCENTER_LEFT = mGraphics.VCENTER | mGraphics.LEFT;

	// =================
	public static final String SAVE_SKILL = "skill";
	public static final String SAVE_VERSIONUPDATE = "versionUpdate";
	public static final String SAVE_KEYKILL = "keyskill";
	public static final String SAVE_ITEM = "item";
	
	// ====================
	public static final int NORMAL = 0;
	public static final int UP_FALL = 1;
	public static final int UP_RUN = 2;
	public static final int FALL_RIGHT = 3;
	public static final int FALL_LEFT = 4;// KO BI THUONG
	public static final int MOD_ATTACK_ME = 100;

	// =============com.sakura.thelastlegend.domain.model obj
	// public static final int TYPE_MOD = 1;
	// public static final int TYPE_ITEM = 2;
	public static final int TYPE_PLAYER = 3;
	// ==============com.sakura.thelastlegend.domain.model obj

	// ========item
	public static final int TYPE_NON = 0;
	public static final int TYPE_VUKHI = 1;
	public static final int TYPE_AO = 2;
	public static final int TYPE_LIEN = 3;
	public static final int TYPE_TAY = 4;
	public static final int TYPE_NHAN = 5;
	public static final int TYPE_QUAN = 6;
	public static final int TYPE_BOI = 7;
	public static final int TYPE_GIAY = 8;
	public static final int TYPE_PHU = 9;
	public static final int TYPE_OTHER = 11;
	public static final int TYPE_CRYSTAL = 15;
	// ==============

	// ===============set focus
	public static final int FOCUS_MOD = 1;
	public static final int FOCUS_ITEM = 2;
	public static final int FOCUS_PLAYER = 3;
	public static final int FOCUS_ZONE = 4;
	public static final int FOCUS_NPC = 5;

	// ==============mang type bg

	public static final int TYPEBG[][] = { { 0, 0, 0, 0}, { 1, 1, 1, -1 },
			{ 2, 2, 2, 2 }, { 2, 2, 2, -1 }, { 3, 3, 3, 3 }, { 4, -1, -1, 4 },
			{ 5, 5, 5, -1 }, { 6, 6, 6, 5 }, { 7, 7, -1, -1 }, { 8, 8, 8, 7 },
			{ 9, -1, -1, 8 }, { 10, -1, -1, 9 }, { 11, -1, -1, -1 } };
	
	public static final int SKYCOLOR[] = { 0x55aaee, 0x4880f8, 0x101010,
			0x1fc3f5, 0x1fc3f5, 0, 0xa0d8f8, 0x268dc6, 0x171808, 0xFFBFBC, 0,
			0x139CAA, 0x139CAA };

}
