package com.sakura.thelastlegend.domain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class LoginRequestDTO {
    private String username;
    private String password;
    private byte zoomLevel;
    private String clientVersion;
}