package com.sakura.thelastlegend.domain.dto.character;

import com.sakura.thelastlegend.domain.dto.game.ItemAttributeDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CharInventoryDTO {

    private long gold;
    private long xu;
    private int inventorySize;
    private List<CharInventoryItemDTO> items;


    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @SuppressWarnings("PMD.UnusedPrivateField")
    public static class CharInventoryItemDTO {
        private int positionIndex;
        private short itemTemplateId;
        private short levelUpgrade;
        private int quantity;
        private int valueBuff;
        private ItemAttributeDTO addedAttribute;
    }
}
