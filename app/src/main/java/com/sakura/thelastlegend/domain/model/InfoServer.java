package com.sakura.thelastlegend.domain.model;


import screen.GameScreen;
import Objectgame.NodeChat;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.mFont;

public class InfoServer {
	public String text;
	public NodeChat node;
	public int x,y;
	public short idplayer;
	public int type,wString,hpaint;
	public static final byte CHATSERVER = 0;
	public static final byte CHATRAO = 1;
	public static final byte CHATWORLD = 2;
	public boolean isStop,isThongBaoDuoi;
	public int tgdung = 50;
	public InfoServer(byte type,String text)
	{
		this.text = text;
		this.hpaint = 10;
		this.type = type;
		wString = mFont.tahoma_7_white.getWidth(text);
		this.x = 3*GameCanvas.w/4+wString/2;
	}
	public InfoServer(byte type,NodeChat text)
	{
		this.node = text;
		this.hpaint = 10;
		this.type = type;
		wString = mFont.tahoma_7_white.getWidth(text.textdai);
		Cout.println("wString  "+wString);
		this.x = 3*GameCanvas.w/4+wString/2;
	}
	public InfoServer(byte type,String text,int hppaint)
	{
		this.type = type;
		this.hpaint = hppaint;
		this.text = text;
		this.type = type;
		isThongBaoDuoi = true;
		wString = mFont.tahoma_7_white.getWidth(text);
		this.x = 3*GameCanvas.w/4+wString/2;
	}
	public void update()
	{
		switch (type) {
		case CHATSERVER:
			updateInfoServer();
			break;
		case CHATWORLD:
			updateChatWorld();
			break;
		}
	}
	public void paint(mGraphics g)
	{
		g.setColor(0,60);

		g.setClip(GameCanvas.w/4-4, hpaint, GameCanvas.w/2+8, 20);
		g.fillRect(GameCanvas.w/4-4, hpaint, GameCanvas.w/2+8, 20,true);
		g.disableBlending();
		switch (type) {
		case CHATSERVER:
			
//			if(!isThongBaoDuoi)
				 mFont.tahoma_7_yellow.drawString(g, text, x, hpaint+3, 2,true);
//			else 
//				mFont.tahoma_7_red.drawStringBorder(g, text, x, hpaint+5*TCanvas.TileZoom, 2,0xff00000,true);
			break;
		case CHATWORLD:
			if(this.node==null)
				 mFont.tahoma_7_white.drawString(g, text, x, hpaint+3, 2,true);
			else node.paint(g, x, hpaint+3, true, true);
			break;
		}
		GameCanvas.resetTrans(g);
	}
	public void updateChatWorld()
	{
		if(x>(GameCanvas.w/4-wString)&&!isStop){
			if(x>GameCanvas.w/4/*+wString/2*/){ 
				x-=10;
			}else{
				if(tgdung<=0){
					tgdung--;
					if(wString<GameCanvas.w/2)
						x+=tgdung/2;
					else x-=2;
				}else{
					x=GameCanvas.w/4/*+wString/2*/;
					tgdung--;
				}
			}
		}
		else{
			GameScreen.listWorld.removeElement(this);
		}
	}
	public void updateInfoServer()
	{
		if(x>(GameCanvas.w/4-wString)){
			x--;
		}else 
			GameScreen.listInfoServer.removeElement(this);
	}
}

