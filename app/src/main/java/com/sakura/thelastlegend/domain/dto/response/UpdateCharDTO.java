package com.sakura.thelastlegend.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class UpdateCharDTO {
    private long idChar;
    private byte type;
    public List<InfoFoodBuff> infoFoodBuff;
    private byte exp;
    private int totalPoint;
    private int charHpMax;
    private int charHp;
    private int hpPlus;
    private int charMpMax;
    private int charMp;
    private int mpPlus;
}
