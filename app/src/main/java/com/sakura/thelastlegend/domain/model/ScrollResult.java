package com.sakura.thelastlegend.domain.model;

public class ScrollResult {
	public boolean isDowning;
	public int selected = -1;
	public boolean isFinish = false;
}
