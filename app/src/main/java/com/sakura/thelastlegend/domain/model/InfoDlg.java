package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import screen.GameScreen;

import com.sakura.thelastlegend.lib.mFont;

public class InfoDlg {
	static boolean isShow;
	private static String title, subtitke;
	public static int delay;
	public static boolean isLock;

	public static void show(String title, String subtitle, int delay) {
		if (title == null)
			return;
		isShow = true;
		InfoDlg.title = title;
		InfoDlg.subtitke = subtitle;
		InfoDlg.delay = delay;
	}

	public static void showWait() {
		show(mResources.PLEASEWAIT, null, 5000);
		isLock = true;
	}

	public static void showWait(String str) {
		show(str, null, 5000);
		isLock = true;
	}

	public static void paint(mGraphics g) {
		String tt = title;
//		if(TileMap.mapName1 != null){
//			tt = TileMap.mapName1;
//		}
		if (!isShow)
			return;
		if (isLock && delay > 4990)
			return;
		if (GameScreen.isPaintAlert)
			return;
		if (GameScreen.isPaintAlert)
			return;
		int yDlg = 10;
		GameCanvas.paint.paintFrame(GameCanvas.hw - 64, yDlg, 128, 40, g);
		if (isLock) {
//			GameCanvas.paintShukiren(GameCanvas.hw - mFont.tahoma_8b.getWidth(tt) / 2 - 10, yDlg + 20, g, false);
			mFont.tahoma_8b.drawString(g, tt, GameCanvas.hw + 5, yDlg + 13, 2);
		} else if (subtitke != null) {
			mFont.tahoma_8b.drawString(g, tt, GameCanvas.hw, yDlg + 8, 2);
			mFont.tahoma_7_white.drawString(g, subtitke, GameCanvas.hw, yDlg + 22, 2);
		} else
			mFont.tahoma_8b.drawString(g, tt, GameCanvas.hw, yDlg + 13, 2);
	}

	public static void update() {
		if (delay > 0) {
			delay--;
			if (delay == 0)
				hide();
		}
	}

	public static void hide() {
		title = "";
		subtitke = null;
		isLock = false;
		delay = 0;
		isShow = false;
	}
}
