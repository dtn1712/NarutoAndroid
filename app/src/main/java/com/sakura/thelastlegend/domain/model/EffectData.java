package com.sakura.thelastlegend.domain.model;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;

import com.sakura.thelastlegend.GameMidlet;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.MyStream;

public class EffectData {

	public mBitmap img;
	public ImageInfo[] imgInfo;
	public Frame[] frame;
	public short[] arrFrame;
	public short[][] arrFrameMove = new short[6][];
	public int ID;

	public ImageInfo getImageInfo(byte id) {
		for (int i = 0; i < imgInfo.length; i++)
			if (imgInfo[i].ID == id)
				return imgInfo[i];
		return null;
	}
	
	public static DataInputStream readdatamobFromRes(String path){
		DataInputStream iss = null;
		try {
			
			iss = new DataInputStream(MyStream.readFile("/mapitem/mapItem"));
			if(iss != null)
				return iss;
			else 
				return null;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
		
	}

	public int width, height;

	public void readData(String patch) {
		Cout.println2222("readData "+patch);
		InputStream iss = null;
		DataInputStream is = null;
		try {
			is = new DataInputStream(MyStream.readFile(patch));
			Cout.println2222("readData DataInputStream "+iss);
			//iss = GameMidlet.asset.open(patch);
			//is = new DataInputStream(iss);
		} catch (Exception e) {
			return;
		}
		readData(is);

	}
	public void readhd(DataInputStream is){
	     try{
//	    	 System.out.println("readhd ----> "+is);
		      
	      byte nFramestand = is.readByte();// 0
	      arrFrameMove[0] = new short[nFramestand];
//	      System.out.println("nFrameStant ----> "+nFramestand);
	      if(nFramestand > 0){
	       for(int i = 0; i < nFramestand; i++){
	    	   arrFrameMove[0][i] = is.readByte();
	       }
	      }
	      byte nFramewalk = is.readByte();//1
//	      System.out.println("nFramewalk ----> "+nFramewalk);
	      if(nFramewalk > 0){
		      arrFrameMove[1] = new short[nFramewalk];
	       for(int i = 0; i < nFramewalk; i++){
	    	   arrFrameMove[1][i] = is.readByte();
	       }
	      }
	      byte nFrameAttack1 = is.readByte();//2
//	      System.out.println("nFrameAttack1 ----> "+nFrameAttack1);
	      
	      if(nFrameAttack1 > 0){
		      arrFrameMove[2] = new short[nFrameAttack1];
	       for(int i = 0; i < nFrameAttack1; i++){
	    	   arrFrameMove[2][i] = is.readByte();
	       }
	      }
	      byte nFrameAttack2 = is.readByte();//3
//	      System.out.println("nFrameAttack2 ----> "+nFrameAttack2);
	      if(nFrameAttack2 > 0){
		      arrFrameMove[3] = new short[nFrameAttack2];
	       for(int i = 0; i < nFrameAttack2; i++){
	    	   arrFrameMove[3][i] = is.readByte();
	       }
	      }
	      byte nFramesit = is.readByte();//4
//	      System.out.println("nFramesit ----> "+nFramesit);
	      if(nFramesit > 0){
		      arrFrameMove[4] = new short[nFramesit];
	       for(int i = 0; i < nFramesit; i++){
	    	   arrFrameMove[4][i] = is.readByte();
	       }
	      }
	      byte nFrameDead = is.readByte();//5
//	      System.out.println("nFrameDead ----> "+nFrameDead);
	      if(nFrameDead > 0){
		      arrFrameMove[5] = new short[nFramesit];
	       for(int i = 0; i < nFrameDead; i++){
	    	   arrFrameMove[5][i] = is.readByte();
	       }
	      }
	      byte nFrameAttack = is.readByte();//6
//	      System.out.println("nFrameAttack ----> "+nFrameAttack);
	      if(nFrameAttack > 0){
	       for(int i = 0; i < nFrameAttack; i++){
	        byte frame = is.readByte();
	       }
	      }
	      byte nFrameBullet1 = is.readByte();//7
//	      System.out.println("nFrameBullet1 ----> "+nFrameBullet1);
	      if(nFrameBullet1 > 0){
	       for(int i = 0; i < nFrameBullet1; i++){
	        byte frame = is.readByte();
	       }
	      }
	      byte nFrameBullet2 = is.readByte();//8
//	      System.out.println("nFrameBullet2 ----> "+nFrameBullet2);
	      if(nFrameBullet2 > 0){
	       for(int i = 0; i < nFrameBullet2; i++){
	        byte frame = is.readByte();
	       }
	      }
	      
	      
	      
	     }catch(Exception e){
	      e.printStackTrace();
	     }
	     
	     
	    }
	public void readData(DataInputStream is) {

		Cout.println2222("readData DataInputStream ");
		int left = 0, top = 0, right = 0, bottom = 0;
		try {
			if(false)
			{
				 if(is != null){
					    byte smallNumImg = is.readByte();
					    System.out.println("smallImage ----> "+smallNumImg);
					    
					    for (int i = 0; i < smallNumImg; i++) {
					    
					     byte id = is.readByte();
					     byte x0 = is.readByte();
					     byte y0 = is.readByte();
					     byte width = is.readByte();
					     byte hieght = is.readByte();
					    }
					    byte nFrame = is.readByte();
					   // short nFrame = is.readShort();
					    System.out.println("nFrame ---> "+nFrame);

					   
					    for (int i = 0; i < nFrame; i++) {
					     
					    	byte nPart = is.readByte();

						    System.out.println(i+ "  nPart ----> "+nPart);
						     for (int a = 0; a < nPart; a++) {
						      short dx = is.readShort();
						      short dy = is.readShort();
						      byte idImage = is.readByte();
						     }

					    }

					    short nFrameCount = is.readShort();
					    System.out.println("nFrameCount ----> "+nFrameCount);
					   // Res.out("nFrame count = " + nFrameCount + " ...........................................................");
					   
					    for (int i = 0; i < nFrameCount; i++) {
					     short arrframe = is.readShort();
					    }
					   }
				return;
			}
			byte smallNumImg = is.readByte();
			Cout.println2222("readData smallNumImg "+smallNumImg);
			imgInfo = new ImageInfo[smallNumImg];
			for (int i = 0; i < smallNumImg; i++) {
				imgInfo[i] = new ImageInfo();
				imgInfo[i].ID = is.readByte();
				imgInfo[i].x0 = is.readByte();
				imgInfo[i].y0 = is.readByte();
				imgInfo[i].w = is.readByte();
				imgInfo[i].h = is.readByte();
				Cout.println2222(imgInfo[i].ID+"  x  "+imgInfo[i].x0+"  y  "+imgInfo[i].y0+"  w  "+imgInfo[i].w+" h "+imgInfo[i].h);
				   
			}
			short nFrame = is.readByte();

			Cout.println2222("readData nFrame "+nFrame);
			frame = new Frame[nFrame];
			for (int i = 0; i < nFrame; i++) {
				frame[i] = new Frame();
				byte nPart = is.readByte();
				Cout.println2222("readData nPart "+nPart);
				frame[i].dx = new short[nPart];
				frame[i].dy = new short[nPart];
				frame[i].idImg = new byte[nPart];
				for (int a = 0; a < nPart; a++) {
					frame[i].dx[a] = is.readByte();
					frame[i].dy[a] = is.readByte();
					frame[i].idImg[a] = is.readByte();
					Cout.println2222(a+ " readData dx "+frame[i].dx[a]+"  "+frame[i].dy[a]+" idImg "+frame[i].idImg[a]);
					if (i == 0) {
						if (left > frame[i].dx[a])
							left = frame[i].dx[a];
						if (top > frame[i].dy[a])
							top = frame[i].dy[a];
						if (right < frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w)
							right = frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w;
						if (bottom < frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h)
							bottom = frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h;
						width = right - left;
						height = bottom - top;
						
					}
				}

			}
			short nFrameCount = is.readByte();
			
			Cout.println2222("nFrameCount  "+nFrameCount);
		//	Res.out("nFrame count = " + nFrameCount + " ...........................................................");
			arrFrame = new short[nFrameCount];
			for (int i = 0; i < nFrameCount; i++) {
				arrFrame[i] = is.readByte();
				Cout.println2222(i+"  nFrameCount  "+arrFrame[i]);
			}
			int w = is.readByte();
			int h = is.readByte();
			Cout.println2222(" w  "+w+" h  "+h +"  avai "+is.available());
			
			int nFrameDung = is.readUnsignedByte();
			Cout.println2222(" nFrameDung  "+nFrameDung);
			arrFrameMove[0] = new short[nFrameDung];
			for (int i = 0; i < nFrameDung; i++) {
				arrFrameMove[0][i] = is.readByte();
				Cout.println2222(i+" nFrameDung  "+arrFrameMove[0][i]);
			}
			short nFrameDi = is.readShort();
			Cout.println2222("nFrameDi  "+nFrameDi);
			arrFrameMove[1] = new short[nFrameDi];
			for (int i = 0; i < nFrameDi; i++) {
				arrFrameMove[1][i] = is.readShort();
			}
			short nFrameAttack = is.readShort();
			Cout.println2222("nFrameAttack  "+nFrameAttack);
			arrFrameMove[2] = new short[nFrameAttack];
			for (int i = 0; i < nFrameAttack; i++) {
				arrFrameMove[2][i] = is.readShort();
			}
			short nFrameBiAttack = is.readShort();
			Cout.println2222("nFrameBiAttack  "+nFrameBiAttack);
			arrFrameMove[3] = new short[nFrameBiAttack];
			for (int i = 0; i < nFrameBiAttack; i++) {
				arrFrameMove[3][i] = is.readShort();
				Cout.println2222(i+" nFrameBiAttack  "+arrFrameMove[3][i]);
			}

			short nFrameBiAttack4 = is.readShort();
			Cout.println2222("arrFrameMove4  "+nFrameBiAttack4);
			for (int i = 0; i < nFrameBiAttack4; i++) {
				arrFrameMove[4][i] = is.readShort();
				Cout.println2222(i+" arrFrameMove4  "+arrFrameMove[4][i]);
			}
			short nFrameBiAttack5 = is.readShort();
			Cout.println2222("nFrameBiAttack5  "+nFrameBiAttack5);
			for (int i = 0; i < nFrameBiAttack4; i++) {
				arrFrameMove[5][i] = is.readShort();
				Cout.println2222(i+" nFrameBiAttack5  "+arrFrameMove[5][i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void readData(byte[] data) {

		DataInputStream is = null;
		ByteArrayInputStream array = new ByteArrayInputStream(data);
		is = new DataInputStream(array);
		readData(is);
	}
	public void readData222(DataInputStream is) {
		  try {

				int left = 0, top = 0, right = 0, bottom = 0;
		   if(is != null){
		    byte smallNumImg = is.readByte();
		    imgInfo = new ImageInfo[smallNumImg];
		    for (int i = 0; i < smallNumImg; i++) {
		     imgInfo[i] = new ImageInfo();
				imgInfo[i].ID = is.readByte();
				imgInfo[i].x0 = is.readUnsignedByte();
				imgInfo[i].y0 = is.readUnsignedByte();
				imgInfo[i].w = is.readUnsignedByte();
				imgInfo[i].h = is.readUnsignedByte();

//			    Cout.println2222(imgInfo[i].ID+"  x  "+imgInfo[i].x0+"  y  "+imgInfo[i].y0+"  w  "+imgInfo[i].w+" h "+imgInfo[i].h);
			   
		     
		    }
		    byte nFrame = is.readByte();
		   // short nFrame = is.readShort();
		    frame = new Frame[nFrame];
//		    Cout.println2222("  nFrame  "+nFrame);
		   
		    for (int i = 0; i < nFrame; i++) 
		    {
		    	frame[i] = new Frame();
		    	byte nPart = is.readByte();

				frame[i].dx = new short[nPart];
				frame[i].dy = new short[nPart];
				frame[i].idImg = new byte[nPart];
//			    Cout.println2222(i+"  nPart  "+nPart);
			    for (int a = 0; a < nPart; a++) {
			    	frame[i].dx[a] = is.readByte();
					frame[i].dy[a] = is.readByte();
					frame[i].idImg[a] = is.readByte();
					//Cout.println2222(a+ " readData dx "+frame[i].dx[a]+"  "+frame[i].dy[a]+" idImg "+frame[i].idImg[a]);
//					if (i == 0) {
//						if (left > frame[i].dx[a])
//							left = frame[i].dx[a];
//						if (top > frame[i].dy[a])
//							top = frame[i].dy[a];
//						if (right < frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w)
//							right = frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w;
//						if (bottom < frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h)
//							bottom = frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h;
//						width = right - left;
//						height = bottom - top;
//						
//					}
		     }

		    }
		    short nFrameCount = is.readByte();

		   // Res.out("nFrame count = " + nFrameCount + " ...........................................................");
		    arrFrame = new short[nFrameCount];
//		    Cout.println2222("  nFrameCount  "+nFrameCount);
		    for (int i = 0; i < nFrameCount; i++) {
		    	arrFrame[i] = is.readByte();
		    }
		   }

		    width = is.readByte();
		    height = is.readByte();
//		    Cout.println2222("  w  "+is.readByte()+"  h "+is.readByte());
		   

		  } catch (Exception e) {
		   e.printStackTrace();
		   // TODO: handle exception
		  }
		 }
	
	public void readData_Hoa(DataInputStream is) {
		  try {
		  
		   if(is != null){
		    byte smallNumImg = is.readByte();

		      System.out.println(" smallNumImg ------> "+smallNumImg);
		    for (int i = 0; i < smallNumImg; i++) {
		    
		     byte id = is.readByte();
		     byte x0 = is.readByte();
		     byte y0 = is.readByte();
		     byte width = is.readByte();
		     byte hieght = is.readByte();
		    }
		    byte nFrame = is.readByte();
		   // short nFrame = is.readShort();


		      System.out.println(" nFrame ------> "+nFrame);
		    for (int i = 0; i < nFrame; i++) {
		     
		     byte nPart = is.readByte();

		      System.out.println(i+" nPart ------> "+nPart);
		     for (int a = 0; a < nPart; a++) {
		      byte dx = is.readByte();
		      byte dy = is.readByte();
		      byte idImage = is.readByte();
		      System.out.println(a+" Frame ------> "+dx+" "+dy+" "+idImage);
		     }

		    }
		    
		    byte runningFrame = is.readByte();
		    

		      System.out.println(" runningFrame ------> "+runningFrame);
		    
		    for(int i = 0; i < runningFrame; i++){
		     byte rFrame = is.readByte();
		    }
		    
		    width = is.readByte();
		    height = is.readByte();
		   }
		   
		   

		  } catch (Exception e) {
		   e.printStackTrace();
		   // TODO: handle exception
		  }
		 }
	public void paintFrame(mGraphics g, int f, int x, int y, int trans, int layer) {
		
		
		if (frame != null && frame.length != 0) {
			Frame fr = frame[f];
			for (int i = 0; i < fr.dx.length; i++) {
				ImageInfo im = getImageInfo(fr.idImg[i]);

				 try{
				if (trans == 0) {
					g.drawRegion(img, im.x0, im.y0, im.w, im.h, 0, x + fr.dx[i], y + fr.dy[i]
							- ((layer < 4 && layer > 0) ? trans : 0), 0);
				}
				if (trans == 1) {
					g.drawRegion(img, im.x0, im.y0, im.w, im.h, Sprite.TRANS_MIRROR, x - fr.dx[i], y + fr.dy[i]
							- ((layer < 4 && layer > 0) ? trans : 0), StaticObj.TOP_RIGHT);
				}
				 }catch (Exception e) {
//				 System.out.println("ERROR AT EFFECTDATA "+ID+" "+im.x0+","+
//				 im.y0+","+im.w+","+ im.h);
				 }
			}

		}
	}
}