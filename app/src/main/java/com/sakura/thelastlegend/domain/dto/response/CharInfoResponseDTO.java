package com.sakura.thelastlegend.domain.dto.response;
import com.sakura.thelastlegend.domain.dto.clan.ClanBasicInfoDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CharInfoResponseDTO {
    private long id;
    private int maxHp;
    private int hp;
    private short level;
    private boolean jinchuuriki;
    private boolean ninjaExile;
    private ClanBasicInfoDTO globalClan;
    private ClanBasicInfoDTO localClan;
    private short headId;
    private short bodyId;
    private short legId;
}
