package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.lib.mVector;
import Objectgame.Char;
import screen.GameScreen;

public class Party {

	public short charId; // in sưa lai short
	public int level;
	public short iconId;
	public String name;
	public boolean isLock;
	public Char c;
	public int size;
	public short idParty;
	public static mVector vCharinParty = new mVector();
	public boolean isLeader = false;
	public Char Charleader;
	public static short idleader;
	
	protected static Party instance;

	public static Party gI() {
		if (instance == null)
			instance = new Party();
		return instance;
	}
	public Party(){
		
	}
	public Party(short partyid, short[] idmember, short idleader, short[] lv, short[] idhead){
		vCharinParty.removeAllElements();
		for(int i = 0; i < idmember.length; i++){
			Char cMem = GameScreen.findCharInMap(idmember[i]);
			
			if(cMem != null){
				cMem.idParty = partyid;
				if(cMem.charID == idleader)
					cMem.isLeader = true;
				else
					cMem.isLeader = false;
				vCharinParty.addElement(cMem);
			}else{
				cMem = Char.myChar();
				cMem.idParty = partyid;
				if(cMem.charID == idleader)
					cMem.isLeader = true;
				else
					cMem.isLeader = false;

				cMem.clevel = lv[i];
				cMem.head = idhead[i];
				vCharinParty.addElement(cMem);
			}
		}
	}
	
	public Party(byte classId, int level, String name, int size){
		switch (classId) {
			case 0: // chua nhap
				iconId = 647;
				break;
			case 1: // kiem
				iconId = 1182;
				break;
			case 2: // fi tien
				iconId = 1181;
				break;
			case 3: // kunai
				iconId = 643;
				break;
			case 4: // cung
				iconId = 645;
				break;
			case 5: // dao
				iconId = 676;
				break;
			case 6: // quat
				iconId = 1119;
				break;
		}
		this.name = name;
		this.level = level;
		this.size = size;
	}
	
//	public Party(short idparty, short[] charmeberid, short leaderid){ 
//		// tao danh sách party
//		vCharinParty.removeAllElements();
//		this.idleader = leaderid;
//		this.idParty = idparty;
////		Charleader = GameScreen.findCharInMap(leaderid);
////		if(Charleader != null){
////			Charleader.isLeaderParty = true;
////			vCharinParty.addElement(Charleader);
////		}
//		for(int i = 0; i < charmeberid.length; i++){
//			c = GameScreen.findCharInMap(charmeberid[i]);
//			if(c != null)
//				vCharinParty.addElement(c);
//			else{
//				c = Char.myChar();
//				vCharinParty.addElement(c);
//			}
//		}
////		c = GameScreen.findCharInMap(leaderid);
////		if(c != null)
////			vCharinParty.addElement(c);
////		else{
////			c = Char.myChar();
////			vCharinParty.addElement(c);
////		}
////		
		
//	}
	

	public Party(short charId, byte classId, String name, boolean isLock){
		this.charId = charId;
		this.isLock = isLock;
		switch (classId) {
			case 0: // chua nhap
				iconId = 647;
				break;
			case 1: // kiem
				iconId = 1182;
				break;
			case 2: // fi tien
				iconId = 1181;
				break;
			case 3: // kunai
				iconId = 643;
				break;
			case 4: // cung
				iconId = 645;
				break;
			case 5: // dao
				iconId = 676;
				break;
			case 6: // quat
				iconId = 1119;
				break;
		}
		this.name = name;
		if(charId == Char.myChar().charID)
			c = Char.myChar();
		else
			c = GameScreen.findCharInMap(charId);
	}

	
//	public static void refreshAll(){
//		for (int j = 0; j < GameScreen.vParty.size(); j++) {
//			Party party = (Party) GameScreen.vParty.elementAt(j);
//			if(party.charId != Char.myChar().charID){
//				party.c = GameScreen.findCharInMap(party.charId);
//			}
//		}
//	}
//	
//	public static void refresh(Char cc){
//		for (int i = 0; i < GameScreen.vParty.size(); i++) {
//			Party party = (Party) GameScreen.vParty.elementAt(i);
//			if(party.charId == cc.charID){
//				party.c = cc;
//				break;
//			}
//		}
//	}
//	
//	public static void clear(int charId){
//		for (int j = 0; j < GameScreen.vParty.size(); j++) {
//			Party party = (Party) GameScreen.vParty.elementAt(j);
//			if(party.charId == charId){
//				party.c = null;
//				break;
//			}
//		}
//	}
}
