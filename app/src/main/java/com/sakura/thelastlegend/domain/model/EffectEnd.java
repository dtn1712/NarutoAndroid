package com.sakura.thelastlegend.domain.model;

import screen.GameScreen;

import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.mVector;


public class EffectEnd extends MainEffect {

	public static final byte END_NUC_DAT = 0;
	public static final byte END_EFF_WATER_BIG = 1;
	public static final byte END_FAR_NEAR = 2;
	public static final byte END_THIEN_THACH = 3;
	public static final byte END_KILL_PS_LV2 = 4;
	public static final byte END_KILL_2KIEM_LV2 = 5;
	public static final byte END_KILL_2KIEM_LV2_BEGIN = 6;
	public static final byte END_KILL_SUNG_LV1 = 7;
	// public static final byte END_KILL_SUNG_LV5 = 8;
	public static final byte END_ROCK = 9;
	public static final byte END_KILL_PS_LV1 = 10;
	public static final byte END_DIE_MONSTER = 11;
	public static final byte END_KILL_SUNG_LV2 = 12;
	public static final byte END_KILL_SUNG_LV2_BEGIN = 13;
	public static final byte END_PHAO_HOA = 14;
	public static final byte END_PHAO_BANG = 15;
	// public static final byte END_KIEM_NUC_DAT = 16;
	// public static final byte END_KIEM_LV4 = 17;
	// public static final byte END_2KIEM_LV3 = 18;
	public static final byte END_WATER_ROCK = 19;
	// public static final byte END_BEGIN_SUNG_LV5 = 20;
	public static final byte END_EFF_LV_UP = 21;
	// public static final byte END_EFF_CLASS = 22;
	// public static final byte END_EFF_4FIRE = 23;
	public static final byte END_EFF_2KIEM_DOC = 24;
	public static final byte END_EFF_KIEM_LV6 = 25;
	public static final byte END_KIEM_NUC_DAT_2 = 26;
	public static final byte END_ICE_BIG = 27;
	public static final byte END_ICE_UP = 28;
	public static final byte END_XUYEN_GIAP = 29;
	public static final byte END_ROCK_FIRE_BIG = 30;
	public static final byte END_REBUILD = 31;
	public static final byte END_LEVEL_UP_REBUILD = 32;
	public static final byte END_EFF_LV_UP_REBUILD = 33;
	public static final byte END_EFF_FINISH_REBUILD = 34;
	public static final byte END_EFF_REMOVE_OBJ = 35;
	public static final byte END_EFF_REMOVE_MON_PHO_BANG = 36;
	public static final byte END_REPLACE_PLUS = 37;
	public static final byte END_NORMAL = 50;
	mVector VecEffEnd = new mVector(), VecSubEffEnd = new mVector();
	int x1000, y1000, lT_Arc, gocT_Arc;
	Point pRebuild;

	public EffectEnd(int type, int x, int y) {
		f = -1;
		this.typeEffect = type;
		this.x = x;
		this.y = y;
		switch (type) {
		case END_NORMAL:
			fraImgEff = new FrameImage(21, 48, 48);
			fRemove = 6;
			break;
		}
	}

	int typeSub;

	public EffectEnd(int type, int x, int y, int typeSub) {
		f = -1;
		this.typeEffect = type;
		this.typeSub = typeSub;
		this.x = x;
		this.y = y;
		// switch (type) {
		// case END_EFF_CLASS:
		// fRemove = 3;
		// switch (typeSub) {
		// case 0:
		// fraImgEff = new FrameImage(77, 17, 17);
		// break;
		// case 1:
		// fraImgEff = new FrameImage(76, 17, 17);
		// break;
		// case 2:
		// fraImgEff = new FrameImage(78, 24, 24);
		// break;
		// case 3:
		// fraImgEff = new FrameImage(79, 24, 24);
		// break;
		// }
		// break;
		// }
	}

	int numEffReplace = 0;
	int plusGoc;

	public EffectEnd(int type, int x, int y, int xTo, int yTo, int num) {
		this.typeEffect = type;
		this.x = x;
		this.y = y;
		this.toX = xTo;
		this.toY = yTo;
		switch (type) {
		case END_REBUILD:
			create_Arc_Big_Small();
			break;
		case END_REPLACE_PLUS:
			fraImgEff = new FrameImage(7, 25, 25);
			fraImgSubEff = new FrameImage(2, 9, 9);
			fRemove = 100;
			lT_Arc = 40;
			gocT_Arc = 0;
			numEffReplace = num;
			plusGoc = 360 / num;
			break;
		}
	}

	public void paint(mGraphics g) {
		switch (typeEffect) {
		case END_NORMAL:
			// case END_BEGIN_SUNG_LV5:
			// case END_EFF_CLASS:
			// case END_KILL_SUNG_LV5:
			fraImgEff.drawFrame((f/fraImgEff.nFrame) % fraImgEff.nFrame, x, y, 0, 3, g);
			break;
		}
	}

	public void update() {
		f++;
		switch (typeEffect) {
		
		case END_NORMAL:
			if (f >= fRemove) {
				removeEff();
			}
			break;
		}
	}

	public void removeEff() {
		isStop  = true;// stop
		GameScreen.veffClient.remove(this);
	}

	private void create_Arc_Big_Small() {
		fraImgEff = new FrameImage(7, 25, 25);
		fraImgSubEff = new FrameImage(2, 9, 9);
		x1000 = x * 1000;
		y1000 = y * 1000;
		fRemove = 15;
		lT_Arc = getDistance(toX, toY, x, y);
		gocT_Arc = CRes.angle(x - toX, y - toY);
		// for (int i = 0; i < 2; i++) {
		// Point
		pRebuild = new Point(toX * 1000, toY * 1000);
		pRebuild.x2 = toX * 1000;
		pRebuild.y2 = toY * 1000;
		pRebuild.f = gocT_Arc;
		pRebuild.vy = CRes.sin(CRes.fixangle(pRebuild.f % 360)) * lT_Arc;
		pRebuild.vx = CRes.cos(CRes.fixangle(pRebuild.f % 360)) * lT_Arc;
		pRebuild.v = 0;
		// VecEffEnd.addElement(p);
		// }
	}

	private void create_Level_up() {
		fraImgSubEff = new FrameImage(67, 59, 17);
		fRemove = 23;
	}
}
