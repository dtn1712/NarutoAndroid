package com.sakura.thelastlegend.domain.model;

import Objectgame.TileMap;
import com.sakura.thelastlegend.lib.Cout;

public class Waypoint {
	public short minX, minY, maxX, maxY;
	public String name;

	public Waypoint(short idmap,short minX, short minY, short maxX, short maxY) {
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.name = (String)TileMap.listNameAllMap.get(idmap+"");
		ServerEffect.addServerEffect(24, minX+(maxX - minX)/2,minY,true);
		
	}

}
