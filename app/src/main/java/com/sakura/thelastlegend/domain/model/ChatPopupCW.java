package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.mFont;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import screen.GameScreen;

public class ChatPopupCW extends Dialog implements IActionListener {
	
	public String[] info;
	public boolean isWait;
	public int timeShow;
	int h;
	private int padLeft;

	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		
		int yDlg = GameCanvas.h - h - 38;
		GameCanvas.paint.paintFrame(padLeft, yDlg, GameCanvas.w - (padLeft * 2), h, g);
		int yStart = yDlg + (h - info.length * mFont.tahoma_8b.getHeight()) / 2 - 2;
		
		for (int i = 0, y = yStart; i < info.length; i++, y += mFont.tahoma_8b.getHeight()) {
			mFont.tahoma_8b.drawString(g, info[i], GameCanvas.hw, y, 2);
		}
		paintCmdBar(g);
		super.paint(g);
		
	}


	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		
	}

	public void show() {
		// TODO Auto-generated method stub
		GameCanvas.currentDialog = this;
		
	}
	
	public void paintCmdBar(mGraphics g){
		mFont f = GameCanvas.isTouch ? mFont.tahoma_7b_yellow : mFont.tahoma_8b;
		int d = GameCanvas.isTouch ? 3 : 1;
		if (!GameCanvas.isTouch) {
			if (left != null) {
				f.drawString(g, left.caption, 5, GameCanvas.h - Screen.cmdH + 4 + d, 0);
			}
			if (center != null) {
				f.drawString(g, center.caption, GameCanvas.hw, GameCanvas.h - Screen.cmdH + 4 + d, 2);
			}
			if (right != null) {
				if (right.img != null)
					g.drawImage(right.img, GameCanvas.w - 5, GameCanvas.h - 11, mGraphics.RIGHT | mGraphics.VCENTER);
				else
					f.drawString(g, right.caption, GameCanvas.w - 5, GameCanvas.h - Screen.cmdH + 4 + d, 1);
			}
		} else {
			if (left != null) {
				lenCaption = f.getWidth(left.caption);
				if (lenCaption > 0) {
					if (left.x > 0 && left.y > 0)
						left.paint(g);
					else {
							if (Screen.keyTouch == 0)
							g.drawImage(GameScreen.imgLbtnFocus, 1, GameCanvas.h - Screen.cmdH + 1, 0);
						else
							g.drawImage(GameScreen.imgLbtn, 1, GameCanvas.h - Screen.cmdH + 1, 0);
						f.drawString(g, left.caption, 35, GameCanvas.h - Screen.cmdH + 4 + d, 2);
					}
				}
			}
			if (center != null) {
				lenCaption = f.getWidth(center.caption);
				if (lenCaption > 0) {
					if (center.x > 0 && center.y > 0)
						center.paint(g);
					else {
							if (Screen.keyTouch == 1)
								g.drawImage(GameScreen.imgLbtnFocus, GameCanvas.hw - 35, GameCanvas.h - Screen.cmdH + 1, 0);
							else
								g.drawImage(GameScreen.imgLbtn, GameCanvas.hw - 35, GameCanvas.h - Screen.cmdH + 1, 0);
							f.drawString(g, center.caption, GameCanvas.hw, GameCanvas.h - Screen.cmdH + 4 + d, 2);
						}

				}

			}
			if (right != null) {

				lenCaption = f.getWidth(right.caption);

				if (lenCaption > 0) {
					if (right.x > 0 && right.y > 0)
						right.paint(g);
					else {
						if (Screen.keyTouch == 2)
							g.drawImage(GameScreen.imgLbtnFocus, GameCanvas.w - 71, GameCanvas.h - Screen.cmdH + 1, 0);
						else
							g.drawImage(GameScreen.imgLbtn, GameCanvas.w - 71, GameCanvas.h - Screen.cmdH + 1, 0);
						f.drawString(g, right.caption, GameCanvas.w - 35, GameCanvas.h - Screen.cmdH + 4 + d, 2);
					}
				}

			}
		}
	}
	
	public void setInfo(String info, Command left, Command center, Command right) {
		this.info = mFont.tahoma_8b.splitFontArray(info, GameCanvas.w - (padLeft * 2+40));
		this.left = left;
		this.center = center;
		this.right = right;
		
		
		if(center!=null){
			this.center.x = GameCanvas.w/2 - 35;
			this.center.y = GameCanvas.h - 26;
			
			if(left!=null){
				this.left.x = GameCanvas.w/2 - 115;
				this.left.y = GameCanvas.h - 26;
			}
			
			if(right!=null){
				this.right.x = GameCanvas.w/2 + 45;
				this.right.y = GameCanvas.h - 26;
			}
		}else{
			if(left!=null){
				this.left.x = GameCanvas.w/2 - 80;
				this.left.y = GameCanvas.h - 50;
			}
			if(right!=null){
				this.right.x = GameCanvas.w/2 + 10;
				this.right.y = GameCanvas.h - 50;
			}
		}
		
		
	
		isWait = false;
		h = 80;
		if (this.info.length >= 5)
			h = this.info.length * mFont.tahoma_8b.getHeight() + 20;
	}
	
	public void update() {
		if(timeShow > 0){
			timeShow --;
			if(timeShow == 1){
				GameCanvas.endDlg();
				timeShow = 0;
			}
		}
		super.update();
	}


	public void perform() {
		// TODO Auto-generated method stub
		
	}

}
