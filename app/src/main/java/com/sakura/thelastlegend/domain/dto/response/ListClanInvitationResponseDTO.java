package com.sakura.thelastlegend.domain.dto.response;

import com.sakura.thelastlegend.domain.dto.clan.ClanBasicInfoDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class ListClanInvitationResponseDTO {
    private List<ClanBasicInfoDTO> clans;
}
