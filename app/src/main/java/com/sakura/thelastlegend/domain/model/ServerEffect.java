package com.sakura.thelastlegend.domain.model;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import Objectgame.Char;
import screen.GameScreen;

public class ServerEffect extends Effect2 {
	public EffectCharPaint eff;
	int i0, x, y, dir = 1;
	Char c;
	public boolean isloopForever;
	short loopCount = 0;
	private long endTime = 0;
	
	public static void addServerEffect(int id, int cx, int cy, int loopCount) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScreen.efs[id];
		e.x = cx;
		e.y = cy;
		e.loopCount = (short) loopCount;
		vEffect2.addElement(e);
	}
	public static void addServerEffect(int id, int cx, int cy, boolean loopForever) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScreen.efs[id];
		e.x = cx;
		e.y = cy;
		e.isloopForever = loopForever;
		vEffect2.addElement(e);
	}
	public static void addServerEffect(int id, int cx, int cy, int loopCount, byte dir) {
		try {
			ServerEffect e = new ServerEffect();
			e.eff = GameScreen.efs[id];
			e.x = cx;
			e.y = cy;
			e.loopCount = (short) loopCount;
			e.dir = dir;
			vEffect2.addElement(e);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void addServerEffect(int id, Char c, int loopCount) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScreen.efs[id];
		e.c = c;
		e.loopCount = (short) loopCount;
		vEffect2.addElement(e);
	}
	public static void addServerEffect(int id, Char c, boolean isForever) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScreen.efs[id];
		e.c = c;
		e.isloopForever = isForever;
		vEffect2.addElement(e);
	}
	public static void addServerEffectWithTime(int id, int cx, int cy, int timeLengthInSecond) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScreen.efs[id];
		e.x = cx;
		e.y = cy;
		e.endTime = System.currentTimeMillis() + timeLengthInSecond * 1000;
		vEffect2.addElement(e);
	}

	public static void addServerEffectWithTime(int id, Char c, int timeLengthInSecond) {
		ServerEffect e = new ServerEffect();
		e.eff = GameScreen.efs[id];
		e.c = c;
		e.endTime = System.currentTimeMillis() + timeLengthInSecond * 1000;
		vEffect2.addElement(e);
	}
	

	public void paint(mGraphics g) {
		try{
			if (c != null) {
				x = c.cx;
				y = c.cy;
			}
			int xp, yp;
			xp = x + eff.arrEfInfo[i0].dx * dir;
			yp = y + eff.arrEfInfo[i0].dy;
			if (GameCanvas.isPaint(xp, yp)) {
				SmallImage.drawSmallImage(g, eff.arrEfInfo[i0].idImg, xp, yp, (dir == 1) ? 0 : 2, mGraphics.VCENTER | mGraphics.HCENTER);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}

	public void update() {
		
		
		if (endTime != 0) {
			i0++;
			if (i0 >= eff.arrEfInfo.length) {
				i0 = 0;
			}
			if (System.currentTimeMillis() - endTime > 0){
//				if(eff.idEf == 120)
//					GameCanvas.isBallEffect = false;
				vEffect2.removeElement(this);
			}
		} else if(!isloopForever){
			i0++;
			if (i0 >= eff.arrEfInfo.length) {
				loopCount--;
				if (loopCount <= 0){
//					if(eff.idEf == 120)
//						GameCanvas.isBallEffect = false;
					vEffect2.removeElement(this);
				}
				else
					i0 = 0;
			}
		}else {
			i0++;
			if (i0 >= eff.arrEfInfo.length) 
				i0 = 0;
		}
		if (GameCanvas.gameTick % 11 == 0 && c!=null && c!=Char.myChar()) {
			if (!GameScreen.vCharInMap.contains(this.c)){
				
				vEffect2.removeElement(this);
			}
		}
	}

	public static void removeEffect(int id)
	{
		for (int i = 0; i < vEffect2.size(); i++) {
			try {
				ServerEffect l = (ServerEffect)vEffect2.elementAt(i);
				if(l.eff.indexEffect==id)
				{
					vEffect2.removeElementAt(i);
					i--;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	}

}
