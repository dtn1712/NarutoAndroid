package com.sakura.thelastlegend.domain.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class MapTemplateDTO {
    private short id;
    private byte tileId;
    private String name;
    private byte country; /// quoc gia
    private byte bg; // background
}
