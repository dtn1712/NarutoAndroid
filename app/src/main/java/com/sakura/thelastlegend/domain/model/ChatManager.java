package com.sakura.thelastlegend.domain.model;


import java.util.Vector;

import com.sakura.thelastlegend.GameCanvas;

import com.sakura.thelastlegend.lib.mVector;


public class ChatManager {
	public mVector chatTabs;
	public int isNewMessage;
	private static ChatManager instance;
	public int currentTabIndex = 0;
	public static boolean blockGlobalChat,blockPrivateChat, isMessageClan, isMessagePt;
	public void switchToNextTab()
	{
		currentTabIndex++;
		if(currentTabIndex>chatTabs.size()-1)currentTabIndex=0;
	}
	public void switchToPreviousTab()
	{
		currentTabIndex--;
		if(currentTabIndex<0)currentTabIndex=chatTabs.size()-1;
	}
	public void switchToTab(int index)
	{
		currentTabIndex=index;
	}
	public void switchToTab(ChatTab t)
	{
		currentTabIndex=chatTabs.indexOf(t);
	}
	public void switchToLastTab() {
		currentTabIndex=chatTabs.size()-1;
		
	}
	public static ChatManager gI()
	{
		return instance==null?instance = new ChatManager():instance;
	}
	public ChatManager()
	{
		chatTabs = new mVector();
		chatTabs.addElement(new ChatTab(mResources.PUBLICCHAT[0], 0));
		chatTabs.addElement(new ChatTab(mResources.PARTYCHAT[0], 1));
		chatTabs.addElement(new ChatTab(mResources.GLOBALCHAT[0], 3));
		chatTabs.addElement(new ChatTab(mResources.CLANCHAT[0], 4));
		ChatTab c = findTab(mResources.GLOBALCHAT[0]);

		c.addInfo("c8" + mResources.GLOBALCHAT[1]);
		c.addInfo("c8" + mResources.GLOBALCHAT[2]);
		c.addInfo("c8" + mResources.GLOBALCHAT[3]);

		ChatTab c1 = findTab(mResources.PARTYCHAT[0]);
		c1.addInfo("c8" + mResources.PARTYCHAT[1]);

		ChatTab c2 = findTab(mResources.CLANCHAT[0]);
		c2.addInfo("c8" + mResources.CLANCHAT[1]);

		ChatTab c3 = findTab(mResources.PUBLICCHAT[0]);
		c3.addInfo("c8" + mResources.PUBLICCHAT[1]);
		
	}
	public ChatTab findTab(String ownerName)
	{
		for(int i=0;i<chatTabs.size();i++)
		{
			ChatTab c=(ChatTab) chatTabs.elementAt(i);
			if(c.ownerName.equals(ownerName))return c;
		}
		return null;
	}
	public void addChat(String ownerName,String whoChat, String text) {
		ChatTab c=findTab(ownerName);
		
		if(c==null) 
		{
			c=addNewTab(ownerName);
		}
		c.addChat(whoChat,text);
	}
	public void addPublicInfo(String text) {
		ChatTab c = findTab(mResources.PUBLICCHAT[0]);
		if (c == null)
			return;
		c.addInfo(text);
	}

	public ChatTab getCurrentChatTab() {
		return (ChatTab) chatTabs.elementAt(currentTabIndex);
	}
	
	public ChatTab addNewTab(String friendName) {
		ChatTab c = new ChatTab(friendName, 2);
		if(!GameCanvas.isTouch)
			c.addInfo("c2" + mResources.CLOSE_CURTAB);
		chatTabs.addElement(c);
		return c;
	}
	
	public mVector waitList = new mVector();

	public void addWaitList(String nick) {
		for (int i = 0; i < waitList.size(); i++)
			if (((String) waitList.elementAt(i)).equals(nick))
				return;
		waitList.addElement(nick);
	}
	
	
	
	public boolean findWaitPerson(String nick) {
		for (int i = 0; i < waitList.size(); i++)
			if (((String) waitList.elementAt(i)).equals(nick))
				return true;
		return false;
	}
	
	public int postWaitPerson(){
		int t = -1;
		for (int i = 3; i < chatTabs.size(); i++) {
			ChatTab c = (ChatTab) chatTabs.elementAt(i);
			for (int j = 0; j < waitList.size(); j++) {
				if(c.ownerName.equals((waitList.elementAt(j)).toString())){
					return i;
				}
			}
		}
		return t;
	}
	
	public void removeFromWaitList(String nick) {
		for (int i = 0; i < waitList.size(); i++)
			if (((String) waitList.elementAt(i)).equals(nick)) {
				waitList.removeElementAt(i);
				return;
			}
	}
	
	public void clear(){
		instance = null;
	}
}
