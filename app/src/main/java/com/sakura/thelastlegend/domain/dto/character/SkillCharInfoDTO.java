package com.sakura.thelastlegend.domain.dto.character;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class SkillCharInfoDTO {
    private byte idSkill;
    private short idIcon;
    private short idIconShort;
    private String nameSkill;
    private String description;
    private byte typeBuff;
    private short idEffect;
    List<SkillLevelInfoDTO> listLevel;
}
