package com.sakura.thelastlegend.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class PartyMemberInfoDTO {
    private long charIdMember;
    private short lvChar;
    private short idHead;
}
