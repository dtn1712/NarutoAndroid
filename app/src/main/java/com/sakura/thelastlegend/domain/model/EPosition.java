package com.sakura.thelastlegend.domain.model;


public class EPosition {
	public int x, y,anchor;
	public byte follow,count=0,dir=1;
	public short index=-1;
	public EPosition(int x,int y) {
		this.x = x;
		this.y = y;
	}
	public EPosition(int x,int y,int fol) {
		this.x = x;
		this.y = y;
		follow=(byte)fol;
	}
	public EPosition() {
	}
}
