package com.sakura.thelastlegend.domain.dto.response;

import com.sakura.thelastlegend.domain.dto.character.CharBasicInfoDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.UnusedPrivateField")
public class CharListResponseDTO {

    private List<CharBasicInfoDTO> chars;
}
