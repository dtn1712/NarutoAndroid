package com.sakura.thelastlegend;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.JSONParser;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.lib.Music;
import com.sakura.thelastlegend.lib.MyStream;
import com.sakura.thelastlegend.lib.Session_ME;
import com.sakura.thelastlegend.lib.Util;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.Dialog;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.InfoDlg;
import com.sakura.thelastlegend.domain.model.InfoServer;
import com.sakura.thelastlegend.domain.model.Input2Dlg;
import com.sakura.thelastlegend.domain.model.InputDlg;
import com.sakura.thelastlegend.domain.model.Menu;
import com.sakura.thelastlegend.domain.model.MsgDlg;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Party;
import com.sakura.thelastlegend.domain.model.Position;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.SmallImage;
import com.sakura.thelastlegend.domain.model.StaticObj;
import com.sakura.thelastlegend.domain.model.Timer;
import com.sakura.thelastlegend.domain.model.mResources;
import com.sakura.thelastlegend.real.Service;
import screen.CreateCharScreen;
import screen.GameScreen;
import screen.LanguageScr;
import screen.LoginScreen;
import screen.SelectCharScreen;
import screen.SplashScreen;
import com.sakura.thelastlegend.gui.GuiMain;
import com.sakura.thelastlegend.gui.TabScreenNew;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.Mob;
import Objectgame.Quest;
import Objectgame.TileMap;
import android.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameCanvas extends SurfaceView implements SurfaceHolder.Callback, IActionListener {

	public static int transY = 10;
	public static byte opacityTab = 70;
	private GThread gThread;
	public static GameCanvas instance;
	static boolean bRun;
	public static mGraphics g = new mGraphics();
	public static Activity gCanvas;
	public static GameScreen gameScreen;
	public static float Tile; //ti le
	public static int realWidth,realHeight;
	// Vibrator
	
	public static Vibrator vibrator;
	public Display display;
	
	// 0 1 2 3 4 5 6 7 8 9 * LSoft RSoft
	public static boolean[] keyPressed = new boolean[15];
	public static boolean[] keyReleased = new boolean[15];
	public static boolean[] keyHold = new boolean[15];
	public static boolean isTouch = false; // ---> Màn hình cảm ứng
	public static boolean isTouchControl; // ----> Điều khiển bằng cảm ứng
	public static boolean isTouchControlSmallScreen; // ----> Điều khiển bằng cảm ứng màn hình nhỏ
	public static boolean isTouchControlLargeScreen; // ----> Điều khiển bằng cảm ứng màn hình lớn
	public static boolean isPointerDown;
	public static boolean isPointerClick;
	public static boolean isPointerJustRelease;
	public static boolean isWrongVersion= false;
	
	public static int px, py, pxFirst, pyFirst, pxLast, pyLast;
	public static int gameTick, taskTick;
	public static boolean isEff1, isEff2;
	public static long timeTickEff1, timeTickEff2;
	public static boolean isTouchKey = true;
	public static boolean isStore = true;

	public static Context Context;
	public static int zoomLevel = 1;
	public static Resources Res;
	public static int w, h, rw, rh, hw, hh, wd3, hd3, w2d3, h2d3, w3d4, h3d4, wd6, hd6; // rw: com.sakura.thelastlegend.real widht
	public static Screen currentScreen;

	public static Menu menu = new Menu();
	public static LoginScreen loginScreen;
	public static LanguageScr languageScr;
	public static Dialog currentDialog;
	public static MsgDlg msgdlg = new MsgDlg();
	public static InputDlg inputDlg;
	public static Input2Dlg input2Dlg;
	public static Vector listPoint;
	public static Paint paint;
	public static Position[] arrPos = new Position[4]; 
	public static Vector currentPopup = new Vector();
	public static boolean lowGraphic = false;
	public static boolean isLoading;
	public static boolean isMoveNumberPad = false;
	public static boolean isGPRS = true,  isBallEffect = false;
	public static mBitmap imgBG[];
	public static int skyColor, curPos=0, idx, timeBallEffect;
	public static int[] imgBGWidth;
	public static TabScreenNew AllInfo/* = new TabScreenNew()*/;
	
	public static int hText = 14;

	public  static String thongbao = "";
	public  static int indexThongBao;
	public static boolean isPaintThongBao;
	public static int wCommand, hCommand = 25;


	public GameCanvas(Context context) {
		super(context);
		JSONParser.parseItemPayment();
		// TODO Auto-generated constructor stub
		Context = context;

		gThread = new GThread(this);

		instance = this;
		Res = getResources();
		
		GameScreen.d = (w > h ? w : h) + 20;
		
		
		// bringToFront();
		setKeepScreenOn(true);
		
		// init sound
//		Music.init(GameCanvas.gCanvas);
//		Music.setVolume(0, 0,100);
		
		// Get instance of Vibrator from current Context
		vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		// make sure we get key events
		setFocusable(true);
		setFocusableInTouchMode(true);
		// register the class itself as the callback
		getHolder().addCallback(this);
		setCanvasAct(GameMidlet.instance);
		initGameCanvas();
		SplashScreen.loadSplashScr();
		GameCanvas.currentScreen = new SplashScreen();
		listPoint = new Vector();
		languageScr = new LanguageScr();
		AllInfo = new TabScreenNew();
		loadImages();
		
	}

	private void loadImages() {
		imgShuriken = GameCanvas.loadImage("/u/f.png");
		imgCheckPass = GameCanvas.loadImage("/u/check.png");
		
		for (int i = 0; i < 3; i++) {
			imgBorder[i] = GameCanvas.loadImage("/hd/bd" + i + ".png");
		}
		CreateCharScreen.loadImage();
		borderConnerW = mGraphics.getImageWidth(imgBorder[0]);
		borderConnerH = mGraphics.getImageHeight(imgBorder[0]);
		borderLineW = mGraphics.getImageWidth(imgBorder[1]);
		borderLineH = mGraphics.getImageHeight(imgBorder[1]);
		borderCenterW = mGraphics.getImageWidth(imgBorder[2]);
		borderCenterH = mGraphics.getImageHeight(imgBorder[2]);
		
		SplashScreen.imgLogo = Image.getResizedBitmap(Image.loadImageFromAsset("njOnline.jpg"));
	}

	@Override
	public void onDraw(Canvas canvas) {
		if(Tile!=1){
			canvas.save();
			canvas.scale(Tile, Tile);//
		}
		g.setCanvas(canvas);
		paint(g);
	    super.onDraw(canvas);
//	    canvas.restore();
	}
@Override
protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	// TODO Auto-generated method stub
	super.onMeasure(widthMeasureSpec, heightMeasureSpec);
}
	public static GameCanvas gI() {
//		 if (instance == null) {
//		 instance = new GameCanvas();
//		 }
		return instance;
	}

	public void initPaint() {
		paint = new com.sakura.thelastlegend.domain.model.Paint();
	}

	public void initGameCanvas() {
		mFont.loadBegin();
		initPaint();
//		loadDust();
//		loadWaterSplash();
//		if (!GameMidlet.isEngVersion) {
//			int languageID = Rms.loadRMSInt("indLanguage");
//			if (languageID < 0)
				mResources.languageID = mResources.Lang_VI;
//			else{
//				mResources.languageID = languageID;
//			}
//		}else{
//			mResources.languageID = mResources.Lang_EN;
//		}
		mResources.loadLanguage();
		SmallImage.loadBigImage();
		MsgDlg.loadbegin();
		loadDust();
		mSystem.my_Gc();
		getScreenSize();
		hw = w / 2;
		hh = h / 2;
		wd3 = w / 3;
		hd3 = h / 3;
		w2d3 = 2 * w / 3;
		h2d3 = 2 * h / 3;
		w3d4 = 3 * w / 4;
		h3d4 = 3 * h / 4;
		wd6 = w / 6;
		hd6 = h / 6;
		//Screen.initPos();
//		GameScreen.loadDataRMS();
		GameScreen.loadImages();
		LoadImageInterface.loadImage();
		loginScreen = new LoginScreen();
		inputDlg = new InputDlg();
		input2Dlg = new Input2Dlg();
//		GameScreen.loadImageBgItem();
	
//		loadImages();
		isTouch = true;
		if (GameCanvas.w >= 240)
			isTouchControl = true;
		if (GameCanvas.w < 320)
			isTouchControlSmallScreen = true;
		if (GameCanvas.w >= 320)
			isTouchControlLargeScreen = true;
		
	}

	protected void paint(mGraphics g) {
		try {

//			resetTrans(g);
//			g.setColor(0);
//			g.fillRect(0, 0, w, h);
			if (currentScreen != null && !isLoading) {
				currentScreen.paint(g);
				
			}
//			Screen.resetTranslate(g);
//			InfoDlg.paint(g); 
			
			if (currentDialog != null) {
//				GameCanvas.debug("PC", 1);
				g.setColor(0xff000000,opacityTab);
				g.fillRect(0,0,w,h);
				g.disableBlending();
				currentDialog.paint(g);
			} else if (menu.showMenu) {
//				GameCanvas.debug("PD", 1);
				menu.paintMenu(g);
			}
			if(isPaintThongBao){
				g.setColor(0xff000000,80);
				g.fillRect(GameCanvas.w-mFont.tahoma_7_white.getWidth(thongbao)-20, 10-(indexThongBao>10?10:indexThongBao), mFont.tahoma_7_white.getWidth(thongbao)+20, 20);
				mFont.tahoma_7_white.drawString(g, thongbao,GameCanvas.w-mFont.tahoma_7_white.getWidth(thongbao)-15, 12-(indexThongBao>10?10:indexThongBao), 0);
				g.disableBlending();
			}
			if(currentScreen == GameScreen.gI()){
				if(GameScreen.listInfoServer.size()>0){
					InfoServer info = (InfoServer) GameScreen.listInfoServer.elementAt(0);
					if(info!=null)
						info.paint(g);
				}else if(GameScreen.listWorld.size()>0){
					InfoServer info = (InfoServer) GameScreen.listWorld.elementAt(0);
					if(info!=null)
						info.paint(g);
				}
			 }
//			g.translate(-g.getTranslateX(), -g.getTranslateY());
//			g.setClip(0, 0, w, h);	
//			for (int i = 0; i < 4; i++) {
//				g.setColor(Color.RED);
//				g.fillRect(arrPos[i].x - 1, arrPos[i].y - 1, 3, 3);
//			}
			
		} catch (Exception e) {
			e.printStackTrace();
			// showErrorForm(1, e.getMessage() + " " + e.toString());
		}
		
	}

	/**
	 * @return
	 */

	public static void endDlg() {
		GameCanvas.inputDlg.tfInput.setMaxTextLenght(500);
		GameCanvas.input2Dlg.tfInput.setMaxTextLenght(500);
		GameCanvas.input2Dlg.tfInput2.setMaxTextLenght(500);
		msgdlg.isPaintCham = false;
		currentDialog = null;
	}

	public static void startOKDlg(String info) {
		msgdlg.setInfo(info, null, new Command(mResources.OK, GameCanvas.instance ,8882,null), null);
//		LoginScreen.isAutoLogin = false;
//		SplashScreen.imgLogo = null;

		msgdlg.isPaintCham = false;
		currentDialog = msgdlg;
	}

	public static void startWaitDlg(String info) {
		msgdlg.setInfo(info, null, new Command(mResources.CANCEL, GameCanvas.instance,8882,null), null);
		msgdlg.isPaintCham = true;
		currentDialog = msgdlg;
		msgdlg.isWait = true;
	}
	
	public static void startWaitDlg(String info, Boolean isBlank) {
		msgdlg.setInfo(info, null, new Command(mResources.CANCEL, GameCanvas.instance,8882,null), null);
		currentDialog = msgdlg;
		msgdlg.isPaintCham = true;
		msgdlg.isWait = true;
	}

	public static void startOKDlg(String info, boolean isError) {
		startOKDlg(info);
	}

	public static void startWaitDlg() {
		startWaitDlg(mResources.PLEASEWAIT);
	}

	

	public static void startOK(String info, int actionID, Object p) {
		// BB: move command from left to center
		msgdlg.setInfo(info, null, new Command(mResources.OK, instance, actionID, p), null);
		msgdlg.isPaintCham = false;
		msgdlg.show();
	}
	
	public static void startWaitDlgWithoutCancel() {
		msgdlg.timeShow = 500;
		msgdlg.setInfo(mResources.PLEASEWAIT, null, null, null);
		currentDialog = msgdlg;
		msgdlg.isWait = true;
	}
	
	public static void startYesNoDlg(String info, int iYes,Object pYes,int iNo,Object pNo) {
		msgdlg.setInfo(info, new Command(mResources.YES, instance,iYes,pYes), new Command("", instance,iYes,pYes), new Command(mResources.NO,instance, iNo,pNo));
		msgdlg.isPaintCham = true;
		msgdlg.show();
	}
	
	public static void startYesNoDlg(String info, Command cmdYes, Command cmdNo) {
		cmdYes.caption = mResources.YES;
		cmdNo.caption = mResources.NO;
		
		msgdlg.setInfo(info, cmdYes, null, cmdNo);
		msgdlg.isPaintCham = true;
		msgdlg.show();
	}

	public static boolean lockNotify;
	public static int tNotify;

	public void update() {

		gameTick=(gameTick+1)%1000;
		GameCanvas.debug("A", 0);
		if (currentScreen != null) {
			if (currentDialog != null) {
				GameCanvas.debug("B", 0);
				currentDialog.update();
				
			} else if (menu.showMenu) {
				GameCanvas.debug("C", 0);
				menu.updateMenuKey();
				menu.updateMenu();
				GameCanvas.debug("D", 0);
			}
			GameCanvas.debug("E", 0);
			

			if(currentDialog==null)
			currentScreen.updateKey();
			if(!isLoading/*&&currentDialog==null*/)
				currentScreen.update();
			if(isPaintThongBao){
				indexThongBao++;
				if(indexThongBao>80){
					indexThongBao=0;
					isPaintThongBao = false;
				}
					
			}
		}
		
		GameCanvas.clearKeyPressed();
		GameCanvas.debug("Ix", 0);
		Timer.update();
		GameCanvas.debug("Hx", 0);
		InfoDlg.update();

		GameCanvas.debug("G", 0);
		if (resetToLoginScr) {
			Music.stopAll();
			resetToLoginScr = false;
			doResetToLoginScr();
		}

		if(currentScreen == GameScreen.gI()){
			if(GameScreen.listInfoServer.size()>0){
				InfoServer info = (InfoServer) GameScreen.listInfoServer.elementAt(0);
				if(info!=null){
					info.update();
				}
			}else if(GameScreen.listWorld.size()>0){
				InfoServer info = (InfoServer) GameScreen.listWorld.elementAt(0);
				if(info!=null)
					info.update();
			}
		 }
		if (GameCanvas.isPointerJustRelease){
			GameCanvas.isPointerJustRelease = false;
		}
	}

	public static int[] bgSpeed;
	public int[] dustX, dustY, dustState;

	public boolean startDust(int dir, int x, int y) {
		if (lowGraphic)
			return false;
		int i = dir == 1 ? 0 : 1;
		if (dustState[i] != -1)
			return false;
		dustState[i] = 0;
		dustX[i] = x;
		dustY[i] = y;
		return true;
	}

	public static int[] wsX, wsY, wsState, wsF;
	public static mBitmap imgWS[], imgShuriken, imgCheckPass;
	public static mBitmap imgDust[][];


	public void loadDust() {
		// if (lowGraphic || level == 9 || level == 10 || level == 8
		// || level == 14)
		if (lowGraphic)
			return;
		if (imgDust == null) {
			imgDust = new mBitmap[2][5];
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 5; j++)
					imgDust[i][j] = GameCanvas.loadImage(("/e/d" + i) + (j + ".png"));
		}
		dustX = new int[2];
		dustY = new int[2];
		dustState = new int[2];
		dustState[0] = dustState[1] = -1;
	}
	
	public void updateDust() {
		// if (lowGraphic || level == 9 || level == 10 || level == 8
		// || level == 14)
		if (lowGraphic)
			return;

		for (int i = 0; i < 2; i++) {
			if (dustState[i] != -1) {
				dustState[i]++;
				if (dustState[i] >= 5)
					dustState[i] = -1;
				if (i == 0)
					dustX[i]--;
				else
					dustX[i]++;
				dustY[i]--;
			}
		}
	}

	public static boolean isPaint(int x, int y) {
		if (x < GameScreen.cmx)
			return false;
		if (x > GameScreen.cmx + GameScreen.gW)
			return false;
		if (y < GameScreen.cmy)
			return false;
		if (y > GameScreen.cmy + GameScreen.gH + 30)
			return false;
		return true;
	}

	public void paintDust(mGraphics g) {
		// if (lowGraphic || level == 9 || level == 10 || level == 8
		// || level == 14)
		if (lowGraphic)
			return;
		for (int i = 0; i < 2; i++) {
			if (dustState[i] != -1) {
				if(isPaint(dustX[i], dustY[i]))
					g.drawImage(imgDust[i][dustState[i]], dustX[i], dustY[i], 3);
			}
		}
	}

	public void loadWaterSplash() {
		if (lowGraphic)
			return;
		imgWS = new mBitmap[3];
		for (int i = 0; i < 3; i++)
			imgWS[i] = GameCanvas.loadImage("/e/w" + i + ".png");
		wsX = new int[2];
		wsY = new int[2];
		wsState = new int[2];
		wsF = new int[2];
		wsState[0] = wsState[1] = -1;
	}

	public boolean startWaterSplash(int x, int y) {
		if (lowGraphic)
			return false;
		int i = wsState[0] == -1 ? 0 : 1;
		if (wsState[i] != -1)
			return false;
		wsState[i] = 0;
		wsX[i] = x;
		wsY[i] = y;
		return true;
	}

	public void updateWaterSplash() {
		if (lowGraphic)
			return;
		for (int i = 0; i < 2; i++) {
			if (wsState[i] != -1) {
				wsY[i] -= 1;
				if (gameTick % 2 == 0) {
					wsState[i]++;
					if (wsState[i] > 2)
						wsState[i] = -1;
					else
						wsF[i] = wsState[i];
				}
			}
		}
	}

	public static void paintShukiren(int x, int y, mGraphics g, boolean noRotate) {
		int f;
//		if (noRotate)
//			f = 0;
//		else
			f = gameTick % 3;
		g.drawRegion(imgShuriken, 0, f * 16, 16, 16, 0, x, y, mGraphics.HCENTER | mGraphics.VCENTER);
	}

	// Catch key-down input and save in to Input Array
	public static long lastTimePress = 0;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		lastTimePress = System.currentTimeMillis();
		int changeKeyCode = 0;
//System.out.println("keyCode: "+keyCode);
		switch (keyCode) {
		
		case KeyEvent.KEYCODE_1:
			changeKeyCode = 1;
			break;
		
		case KeyEvent.KEYCODE_DPAD_UP:
		case KeyEvent.KEYCODE_2:
			changeKeyCode = 2;
			break;
			
		case KeyEvent.KEYCODE_3:
			changeKeyCode = 3;
			break;
			
		case KeyEvent.KEYCODE_4:
		case KeyEvent.KEYCODE_DPAD_LEFT:
			changeKeyCode = 4;
			break;
			
		case KeyEvent.KEYCODE_DPAD_CENTER:
		case KeyEvent.KEYCODE_5:
		case KeyEvent.KEYCODE_ENTER:
			changeKeyCode = 5;
			break;
		
		case KeyEvent.KEYCODE_DPAD_RIGHT:
		case KeyEvent.KEYCODE_6:
			changeKeyCode = 6;
			break;
			
		case KeyEvent.KEYCODE_7:
			changeKeyCode = 7;
			break;	
			
		case KeyEvent.KEYCODE_DPAD_DOWN:
		case KeyEvent.KEYCODE_8:
			changeKeyCode = 8;
			break;

		
		case KeyEvent.KEYCODE_9:
			changeKeyCode = 9;
			break;
			
		
		case KeyEvent.KEYCODE_MENU:
			changeKeyCode = 12;
			break;
		case KeyEvent.KEYCODE_BACK:
			changeKeyCode = 14;
			break;
		}

		mapKeyPress(changeKeyCode);
		

		if (keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

//		clearKeyPressed();
//		clearKeyHold();	
		// curScr.keyReleased(changeKeyCode);

		if (keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	

	private int getIndex(MotionEvent event) {
		return  (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

//		int action = event.getAction();

//		switch (action) {
//		case MotionEvent.ACTION_DOWN:
//			onPointerPressed(
//					(int) (event.getX()/Tile),
//					(int) (event.getY()/Tile));
//			break;
//		case MotionEvent.ACTION_MOVE:
//			onPointerDragged(
//					(int) (event.getX()/Tile),
//					(int) (event.getY()/Tile));
//			break;
//		case MotionEvent.ACTION_UP:
//			onPointerReleased(
//					(int) (event.getX()/Tile),
//					(int) (event.getY()/Tile));
//			break;
//		}
//		return true;
//	
		
		int action = event.getAction() & MotionEvent.ACTION_MASK;
        int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
        int pointerId = event.getPointerId(pointerIndex);
		switch (action) {		
		case MotionEvent.ACTION_DOWN:
			pointerPressed((int) (event.getX()/(mGraphics.zoomLevel*Tile)), (int) (event.getY()/(mGraphics.zoomLevel*Tile)));
			break;
		case MotionEvent.ACTION_MOVE:
			pointerDragged((int) (event.getX()/(mGraphics.zoomLevel*Tile)), (int) (event.getY()/(mGraphics.zoomLevel*Tile)));
			break;
		case MotionEvent.ACTION_UP:
			pointerReleased((int) (event.getX()/(mGraphics.zoomLevel*Tile)), (int) (event.getY()/(mGraphics.zoomLevel*Tile)));
			break;
	
		}
	
		return true;
		// return super.onTouchEvent( event);
	}

	public static int keyAsciiPress;

	protected void keyPressed(int keyCode) {
		// Screen.test="key: "+keyCode;
		lastTimePress = System.currentTimeMillis();
		if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 122) || keyCode == 10 || keyCode == 8 || keyCode == 13 || keyCode == 32) {
			keyAsciiPress = keyCode;
		}
		mapKeyPress(keyCode);
	}

	public void mapKeyPress(int keyCode) {
//		if (Char.lockKey)
//			return;
		switch (keyCode) {
		case 0:
			keyHold[0] = true;
			keyPressed[0] = true;
			break;
			
		case 1:// 1
			keyHold[1] = true;
			keyPressed[1] = true;
			break;
			
		case 2:
			if (currentScreen instanceof GameScreen)
				if (Char.myChar().isAttack) {
					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[2] = true;
			keyPressed[2] = true;
			break;
			
		case 3:// 3
			keyHold[3] = true;
			keyPressed[3] = true;
			break;
			
		case 4:
			if (currentScreen instanceof GameScreen)
				if (Char.myChar().isAttack) {
					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[4] = true;
			keyPressed[4] = true;

			break;
			
		case 5:
			if (currentScreen instanceof GameScreen)
				if (Char.myChar().isAttack) {
					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[5] = true;
			keyPressed[5] = true;
			
			break;
			
		case 6:
			if (currentScreen instanceof GameScreen)
				if (Char.myChar().isAttack) {

					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[6] = true;
			keyPressed[6] = true;
			break;
			
		case 7:// 7
			keyHold[7] = true;
			keyPressed[7] = true;
			break;
			
		case 8:
			if (currentScreen instanceof GameScreen)
				if (Char.myChar().isAttack) {
					clearKeyHold();
					clearKeyPressed();

				}
			keyHold[8] = true;
			keyPressed[8] = true;
			break;
			
		case 9:// 9
			keyHold[9] = true;
			keyPressed[9] = true;
			break;

		case 12:
			keyHold[12] = true;
			keyPressed[12] = true;
			// Soft1
			break;
		case 13:
			keyHold[13] = true;
			keyPressed[13] = true;
			// Soft2
			break;
		case 14:
			keyHold[14] = true;
			keyPressed[14] = true;
			// Soft2
			break;

		}

		if (currentScreen != null) {
			if (currentDialog != null) {
				currentDialog.update();
				return;
			}else if( menu.showMenu){
				menu.updateMenuKey();
				return;
			}
			currentScreen.updateKey();

		}
	}

	// Catch key-up input and clear from Input Array
	protected void keyReleased(int keyCode) {
		keyAsciiPress = 0;
		mapKeyRelease(keyCode);
	}

	public void mapKeyRelease(int keyCode) {

		switch (keyCode) {
		case 48:
			keyHold[0] = false;
			keyReleased[0] = true;
			return;
		case 49:// 1
			keyHold[1] = false;
			keyReleased[1] = true;
			return;
		case 51:// 3
			keyHold[3] = false;
			keyReleased[3] = true;
			return;
		case 55:// 7
			keyHold[7] = false;
			keyReleased[7] = true;
			return;
		case 57:// 9
			keyHold[9] = false;
			keyReleased[9] = true;
			return;
		case 42:
			keyHold[10] = false;
			keyReleased[10] = true;
			return; // Key [*]
		case 35:
			keyHold[11] = false;
			keyReleased[11] = true;
			return; // Key [#]
		case -6:
		case -21:
			keyHold[12] = false;
			keyReleased[12] = true;
			return; // Soft1
		case -7:
		case -22:
			keyHold[13] = false;
			keyReleased[13] = true;
			return; // Soft2
		case -5:
		case 10:
			keyHold[5] = false;
			keyReleased[5] = true;
			return; // [i]
		case -1:
		case -38:

			keyHold[2] = false;

			return; // UP
		case -2:
		case -39:

			keyHold[8] = false;
			return; // DOWN
		case -3:

			keyHold[4] = false;
			return; // LEFT
		case -4:

			keyHold[6] = false;
			return; // RIGHT
		}

	}

	
	protected void pointerSecondPressed(int x, int y) {
		isPointerDown = true;
		isPointerClick = true;
		pxFirst = x;
		pyFirst = y;
		pxLast = x ;
		pyLast = y ;
		px = x ;
		py = y ;
		
	}
	
	public static boolean isHoldPress(){
		if(System.currentTimeMillis() - lastTimePress >= 800)
			return true;
		return false;
	}
	
	protected void pointerPressed(int x, int y) {
		isPointerDown = true;
		isPointerClick = true;
		isPointerJustRelease = false;
		lastTimePress = System.currentTimeMillis();
//		Cout.println(isPointerClick+" pointerPressed    "+isPointerDown +" release  "+isPointerJustRelease);
		pxFirst = x;
		pyFirst = y;
		pxLast = x ;
		pyLast = y ;
		px = x ;
		py = y ;
	}
	

	protected void pointerDragged(int x, int y) {
		if (com.sakura.thelastlegend.domain.model.Res.abs(x - pxLast) >= 10*mGraphics.zoomLevel || com.sakura.thelastlegend.domain.model.Res.abs(y - pyLast) >= 10*mGraphics.zoomLevel){
			isPointerClick = false;
//			Cout.println("pointerDragged  falseeeeeeeeeee ");
			
		}
//		isPointerDown = true;
		isPointerClick = isPointerJustRelease = false;
		curPos++;
		if (curPos > 3)
			curPos = 0;
		arrPos[curPos] = new Position(x, y);
		px = x ;
		py = y ;

//		Cout.println(isPointerClick+" pointerDragged    "+isPointerDown +" release  "+isPointerJustRelease);
		// pXLast = x ;
		// pYLast = y ;
	}



	protected void pointerReleased(int x, int y) {
		isPointerDown = false;
		isPointerJustRelease = true;
		Screen.keyTouch = -1;
		px = x ;
		py = y ;
//		Cout.println(isPointerClick+" pointerReleased    "+isPointerDown +" release  "+isPointerJustRelease);
		clearKeyPressed();
		clearKeyHold();
		// CScreen.clearKey();
		// curScr.pointerReleased(pX, pY);
	
	}

	public static boolean isPointerInGame(int x, int y, int w, int h) {
		
		int px1 = px + GameScreen.cmx, py1 = GameScreen.cmy + py;
		if (!isPointerDown && !isPointerJustRelease)
			return false;
		if (px1 >= x && px1 <= x + w && py1 >= y && py1 <= y + h){
			return true;}
		return false;
	}
	
	public static boolean isPointerInRoll(int x, int y, int w, int h, Scroll scroll) {
		int px1 = px + scroll.cmx, py1 = scroll.cmy + py;
		if (!isPointerDown && !isPointerJustRelease)
			return false;
		if (px1 >= x && px1 <= x + w && py1 >= y && py1 <= y + h)
			return true;
		return false;
	}
	
	public static boolean isPointerHoldIn(int x, int y, int w, int h) {
		
		if (!isPointerDown && !isPointerJustRelease)
			return false;
		if (isPointerDown&&px >= x && px <= x + w && py >= y && py <= y + h){
			return true;
			}
		return false;
	}

	public static void clearKeyPressed() {
//		isPointerJustRelease = false;
		for (int i = 0; i < keyPressed.length; i++)
			keyPressed[i] = false;
	}

	public static void clearKeyHold() {
		for (int i = 0; i < keyHold.length; i++)
			keyHold[i] = false;
	}


	public void setCanvasAct(Activity Act) {
		gCanvas = Act;
		getScreenSize();
	}

	public static void debug(String str, int index) {
	//	System.out.println(">>>>>> " + str + index);
	}

	public static void debug(String str) {
		System.out.println(">>>>>> " + str);
	}

	public static String getMoneys(int m) {
		String str = "";
		int mm = m / 1000 + 1;
		for (int i = 0; i < mm; i++) {
			if (m >= 1000) {
				int a = m % 1000;
				if (a == 0)
					str = ".000" + str;
				else if (a < 10)
					str = ".00" + a + str;
				else if (a < 100)
					str = ".0" + a + str;
				else
					str = "." + a + str;
				m /= 1000;
			} else {
				str = m + str;
				break;
			}
		}
		return str;
	}
	
	public void getScreenSize() {
		// get size of screen
		display = gCanvas.getWindowManager().getDefaultDisplay();
		w = display.getWidth() ;
		h = display.getHeight() ;
		GameScreen.d = (w > h ? w : h) + 20;
//		if (w < h && Build.VERSION.SDK_INT >= 11) {
//			h = display.getWidth();
//			w = display.getHeight();
//		}

		w += (3 - w % 3);
		h += (3 - h % 3);
		
		GameMidlet.Model = Build.MODEL;
		if (GameMidlet.Model.compareTo("A101IT") == 0){// for archos 10.1 in
			w -= 44;
			
		}
//		else if (GameMidlet.Model.compareTo("GT-P7500") == 0) // for galaxy tab
//			h -= 48;
		Cout.println("w: " + w + ", h: " + h);

		rw = w;
		rh = h;
		realHeight = h;
		realWidth = w;
		Tile = 1f;
		if(h>1280){
			mGraphics.zoomLevel = 4;
			zoomLevel = 4;
			float demchia = 960;
			Tile = realHeight/demchia;
		}
		else if ((w * h) >= (1280 * 960)) {
			mGraphics.zoomLevel = 4;
			zoomLevel = 4;
//			float demchia = 960;
//			Tile = realHeight/demchia;
		} else if ((w * h) >= (960 * 720)) {
			mGraphics.zoomLevel = 3;
			zoomLevel = 3;
		}else if ((w * h) >= (600 * 400)) {
			mGraphics.zoomLevel = 2;
			zoomLevel = 2;
		}else if ((w * h) >= (600 * 400)){
			mGraphics.zoomLevel = 2;
			zoomLevel = 2;
		}else 
		{
			mGraphics.zoomLevel = 1;
			zoomLevel = 1;
			float demchia = 360;
			Tile = realWidth/demchia;
		}
//		mGraphics.zoomLevel = 4;
//		zoomLevel = 4;
		float wk = w;
		float hk = h;
//		System.out.println("outtt "+mGraphics.zoomLevel+"  Tile  "+Tile);
		Cout.println(mGraphics.zoomLevel+"  Tile  "+Tile);
		wk = (wk/(mGraphics.zoomLevel*Tile));
		hk = (hk/(mGraphics.zoomLevel*Tile));
		w = (int)(wk);
		h = (int)(hk);
		hw = w / 2;
		hh = h / 2;
	}

	// TODO get id of drawable file
	// how to use
	// String filename = "filename" + number(if have);
	// int id = getDrawableID(filename);
	// imgPigBozz[i] = BitmapFactory.decodeResource(resources, id);
	
	public static Class<R.drawable> res = R.drawable.class;
	public static int getDrawableID(String filename) {
		try {
			java.lang.reflect.Field field = res.getField(filename);
			int drawableId = field.getInt(null);
			return drawableId;
		} catch (Exception e) {
			Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return 0;
	}

	public final void pause() {
		// bPause = true, mean main While-loop do nothing.
		// bPause = true;
		// Sound.pauseMusic();
	}

	public final void resume() {
		// resume App
		// bPause = false;
		// Sound.resumeMusic();
	}

	

	public class GThread extends Thread {
		private GameCanvas mn;
		private SurfaceHolder mHolder;
		private boolean mRun = false;

		public GThread(GameCanvas viewCanvas) {
			mn = viewCanvas;
			mHolder = mn.getHolder();
		}

		public void setRunning(boolean run) {
			mRun = run;
		}

		@SuppressLint("WrongCall") @Override
		public void run() {

			// call for run handler
			Looper.prepare();

			
			Canvas canvas = null;
			while (mRun) {

				long l1 = System.currentTimeMillis();
				if(l1 - timeTickEff1 >= 780 && !isEff1){
					timeTickEff1 = l1;
					isEff1 = true;
				}
				else
					isEff1 = false;
				if(l1 - timeTickEff2 >= 7800 && !isEff2){
					timeTickEff2 = l1;
					isEff2 = true;
				}
				else
					isEff2 = false;
				if (taskTick > 0)
					taskTick--;
				gameTick++;
				if (gameTick > 10000) {
					if (System.currentTimeMillis() - lastTimePress > 20000
							&& GameCanvas.currentScreen == GameCanvas.loginScreen) {
						GameCanvas.gCanvas.finish();
					}
					gameTick = 0;
				}
				
				update();
				// unclock canvas for draw
				if(!Char.ischangingMap){
					canvas = mHolder.lockCanvas();
					if (canvas != null) {					
						mn.onDraw(canvas);
						mHolder.unlockCanvasAndPost(canvas);
					}
				}
				
				// Synchronize time
				long l2 = System.currentTimeMillis() - l1;
				//System.out.println("~~~~~~frame: "+l2);
				try {
					if (l2 < 40)
						Thread.sleep(40 - l2);
					else
						Thread.sleep(1);
				} catch (InterruptedException e) {
				}
				//GameCanvas.debug("Zzz", 0);
			}
		}

	}

	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		if (!gThread.isAlive()) {
			gThread = new GThread(this);
			gThread.setRunning(true);
			gThread.start();
		}
	}

	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		if (gThread.isAlive()) {
			gThread.setRunning(false);
		}
	}

	public static boolean isPointer(int x, int y, int w, int h) {
		if (!isPointerDown && !isPointerJustRelease){
			return false;
		}
		if (px >= x && px <= x + w && py >= y && py <= y + h)
			return true;
		return false;
	}
	public static boolean isPointer222(int x, int y, int w, int h) {
//		if (!isPointerDown && !isPointerJustRelease){
//			return false;
//		}
		if (px >= x && px <= x + w && py >= y && py <= y + h)
			return true;
		return false;
	}
	public static void connect() {
		GameMidlet.PORT = 19158;
//		if(!LoginScreen.isTest)
//			GameMidlet.IP = "103.15.51.32";

		GameMidlet.IP = "10.235.230.45";
		// String host = "socket://" + GameMidlet.IP + ":" + GameMidlet.PORT;
		Session_ME.gI().connect(GameMidlet.IP, GameMidlet.PORT);
//		if(!LoginScreen.isAutoLogin)
//			GameCanvas.startWaitDlg(mResources.LOGGING);
//		else
//			GameCanvas.startWaitDlg(mResources.LOGGING, true);
	}
	
	private void doResetToLoginScr() {
		GameCanvas.loginScreen.switchToMe();
		try {
			Char.clearMyChar();
			GameScreen.clearGameScr();
			GameScreen.resetAllvector();
			GameCanvas.endDlg();
			InfoDlg.hide();
			GameScreen.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
			GameScreen.cmx = 100;
			GameCanvas.loadBG(TileMap.bgID);
//			GameScreen.vParty.removeAllElements();
			GameScreen.vFriend.removeAllElements();
			GameScreen.vEnemies.removeAllElements();
			GameScreen.vClan.removeAllElements();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void updateBallEffect() {
		if (isBallEffect) {
			timeBallEffect--;
			if (timeBallEffect < 0)
				isBallEffect = false;
		}
	}

	// ============================paint bgggggggggggggggggggg
	public static boolean paintBG;
	public static int gsskyHeight;
	public static int gsgreenField1Y, gsgreenField2Y, gshouseY, gsmountainY,
			bgLayer0y, bgLayer1y;
	public static mBitmap imgCloud, imgSun;
	public static mBitmap[] imgBorder = new mBitmap[3];
	public static int borderConnerW, borderConnerH, borderLineW, borderLineH, borderCenterW, borderCenterH;
	public static int cloudX[], cloudY[], sunX, sunY;

	public static void updateBG() {
		if(typeBg == 0)
			return;
		if (lowGraphic)
			return;
		if (imgCloud == null)
			return;
		for (int i = 0; i < cloudX.length; i++) {
			if (gameTick % ((i + 2) << 3) == 0) {
				cloudX[i] += 1;
				if (cloudX[i] > GameScreen.gW + (Image.getWidth(imgCloud) >> 1))
					cloudX[i] = -(Image.getWidth(imgCloud) >> 1);
			}
		}
	}

	public static void paintBGGameScr(mGraphics g) {
		// Paint the Sky
		if (paintBG && !lowGraphic) {
//			g.setColor(skyColor);
//			g.fillRect(0, 0, GameScreen.gW, gsskyHeight);
			// Paint the GreenField1
			if (typeBg >= 0 && typeBg <= 1) {
				if(GameCanvas.currentScreen == SelectCharScreen.gI()||GameCanvas.currentScreen == CreateCharScreen.gI()){
					if (imgBG[3] != null)
						for (int i = -((GameScreen.cmx >> 4) % 64); i < GameScreen.gW; i += 64)
							g.drawImage(imgBG[3], i, gsmountainY, 0);
					if (imgBG[2] != null)
						for (int i = -((GameScreen.cmx >> 3) % 192); i < GameScreen.gW; i += Image.getWidth(imgBG[2])/*192*/)
							g.drawImage(imgBG[2], i, gshouseY, 0);
					
					// // Paint the GreenField2
					if (imgBG[1] != null){
						
//						for (int i = -((GameScreen.cmx >> 2) % 24); i < GameScreen.gW; i += Image.getWidth(imgBG[1])/*24*/)
						for(int i = GameCanvas.w/2; i < GameCanvas.w + Image.getWidth(imgBG[1]); i += (Image.getWidth(imgBG[1])) ){
								
								g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER);
							}
								
						for(int i = GameCanvas.w/2; i > -Image.getWidth(imgBG[1]); i -= (Image.getWidth(imgBG[1]) ) ){
							g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER);
						}
//					for(int j = 0; j > 0; j -= (Image.getWidth(imgBG[1]))){
//						g.drawImage(imgBG[1], j, gsgreenField2Y, mGraphics.BOTTOM | mGraphics.HCENTER);
//					}
//						for(int i = GameCanvas.w/2; i > 0; i += (Image.getWidth(imgBG[1]) - 100))
//							g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER);
//						for(int k = GameCanvas.w/2; k < GameCanvas.w; k -= Image.getWidth(imgBG[1]) )
//							g.drawImage(imgBG[1], k, gsgreenField2Y - 120, mGraphics.HCENTER | mGraphics.HCENTER);
						
					}
					if (imgBG[0] != null)
						for (int i = -((GameScreen.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScreen.gW; i += Image.getWidth(imgBG[0])/*24*/)
							g.drawImage(imgBG[0], i, gsgreenField1Y - 150, 0);
					// // Paint Mountain
//					if (imgBG[3] != null)
//						for (int i = -((GameScreen.cmx >> 4) % 64); i < GameScreen.gW; i += 64)
//							g.drawImage(imgBG[3], i, gsmountainY, 0);
					// Paint sun and cloud
					if (imgSun != null)
						g.drawImage(imgSun, sunX, sunY, 3);
					if (imgCloud != null)
						for (int i = 0; i < 2; i++)
							g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
					// Paint House
					
				}else if(GameCanvas.currentScreen == GameCanvas.loginScreen || GameCanvas.currentScreen == GameCanvas.languageScr){
					if (imgBG[3] != null)
						for (int i = -((GameScreen.cmx >> 4) % Image.getWidth(imgBG[3])); i < GameScreen.gW; i += Image.getWidth(imgBG[3]))
							g.drawImage(imgBG[3], i, gsmountainY, 0);
//					if (imgBG[0] != null)
//						for (int i = -((GameScreen.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScreen.gW; i += Image.getWidth(imgBG[0]))
//							g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
					// // Paint the GreenField2
					if (imgBG[2] != null)
						for (int i = -((GameScreen.cmx >> 3) % Image.getWidth(imgBG[2])); i < GameScreen.gW; i += Image.getWidth(imgBG[2]))
							g.drawImage(imgBG[2], Image.getWidth(imgBG[1]) - 50, gshouseY, 0);
					if (imgBG[1] != null)
						for (int i = -((GameScreen.cmx >> 2) % Image.getWidth(imgBG[1])); i < GameScreen.gW; i += Image.getWidth(imgBG[1]))
							g.drawImage(imgBG[1], 0, gshouseY + 3, 0);
					// // Paint Mountain
					
					// Paint sun and cloud
					if (imgSun != null)
						g.drawImage(imgSun, sunX, sunY, 3);
					if (imgCloud != null)
						for (int i = 0; i < 2; i++)
							g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
					// Paint House
					
					if (imgBG[0] != null)
						for (int i = -((GameScreen.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScreen.gW; i += Image.getWidth(imgBG[0]))
							g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
				}
			

				// ==========================
			} else if (typeBg >= 2 && typeBg <= 6) {
//				System.out.println("PAINT BG GAME SCR");
//				if (imgSun != null) {
//					// Paint sun and cloud
//					g.drawImage(imgSun, sunX, sunY, 3);
//				}
//				if (imgCloud != null)
//					for (int i = 0; i < cloudX.length; i++)
//						g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);

//				if(typeBg!=2){
//					 Paint Mountain
//					if (imgBG[3] != null)
//						for (int i = -((GameScreen.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScreen.gW; i += imgBGWidth[3])
//							g.drawImage(imgBG[3], i, gsmountainY, 0);
//					// Paint House
//					if (imgBG[2] != null){
//						for (int i = -((GameScreen.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScreen.gW; i += imgBGWidth[2])
//							g.drawImage(imgBG[2], i, gshouseY + 40, 0);
//					}
					// Paint the GreenField2
					if (imgBG[1] != null){
						for (int i = -((GameScreen.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScreen.gW; i += imgBGWidth[1])
							g.drawImage(imgBG[1], i,  GameCanvas.h-imgBG[1].getHeight() , 0);
					}
//					// Paint the GreenField1
//					if (imgBG[0] != null) {
//						for (int i = -((GameScreen.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScreen.gW; i += imgBGWidth[0])
//							g.drawImage(imgBG[0], i, bgLayer0y, 0);
//					}
//				}
				
			} else if (typeBg >= 7 && typeBg <= 12) {
				g.setColor(skyColor);
				g.fillRect(0, 0, GameScreen.gW, GameScreen.gH);
				if (typeBg != 8 && imgBG[3] != null) {

					for (int i = -((GameScreen.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScreen.gW; i += imgBGWidth[3])
						if (typeBg == 11 || typeBg == 12) {
							g.drawImage(imgBG[3], i, GameScreen.gH - mGraphics.getImageHeight(imgBG[3]), 0);

						} else
							g.drawImage(imgBG[3], i, gsmountainY, 0);

				}

				if (typeBg != 8 && typeBg != 11 && typeBg != 12 && imgBG[2] != null) {

					if (TileMap.mapID == 45)
						g.drawImage(imgBG[2], GameScreen.gW, gshouseY, 0);
					else
						for (int i = -((GameScreen.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScreen.gW; i += imgBGWidth[2])
							g.drawImage(imgBG[2], i, gshouseY, 0);
				}
				if (typeBg != 11 && typeBg != 12 && imgBG[1] != null) {

					if (TileMap.mapID != 52)
						for (int i = -((GameScreen.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScreen.gW; i += imgBGWidth[1])
							g.drawImage(imgBG[1], i, bgLayer1y, 0);

				}
				if (TileMap.mapID == 45 || TileMap.mapID == 55) {
					g.setColor(0x110000);
					g.fillRect(0, bgLayer0y + 20, GameScreen.gW, GameScreen.gH);
				}

				if (imgBG[0] != null) {

					for (int i = -((GameScreen.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScreen.gW; i += imgBGWidth[0])
						g.drawImage(imgBG[0], i, bgLayer0y, 0);

				}

				if (imgCloud != null) {

					for (int i = 0; i < 2; i++)
						g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);

				}
			}
			// ==============================
		} else {
			g.setColor(skyColor);
			g.fillRect(0, 0, GameScreen.gW, GameScreen.gH);
		}
	}

	public static void paintBGGameScr(mGraphics g,boolean Uclip) {
		// Paint the Sky
		if (paintBG && !lowGraphic) {		
			g.setColor(skyColor);
			g.fillRect(0, 0, GameScreen.gW, gsskyHeight,Uclip);
			// Paint the GreenField1
			if (typeBg >= 0 && typeBg <= 1) {
				if(GameCanvas.currentScreen == SelectCharScreen.gI()||GameCanvas.currentScreen == CreateCharScreen.gI()){
					if (imgBG[3] != null)
						for (int i = -((GameScreen.cmx >> 4) % Image.getWidth(imgBG[3])); i < GameScreen.gW; i += Image.getWidth(imgBG[3]))
							g.drawImage(imgBG[3], i, gsmountainY, 0,Uclip);
					if (imgBG[2] != null)
						for (int i = -((GameScreen.cmx >> 3) % 192); i < GameScreen.gW; i += Image.getWidth(imgBG[2])/*192*/)
							g.drawImage(imgBG[2], i, gshouseY, 0,Uclip);
					
					// // Paint the GreenField2
					if (imgBG[1] != null){
						
//						for (int i = -((GameScreen.cmx >> 2) % 24); i < GameScreen.gW; i += Image.getWidth(imgBG[1])/*24*/)
						for(int i = GameCanvas.w/2; i < GameCanvas.w + Image.getWidth(imgBG[1]); i += (Image.getWidth(imgBG[1])) ){
								
								g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER,Uclip);
							}
								
						for(int i = GameCanvas.w/2; i > -Image.getWidth(imgBG[1]); i -= (Image.getWidth(imgBG[1]) ) ){
							g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER,Uclip);
						}
//					for(int j = 0; j > 0; j -= (Image.getWidth(imgBG[1]))){
//						g.drawImage(imgBG[1], j, gsgreenField2Y, mGraphics.BOTTOM | mGraphics.HCENTER);
//					}
//						for(int i = GameCanvas.w/2; i > 0; i += (Image.getWidth(imgBG[1]) - 100))
//							g.drawImage(imgBG[1], i, gsgreenField2Y + 100, mGraphics.BOTTOM | mGraphics.HCENTER);
//						for(int k = GameCanvas.w/2; k < GameCanvas.w; k -= Image.getWidth(imgBG[1]) )
//							g.drawImage(imgBG[1], k, gsgreenField2Y - 120, mGraphics.HCENTER | mGraphics.HCENTER);
						
					}
					if (imgBG[0] != null)
						for (int i = -((GameScreen.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScreen.gW; i += Image.getWidth(imgBG[0])/*24*/)
							g.drawImage(imgBG[0], i, gsgreenField1Y - 150, 0,Uclip);
					// // Paint Mountain
//					if (imgBG[3] != null)
//						for (int i = -((GameScreen.cmx >> 4) % 64); i < GameScreen.gW; i += 64)
//							g.drawImage(imgBG[3], i, gsmountainY, 0);
					// Paint sun and cloud
					if (imgSun != null)
						g.drawImage(imgSun, sunX, sunY, 3,Uclip);
					if (imgCloud != null)
						for (int i = 0; i < 2; i++)
							g.drawImage(imgCloud, cloudX[i], cloudY[i], 3,Uclip);
					// Paint House
					
				}else if(GameCanvas.currentScreen == GameCanvas.loginScreen || GameCanvas.currentScreen == GameCanvas.languageScr){
					if (imgBG[3] != null)
						for (int i = -((GameScreen.cmx >> 4) % Image.getWidth(imgBG[3])); i < GameScreen.gW; i += Image.getWidth(imgBG[3]))
							g.drawImage(imgBG[3], i, gsmountainY, 0,Uclip);
//					if (imgBG[0] != null)
//						for (int i = -((GameScreen.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScreen.gW; i += Image.getWidth(imgBG[0]))
//							g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
					// // Paint the GreenField2
					if (imgBG[2] != null)
						for (int i = -((GameScreen.cmx >> 3) % Image.getWidth(imgBG[2])); i < GameScreen.gW; i += Image.getWidth(imgBG[2]))
							g.drawImage(imgBG[2], Image.getWidth(imgBG[1]) - 50, gshouseY, 0,Uclip);
					if (imgBG[1] != null)
						for (int i = -((GameScreen.cmx >> 2) % Image.getWidth(imgBG[1])); i < GameScreen.gW; i += Image.getWidth(imgBG[1]))
							g.drawImage(imgBG[1], 0, gshouseY + 3, 0,Uclip);
					// // Paint Mountain
					
					// Paint sun and cloud
					if (imgSun != null)
						g.drawImage(imgSun, sunX, sunY, 3,Uclip);
					if (imgCloud != null)
						for (int i = 0; i < 2; i++)
							g.drawImage(imgCloud, cloudX[i], cloudY[i], 3,Uclip);
					// Paint House
					
					if (imgBG[0] != null)
						for (int i = -((GameScreen.cmx >> 1) % Image.getWidth(imgBG[0])); i < GameScreen.gW; i += Image.getWidth(imgBG[0]))
							g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0,Uclip);
				}
			

				// ==========================
			} else if (typeBg >= 2 && typeBg <= 6) {
//				System.out.println("PAINT BG GAME SCR");
//				if (imgSun != null) {
//					// Paint sun and cloud
//					g.drawImage(imgSun, sunX, sunY, 3);
//				}
//				if (imgCloud != null)
//					for (int i = 0; i < cloudX.length; i++)
//						g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);

//				if(typeBg!=2){
//					 Paint Mountain
					if (imgBG[3] != null)
						for (int i = -((GameScreen.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScreen.gW; i += imgBGWidth[3])
							g.drawImage(imgBG[3], i, gsmountainY, 0,Uclip);
					// Paint House
					if (imgBG[2] != null){
						for (int i = -((GameScreen.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScreen.gW; i += imgBGWidth[2])
							g.drawImage(imgBG[2], i, gshouseY + 40, 0,Uclip);
					}
					// Paint the GreenField2
					if (imgBG[1] != null){
						for (int i = -((GameScreen.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScreen.gW; i += imgBGWidth[1])
							g.drawImage(imgBG[1], i, bgLayer1y - 250, 0,Uclip);
					}
					// Paint the GreenField1
					if (imgBG[0] != null) {
						for (int i = -((GameScreen.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScreen.gW; i += imgBGWidth[0])
							g.drawImage(imgBG[0], i, bgLayer0y, 0,Uclip);
					}
//				}
				
			} else if (typeBg >= 7 && typeBg <= 12) {
				g.setColor(skyColor);
				g.fillRect(0, 0, GameScreen.gW, GameScreen.gH,Uclip);
				if (typeBg != 8 && imgBG[3] != null) {

					for (int i = -((GameScreen.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScreen.gW; i += imgBGWidth[3])
						if (typeBg == 11 || typeBg == 12) {
							g.drawImage(imgBG[3], i, GameScreen.gH - mGraphics.getImageHeight(imgBG[3]), 0,Uclip);

						} else
							g.drawImage(imgBG[3], i, gsmountainY, 0,Uclip);

				}

				if (typeBg != 8 && typeBg != 11 && typeBg != 12 && imgBG[2] != null) {

					if (TileMap.mapID == 45)
						g.drawImage(imgBG[2], GameScreen.gW, gshouseY, 0,Uclip);
					else
						for (int i = -((GameScreen.cmx >> bgSpeed[2]) % imgBGWidth[2]); i < GameScreen.gW; i += imgBGWidth[2])
							g.drawImage(imgBG[2], i, gshouseY, 0,Uclip);
				}
				if (typeBg != 11 && typeBg != 12 && imgBG[1] != null) {

					if (TileMap.mapID != 52)
						for (int i = -((GameScreen.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScreen.gW; i += imgBGWidth[1])
							g.drawImage(imgBG[1], i, bgLayer1y, 0,Uclip);

				}
				if (TileMap.mapID == 45 || TileMap.mapID == 55) {
					g.setColor(0x110000);
					g.fillRect(0, bgLayer0y + 20, GameScreen.gW, GameScreen.gH,Uclip);
				}

				if (imgBG[0] != null) {

					for (int i = -((GameScreen.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScreen.gW; i += imgBGWidth[0])
						g.drawImage(imgBG[0], i, bgLayer0y, 0,Uclip);

				}

				if (imgCloud != null) {

					for (int i = 0; i < 2; i++)
						g.drawImage(imgCloud, cloudX[i], cloudY[i], 3,Uclip);

				}
			}
			// ==============================
		} else {
			g.setColor(skyColor);
			g.fillRect(0, 0, GameScreen.gW, GameScreen.gH,Uclip);
		}
	}

	public static void resetBg() {
		imgBG = null;
		imgCloud = null;
		imgSun = null;
	}
	
	public static int typeBg = -1;
	public static void cleanImgBg(){
		Cout.println("keybgLoginSelect.size()   "+keybgLoginSelect.size());
		for (int i = 0; i < keybgLoginSelect.size(); i++) {
			String key = (String)keybgLoginSelect.elementAt(i);
			if(key!=null){
				mBitmap imgg = (mBitmap)Image.listImgLoad.get(key);
				if(imgg!=null){
					imgg.cleanImg();
				}
			}
		}
		keybgLoginSelect.clear();
	}
	public static mVector keybgLoginSelect = new mVector();
	public static boolean loadBG(int typeBG) {
		int deltaLayer1 = 0;
		int deltaLayer2 = 0;
		int deltaLayer3 = 0;
		typeBg = typeBG;
		switch (typeBG) {
		case 0:
			// typeBg = 0;
			
			break;
		case 2:
			deltaLayer1 = 16;
			deltaLayer2 = 10;
			deltaLayer3 = 10;
			// typeBg = 2;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 3:
			// typeBg = 3;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 4:
			// typeBg = 4;
			deltaLayer1 = 9;
			deltaLayer2 = 6;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 5:
			// typeBg = 5;
			bgSpeed = new int[] { 1, 1, 1, 1 };
			break;
		case 6:
			// typeBg = 6;
			deltaLayer1 = 12;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 12:
			// typeBg = 12;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 11:
			// typeBg = 11;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 10:
			// typeBg = 10;
			bgSpeed = new int[] { 1, 1, 1, 1 };
			break;
		case 9:
			// typeBg = 9;
			deltaLayer1 = 16;
			deltaLayer2 = 10;
			deltaLayer3 = 10;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 8:
			// typeBg = 8;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		case 7:
			// typeBg = 7;
			bgSpeed = new int[] { 1, 2, 3, 4 };
			break;
		}
		
		GameCanvas.skyColor = StaticObj.SKYCOLOR[typeBg];

		try {
			if (!lowGraphic) {
				imgBG = new mBitmap[4];
				imgBGWidth = new int[4];
				for (int i = 0; i < 4; i++) {
					try {
						if (StaticObj.TYPEBG[typeBg][i] != -1) {
							String path = "/bg/bg" + i + StaticObj.TYPEBG[typeBg][i] + ".png";
							boolean isAdded = false;
							for (int j = 0; j < keybgLoginSelect.size(); j++) {
								String pathL = (String)keybgLoginSelect.elementAt(j);
								if(pathL.equals(path)) isAdded = true;
							}
							if(currentScreen!= GameScreen.gI()&&!isAdded){
								keybgLoginSelect.add(path);
								Cout.println(isAdded+" addd key  "+keybgLoginSelect.size());
							}
							imgBG[i] = GameCanvas.loadImage("/bg/bg" + i + StaticObj.TYPEBG[typeBg][i] + ".png");
							if(imgBG[i]==null) return false;
						}
					} catch (Exception e) {
						e.printStackTrace();
						return false;
						
					}
					if (imgBG[i] != null)
						imgBGWidth[i] = mGraphics.getImageWidth(imgBG[i]);
				}

				if (typeBg == 10) {
					imgBG[1] = GameCanvas.loadImage("/bg/bg09.png");
					imgBG[2] = GameCanvas.loadImage("/bg/bg09.png");
					imgBGWidth[1] = mGraphics.getImageWidth(imgBG[1]);
					imgBGWidth[2] = mGraphics.getImageWidth(imgBG[2]);
				}
				if (typeBg == 12) {
					imgBG[3] = GameCanvas.loadImage("/bg/bg39.png");
					imgBGWidth[3] = mGraphics.getImageWidth(imgBG[3]);
				}
			}

			if (typeBg >= 0 && typeBg <= 1) {
				imgCloud = GameCanvas.loadImage("/bg/cl0.png");
				imgSun = GameCanvas.loadImage("/bg/sun0.png");
			} else {
				imgCloud = null;
				imgSun = null;
			}
//			if (typeBg == 2) {
//				imgCloud = GameCanvas.loadImage("/bg/cl1.png");
//				imgSun = GameCanvas.loadImage("/bg/sun1.png");
//			}
			if (typeBg == 7 || typeBg == 11 || typeBg == 12) {
				if (TileMap.mapID == 20) {
					imgCloud = null;
				} else
					imgCloud = GameCanvas.loadImage("/bg/cl0.png");
			}
		} catch (Exception e) {
			return false;
		}

		paintBG = false;
		if (!lowGraphic) {
			paintBG = true;
//			if (imgBG[0] != null && imgBG[1] != null && imgBG[2] != null)
//				gsskyHeight = GameScreen.gH - (mGraphics.getImageHeight(imgBG[0]) + mGraphics.getImageHeight(imgBG[1]) + mGraphics.getImageHeight(imgBG[2])) /*+ 11*/;
//			if (imgBG[0] != null)
//				gsgreenField1Y = GameScreen.gH - mGraphics.getImageHeight(imgBG[0]);
//			if (imgBG[1] != null)
//				gsgreenField2Y = gsgreenField1Y - mGraphics.getImageHeight(imgBG[1]);
//			if (imgBG[2] != null)
//				gshouseY = gsgreenField2Y - mGraphics.getImageHeight(imgBG[2]);
//			if (imgBG[3] != null)
//				gsmountainY = gsgreenField2Y - mGraphics.getImageHeight(imgBG[3]) /*- 10*/;
			if (imgBG[0] != null && imgBG[1] != null && imgBG[2] != null)
				gsskyHeight = GameScreen.gH - (mGraphics.getImageHeight(imgBG[0]) + mGraphics.getImageHeight(imgBG[1]) + mGraphics.getImageHeight(imgBG[2])) /*+ 11*/;
			if (imgBG[0] != null)
				gsgreenField1Y = GameScreen.gH - Image.getHeight(imgBG[0]);
			if (imgBG[1] != null)
				gsgreenField2Y = gsgreenField1Y - Image.getHeight(imgBG[1]);
			if (imgBG[2] != null)
				gshouseY = gsgreenField2Y - Image.getHeight(imgBG[2]);
			if (imgBG[3] != null)
				gsmountainY = gsgreenField2Y - Image.getHeight(imgBG[3]) /*- 10*/;

			if (typeBg >= 2 && typeBg <= 12) {
				int tem = GameScreen.gH - mGraphics.getImageHeight(imgBG[0]);
				bgLayer0y = tem;
				if (imgBG[1] != null)
					tem = tem - mGraphics.getImageHeight(imgBG[1]) + deltaLayer1;
				bgLayer1y = tem;
				if (imgBG[3] != null)
					tem = tem - mGraphics.getImageHeight(imgBG[3]) + deltaLayer3;
				gsmountainY = tem;
				//
				gsskyHeight = tem;

				if (imgBG[2] != null)
					gshouseY = bgLayer1y - mGraphics.getImageHeight(imgBG[2]) + deltaLayer2;
				if(typeBg == 2)
					gsskyHeight = GameCanvas.h;
			}
			
			if(typeBG == 3){
				int tem = GameScreen.gH - mGraphics.getImageHeight(imgBG[0]);
				bgLayer0y = tem;
				if (imgBG[1] != null)
					tem = tem - mGraphics.getImageHeight(imgBG[1])  + 30+ deltaLayer1;
				bgLayer1y = tem;
				if (imgBG[3] != null)
					tem = tem - mGraphics.getImageHeight(imgBG[3]) + deltaLayer3;
				gsmountainY = tem;
				//
				gsskyHeight = tem;

				if (imgBG[2] != null)
					gshouseY = bgLayer1y - mGraphics.getImageHeight(imgBG[2]) +20 + deltaLayer2;
				if(typeBg == 2)
					gsskyHeight = GameCanvas.h;

			}

		}
		int bgDelta = 0;
		if (typeBg >= 2 && typeBg <= 12) {
			bgDelta = (2 * GameScreen.gH / 3) - bgLayer1y;
		} else {
			bgDelta = (2 * GameScreen.gH / 3) - gsgreenField2Y;
		}
		if (bgDelta < 0)
			bgDelta = 0;
		if (TileMap.mapID == 48 && TileMap.mapID == 51)
			bgLayer0y += bgDelta;
		if (typeBg >= 2 && typeBg <= 6)
			bgLayer1y += bgDelta;

		gsskyHeight += bgDelta;
		gsgreenField1Y += bgDelta;
		gsgreenField2Y += bgDelta;
		gshouseY += bgDelta;
		gsmountainY += bgDelta;
		sunX = 3 * GameScreen.gW / 4;
		sunY = gsskyHeight / 3;
		cloudX = new int[2];
		cloudY = new int[2];
		cloudX[0] = GameScreen.gW / 3;
		cloudY[0] = gsskyHeight / 2 - 8;
		cloudX[1] = 2 * GameScreen.gW / 3;
		cloudY[1] = gsskyHeight / 2 + 8;
		if(typeBg == 2){
			sunY = gsskyHeight / 5;
			cloudX = new int[5];
			cloudY = new int[5];
			cloudX[0] = GameScreen.gW / 3;
			cloudY[0] = gsskyHeight / 3 - 35;
			cloudX[1] = 3 * GameScreen.gW / 4;
			cloudY[1] = gsskyHeight / 3 + 12;
			cloudX[2] = GameScreen.gW / 3 -15;
			cloudY[2] = gsskyHeight / 3 + 12;
			cloudX[3] =  GameScreen.gW / + 15;
			cloudY[3] = gsskyHeight / 2 + 12;
			cloudX[4] = 2 * GameScreen.gW / 3 + 25;
			cloudY[4] = gsskyHeight / 3 + 12;
		}
		
		if (!lowGraphic) {
			if (typeBg == 8)
				bgLayer0y = bgLayer1y = GameScreen.gH2 - 50;
			if (typeBg == 10) {
				if (imgBG[3] != null)
					gsmountainY = gshouseY - mGraphics.getImageHeight(imgBG[3]);
			}
			if (typeBg == 11 || typeBg == 12) {
				gsmountainY = 0;

			}
		}
		return true;
	}


	
	boolean resetToLoginScr;

	public void resetToLoginScr() {
	//  paintBG = true;
		Mob.isBossAppear = false;
		Party.gI().isLeader = false;
		GuiMain.vecItemOther.removeAllElements();
		GameScreen.hParty.clear();
		Quest.listUnReceiveQuest.removeAllElements();
		Quest.vecQuestFinish.removeAllElements();
		Quest.vecQuestDoing_Main.removeAllElements();
		Quest.vecQuestDoing_Sub.removeAllElements();
		ChatPrivate.vOtherchar.removeAllElements();
		ChatPrivate.nTinChuaDoc = 0;
		Char.myChar.typePk = -1;
		if(GameCanvas.currentScreen!= LoginScreen.gI()){
			LoginScreen.gI().switchToMe();
			GameCanvas.gameScreen = null;
			GameScreen.vCharInMap.removeAllElements();
			Session_ME.gI().close();
			loadBG(0);
			startOKDlg("Mất kết nối với máy chủ. Vui lòng thử lại !");
		}else{
			startOKDlg("Có lỗi đăng nhập xảy ra. Vui lòng thử lại !");
		}
	//  isLoading = false;
	//  resetToLoginScr = true;
	}
	
	public static void wrongVersion(){
		AlertDialog.Builder builder = new AlertDialog.Builder(GameCanvas.gCanvas);
		builder.setMessage("Phiên bản này không phù hợp với máy của bạn. Vui lòng tải phiên bản có độ phân giải thấp hơn.")
						.setCancelable(false)
						.setPositiveButton("Tải game",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								Util.openUrl("http://my.teamobi.com/app/index.php?for=forum&do=setting&uid=40&p=0&sz=15");
							}
						})
						.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								GameCanvas.gCanvas.finish();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}
	public static void openWeb(String link)
	{
		Util.openUrl(link);
	}
	public static mBitmap loadImage(String path) {
		path = "x" + zoomLevel+"" + path;
		//Cout.println("loadImageeee  "+path);
		return Image.loadImageFromAsset(path);
	}
	
	
	public static DataInputStream openFile(String path){
		DataInputStream file = null;
		try {
			file = new DataInputStream(GameMidlet.asset.open(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;
	}
	public static DataInputStream readdatafile(String path){
		InputStream iss = null;
		DataInputStream is = null;
		iss = MyStream.readFile(path);
		is = new DataInputStream(iss);
		if(is != null)
			return is;
		return null;
		
	}
	
	public static boolean isPointSelect(int x, int y, int w, int h) {
		if (!isPointerJustRelease) {
			return false;
		}
		return isPoint(x, y, w, h);
	}
	
	public static boolean isPoint(int x, int y, int w, int h) {
		if (px >= x && px <= x + w && py >= y && py <= y + h)
			return true;
		return false;
	}
	public static void startCommandDlg(String info, Command cmdYes, Command cmdNo) {
		msgdlg.setInfo(info, cmdYes, null, cmdNo);
		msgdlg.isPaintCham = false;
		msgdlg.show();
	}
	@Override
	public void perform(int idAction, Object p) {
//		Cout.println(getClass(), " idAction  "+idAction);
		currentDialog = null;
		switch (idAction) {
		case 8882:
			break;
		case cLogout:
			LoginScreen.isLogout = true;
			Service.gI().PlayerOut();
			resetToLoginScr(true);
			break;
		case cHoiSinh:
			Service.gI().HoiSinh((byte)1);
			break;
		case cVeLang:
			Service.gI().HoiSinh((byte)0);
			break;
		case cDongYThachDau:
			String id = (String)p;
			Service.gI().ThachDau((byte)1, Short.parseShort(id));
			break;
		case cTaiVersionMoi:
			openWeb((String)p);
			break;
		case cMenuNpc:
			Cout.println2222("com.sakura.thelastlegend.gui len menuuuuuuu");
			String text = (String)p;
			Cout.println2222("com.sakura.thelastlegend.gui len menuuuuuuu  index "+text);
			byte index_menu = Byte.parseByte(text);
			if(Char.myChar().npcFocus!=null)
			Service.gI().MenuNpc(Char.myChar().npcFocus.template.npcTemplateId,index_menu);
			
			break;
		case cTaoClan:
			String namenhap = msgdlg.getTextTF();
			if(namenhap==null) return;
			Cout.println2222("nhan  cTaoClan  " +namenhap);
			if(namenhap.trim().length()==0)
			{
				GameCanvas.StartDglThongBao("Vui lòng nhập lại !!!");
				startDlgTField("Tạo Bang", null, new Command("Tạo", this, cTaoClan, null), null);
			}else if(namenhap.trim().length()>12||namenhap.trim().length()<6)
			{
				GameCanvas.StartDglThongBao("Tên bang nhiều hơn 6 ký tự và không quá 12 ký tự !!!");
				startDlgTField("Tạo Bang", null, new Command("Tạo", this, cTaoClan, null), null);
			}
			Service.gI().Clan_createClanName(namenhap);
			
			break;
		}

//		Cout.println(getClass(), " currentDialog  "+currentDialog);
	}
	public static void startDlgTField(String tieude,Command cmdleft,Command center,Command right)
	{
		msgdlg.setInfoTF(tieude, cmdleft, center, right);
		msgdlg.isPaintCham = false;
		msgdlg.show();
	}

	public void resetToLoginScr(boolean isLogout) {
	//  paintBG = true;
		Mob.isBossAppear = false;
			Char.myChar.typePk = -1;
			Party.gI().isLeader = false;
			GameScreen.hParty.clear();
			GuiMain.vecItemOther.removeAllElements();
			Char.myChar().clearAllFocus();
			Char.myChar = null;
			Quest.listUnReceiveQuest.removeAllElements();
			Quest.vecQuestFinish.removeAllElements();
			Quest.vecQuestDoing_Main.removeAllElements();
			Quest.vecQuestDoing_Sub.removeAllElements();
			ChatPrivate.vOtherchar.removeAllElements();
			ChatPrivate.nTinChuaDoc = 0;
			if(GameCanvas.currentScreen!= LoginScreen.gI()){
				LoginScreen.gI().switchToMe();
				GameCanvas.gameScreen = null;
				loadBG(0);
				GameScreen.vCharInMap.removeAllElements();
				Session_ME.gI().close();
				if(!isLogout)
				startOKDlg("Mất kết nối với máy chủ. Vui lòng thử lại !");
			}else{
				startOKDlg("Có lỗi đăng nhập xảy ra. Vui lòng thử lại !");
			}
		 }
	public static void resetTrans(mGraphics g){
		  g.translate(-g.getTranslateX(), -g.getTranslateY());
		  g.setClip(0, 0, w, h);  
	}

	public static void clearPointerEvent() {
		// TODO Auto-generated method stub
		isPointerClick = isPointerDown = isPointerJustRelease = false;
	}

	public static void startDlgTime(String info, Command cmdYes, int time) {
		// TODO Auto-generated method stub
		cmdYes.caption = mResources.OK;
		msgdlg.setInfo(info, null,cmdYes,  null);
		msgdlg.setTimeLive(time);
		msgdlg.isPaintCham = false;
		msgdlg.show();
		currentDialog = msgdlg;
	}
	public static void StartDglThongBao(String text){
		isPaintThongBao = true;
		indexThongBao = 0;
		thongbao = text;
	}
	public static final int cEndDgl = 8882;
	public static final int cLogout = 9990;
	public static final int cHoiSinh = 9991;
	public static final int cVeLang = 9992;
	public static final int cDongYThachDau = 9993;
	public static final int cTaiVersionMoi = 9994;
	public static final int cTaoClan = 9995;
	public static final int cMenuNpc = 10000;
}