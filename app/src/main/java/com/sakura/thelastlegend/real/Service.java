package com.sakura.thelastlegend.real;

import java.io.IOException;
import java.util.Vector;

import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Session_ME;
import com.sakura.thelastlegend.domain.model.CRes;
import com.sakura.thelastlegend.domain.model.Cmd;
import com.sakura.thelastlegend.network.ISesion;
import com.sakura.thelastlegend.network.Message;
import screen.ClanScreen;
import screen.GameScreen;
import com.sakura.thelastlegend.gui.TabNangCap;
import Objectgame.Char;
import Objectgame.Item;
import Objectgame.Mob;

import com.sakura.thelastlegend.GameMidlet;

public class Service {

	ISesion session = Session_ME.gI();
	protected static Service instance;

	public static Service gI() {
		if (instance == null)
			instance = new Service();
		return instance;
	}
	
	public void doLogin(String user, String pass,byte zoomlevel){ // login
		Message m;
		try {
			Cout.println("doLogin "+user+"  pass "+pass+" zoom "+zoomlevel);
			m = new Message(Cmd.LOGIN);
			m.writer().writeUTF(user);
			m.writer().writeUTF(pass);
			m.writer().writeByte(zoomlevel);
			m.writer().writeUTF(GameMidlet.VERSION);
			session.sendMessage(m);
			m.cleanup();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void createChar(byte typeChar, byte country , byte gender, String charName){ // tạo char
		Message m;
		try{
			Cout.println(country+ " typeChar "+typeChar+" charName "+charName);
			m = new Message(Cmd.CREATE_CHAR);
			m.writer().writeByte(country);
			m.writer().writeByte(typeChar);
			m.writer().writeByte(gender);
			m.writer().writeUTF(charName);
			session.sendMessage(m);
			m.cleanup();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void selectChar(int charIDDB){ // select char to play
		Message m;
		try{
			m = new Message(Cmd.SELECT_CHAR);
			m.writer().writeInt(charIDDB);
			session.sendMessage(m);
			m.cleanup();
			
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void charMove() { // com.sakura.thelastlegend.gui char di chuyen
		int dx = Char.myChar().cx - Char.myChar().cxSend;
		int dy = Char.myChar().cy - Char.myChar().cySend;
		boolean isGuilen = (CRes.abs(dx)>24||CRes.abs(dy)>24)?true:false;
		if (Char.ischangingMap || (dx == 0 && dy == 0)||(!isGuilen&&Char.myChar().statusMe!=Char.A_STAND&&Char.myChar().statusMe!=Char.A_DEAD&&Char.myChar().statusMe!=Char.A_DEADFLY)){
			return;
		}
		try {
			if(GameScreen.isSendMove)
			GameScreen.isSendMove = false;
			Message m = new Message(Cmd.PLAYER_MOVE);
			Char.myChar().cxSend = Char.myChar().cx;
			Char.myChar().cySend = Char.myChar().cy;
			Char.myChar().cdirSend = Char.myChar().cdir;
			Char.myChar().cactFirst = Char.myChar().statusMe;
			m.writer().writeShort(Char.myChar().cx);
//			if (dy != 0)
			m.writer().writeShort(Char.myChar().cy);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void requestImage(int id,byte sub){ // yeu cau hinh tu server
		Message m = null;
		try {
			Cout.println("requestImage --> "+id +" subbbb  "+sub);
			m = new Message(Cmd.REQUEST_IMAGE);
			m.writer().writeByte(sub);// o là normal 1 la quai.
			m.writer().writeInt(id);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestChangeMap() { // server tự biết nó đứng đâu mà chuyển đến
		// map nào
		try{
			Cout.println("com.sakura.thelastlegend.gui chuyen map");
			Message m = new Message(Cmd.CHANGE_MAP);
			session.sendMessage(m);
			m.cleanup();
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	public void requestinventory(){ // yeu cau request Inventory
		try{
			Message m = new Message(Cmd.GET_ITEM_INVENTORY);
			session.sendMessage(m);
			m.cleanup();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void requestIcon(int id){ // request hinh nho
		Message m = null;
		try {
			//System.out.println("idicon --> "+id);
			m = new Message(Cmd.REQUEST_IMAGE);
			m.writer().writeInt(id);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void chat(String text, byte type) { // yeu cau chat trong map
		Message m = null;
		try {
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeUTF(text);
			Cout.println2222("chatmap   "+text);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}
	
	public void chatGlobal(String text, byte type) { // yeu cau chat kenh the gioi
		Message m = null;
		try {
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeUTF(text);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			m.cleanup();
		}		
		
	}
	
	public void requetsInfoMod(short idMod){ // yeu cau thong tin quai khi focus
		Message m = null;
		try{
			m = new Message(Cmd.REQUEST_MONSTER_INFO);
//			Cout.println("requetsInfoMod  "+idMod);
			m.writer().writeShort(idMod);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}
	
	public void sendPlayerAttack(Vector vMob, Vector vChar, int type,int idskill,boolean isMob) { 
		try {
			// 0 player
			// 1 monter
//			Cout.println("attack  "+type);
			Message m = null;
			GameScreen.isSendMove = true;
			m = new Message(Cmd.ATTACK);
			m.writer().writeByte((byte)type);
//			Cout.println("sendPlayerAttack --->type "+type);
			m.writer().writeByte((byte)idskill);// type tan cong 0//châ//quai 0
//			Cout.println("sendPlayerAttack --->idskill "+idskill);
			m.writer().writeByte((byte)vMob.size());//ntarget
//			Cout.println("sendPlayerAttack --->vMob.size() "+vMob.size());
//			m.writer().writeByte(1);
//			if (vMob.size() > 0) {
//				if(Char.myChar().mobFocus != null){
//					if(Char.myChar().mobFocus != null){
//						m.writer().writeShort(Char.myChar().mobFocus.mobId);
//						
//					}
//				}

//		} 

			if(isMob)
			{
				for (int i = 0; i < vMob.size(); i++) {
					Mob b = (Mob) vMob.elementAt(i);
					if(b != null){
						m.writer().writeShort(b.mobId);
					}
				}
			}
			else{
				for (int i = 0; i < vChar.size(); i++) {
					Char b = (Char) vChar.elementAt(i);
					if(b != null){
						m.writer().writeShort(b.charID);
					}
				}
			}

//				Cout.println("sendPlayerAttack --mob-> ");
			session.sendMessage(m);

		} catch (Exception e) {
		}
	}
	public void itemPick(byte Type, short idItem){ // yêu cầu nhặt item
		Message m = null;
		try {
			m = new Message(Cmd.PICK_REMOVE_ITEM);
			m.writer().writeByte(Type);
			m.writer().writeShort(idItem);
			Cout.println("nhat idItem ---> "+idItem);
			session.sendMessage(m);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			m.cleanup();
		}
	}
	
	public void requestNPC(short IdNpc){
		Message m = null;
		try{
			m = new Message(Cmd.NPC_REQUEST);
			m.writer().writeShort(IdNpc);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		} finally{
			m.cleanup();
		}
	}
	
	public void giveUpItem(byte index){
		Message m = null;
		try{
			m = new Message(Cmd.GIVEUP_ITEM);
			Cout.println("Vut item  "+index);
			m.writer().writeByte(index);
			session.sendMessage(m);
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}

	public void useItem(byte index, Item type){
		Message m = null;
		try{
			Cout.println2222(type.template.type+"  su dung "+index);
			m = new Message(Cmd.ITEM);
			m.writer().writeByte(type.typeUI);
			m.writer().writeByte(index);
//			for(int i = index; i < Char.myChar().arrItemBag.length; i++)
//			type = Char.myChar().arrItemBag[index];
			session.sendMessage(m);
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void inviteParty(short charID, byte type){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			m.cleanup();
		}
	}
	
	public void AcceptParty(byte type, short charID){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void leaveParty(byte type, short charID){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void kickPlayeleaveParty(byte type, short charID){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(charID);
			session.sendMessage(m);
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void removeParty(byte type){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(Char.myChar().charID);
			session.sendMessage(m);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void requestjoinParty(byte type, short CharID){ // yeu cau vao party
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestinfoPartynearME(byte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.PARTY);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestAddfriend(byte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void AcceptFriend(byte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestFriendList(byte type, short CharID){
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void deleteFriend(byte type,short myCharID ,short CharID){ // xóa bạn 
		Message m = null;
		try{
			m = new Message(Cmd.FRIEND);
			m.writer().writeByte(type);
			m.writer().writeShort(myCharID);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void chatPrivate(byte type, short userid, String contend){
		Message m = null;
		try{
			Cout.println(getClass(),userid+ " com.sakura.thelastlegend.gui chat chatPrivate "+contend);
			m = new Message(Cmd.CHAT);
			m.writer().writeByte(type);
			m.writer().writeShort(userid);
			m.writer().writeUTF(contend);
			session.sendMessage(m);
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			m.cleanup();
		}
	}
	
	public void inviteTrade(byte typeTrade, short CharID){ // mời trao đổi
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void acceptTrade(byte typeTrade, short CharID){ // đồng ý trao đổi 
		
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void moveItemTrade(byte typeTrade, short charID, byte typeMoveItem, short Iditem){ // chuyển item trao đổi
		Cout.println2222("SendItem move ----> "+typeTrade+", "+charID+", "+typeMoveItem+", "+Iditem);
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(charID);
			m.writer().writeByte(typeMoveItem);
			m.writer().writeShort(Iditem);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void cancelTrade(byte typeTrade, short CharID){ // hủy trao đổi 
		Cout.println2222(" cancelTradeTradeeeeeeeeeeeeeeeeeeeee");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(Char.myChar().partnerTrade.charID);
			session.sendMessage(m);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void comfirmTrade(byte typeTrade, short CharID){ // lock
		Cout.println2222(" comfirmTrade");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void doTrade(byte typeTrade, short CharID){ // giao dịch 
		Cout.println2222(" doTrade");
		Message m = null;
		try{
			m = new Message(Cmd.TRADE);
			m.writer().writeByte(typeTrade);
			m.writer().writeShort(CharID);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	
	public void requestShop(int idMenuShop){ // thong tin shop
		Message m = null;
		try{
			Cout.println("requestShop  "+idMenuShop);
			m = new Message(Cmd.REQUEST_SHOP);
			m.writer().writeByte(idMenuShop);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void BuyItemShop(byte idmenu,short iditem){ // thong tin shop
		Message m = null;
		try{
			Cout.println("BuyItemShop  "+idmenu+" iditem "+iditem);
			m = new Message(Cmd.BUY_ITEM_FROM_SHOP);
			m.writer().writeByte(idmenu);
			m.writer().writeShort(iditem);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}
	public void requestMenuShop(){
		Message m = null;
		try{
			m = new Message(Cmd.CMD_DYNAMIC_MENU);
			session.sendMessage(m);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			m.cleanup();
		}
	}

	public void Quest(byte type, short questId, byte mainSub)
	{
		Message m= new Message(Cmd.QUEST);		
		try {
			m.writer().writeByte(type);//0 nhan // 1 tra// 2 huy
			m.writer().writeByte(questId);//
			m.writer().writeByte(mainSub);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void PlayerOut()
	{
		Message m= new Message(Cmd.PLAYER_REMOVE);		
		try {
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void HocSkill(byte type, int idskill){
		Message m= new Message(Cmd.CHAR_SKILL_STUDIED);		
		try {
			m.writer().writeByte(type);
			m.writer().writeInt(idskill);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void HoiSinh(byte type)//0 ve lang//1 hoi sinh tai cho
	{
		Message m= new Message(Cmd.COME_HOME_DIE);		
		try {
			m.writer().writeByte(type);//0 nhan // 1 tra// 2 huy
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}

	public void requestRegister(String user, String text, String version) {
		// TODO Auto-generated method stub
		Message m= new Message(Cmd.REGISTER);		
		try {
			m.writer().writeUTF(user);
			m.writer().writeUTF(text);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void requestPlayerInfo(short idchar){
		Message m= new Message(Cmd.PLAYER_INFO);		
		try {
			m.writer().writeShort(idchar);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
		
	}
	public void requestDanhKhu(){
		Message m= new Message(Cmd.REQUEST_REGION);		
		try {
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void requestChangeKhu(byte idkhu){
		Message m= new Message(Cmd.CHANGE_REGION);		
		try {
			m.writer().writeByte(idkhu);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void requestADD_BASE_POINT(byte idnex,short value){
		
		Message m= new Message(Cmd.ADD_BASE_POINT);		
		try {
			m.writer().writeByte(1);
			m.writer().writeByte(idnex);
			m.writer().writeShort(value);
//			 byte type = message.dis.readByte();//loai cho base point va skill
//		        byte index = message.dis.readByte();//sinh,van,khi,tri,luc,toc
//		        short value = message.dis.readShort();//gia tri cong
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void ThachDau(byte index,short idchar){
		Message m= new Message(Cmd.COMPETED_ATTACK);		
		try {
			m.writer().writeByte(index); //0 moi 1 ok
			m.writer().writeShort(idchar);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void ChangeFlag(byte indexCo){
		Message m= new Message(Cmd.STATUS_ATTACK);		
		try {
			m.writer().writeByte(indexCo); //0 moi 1 ok
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void SellItem(byte index)
	{
		Message m= new Message(Cmd.SELL_ITEM);		
		try {
			//m.writer().writeByte(1);
			Cout.println("SellItem  "+index);
			m.writer().writeByte(index); //0 moi 1 ok
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void GoItemTrangBi(byte id)
	{
		Message m= new Message(Cmd.TAKE_OFF_);		
		try {
			Cout.println("GoItemTrangBi   "+id);
			m.writer().writeByte(id); //0 moi 1 ok
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
		}
	}
	public void MenuNpc(int idNpc, byte idmenu)
	{

//		Cout.println2222("nhan ve menuuuuuuu");
		try {
			Message m = new Message(Cmd.MENU_NPC);
			m.writer().writeInt(idNpc);
			m.writer().writeByte(idmenu);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void NangCap_Dualen(byte indexItem)
	{
		try {
//			Cout.println2222("NangCap_Dualen  "+indexItem);
			Message m = new Message(Cmd.DAPDO);
			m.writer().writeByte(TabNangCap.SUB_TAKE_ON);
			m.writer().writeByte(indexItem);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void NangCap_ThaoXuong(byte indexItem)
	{
		try {

//			Cout.println2222("NangCap_ThaoXuong  "+indexItem);
			Message m = new Message(Cmd.DAPDO);
			m.writer().writeByte(TabNangCap.SUB_TAKE_OFF);
			m.writer().writeByte(indexItem);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void NangCap_NangCap()
	{
		try {

			Message m = new Message(Cmd.DAPDO);
			m.writer().writeByte(TabNangCap.SUB_UPGRADE);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_getInfo()
	{
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.REQUEST_CLAN);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_createClanName(String name)
	{
//		Cout.println2222("Clan_createClanName  "+name);
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.REQUETS_NAME);
			m.writer().writeUTF(name);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_MoiBang(short idChar)
	{
		Cout.println2222("Clan_moivaobang  "+idChar);
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.INVITA_CLAN);
			m.writer().writeShort(idChar);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_MoiBang_GOBAL(short idChar)
	{
		Cout.println2222("Clan_moivaobang  "+idChar);
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.INVITE_CLAN_GLOBAL);
			m.writer().writeShort(idChar);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_KichKhoiBang(short idChar)
	{
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.KICK_CLAN);
			m.writer().writeShort(idChar);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_XoaBang()
	{
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.DELETE_CLAN);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_RoiBang()
	{
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.LEAVE_CLAN);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_GetlistUser_local()
	{

		Cout.println2222("Clan_GetlistUser_local  ");
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.GET_LIST_LOCAL);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Clan_GetlistUser_gobal()
	{
		try {
			Message m = new Message(Cmd.CLAN);
			m.writer().writeByte(ClanScreen.GET_LIST_LOCAL);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Payment_Sendcode(String code, String seri, byte typeThe)
	{
		try {
			Cout.println2222("Payment_Sendcode");
			Message m = new Message(Cmd.PAYMENT_GET);
			m.writer().writeUTF(code);
			m.writer().writeUTF(seri);
			m.writer().writeByte(typeThe);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Payment_GetInfo()
	{
		Cout.println2222("Payment_GetInfo");
		try {
			Message m = new Message(Cmd.PAYMENT_INFO);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void ActionDialogYesNo(byte type,short idDialog,byte subCmd,short iduser, byte action)
	{
		Cout.println2222(type+" ActionDialog "+idDialog+" iduser "+iduser+" action  "+action);
		try {
			Message m = new Message(Cmd.DIALOG);
			m.writer().writeByte(type);
			m.writer().writeShort(idDialog);
			m.writer().writeByte(subCmd);
			m.writer().writeShort(iduser);
			m.writer().writeByte(action);
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void Shutdown_server()
	{
		Cout.println2222("Shutdown_server");
		try {
			Message m = new Message(47);
			m.writer().writeUTF("tuan@T1234");
			m.writer().writeUTF("shutdown");
			session.sendMessage(m);
			m.cleanup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}

