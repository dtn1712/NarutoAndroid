package com.sakura.thelastlegend.real;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Vector;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.JSONParser;
import com.sakura.thelastlegend.lib.Music;
import com.sakura.thelastlegend.lib.Rms;
import com.sakura.thelastlegend.lib.Util;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.domain.model.CRes;
import com.sakura.thelastlegend.domain.model.ChatManager;
import com.sakura.thelastlegend.domain.model.ChatPopup;
import com.sakura.thelastlegend.domain.model.Cmd;
import com.sakura.thelastlegend.domain.model.Const;
import com.sakura.thelastlegend.domain.model.Effect2;
import com.sakura.thelastlegend.domain.model.EffectData;
import com.sakura.thelastlegend.domain.model.InfoServer;
import com.sakura.thelastlegend.domain.model.Party;
import com.sakura.thelastlegend.domain.model.QuickSlot;
import com.sakura.thelastlegend.domain.model.ServerEffect;
import com.sakura.thelastlegend.domain.model.SmallImage;
import com.sakura.thelastlegend.domain.model.Type_Chat;
import com.sakura.thelastlegend.domain.model.Type_Object;
import com.sakura.thelastlegend.domain.model.Type_Party;
import com.sakura.thelastlegend.domain.model.Waypoint;
import com.sakura.thelastlegend.domain.model.mResources;
import com.sakura.thelastlegend.network.IMessageHandler;
import com.sakura.thelastlegend.network.Message;
import screen.ClanScreen;
import screen.DownloadImageScreen;
import screen.FlagScreen;
import screen.GameScreen;
import screen.KhuScreen;
import screen.LoginScreen;
import screen.SelectCharScreen;
import screen.WaitingScreen;

import com.sakura.thelastlegend.gui.GuiMain;
import com.sakura.thelastlegend.gui.GuiQuest;
import com.sakura.thelastlegend.gui.MenuIcon;
import com.sakura.thelastlegend.gui.ShopMain;
import com.sakura.thelastlegend.gui.TabBag;
import com.sakura.thelastlegend.gui.TabNangCap;
import com.sakura.thelastlegend.gui.TabSkill;
import com.sakura.thelastlegend.gui.TradeGui;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.BgItem;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.ChatWorld;
import Objectgame.Friend;
import Objectgame.InfoItem;
import Objectgame.Item;
import Objectgame.ItemMap;
import Objectgame.ItemOptionTemplate;
import Objectgame.ItemTemplate;
import Objectgame.ItemTemplates;
import Objectgame.ItemThucAn;
import Objectgame.Laroi;
import Objectgame.Mob;
import Objectgame.MobTemplate;
import Objectgame.Npc;
import Objectgame.NpcTemplate;
import Objectgame.OtherChar;
import Objectgame.Quest;
import Objectgame.Skill;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import Objectgame.Task;
import Objectgame.TileMap;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;


public class Controller implements IMessageHandler {

	protected static Controller me;
	public Message messWait;

	public static Controller gI() {
		if (me == null)
			me = new Controller();
		return me;
	}

	public void onConnectOK() {
		Cout.println("Connect ok");
	}
	
	public void onConnectionFail() {
//		if (!isTryGetIPFromWap) {
//			new Thread(new Runnable() {
//				public void run() {
//					try {
//						Thread.sleep(1000);
//						System.out.println(GameMidlet.linkGetHost);
//						HttpConnection connection = (HttpConnection) Connector.open(GameMidlet.linkGetHost);
//						connection.setRequestMethod(HttpConnection.GET);
//						connection.setRequestProperty("Content-Type", "//text plain");
//						connection.setRequestProperty("Connection", "close");
//						if (connection.getResponseCode() == HttpConnection.HTTP_OK) {
//							String str = "";
//							InputStream inputstream = connection.openInputStream();
//							int length = (int) connection.getLength();
//							if (length != -1) {
//								byte incomingData[] = new byte[length];
//								inputstream.read(incomingData);
//								str = new String(incomingData);
//							}
//							System.out.println("New ip: " + str);
//							Rms.saveIP(str);
//							Session_ME.gI().connect("socket://" + str);
//							isTryGetIPFromWap = true;
//							return;
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				}
//			}).start();
//			return;
//		}
		GameCanvas.startOK(mResources.SERVER_MAINTENANCE, 8882, null);
	}
	
	public void onDisconnected() {
		Cout.println2222("onDisconnected");
		GameCanvas.instance.resetToLoginScr();
	}
	
	public void onMessage(Message msg) {
		try{
			int size;
			Char c = new Char();
			Cout.println("MESSAGE RECIVE ----> "+msg.command);

			Quest q = null;
			switch (msg.command) {
			case Cmd.QUEST:
				GuiQuest.listNhacNv.removeAllElements();

				byte typeUpdate = msg.reader().readByte(); // update
				byte typeQuest = msg.reader().readByte(); // loại nhiệm vụ
				Cout.println(typeUpdate+" MESSAGE RECIVE ---QUEST typeQuest-> "+typeQuest);
				int[] idquestupdate =null;
				String[] chuoinhacUpdate =null;
				if(typeUpdate==1){
					int nsize = msg.reader().readByte();// msg.reader().readShort();
					idquestupdate = new int [nsize];
					chuoinhacUpdate = new String[nsize];
					
					for (int i = 0; i < nsize; i++) {
						idquestupdate[i] = msg.reader().readShort();//idquesst
						Boolean isMain = msg.reader().readBoolean();
						String name = msg.reader().readUTF();
						chuoinhacUpdate[i]= msg.reader().readUTF();
						String[] cat = mFont.tahoma_7_white.splitFontArray(chuoinhacUpdate[i], InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}
				}
				else {
					if(typeQuest==Task.CAN_RECEIVE_TASK){
						Quest.listUnReceiveQuest.removeAllElements();
						for (int j = 0; j < GameScreen.vNpc.size(); j++) {
							Npc npc = (Npc) GameScreen.vNpc.elementAt(j);
							if(npc!=null&&npc.typeNV==0)
								npc.typeNV = -1;
						}
					}else if(typeQuest==Task.COMPLETE_TASK){
						Quest.vecQuestFinish.removeAllElements();
						for (int j = 0; j < GameScreen.vNpc.size(); j++) {
							Npc npc = (Npc) GameScreen.vNpc.elementAt(j);
							if(npc!=null&&npc.typeNV==1)
								npc.typeNV = -1;
						}
					}else if(typeQuest==Task.DOING_TASK){
						Quest.vecQuestDoing_Main.removeAllElements();
						Quest.vecQuestDoing_Sub.removeAllElements();
						Quest.vecQuestDoing_Server.removeAllElements();
						for (int j = 0; j < GameScreen.vNpc.size(); j++) {
							Npc npc = (Npc) GameScreen.vNpc.elementAt(j);
							if(npc!=null&&npc.typeNV==2)
								npc.typeNV = -1;
						}
					}
					byte sizeQuest = msg.reader().readByte(); // size nhiệm vụ

					Cout.println2222(typeQuest+"  ---QUEST sizeQuest- "+sizeQuest);
					
					for(int i = 0; i < sizeQuest; i++){
						int idQuest = msg.reader().readShort(); //  id nhiệm vụ
						boolean isMain = msg.reader().readBoolean(); //  có phải nhiệm vụ chính k
						String nameQuest = msg.reader().readUTF(); // tên nhiệm vụ
						Cout.println2222(idQuest+" ---QUEST typeQuest-> "+typeQuest);
						if(typeQuest == Task.CAN_RECEIVE_TASK){ // nhiệm vụ có thể nhận
							short idNPC = msg.reader().readShort(); // idNpc nhận nv
							for (int j = 0; j < GameScreen.vNpc.size(); j++) {
								Npc npc = (Npc) GameScreen.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNPC)
									npc.typeNV = 0;
							}
		                     // noi dung nc de nhan nhiem vu(trước khi nhận nv)
							String taklNPC = msg.reader().readUTF();// 
							byte typeQuestThis = msg.reader().readByte(); // loại nhiệm vụ này
							String shortdis = msg.reader().readUTF(); // //Câu mô tả tóm tắt nhiệm vụ phải làm(sau khi nhận nv)
							String gift = msg.reader().readUTF();// phần thưởng
							gift = gift.trim().length()==0?"0/0/0":gift;
							String nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
//							Cout.println(nameQuest+" namequest "+idNPC+"typeQuestThis  "+typeQuestThis);
							q = new Quest(idQuest, isMain, nameQuest,
									idNPC, taklNPC,
									typeQuestThis, shortdis,gift,nhacnv);
							Quest.listUnReceiveQuest.addElement(q);
						}else if(typeQuest == Task.COMPLETE_TASK){ // nhiệm vụ hoàn thành
							short idNPC = msg.reader().readShort(); // idNPC trả nhiệm vụ
//							Cout.println(getClass(), "Quest nv hoan thanh "+idNPC);
							for (int j = 0; j < GameScreen.vNpc.size(); j++) {
								Npc npc = (Npc) GameScreen.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNPC)
									npc.typeNV = 1;
							}
							String taklNPC = msg.reader().readUTF(); // nội dung nói chuyện NPC
//							Cout.println("nv hoan thanh taklNPC "+taklNPC);
							String talknof = msg.reader().readUTF(); // thông báo và hướng dẫn trả nhiệm vụ từ server
//							Cout.println("nv hoan thanh talknof "+talknof);
							String nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
//							Cout.println("nv hoan thanh nhacnv "+nhacnv);
							q = new Quest(idQuest, isMain, nameQuest,
									idNPC, taklNPC,
									talknof,nhacnv);
							Quest.vecQuestFinish.addElement(q);
						}else if(typeQuest == Task.DOING_TASK){ // nhiệm vụ đang làm
							byte typeDoingQuest = msg.reader().readByte();// loai nhiem vu 1killqua//2chuyendo//3nc
							
							String contendQuest = msg.reader().readUTF();// mo ta nhiem vu cho nay (paint trong màn hình nv)
							String supContend = msg.reader().readUTF();//get noi dung support khi dang lam nhiem vu
							short idNpcPay = msg.reader().readShort();// idnpc tra nhiem vu
							String nhacnv = msg.reader().readUTF(); /// nhắc nv paint ở ngoài
//							Cout.println("nv daglam idNpcPay "+idNpcPay);
							for (int j = 0; j < GameScreen.vNpc.size(); j++) {
								Npc npc = (Npc) GameScreen.vNpc.elementAt(j);
								if(npc!=null&&npc.npcId==idNpcPay)
									npc.typeNV = 1;
							}
							Cout.println2222("  typeDoingQuest  "+typeDoingQuest);
							
							if(typeDoingQuest == Task.TYPE_DOING_TASK_IP){// vận chuyển
								short idNpc = msg.reader().readShort();// id Npc can chuyen den 
							}else if(typeDoingQuest == Task.TYPE_DOING_TASK_COLLECT){ // thu thập
								byte total = msg.reader().readByte();// soluong item
								// id các item cần phải lấy
								short item[] = new short[total];
								// số item cần phải lấy
								short totalitem[] = new short[total];
								// số item đã dc
								short nItemGot[] = new short[total];
								for(int j = 0; j < total; j++){
									item[j] = msg.reader().readShort();//id item
									nItemGot[j] = msg.reader().readShort();// soluong đã nhặt đc 
									totalitem[j] = msg.reader().readShort();// số lượng cần nhặt
								}
	
								String giftforQuest = msg.reader().readUTF();
								giftforQuest = giftforQuest.trim().length()==0?"0/0/0":giftforQuest;
//								Cout.println("MESSAGE RECIVE ---QUEST-> NV NHAT ITEM");
								q = new Quest(idQuest, isMain, nameQuest,
										supContend, Task.DOING_TASK, contendQuest,
										idNpcPay, item, totalitem, nItemGot,
										Quest.TYPE_ITEM,giftforQuest,nhacnv);
							}else if(typeDoingQuest == Task.TYPE_DOING_TASK_SKILL){//giet quai
								byte totalKill = msg.reader().readByte();
								short idMons[] = new short[totalKill];
								short totalMons[] = new short[totalKill];
								short nMonsKilled[] = new short[totalKill];
								for(int k = 0; k < totalKill; k++){
									idMons[k] = msg.reader().readShort(); // id quái
									nMonsKilled[k] =msg.reader().readShort(); // số lượng đã giết
									totalMons[k] =msg.reader().readShort(); // số lượng cần giết
								}
	
								String giftforQuest = msg.reader().readUTF();
								giftforQuest = giftforQuest.trim().length()==0?"0/0/0":giftforQuest;
								q = new Quest(idQuest, isMain, nameQuest,
										supContend, Task.DOING_TASK, contendQuest,
										idNpcPay, idMons, totalMons,
										nMonsKilled, Quest.TYPE_MONSTER,giftforQuest,nhacnv);
							}
							else if(typeDoingQuest == 4) //nhiem vu theo yêu cau
							{
								String chuoinhac = msg.reader().readUTF();
								q = new Quest(idQuest, chuoinhac);
							}
//							Cout.println(" nv dang lam "+q);
							if (q != null) {
								if (isMain&&typeDoingQuest != 4) {
//									Cout.println(" nv dang lam adđ  "+q);
									Quest.vecQuestDoing_Main.addElement(q);
								} else if(typeDoingQuest != 4) {
//									Cout.println(" nv dang lam adđ 222 "+q); 
									Quest.vecQuestDoing_Sub.addElement(q);
								}else 
								{
									Quest.vecQuestDoing_Server.addElement(q);
								}
							}
						}
						
					}
				}
				if(InfoItem.wcat==-1) 
					InfoItem.wcat =  10*mGraphics.getImageWidth(LoadImageInterface.imgChatConner)-10;
				for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
					Quest qe =  (Quest)Quest.listUnReceiveQuest.elementAt(i);
					if(typeUpdate==0){

						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.CAN_RECEIVE_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestDoing_Main.elementAt(i);
					if(typeUpdate==0){
						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);

						//Cout.println(i+" ---QUEST chuoinhac strNhacNv-> "+qe.strNhacNv);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
							//Cout.println(j+" ---strNhacNv-> "+cat[j]);
						}
					}else{
						if(typeQuest==Task.DOING_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestDoing_Sub.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestDoing_Sub.elementAt(i);
					if(typeUpdate==0){
						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.DOING_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				Cout.println2222("  vecQuestDoing_Server Main  "+Quest.vecQuestDoing_Main.size());
				
				Cout.println2222("  vecQuestDoing_Server subb  "+Quest.vecQuestDoing_Sub.size());
				
				for (int i = 0; i < Quest.vecQuestDoing_Server.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestDoing_Server.elementAt(i);
					if(typeUpdate==0){
						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.DOING_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));

										Cout.println2222("  add _Server  "+GuiQuest.listNhacNv.size());
									}
								}
							}
					}
				}
				for (int i = 0; i < Quest.vecQuestFinish.size(); i++) {
					Quest qe =  (Quest)Quest.vecQuestFinish.elementAt(i);
					if(typeUpdate==0){
						String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
						for (int j = 0; j < cat.length; j++) {
							GuiQuest.listNhacNv.add(new InfoItem(cat[j]));
						}
					}else{
						if(typeQuest==Task.COMPLETE_TASK)
							for (int j = 0; j < idquestupdate.length; j++) {
								if(qe.ID!=idquestupdate[j]){
									String[] cat = mFont.tahoma_7_white.splitFontArray(qe.strNhacNv, InfoItem.wcat);
									for (int k = 0; k < cat.length; k++) {
										GuiQuest.listNhacNv.add(new InfoItem(cat[k]));
									}
								}
							}
					}
				}
				//tttt
				break;
			case Cmd.SKILL_CHAR:
				int sizeKill = msg.reader().readShort();
				//Cout.println("cmd skillllllllllllll "+sizeKill);
				int idskill0 = 0;
				for(int i = 0; i < sizeKill; i++){
					byte id =msg.reader().readByte();
					if(i==0) idskill0 = id;
					short idIcon = msg.reader().readShort();
					short idIconTron = msg.reader().readShort();//icon skill tron
					String name = msg.reader().readUTF();
					byte typeskill = msg.reader().readByte();//chu dong or bi dong
					String mota = msg.reader().readUTF(); //mo ta
					byte ntaget = msg.reader().readByte();//so luong muc tieu danh
					byte typebuff = msg.reader().readByte();//loai skill buff
					byte subeff = msg.reader().readByte();//so luong muc tieu danh
					short range = msg.reader().readShort(); //pham vi
					short ideff =  msg.reader().readShort();
					byte nlevel = msg.reader().readByte();
					SkillTemplate skilltemplate = new SkillTemplate(id, name, idIcon,typeskill,ntaget,typebuff,subeff,range,mota);
					skilltemplate.iconTron = idIconTron;
					skilltemplate.ideff = ideff;
//					Cout.println(id+" ideff "+ideff);

					Cout.println(id+" SKILL_CHAR nlevel "+nlevel);
					skilltemplate.khoitaoLevel(nlevel);//// tong so level cua 1 skill
					for (int j = 0; j < nlevel; j++) {
						skilltemplate.levelChar = new short[nlevel];
						skilltemplate.levelChar[j] = msg.reader().readShort(); // mp hao
						skilltemplate.mphao[j] = msg.reader().readShort(); // mp hao
						skilltemplate.cooldown[j] = msg.reader().readInt(); // cooldow thoi gian cho skill
//						Cout.println(j+" cooldown "+skilltemplate.cooldown[j]);
						skilltemplate.timebuffSkill[j] = msg.reader().readInt(); /// thoi gian buff cua skill
						skilltemplate.pcSubEffAppear[j]  = msg.reader().readByte();// ty le xuat hien hieu ung phu
						skilltemplate.tExistSubEff[j]  = msg.reader().readShort(); // thoi gian ton tai cua hieu ung phu
						skilltemplate.usehp[j]  = msg.reader().readShort(); // ty le tang % su dung binh hp
						skilltemplate.usemp[j]  = msg.reader().readShort(); // ty le tang % su dung binh mp
						skilltemplate.ntargetlv[j]  = msg.reader().readByte();
						skilltemplate.rangelv[j]  = msg.reader().readShort(); //phạm vi ảnh hưởng của skill
						skilltemplate.dame[j] = msg.reader().readShort(); 
					}

					SkillTemplates.add(skilltemplate); // làm giống như itemtemplate
					
					
				}
//				 Char.myChar().mQuickslot[i].setIsSkill(idSkillGan[i], false);
//				for (int i = 0; i < Char.myChar().mQuickslot.length; i++) {
//					if( Char.myChar().mQuickslot[i].idSkill!=-1)
//						return;
//				}
//				//troung hop chua gan phim nao het
//				Char.myChar().mQuickslot[0].setIsSkill(idskill0, false);
//				QuickSlot.SaveRmsQuickSlot();
				break;
			case Cmd.REQUEST_SHOP:
				ShopMain.idItemtemplate = null;
				byte menuId = msg.reader().readByte();

				int itemlistSize = msg.reader().readShort();
				ShopMain.indexidmenu = menuId;
				ShopMain.idItemtemplate = new short[itemlistSize];
				for(int i = 0; i < itemlistSize; i++ ){
					short idTemplate = msg.reader().readShort();
					
					ShopMain.idItemtemplate[i] = idTemplate;
				}
				ShopMain.getItemList(ShopMain.indexidmenu,ShopMain.idItemtemplate);
				break;
			case Cmd.CMD_DYNAMIC_MENU:
//				Cout.println(getClass(), " nhan ve list tabshop");
				byte menuShopsize = msg.reader().readByte(); // soluong menu shop
				ShopMain.idMenu = new int[menuShopsize];
				ShopMain.nameMenu = new String[menuShopsize];
				ShopMain.dis = new String[menuShopsize];
				ShopMain.cmdShop = new Command[menuShopsize];
//				ShopMain.idItemtemplate = new int[menuShopsize];
				for(int i = 0; i < menuShopsize; i++){
					byte idmenu = msg.reader().readByte();
					ShopMain.idMenu[i] = idmenu;
					String menuname = msg.reader().readUTF();
					ShopMain.nameMenu[i] = menuname;
//					Cout.println(i+"  idMenu  "+ShopMain.idMenu[i]+"  name "+ShopMain.nameMenu[i]);
					String dis = msg.reader().readUTF();
					ShopMain.dis[i] = dis;
				}
				break;
			case Cmd.TRADE:
				byte typeTrade = msg.reader().readByte();

				Cout.println2222(" typeTrade "+typeTrade);
				if(typeTrade == Constants.INVITE_TRADE){ // moi giao dich
					short CharIDpartner = msg.reader().readShort();
					Char.myChar().partnerTrade = null;
					Char.myChar().partnerTrade = GameScreen.findCharInMap(CharIDpartner);
					Cout.println(CharIDpartner+ " moigiao dich "+Char.myChar().partnerTrade);
					GameCanvas.startYesNoDlg(msg.reader().readUTF(), GameScreen.gI().cmdAcceptTrade, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeTrade == Constants.ACCEPT_INVITE_TRADE){ // dong y giao dich
					short CharIDpartner = msg.reader().readShort();
					if(Char.myChar().partnerTrade==null)
						Char.myChar().partnerTrade = GameScreen.findCharInMap(CharIDpartner);

					GameScreen.isBag=false;
					GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.gameScreen.guiMain.menuIcon.iconTrade.performAction();

					Cout.println(getClass(), CharIDpartner+" moigiao dich2 "+Char.myChar().partnerTrade);
//					
//					GameScreen.gI().guiMain.moveClose=false;
//					indexpICon = Constants.ICON_TRADE;
//					trade=new TradeGui(GameCanvas.wd6-20, 20);
//					trade.SetPosClose(cmdClose);
//					paintButtonClose=true;
//					MenuIcon.lastTab.add(""+Constants.ICON_TRADE);
					Service.gI().requestinventory();
					//isPaintTrade = true;
				}else if(typeTrade == Constants.MOVE_ITEM_TRADE){
					byte typeItem = msg.reader().readByte(); // loại
					byte typeItemmove = msg.reader().readByte(); // đưa xuống hay đưa lên 
					short charId = msg.reader().readShort();
//					if(Char.myChar().partnerTrade==null){
//						Cout.println(charId+ " moigiao dich222 "+Char.myChar().partnerTrade);
//						Char.myChar().partnerTrade= GameScreen.findCharInMap(charId);
//					}
					if(typeItemmove == 0){ // add item
						if(charId  == Char.myChar().charID){ // add vao danh sach item giao dịch của mình
//							Char.myChar().ItemMyTrade = new Item[8];
							for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
							
								if(Char.myChar().ItemMyTrade[i] == null){
									Char.myChar().ItemMyTrade[i] = new Item();
									short IdItem = msg.reader().readShort();
									Char.myChar().ItemMyTrade[i].itemId = IdItem;
									short iditemTemplate = msg.reader().readShort();
									Cout.println(IdItem+" iditemTemplate "+iditemTemplate);
									if(iditemTemplate != -1){
										Char.myChar().ItemMyTrade[i].template = ItemTemplates.get(iditemTemplate);
									}
//									byte optionsize = msg.reader().readByte();
//									for (int j = 0; j < optionsize; j++) {
//										String info = msg.reader().readUTF();
//										byte colorname = msg.reader().readByte();
//									}
									break;
								}
							}
						}else{
							for(int i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.length; i++){ // add vào danh sách item của parner
								if(Char.myChar().partnerTrade.ItemParnerTrade[i] == null){
									//System.out.println("ADD ITEM PARNER TRADE");
									Char.myChar().partnerTrade.ItemParnerTrade[i] = new Item();
									short IdItem = msg.reader().readShort();
									Char.myChar().partnerTrade.ItemParnerTrade[i].itemId = IdItem;
									short iditemTemplate = msg.reader().readShort();
									Cout.println("partner iditemTemplate "+iditemTemplate);
									if(iditemTemplate != -1){
										Char.myChar().partnerTrade.ItemParnerTrade[i].template = ItemTemplates.get(iditemTemplate);
									}
//									byte optionsize = msg.reader().readByte();
//									for (int j = 0; j < optionsize; j++) {
//										String info = msg.reader().readUTF();
//										byte colorname = msg.reader().readByte();
//									}
									break;
								}
							}
						}
					}else{ // remove item
						Cout.println(getClass(),Char.myChar().charID+ "remove item "+charId);
						if(Char.myChar().charID == charId){
							Cout.println(getClass(), "remove Me item ");
							short IdItem = msg.reader().readShort();
							for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
								if(Char.myChar().ItemMyTrade[i] != null){
									if(Char.myChar().ItemMyTrade[i].itemId == IdItem){
										Cout.println(getClass(),i+ "remove item null");
										Char.myChar().ItemMyTrade[i] = null;
										GameCanvas.gameScreen.guiMain.menuIcon.trade.indexSelect1 = -1;
										break;
									}
								}
							}
						}else{
							Cout.println(getClass(), "remove parner item ");
//							Char.partnerTrade.ItemParnerTrade = new Item[15]; 
							short IdItem = msg.reader().readShort();
							for(int i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.length; i++){
								if(Char.myChar().partnerTrade.ItemParnerTrade[i] != null){
									if(Char.myChar().partnerTrade.ItemParnerTrade[i].itemId == IdItem){
										Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
//										TradeGui.indexSelect1 = -1;
									}
									
								}
							}
						}
					}
					
					
				}else if(typeTrade == Constants.CANCEL_TRADE){
					short charidP = msg.reader().readShort();
					Char charPar = GameScreen.findCharInMap(charidP);
					Cout.println(" CANCEL_TRADE ");
					GameCanvas.gameScreen.guiMain.menuIcon.trade=null;
					GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.startOKDlg(charPar.cName+(msg.reader().readUTF()));
					//GameScreen.gI().tradeGui = null;
				}else if(typeTrade == Constants.LOCK_TRADE){
					short charidP = msg.reader().readShort();
//					Char charPar = GameScreen.findCharInMap(charidP);
					if(Char.myChar().charID != charidP){

						Cout.println(" LOCK_TRADE other ");
						GameCanvas.gameScreen.guiMain.menuIcon.trade.block2 = true;
					}else{

						Cout.println(" LOCK_TRADE me ");
						GameCanvas.gameScreen.guiMain.menuIcon.trade.block1 = true;
					}

				}else if(typeTrade == Constants.TRADE){
					short charidP = msg.reader().readShort();
					Char cTrade = GameScreen.findCharInMap(charidP);
					GameCanvas.startYesNoDlg(cTrade.cName+msg.reader().readUTF(), TradeGui.cmdTradeEnd, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
					//GameScreen.gI().tradeGui = null;
					for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
						Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
						Char.myChar().ItemMyTrade[i] = null;
					}
				}else if(typeTrade == Constants.END_TRADE){
					Cout.println(" end trade ");
					for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
						Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
						Char.myChar().ItemMyTrade[i] = null;
					}
					Char.myChar().partnerTrade = null;
					GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
					GameCanvas.gameScreen.guiMain.menuIcon.trade = null;
				}
				break;
			case Cmd.FRIEND:
				byte typeFriend = msg.reader().readByte();
				if(typeFriend == Friend.INVITE_ADD_FRIEND){
					Char.myChar().idFriend = msg.reader().readShort();
					String tempDebug = msg.reader().readUTF();
					//System.out.println("IDfiend ----> "+Char.myChar().idFriend+"  "+tempDebug);
					GameCanvas.startYesNoDlg(tempDebug, GameScreen.gI().cmdComfirmFriend, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeFriend == Friend.ACCEPT_ADD_FRIEND){
					short CharIDFriend = msg.reader().readShort();
					c = GameScreen.findCharInMap(CharIDFriend);
					Char.myChar().vFriend.addElement(c);
				}else if(typeFriend == Friend.REQUEST_FRIEND_LIST){
					Char.myChar().vFriend.removeAllElements();
					byte sizeFriendList = msg.reader().readByte();
					for(int i = 0; i < sizeFriendList; i++){
						Char ch = new Char();
						ch.isOnline = msg.reader().readBoolean();
						if(ch.isOnline){
							ch.charID = msg.reader().readShort(); // không online defaut là 0							
						}
						ch.CharidDB = msg.reader().readShort();
						ch.cName = msg.reader().readUTF();
						ch.clevel = msg.reader().readShort();
//						ch.head = msg.reader().readShort();
						Char.myChar().vFriend.addElement(ch);
					}
				}else if(typeFriend == Friend.UNFRIEND){
					short CharID = msg.reader().readShort();
					short charUser = msg.reader().readShort();
				//	System.out.println("Unfriend -----> "+CharID);
					for(int i = 0; i < Char.myChar().vFriend.size(); i++){
						Char chardel = (Char) Char.myChar().vFriend.elementAt(i);
						if(chardel.charID != 0){ // khi online 
							if(chardel.charID == CharID)
								Char.myChar().vFriend.removeElementAt(i);		
						}else{
							if(chardel.CharidDB == charUser){ // khi off line

								Char.myChar().vFriend.removeElementAt(i);		
							}
						}
					}
				}
				break;
			case Cmd.PARTY:
				byte typeParty = msg.reader().readByte();
				Cout.println("type Party  "+typeParty);
				if(typeParty == Type_Party.INVITE_PARTY){
					Party.gI().charId = msg.reader().readShort();
					GameCanvas.startYesNoDlg(msg.reader().readUTF(), GameScreen.gI().cmdAcceptParty, new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
				}else if(typeParty == Type_Party.GET_INFOR_PARTY){
					short PartyId = msg.reader().readShort();
					short CharLeaderid = msg.reader().readShort();
					if(CharLeaderid==Char.myChar().charID)
						Party.gI().isLeader = true;
					Cout.println(Party.gI().isLeader+"CharLeaderid ==== "+CharLeaderid);
					byte membersize = msg.reader().readByte();
					short[] charIDmeber =  new short[membersize];
					short[] lvchar = new short[membersize];
					short[] idhead = new short[membersize];
					for(int i = 0; i < membersize; i++){
						charIDmeber[i] = msg.reader().readShort();
						lvchar[i] = msg.reader().readShort();
						idhead[i] = msg.reader().readShort();
		                //m.dos.writeShort(members.get(i).getLevel());
		                //  m.dos.writeShort(members.get(i).getIdPartHead());
					}
					GameScreen.hParty.contains(PartyId+"");
					GameScreen.hParty.put(PartyId+"",(Object)new Party(PartyId, charIDmeber, CharLeaderid,lvchar,idhead));
				}else if(typeParty == Type_Party.OUT_PARTY){
					short IdChar = msg.reader().readShort();
					if(IdChar==Char.myChar().charID){
						Cout.println("bi kichkkkkkkkkkkkkkk");
						GameScreen.hParty.clear();
						Party party  = (Party) GameScreen.hParty.get(Char.myChar().idParty+"");
						party.vCharinParty.removeAllElements();
					}
						
					Char chaRe = GameScreen.findCharInMap(IdChar);
					Party party = null;
					if(chaRe != null){
						party = (Party) GameScreen.hParty.get(chaRe.idParty+"");
						for(int i = 0; i < party.vCharinParty.size();i++){
							Char charRemove = (Char) party.vCharinParty.elementAt(i);
							if(charRemove.charID == IdChar){
								charRemove.idParty = -1;
								charRemove.isLeader = false;
								party.vCharinParty.removeElementAt(i);
							}
							
						}
						if(party.vCharinParty.size()<2){
							GameScreen.hParty.clear();
							party.vCharinParty.removeAllElements();
						}
						
					}else{
						chaRe = Char.myChar();
						party = (Party) GameScreen.hParty.get(chaRe.idParty+"");
						for(int i = 0; i < party.vCharinParty.size();i++){
							Char charRemove = (Char) party.vCharinParty.elementAt(i);
							if(charRemove.charID == IdChar){
								charRemove.idParty = -1;
								charRemove.isLeader = false;
								party.vCharinParty.removeElementAt(i);
							}
							
						}
					}
					if(party.vCharinParty.size() <= 0)
						GameScreen.hParty.remove(chaRe.idParty+"");
//					for(int i = 0; i < GameScreen.vParty.size(); i++){
//						Party party = (Party) GameScreen.vParty.elementAt(i);
//						for(int j = 0; j < party.vCharinParty.size(); j++){
//							Char charre = (Char) party.vCharinParty.elementAt(j);
//							if(charre.charID == IdChar)
//								party.vCharinParty.removeElement(charre);
//							
//						}
//						if(party.vCharinParty.size() <= 1);
//					}
				}else if(typeParty == Type_Party.DISBAND_PARTY){
					//System.out.println("DISBAND_PARTY");
					short idParty = msg.reader().readShort();
					//System.out.println("ID Party ---> "+idParty);
					Party party = (Party) GameScreen.hParty.get(idParty+"");
					
					for(int i = 0;i < party.vCharinParty.size();i++){
						Char ch = (Char)party.vCharinParty.elementAt(i);
						ch.idParty = -1;
						ch.isLeader = false;
						party.vCharinParty.removeElementAt(i);
					}
//					part.vCharinParty.removeAllElements();
					GameScreen.hParty.remove(idParty+"");
				}else if(typeParty == Type_Party.GET_INFOR_NEARCHAR){
					GameScreen.charnearByme.removeAllElements();
					byte sizeCharlist = msg.reader().readByte();
					//System.out.println("Size ----> "+sizeCharlist);
					for(int i = 0; i < sizeCharlist; i++){
						Char ch = new Char();
						ch.charID = msg.reader().readShort();
						//System.out.println("IDChar ----> "+ch.charID);
						ch.cName = msg.reader().readUTF();
						//System.out.println("CharName ----> "+ch.cName);
						ch.idParty = msg.reader().readShort();
						//System.out.println("Idparty ----> "+ch.idParty);
						GameScreen.charnearByme.addElement(ch);
					}
				}
				break;
			case Cmd.MENU_NPC:

//				Cout.println2222("nhan ve menuuuuuuu");
				int idNpc_menu = msg.reader().readInt();
				int size_menu = msg.reader().readByte();

//				Cout.println2222(idNpc_menu+ " nhan ve menuuuuuuu  "+size_menu);
				Vector vmenu = new Vector();
				
				for (int i = 0; i < size_menu; i++) {
					String text_cmd = msg.reader().readUTF();
					vmenu.addElement(new Command(text_cmd, GameCanvas.instance,GameCanvas.cMenuNpc,i+""));//i index_menu
					
				}
				Npc npc_dem = null;
				for (int i = 0; i < GameScreen.vNpc.size(); i++) {
					Npc npc = (Npc) GameScreen.vNpc.elementAt(i);
					if (npc!=null&&npc.template!=null&&npc.template.npcTemplateId == idNpc_menu && npc.equals(Char.myChar().npcFocus)) {
						npc_dem = npc;
					}
				}
				if(npc_dem!=null&&vmenu.size()>0)
				{
					Cout.println2222("startmenuNpc nhan ve");
					//startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "")
					GameCanvas.menu.doCloseMenu();
					GameCanvas.menu.startAtNPC(vmenu, 0, npc_dem.npcId, npc_dem, "");
				}
//				try {
//					int sizee = msg.reader().readByte();
//					GameScreen.showChatPopup(npc.template.contendChat, 0, null, 0, null);
//					final String[] arrSub = new String[sizee/*msg.reader().readByte()*/];
//					for (int i = 0; i < sizee; i++) {
////						for (int j = 0; j < arrSub.length; j++) {
//							arrSub[i] = msg.reader().readUTF();
////						}
//						vmenu.addElement(new Command(arrSub[i], GameCanvas.instance,88820,arrSub));
//					}
////					GameCanvas.startYesNoDlg(npc.template.contendChat, 0, null, 0, null, arrSub[0], arrSub[1]);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				if (Char.myChar().npcFocus == null)
//					return;
//				GameCanvas.menu.startAt(vmenu, 3);
				break;
			case Cmd.NPC:
				GameScreen.vNpc.removeAllElements();
				byte sizeNpc = msg.reader().readByte();
				for(int i = 0; i < sizeNpc; i++){
					short idNpc = msg.reader().readShort();
					short npcX = msg.reader().readShort();
					short npcY = msg.reader().readShort();
					short npcIdtemplate = msg.reader().readShort();
					short npciDicon = 0;//msg.reader().readShort();
					Npc npcz = new Npc(npcX, npcY, npcIdtemplate, npciDicon);
					npcz.npcId = npcIdtemplate;
					for (int j = 0; j < Quest.listUnReceiveQuest.size(); j++) {
						Quest q1 = (Quest)Quest.listUnReceiveQuest.elementAt(j);
						if(q1.idNPC_From==npcIdtemplate)
							npcz.typeNV = 0;
					}
					for (int j = 0; j < Quest.vecQuestDoing_Main.size(); j++) {
						Quest q2 = (Quest)Quest.vecQuestDoing_Main.elementAt(j);
						if(q2.idNPC_To==npcIdtemplate)
							npcz.typeNV = 1;
					}
					for (int j = 0; j < Quest.vecQuestDoing_Sub.size(); j++) {
						Quest q3 = (Quest)Quest.vecQuestDoing_Sub.elementAt(j);
						if(q3.idNPC_From==npcIdtemplate)
							npcz.typeNV = 1;
					}
					for (int j = 0; j < Quest.vecQuestFinish.size(); j++) {
						Quest q4 = (Quest)Quest.vecQuestFinish.elementAt(j);
						if(q4.idNPC_From==npcIdtemplate||q4.idNPC_To==npcIdtemplate)
							npcz.typeNV = 1;
					}
					GameScreen.vNpc.addElement(npcz);

					//Cout.println("nhan ve npppppppppppppp sizeNpc add "+GameScreen.vNpc.size());
				}
				
				break;
			case Cmd.PICK_REMOVE_ITEM:
				byte typeItemPick = msg.reader().readByte();
				short idItemPick = msg.reader().readShort();
				Cout.println2222(typeItemPick+"  PICK_REMOVE_ITEM   "+idItemPick);
				if(typeItemPick==-1){
					for (int i = 0; i < GameScreen.vItemMap.size(); i++) {
						ItemMap itempick = (ItemMap) GameScreen.vItemMap.elementAt(i);
						if(itempick.itemMapID==idItemPick){
							if(Char.myChar().itemFocus!=null&&Char.myChar().itemFocus.itemMapID==idItemPick)
								Char.myChar().itemFocus = null;
							GameScreen.vItemMap.removeElementAt(i);
							break;
						}
					}
					return;
				}
				if(Char.myChar().itemFocus!=null&&Char.myChar().itemFocus.itemMapID==idItemPick){
					Char.myChar().itemFocus = null;
					Char.myChar().clearFocus(10);
				}
				short idPlayerPick = msg.reader().readShort();
				c = GameScreen.findCharInMap(idPlayerPick);
				if(c == null){
					
				}
				else {
					for (int i = 0; i < GameScreen.vItemMap.size(); i++) {
						ItemMap itempick = (ItemMap) GameScreen.vItemMap.elementAt(i);
						if(itempick.itemMapID==idItemPick){
						itempick.setPoint(c.cx, c.cy);
						if(c.charID==Char.myChar().charID)
							itempick.isMe = true;
	//					if (itempick.itemMapID == idItemPick) {
	//						GameScreen.vItemMap.removeElementAt(i);
	//						break;
	//					}
						}
					}
				}
				
				break;
			case Cmd.DROP_ITEM:
				byte typeDrop = msg.reader().readByte();
				byte typeItemRotRa = msg.reader().readByte();//1 đánh quái rớt ra, 0 char vứt ra.
				short IdMonterDie = msg.reader().readShort();
//				short iconID = msg.reader().readShort();
				short id = msg.reader().readShort();
				short idTemplate = msg.reader().readShort();
				//Cout.println("DROP_ITEM ---->id "+id+" idTemplate  "+idTemplate);
				ItemMap itemDrop = null;
				if(typeItemRotRa==1){

					Mob mons = GameScreen.findMobInMap(IdMonterDie);
					if(mons!=null){
						itemDrop = new ItemMap(id, idTemplate, mons.x, mons.yFirst+10);
	//					ServerEffect.addServerEffect(35, mons.x, yItem-20, 3);
					}
					else {
	//					ServerEffect.addServerEffect(35, xItem, yItem-20, 3);
						short xItem = msg.reader().readShort();
						short yItem = msg.reader().readShort();
						itemDrop = new ItemMap(id, idTemplate, xItem, yItem);
					}
					GameScreen.vItemMap.addElement(itemDrop);
					GameScreen.vNhatItemMap.addElement(itemDrop);
				}else{
					Char ch = GameScreen.findCharInMap(IdMonterDie);
					if(ch!=null){
						itemDrop = new ItemMap(id, idTemplate, ch.cx, ch.cy);
						GameScreen.vItemMap.addElement(itemDrop);
						GameScreen.vNhatItemMap.addElement(itemDrop);
					}
				}
				itemDrop.type = typeDrop;
				break;
			case Cmd.REMOVE_TARGET:

//				byte typeRemove = msg.reader().readByte();
//				Cout.println("REMOVE_TARGET "+typeRemove);
//				if(typeRemove == Type_Object.CAT_MONSTER){
//					short idMobRetaget = msg.reader().readShort();
//					for(int i = 0; i < GameScreen.vMob.size(); i++){
//						Mob	mobTg = (Mob) GameScreen.vMob.elementAt(i);
//						if(mobTg.mobId == idMobRetaget)
//							mobTg.cFocus = null;
////						if(mobTargetRe.cFocus != null){
////							mobTargetRe.cFocus = null;
////						}
//					}
//				}
				break;
				
			case Cmd.DIE:
				byte byteDie = msg.reader().readByte(); // 
				if(byteDie == Type_Object.CAT_PLAYER){
					short playerID = msg.reader().readShort();
					if(playerID==Char.myChar().charID){
						GameScreen.gI().guiMain.moveClose = true;
						if(MenuIcon.isShowTab)
							GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
						if(GameScreen.isBag)
							GameCanvas.AllInfo.cmdClose.performAction();
//						GameCanvas.startCommandDlg("Bạn có muốn hồi sinh tại chỗ (10xu)?", 
//								new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
//								new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
					}
					c = GameScreen.findCharInMap(playerID);

					if(c != null){
						c.cHP = 0;
						c.statusMe = Char.A_DEAD;
						//Cout.println("dieeeeeeeeeeeeee   ");
					}
					
				}else if(byteDie == Type_Object.CAT_MONSTER){
					short mobID = msg.reader().readShort();
					for (int i = 0; i < GameScreen.vMob.size(); i++) {
						Mob mob = (Mob) GameScreen.vMob.elementAt(i);
						if(mob.mobId==mobID){
							mob.status = Mob.MA_DEADFLY;
							mob.startDie();
							//Cout.println("Mob   dieeeeeeeeeeeeee   ");
							if(Char.myChar().mobFocus!=null&&Char.myChar().mobFocus.mobId==mobID)
								Char.myChar().mobFocus = null;
							break;
						}
					}
//					if(Char.myChar().mobFocus!=null&&Char.myChar().mobFocus.mobId == mobID)
//						Char.myChar().mobFocus.status = Mob.MA_DEADFLY;
//						Char.myChar().mobFocus.startDie();
				}
				
				break;
			case Cmd.ATTACK:
				byte typeAtt = msg.reader().readByte(); // 0 PLAYER 1 MONTERS
//				Cout.println("typeAtt  "+typeAtt);
				if(typeAtt == 0){// PLAY

					byte Cat_type = msg.reader().readByte(); // 0 playerAtt monters 1 player player
//					Cout.println("Cat_typeAtt  "+Cat_type);
					short PlayerAttID = msg.reader().readShort();
					 c = GameScreen.findCharInMap(PlayerAttID);
				     if (c == null)
					      c = Char.myChar();
				     if(Cat_type==0){//player attack monster
						short mobvictim = msg.reader().readShort();
						for(int i = 0; i < GameScreen.vMob.size(); i++){
							Mob mob = (Mob) GameScreen.vMob.elementAt(i);
							if(mob.mobId == mobvictim)
								c.mobFocus = mob;
							if(mobvictim == mob.mobId){
								c.mobFocus.hp = msg.reader().readInt();
								c.cMP= msg.reader().readInt();
								c.cdame = msg.reader().readInt();
								c.mobFocus.hp = (c.mobFocus.hp<0?0:c.mobFocus.hp);
								if(c.cdame>0)
									GameScreen.startFlyText("-" +c.cdame, c.mobFocus.x, c.mobFocus.y-2*mob.h-5, 0, -2, mFont.RED);
								else GameScreen.startFlyText("miss", c.mobFocus.x, c.mobFocus.y-2*mob.h-5, 0, -2, mFont.RED);
								
	//							ServerEffect.addServerEffect(91,  c.mobFocus.x, c.mobFocus.y, 1);
								if(PlayerAttID!=Char.myChar().charID){

									c.cdir = (c.cx-c.mobFocus.x)>0?-1:1;
									c.mobFocus.setInjure();
									c.mobFocus.injureBy = c;
									c.mobFocus.status = Mob.MA_INJURE;
								}
								if(mob.hp <= 0){
									mob.status = Mob.MA_DEADFLY;
								}
							}
						}
						//byte idskill
						byte idskill = msg.reader().readByte();

						if(idskill>-1&&idskill< GameScreen.sks.length-1){
							byte ideff = msg.reader().readByte();
							if(c.charID!=Char.myChar().charID){
								if(idskill==0)
									Music.play(Music.ATTACK_0, 0.5f);
								else Music.play(Music.SKILL2, 0.5f);
								c.setSkillPaint(GameScreen.sks[ideff], Skill.ATT_STAND);
							}
						}
				     }else if(Cat_type==1){//player attack player
//				    	 msg.dos.writeShort(this.id);
				    	 c.cMP = msg.reader().readInt();
				    	 byte sizeBeAttack = msg.reader().readByte();
//				            msg.dos.writeInt(this.getMp());
//				            msg.dos.writeByte(target.size());
				    	 for (int i = 0; i < sizeBeAttack; i++) {
				    		 short charBeAttack = msg.reader().readShort();
				    		 Char charBe = GameScreen.findCharInMap(charBeAttack);
				    		 if(charBe!=null){
				    			 charBe.cHP = msg.reader().readInt();
				    			 Cout.println(charBe.cName+"  onAttackkkk charBe.cHP "+charBe.cHP);
//				    			 if(charBe.cHP>0)
//				    			 charBe.statusMe = Char.A_INJURE;

				    			 charBe.doInjure(1, 0,false,1 );
				 				c.cdir = (c.cx-charBe.cx)>0?-1:1;
						    	 int dam = msg.reader().readInt();
								ServerEffect.addServerEffect(25, charBe.cx, charBe.cy-20, 1);
						    	 GameScreen.startFlyText("-" +dam, charBe.cx,charBe.cy-60, 0, -2, mFont.RED);
									
				    		 }
						}
				           byte idskill = msg.reader().readByte();

							if(idskill>-1&&idskill< GameScreen.sks.length-1){
								byte ideff = msg.reader().readByte();
								if(c.charID!=Char.myChar().charID){
									c.setSkillPaint(GameScreen.sks[ideff], Skill.ATT_STAND);
									Music.play(Music.ATTACK_0, 0.5f);
								}
								}
				     }
					//paint skill for all char
				    
//				     if ((TileMap.tileTypeAtPixel(c.cx, c.cy) & TileMap.T_TOP) == TileMap.T_TOP)
//				      c.setSkillPaint(GameScreen.sks[2],
//				        Skill.ATT_STAND);
//				     else
//				      c.setSkillPaint(GameScreen.sks[2],
//				        Skill.ATT_FLY);
					
				}
				else if(typeAtt == 1){
					short mobAttId = msg.reader().readShort();
					Mob mob = null;
					for(int i = 0; i < GameScreen.vMob.size(); i++){
						Mob mobs = (Mob) GameScreen.vMob.elementAt(i);
						if(mobs.mobId==mobAttId)
							mob = mobs;
						
					}
					short CharIdVictim = msg.reader().readShort();
					//Cout.println(mobAttId+ "  monster attack charID "+CharIdVictim);
					
					if(mob!=null){
						Char charbiattack = GameScreen.findCharInMap(CharIdVictim);

						mob.cFocus  = charbiattack;
						if(mob.cFocus == null){
							mob.f = -1;
							mob.cFocus = Char.myChar();
						}
						mob.dir = (mob.x-mob.cFocus.cx>0?-1:1);
						int hpmat = msg.reader().readInt();
						mob.cFocus.cHP = hpmat;
						if(charbiattack!=null){
							charbiattack.cHP = hpmat;
						}

						mob.dame = msg.reader().readInt();
						mob.status = Mob.MA_ATTACK;
						if(mob.status == Mob.MA_ATTACK){
							//ServerEffect.addServerEffect(91, mob.cFocus.cx, mob.cFocus.cy, 1);
							//Cout.println(getClass(),"   add dame");
							GameScreen.startFlyText("-" +mob.dame, mob.cFocus.cx, mob.cFocus.cy-60, 0, -3, mFont.RED);
							//mob.cFocus.statusMe = Char.A_INJURE;						
						}
						if(mob.typeMob==Mob.TYPE_MOB_TOOL){
							ServerEffect.addServerEffect(21, mob.x, mob.y, 1);
						}
						
						mob.setAttack(mob.cFocus);
					}
//					if(mob.cFocus != null){
//						
//					}
				}
				break;
			case Cmd.REQUEST_MONSTER_INFO:
				short idmob = msg.reader().readShort();
				String namemob = msg.reader().readUTF();
				int maxhp = msg.reader().readInt();
				int hp =  msg.reader().readInt();
				for(int i = 0; i < GameScreen.vMob.size(); i++){
					Mob modInMap = (Mob) GameScreen.vMob.elementAt(i);

					if(modInMap !=  null&&modInMap.mobId==idmob){
//						Char.myChar().mobFocus.mobId 
						modInMap.mobName = namemob;
						modInMap.maxHp = maxhp;
						modInMap.hp = hp;
						Cout.println("REQUEST_MONSTER_INFO  ");
					}
				}
				break;
			case Cmd.CHAT:
				byte typeChat = msg.reader().readByte();
				if(typeChat == Type_Chat.CHAT_MAP){ // char map
					
					short useriD = msg.reader().readShort();
					String text = msg.reader().readUTF();
					if (Char.myChar().charID == useriD)
						c = Char.myChar();
					else
						c = GameScreen.findCharInMap(useriD);
					if (c == null)
						return;
					ChatPopup.addChatPopup(text, 200, c);
					ChatManager.gI().addChat(mResources.PUBLICCHAT[0], c.cName, text);
					
				}else if(typeChat == Type_Chat.CHAT_WORLD){
					
					ChatWorld.ChatWorld(msg);
					
				}else if(typeChat == Type_Chat.CHAT_FRIEND){
					
					ChatPrivate.ChatFriend(msg);
				}
				break;
			case Cmd.GET_ITEM_INVENTORY:
				//Char.myChar().arrItemBag = null;
				byte typeInventory = msg.reader().readByte();
					Char.myChar().luong = msg.reader().readLong();
					Char.myChar().xu = msg.reader().readLong();
					if(typeInventory==3){ //cat item
						Char.myChar().arrItemBag = new Item[msg.reader().readByte()];
						for (int i = 0; i < Char.myChar().arrItemBag.length; i++) {
						byte typeItem = msg.reader().readByte();
						short idItem = msg.reader().readShort();
						short itemTemplateId = msg.reader().readShort();
							if (itemTemplateId != -1) {
								Char.myChar().arrItemBag[i] = new Item();
								Char.myChar().arrItemBag[i].itemId = idItem;
								Char.myChar().arrItemBag[i].typeUI = Item.UI_BAG;
								Char.myChar().arrItemBag[i].indexUI = i;
								Char.myChar().arrItemBag[i].template = ItemTemplates.get(itemTemplateId);
								if (Char.myChar().arrItemBag[i].template != null && Item.isBlood(Char.myChar().arrItemBag[i].template.type)
                                        || Char.myChar().arrItemBag[i].template != null && Item.isMP(Char.myChar().arrItemBag[i].template.type))
								{
									Char.myChar().arrItemBag[i].quantity = msg.reader().readShort();//so luong
									Char.myChar().arrItemBag[i].value = msg.reader().readShort();
								} 
//								 Char.myChar().arrItemBag[i].level_dapdo = msg.reader().readByte();
//								 Cout.println2222(i+ "   dapdo ===  "+Char.myChar().arrItemBag[i].level_dapdo);
			                    
							}
						}
				}else if(typeInventory==4){
					
				}
				break;
			case Cmd.PLAYER_REMOVE:
				short CharIDRemove = msg.reader().readShort();
				if(CharIDRemove==Char.myChar().charID){
//					GameCanvas.instance.resetToLoginScr();
					return;
				}
				for(int i = 0; i < GameScreen.vCharInMap.size(); i++){
					c = (Char) GameScreen.vCharInMap.elementAt(i);
					if(CharIDRemove == c.charID){
						GameScreen.vCharInMap.removeElementAt(i);
					}
				}
				break;
			case Cmd.PLAYER_INFO:
				
				short charID = msg.reader().readShort();

				int cMaxHP = msg.reader().readInt();
				int cHP = msg.reader().readInt();
				short clevel = msg.reader().readShort();
				byte  typeBePhongAn = msg.reader().readByte();
				for(int i = 0; i < GameScreen.vCharInMap.size(); i++){
					Char ch = (Char) GameScreen.vCharInMap.elementAt(i);
					if(ch.charID == charID){
						ch.cMaxHP = cMaxHP;
						ch.cHP = cHP;
						if(ch.clevel>0&&ch.clevel<clevel){
							Music.play(Music.LENLV, 10);
							ServerEffect.addServerEffect(22,ch.cx,ch.cy,1);
						}
						if(typeBePhongAn==1)
						{
							ServerEffect.addServerEffect(40, ch, true);
						}
						else
						{
							ServerEffect.removeEffect(40);
						}
						ch.clevel =clevel;
						if(ch.cMaxHP<=0) ch.cMaxHP = (ch.cHP<=0?1:ch.cHP);
					}
				}
//				c.charID = msg.reader().readShort();
//				System.out.println("Charinmap ----> "+c.charID);
//				c.cMaxHP = msg.reader().readShort();
//				c.cHP = msg.reader().readShort();

//				GameScreen.vCharInMap.addElement(c);
				break;
			case Cmd.REQUEST_IMAGE:// yeu cau hinh tu server

				byte subImg = -1;
				subImg = msg.reader().readByte();
				int idimg = msg.reader().readInt();
				Cout.println(subImg+ "REQUEST_IMAGE receive  "+idimg);
				if(subImg==0){
					byte[] dataImg = Util.readByteArray(msg);
					if(GameCanvas.currentScreen==DownloadImageScreen.gI())
					DownloadImageScreen.isOKNext = true;
					String path = SmallImage.getPathImage(idimg)+""+idimg;
					Rms.saveRMS(mSystem.getPathRMS(path), dataImg);
				}else if(subImg==1){
					int sizeImg  = msg.reader().readByte();
					Cout.println(subImg+ "REQUEST_IMAGE sizeImg  "+sizeImg);
					
					for (int i = 0; i < sizeImg; i++) {
						int lengread = msg.reader().readInt();
						
						byte[] dataImg = Util.readByteArray(msg,lengread);
						String path = SmallImage.getPathImage(idimg)+""+idimg+"/_"+(sizeImg==2?(i==0?1:4):(i+1));
						Cout.println(dataImg.length+ "  REQUEST_IMAGE path  "+path);
						Rms.saveRMS(mSystem.getPathRMS(path), dataImg);
					}
				}
				break;
			case Cmd.PLAYER_MOVE: // char khac move lai kt
				byte typemove = msg.reader().readByte();
				try {
				if(typemove == Const.CAT_MONSTER){
					short idmobb = msg.reader().readShort();
					short idmobx = msg.reader().readShort();
					short idmoby = msg.reader().readShort();
					String namemobb = msg.reader().readUTF();       
					int hpmob = msg.reader().readInt();
					for (int i = 0; i < GameScreen.vMob.size(); i++) {
						Mob mobb = (Mob) GameScreen.vMob.elementAt(i);
						if(mobb.mobId==idmobb&&mobb.status==Mob.MA_INHELL)
						{
							mobb.injureThenDie = false;
							mobb.status = Mob.MA_WALK;
							mobb.x = idmobx;
							mobb.y = idmoby-10;
							mobb.mobName = namemobb;
							ServerEffect.addServerEffect(37, mobb.x, mobb.y-mobb.getH()/4-10, 1);
							mobb.hp = hpmob>mobb.maxHp?mobb.maxHp:hpmob;
						}
					}
//		            m.dos.writeShort(a.id);
//		            m.dos.writeShort(a.x);
//		            m.dos.writeShort(a.y);
//		            m.dos.writeUTF(a.getName(language));
					
				}else if(typemove == Const.CAT_PLAYER){
					
					int charId = msg.reader().readShort();

					short cxMoveLast = msg.reader().readShort();
					short cyMoveLast = msg.reader().readShort();
					String cName = msg.reader().readUTF();
					byte typePk = msg.reader().readByte();
					for (int i = 0; i < GameScreen.vCharInMap.size(); i++)
					{						
						c = (Char) GameScreen.vCharInMap.elementAt(i);
						if (c.charID == charId)
						{
							c.cxMoveLast =cxMoveLast;
							c.cyMoveLast = cyMoveLast;
							c.cName =cName;
							c.typePk =typePk;
							
							c.moveTo(c.cxMoveLast, c.cyMoveLast);						
							c.lastUpdateTime = System.currentTimeMillis();
						}
					}
				}
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				break;
			case Cmd.CHANGE_MAP:
				Cout.println("CHANGE_MAPPPPPPPPPPP  ");
					GameScreen.gI().guiMain.moveClose = false;
					GameScreen.gI().guiMain.menuIcon.indexpICon =0;
					MenuIcon.isShowTab = false;
					GameScreen.vNpc.removeAllElements();
					TileMap.vGo.removeAllElements();
					GameScreen.vMob.removeAllElements();
					GameScreen.vItemMap.removeAllElements();
					GameScreen.vNhatItemMap.removeAllElements();
					BgItem bgi = new BgItem();
//					effdata.readData("/map/data");
					Char.myChar().ischangingMap = true;
					GameScreen.vCharInMap.removeAllElements();
					GameScreen.vCharInMap.addElement(Char.myChar());
					TileMap.mapID = msg.reader().readShort();
					Cout.println("TileMap.mapID  "+TileMap.mapID);
					Effect2.vEffect2.removeAllElements();
					TileMap.mapName = (String)TileMap.listNameAllMap.get(TileMap.mapID+"");
					TileMap.zoneID = msg.reader().readByte();
					Cout.println("TileMap.zoneID  "+TileMap.zoneID);
					short vsize = msg.reader().readShort();
					//Cout.println("SIZE LINK MAP -----> "+vsize);
					for (int i = 0; i < vsize; i++){
						TileMap.vGo.addElement(new Waypoint(msg.reader().readShort(),msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort()));
						
					}
					Char.myChar().cx = msg.reader().readShort();
					Char.myChar().cy = msg.reader().readShort();
					Char.myChar().statusMe = Char.A_FALL;
					TileMap.tileID = msg.reader().readByte();	
					// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
					byte nTile = msg.reader().readByte();
					TileMap.tileIndex = new int[nTile][][];
					TileMap.tileType = new int[nTile][];
//					bgi.loadMapItem();
				
					//bgi.loadMaptable();
					//bgi.loadImgmap();

					for (int i = 0; i < nTile; i++) {
						byte nTypeSize = msg.reader().readByte();
						TileMap.tileType[i] = new int[nTypeSize];
						TileMap.tileIndex[i] = new int[nTypeSize][];
						for (int a = 0; a < nTypeSize; a++) {
							TileMap.tileType[i][a] = msg.reader().readInt();
							byte sizeIndex = msg.reader().readByte();
							TileMap.tileIndex[i][a] = new int[sizeIndex];
							for (int b = 0; b < sizeIndex; b++) {
								TileMap.tileIndex[i][a][b] = msg.reader()
										.readByte();
							}
						}
					}
					TileMap.loadimgTile(TileMap.tileID); // load hinh tile
					TileMap.loadMapfile(TileMap.mapID); // load file map
					TileMap.loadMap(TileMap.tileID); // load va cham 

					Laroi.load();
					GameScreen.loadMapItem(); // load toan bo file data object
					GameScreen.loadMapTable(TileMap.mapID); // object trong 1 map filedata
					TileMap.vItemBg.removeAllElements();
					BgItem.imgPathLoad.clear();
					Mob.imgMob.clear();
					int[] idObjMap = new int [TileMap.vCurrItem.size()];
					for (int i = 0; i < TileMap.vCurrItem.size(); i++) {
						BgItem biMap = (BgItem)TileMap.vCurrItem.elementAt(i);
						if(biMap!=null){
							idObjMap[i] = biMap.idImage;
						}
					}
					Cout.println("TileMap.vCurrItem.size()   "+TileMap.vCurrItem.size());
					byte sizeMod = msg.reader().readByte();
				//	System.out.println("SIZE MOB ---> "+sizeMod);
					Mob.arrMobTemplate = new MobTemplate[sizeMod];
					for(int i = 0; i < Mob.arrMobTemplate.length; i++){
						Mob.arrMobTemplate[i] = new MobTemplate();
//						Mob.arrMobTemplate[i].imgs = null;
						byte idModtemp = msg.reader().readByte();
						short idloadmob = msg.reader().readShort();
						Mob.arrMobTemplate[i].idloadimage = idloadmob;
						
						
						Mob.loadImgMob(idloadmob);
//						Mob.arrMobTemplate[i].data.img = Mob.loadImgMob(idloadmob);
						
						Mob mob = new Mob(idModtemp, msg.reader().readInt(), msg.reader().readByte(),msg.reader().readInt(),
								msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(),msg.reader().readShort(),
								msg.reader().readShort());
						boolean isBoss = msg.reader().readBoolean();
						mob.isBoss = isBoss;
						if(isBoss)
						{

							int sizeSmall = msg.reader().readInt();
							
							
							
							byte[] dataSmall = Util.readByteArray(msg, sizeSmall);
							
							int sizeFrame = msg.reader().readInt();
							

							
							byte[] dataFrame = Util.readByteArray(msg, sizeFrame);
							mob.typeMob = Mob.TYPE_MOB_TOOL;
//							mob.hp = 0;
//							mob.startDie();
//							mob.status  = Mob.MA_INHELL;
							Mob.arrMobTemplate[i].data = new EffectData(); // doc file data quai trong tool
							

							DataInputStream is = null;
							ByteArrayInputStream array = new ByteArrayInputStream(dataSmall);
							is = new DataInputStream(array);
							
							Mob.arrMobTemplate[i].data.readData222(is);
							DataInputStream is2 = null;
							ByteArrayInputStream array2 = new ByteArrayInputStream(dataFrame);
							is2 = new DataInputStream(array2);
							
							Mob.arrMobTemplate[i].data.readhd(is2);
							Cout.println2222("read_data okkkkkkkkkkkkkkk");
							
						}
						mob.idloadimage = idloadmob;
						GameScreen.vMob.addElement(mob);
					}
					
//					Npc.arrNpcTemplate = new NpcTemplate[msg.reader().readByte()];
//
//					for (byte i = 0; i < Npc.arrNpcTemplate.length; i++) {
//						Npc.arrNpcTemplate[i] = new NpcTemplate();
//						Npc.arrNpcTemplate[i].npcTemplateId = i;
//						Npc.arrNpcTemplate[i].name = msg.reader().readUTF();
//						Npc.arrNpcTemplate[i].headId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].bodyId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].legId = msg.reader().readShort();
//						Npc.arrNpcTemplate[i].contendChat = msg.reader().readUTF();
////						Npc.arrNpcTemplate[i].menu = new String[msg.reader().readByte()][];
////						for (int j = 0; j < Npc.arrNpcTemplate[i].menu.length; j++) {
////							Npc.arrNpcTemplate[i].menu[j] = new String[msg.reader().readByte()];
////							for (int j2 = 0; j2 < Npc.arrNpcTemplate[i].menu[j].length; j2++)
////								Npc.arrNpcTemplate[i].menu[j][j2] = msg.reader().readUTF();
////						}
//					}
				
					int sizeItemMap = msg.reader().readShort();
					Cout.println("sizeItemMappppppp  "+sizeItemMap);
					for (int i = 0; i < sizeItemMap; i++) {
						int iditemm=msg.reader().readInt() ;
						short idtemitem=msg.reader().readShort() ;
						int xitem = msg.reader().readShort();
						int yitem = msg.reader().readShort();
						ItemMap itemDropmap = new ItemMap(iditemm, idtemitem, xitem, yitem);
						//					
						GameScreen.vItemMap.addElement(itemDropmap);
						GameScreen.vNhatItemMap.addElement(itemDropmap);
						
					}
					
//					GameScreen.gI().loadGameScr();
					Service.gI().requestinventory();
					if(GameCanvas.currentScreen != GameCanvas.gameScreen){
						GameCanvas.gameScreen =  new GameScreen();
//						GameCanvas.gameScreen.switchToMe();// mo man hình gamescr
					}

					Cout.println("CHANGE_MAPPPPPPPPPPP download ");
					DownloadImageScreen.gI().switchToMe(SmallImage.ID_ADD_MAPOJECT, idObjMap);
					//WaitingScreen.gI().switchToMe();
					Char.myChar().ischangingMap = false;
					Char.myChar().statusMe = Char.A_FALL;
					GameScreen.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
					GameCanvas.endDlg();

					Music.play(CRes.random(1, 4), 0.3f);
				break;
			case Cmd.ITEM:
				try{

					GameScreen.currentCharViewInfo = Char.myChar();
					byte type = msg.reader().readByte();
					if(type == ItemTemplate.ITEM_TEMPLATE){
						short nItemTemplate = msg.reader().readShort(); // số template ID
						Cout.println("  nItemTemplate  "+nItemTemplate);
						for(int i = 0; i < nItemTemplate; i++){
							short templateID = msg.reader().readShort(); // id temp
							byte typeItem = msg.reader().readByte(); // loai
							byte gender = msg.reader().readByte(); //  gioi tinh
							String name = msg.reader().readUTF(); // ten 
							String des = msg.reader().readUTF(); // mo ta
							byte level = msg.reader().readByte(); // level
							short iconID = msg.reader().readShort(); // idIcon
							boolean isUptoUp = msg.reader().readBoolean(); // có nâng cấp đc k 
							long giaitem = msg.reader().readLong();
							byte clazz = msg.reader().readByte();
							byte quocgia = msg.reader().readByte();
							short part = msg.reader().readShort(); // part cơ bản chắc chắn thằng nào cũng phải có
							short partquan = -1,partdau=-1;
//							ItemTemplate itt = new ItemTemplate(msg.reader().readShort(), msg.reader().readByte(),  msg.reader().readByte(),  msg.reader().readUTF(),  msg.reader().readUTF(),  msg.reader().readByte(),  msg.reader().readShort(),  msg.reader().readShort(),  msg.reader().readBoolean());
							if(typeItem == Item.TYPE_AO){ // áo đọc thêm 1 part nữa là part quần
								partquan = msg.reader().readShort(); 
								partdau = msg.reader().readShort(); 
								
							}
							byte typeSell = msg.reader().readByte();
							ItemTemplate itt = null;
							if(typeItem != Item.TYPE_AO)
								itt = new ItemTemplate(templateID, typeItem,  gender,  name,  des,  level,  iconID,  part,  isUptoUp);
							else{
								itt = new ItemTemplate(templateID, typeItem,  gender,  name,  des,  level,  iconID,  part, partquan,partdau,  isUptoUp);
							}
							itt.despaint = mFont.tahoma_7_white.splitFontArray(des, 140);
							itt.gia = giaitem;
							itt.clazz = clazz;
							itt.quocgia = quocgia;
							itt.typeSell = typeSell;
							ItemTemplates.add(itt);
						}
					}else if(type == ItemTemplate.CHAR_WEARING){
						short CharID = msg.reader().readShort();
						byte leng = msg.reader().readByte();
						if(Char.myChar().charID != CharID){
							boolean isAdded = false;
							for (int i = 0; i < GameScreen.vCharInMap.size(); i++) {
								Char dem = (Char) GameScreen.vCharInMap.elementAt(i);
								if(dem!=null&&dem.charID==CharID){
									isAdded = true;
									c = dem;
									//Cout.println("LOIIIIIIIIIIIIIIIIIIIII ADD THEM CHAR");
								}
							}
							if(!isAdded){
								c = new Char();
								c.charID = CharID;
							}
							readcharInmap(c,msg);
							if(!isAdded)
							GameScreen.vCharInMap.addElement(c);
							//Cout.println("  char.Id CHAR_WEARING add new char"+GameScreen.vCharInMap.size());
						}else{
							Cout.println2222("CHAR_WEARING leng ----> "+leng);
							
							Char.myChar().arrItemBody = new Item[10];
							try {
								for (int i = 0; i < Char.myChar().arrItemBody.length; i++) {
									short idtemplate = msg.reader().readShort();
										if(idtemplate != -1){
										//	Cout.println(" KHAC -1 ----->");
											ItemTemplate template = ItemTemplates.get(idtemplate);
				
											int indexUI = Item.getPosWearingItem(template.type);
											

											//Cout.println("CHAR_WEARING idtemplate ----> "+idtemplate);
											
//											GameScreen.currentCharViewInfo.arrItemBody[indexUI] = new Item();
//										
//											GameScreen.currentCharViewInfo.arrItemBody[indexUI].indexUI = indexUI;
//											
//											GameScreen.currentCharViewInfo.arrItemBody[indexUI].typeUI = Item.UI_BODY;
//											GameScreen.currentCharViewInfo.arrItemBody[indexUI].template = template;
//											GameScreen.currentCharViewInfo.arrItemBody[indexUI].isLock = true;
											if(indexUI>=Char.myChar().arrItemBody.length)
											{
												Cout.println2222(Char.myChar().arrItemBody.length+"  indexUi   "+indexUI);
												continue;
											}
											Char.myChar().arrItemBody[indexUI] = new Item();
											
											Char.myChar().arrItemBody[indexUI].indexUI = indexUI;
											
											Char.myChar().arrItemBody[indexUI].typeUI = Item.UI_BODY;
											Char.myChar().arrItemBody[indexUI].template = template;

											Cout.println("CHAR_WEARING name ----> "+Char.myChar().arrItemBody[indexUI].template.name);
											Char.myChar().arrItemBody[indexUI].isLock = true;
			//								GameScreen.currentCharViewInfo.arrItemBody[indexUI].upgrade = msg.reader().readByte();
			//								GameScreen.currentCharViewInfo.arrItemBody[indexUI].sys = msg.reader().readByte();

//											Char.myChar().leg = GameScreen.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
											if (template.type == Item.TYPE_AO){
//												Char.myChar().leg = GameScreen.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
												Char.myChar().body = GameScreen.currentCharViewInfo.arrItemBody[indexUI].template.part;
												Char.myChar().leg = GameScreen.currentCharViewInfo.arrItemBody[indexUI].template.partquan;
												Char.myChar().head = GameScreen.currentCharViewInfo.arrItemBody[indexUI].template.partdau;
												Cout.println(Char.myChar().leg+"  head ----> "+Char.myChar().head+"  body  "+Char.myChar().body);
												
											}
//											else if (template.type == Item.TYPE_LEG){
//												
//												Char.myChar().leg = GameScreen.currentCharViewInfo.arrItemBody[indexUI].template.part;
//												Cout.println("LEG ----> "+Char.myChar().leg);
//
//											}
											else if (template.type == Item.TYPE_NON){
												Char.myChar().head = GameScreen.currentCharViewInfo.arrItemBody[indexUI].template.part;
											}
									
									}
									
								}
//							Char.myChar().head = charWearing[0];
								
								
							} catch (Exception e) {
								e.printStackTrace();
							}
						
					}
//						Char.myChar().charID = msg.reader().readShort();
//						byte lengtharrbody = msg.reader().readByte();
//						short charWearing[] = new short[lengtharrbody];
//						for(int i = 0; i < lengtharrbody; i++ ){
//							short idItemTemplate = msg.reader().readShort();
//							if(idItemTemplate != -1){
//								charWearing[i] = msg.reader().readShort();
//							}
//								
//						}
//						Char.myChar().head = charWearing[0];
//						Char.myChar().body = charWearing[4];
//						Char.myChar().leg = charWearing[11];
						         
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				                        
				
				break;
			case Cmd.CHAR_INFO:
//				Cout.println("Char_infoooooooooooooooooooooooooooooooooooo");
				Char.myChar().charID = msg.reader().readShort();
//				Cout.println("Char_infooooooo  "+Char.myChar().charID);
				Char.myChar().cName = msg.reader().readUTF();
				Char.myChar().cClass = msg.reader().readByte();
				Cout.println("Char_infooooooo  cClass "+Char.myChar().cClass);
				Char.myChar().cgender = msg.reader().readByte();
				short clevel2 = msg.reader().readShort();
				if(Char.myChar().clevel>0&&Char.myChar().clevel<clevel2){
					Music.play(Music.LENLV, 10);
					ServerEffect.addServerEffect(22,Char.myChar().cx,Char.myChar().cy,1);
				}
				Char.myChar().clevel = clevel2;
				Char.myChar().cEXP = msg.reader().readByte();
//				m.dos.writeByte(p.lvDetail.getPercent()); 
				Char.myChar().cHP = msg.reader().readInt();

				Cout.println(Char.myChar().cName+"  CHAR_INFO Char.myChar().cHP "+Char.myChar().cHP);
//				Cout.println("Char_infooooo  "+Char.myChar().cHP);
				Char.myChar().cMaxHP = msg.reader().readInt();

//				Cout.println(Char.myChar().cHP+"Char_infoo maxhp"+Char.myChar().cMaxHP );
				Char.myChar().cMP = msg.reader().readInt();
				Cout.println(Char.myChar().cHP+"Char.myChar().cMP" +Char.myChar().cMP );
				Char.myChar().cMaxMP = msg.reader().readInt();
//				Cout.println(Char.myChar().cMP+"Char_infoo maxhp"+Char.myChar().cMaxMP);
				//Char.myChar().totalTN = msg.reader().readInt();
				Char.myChar().diemTN = null;
				Char.myChar().diemTN = new int[8];
				Char.myChar().diemTN[0] = msg.reader().readInt();
				Char.myChar().diemTN[1]  = msg.reader().readInt();
				Char.myChar().diemTN[2]  = msg.reader().readInt();
				Char.myChar().diemTN[3]  = msg.reader().readInt();
				Char.myChar().diemTN[4]  = msg.reader().readInt();
				Char.myChar().diemTN[5]  = msg.reader().readInt();
				Char.myChar().diemTN[6]  = msg.reader().readInt();
				Char.myChar().diemTN[7]  = msg.reader().readInt();
				Char.myChar().totalTN =  msg.reader().readInt();
//				Cout.println(" Char.myChar().totalTN "+Char.myChar().totalTN);
				
				//com.sakura.thelastlegend.gui ve thong tin sub
				short sizeSub = msg.reader().readShort();
				Char.myChar().subTn = new int[sizeSub];
//				Cout.println(" sizeSub "+sizeSub);
				for (int i = 0; i < sizeSub; i++) {
					byte indexMon = msg.reader().readByte();
					int valueSub = msg.reader().readInt();
//					Cout.println(indexMon+" sizeSub "+valueSub);
					if(indexMon<Char.myChar().subTn.length)
						Char.myChar().subTn[indexMon] = valueSub;
				}
				if(GameScreen.findCharInMap((short)Char.myChar().charID)==null)
					GameScreen.vCharInMap.addElement(Char.myChar());
				break;
			case Cmd.CHAR_LIST:
				//Cout.println(getClass(), "  nhan ve charlisst "+Cmd.CHAR_LIST);
				byte sizeChar = msg.reader().readByte();
				//Cout.println(getClass(),sizeChar+ "  nhan ve charlisst "+Cmd.CHAR_LIST);
				LoginScreen.isLoggingIn = false;
//				LoginScreen.selectCharScr = new SelectCharScreen();
				SelectCharScreen.gI().initSelectChar();
//				LoginScreen.selectCharScr.switchToMe();
				GameCanvas.endDlg();
				if(sizeChar > 0){
//					LoginScreen.selectCharScr = new SelectCharScreen();
					// dùng biến riêng màn hình login 
//					//Char[] c = new Char[sizeChar];
					
					for(int i = 0; i < sizeChar; i++){
						
//						c[i] = new Char();
//						c[i].charIdDB = msg.reader().readInt();
						SelectCharScreen.gI().charIDDB[i] = msg.reader().readInt();
						//Cout.println("id----> "+SelectCharScreen.gI().charIDDB[i]);
						SelectCharScreen.gI().name[i] = msg.reader().readUTF();
						Cout.println("name----> "+ SelectCharScreen.gI().name[i]);
						SelectCharScreen.gI().gender[i] = msg.reader().readByte();
						//Cout.println("gender----> "+SelectCharScreen.gI().gender[i]);
						SelectCharScreen.gI().type[i] = msg.reader().readByte();
						Cout.println("SelectCharScreen.gI().type[i] ----> "+ SelectCharScreen.gI().type[i]);
						SelectCharScreen.gI().lv[i] = msg.reader().readShort();
						//Cout.println("SelectCharScreen.gI().lv[i] ----> "+SelectCharScreen.gI().lv[i]);
						byte sizePart = msg.reader().readByte();
						Cout.println("sizePart ----> "+sizePart);
						SelectCharScreen.gI().parthead[i] = msg.reader().readShort();
						Cout.println("PARTHEAD ----> "+ SelectCharScreen.gI().parthead[i]);

						
						SelectCharScreen.gI().partbody[i] = msg.reader().readShort();
						Cout.println("PARTBODY ---> "+ SelectCharScreen.gI().partbody[i]);
						
						//System.out.println("Headpart "+msg.reader().readShort());
						SelectCharScreen.gI().partleg[i] = msg.reader().readShort();
						Cout.println("PARTLEG ----> "+ SelectCharScreen.gI().partleg[i]);
						
//						if (SelectCharScreen.gI().partWp[i] == -1)
//							SelectCharScreen.gI().partWp[i] = 15;
//						if (SelectCharScreen.gI().partbody[i] == -1) {
//							if (SelectCharScreen.gI().gender[i] == 0)
//								SelectCharScreen.gI().partbody[i] = 10;
//							else
//								SelectCharScreen.gI().partbody[i] = 1;
//						}
//						if (SelectCharScreen.gI().partleg[i] == -1) {
//							if (SelectCharScreen.gI().gender[i] == 0)
//								SelectCharScreen.gI().partleg[i] = 9;
//							else
//								SelectCharScreen.gI().partleg[i] = 0;
//						}
//						SelectCharScreen.gI().partWp[i] = msg.reader().readShort();
//						System.out.println("size part --> "+sizePart);
////						for(int j = 0; j < sizePart; j++){
//							c[i].leg = msg.reader().readShort();
//							c[i].body = msg.reader().readShort();
//							c[i].head = msg.reader().readShort();
//							c[i].wp = msg.reader().readShort();
////						}
					}
					DownloadImageScreen.gI().switchToMe();
					GameCanvas.endDlg();
				}else{
					DownloadImageScreen.gI().switchToMe(1);
					GameCanvas.endDlg();
				}
				
					
				break;
			case Cmd.LOGIN:
//				byte typeLogin = msg.reader().readByte();
//				Cout.println2222("zzzzzzzzzzzzzzzzzzzzzzzz  typeLogin "+typeLogin);
//				if(typeLogin==-1)
//				{
//					String link = msg.reader().readUTF();
//					Cout.println2222("zzzzzzzzzzzzzzzzzzzzzzzz");
//					return;
//				}else if(typeLogin==1)
//				{
//					GameCanvas.startOKDlg("Sai thông tin đăng nhập. Vui lòng thử lại");
//					return;
//				}
//				short lengtdata = msg.reader().readShort();
//				System.out.println("LENGTDATA ---> "+lengtdata);
//				short sizebyte = msg.reader().readShort();
				createData(msg.reader());
//				createDataEff(msg.reader());
//				createDataImage(msg.reader());
//				createDataPart(msg.reader());
//				createDataKill(msg.reader());

				// SAVE RMS NEW DATA
				msg.reader().reset();
				byte[] newData = new byte[msg.reader().available()];
				msg.reader().readFully(newData);
				// SAVE RMS NEW DATA VERSION
//				byte[] newDataVersion = new byte[] { GameScreen.vcData };
//				Rms.saveRMS("dataVersion", newDataVersion);
//				 =========================
//				LoginScreen.isUpdateData = false;
//				if (GameScreen.vsData == GameScreen.vcData
//						&& GameScreen.vsMap == GameScreen.vcMap
//						&& GameScreen.vsSkill == GameScreen.vcSkill
//						&& GameScreen.vsItem == GameScreen.vcItem) {
//					System.out.println(GameScreen.vsData + "," + GameScreen.vsMap
//							+ "," + GameScreen.vsSkill + "," + GameScreen.vsItem);
					//GameScreen.gI().readDart();
					GameScreen.readArrow();
					GameScreen.readEfect();// doc ef
					SmallImage.gI().readImage();
					GameScreen.readPart();
					GameScreen.gI().readSkill();
//					Service.gI().clientOk();
//					return;
//				}
				break;
			case Cmd.DIALOG:
				byte typeDialog = msg.reader().readByte();
				Cout.println("typeDialog  "+typeDialog);
				if(typeDialog == 0){
					//  ok
					GameCanvas.startOKDlg( msg.reader().readUTF());
				}
				else if(typeDialog == 1){
					int idDialog = msg.reader().readShort();
					byte subCmd = msg.reader().readByte();
					int iduser = msg.reader().readShort();
					String noidung = ""+idDialog+","+subCmd+","+iduser;
					GameCanvas.startYesNoDlg(msg.reader().readUTF(),new Command("Yes", GameScreen.gI(), 1000, noidung),
							new Command("No", GameScreen.gI(), 1001, noidung));
				}else if(typeDialog == 2){
					GameCanvas.startDlgTime(msg.reader().readUTF(),new Command("Yes", GameCanvas.instance, 8882, null),
							msg.reader().readInt());
				}else if(typeDialog == 3){//tren dau
					GameCanvas.StartDglThongBao( msg.reader().readUTF());
				}
				else if(typeDialog == 4){//ben goc phai
					GameCanvas.StartDglThongBao( msg.reader().readUTF());
				}
				break;
			case Cmd.NPC_TEAMPLATE:
				int leng = msg.reader().readByte();

				for (byte i = 0; i < leng; i++) {
					NpcTemplate npctem = new NpcTemplate();
					npctem.npcTemplateId = msg.reader().readShort();
					npctem.name = msg.reader().readUTF();
					npctem.headId = msg.reader().readShort();
					npctem.bodyId = msg.reader().readShort();
					npctem.legId = msg.reader().readShort();
					npctem.contendChat = msg.reader().readUTF();
					if(npctem.npcTemplateId!=Npc.idXaPhu&&npctem.headId==-1&&npctem.bodyId==-1&&npctem.legId==-1)
						npctem.typeKhu = -1;
					npctem.idavatar = msg.reader().readShort();//avatar
					Npc.arrNpcTemplate.put(npctem.npcTemplateId+"",npctem);
					///*Npc.arrNpcTemplate[i]. = */msg.reader().readShort();
//					Npc.arrNpcTemplate[i].menu = new String[msg.reader().readByte()][];
//					for (int j = 0; j < Npc.arrNpcTemplate[i].menu.length; j++) {
//						Npc.arrNpcTemplate[i].menu[j] = new String[msg.reader().readByte()];
//						for (int j2 = 0; j2 < Npc.arrNpcTemplate[i].menu[j].length; j2++)
//							Npc.arrNpcTemplate[i].menu[j][j2] = msg.reader().readUTF();
//					}
				}
			
				break;
			case Cmd.MAP_TEAMPLATE:
				int sizee = msg.reader().readShort();
				for (int i = 0; i < sizee; i++) {
					short idmap = msg.reader().readShort();
					short idtile = msg.reader().readShort();
					String name = msg.reader().readUTF();
					TileMap.listNameAllMap.put(idmap+"", name);
				}
				break;
			case Cmd.CHAR_SKILL_STUDIED:
				short dsSkillDaHoc = msg.reader().readShort();
				Cout.println("CHAR_SKILL_STUDIED size  "+dsSkillDaHoc);
				for (int i = 0; i < dsSkillDaHoc; i++) {
					byte idskill = msg.reader().readByte();
					if(i==0)
						QuickSlot.idSkillCoBan = idskill;
					short idtem = msg.reader().readShort();//icon skill tron
					short levelskill = msg.reader().readShort();

					if(TabSkill.skillIndex!=null&&idskill==TabSkill.skillIndex.id){
						if(TabSkill.skillIndex.level==-1&&levelskill>-1){
							TabSkill.skillIndex.level = levelskill;
							TabSkill.isDaHoc = true;
						}
						TabSkill.skillIndex.level = levelskill;
					}
					SkillTemplate skill = (SkillTemplate)SkillTemplates.hSkilltemplate.get(""+idskill);
					
					skill.level = (short)(levelskill);
					if(skill.level>=skill.nlevelSkill)
						skill.level = (short)(skill.nlevelSkill-1);
					
				}

				QuickSlot.loadRmsQuickSlot();
				break;
			case Cmd.UPDATE_CHAR: //UPDATE_BLOOD = 0;//UPDATE_EXP = 1;//su dung tang exp/// UPDATE_HP = 2; //UPDATE_MP = 3;
				//Cout.println("update charrr ");
				short idchar = msg.reader().readShort();
				byte type = msg.reader().readByte();
				//Cout.println(type+" update charrr  "+idchar);
				Char ccharupdate = GameScreen.findCharInMap(idchar);
				if(ccharupdate ==null) return;
				switch (type) {
				case 0:
					byte sizeThucAn =msg.reader().readByte();
					GuiMain.vecItemOther.removeAllElements();
					for (int i = 0; i < sizeThucAn; i++) {
						short idtemplate = msg.reader().readShort();
						short time = msg.reader().readShort();
						//Cout.println("time  "+time);
						GuiMain.vecItemOther.add(new ItemThucAn((short)i,
								(byte)0,idtemplate,time, 100));
					}
//					msg.dos.writeByte(p.getListItemsBuff().size());
//                    for (int i = 0; i < p.getListItemsBuff().size(); i++) {
//                        //id template
//                        msg.dos.writeShort(p.getListItemsBuff().get(i).idTemplate);
//                        //thời gian buff . đơn vị tính bằng giay
//                        msg.dos.writeShort((int) (p.getListItemsBuff().get(i).getTimeBuff() / Actor.SECOND));
//                    }
					break;
				case 1:
					Char.myChar().cEXP =msg.reader().readByte();
					Char.myChar().totalTN =msg.reader().readInt();
					break;
				case 2: //hp
					ccharupdate.cMaxHP = msg.reader().readInt(); //maxhp
					ccharupdate.cHP = msg.reader().readInt(); //hp
					int hpcong= msg.reader().readInt(); //hp cong them
					GameScreen.startFlyText("+" +hpcong, ccharupdate.cx, ccharupdate.cy-50, 0, -2, mFont.RED);
					break;
				case 3: //hp
					ccharupdate.cMaxMP = msg.reader().readInt(); //maxhp
					ccharupdate.cMP = msg.reader().readInt(); //hp
					int mpcong =  msg.reader().readInt(); //hp cong them
						GameScreen.startFlyText("+" +mpcong, ccharupdate.cx, ccharupdate.cy-50, 0, -2, mFont.GREEN);
					break;
				}
				break;
			case Cmd.COME_HOME_DIE:
				byte typehs = msg.reader().readByte();
				switch (typehs) {
					case 0://ve lang
						Char.myChar().statusMe = Char.A_FALL;
						Char.myChar().cHP = msg.reader().readShort();
						Char.myChar().cMP = msg.reader().readShort();
						break;
	
					case 1://hs tai cho
						short idcharhs = msg.reader().readShort();
						Char chs = GameScreen.findCharInMap(idcharhs);
	
						if(chs!=null){
							ServerEffect.addServerEffect(34, chs.cx, chs.cy, 3);
							chs.statusMe = Char.A_FALL;
							chs.cHP = msg.reader().readShort();
							chs.cMP = msg.reader().readShort();
						}
						break;
				}
				break;
			case Cmd.REGISTER:
				boolean isDK = msg.reader().readBoolean();
				String text = msg.reader().readUTF();
				GameCanvas.startOK(text, 8882, null);
				break;
			case Cmd.XP_CHAR:
				short idc = msg.reader().readShort();
	            int xp = msg.reader().readInt();//m.dos.writeInt((int) dxp);
	            Char acc = GameScreen.findCharInMap(idc);
	            if(acc!=null){
	            	GameScreen.startFlyText("+" +xp+"exp",
	            			acc.cx, acc.cy-acc.ch-5, 0, -2, mFont.YELLOW);

	            }
				break;
			case Cmd.BUY_ITEM_FROM_SHOP:
				Cout.println("nhan ve BUY_ITEM_FROM_SHOP ");
				break;
			case Cmd.REQUEST_REGION:
				ThongTinKhu(msg);
				break;
			case Cmd.CHANGE_REGION:
				ChangeKhu(msg);
				break;
			case Cmd.COMPETED_ATTACK:
				ThachDau(msg);
				break;
			case Cmd.STATUS_ATTACK:
				StatusAttack(msg);
				break;
			case Cmd.SERVER_CHAT:
				byte type_info_server = msg.reader().readByte();
		 		String text_info_server = msg.reader().readUTF();
		 		InfoServer info_server = new InfoServer(InfoServer.CHATSERVER,text_info_server);
		 		GameScreen.listInfoServer.add(info_server);
//		 		Cout.println2222(GameCanvas.gameTick+" nhan ve chat server "+GameScreen.listInfoServer.size());
				//GameCanvas.StartDglThongBao(m);
				break;
			case Cmd.ERROR_VERSION:
//				byte typeLoginerror = msg.reader().readByte();
				Cout.println2222("ERROR_VERSION  typeLogin ");
				
//				Cout.println2222("zzzzzzzzzzzzzzzzzzzzzzzz  typeLogin "+typeLoginerror);
//				if(typeLoginerror==-1)
//				{
//					String link = msg.reader().readUTF();
//					Cout.println2222("zzzzzzzzzzzzzzzzzzzzzzzz");
//					return;
//				}else if(typeLoginerror==1)
//				{
//					GameCanvas.startOKDlg("Sai thông tin đăng nhập. Vui lòng thử lại");
//					return;
//				}
				String link = msg.reader().readUTF();
				GameCanvas.startOK("Bạn đang dùng phiên bản cũ. Vui lòng tải phiên bản mới !",GameCanvas.cTaiVersionMoi, link);
				break;
			case Cmd.DAPDO:
				byte type_dapdo = msg.reader().readByte();
				switch (type_dapdo) {
					case TabNangCap.SUB_UPDATE_INFO_UPGRADE:
						int leng_item = msg.reader().readByte();
						short[] listiditem = new short[leng_item];
						for (int i = 0; i < leng_item; i++) {
							listiditem[i] = msg.reader().readShort();
						}
						TabNangCap tab_nc = (TabNangCap)GameCanvas.AllInfo.VecTabScreen.get(4);
						if(tab_nc!=null)
						{
							tab_nc.updateListItemNangCap(listiditem);
						}
						break;
					case TabNangCap.SUB_UPGRADE_FAIL:
					case TabNangCap.SUB_UPGRADE_SUCCESS:
						short idEff_dap = msg.reader().readShort();
						ServerEffect.addServerEffect(idEff_dap, GameCanvas.w/2,GameCanvas.h/2,false);
						break;
				
				}
				break;
			case Cmd.CLAN:
				byte typeClan = msg.reader().readByte();
				switch (typeClan) {
					case ClanScreen.REQUETS_NAME:
						// nhap ten clan
						GameCanvas.startDlgTField("Tạo Bang", null, new Command("Tạo", GameCanvas.instance, GameCanvas.cTaoClan, null), null);
						return;
					case ClanScreen.GET_LIST_LOCAL:
						short idhost = msg.reader().readShort();
						int size_User = msg.reader().readByte();
						ClanScreen.gI().isBoss = false;
						ClanScreen.gI().listUser_local.removeAllElements();
						if(idhost==Char.myChar().charID)
							ClanScreen.gI().isBoss = true;
						for (int i = 0; i < size_User; i++) {
							short idClanUser = msg.reader().readShort();
							String nameClanUser = msg.reader().readUTF();
							short lvClanUser = msg.reader().readShort();
							boolean isHost = false;
							if(idhost==idClanUser)
								isHost = true;
							OtherChar otherClan = new OtherChar(idClanUser, nameClanUser,lvClanUser,isHost);
							ClanScreen.gI().listUser_local.add(otherClan);
						}
						break;
					case ClanScreen.GET_LIST_GOBAL:
						ClanScreen.gI().isBossGobal = false;
						short idhost_gobal = msg.reader().readShort();
						if(idhost_gobal==Char.myChar().charID)
							ClanScreen.gI().isBossGobal = true;
						int size_User_gobal = msg.reader().readByte();
						ClanScreen.gI().listUser_gobal.removeAllElements();
						for (int i = 0; i < size_User_gobal; i++) {
							short idClanUser = msg.reader().readShort();
							String nameClanUser = msg.reader().readUTF();
							short lvClanUser = msg.reader().readShort();
							boolean isHost = false;
							if(idClanUser==idhost_gobal)
								isHost = true;
							OtherChar otherClan = new OtherChar(idClanUser, nameClanUser,lvClanUser,isHost);
							ClanScreen.gI().listUser_gobal.add(otherClan);
						}
						break;
				
				}
				break;
			case Cmd.PAYMENT_GET:
				boolean isSuccess = msg.reader().readBoolean();
				if(isSuccess)
					GameCanvas.StartDglThongBao("Bạn đã nạp thẻ thành công !!!");
				else GameCanvas.StartDglThongBao("Bạn nạp không thành công, vui lòng kiểm tra lại !");
				 
				break;
			case Cmd.PAYMENT_INFO:
				String json = msg.reader().readUTF();
				TabBag.list_item = JSONParser.parseItemPayment(json);
				break;
			case Cmd.PAYMENT:

				 TabBag.isShowPayment = true;
				break;

			case Cmd.BOSS_APPEAR:
				boolean isturnOnBoss = msg.reader().readBoolean();
				Cout.println2222("isturnOnBoss   "+isturnOnBoss);
				Mob.isBossAppear = isturnOnBoss;
                short appear = msg.reader().readShort();
                if(Mob.isBossAppear==true)
				{
					for (int i = 0; i < GameScreen.vMob.size(); i++) {
						Mob dem_mob = (Mob) GameScreen.vMob.elementAt(i);
						if(dem_mob!=null&&dem_mob.isBoss)
						{
							Cout.println2222("isturnOnBoss songlai  "+isturnOnBoss);
							dem_mob.injureThenDie = false;
							dem_mob.status = Mob.MA_WALK;
							dem_mob.x = dem_mob.xFirst;
							dem_mob.y = dem_mob.yFirst;
							ServerEffect.addServerEffect(37, dem_mob.x, dem_mob.y-dem_mob.getH()/4-10, 1);
							dem_mob.hp = dem_mob.maxHp;
							break;
						}
							
					}
				}
				break;
			}
		}catch(Exception e){
			System.out.println("Error AT ----> "+msg.command);
			e.printStackTrace();
		}
		finally{
			if(msg != null)
				msg.cleanup();
		}
		
	}
	public void ThongTinKhu(Message msg){
		try {

			Cout.println("ThongTinKhu");
			byte nkhu = msg.reader().readByte();
			Cout.println("ThongTinKhu  "+nkhu);
			KhuScreen.gI().listKhu = new byte[nkhu][1];
			for (int i = 0; i < nkhu; i++) {
				KhuScreen.gI().listKhu[i][0] = msg.reader().readByte();
			}

			Cout.println("ThongTinKhu KhuScreen ");
			KhuScreen.gI().srclist.selectedItem = -1;
			KhuScreen.gI().switchToMe();
		} catch (Exception e) {
			// TODO: handle exception
			Cout.println("ThongTinKhu KhuScreen e"+e.toString());
		}
	}
	public void ThachDau(Message msg){
		try {
			byte type = msg.reader().readByte();
			short idchar = msg.reader().readShort();
			String loimoi = msg.reader().readUTF();
			GameCanvas.startYesNoDlg(loimoi, new Command("Đồng ý", GameCanvas.instance, GameCanvas.cDongYThachDau, idchar+""), 
					new Command("Không", GameCanvas.instance, 8882, null));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void StatusAttack(Message msg){
		try {
			short idchar = msg.reader().readShort();
			byte typeAttack = msg.reader().readByte();
			if(Char.myChar().charID==idchar){
				FlagScreen.time = mSystem.currentTimeMillis();
			}
			Char ch = GameScreen.findCharInMap(idchar);
			if(ch!=null) ch.typePk = typeAttack;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void ChangeKhu(Message msg){
		try {
			TileMap.zoneID = msg.reader().readByte();
			Char.myChar().cx = msg.reader().readShort();
			Char.myChar().cy = msg.reader().readShort();

//			GameScreen.vMob.removeAllElements();
			for (int i = 0; i < GameScreen.vMob.size(); i++) {
				Mob dem = (Mob) GameScreen.vMob.elementAt(i);
				dem.ResetToDie();
			}
			GameScreen.vItemMap.removeAllElements();
			GameScreen.vCharInMap.removeAllElements();
			Cout.println(TileMap.vCurrItem.size()+"  ChangeKhu size npc =="+ GameScreen.vNpc.size());
			WaitingScreen.isChangeKhu = true;
			WaitingScreen.gI().switchToMe();
			GameScreen.vCharInMap.add(Char.myChar());
			Char.myChar().ischangingMap = false;
			Char.myChar().statusMe = Char.A_FALL;
			GameScreen.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
			GameCanvas.endDlg();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
		private void createData(DataInputStream d) {
			try{
				Rms.saveRMS("nj_arrow", Util.readByteArray(d));
				Rms.saveRMS("nj_effect", Util.readByteArray(d));
				Rms.saveRMS("nj_image", Util.readByteArray(d));
				Rms.saveRMS("nj_part", Util.readByteArray(d));
				Rms.saveRMS("nj_skill", Util.readByteArray(d));
			}catch(Exception e){
				e.printStackTrace();
			}
//		GameScreen.vcData = d.readByte();
		
		}
		
		public void readcharInmap(Char c, Message msg){ // add player trong map
			try{
			
//				c.charID = msg.reader().readShort();
//				byte lengtharrbody = msg.reader().readByte();
				c.arrItemBody = new Item[13];
				
					for (int i = 0; i < c.arrItemBody.length; i++) {
						short idtemlate = msg.reader().readShort();
							if(idtemlate != -1){
								ItemTemplate template = ItemTemplates.get(idtemlate);
								int indexUI = Item.getPosWearingItem(template.type);
								
								
								c.arrItemBody[indexUI] = new Item();
							
								c.arrItemBody[indexUI].indexUI = indexUI;
								
								c.arrItemBody[indexUI].typeUI = Item.UI_BODY;
								c.arrItemBody[indexUI].template = template;
								c.arrItemBody[indexUI].isLock = true;
								
//								GameScreen.currentCharViewInfo.arrItemBody[indexUI].upgrade = msg.reader().readByte();
//								GameScreen.currentCharViewInfo.arrItemBody[indexUI].sys = msg.reader().readByte();
							
								if (template.type == Item.TYPE_AO){
									
									c.body = c.arrItemBody[indexUI].template.part;
									c.leg = c.arrItemBody[indexUI].template.partquan;
									c.head = c.arrItemBody[indexUI].template.partdau;
								}
//								else if (template.type == Item.TYPE_LEG){
//									c.leg = c.arrItemBody[indexUI].template.part;
//								}
								else if (template.type == Item.TYPE_NON){
									c.head = c.arrItemBody[indexUI].template.part;
								}
						
						}
						
					}
			
				//return true;
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			//return false;
		}
		
		private mBitmap createImage(byte[] arr) {
			try {
				return Image.createImage(arr, 0, arr.length);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}
		
		private void createItem(DataInputStream d) throws Exception {
			GameScreen.vcItem = d.readByte();
			GameScreen.iOptionTemplates = new ItemOptionTemplate[d.readByte()];
			
			for (int i = 0; i < GameScreen.iOptionTemplates.length; i++) {
				GameScreen.iOptionTemplates[i] = new ItemOptionTemplate();
				GameScreen.iOptionTemplates[i].id = i;
				GameScreen.iOptionTemplates[i].name = d.readUTF();
				GameScreen.iOptionTemplates[i].type = d.readByte();
			}
			int nItemTemplate = d.readShort();
			for (int i = 0; i < nItemTemplate; i++) {
				ItemTemplate itt = new ItemTemplate((short) i, d.readByte(), d.readByte(), d.readUTF(), d.readUTF(), d.readByte(), d.readShort(), d.readShort(), d.readBoolean());
				//System.out.println(itt.id+", "+itt.name);
				ItemTemplates.add(itt);
			}

		}
		
	
	
}

