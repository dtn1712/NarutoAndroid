package com.sakura.thelastlegend.gui;


import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import screen.GameScreen;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Sprite;

public class GuiChatClanWorld implements IActionListener {
	
	public static final byte CHAT_WORLD=0;
	public static final byte CHAT_CLAN=1;
	
	FatherChat []fatherChat;
	String[] listNameTab ;
	
	int x,y;
	int selectTab;
	int nTab;
	int distanceTab=5;//distance between 2 tab
	int widthTab=30;
	int heightTab=25;
	Command bntOpen;
	int yButtonTab=GameCanvas.h-50;//y of button tab
	public boolean moveClose,moveOpen;
	int v;//gia toc tang toc do move
	public static int xMove;
	int limMove=FatherChat.popw-10;
	
	int xBtnMove=FatherChat.popw+10;
	
	public GuiChatClanWorld(int x,int y, String[]arrayName) {
		
		this.x=x;
		this.y=y;
		this.nTab=arrayName.length;
		this.listNameTab=arrayName;
		fatherChat= new FatherChat[arrayName.length];
		fatherChat[0]=new GuiChatWorld(this.x,this.y);
		fatherChat[1]=new GuiChatClan(this.x,this.y);
		
		bntOpen= new Command("", this, Constants.BUTTON_OPEN, null, 0, 0);
		bntOpen.setPos(0, y+130/2-Image.getHeight(LoadImageInterface.imgShortQuest)/2 +25, LoadImageInterface.imgShortQuest, LoadImageInterface.imgShortQuest);
	}
	
	public void ActionPerformSend()
	{
		fatherChat[selectTab].ActionPerformSend();
	}
	public void ActionPerformIconChat()
	{
		fatherChat[selectTab].ActionPerformIconChat();
	}
	public void Update()
	{
		fatherChat[selectTab].Update();
		
		if(moveClose)//move to right
		{
			if(xMove>limMove)
			{
//				xMove=limMove;
				v=0;
			}
			else
			{
				
				xMove+=v;
				v++;
			}
		}else//move to left
		{
			
			if(xMove<=-30)
			{
//				xMove=-10;
				v=0;
				xMove =Image.getWidth(LoadImageInterface.imgShortQuest)- this.x-xBtnMove - 35;
			}
			else
			{
				xMove+=v;
				v--;
			}
		}
		if(v!=0)
			Move();
		
	}
	public void UpdateKey()
	{

        if (moveClose)
		fatherChat[selectTab].UpdateKey();

		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(bntOpen)) {
			if (bntOpen != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (bntOpen != null)
				{
					bntOpen.performAction();
				}
			}
		}
	}
	public void KeyPress(int keyCode)
	{
		fatherChat[selectTab].KeyPress(keyCode);
	}
	public void Close()
	{
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		fatherChat[selectTab].perform(idAction,p);
		switch (idAction) {
		case Constants.BUTTON_OPEN:
			if(!moveClose)
			{
				Cout.println(" -----> mo chat");
				GameScreen.gI().guiMain.menuIcon.indexpICon = Constants.BUTTON_OPEN; //truong hop mo chat ko upda menuicon
			
				moveClose=true;
				bntOpen.setPos(x+xBtnMove-Image.getWidth(LoadImageInterface.imgShortQuest) + 35, y+130/2-Image.getHeight(LoadImageInterface.imgShortQuest)/2 +25, LoadImageInterface.imgShortQuest_Close, LoadImageInterface.imgShortQuest_Close);
			}
			else
			{

				if(fatherChat[selectTab].iconChat!=null) return;
				GameScreen.gI().guiMain.menuIcon.indexpICon = 0;//truong hop dong chat ko upda menuicon
				
				moveClose=false;
				bntOpen.setPos(x+xBtnMove-Image.getWidth(LoadImageInterface.imgShortQuest) + 35, y+130/2-Image.getHeight(LoadImageInterface.imgShortQuest)/2 +25, LoadImageInterface.imgShortQuest, LoadImageInterface.imgShortQuest);
			}
			break;

		default:
			break;
		}
			
	}
	/*
	 * Paint background tab  working and done
	 */
	public void paintTabBig(mGraphics g, int x, int y,int w,int h) {
		
//		g.setColor(0x584848);
		g.drawImage(LoadImageInterface.imgChatButton, x, y, 0);

		
	}
	/*
	 * Paint background tab is selected
	 */
	public void paintTabFocus(mGraphics g, int x, int y,int w,int h) {
//		g.setColor(0x454545);
//		g.fillRect(x, y, w, h);
		g.drawImage(LoadImageInterface.imgChatButtonFocus, x, y, 0);

	}
	
	/*
	 * execute touch
	 */
	public void updatePointer() {
		if (GameCanvas.isPointerClick) 
		{
			//index select tab
			for(int i=0;i<nTab;i++)
			{
				if (GameCanvas.isPointer(this.x+(this.widthTab+this.distanceTab)*i+xMove,this.yButtonTab,this.widthTab,this.heightTab)) 
				{
					selectTab=i;
					GameCanvas.isPointerClick = false;
					break;
				}
			}
		}
	}

	public void resetTab(boolean isResetCmy) {
		TabScreenNew.timeRepaint = 10;
		int t = 0;
		
	}
	
	private void Move()
	{
		bntOpen.x=this.x+xBtnMove-Image.getWidth(LoadImageInterface.imgShortQuest)+xMove + 35;
	}
	public void Paint(mGraphics g)
	{
		PaintFrameChat(g);
		//paint background tab
		for(int i=0;i<nTab;i++)
		{
			paintTabBig(g,this.x+(this.widthTab+this.distanceTab)*i+xMove ,this.yButtonTab,this.widthTab+this.distanceTab , this.heightTab);
			mFont.tahoma_7_white.drawString(g,this.listNameTab[i] , this.x+(this.widthTab+this.distanceTab)*i+this.widthTab/2+xMove - 7 , this.heightTab/4+this.yButtonTab-4 ,2);
		}
		//paint background tab focus
		paintTabFocus(g,this.x+(this.widthTab+this.distanceTab)*selectTab+xMove ,this.yButtonTab, this.widthTab+this.distanceTab , this.heightTab);
		mFont.tahoma_7_white.drawString(g,this.listNameTab[selectTab] , this.x+(this.widthTab+this.distanceTab)*selectTab+this.widthTab/2+xMove - 7 ,this.heightTab/4+this.yButtonTab-4,2);

		bntOpen.paint(g);
		fatherChat[selectTab].paintContentChatWorld(g);
		GameScreen.resetTranslate(g);
	}
	
	private void PaintFrameChat(mGraphics g)
	{
		//first row
		g.drawImage(LoadImageInterface.imgChatConner, x+xMove, y, mGraphics.TOP | mGraphics.LEFT);
		for(int i=1;i<18;i++)
		{
			g.drawImage(LoadImageInterface.imgChatRec, x+i*(Image.getWidth(LoadImageInterface.imgChatRec))+xMove, y, 0);
		}
		g.drawRegion(LoadImageInterface.imgChatConner, 0, 0,Image.getWidth(LoadImageInterface.imgChatConner),
				Image.getHeight(LoadImageInterface.imgChatConner), Sprite.TRANS_MIRROR, x +18*(Image.getWidth(LoadImageInterface.imgChatConner))+xMove, y,mGraphics.TOP | mGraphics.LEFT);
		
		//paint all rac of frame
		for(int i=1;i<20;i++)
			for(int j=0;j<19;j++)
			{
				g.drawImage(LoadImageInterface.imgChatRec, x+j*(Image.getWidth(LoadImageInterface.imgChatRec))+xMove, y+Image.getHeight(LoadImageInterface.imgChatRec)*i, 0);
			}
		
		
		g.drawImage(LoadImageInterface.imgChatConner_1, x+xMove, y+Image.getHeight(LoadImageInterface.imgChatRec)*20, mGraphics.TOP | mGraphics.LEFT);
		for(int i=1;i<18;i++)
		{
			g.drawImage(LoadImageInterface.imgChatRec_1, x+i*(Image.getWidth(LoadImageInterface.imgChatRec_1))+xMove, y+Image.getHeight(LoadImageInterface.imgChatRec)*20, 0);
		}
		
		g.drawRegion(LoadImageInterface.imgChatConner_1, 0, 0,Image.getWidth(LoadImageInterface.imgChatConner_1),
				Image.getHeight(LoadImageInterface.imgChatConner_1), Sprite.TRANS_MIRROR, x +18*(Image.getWidth(LoadImageInterface.imgChatConner_1))+xMove,y+Image.getHeight(LoadImageInterface.imgChatRec)*20,mGraphics.TOP | mGraphics.LEFT);
	}
}
