package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.real.Service;
import screen.FlagScreen;
import screen.GameScreen;
import screen.SettingScreen;
import Objectgame.Char;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.mResources;

public class MenuIcon extends Screen implements IActionListener{
	
	public static boolean isCloseSub=false,isShowTab;
	int x,y;
	public int indexpICon;
	public Command iconChar,iconMission,iconShop,iconContact,iconImprove,iconTrade,iconPK,iconLogout;//main
	public static mVector lastTab = new mVector();
	int width=430;

	GuiFriend friend;
	ShopMain shop;//shop
	GuiQuest quest;//quest
	public TradeGui trade;//trade
	QuestMain questMain;
	public GuiContact contact;
	TabParty party;
	
	public boolean paintButtonClose;
	
	MenuIcon subMenu;
	
	public MenuIcon(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x=x;
		this.y=y;
		
		InitComand();
		
	}
	
	public void InitComand()
	{
		int x=0;
		//trade
		//Logout

		int indexcong = 20;
		x=this.x+width/2 -156+indexcong;

		iconLogout = new Command("", this, Constants.ICON_LOGOUT, null, 0, 0);
		iconLogout.setPos(x,y, LoadImageInterface.imgLogout, LoadImageInterface.imgLogout);

		x=this.x+width/2 -125+indexcong;

		iconTrade= new Command("", this, Constants.ICON_TRADE, null, 0, 0);
		iconTrade.setPos(x,y+1, LoadImageInterface.imgIconDeoCo, LoadImageInterface.imgIconDeoCo);
		
		iconPK=new Command("", this, Constants.ICON_DEO_CO, null, 0, 0);
		iconPK.setPos(x,y+1, LoadImageInterface.imgIconDeoCo, LoadImageInterface.imgIconDeoCo);
		
		
		//improve//settings
		x=this.x+width/2 -84+indexcong;
		iconImprove= new Command("", this, Constants.ICON_IMPROVE, null, 0, 0);
		iconImprove.setPos(x,y+1, LoadImageInterface.imgSettingIcon, LoadImageInterface.imgSettingIcon);
		
		//quest
		x=this.x+width/2 -47+indexcong;
		iconMission= new Command("", this, Constants.ICON_MISSION, null, 0, 0);
		iconMission.setPos(x,y+4, LoadImageInterface.imgMissionIcon, LoadImageInterface.imgMissionIcon);
		
		//char
		x=this.x+width/2-10+indexcong;
		iconChar= new Command("", this, Constants.ICON_CHAR, null, 0, 0);
		iconChar.setPos(x,y, LoadImageInterface.imgCharIcon, LoadImageInterface.imgCharIcon);
		
		//shop
		x=this.x+width/2+27+indexcong;
		iconShop= new Command("", this, Constants.ICON_SHOP, null, 0, 0);
		iconShop.setPos(x,y+2, LoadImageInterface.imgShopIcon, LoadImageInterface.imgShopIcon);
		
		//contact
		x=this.x+width/2+64+indexcong;
		iconContact= new Command("", this, Constants.ICON_CONTACT, null, 0, 0);
		iconContact.setPos(x,y+2, LoadImageInterface.imgContactIcon, LoadImageInterface.imgContactIcon);

		
		//close com.sakura.thelastlegend.gui
		cmdClose = new Command(" ",this, Constants.CMD_TAB_CLOSE,null,0,0);
		cmdClose.setPos(GameCanvas.hw+180, y, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}

	public void updatePointer()
	{
		if(shop!=null)//shop
		{
			shop.updatePointer();
		}
		
//		if(quest!=null)//quest
//		{
//			quest.updatePointer();
//		}
//		
//		if(trade!=null)//trade
//		{
//			trade.updatePointer();
//		}
//		
//		if(subMenu!=null)//sub menu
//		{
//			subMenu.updatePointer();
//		}
	}
	public void update(){

		if(!isShowTab&&lastTab.size()==0&&indexpICon!=0){
			Cout.println("indexpICon  "+indexpICon);
			indexpICon = 0;
		}
		switch (indexpICon) {
		case Constants.ICON_CONTACT:
			if(contact!=null)
				contact.Update();
			break;
		case Constants.ICON_SHOP:
			if(shop!=null)//shop
			{
				shop.update();
			}
			break;
		case Constants.ICON_TEAM:
			if(party!=null)//shop
			{
				party.update();
			}
			break;
		case Constants.ICON_SUB_FRIEND:
			if(friend!=null)
				friend.update();
			break;
		case Constants.ICON_GIAOTIEP:
			if(contact!=null)
				contact.Update();
			break;
		case Constants.ICON_MISSION:
				quest.update();
			break;
		}
	}
	public void updateKey()
	{
		//click close
				if ((GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdClose))) {
					if (iconShop != null) {
						GameCanvas.isPointerJustRelease = false;
						GameCanvas.keyPressed[5] = false;
						Screen.keyTouch = -1;
						if (cmdClose != null)
						{
							cmdClose.performAction();	
						}
					}
				}

		switch (indexpICon) { //chi dc update 1 trong các man hinh
		case Constants.ICON_SHOP:
			if(shop!=null)//shop
			{
				shop.updateKey();
				return;
			}
			break;

		case Constants.ICON_TRADE:
			if(trade!=null)//trade
			{
				trade.updateKey();
				return;
//				trade.update();
			}
			break;
		case Constants.ICON_CHAR:
			
			break;
		case Constants.ICON_MISSION:
			if(quest!=null)//shop
			{
				quest.updateKey();
				return;
			}
			break;
		case Constants.ICON_CONTACT:
			if(contact!=null)//
			{
				contact.UpdateKey();
				return;
			}
			break;

		case Constants.ICON_GIAOTIEP:
			if(contact!=null)
				contact.UpdateKey();
			break;
		case Constants.ICON_TEAM:
				if(party!=null){
					party.updateKey();
					return;
				}
				
			break;
		case Constants.ICON_SUB_FRIEND:
			if(friend!=null)
				friend.updateKey();
			break;
		case 0:
			//click quest
//			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconMission)) {
//				if (iconMission != null) {
//					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
//					Screen.keyTouch = -1;
//					if (iconMission != null)
//					{
//						iconMission.performAction();	
//					}
//				}
//			}
			
			//click char
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconChar)) {
				if (iconChar != null) {
					GameCanvas.isPointerJustRelease = false;
					Cout.println("menucicon iconChar "+GameCanvas.isPointerJustRelease);
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconChar != null)
					{
						iconChar.performAction();	
					}
				}
			}
			
			//click shop
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconShop)) {
				if (iconShop != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconShop != null)
					{
						iconShop.performAction();	
					}
				}
			}
			//logout
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconLogout)) {
				if (iconLogout != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconLogout != null)
					{
						iconLogout.performAction();	
					}
				}
			}
			
			//click trade
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconPK)) {
				if (iconPK != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconPK != null)
					{
						iconPK.performAction();	
					}
				}
			}
			
			//click contact
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconContact)) {
				if (iconContact != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (iconContact != null)
					{
						iconContact.performAction();	
					}
				}
			}
			
			//click quest
					if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconMission)) {
						if (iconMission != null) {
							GameCanvas.isPointerJustRelease = false;
							GameCanvas.keyPressed[5] = false;
							Screen.keyTouch = -1;
							if (iconMission != null)
							{
								iconMission.performAction();	
							}
						}
					}
					if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconImprove)) {
						if (iconImprove != null) {
							GameCanvas.isPointerJustRelease = false;
							GameCanvas.keyPressed[5] = false;
							Screen.keyTouch = -1;
							if (iconImprove != null)
							{
								iconImprove.performAction();	
							}
						}
					}
			break;
		}
		
		
		if(subMenu!=null)//sub contact
		{
			subMenu.updateKey();
		}
		if(!GameCanvas.isPointerDown &&isCloseSub)
		{
			isCloseSub=false;
			subMenu=null;
		}
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub

		GameScreen.gI().guiMain.moveClose=false;
		Cout.println(MenuIcon.lastTab.size()+" idAction "+idAction);
		switch (idAction) {
		case Constants.ICON_CHAR:
			GameScreen.gI().guiMain.moveClose=false;
			indexpICon = Constants.ICON_CHAR;
			GameScreen.isBag=true;
			TabBag bg = (TabBag)GameCanvas.AllInfo.VecTabScreen.elementAt(0);
			bg.idSelect = -1;
			Service.gI().requestinventory();
			MenuIcon.lastTab.add(""+ Constants.ICON_CHAR);
			Cout.println("adđ ICON_CHAR "+MenuIcon.lastTab.size());
			break;

		case Constants.ICON_MISSION:
			GameScreen.gI().guiMain.moveClose=false;
			indexpICon = Constants.ICON_MISSION;
			if(quest==null)
				quest= new GuiQuest(GameCanvas.wd6-20, 20);
			MenuIcon.lastTab.add(""+ Constants.ICON_MISSION);
			Cout.println("adđ ICON_MISSION "+MenuIcon.lastTab.size());
			paintButtonClose=true;
			quest.SetPosClose(cmdClose);
			break;
		case Constants.ICON_IMPROVE:
			SettingScreen.gI().switchToMe();
			//Service.gI().Clan_GetlistUser_local();
			//Service.gI().Clan_GetlistUser_gobal();
			//ClanScreen.gI().switchToMe();
//			GameCanvas.StartDglThongBao("Chức năng đang phát triển");
			break;
		case Constants.ICON_TEAM:

			indexpICon = Constants.ICON_TEAM;
			if(party==null)
				party=new TabParty(GameCanvas.hw, 20);

			MenuIcon.lastTab.add(""+ Constants.ICON_TEAM);
			Cout.println("adđ ICON_TEAM "+MenuIcon.lastTab.size());
			paintButtonClose=true;
			party.SetPosClose(cmdClose);
			
			break;
		case Constants.ICON_SHOP:
			if(ShopMain.nameMenu==null){
				GameCanvas.startOKDlg("Cửa hàng chưa mở cửa. Vui lòng quay lại sau !");
				return;
			}else{
			GameScreen.gI().guiMain.moveClose=false;
			indexpICon = Constants.ICON_SHOP;
			if(shop==null)
				shop=new ShopMain(GameCanvas.wd6-20, 20);
			shop.indexidmenu = 0;
			paintButtonClose=true;
			shop.SetPosClose(cmdClose);

			MenuIcon.lastTab.add(""+ Constants.ICON_SHOP);
			Cout.println("adđ ICON_SHOP "+MenuIcon.lastTab.size());
			}
			break;
			
		case Constants.ICON_TRADE:
			if(Char.myChar().partnerTrade!=null){
				GameScreen.gI().guiMain.moveClose=false;
				indexpICon = Constants.ICON_TRADE;
				trade=new TradeGui(GameCanvas.wd6-20, 20);
				trade.SetPosClose(cmdClose);
				paintButtonClose=true;
				MenuIcon.lastTab.add(""+ Constants.ICON_TRADE);
				Cout.println("adđ ICON_TRADE "+MenuIcon.lastTab.size());
			}
			break;
			
		case Constants.ICON_CONTACT:

			indexpICon = Constants.ICON_CONTACT;
			GameScreen.gI().guiMain.moveClose=false;
			paintButtonClose=true;
//			giaotiep = new GuiGiaoTiep(GameCanvas.hw, 20);
//			giaotiep.SetPosClose(cmdClose);

//			contact = new GuiContact(GameCanvas.hw, 20);
//			contact.SetPosClose(cmdClose);
//			contact
			if(subMenu==null)
				subMenu=new SubMenuContact(iconContact.x-SubMenuContact.width2,iconContact.y-50);
			subMenu.SetPosClose(cmdClose);
			MenuIcon.lastTab.add(""+ Constants.ICON_CONTACT);

			Cout.println("adđ contact "+MenuIcon.lastTab.size());
			break;
		case Constants.ICON_DEO_CO:
			GameScreen.gI().guiMain.moveClose=false;
			FlagScreen.gI().switchToMe();
			break;
		case Constants.CMD_TAB_CLOSE:
			indexpICon = 0;

			paintButtonClose=false;
			if(lastTab.size()>0){
				String indextabx = (String)lastTab.elementAt(lastTab.size()-1);
				
				try {
					int indextab =Integer.parseInt(indextabx);
					if(lastTab.size()>1){
						indexpICon = Integer.parseInt((String)lastTab.elementAt(lastTab.size()-2));
						setPostCloseTab(indexpICon);
					}
					lastTab.removeElementAt(lastTab.size()-1);
					switch (indextab) {
					case Constants.ICON_CHAR:
						GameScreen.isBag=false;
						lastTab.removeAllElements();
						break;
					case Constants.ICON_GIAOTIEP:
						if(contact!=null)
							contact=null;
						break;
					case Constants.ICON_CONTACT:
						if(subMenu!=null)
							subMenu=null;
						break;
					case Constants.ICON_TRADE:
						if(trade!=null) {
							Service.gI().cancelTrade(Constants.CANCEL_TRADE, (short) Char.myChar().partnerTrade.charID);
							trade = null;
						}
						break;
					case Constants.ICON_SHOP:
						if(shop!=null)
							shop=null;
						break;
					case Constants.ICON_TEAM:
						if(party!=null)
							party = null;
						break;
					case Constants.ICON_MISSION:
						if(quest!=null)
							quest=null;
						break;
					case Constants.ICON_SUB_FRIEND:
						if(friend!=null)
							friend = null;
						break;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			Cout.println(GameScreen.gI().guiMain.moveClose+" lasstTab  "+lastTab.size());
			lastTab.removeAllElements();
			if(GameScreen.gI().guiChatClanWorld.moveClose)//TRUONG HOP DANG MO CHAT WORLD
			{
				GameScreen.gI().guiMain.menuIcon.indexpICon = 0;//truong hop dong chat ko upda menuicon
				
				GameScreen.gI().guiChatClanWorld.moveClose=false;
				GameScreen.gI().guiChatClanWorld.bntOpen.setPos(GameScreen.gI().guiChatClanWorld.x+ GameScreen.gI().guiChatClanWorld.xBtnMove-Image.getWidth(LoadImageInterface.imgShortQuest) + 35, GameScreen.gI().guiChatClanWorld.y+130/2-Image.getHeight(LoadImageInterface.imgShortQuest)/2 +25, LoadImageInterface.imgShortQuest, LoadImageInterface.imgShortQuest);
			}
			break;
		case Constants.ICON_LOGOUT:
			Cout.println("showwwwwwww dialog");
			GameCanvas.startYesNoDlg("Bạn có muốn thoát game?", new Command(mResources.YES, GameCanvas.instance, GameCanvas.cLogout, null), new Command(mResources.CANCEL, GameCanvas.instance,8882,null));
			break;
		}
		isShowTab = lastTab.size()>0?true:false;
		if(!isShowTab&&lastTab.size()==0) indexpICon = 0;

		Cout.println2222(MenuIcon.lastTab.size()+" endddd idAction "+idAction+" moveLose "+ GameScreen.gI().guiMain.moveClose);
	}
	public void setPostCloseTab(int indexTab){
		switch (indexTab) {
		case Constants.ICON_CHAR:
			break;

		case Constants.ICON_MISSION:
			GameScreen.gI().guiMain.moveClose=false;
			if(quest!=null)
				quest.SetPosClose(cmdClose);
			paintButtonClose=true;
			break;
		case Constants.ICON_TEAM:
			if(party!=null)
				party.SetPosClose(cmdClose);
			paintButtonClose=true;
			break;
		case Constants.ICON_SHOP:
			GameScreen.gI().guiMain.moveClose=false;
			if(shop!=null)
				shop.SetPosClose(cmdClose);
			paintButtonClose=true;
			break;
			
		case Constants.ICON_TRADE:
			GameScreen.gI().guiMain.moveClose=false;
			indexpICon = Constants.ICON_TRADE;
			if(trade!=null)
				trade.SetPosClose(cmdClose);
			paintButtonClose=true;
			break;
			
		case Constants.ICON_CONTACT:
			GameScreen.gI().guiMain.moveClose=false;
			paintButtonClose=true;
			if(contact!=null)
				contact.SetPosClose(cmdClose);
			break;
		}
	}

	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		Cout.println(getClass(), "menuicon setposclose");
		cmd.setPos(x+width-Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}
	
	public void paint(mGraphics g)
	{
		Paint.PaintBGMenuIcon(x,y,10,g);
		
		iconPK.y=y+1;
		iconImprove.y=y+7;
		iconMission.y=y+4;
		iconChar.y=y;
		iconShop.y=y+2;
		iconContact.y=y+2;
		iconLogout.y=y+1;
		iconLogout.paint(g);
		iconPK.paint(g);
		iconMission.paint(g);
		iconChar.paint(g);
		iconShop.paint(g);
		iconImprove.paint(g);
		iconContact.paint(g);
		
		if(shop!=null)//shop
			shop.paint(g);
		if(party!=null)
			party.paint(g);
		if(quest!=null)//quest
			quest.paint(g);
		
		if(trade!=null)//trade
			trade.paint(g);

		if(contact!=null)
			contact.Paint(g);
		if(subMenu!=null)
		{
			subMenu.paint(g);
		}
		if(friend!=null)
			friend.paint(g);
		if(paintButtonClose){
			cmdClose.paint(g);
		}
		
		GameScreen.resetTranslate(g);
		
		
		
	}

}
