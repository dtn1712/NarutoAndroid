package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import screen.GameScreen;

import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mVector;

/*
 * this class is all menu game
 */
public class TabScreenNew extends Screen implements IActionListener {
    public int selectTab = 0;
    public mVector VecTabScreen = new mVector();
    public static Command cmdTab, cmdBack;

    public static final int CMD_BACK_TAB = 1;
    public static int timeRepaint = 0;
    public static int xback, yback;

    public static Command cmdClose;


    int xMenu, yMenu;
    int wMenu, hMenu;
    String[] nameMenu = {"Hành trang", "Trang bị", "Nhân vật", "Kỹ năng", "Nâng cấp"};


    public TabScreenNew() {
        cmdClose = new Command(" ", this, Constants.CMD_TAB_CLOSE, null, 0, 0);
        xback = 300;
        yback = 20;
        cmdClose.setPos(MainTabNew.gI().xTab + MainTabNew.gI().wtab2 - LoadImageInterface.closeTab.width / 2,
                MainTabNew.gI().yTab, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
        boolean xm = (GameCanvas.w - MainTabNew.gI().wtab2 - MainTabNew.gI().widthSubFrame) > 0 ? true : false;
        xMenu = (xm == false ? MainTabNew.gI().xTab - LoadImageInterface.btnTab.width / 4 :
                MainTabNew.gI().xTab + MainTabNew.gI().wtab2 - 10);
        yMenu = 50;

        wMenu = Image.getWidth(LoadImageInterface.btnTab);
        hMenu = Image.getHeight(LoadImageInterface.btnTab);

    }

    public void Show() {
    }

    public void Show(Screen last) {
        timeRepaint = 10;

        if (GameCanvas.isTouch) {
            MainTabNew.Focus = MainTabNew.INFO;
            MainTabNew msg = getCurrentTab();
            msg.init();
            MainTabNew.timePaintInfo = 0;
            //right = cmdBack;
        } else
            MainTabNew.Focus = MainTabNew.TAB;
        this.lastScreen = last;
        super.switchToMe();

    }

    public void commandPointer(int index, int subIndex) {
        // TODO Auto-generated method stub
        switch (index) {
            case Constants.CMD_TAB_CLOSE:
                if (lastScreen.lastScreen != null)
                    lastScreen.switchToMe();
                else
                    lastScreen.switchToMe();
                break;

        }
    }

    public void addMoreTab(mVector t) {
        VecTabScreen.removeAllElements();
        VecTabScreen = t;
    }

    public void paint(mGraphics g) {
        MainTabNew tab = getCurrentTab();
        GameScreen.resetTranslate(g);
        for (int i = 0; i < VecTabScreen.size(); i++) {
            g.drawImage(LoadImageInterface.btnTab, xMenu, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i, 0);
        }
        for (int i = 0; i < VecTabScreen.size(); i++) {
            if (i != selectTab)
                mFont.tahoma_7b_white.drawString(g, nameMenu[i],
                        xMenu + Image.getWidth(LoadImageInterface.btnTab) / 2,
                        yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i
                                + Image.getHeight(LoadImageInterface.btnTab) / 4, 2);
        }
        MainTabNew.gI().paintTab(g, tab.nameTab, selectTab, VecTabScreen, false);


        //paint tab is selected
        tab.paint(g);
        GameScreen.resetTranslate(g);


        g.drawImage(LoadImageInterface.btnTabFocus, xMenu, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * selectTab, 0);
        for (int i = 0; i < VecTabScreen.size(); i++) {
            if (i == selectTab)
                mFont.tahoma_7b_white.drawString(g, nameMenu[i],
                        xMenu + Image.getWidth(LoadImageInterface.btnTab) / 2,
                        yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i
                                + Image.getHeight(LoadImageInterface.btnTab) / 4, 2);
        }
        if (tab.isPaintCmdClose)
            cmdClose.paint(g);

//		

//		if (!GameCanvas.menu2.isShowMenu && GameCanvas.currentDialog == null
//				&& GameCanvas.subDialog == null
//				&& GameCanvas.currentScreen == this) {
//			if (GameCanvas.isTouch) {
//				super.paintCmd(g);
//			} else {
//				if (MainTabNew.Focus == MainTabNew.TAB)
//					paintCmd_OnlyText(g);
//				else
//					tab.paintCmd_OnlyText(g);
//			}
//		} else {
//			timeRepaint = 10;
//		}
//		if (GameScreen.help.Step >= 0) {
//			if (!GameCanvas.menu2.isShowMenu
//					&& GameCanvas.currentDialog == null
//					&& GameCanvas.subDialog == null
//					&& tab.typeTab != MainTabNew.INVENTORY)
//				GameScreen.help.itemTabHelp(g, null, tab.typeTab);
//		}
        // if(GameScreen.help.Step>=0){
        // GameScreen.help.paintHelpFrist(g)
        // }

    }

    public void update() {
        if (lastScreen == GameCanvas.currentScreen)
            lastScreen.update();
        MainTabNew tab = getCurrentTab();
        tab.update();
//		if (!GameCanvas.menu2.isShowMenu && GameCanvas.currentDialog == null&&GameCanvas.currentScreen==this)
//			tab.update();
//		if (GameCanvas.menu2.isShowMenu || GameCanvas.currentDialog != null)
//			timeRepaint = 10;
//		else
        if (timeRepaint > 0) {
            timeRepaint--;
        }
    }

    private MainTabNew getCurrentTab() {
        if (selectTab >= VecTabScreen.size()) selectTab = 0;
        return (MainTabNew) VecTabScreen.elementAt(selectTab);
    }

    public void updatekey() {

//		if(GameCanvas.menu2.isShowMenu||GameCanvas.currentDialog!=null||GameCanvas.subDialog!=null)
//			return;
        if (MainTabNew.Focus == MainTabNew.TAB) {
            timeRepaint = 10;
            left = null;
            center = null;
            right = cmdBack;
            int cur = selectTab;
            if (GameCanvas.keyHold[2]) {
                selectTab--;
                GameCanvas.clearKeyHold();
//				if(TabRebuildItem.resetItemReplace){
//					TabRebuildItem.itemFree=null;
//					TabRebuildItem.itemPlus=null;
//					TabRebuildItem.itemWing=null;
//					TabRebuildItem.resetItemReplace=false;
//				}
            } else if (GameCanvas.keyHold[8]) {
                selectTab++;
                GameCanvas.clearKeyHold();
//				if(TabRebuildItem.resetItemReplace){
//					TabRebuildItem.itemFree=null;
//					TabRebuildItem.itemPlus=null;
//					TabRebuildItem.itemWing=null;
//					TabRebuildItem.resetItemReplace=false;
//				}
            } else if (GameCanvas.keyHold[4] || GameCanvas.keyHold[6]) {
                GameCanvas.clearKeyHold();
                GameCanvas.clearKeyPressed();
                SetInit();
            }


//			selectTab = resetSelect(selectTab, VecTabScreen.size() - 1, true);
//			if(cur!=selectTab){
//				GameScreen.cameraSub.yCam=0;
//			}
            MainTabNew msg = getCurrentTab();
            if (msg.typeTab == MainTabNew.CONFIG || msg.typeTab == MainTabNew.FUNCTION)
                msg.init();
//			if (GameScreen.help.setStep_Next(3, 8)) {
//				if (msg.typeTab == MainTabNew.EQUIP) {
//					msg.init();
//					GameScreen.help.NextStep();
//					GameScreen.help.setNext();
//				}
//			} else if (GameScreen.help.setStep_Next(7, 9)) {
//				if (msg.typeTab == MainTabNew.SKILLS) {
//					msg.init();
//					GameScreen.help.NextStep();
//					GameScreen.help.setNext();
//				}
//			} else if (GameScreen.help.setStep_Next(9, 1)) {
//				if (msg.typeTab == MainTabNew.QUEST) {
//					msg.init();
//					GameScreen.help.Next++;
//					GameScreen.help.setNext();
//				}
//			}
            super.updateKey();
        } else {
//			MainTabNew msg = getCurrentTab();
//			if (!GameCanvas.menu2.isShowMenu
//					&& GameCanvas.currentDialog == null)
//				msg.updatekey();
        }
        MainTabNew tab = getCurrentTab();
        tab.updateKey();

        if (tab.isPaintCmdClose && (GameCanvas.keyPressed[13] || Screen.getCmdPointerLast(cmdClose))) {
            if (cmdClose != null) {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                Screen.keyTouch = -1;
                if (cmdClose != null) {
                    cmdClose.performAction();
                }
            }
        }
        GameCanvas.clearKeyPressed();

    }

    //execute touch
    public void updatePointer() {
//		if(GameCanvas.menu2.isShowMenu||GameCanvas.currentDialog!=null||GameCanvas.subDialog!=null)
//			return;
        MainTabNew msg = getCurrentTab();
        if (msg.typeTab == MainTabNew.REBUILD) {
//			if(TabRebuildItem.isBeginEff!=0)
//				return;
        }

        int ySelect = yMenu;
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu)) {

            selectTab = 0;
            TabBag b = (TabBag) getCurrentTab();
            b.idSelect = -1;
            GameCanvas.isPointerClick = false;

        }
        ySelect += (5 + hMenu);
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu)) {

            selectTab = 1;
            GameCanvas.isPointerClick = false;

        }
        ySelect += (5 + hMenu);
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu)) {

            selectTab = 2;
            GameCanvas.isPointerClick = false;

        }
        ySelect += (5 + hMenu);
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu)) {

            selectTab = 3;
            GameCanvas.isPointerClick = false;

        }
        ySelect += (5 + hMenu);
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu)) {

            selectTab = 4;
            GameCanvas.isPointerClick = false;

        }
        SetInit();
        msg = getCurrentTab();
        msg.updatePointer();
        if (true) return;


        if (GameCanvas.isPointer(MainTabNew.gI().xTab, MainTabNew.gI().yTab
                        + GameCanvas.h / 5, MainTabNew.wOneItem + MainTabNew.wOne5 * 2,
                ((MainTabNew.wOneItem)) * VecTabScreen.size())) {

            int select = (GameCanvas.py - (MainTabNew.gI().yTab + GameCanvas.h / 5))
                    / (MainTabNew.wOneItem);
            //select = resetSelect(select, VecTabScreen.size() - 1, false);
            if (select != selectTab) {
//				if(TabRebuildItem.resetItemReplace){
//					TabRebuildItem.itemFree=null;
//					TabRebuildItem.itemPlus=null;
//					TabRebuildItem.itemWing=null;
//					TabRebuildItem.resetItemReplace=false;
            }
            timeRepaint = 10;
            selectTab = select;
            SetInit();
            msg = getCurrentTab();
//				if (GameScreen.help.setStep_Next(3, 8)) {
//					if (msg.typeTab == MainTabNew.EQUIP) {
//						GameScreen.help.NextStep();
//						GameScreen.help.setNext();
//					}
//				} else if (GameScreen.help.setStep_Next(7, 9)) {
//					if (msg.typeTab == MainTabNew.SKILLS) {
//						GameScreen.help.NextStep();
//						GameScreen.help.setNext();
//					}
//				} else if (GameScreen.help.setStep_Next(9, 1)) {
//					if (msg.typeTab == MainTabNew.QUEST) {
//						GameScreen.help.Next++;
//						GameScreen.help.setNext();
//					}
//				}

//			}
            GameCanvas.isPointerClick = false;
        }
        // if (MainTabNew.Focus == MainTabNew.INFO) {
        msg.updatePointer();
        // } else
        super.update();
        // TODO Auto-generated method stub

    }

    public void SetInit() {
        MainTabNew.Focus = MainTabNew.INFO;
        MainTabNew msg = getCurrentTab();
        msg.init();
    }

    public void keyPress(int keyCode) {
        MainTabNew msg = getCurrentTab();
        msg.keypress(keyCode);
        super.keyPress(keyCode);
    }

    public void perform(int idAction, Object p) {
        // TODO Auto-generated method stub
        switch (idAction) {
            case Constants.CMD_TAB_CLOSE:
                GameScreen.gI().guiMain.menuIcon.cmdClose.performAction();
                GameScreen.isBag = false;
                break;

        }
    }
}
