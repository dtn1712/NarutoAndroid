package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Quest;
import screen.GameScreen;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;

public class GuiQuest extends Screen implements IActionListener {
	public mVector VecTabScreen = new mVector();
	
	int x,y,width=180,height=232,widthSub=160,heightSub=230,xSub;
	int indexSize;
	int wScollReceived=100,hScollReceived=60;
	int indexRow=-1,indexRowUnRecive;
	public static mVector listNhacNv = new mVector();
	String nameMenu[]={"Chính","Phụ","Phó bản"};
	int xMenu,yMenu;
	int wMenu,hMenu;
	public int indexselectNV = 0;//0 nv moi, 1 nv da nhan
	public int selectTab = 0;//chinh,phu,phoban
	Scroll info = new Scroll();
	public int lengInfo = 3;
	
	int xM;//diem nam giua frame
	
	public GuiQuest(int x, int yy)
	{
		indexselectNV = 0;
		int wall = width+30+widthSub;
		this.x=(GameCanvas.w/2-wall/2);
		this.y=(GameCanvas.h/2-heightSub/2<0?0:GameCanvas.h/2-heightSub/2);
		
		int xdem  = this.x+widthSub;
		xSub = this.x;
		this.x = xdem+10;
		xMenu=this.x+width-15;
		yMenu=y+40;
		this.xM=this.x+width/2;
		wMenu=Image.getWidth(LoadImageInterface.btnTab);
		hMenu=Image.getHeight(LoadImageInterface.btnTab);
		
		indexSize=mFont.tahoma_7_yellow.getHeight()+2;
		
		QMain qMain= new QMain();
		VecTabScreen.addElement(qMain);
		
		QMain qMain1= new QMain();
		VecTabScreen.addElement(qMain1);
		
		QMain qMain2= new QMain();
		VecTabScreen.addElement(qMain2);
		
	}
	
	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		cmd.setPos(x+width-Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
		
	}
	public void updateKey() {

		info.updatecm();
		ScrollResult s1 =  info.updateKey();
//		x+40,y+70,160
		if(GameCanvas.isPointer(x+40,y+70,160, Image.getHeight(LoadImageInterface.bgQuestLineFocus))&&selectTab==0){
			indexselectNV = 0;
			GameCanvas.isPointerClick= false;
		}
		else if(GameCanvas.isPointer(x+40,y+165,160, Image.getHeight(LoadImageInterface.bgQuestLineFocus))&&selectTab==0){
			indexselectNV = 1;
			GameCanvas.isPointerClick= false;
		}
		for(int i=0;i<VecTabScreen.size();i++)
		{
			if(GameCanvas.isPointer(xMenu,
					yMenu+(5+Image.getHeight(LoadImageInterface.btnTab))*i,
					Image.getWidth(LoadImageInterface.btnTab), Image.getHeight(LoadImageInterface.btnTab)))
			{
				selectTab = i;
				indexselectNV = -1;
				GameCanvas.isPointerClick= false;
			}
		}
	}
	
	
	public void updatePointer() {
		// TODO Auto-generated method stub
		int ySelect=yMenu;
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			Cout.println(getClass(), " update 1");
			selectTab=0;
			GameCanvas.isPointerClick= false;
			
		}
		ySelect+=(5+hMenu);
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			Cout.println(getClass(), " update 1");
			selectTab=1;
			GameCanvas.isPointerClick= false;
			
		}
		ySelect+=(5+hMenu);
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			Cout.println(getClass(), " update 1");
			selectTab=2;
			GameCanvas.isPointerClick= false;
			
		}
	}
	public void update(){
	}

	public void paint(mGraphics g)
	{
		GameScreen.resetTranslate(g);
		g.setColor(0xff000000,GameCanvas.opacityTab);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		QuestMain questMain= (QuestMain) VecTabScreen.elementAt(selectTab);
		
		for(int i=0;i<VecTabScreen.size();i++)
		{
			g.drawImage(LoadImageInterface.btnTab, xMenu,yMenu+(5+Image.getHeight(LoadImageInterface.btnTab))*i, 0);
		}
		
		for(int i=0;i<VecTabScreen.size();i++)
		{
			if(i!=selectTab)
			mFont.tahoma_7b_white.drawString(g,nameMenu[i],xMenu+32,yMenu+Image.getHeight(LoadImageInterface.btnTab)/4+(5+Image.getHeight(LoadImageInterface.btnTab))*i,2);
		}
		GameCanvas.paint.paintFrameNaruto(this.x,this.y, this.width,this.height,g);
		Paint.PaintBoxName("Nhiệm vụ "+nameMenu[selectTab].toLowerCase(),x+45,this.y,width-90,g);
		Paint.SubFrame(xSub,y,widthSub,heightSub, g);//sub
		// thong tin quest focus 
		if(indexselectNV==0
				&&(Quest.listUnReceiveQuest.size()>0)
				)
		for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
			Quest q = (Quest)Quest.listUnReceiveQuest.elementAt(i);
			paintInfoQuestFocus(g, xSub+15,y+5, q,(byte)0);
		}
		//new quest
		g.drawImage(LoadImageInterface.imgName,xM,y+50,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g, "Mới",xM,y+45,2);
		Paint.PaintBGListQuest(x+30,y+70,width-50,g);//new quest
		if(indexselectNV==0&&Quest.listUnReceiveQuest.size()>0&&(GameCanvas.gameTick/10)%2==0)
			Paint.PaintBGListQuestFocus(x+30,y+70,width-50,g);//new focus quest
		//paint ds nv moi
		if(Quest.listUnReceiveQuest.size()>0&&selectTab==0)
		for (int i = 0; i < Quest.listUnReceiveQuest.size(); i++) {
				Quest q = (Quest)Quest.listUnReceiveQuest.elementAt(i);
				mFont.tahoma_7b_yellow.drawString(g,q.name,
						x+30,y+75,0);
			
		}
		else {
			mFont.tahoma_7b_yellow.drawString(g,"Không có nhiệm vụ",
					x+30,y+75,0);
		}
		Paint.PaintLine(x+5,y+125,width-30, g);//line
		
		//reciviced
		g.drawImage(LoadImageInterface.imgName,xM,y+145,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g, "Đã nhận",xM,y+140,2);
		Paint.PaintBGListQuest(x+30,y+165,width-50,g);//dang lam or hoan thanh
		if(indexselectNV==1
				&&(Quest.vecQuestDoing_Main.size()>0||Quest.vecQuestFinish.size()>0)
				&&(GameCanvas.gameTick/10)%2==0)
			Paint.PaintBGListQuestFocus(x+30,y+165,width-50,g);//new focus quest
		//paint ds nv dg lam
		if(Quest.vecQuestDoing_Main.size()>0&&selectTab==0)
			for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
				if(i==0){
					Quest q = (Quest)Quest.vecQuestDoing_Main.elementAt(i);
					mFont.tahoma_7b_yellow.drawString(g,q.name,
							x+30,y+170,0);
				}
			}
		else{
			mFont.tahoma_7b_yellow.drawString(g,"Không có nhiệm vụ",
					x+30,y+170,0);
		}
		// thong tin quest dg lamfocus 
		if(indexselectNV==1
				&&(Quest.vecQuestDoing_Main.size()>0)
				)
				for (int i = 0; i < Quest.vecQuestDoing_Main.size(); i++) {
					Quest q = (Quest)Quest.vecQuestDoing_Main.elementAt(i);
					paintInfoQuestFocus(g, xSub+15,y+5, q,(byte)1);
				}
		
		g.drawImage(LoadImageInterface.btnTabFocus,xMenu,yMenu+(5+Image.getHeight(LoadImageInterface.btnTabFocus))*selectTab, 0);
		for(int i=0;i<VecTabScreen.size();i++)
		{
			if(i==selectTab)
			mFont.tahoma_7b_white.drawString(g,nameMenu[i],xMenu+32,yMenu+Image.getHeight(LoadImageInterface.btnTab)/4+(5+Image.getHeight(LoadImageInterface.btnTab))*i,2);
		}
		
	}
	public void paintInfoQuestFocus(mGraphics g, int x, int y, Quest q,byte typequest){ // 0 co the nha,1dg lam//2 hoan thanh
		int size = 3+(q.strPaintDetailHelp!=null?+q.strPaintDetailHelp.length:0);
		size +=(typequest==1?3+q.strPaintDetailHelp.length:0);//yeu cau
		if(q.luong>0) size+=2;
		if(q.xu>0) size+=2;
		info.setStyle(size, 12,x-5,y,widthSub,heightSub-20, true, 1);
		info.setClip(g,x-5,y,widthSub,heightSub-20);
		//name
		g.drawImage(LoadImageInterface.imgName,x+30,y+10,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g,"Nhiệm vụ",x+30,y+5,2);
		mFont.tahoma_7b_yellow.drawString(g,q.name,x+15,y+20,0);
		// mo ta
		g.drawImage(LoadImageInterface.imgName,x+30,y+40,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g,"Mô tả",x+30,y+35,2);
		if(q.strPaintDetailHelp!=null)
		for (int i = 0; i < q.strPaintDetailHelp.length; i++) {
			mFont.tahoma_7_white.drawString(g,q.strPaintDetailHelp[i],x+15,y+50+12*i,0);
		}
		switch (typequest) {
		case 0:
			// qua
			g.drawImage(LoadImageInterface.imgName,x+30,y+65+12*q.strPaintDetailHelp.length,g.VCENTER|g.HCENTER);
			mFont.tahoma_7b_white.drawString(g,"Thưởng",x+30,y+60+12*q.strPaintDetailHelp.length,2);
			//mFont.tahoma_7_white.drawString(g,q.strGift,x+15,y+80+12*q.strPaintDetailHelp.length,0);
			if(q.luong>0){
				g.drawImage(LoadImageInterface.coins[1], x+15,y+75+12*q.strPaintDetailHelp.length,mGraphics.TOP|mGraphics.LEFT);
				mFont.tahoma_7_white.drawString(g,q.luong+"",x+40,y+80+12*q.strPaintDetailHelp.length,0);
			}
			if(q.xu>0){
				g.drawImage(LoadImageInterface.coins[0], x+15,y+75+(q.luong>0?20:0)+12*q.strPaintDetailHelp.length,mGraphics.TOP|mGraphics.LEFT);
				mFont.tahoma_7_white.drawString(g,q.xu+"",x+40,y+75+(q.luong>0?28:0)+12*q.strPaintDetailHelp.length,0);
			}
			
			break;
		case 1: //dag lam
			g.drawImage(LoadImageInterface.imgName,x+30,y+60+12*q.strPaintDetailHelp.length,g.VCENTER|g.HCENTER);
			mFont.tahoma_7b_white.drawString(g,"Yêu cầu",x+30,y+55+12*q.strPaintDetailHelp.length,2);
//			mFont.tahoma_7_white.drawString(g,q.strGift,x+15,y+80+12*q.strPaintDetailHelp.length,0);
			if(q.strPaintShortDetail!=null)
				for (int i = 0; i < q.strPaintShortDetail.length; i++) {
					mFont.tahoma_7_white.drawString(g,q.strPaintShortDetail[i],x+15,y+70+12*q.strPaintDetailHelp.length+12*i,0);
				}
			// qua
			g.drawImage(LoadImageInterface.imgName,x+30,y+85+12*q.strPaintDetailHelp.length+12*q.strPaintShortDetail.length,g.VCENTER|g.HCENTER);
			mFont.tahoma_7b_white.drawString(g,"Thưởng",x+30,y+80+12*q.strPaintDetailHelp.length+12*q.strPaintShortDetail.length,2);
			if(q.luong>0){
				g.drawImage(LoadImageInterface.coins[1], x+15,y+95+12*q.strPaintDetailHelp.length+12*q.strPaintShortDetail.length,mGraphics.TOP|mGraphics.LEFT);
				mFont.tahoma_7_white.drawString(g,q.luong+"",x+40,y+100+12*q.strPaintDetailHelp.length+12*q.strPaintShortDetail.length,0);
			}
			if(q.xu>0){
				g.drawImage(LoadImageInterface.coins[0], x+15,y+95+(q.luong>0?20:0)+12*q.strPaintDetailHelp.length+12*q.strPaintShortDetail.length,mGraphics.TOP|mGraphics.LEFT);
				mFont.tahoma_7_white.drawString(g,q.xu+"",x+40,y+95+(q.luong>0?28:0)+12*q.strPaintDetailHelp.length+12*q.strPaintShortDetail.length,0);
			}
			break;
		default:
			break;
		}
		GameCanvas.resetTrans(g);
	}
	public void perform(int idAction, Object p) {
	}

}
