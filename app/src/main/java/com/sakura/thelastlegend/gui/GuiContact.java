package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.real.Service;
import screen.ClanScreen;
import screen.GameScreen;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.OtherChar;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.Part;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.domain.model.SmallImage;

/*
 * Giao dien giao tiep giua cac nhan vat voi nhau
 */
public class GuiContact extends FatherChat{
	
	int x,y;
	int xM;//toan do giua cua ma hinh
	String [] listName={"Trò chuyện","Kết bạn","Thông tin","Mời nhóm","Giao dịch","Thách đấu"/*,"Mời vào bang local","Mời vào bang gobal"*/};
	
	public final int cTroChuyen = 2;
	public final int cKetban = 3;
	public final int cthongtin = 4;
	public final int cMoinhom = 5;
	public final int cGiaodich = 6;
	public final int cThachdau = 7;
	public final int cMoiVaoBang = 8;
	public final int cMoiVaoBangGobal = 9;
	
	public GuiContact(int x,int y) {
		
		popw=156;
		poph=203;
		this.xM=x;
		this.x=x-popw/2;
		this.y=y;
		scrMain.selectedItem = -1;
	}
	public void perform(int idAction, Object p) {
		
		
	}
	
	void PaintList(mGraphics g, String[] list,int x,int y,int width)
	{
		int nCol=width/Image.getWidth(LoadImageInterface.imgLineTrade);
//		System.out.println("");
		int yy=0;
		for(int i=0;i<list.length;i++)
		{
			yy+=30;
			mFont.tahoma_7b_red.drawString(g, list[i], x+width/2, y+yy-17, 2);
			for(int j=0;j<nCol;j++)
			{
				g.drawImage(LoadImageInterface.imgLineTrade,x+(Image.getWidth(LoadImageInterface.imgLineTrade)-1)*j+13,y+yy, 0,true);
			}
		}
	}
	public void Update() {
		// TODO Auto-generated method stub
		if(scrMain!=null)
		{
			scrMain.updatecm();
		}
		if(scrMain.selectedItem>-1){

			MenuIcon.isShowTab = false ;
			switch (scrMain.selectedItem+2) {
			case cTroChuyen:
				scrMain.selectedItem = -1;
				boolean isAdded = false;
				for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++) {
					OtherChar other =(OtherChar)ChatPrivate.vOtherchar.elementAt(i);
					if(other==null) return;
					if(other.name.equals(Char.myChar().charFocus.cName)||other.id==Char.myChar().charFocus.charID){
						if(other.id!=Char.myChar().charFocus.charID)
							other.id = (short)Char.myChar().charFocus.charID;
						isAdded = true;
						break;
					}
					
				}
				if(!isAdded&&Char.myChar().charFocus!=null)
					ChatPrivate.AddNewChater((short)Char.myChar().charFocus.charID, Char.myChar().charFocus.cName);
				TabChat.gI().switchToMe();
				GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
				break;
			case cKetban:
				Cout.println(getClass(), " cKetban ");
				scrMain.selectedItem = -1;
				if(Char.myChar().charFocus!=null)
				Service.gI().requestAddfriend((byte)0, (short)Char.myChar().charFocus.charID);
				GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
				break;
			case cthongtin:
				Cout.println(getClass(), " cthongtin ");
				scrMain.selectedItem = -1;
				GameCanvas.StartDglThongBao("Chức năng đang phát triển");
				GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
				break;
			case cMoinhom:
				Cout.println(getClass(), " cMoinhom ");
				scrMain.selectedItem = -1;
				if(Char.myChar().charFocus!=null)
				Service.gI().inviteParty((short)Char.myChar().charFocus.charID, (byte)0);
				GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
				break;
			case cGiaodich:
				Cout.println(getClass(), " cGiaodich ");
				scrMain.selectedItem = -1;
				Char.myChar().partnerTrade = null;
				if(Char.myChar().charFocus!=null)
				Service.gI().inviteTrade((byte)0, (short)Char.myChar().charFocus.charID);
				GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
				break;
			case cThachdau:
				if(Char.myChar().charFocus!=null)
				Service.gI().ThachDau((byte)0, (short)Char.myChar().charFocus.charID);
				scrMain.selectedItem = -1;
				GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
//				Service.gI().
				break;
			case cMoiVaoBang:
				Cout.println2222(" moivao bang ");
				if(Char.myChar().charFocus!=null&&ClanScreen.gI().isBoss)
				Service.gI().Clan_MoiBang((short)Char.myChar().charFocus.charID);
				scrMain.selectedItem = -1;
				GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
				break;
			case cMoiVaoBangGobal:
				Cout.println2222(" moivao bang ");
				if(Char.myChar().charFocus!=null&&ClanScreen.gI().isBossGobal)
				Service.gI().Clan_MoiBang_GOBAL((short)Char.myChar().charFocus.charID);
				scrMain.selectedItem = -1;
				GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
				break;
			}
		}
	}
	public void UpdateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.isTouch) {
			   ScrollResult r = scrMain.updateKey();
			   if (r.isDowning || r.isFinish) {
				   
			    indexRow = r.selected;
			   }
		}

	}
	public void Paint(mGraphics g)
	{
		GameScreen.resetTranslate(g);

        g.setColor(0xff000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();
		com.sakura.thelastlegend.domain.model.Paint.paintFrameNaruto(x, y, popw, poph, g);
		
		g.drawImage(LoadImageInterface.charPic,xM,y+poph/3-22,g.VCENTER|g.HCENTER);
		try {
			Part ph = GameScreen.parts[Char.myChar().charFocus.head];
			SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id, 
					xM-1,y+poph/3-24, 0, mGraphics.VCENTER|mGraphics.HCENTER);
		} catch (Exception e) {
			// TODO: handle exception
		}
//		g.drawImage(Char.myChar().charFocus.,xM-3,y+poph/3-24,g.VCENTER|g.HCENTER);
		
		g.drawImage(LoadImageInterface.imgName,xM,y+poph/3+8,g.VCENTER|g.HCENTER);

		if(Char.myChar().charFocus!=null&&Char.myChar().charFocus.cName!=null)
		mFont.tahoma_7b_white.drawString(g,Char.myChar().charFocus.cName, xM,y+poph/3+3,g.VCENTER);


		com.sakura.thelastlegend.domain.model.Paint.PaintBoxName("Giao Tiếp", xM-40, y, 80, g);
		if(scrMain!=null)
		{
			scrMain.setStyle(listName.length,30,x, y+80,popw ,100, true,0);
			scrMain.setClip(g);
		}
		
		if(indexRow>=0)
		{
			g.setColor(0x964210);
			g.fillRect(x+10,y+87+30*indexRow,popw-20,20);
		}
		PaintList(g,listName,x,y+80,popw);
		GameCanvas.resetTrans(g);
	}
	public void SetPosClose(Command cmdClose) {
		// TODO Auto-generated method stub
		cmdClose.setPos(x+popw-Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
		
	}

}
