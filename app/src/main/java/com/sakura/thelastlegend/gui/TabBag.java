package com.sakura.thelastlegend.gui;




import java.util.ArrayList;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.JSONParser;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.real.Service;
import Objectgame.Char;
import Objectgame.Item;
import Objectgame.ItemPayment;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

/*
 * this class inventory tab
 */
public class TabBag extends MainTabNew{ //HANG TRANG
	
	int maxw, maxh, indexPaint = 12, winfo = 140;
	int numW = 5, numH = 6, numHPaint, maxSize = 60;
//	int[] xItem = new int[Char.myChar().arrItemBag.length], yItem = new int[Char.myChar().arrItemBag.length];
	int[] xItem = new int[9], yItem = new int[9];
	public ListNew listContent = null;
	ListNew list;
	int idSelect = 0;
	Command cmdSelect,cmdSale;//use
	Command cmdXoaItem;//drop item
	int hcmd = 0;
	public int coutFc;
	mBitmap imgCoins1,imgCoins2;
	Scroll listItemPayment = new Scroll();

	Command cmdopenPayment,cmdSendPayment,cmdClosePayment;//use
	public static boolean isPayment,isShowPayment=true;
	boolean[] isTypeTheCao = new boolean[]{true,false,false,false,false,false,false,false,false};
	public int x_payment,y_payment,w_payment,h_payment;
	TField tf_mathecao = new TField();
	TField tf_soseri = new TField();
	public String[] text_info;
	public int[][] xypaint_text = new int[10][];
	public static ArrayList<ItemPayment> list_item = new ArrayList<ItemPayment>();

	public TabBag(String name)
	{
//		if (GameCanvas.isTouch)
//			idSelect = -1;
//		else
//			idSelect = 0;
		text_info = new String[]{"Mã thẻ:","Số serial:","Viettel","Mobile","Vinaphone","Vn mobile","Zing","Vcoin","Bit","Gate"};
		idSelect = -1;
		typeTab=INVENTORY;
		this.nameTab=name;
		xBegin = super.xTab + wOneItem/2 +4;
		yBegin = super.yTab + 30 + wOneItem - 5;
		maxw = (wblack - 8) / 32;
		maxh = (hblack - 8) / 32;
		cmdSelect = new Command("Sử dụng", this, Constants.BTN_USE_ITEM,null, 0,0);
		cmdSelect.setPos(xBegin-widthSubFrame-21, yBegin+heightSubFrame*2/3-10, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		cmdSale = new Command("Bán ", this, Constants.BTN_SALE_ITEM,null, 0,0);
		cmdSale.setPos(xBegin-widthSubFrame/2-26-Image.getWidth(LoadImageInterface.img_use_focus)/2,
				yBegin+heightSubFrame*2/3-14-Image.getHeight(LoadImageInterface.img_use_focus), LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		
		cmdXoaItem = new Command("Vứt bỏ", this, 36,null, 0,0);
		cmdXoaItem.setPos(xBegin+Image.getWidth(LoadImageInterface.img_use)-widthSubFrame-13, yBegin+heightSubFrame*2/3-10, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		
		cmdopenPayment = new Command("", this, 40,null, 0,0);
		cmdopenPayment.setPos(xBegin+widthSubFrame-10, 
				yBegin-18, LoadImageInterface.bt_Plus, LoadImageInterface.bt_Plus);
		
		
		
		init();

		w_payment = 240;
		h_payment = 200;

		x_payment = GameCanvas.w/2-w_payment/2;
		y_payment = GameCanvas.h/2-h_payment/2-10;
		
		cmdSendPayment = new Command("Send ", this, 41,null, 0,0);
		cmdSendPayment.setPos(x_payment+w_payment/2- LoadImageInterface.img_use.getWidth()/2,
				y_payment+h_payment- LoadImageInterface.img_use.getHeight()/2, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		
		cmdClosePayment = new Command("", this, 42,null, 0,0);
		cmdClosePayment.setPos(x_payment+w_payment- LoadImageInterface.closeTab.getWidth()/2,
				y_payment- LoadImageInterface.closeTab.getHeight()/2,
				LoadImageInterface.closeTab , LoadImageInterface.closeTab);
		
		
		tf_mathecao.x = x_payment+ w_payment/2-60;
		tf_mathecao.y = y_payment+ 40;
		tf_mathecao.width = 120;
		tf_mathecao.height = ITEM_HEIGHT + 2;
		tf_mathecao.isFocus = false;
		tf_mathecao.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);
		

		tf_soseri.x = x_payment+ w_payment/2-60;
		tf_soseri.y = y_payment+ 60;
		tf_soseri.width = 120;
		tf_soseri.height = ITEM_HEIGHT + 2;
		tf_soseri.isFocus = false;
		tf_soseri.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);
		
		for (int i = 0; i < xypaint_text.length; i++) {
			xypaint_text[i] = new int[2];
		}
		xypaint_text[0][0] = x_payment+5;
		xypaint_text[0][1] = tf_mathecao.y+5;
		xypaint_text[1][0] = x_payment+5;
		xypaint_text[1][1] = tf_soseri.y+5;
		
		for (int i = 2; i < 6; i++) {
			xypaint_text[i][0]  = x_payment+(i-1)*(w_payment-20)/5-(w_payment-20)/10+4*(i-1)+(i==5?10:0);
			xypaint_text[i][1]  = tf_soseri.y+30;
		}

//		xypaint_text[2][0]  = x_payment+(w_payment-20)/5-(w_payment-20)/10+4;
//		xypaint_text[3][0]  = x_payment+2*(w_payment-20)/5-(w_payment-20)/10+6;
//		xypaint_text[4][0]  = x_payment+3*(w_payment-20)/5-(w_payment-20)/10+10;
//		xypaint_text[5][0]  = x_payment+4*(w_payment-20)/5-(w_payment-20)/10+14;
		
		for (int i = 6; i < xypaint_text.length; i++) {
			xypaint_text[i][0]  =  x_payment+(i-5)*(w_payment-20)/5-(w_payment-20)/10+4*(i-5)+(i==9?10:0);
			xypaint_text[i][1]  = tf_soseri.y+50;
		}
		Service.gI().Payment_GetInfo();
	}
	private void LoadImage()
	{
		if(list_item==null||list_item.size()==0)
			list_item = JSONParser.parseItemPayment("");
		imgCoins1=GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
		imgCoins2=GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
	}
	int cItem = 0, rItem = 0;
	public void paint(mGraphics g) 
	{
		
		g.setColor(color[1]);
		int dem = 0;
		
		//money
		g.drawImage(imgCoins1, xBegin-7,yBegin-Image.getHeight(imgCoins1)-1,0);
		mFont.tahoma_7b_white.drawString(g, Char.myChar().xu+"",
				xBegin+18,yBegin-Image.getHeight(imgCoins1)/2-4, 0);
		
		//gold
		g.drawImage(imgCoins2, xBegin+(Image.getWidth(LoadImageInterface.ImgItem))*5+9,yBegin-Image.getHeight(imgCoins2)-1,g.TOP|g.RIGHT);
		mFont.tahoma_7b_white.drawString(g,Char.myChar().luong+"",
				xBegin+(Image.getWidth(LoadImageInterface.ImgItem)+2)*5-Image.getWidth(imgCoins1)+34,yBegin-Image.getHeight(imgCoins1)/2-4,0);
		//paint image item
		for(int i=0;i<numW;i++)
			for(int j=0;j<numH;j++)
			{
				g.drawImage(LoadImageInterface.ImgItem,xBegin+(Image.getWidth(LoadImageInterface.ImgItem))*i,yBegin+(Image.getHeight(LoadImageInterface.ImgItem))*j+2, 0);
			}
		
		// paint item
		if( Char.myChar().arrItemBag!=null)
		for(int i = 0; i < Char.myChar().arrItemBag.length; i++){	
			dem++;
			if(i>29) return;
			Item it = Char.myChar().arrItemBag[i];
			if(it==null) continue;
			int r = it.indexUI / numW;
			int c = it.indexUI - (r * numW);
		

			if(it != null){
//				it.paintItem(g, xBegin+(Image.getWidth(LoadImageInterface.ImgItem)+2)*c + 13,
//						yBegin+(Image.getHeight(LoadImageInterface.ImgItem)+2)*r + 13);
//				System.out.println(" LOCAT ----> "+(((xBegin)+(Image.getWidth(LoadImageInterface.ImgItem)+2)*c + 13)));
				it.paintItem(g, xBegin+(Image.getWidth(LoadImageInterface.ImgItem))*c + 14,
						(yBegin+(Image.getHeight(LoadImageInterface.ImgItem))*r + 14) + 2);
			}
				
			
			
//			if (!GameCanvas.menu2.isShowMenu && GameCanvas.currentDialog == null) {
				if (Focus == INFO && timePaintInfo > timeRequest) {
////					GameCanvas.menu2.isShowMenu = true;
					
					//paintNameItem(g, xTab-widthSubFrame ,yTab-30+ GameCanvas.h / 5, longwidth, name, colorName);
//					paintPopupContent(g, false);
					
				}
//			}
		}
		if (idSelect > -1 && Focus == INFO) {//focus
			setPaintInfo();
			Paint.paintFocus(g,
					 (xBegin + ((idSelect % numW)*(Image.getWidth(LoadImageInterface.ImgItem))))+11- LoadImageInterface.ImgItem.getWidth()/4,
					 yBegin+ (idSelect / numW) * (Image.getHeight(LoadImageInterface.ImgItem) ) +13- LoadImageInterface.ImgItem.getWidth()/4
					, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
//			if(GameCanvas.gameTick%10==0)
//				g.drawImage(LoadImageInterface.imgFocusSelectItem0, (xBegin + ((idSelect % numW)*(Image.getWidth(LoadImageInterface.ImgItem))))+14 ,
//			yBegin
//						+ (idSelect / numW) * (Image.getHeight(LoadImageInterface.ImgItem) ) +16, mGraphics.VCENTER|mGraphics.HCENTER);
//			else
//				g.drawImage(LoadImageInterface.imgFocusSelectItem1, (xBegin + ((idSelect % numW)*(Image.getWidth(LoadImageInterface.ImgItem))))+14 , yBegin
//						+ (idSelect / numW) * (Image.getHeight(LoadImageInterface.ImgItem) ) +16, mGraphics.VCENTER|mGraphics.HCENTER);
//			Paint.SubFrame(xTab-widthSubFrame,yTab,widthSubFrame,heightSubFrame, g);
			paint.paintItemInfo(g, itemFocus, xTab-widthSubFrame-5,yTab+10);
			cmdSelect.paint(g);
			cmdXoaItem.paint(g);
			cmdSale.paint(g);
//			paint.paintItemInfo(g, itemFocus, xTab-widthSubFrame + 10,yTab-25+ GameCanvas.h / 5);
//			paintNameItem(g, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 20, longwidth, name, colorName);
//			paintNameItem(g, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 30, longwidth, "Tấn công: 1000", colorName);
//			paintNameItem(g, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 40, longwidth, "Độ bền: 10", colorName);
//			paintNameItem(g, xTab-widthSubFrame + 20,(yTab-30+ GameCanvas.h / 5) + 50, longwidth, "tắng sát thương vật lý 10%", colorName);
//			paintPopupContent(g, false);
		}

//		paintRect(g);

		if(isPayment)
		{
			paintPayment(g);
		}
		else 
			cmdopenPayment.paint(g);
	}
	
	
	public Item getItem(Item[] item){
		for(int i = 0; i < item.length; i++){
			Item it = item[i];
			return it;
		}
		return null;
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case Constants.BTN_USE_ITEM:
			if(Char.myChar().arrItemBag!=null&&idSelect>-1&&idSelect<Char.myChar().arrItemBag.length)
				Service.gI().useItem((byte)/*Char.myChar().arrItemBag[*/idSelect/*].itemId*/, Char.myChar().arrItemBag[idSelect]);
			idSelect = -1;
//			Service.gI().giveUpItem((byte)idSelect);
			break;
		case Constants.BTN_SALE_ITEM:
			if(Char.myChar().arrItemBag!=null&&idSelect>-1&&idSelect<Char.myChar().arrItemBag.length
			&&Char.myChar().arrItemBag[idSelect].template!=null)
			GameCanvas.startYesNoDlg("Bạn có muốn bán "+Char.myChar().arrItemBag[idSelect].template.name+" không?",
					new Command("Có", this,37,null, 0,0),
					new Command("Không", GameCanvas.instance, GameCanvas.cEndDgl,null, 0,0));
			
			break;
		case 36:

			Cout.println("hoi vuetttttttttttt ");
			if(Char.myChar().arrItemBag!=null&&idSelect>-1&&idSelect<Char.myChar().arrItemBag.length
			&&Char.myChar().arrItemBag[idSelect].template!=null)
			GameCanvas.startYesNoDlg("Bạn có muốn vứt bỏ "+Char.myChar().arrItemBag[idSelect].template.name+" không?",
					new Command("Có", this, Constants.BTN_DROP_ITEM,null, 0,0),
					new Command("Không", GameCanvas.instance, GameCanvas.cEndDgl,null, 0,0));
			break;
		case Constants.BTN_DROP_ITEM:
			Cout.println("ok vuetttttttttttt ");
//			Service.gI().useItem((byte)idSelect, Char.myChar().arrItemBag[idSelect]);
			if(Char.myChar().arrItemBag!=null&&idSelect>-1&&idSelect<Char.myChar().arrItemBag.length
					&&Char.myChar().arrItemBag[idSelect].template!=null)
			Service.gI().giveUpItem((byte)idSelect);
			GameCanvas.endDlg();
			idSelect = -1;
			break;
		case 37: //ok ban item
			Cout.println("ok vuetttttttttttt idSelect "+idSelect);
			if(Char.myChar().arrItemBag!=null&&idSelect>-1&&idSelect<Char.myChar().arrItemBag.length
					&&Char.myChar().arrItemBag[idSelect].template!=null)
				
			Service.gI().SellItem((byte)idSelect);
			GameCanvas.endDlg();
			idSelect = -1;
			break;
		case 40:

			if(GameCanvas.isStore) return;
			if(isShowPayment)
			{
				isPayment = !isPayment;
				isPaintCmdClose = !isPaintCmdClose;
			}
			else {
				isPayment = false;
				isPaintCmdClose = true;
			}
			break;
		case 41:
			byte indexthecao = 0;
			for (int i = 0; i < isTypeTheCao.length; i++) {
				if(isTypeTheCao[i])
				{
					indexthecao = (byte)i;
					break;
				}
			}
			if(tf_mathecao.getText().trim().length()>0&&tf_soseri.getText().trim().length()>0){
				Service.gI().Payment_Sendcode(tf_mathecao.getText(), tf_soseri.getText(), (byte)indexthecao);
				tf_mathecao.clear();
				tf_soseri.clear();
				GameCanvas.StartDglThongBao("Đã gửi mã thẻ vui lòng chờ giây lát...");
			}
			else GameCanvas.StartDglThongBao("Vui lòng nhập đầy đủ thông tin !!!");
			break;
		case 42:
			isPayment = false;
			isPaintCmdClose = true;
			break;
		default:
			break;
		}
	}
	public void updatePointer() {
		if(imgCoins1==null){
			LoadImage();
		}
		listItemPayment.updateKey();
		listItemPayment.updatecm();
		if(GameCanvas.gameTick%4 ==0){
			coutFc++;
			if(coutFc>2)
				coutFc =0;
		}
		if (!isPayment&&GameCanvas.isPointerJustRelease&&GameCanvas.isPointSelect(xBegin, yBegin, numW * (Image.getWidth(LoadImageInterface.ImgItem) + 2),
				numH * (Image.getHeight(LoadImageInterface.ImgItem) + 4))) {
			GameCanvas.isPointerJustRelease = false;
			int row=(GameCanvas.px - xBegin)/ (Image.getWidth(LoadImageInterface.ImgItem));
			int col=(GameCanvas.py - yBegin) / (Image.getWidth(LoadImageInterface.ImgItem));
			
			 row=(GameCanvas.px - xBegin-(row-1)*2)/ (Image.getWidth(LoadImageInterface.ImgItem));
			 col=(GameCanvas.py - yBegin-(col-1)*2) / (Image.getWidth(LoadImageInterface.ImgItem));
			
			
			if(row>6)
				row=6;
			if(col>5)
				col=5;
			
			int select=row+col*numW;
			
			int size = 0;
			if (typeTab == INVENTORY&&Char.myChar().arrItemBag!=null){
				size = Char.myChar().arrItemBag.length;
				
			}
			Cout.println(idSelect+"  select  "+select);
			if (select >= 0 && select < size) {
				GameCanvas.isPointerClick = false;
				if (select == idSelect) {

						
				} else {
					timePaintInfo = 0;
					idSelect = select;
					listContent = null;
				}
				if (MainTabNew.Focus != MainTabNew.INFO)
					MainTabNew.Focus = MainTabNew.INFO;
			} else {
				
				idSelect = -1;
				Cout.println("------------- set - 1");
				timePaintInfo = 0;
				listContent = null;
			}
			
		}
		
//		if (timePaintInfo == MainTabNew.timeRequest) {
//			setPaintInfo();
//		}
	}
	
	public void updateKey() {
		// TODO Auto-generated method stub
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdSelect)) {
			if (cmdSelect != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdSelect != null)
					cmdSelect.performAction();
			}
		}
		
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdXoaItem)) {
			if (cmdXoaItem != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdXoaItem != null)
					cmdXoaItem.performAction();
			}
		}
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdSale)) {
			if (cmdSale != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdSale != null)
					cmdSale.performAction();
			}
		}
		if (!isPayment&&Screen.getCmdPointerLast(cmdopenPayment)) {
			if (cmdopenPayment != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdopenPayment != null)
					cmdopenPayment.performAction();
			}
		}
		
		if(isPayment)
		{

			if(tf_mathecao!=null)
				tf_mathecao.update();
			if(tf_soseri!=null)
				tf_soseri.update();
			updateKey_payment();
			if (Screen.getCmdPointerLast(cmdClosePayment)) {
				if (cmdClosePayment != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdClosePayment != null)
						cmdClosePayment.performAction();
				}
			}
			if (Screen.getCmdPointerLast(cmdSendPayment)) {
				if (cmdSendPayment != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdSendPayment != null)
						cmdSendPayment.performAction();
				}
			}
		}
		
	}
	Item itemFocus = null;
	public void setPaintInfo() {
		if(idSelect<Char.myChar().arrItemBag.length){
			Item item = Char.myChar().arrItemBag[idSelect];
			itemFocus = Char.myChar().arrItemBag[idSelect];
			if(item.template!=null)
			name = item.template.name;
		}

	}
	public void paintPayment(mGraphics g)
	{
		g.setColor(0xff000000,GameCanvas.opacityTab);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		Paint.paintFrameNaruto(x_payment,y_payment,w_payment,h_payment+2,g);
		Paint.PaintBoxName("Nạp gold",x_payment+w_payment/2-40,y_payment,80,g);
		tf_mathecao.paint(g);
		tf_soseri.paint(g);
		cmdSendPayment.paint(g);
		cmdClosePayment.paint(g);
		for (int i = 0; i < text_info.length; i++) {
			mFont.tahoma_7.drawString(g, text_info[i], xypaint_text[i][0]+ LoadImageInterface.imgCheckSetting.getWidth()/2+2, xypaint_text[i][1], 0);
			if(i>1&&i-2<isTypeTheCao.length)
			g.drawRegion(LoadImageInterface.imgCheckSetting, 0, (isTypeTheCao[i-2]==true?1:0)* LoadImageInterface.imgCheckSetting.getHeight()/2,
					LoadImageInterface.imgCheckSetting.getWidth(), LoadImageInterface.imgCheckSetting.getHeight()/2,
					0,xypaint_text[i][0], xypaint_text[i][1]+4, mGraphics.VCENTER|mGraphics.HCENTER);
		}
		if(list_item!=null&&list_item.size()>0)
		{
			listItemPayment.setStyle(list_item.size(),15 ,x_payment+5,xypaint_text[xypaint_text.length-1][1]+20 , w_payment-10, h_payment-(xypaint_text[xypaint_text.length-1][1]+20 ), true, 1);
			listItemPayment.setClip(g,x_payment+5,xypaint_text[xypaint_text.length-1][1]+20 , w_payment-10, h_payment-(xypaint_text[xypaint_text.length-1][1]+20 ));
			for (int i = 0; i < list_item.size(); i++) {
				ItemPayment item = list_item.get(i);
				if(item!=null){
					mFont.tahoma_7.drawString(g, "Mệnh giá "+item.getRealMoneyAmount()+ " ("+item.getRealMoneyUnit()+")",
							x_payment+15, xypaint_text[xypaint_text.length-1][1]+15*i+25, 0);
					mFont.tahoma_7.drawString(g, item.getGameMoneyAmount()+ " ("+item.getGameMoneyUnit()+")",
							x_payment+w_payment-10, xypaint_text[xypaint_text.length-1][1]+15*i+25, 1);
					
				}
			}
			GameCanvas.resetTrans(g);
		}
	}
	public void updateKey_payment()
	{
		int indexTypethe = -1;
		for (int i = 2; i < text_info.length; i++) {
			if(GameCanvas.isPointerJustRelease&&GameCanvas.isPointer(
					xypaint_text[i][0]- LoadImageInterface.imgCheckSetting.getWidth()/2,
					xypaint_text[i][1]- LoadImageInterface.imgCheckSetting.getWidth()/2,
					LoadImageInterface.imgCheckSetting.getWidth(), LoadImageInterface.imgCheckSetting.getWidth()))
			{
				indexTypethe = i-2;
				break;
			}
		}
		if(indexTypethe>-1&&indexTypethe<isTypeTheCao.length){
			for (int i = 0; i < isTypeTheCao.length; i++) {
				isTypeTheCao[i] = false;
			}
			isTypeTheCao[indexTypethe] = true;
		}
	}
}
