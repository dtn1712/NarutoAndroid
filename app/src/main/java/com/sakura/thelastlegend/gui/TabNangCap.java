package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.real.Service;
import Objectgame.Char;
import Objectgame.Item;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

public class TabNangCap  extends MainTabNew{ //trang bi
	int numW, numH,coutFc;
	public static int maxSize = 12;
	int h12, w5;
	public static int delta = 0;
	int idSelect;
	// info
	int xStart, yStart;
	int[] mColorInfo;
	// list item
	public static String[] meffskill = new String[5];
	public int[][] xyItemNangCap = new int[14][];
	public Item[] listItemNangCap = new Item[9];
	boolean isList = false;
	int maxList, selectList, xList, yList, timeUpdateInfo;
	boolean isShowInfo = false;
	private mBitmap[] list_eff = new mBitmap[12];
	
	private mBitmap imgButton_dressed,imgButton_dressed2,imgBgInfo,imgEff_dapdo,
	imgChien_luc,imgCoins1,imgCoins2,imgExp_tube,imgExp,imgLevel;
	
	int xBGHuman=xTab+widthFrame/6-10;
	int yBGHuman=yTab+ GameCanvas.h /6 -10;

	int wsize;
	int indexFocusListItemNangCap = -1;
	int widthBGChar=130;
	int heightBGChar=100;
	Scroll scroll_listItem = new Scroll();
	Command dropAllItem,shop,cmdGo,cmdNangCap,cmdDuaLen;
	boolean isDuaLen = false,isShowCmd = true;
	
	 public static final byte SUB_UPGRADE = 0;
	 public static final byte SUB_TAKE_OFF = 1;
	 public static final byte SUB_TAKE_ON = 2;
	 public static final byte SUB_UPDATE_INFO_UPGRADE = 3;  
	 public static final byte SUB_UPGRADE_SUCCESS = 4;  
	 public static final byte SUB_UPGRADE_FAIL = 5;
	
	public TabNangCap(String nametab) {
		xBGHuman=xTab+16;
//		LoadImage();
		if (GameCanvas.isTouch)
			idSelect = -1;
		else
			idSelect = 0;
		
		for (int i = 0; i < xyItemNangCap.length; i++) {
			xyItemNangCap[i] = new int[2];
		}
		xStart = xBegin + wblack / 2 - (numW * wOneItem) / 2 +(numW / 2);
		yStart = yBegin +10;

		xBegin = super.xTab + wOneItem/2 +4;
		yBegin = super.yTab + 30 + wOneItem - 5;
		wsize = wOneItem;
		typeTab = EQUIP;
		yBGHuman=yTab+15;
		this.nameTab = nametab;
		int xCmd=xBGHuman+widthBGChar+32;
		int yCmd=yBGHuman+27;
		
		dropAllItem= new Command("", this, Constants.BTN_USE_ITEM,null, 0,0);
		dropAllItem.setPos(xCmd, yCmd,imgButton_dressed,imgButton_dressed2);
		
		yCmd=yCmd+30;
		shop= new Command("", this, Constants.BTN_USE_ITEM,null, 0,0);
		shop.setPos(xCmd, yCmd,imgButton_dressed,imgButton_dressed2);
		
		yCmd=yCmd+30;
		cmdGo= new Command("Gỡ", this, 9,null, 0,0);
		cmdGo.setPos(xBegin+Image.getWidth(LoadImageInterface.img_use)-widthSubFrame-13, yBegin+heightSubFrame*2/3-10, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		cmdNangCap = new Command("Nâng cấp", this, 10,null, 0,0);
		cmdNangCap.setPos(xBegin-widthSubFrame-21, yBegin+heightSubFrame*2/3-10, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		
		cmdDuaLen = new Command("Đưa lên", this, 11,null, 0,0);
		cmdDuaLen.setPos(xBegin-widthSubFrame/2-26-Image.getWidth(LoadImageInterface.img_use_focus)/2, yBegin+heightSubFrame*2/3-10, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		
		
		
		xyItemNangCap[0][0] = xBGHuman-16+65+25;//xBGHuman-16,yBGHuman
		xyItemNangCap[0][1] = yBGHuman+46+21;
		
		xyItemNangCap[1][0] = xBGHuman+10;//top left
		xyItemNangCap[1][1] = yBGHuman+25;
		xyItemNangCap[2][0] = xBGHuman-16+65+25;//xBGHuman-16,yBGHuman
		xyItemNangCap[2][1] = yBGHuman+25;
		xyItemNangCap[3][0] = xBGHuman+138;//top right
		xyItemNangCap[3][1] = yBGHuman+25;
		xyItemNangCap[4][0] = xBGHuman+138;//xBGHuman-16,yBGHuman
		xyItemNangCap[4][1] = yBGHuman+68;
		xyItemNangCap[5][0] = xBGHuman+138;//bottom right
		xyItemNangCap[5][1] = yBGHuman+114;
		xyItemNangCap[6][0] = xBGHuman+74;//
		xyItemNangCap[6][1] = yBGHuman+114;
		xyItemNangCap[7][0] = xBGHuman+10;//bottom left
		xyItemNangCap[7][1] = yBGHuman+114;
		xyItemNangCap[8][0] = xBGHuman+10;//
		xyItemNangCap[8][1] = yBGHuman+68;
		
		
		
		// w276
		//136
		//232
	}
	public void init() {
		// TODO Auto-generated method stub
//		if (GameCanvas.isTouch)
//			idSelect = -1;
//		else
//			idSelect = 0;
//		isList = false;
	}
	public void LoadImage()
	{
		for (int i = 0; i < list_eff.length; i++) {
			list_eff[i] = GameCanvas.loadImage("/eff/eff_dapdo_"+(i+1)+".png");
		}
		imgEff_dapdo  = GameCanvas.loadImage("/eff/nen_eff.png");
		imgBgInfo = GameCanvas.loadImage("/GuiNaruto/imageBGChar/bg_info.png");
		imgButton_dressed=GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed.png");
		imgButton_dressed2=GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed2.png");;
		imgChien_luc=GameCanvas.loadImage("/GuiNaruto/myseft/chien_luc.png");
		imgCoins1=GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
		imgCoins2=GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
		imgExp_tube=GameCanvas.loadImage("/GuiNaruto/myseft/exp_tube.png");
		imgExp=GameCanvas.loadImage("/GuiNaruto/myseft/exp.png");
		imgLevel=GameCanvas.loadImage("/GuiNaruto/myseft/level.png");
		
	}
	int mamau = 0xefec14;
	//paint bg human
	public int[] mang_eff = new int[]{0,1,2,3,4,3,2,1};
	private void PaintBGHuman(int x,int y,mGraphics g)
	{
		g.drawImage(imgBgInfo, x+13*7 , y+70, mGraphics.VCENTER|mGraphics.HCENTER);
		//		int wImage=Image.getWidth(arrayBGHuman[0]);
//		int hImage=Image.getHeight(arrayBGHuman[0]);
//		int index=0;
//		for(int i=1;i<6;i++)
//			for(int j=1;j<6;j++)
//			{
//				g.drawImage(arrayBGHuman[index],x+j*wImage,y+i*hImage,0,true);
//				index++;
//			}
	}
	
	//paint exp
	void PaintExp(int x,int y ,mGraphics g)
	{
		int Exppaint = (int)((Char.myChar().cEXP*mGraphics.getImageHeight(imgExp))/Char.myChar().cMaxEXP);
		Exppaint = (Exppaint<7&&Exppaint!=0?7:Exppaint);
		g.drawRegion(imgExp,mGraphics.getImageHeight(imgExp)-Exppaint,0,Image.getWidth(imgExp),
				Exppaint,
				0,x+3,y+2+(mGraphics.getImageHeight(imgExp)-Exppaint),mGraphics.TOP|mGraphics.LEFT);
		g.drawImage(imgExp_tube,x,y,0);
		g.drawImage(imgLevel,x+Image.getWidth(imgExp)/2+2,y+Image.getHeight(imgExp)+10,g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g,Char.myChar().clevel+"",x+Image.getWidth(imgExp)/2-1,y+Image.getHeight(imgExp)+5,g.VCENTER|g.HCENTER);
	}
	
	void PaintItem(int x,int y ,mGraphics g)
	{
		if(Char.myChar().arrItemBag==null) return;
		scroll_listItem.setStyle( Char.myChar().arrItemBag.length/5+ (Char.myChar().arrItemBag.length%5!=0?1:0), 28, x, y, 140, 56, true, 5);
		scroll_listItem.setClip(g,x, y, 140, 56);
		// paint item
				if( Char.myChar().arrItemBag!=null)
				for(int i = 0; i < 30; i++){	
					int r = i / 5;
					int c = i - (r * 5);
					g.drawImage(LoadImageInterface.ImgItem,
							 x+(Image.getWidth(LoadImageInterface.ImgItem))*c + 14,
							 y+(Image.getHeight(LoadImageInterface.ImgItem))*r + 14 + 2,
							 mGraphics.VCENTER|mGraphics.HCENTER,true);

					if(i>29||i>=Char.myChar().arrItemBag.length||Char.myChar().arrItemBag[i]==null) continue;
					Item it = Char.myChar().arrItemBag[i];
					r = it.indexUI / 5;
					c = it.indexUI - (r * 5);
					if(it != null){
						it.paintItem(g, x+(Image.getWidth(LoadImageInterface.ImgItem))*c + 14,
								(y+(Image.getHeight(LoadImageInterface.ImgItem))*r + 14) + 2);
					}
				}

				if(scroll_listItem.selectedItem>=0&&scroll_listItem.selectedItem<Char.myChar().arrItemBag.length){
					Item it = Char.myChar().arrItemBag[scroll_listItem.selectedItem];
					int r = it.indexUI / 5;
					int c = it.indexUI - (r * 5);
					Paint.paintFocus(g,
							x+(Image.getWidth(LoadImageInterface.ImgItem))*c + 4,
							(y+(Image.getHeight(LoadImageInterface.ImgItem))*r + 4) + 2
							, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
				}
				GameCanvas.resetTrans(g);
	}
	void PaintStrong(mGraphics g,int x,int y)
	{
		g.drawImage(imgChien_luc,x,y, g.VCENTER|g.HCENTER);
		mFont.tahoma_7b_white.drawString(g, ""+((Char.myChar().diemTN[1]+Char.myChar().diemTN[2])/2),x , y-2,2);
	}
	void PaintGoldMoney(int x,int y ,mGraphics g)
	{
		
	}
	public void paint(mGraphics g)
	{
		
		PaintBGHuman(xBGHuman-16,yBGHuman,g);
		if (idSelect > -1 && Focus == INFO) {//focus
			Paint.paintItemInfo(g, itemFocus, xTab-widthSubFrame-5,yTab+10);
		}
		
//		paintRect(g);
		
		if(itemFocus!=null&&isShowCmd)
		{
			if(isDuaLen){
				cmdDuaLen.paint(g);
			}else {
				
				cmdGo.paint(g);
				cmdNangCap.paint(g);
			}
		}
		paintListItemNangCap(g);
		dropAllItem.paint(g);
		shop.paint(g);
		PaintItem(xBGHuman,yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10,g);
		
	}
	
	public void updatePointer() {
		boolean ismove = false;
		if(GameCanvas.gameTick%4 ==0){
			coutFc++;
			if(coutFc>2)
				coutFc =0;
		}
		if(imgButton_dressed==null)
			LoadImage();
//		if (isList) {
//
//			if (listContent != null) {
//				if (GameCanvas.isPoint(listContent.x, listContent.y,
//						listContent.maxW, listContent.maxH)) {
//					listContent.update_Pos_UP_DOWN();
//					ismove = true;
//				}
//			}
////			if (GameCanvas.isTouch && !ismove) {
////				list.updatePos_LEFT_RIGHT();
////				GameScreen.cameraSub.xCam = list.cmx;
////			}
//			if (GameCanvas.isPointerClick /*&& !ismove*/) {
//				if (GameCanvas.isPoint(xList, yList, (wsize) * maxList,
//						wOneItem)) {
//					byte select = (byte) ((/*GameScreen.cameraSub.xCam*/
//							+ GameCanvas.px - xList) / (wsize));
//					if (select >= 0 && select < vecItemMenu.size()) {
////						if (select == selectList) {
////							cmdChangeEquip.perform();
////						} else {
////							selectList = select;
////							timePaintInfo = 0;
////						}
////						listContent = null;
//					}
//					GameCanvas.isPointerClick = false;
//				} else if (!GameCanvas.isPoint(0, GameCanvas.h
//						- GameCanvas.hCommand, GameCanvas.w,
//						GameCanvas.hCommand)) {
////					cmdCloseChange.perform();
//					GameCanvas.isPointerClick = false;
//				}
//
//			}
//		} else {
//			if (listContent != null) {
//				if (GameCanvas.isPoint(listContent.x, listContent.y,
//						listContent.maxW, listContent.maxH)) {
//					listContent.update_Pos_UP_DOWN();
//					ismove = true;
//				}
//			}
//			if (GameCanvas.isPointSelect(xStart, yStart, (wsize) * numW,
//					(wsize) * numH)
//					&& !ismove) {
//				GameCanvas.isPointerClick = false;
//				byte select = (byte) ((GameCanvas.px - xStart) / (wsize) + ((GameCanvas.py - yStart) / (wsize))
//						* numW);
//				if (select >= 0 && select < maxSize) {
//					if (select == idSelect) {
//						setPaintInfo();
////						cmdChange.perform();
//					} else {
//						idSelect = select;
//						timePaintInfo = 0;
//
//					}
//					listContent = null;
//					if (MainTabNew.Focus != MainTabNew.INFO)
//						MainTabNew.Focus = MainTabNew.INFO;
//				}
//
//			}
//		}
//		int x=xBGHuman;
//		int y=yBGHuman+heightBGChar+Image.getHeight(imgChien_luc)+10;
//		int w=(Image.getWidth(LoadImageInterface.ImgItem))*7;
//		int h=(Image.getHeight(LoadImageInterface.ImgItem))*2;
//		
//		if (GameCanvas.isPointSelect(x,y,w,h))
//		{
//			//khoang cach 2 pixel giua cac o
//			int row=(GameCanvas.px - x)/ (Image.getWidth(LoadImageInterface.ImgItem));
//			int col=(GameCanvas.py - y) / (Image.getHeight(LoadImageInterface.ImgItem));
//			
//			 row=(GameCanvas.px - x-(row-1)*2)/ (Image.getWidth(LoadImageInterface.ImgItem));
//			 col=(GameCanvas.py - y-(col-1)*2) / (Image.getHeight(LoadImageInterface.ImgItem));
//			
//			if(row>6)
//				row=6;
//			if(col>1)
//				col=1;
//			
//			idSelect=(byte) (row+col*5);
//			GameCanvas.clearKeyPressed();
//			GameCanvas.clearKeyHold();
//		}
		for (int i = 0; i < 9; i++) {
			if(GameCanvas.isPointerClick||GameCanvas.isPointerJustRelease)
			{
				if(GameCanvas.isPoint(xyItemNangCap[i][0]-14, xyItemNangCap[i][1]-14, 28, 28))
				{
					if(i<listItemNangCap.length&&listItemNangCap[i]!=null)
						itemFocus = listItemNangCap[i];
					isShowCmd = true;
					indexFocusListItemNangCap = i;
					scroll_listItem.selectedItem = -1;
					GameCanvas.clearPointerEvent();
					break;
				}
			}
		}
		if(itemFocus!=null&&!isDuaLen&&isShowCmd)
		{
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdNangCap)) {
				if (cmdNangCap != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdNangCap != null)
						cmdNangCap.performAction();
				}
			}
		}
		if(cmdDuaLen!=null&&isDuaLen&&isShowCmd)
		{
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdDuaLen)) {
				if (cmdDuaLen != null&&isDuaLen) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdDuaLen != null)
						cmdDuaLen.performAction();
				}
			}
		}
		if(cmdGo!=null&&!isDuaLen&&isShowCmd)
		{
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdGo)) {
				if (cmdDuaLen != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdGo != null)
						cmdGo.performAction();
				}
			}
		}
	}
	
	Item itemFocus = null;
	public void setPaintInfo() {
//		Item item = Char.myChar().arrItemBody[idSelect];
		if(itemFocus!=Char.myChar().arrItemBag[idSelect]){
			itemFocus = Char.myChar().arrItemBag[idSelect];
			isShowCmd = isDuaLen = true;
			if(Item.isHP(itemFocus.template.type)||Item.isBlood(itemFocus.template.type)
					||Item.isMP(itemFocus.template.type))
			{
				isShowCmd = isDuaLen = false;
			}
			else if(itemFocus.isItemDapDo(itemFocus.template.type))
			{
				isShowCmd = isDuaLen = true;
			}
			else {
				if(listItemNangCap[0]!=null){
					isShowCmd = isDuaLen = false;
//					if(itemFocus==listItemNangCap[0]){
//						isShowCmd = true;
//						isDuaLen = false;
//					}

					for (int i = 0; i < listItemNangCap.length; i++) {
						if(listItemNangCap[i]!=null&&listItemNangCap[i]==itemFocus)
						{
							isShowCmd = true;
							isDuaLen = false;
							break;
						}
					}

					Cout.println2222(isDuaLen+" dua lennnnn 2222 "+isShowCmd);
//					else {
//						isShowCmd = false;
//					}
				}else {

					isShowCmd = isDuaLen = true;
					Cout.println2222(isDuaLen+" dua lennnnnnnnnnnnnnn 33333333333 "+isShowCmd);
				}
//				for (int i = 0; i < listItemNangCap.length; i++) {
//					if(itemFocus==listItemNangCap[i]){
//						isDuaLen = false;
//						break;
//					}
//				}
			}
//			if(!isExit&&indexNull<listItemNangCap.length)
//			{
//				listItemNangCap[indexNull] = itemFocus;
//			}

			Cout.println2222(isDuaLen+" dua lennnnn end "+isShowCmd);
		}

//		name = item.template.name;

	}
	@Override
		public void perform(int idAction, Object p) {
			// TODO Auto-generated method stub
		switch (idAction) {
		case 9://go xuong
			if(indexFocusListItemNangCap>=0)
			{
				Service.gI().NangCap_ThaoXuong((byte)indexFocusListItemNangCap);
				indexFocusListItemNangCap = -1;
			}
//			for (int i = 0; i < Char.myChar().arrItemBag.length; i++) {
//				if(Char.myChar().arrItemBag[i]!=null&&Char.myChar().arrItemBag[i]==itemFocus)
//				{
//					Service.gI().NangCap_ThaoXuong((byte)i);
//					break;
//				}
//			}
			for (int i = 0; i < listItemNangCap.length; i++) {
				if(listItemNangCap[i]!=null&&itemFocus==listItemNangCap[i]){
					listItemNangCap[i] = null;
					itemFocus = null;
					isDuaLen = true;
					break;
				}
			}
			break;
		case 10:
//			if(itemFocus!=null&&idSelect>-1&&idSelect<Char.myChar().arrItemBag.length)
//			Service.gI().GoItemTrangBi((byte)idSelect);
			break;
		case 11:
			int indexNull = 9; 
			boolean isExit = false;
			if(listItemNangCap[0]==null&&
					Item.isItemDapDo(itemFocus.template.type))
			{
				GameCanvas.startOKDlg("Bạn cần bỏ đồ cần nâng cấp lên trước !!");
				return;
			}
			int itemNangCap = listItemNangCap[0]==null?0:1;
			for (int i = itemNangCap; i < listItemNangCap.length; i++) {
				if(listItemNangCap[i]==null&&i<indexNull)
					indexNull = i;
				if(itemFocus==listItemNangCap[i]){
					listItemNangCap[i] = null;
					isExit = true;
					break;
				}
			}
			if(!isExit&&indexNull<listItemNangCap.length)
			{
				listItemNangCap[indexNull] = itemFocus;
				for (int i = 0; i < Char.myChar().arrItemBag.length; i++) {
					if(Char.myChar().arrItemBag[i]!=null&&Char.myChar().arrItemBag[i]==itemFocus)
					{
						Service.gI().NangCap_Dualen((byte)i);
						break;
					}
				}
				isDuaLen = false;
			}
			break;
		default:
			break;
		}
			super.perform(idAction, p);
		}
	public void paintListItemNangCap(mGraphics g)
	{
		for (int i = 0; i < 9; i++) {
			g.drawImage(LoadImageInterface.ImgItem,xyItemNangCap[i][0],xyItemNangCap[i][1], mGraphics.VCENTER|mGraphics.HCENTER,true);
			
			if(i<listItemNangCap.length&&listItemNangCap[i]!=null)
				listItemNangCap[i].paintItem(g, xyItemNangCap[i][0], xyItemNangCap[i][1],false);
			if(indexFocusListItemNangCap==i)
			{
				Paint.paintFocus(g,
						xyItemNangCap[i][0]-10,xyItemNangCap[i][1]-10
						, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
			
			}
		}
		
		if(listItemNangCap[0]!=null&&idSelect>-1&&idSelect<Char.myChar().arrItemBag.length){
			int x = xBGHuman-16;
			int y = yBGHuman;
			g.drawRegionRotate(imgEff_dapdo, 0, 0, imgEff_dapdo.getWidth(), imgEff_dapdo.getHeight(), 0,
					 x+13*7 , y+70, mGraphics.VCENTER|mGraphics.HCENTER,(GameCanvas.gameTick*3)%360);
			if(indexFocusListItemNangCap==0)
			{
				Paint.paintFocus(g,
						xyItemNangCap[0][0]-10,xyItemNangCap[0][1]-10
						, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
			
			}
			if(GameCanvas.gameTick%3!=0)
				g.drawImage(list_eff[(GameCanvas.gameTick/2)%list_eff.length], x+13*7 , y+65,  mGraphics.VCENTER|mGraphics.HCENTER);
			if(listItemNangCap[0]!=null)
				listItemNangCap[0].paintItem(g, xyItemNangCap[0][0],xyItemNangCap[0][1],false);
			
			if(GameCanvas.gameTick%3==0)
				g.drawImage(list_eff[(GameCanvas.gameTick/2)%list_eff.length], x+13*7 , y+65,  mGraphics.VCENTER|mGraphics.HCENTER);
				
		}
	}
	@Override
		public void updateKey() {
			// TODO Auto-generated method stub
			super.updateKey();

			ScrollResult s1 = scroll_listItem.updateKey();
			scroll_listItem.updatecm();

			if(Char.myChar().arrItemBag!=null&&scroll_listItem.selectedItem>-1&&scroll_listItem.selectedItem<Char.myChar().arrItemBag.length)
			{
				indexFocusListItemNangCap = -1;
				idSelect = scroll_listItem.selectedItem;
				setPaintInfo();

			}
//			int random = CRes.random(10, 25);
//			for (int i = 1; i < listItemNangCap.length; i++) {
//				if(listItemNangCap[i]!=null&&(GameCanvas.gameTick/8)%(random+i)==0)
//				{
//					GameScreen.addEffectEnd(EffectKill.EFF_CUONG_HOA,
//							xyItemNangCap[i][0], xyItemNangCap[i][1],
//							xyItemNangCap[0][0], xyItemNangCap[0][1]);
//				}
//			}
		}
	public void updateListItemNangCap(short[] listId)
	{
		for (int i = 0; i < listItemNangCap.length; i++) {
			listItemNangCap[i] = null;
		}
		for (int i = 0; i < listId.length; i++) {
			if(listId[i]<Char.myChar().arrItemBag.length&&Char.myChar().arrItemBag[listId[i]]!=null)
			{
				listItemNangCap[i] = Char.myChar().arrItemBag[listId[i]];
			}
		}
	}
}