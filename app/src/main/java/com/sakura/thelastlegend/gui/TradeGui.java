package com.sakura.thelastlegend.gui;


import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.real.Service;
import screen.GameScreen;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Char;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.domain.model.SmallImage;


public class TradeGui extends Screen implements IActionListener{
	public int zoneCol = 6,coutFc;
	public  int popupY, popupX, isborderIndex, isselectedRow;
	public  int xstart, ystart, popupW = 163, popupH = 160, cmySK, cmtoYSK, cmdySK, cmvySK, cmyLimSK;
	public  int columns = 8, rows;
	public int indexSize = 30, indexTitle = 0, indexSelect = -1, indexRow = -1, indexRowMax, indexMenu = 0, indexCard = -1, indexSelect1 = -1, indexSelect2 = -1;
	public static Char parner = new Char();
	public static Scroll scrMain = new Scroll();
	public static byte MAX_PAGE_INVETORY=3;
	
	public Command  cmdTrade,cmdClose,cmdUpItem,cmdBlock1,cmdBlock2,cmdA,cmdLeftArrow,cmdRightArrow;
	
	
	int widthInvent=345;//width of frame
	int heightInvent=232;//height of frame
	int widthTrade=170;
	
	int numWInvent=8;//row of inventory
	int numHInvent=2;//col of inventory
	
	int rowsTrade=4;
	int colsTrade=2;
	
	public boolean block1,block2;
	
	public int typeTrade = 1, typeTradeOrder = 1;
	public byte currentPage=1;
	
	
	//trade
	public  mBitmap imgBar,imgButtontrade,imgButtontradeFocus,imgLeftArrow,imgLeftArrowFocus,imgLineTrade,
	imgLocked,imgUnClocked,imgName,imgRightArrow,imgRightArrowFocus;
	public static Command cmdTradeEnd;
	
	// idselect đưa lên, idselect1 đưa xuống 
	
	public TradeGui(int x, int y){
		
		this.popupX = GameCanvas.w/2-widthInvent/2;
		this.popupY = (y+heightInvent+10>GameCanvas.h?GameCanvas.h-heightInvent-10:y);
		
		cmdTrade = new Command("Giao dịch", this, Constants.BTN_TRADE, null);
		
		cmdUpItem = new Command("Chọn", this, Constants.BTN_SELECT, null);
		
		cmdLeftArrow = new Command("", this, Constants.BTN_lEFT_ARROW, null);
		
		cmdRightArrow = new Command("", this, Constants.BTN_RIGHT_ARROW, null);
		cmdTradeEnd = new Command("Đồng ý", this, Constants.END_TRADE, null);
	}
	
	//load all images of trade
	public void LoadImage()
	{
		//load image
		imgBar=GameCanvas.loadImage("/GuiNaruto/Trade/bar.png");
		imgButtontrade=GameCanvas.loadImage("/GuiNaruto/Trade/button_trade.png");
		imgButtontradeFocus=GameCanvas.loadImage("/GuiNaruto/Trade/button_trade2.png");
		
		imgLineTrade=GameCanvas.loadImage("/GuiNaruto/Trade/line.png");
		imgLocked=GameCanvas.loadImage("/GuiNaruto/Trade/locked.png");
		imgUnClocked=GameCanvas.loadImage("/GuiNaruto/Trade/unlocked.png");
		imgName=GameCanvas.loadImage("/GuiNaruto/Trade/name.png");
		
		imgLeftArrow=GameCanvas.loadImage("/GuiNaruto/Trade/left_arrow.png");
		imgLeftArrowFocus=GameCanvas.loadImage("/GuiNaruto/Trade/left_arrow2.png");
		imgRightArrow=GameCanvas.loadImage("/GuiNaruto/Trade/right_arrow.png");
		imgRightArrowFocus=GameCanvas.loadImage("/GuiNaruto/Trade/right_arrow2.png");
		cmdTrade.setPos(popupX + widthInvent/2-Image.getWidth(imgButtontrade)/2,popupY+heightInvent/3,imgButtontrade,imgButtontradeFocus);
		cmdUpItem.setPos(popupX + widthInvent-Image.getWidth(imgButtontrade)-15,popupY+heightInvent-Image.getHeight(imgButtontrade)-5,imgButtontrade,imgButtontradeFocus);
		cmdLeftArrow.setPos(popupX +widthInvent/3-15,popupY+heightInvent-Image.getHeight(imgButtontrade)+7,imgLeftArrow,imgLeftArrowFocus);
		cmdRightArrow.setPos(popupX +widthInvent/3-15+35,popupY+heightInvent-Image.getHeight(imgButtontrade)+7,imgRightArrow,imgRightArrowFocus);
		
	}
	public void update(){
//		if(imgBar==null&&GameCanvas.gameTick%4==0) LoadImage();
		if(imgBar==null||imgUnClocked==null){
			LoadImage();
		}
		
		if(scrMain!=null)
		{
			scrMain.updatecm();
		}
	}
	
	public void updateKey(){ // xử lý rool select item
		if(imgBar==null&&GameCanvas.gameTick%4==0) LoadImage();
//		    select inventory
			if (GameCanvas.isTouch) {
				if(indexSelect!=-1){
				indexSelect1=-1;
				indexSelect2=-1;
				}
				ScrollResult r = scrMain.updateKey();
				if (r.isDowning || r.isFinish) {
					indexSelect = r.selected;
					indexSelect-=(currentPage-1)*8;
					if(Char.myChar().arrItemBag !=null&&indexSelect>=Char.myChar().arrItemBag.length)
						indexSelect =-1;
					Cout.println(Char.myChar().arrItemBag.length+" indexSelect>>>>  "+indexSelect);
				}
			}
			if(GameCanvas.gameTick%4 ==0){
				coutFc++;
				if(coutFc>2)
					coutFc =0;
			}
			//up item
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdUpItem))
			{
				if (cmdUpItem != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdUpItem != null)
						cmdUpItem.performAction();
				}
			}
			
			//trade
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdTrade)) 
			{
				if (cmdTrade != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdTrade != null)
						cmdTrade.performAction();
				}
			}
			
			//arrow left
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdLeftArrow)) 
			{
				if (cmdLeftArrow != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdLeftArrow != null)
						cmdLeftArrow.performAction();
				}
			}
			
			//arrow right
			if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdRightArrow)) 
			{
				if (cmdRightArrow != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdRightArrow != null)
						cmdRightArrow.performAction();
				}
			}
			
	    this.UpdatePointer();
	}
	
	public void PaintLine(mGraphics g)
	{
		ystart = popupY +10+heightInvent-100;
		int nCol=widthInvent/Image.getWidth(imgLineTrade)+6;
		
		for(int i=0;i<nCol;i++)
		{
			g.drawImage(imgLineTrade,popupX+(Image.getWidth(imgLineTrade)-1)*i+15,ystart, 0);
		}
		
		int yMoney=ystart+10;
		
		// draw bg money
//		g.drawImage(imgBar,popupX+widthInvent-Image.getWidth(imgBar)-40,yMoney, 0);
//		mFont.tahoma_7_yellow.drawString(g,"10000",popupX+widthInvent-Image.getWidth(imgBar)-35,yMoney, 0);
//		
//		yMoney+=(Image.getHeight(imgBar)+5);
//		g.drawImage(imgBar,popupX+widthInvent-Image.getWidth(imgBar)-40,yMoney, 0);
//		mFont.tahoma_7_yellow.drawString(g,"10000",popupX+widthInvent-Image.getWidth(imgBar)-35,yMoney, 0);
//		
//		yMoney+=Image.getHeight(imgBar)+5;
//		g.drawImage(imgBar,popupX+widthInvent-Image.getWidth(imgBar)-40,yMoney, 0);
//		mFont.tahoma_7_yellow.drawString(g,"10000",popupX+widthInvent-Image.getWidth(imgBar)-35,yMoney, 0);
		
	}
	
	public void UpdatePointer()
	{
		xstart = popupX+20;
		ystart = popupY +55+10;

		//focus trade 1
		if (GameCanvas.isPointSelect(xstart, ystart, rowsTrade* (Image.getWidth(LoadImageInterface.ImgItem) + colsTrade),
				colsTrade * (Image.getHeight(LoadImageInterface.ImgItem) + 2)+12)) {
			indexSelect=-1;//not focus inventory
			int col=(GameCanvas.px - xstart)/ (Image.getWidth(LoadImageInterface.ImgItem));
			int row=(GameCanvas.py - ystart) / (Image.getWidth(LoadImageInterface.ImgItem));
			
			if(col>rowsTrade)
				col=4;
			if(row>=colsTrade)
				row=1;
			int dem = col+row*rowsTrade;
			if(dem>=0&&Char.myChar().ItemMyTrade!=null&&dem<Char.myChar().ItemMyTrade.length&&Char.myChar().ItemMyTrade[dem]!=null)
			 indexSelect1=dem;
		}
		
		xstart = popupX+widthInvent-20-112;
		ystart = popupY +55+10;
		//focus trade 2
		if (GameCanvas.isPointSelect(xstart, ystart, rowsTrade * (Image.getWidth(LoadImageInterface.ImgItem) + 2),
				colsTrade * (Image.getHeight(LoadImageInterface.ImgItem) +2) +12 )) {
			indexSelect=-1;//not focus inventory
			int col=(GameCanvas.px - xstart)/ (Image.getWidth(LoadImageInterface.ImgItem));
			int row=(GameCanvas.py - ystart) / (Image.getWidth(LoadImageInterface.ImgItem));
			
			if(col>rowsTrade)
				col=4;
			if(row>=colsTrade)
				row=1;

			int dem = col+row*rowsTrade;
			if(dem>=0&&Char.myChar().partnerTrade!=null&&Char.myChar().partnerTrade.ItemMyTrade!=null
					&&dem<Char.myChar().partnerTrade.ItemMyTrade.length&&Char.myChar().partnerTrade.ItemMyTrade[dem]!=null)
			 indexSelect2=dem;
		}
		
		//touch block and unblock
		int x=popupX+20+12+Image.getWidth(imgName);
		int y=ystart-12-Image.getHeight(imgUnClocked);
		if (GameCanvas.isPointSelect(x, y,Image.getWidth(imgUnClocked),Image.getHeight(imgUnClocked)))
		{
//			block1=true;
			if(Char.myChar().partnerTrade!=null)
			Service.gI().comfirmTrade(Constants.LOCK_TRADE, (short) Char.myChar().partnerTrade.charID);
			GameCanvas.isPointerClick = false;
		}

//		int x1=popupX+widthInvent-20-112+12+Image.getWidth(imgName);
//		int y1=ystart-12-Image.getHeight(imgUnClocked);
//		if (GameCanvas.isPointSelect(x1, y1,Image.getWidth(imgUnClocked),Image.getHeight(imgUnClocked)))
//		{
////			block2=true;
//			if(Char.myChar().partnerTrade!=null)
//			Service.gI().comfirmTrade(Constants.LOCK_TRADE, (short) Char.myChar().partnerTrade.charID);
//			GameCanvas.isPointerClick = false;
//			
//		}
	}
	
	public void paint(mGraphics g){
		GameScreen.resetTranslate(g);
//		System.out.println("ARR ITEM BAG ----> "+Char.myChar().arrItemBag.length);
		GameCanvas.paint.paintFrameNaruto(popupX, popupY +10, widthInvent ,heightInvent,g);	
		paintTrade(g);
//		if(Char.partnerTrade != null)
		//paitnTrade1(g);
		paintParnerTrade(g);
		GameScreen.resetTranslate(g);
		
		cmdUpItem.paint(g,true);
		cmdTrade.paint(g,true);
		cmdLeftArrow.paint(g,true);
		cmdRightArrow.paint(g,true);
		
		//paint page
		mFont.tahoma_7b_white.drawString(g,currentPage+"/"+MAX_PAGE_INVETORY,
				cmdLeftArrow.x+cmdLeftArrow.w+(cmdRightArrow.x-(cmdLeftArrow.x+cmdLeftArrow.w))/2,cmdLeftArrow.y+2,g.VCENTER);
		
		
		Paint.PaintBoxName("Giao Dịch",popupX+widthInvent/4,popupY +10,widthInvent/2,g);
		PaintLine(g);
		
	}
	
	//paint trade zone
	public void paintTrade(mGraphics g) {
		try{
	    GameScreen.resetTranslate(g);
		xstart = popupX+20;
		ystart = popupY +55;

		rows = 4;
		columns = 2;
		//draw name char
		g.drawImage(imgName,xstart+10,ystart-10, 0);
		mFont.tahoma_7_yellow.drawString(g, Char.myChar().cName,xstart +10+Image.getWidth(imgName)/2,ystart-10, mFont.CENTER);
		
		if(!block1)
			//draw block
			g.drawImage(imgUnClocked,xstart+12+Image.getWidth(imgName),ystart-12, 0,true);
		else
			g.drawImage(imgLocked,xstart+12+Image.getWidth(imgName),ystart-12, 0,true);
		
		//trade 1
		for(int i=0;i<rows;i++)
			for(int j=0;j<columns;j++)
			{
				g.drawImage(LoadImageInterface.ImgItem,xstart+(Image.getWidth(LoadImageInterface.ImgItem)+2)*i,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*j, 0);
//				if(Char.myChar().ItemMyTrade != null){
//					if(Char.myChar().ItemMyTrade.length > 0){
						
//					}
//				}
			}
		for(int k = 0; k < Char.myChar().ItemMyTrade.length; k++){
			if(Char.myChar().ItemMyTrade[k] != null){
//				Char.myChar().ItemMyTrade[k].template = ItemTemplates.get((short)Char.myChar().ItemMyTrade[k].itemId);
				if(Char.myChar().ItemMyTrade[k].template != null){
					int r = k % 4;
					int c = k / 4;
					SmallImage.drawSmallImage(g, Char.myChar().ItemMyTrade[k].template.iconID, xstart + (r * indexSize) + 14,10+ ystart + (c * indexSize) + 14, 0,
							mGraphics.VCENTER | mGraphics.HCENTER);
				
				}
				
			}
		}
		//focus item
		if (indexSelect1 >= 0) {
			Paint.paintFocus(g,
					(xstart + ((indexSelect1 % rows)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+4 , ystart
					+ (indexSelect1 / rows) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +14
					, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
//			if(GameCanvas.gameTick % 10 == 0)
//				g.drawImage(LoadImageInterface.imgFocusSelectItem0, (xstart + ((indexSelect1 % rows)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+2 , ystart
//						+ (indexSelect1 / rows) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +12 , 0);
//			else
//				g.drawImage(LoadImageInterface.imgFocusSelectItem1, (xstart + ((indexSelect1 % rows)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+2 , ystart
//						+ (indexSelect1 / rows) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +12 , 0);
		}
		
		// draw bg money
//		g.drawImage(imgBar,xstart+10,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*columns, 0);
//		g.drawImage(imgBar,xstart+(Image.getWidth(LoadImageInterface.ImgItem)+2)*2+10,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*columns, 0);
//		mFont.tahoma_7_yellow.drawString(g,"10000",xstart+14,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*columns,0);
//		mFont.tahoma_7_yellow.drawString(g,"10000",xstart+(Image.getWidth(LoadImageInterface.ImgItem)+2)*2+14,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*columns,0);
		
		xstart = popupX+widthInvent-20-112;
		ystart = popupY +55;

		//draw name char
		g.drawImage(imgName,xstart+10,ystart-10, 0);
		if(Char.myChar().partnerTrade!=null)
		mFont.tahoma_7_yellow.drawString(g,Char.myChar().partnerTrade.cName,xstart +10+Image.getWidth(imgName)/2,ystart-10, mFont.CENTER);
		
		//draw block
		if(!block2)
			g.drawImage(imgUnClocked,xstart+12+Image.getWidth(imgName),ystart-12, 0);
		else
			g.drawImage(imgLocked,xstart+12+Image.getWidth(imgName),ystart-12, 0);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				g.drawImage(LoadImageInterface.ImgItem,xstart+(Image.getWidth(LoadImageInterface.ImgItem)+2)*i,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*j, 0,true);
//				if(Char.myChar().partnerTrade != null){
//					if(Char.myChar().partnerTrade.ItemParnerTrade != null){
						//					}
					
//				}
			}
		}
		if(Char.myChar().partnerTrade!=null&&Char.myChar().partnerTrade.ItemParnerTrade.length > 0){
			for(int k = 0; k < Char.myChar().partnerTrade.ItemParnerTrade.length; k++){
				if(Char.myChar().partnerTrade.ItemParnerTrade[k] != null){
//					Char.myChar().partnerTrade.ItemParnerTrade[k].template = ItemTemplates.get((short)Char.myChar().partnerTrade.ItemParnerTrade[k].itemId);
					if(Char.myChar().partnerTrade.ItemParnerTrade[k].template != null&&Char.myChar().arrItemBag!=null){
						int r = k % 4;
						int c = k / 4;
							SmallImage.drawSmallImage(g, Char.myChar().partnerTrade.ItemParnerTrade[k].template.iconID, xstart + (r * indexSize) + 14,10+ ystart + (c * indexSize) + 14, 0,
									mGraphics.VCENTER | mGraphics.HCENTER,true);
//						System.out.println("XXXXXXXXXX ----> "+(xstart + (c * indexSize) + 13)+" iiiiiiiiiiii "+10+ ystart + (r * indexSize) + 13);
					}
					
				}
			}
		}

		//focus item
		if (indexSelect2 >= 0) {
			
			g.drawImage(LoadImageInterface.imgFocusSelectItem, (xstart + ((indexSelect2 % rows)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+2 , ystart
					+ (indexSelect2 / rows) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +12 , 0);
		}
		
		// draw bg money
//		g.drawImage(imgBar,xstart+10,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*columns, 0);
//		g.drawImage(imgBar,xstart+(Image.getWidth(LoadImageInterface.ImgItem)+2)*2+10,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*columns, 0);
//		mFont.tahoma_7_yellow.drawString(g,"10000",xstart+14,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*columns,0);
//		mFont.tahoma_7_yellow.drawString(g,"10000",xstart+(Image.getWidth(LoadImageInterface.ImgItem)+2)*2+14,10+ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*columns,0);
		
//		if (timeTrade - timeNow > 0 && typeTrade == 1 && typeTradeOrder == 1) {
//			mFont.tahoma_7_white.drawString(g, mResources.WAIT + " " + (timeTrade - timeNow) + " " + mResources.SECOND, popupX + popupW / 2,
//					popupY + popupH - 13, 2);	
//		} else if (typeTrade == 0) {
//			mFont.tahoma_7_white.drawString(g, mResources.TRADEHELP, popupX + popupW / 2, popupY + popupH - 13, 2);
//		}

	}catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	
}

	public void paintParnerTrade(mGraphics g){
		try{
			GameScreen.resetTranslate(g);
			xstart = popupX +20;
			ystart = popupY +10+heightInvent-90;
			rows = 2;
			columns=8;

//			Paint.paintBorder(xstart-10, ystart-11, columns * indexSize + 2+20,rows * indexSize + 2+22,g,0xd1dcdd,1);
			if(currentPage==3)
			{
				columns=4;
				scrMain.setStyle(1, indexSize, xstart, ystart, columns * indexSize , rows * indexSize,true,true,4);
				scrMain.setClip(g, xstart, ystart, columns * indexSize, rows * indexSize + 2);
			}else
			{
				scrMain.setStyle(1, indexSize, xstart, ystart, columns * indexSize , rows * indexSize,true,true,8);
				scrMain.setClip(g, xstart, ystart, columns * indexSize, rows * indexSize + 2);
			}
			
			for(int i=0;i<21;i++)
				for(int j=0;j<rows;j++)
				{
					g.drawImage(LoadImageInterface.ImgItem,xstart+(Image.getWidth(LoadImageInterface.ImgItem)+2)*i,ystart+(Image.getHeight(LoadImageInterface.ImgItem)+2)*j, 0,true);
					
				}
			if(Char.myChar().arrItemBag != null){
				for(int k = 0; k < Char.myChar().arrItemBag.length; k++){
					int r = (k%16) / columns;
					int c =  ((k%16) %columns);
					if( Char.myChar().arrItemBag[k] != null
							&& k < Char.myChar().arrItemBag.length&&
							Char.myChar().arrItemBag[k].template!=null
							&&(k>=(currentPage-1)*16&&(k<(currentPage)*16))){
						 Char.myChar().arrItemBag[k].paintItem(g, 
								 xstart + (c * indexSize) + 14, 
								 ystart + (r * indexSize) + 14);
//						SmallImage.drawSmallImage(g,Char.myChar().arrItemBag[k].template.iconID,
//						 xstart + (c * indexSize) + 14, ystart + (r * indexSize) + 14, 0,
//								mGraphics.VCENTER | mGraphics.HCENTER,true);
					}
				}
			}
//			for (int i = 0; i < rows; i++) {
//				for (int j = 0; j < columns; j++) {
//					
////					g.setColor(0xBB6611);
////					g.drawRect(xstart + (j * indexSize), ystart + (i * indexSize), indexSize, indexSize);
////					for(int k = 0; k < Char.myChar().arrItemBag.length; k++){
////						SmallImage.drawSmallImage(g, Char.myChar().arrItemBag[k].template.iconID, xstart + (k * indexSize) + indexSize / 2, ystart + (r * indexSize) + indexSize / 2, 0,
////								mGraphics.VCENTER | mGraphics.HCENTER);
////					}
//				}
//			}

			GameScreen.resetTranslate(g);
			if(currentPage==3)
			{
				numWInvent=4;
				if (indexSelect >= 0) {
					Paint.paintFocus(g,
							(xstart + (((indexSelect) % numWInvent)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+4 , ystart
							+ ((indexSelect) / numWInvent) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +4
							, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
//					if(GameCanvas.gameTick%10 == 0)
//						g.drawImage(LoadImageInterface.imgFocusSelectItem1, (xstart + (((indexSelect) % numWInvent)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+2 , ystart
//								+ ((indexSelect) / numWInvent) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +2 , 0);
//					else
//						g.drawImage(LoadImageInterface.imgFocusSelectItem0, (xstart + (((indexSelect) % numWInvent)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+2 , ystart
//								+ ((indexSelect) / numWInvent) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +2 , 0);
					
				}
			}else
			{
				numWInvent=8;
				if (indexSelect >= 0) {
					Paint.paintFocus(g,
							(xstart + (((indexSelect) % numWInvent)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+4 , ystart
							+ ((indexSelect) / numWInvent) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +4
							, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
//					if(GameCanvas.gameTick % 10 == 0)
//					g.drawImage(LoadImageInterface.imgFocusSelectItem1, (xstart + (((indexSelect) % numWInvent)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+2 , ystart
//							+ ((indexSelect) / numWInvent) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +2 , 0);
//					else
//						g.drawImage(LoadImageInterface.imgFocusSelectItem0, (xstart + (((indexSelect) % numWInvent)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+2 , ystart
//								+ ((indexSelect) / numWInvent) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +2 , 0);
					
				}
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void perform(int idAction, Object p) {
		Cout.println2222("perform trade  "+idAction);
		switch (idAction) {
			case Constants.BTN_SELECT:
				if(Char.myChar().arrItemBag==null||indexSelect==-1||indexSelect>=Char.myChar().arrItemBag.length)
					return;
				Cout.println2222(indexSelect1+" index itemId "+Char.myChar().arrItemBag[indexSelect].itemId+" iditem "+Char.myChar().arrItemBag[indexSelect].template.id);
				if(indexSelect != -1&&Char.myChar().partnerTrade!=null&&indexSelect<Char.myChar().arrItemBag.length)
					Service.gI().moveItemTrade(Constants.MOVE_ITEM_TRADE, (short)Char.myChar().partnerTrade.charID, (byte)0, (short)Char.myChar().arrItemBag[indexSelect].itemId); // 0 đưa lên 1 đưa xuống
				else if(indexSelect1 != -1&&Char.myChar().partnerTrade!=null&&indexSelect<Char.myChar().ItemMyTrade.length&&Char.myChar().ItemMyTrade[indexSelect1]!=null){
					Service.gI().moveItemTrade(Constants.MOVE_ITEM_TRADE, (short)Char.myChar().partnerTrade.charID, (byte)1, (short)Char.myChar().ItemMyTrade[indexSelect1].itemId); // 0 đưa lên 1 đưa xuống
				}
//				Service.gI().cancelTrade(Constants.CANCEL_TRADE, (short)Char.myChar().partnerTrade.charID);
				break;
			case Constants.BTN_lEFT_ARROW:
				if(currentPage>1)
				{
					currentPage-=1;
					indexSelect=-1;
					scrMain.cmtoX-=(8 * indexSize);
				}
//				Service.gI().comfirmTrade(Constants.LOCK_TRADE, (short)Char.myChar().partnerTrade.charID);
				break;
//			case Constants.BTN_TRADE:
////				Service.gI().doTrade(Constants.TRADE, (short)Char.myChar().partnerTrade.charID);
//				break;
			case Constants.BTN_RIGHT_ARROW:
				if(currentPage<3)
				{
					currentPage+=1;
					indexSelect=-1;
					scrMain.cmtoX+=(8 * indexSize);
				}
//				Service.gI().doTrade(Constants.END_TRADE, (short)Char.myChar().partnerTrade.charID);
				break;
			case Constants.BTN_TRADE:
				if(!block1)
					GameCanvas.startOKDlg("Bạn chưa khóa giao dịch !");
				else if(!block2&&Char.myChar().partnerTrade!=null)
						GameCanvas.startOKDlg(Char.myChar().partnerTrade.cName+" chưa khóa giao dịch !");
				else if(Char.myChar().partnerTrade!=null){
						Service.gI().doTrade(Constants.TRADE, (short)Char.myChar().partnerTrade.charID);
//						for(int i = 0; i < Char.myChar().ItemMyTrade.length; i++){
//							Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
//							Char.myChar().ItemMyTrade[i] = null;
//						}
					}
//					GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
//					Char.myChar().partnerTrade = null;
					
						
				break;
		
			case Constants.END_TRADE:
				if(Char.myChar().partnerTrade!=null)
				Service.gI().doTrade(Constants.END_TRADE, (short)Char.myChar().partnerTrade.charID);
				GameCanvas.endDlg();
//				GameScreen.gI().tradeGui = null;
				break;
		}
		
	}
	
//	public void paitnTrade1(mGraphics g){
//		columns = 5;
//		rows = 3;
//		xstart = popupX - 125;
//		ystart = popupY  + 50;
//		
//		
////		scrMain.setStyle(rows, indexSize, xstart, ystart, columns * indexSize + 2, 5 * indexSize + 2, true, 6);
////		scrMain.setClip(g);
//		mFont.tahoma_7_yellow.drawString(g,Char.partnerTrade.cName, xstart + 1, popupY  + 30, mFont.LEFT);
//		g.fillRect(xstart - 1, ystart - 1, indexSize * columns + 3, indexSize * 3 + 3);
//		for (int i = 0; i < rows; i++) {
//			for (int j = 0; j < columns; j++) {
//				SmallImage.drawSmallImage(g, 154, xstart + (j * indexSize) + indexSize / 2, ystart + (i * indexSize) + indexSize / 2, 0,
//						mGraphics.VCENTER | mGraphics.HCENTER);
//				g.setColor(0xffffff);
//				g.drawRect(xstart + (j * indexSize), ystart + (i * indexSize), indexSize, indexSize);
//				if(Char.partnerTrade != null){
//					if(Char.partnerTrade.ItemTradeParner != null){
////					if(Char.myChar().partnerTrade.ItemTradeParner.length > 0){
//						for(int k = 0; k < Char.partnerTrade.ItemTradeParner.length; k++){
//							if(Char.partnerTrade.ItemTradeParner[k].template != null){
//								SmallImage.drawSmallImage(g, Char.partnerTrade.ItemTradeParner[k].template.iconID, xstart + (k * indexSize) + indexSize / 2, ystart + (k * indexSize) + indexSize / 2, 0,
//										mGraphics.VCENTER | mGraphics.HCENTER);
//							}
//						}
////					}
//					}
//					
//				}
//			}
//		}
//	}
	public void SetPosClose(Command cmdClose) {
		// TODO Auto-generated method stub
//		popupX, popupY +10, widthInvent ,heightInvent
		cmdClose.setPos(popupX+widthInvent-Image.getWidth(LoadImageInterface.closeTab),popupY+10, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
		
	}

}
