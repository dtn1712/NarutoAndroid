package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.Friend;
import Objectgame.OtherChar;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.domain.model.mResources;
import com.sakura.thelastlegend.real.Service;
import screen.GameScreen;

public class GuiFriend  extends Screen implements IActionListener{

	public static Scroll scrMain = new Scroll();
	public int indexRow;
	public long timestartUpdate;
	public int popupX,popupY,popupW,popupH =220;
	//friend list com.sakura.thelastlegend.gui
		public static Command btnUnfriend;//display quest info
		public static Command btnChat;//display quest info
	public GuiFriend(){
		popupX = GameCanvas.w/2- GameScreen.widthGui/2;
		popupY = GameScreen.popupY;
		popupW = GameScreen.widthGui;
		btnUnfriend = new Command("Xóa bạn",this, Constants.UN_FRIEND, null, 0, 0);
		btnChat = new Command("Chat riêng", this, Constants.CHAT_PRIVATE, null, 0, 0);
		btnUnfriend.setPos(popupX + popupW-Image.getWidth(LoadImageInterface.btnTab)/2,
				popupY+40, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
		btnChat.setPos(popupX + popupW-Image.getWidth(LoadImageInterface.btnTab)/2,
				popupY + 3*Image.getHeight(LoadImageInterface.btnTab), LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
	
	}
	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		scrMain.updatecm();
		ScrollResult s1 =  scrMain.updateKey();
		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnChat)) {
			if (btnChat != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (btnChat != null)
					btnChat.performAction();
			}
		}
		//unfriend
		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnUnfriend)) {
			if (btnUnfriend != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (btnUnfriend != null)
					btnUnfriend.performAction();
			}
		}
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		if((mSystem.currentTimeMillis()-timestartUpdate)/1000>30)
		{
			timestartUpdate = mSystem.currentTimeMillis();
			Service.gI().requestFriendList((byte)Friend.REQUEST_FRIEND_LIST, (short)Char.myChar().charID);
		}
		if(scrMain.selectedItem == -1)
			return;
		indexRow = scrMain.selectedItem;
		if (indexRow<Char.myChar().vFriend.size()&&Char.myChar().vFriend.size() > 0) {
			Char c = (Char) Char.myChar().vFriend.elementAt(indexRow);
			Char.toCharChat=c;
//			yMenu+(5+LoadImageInterface.btnTab.getHeight())*i
		}
	}

	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		g.setColor(0xff000000,GameCanvas.opacityTab);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		paintFriend(g);
	}

	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		cmd.setPos(popupX+popupW-Image.getWidth(LoadImageInterface.closeTab), popupY, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
		
	}
	
	
	
	public void paintFriend(mGraphics g) {

			String str =  mResources.FRIENDS[0] ;
			
			GameCanvas.paint.paintFrameNaruto(popupX, popupY,popupW,popupH, g);
			if (Char.myChar().vFriend.size() > 0) {
				GameScreen.xstart =popupX + 5;
				GameScreen.ystart =popupY + 40;
				
				resetTranslate(g);
				scrMain.setStyle(Char.myChar().vFriend.size(), 50, GameScreen.xstart, GameScreen.ystart,  popupW- 3, 180, true, 1);
				scrMain.setClip(g, GameScreen.xstart, GameScreen.ystart-10, popupW - 3, 180);
				int friendCount = 0;
				int yBGFriend=0;
				for (int i = 0; i < Char.myChar().vFriend.size(); i++) {
					Char c = (Char) Char.myChar().vFriend.elementAt(i);
//					
					
//					Paint.PaintBGListQuest(x+40,y+70,width-50,g);//new quest
//					if(indexselectNV==0&&Quest.listUnReceiveQuest.size()>0&&(GameCanvas.gameTick/10)%2==0)
//						Paint.PaintBGListQuestFocus(x+40,y+70,width-50,g);//new focus quest
					   
						Paint.PaintBGListQuest(GameScreen.xstart +35, GameScreen.ystart+yBGFriend,160,g);//new quest
						if (indexRow == i) {
//							g.setColor(Paint.COLORLIGHT);
//							g.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
//							g.setColor(0xffffff);
//							g.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
//							btnChat.paint(g);
//							btnUnfriend.paint(g);
						
							Paint.PaintBGListQuestFocus(GameScreen.xstart +35, GameScreen.ystart+yBGFriend,160,g);
						}
//					else {
//							g.setColor(Paint.COLORBACKGROUND);
//							g.fillRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
//							g.setColor(0xd49960);
//							g.drawRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 15, indexSize - 4);
//						}

						
						mFont.tahoma_7_white.drawString(g, "Level: "+c.clevel, GameScreen.xstart + 73, GameScreen.ystart+yBGFriend+24 , 0,true);
						
						g.drawImage(LoadImageInterface.charPic, GameScreen.xstart +45, GameScreen.ystart+yBGFriend+20,g.VCENTER|g.HCENTER);
						if(c.isOnline)
						{
							g.drawImage(LoadImageInterface.imgName, GameScreen.xstart+100, GameScreen.ystart+yBGFriend+15,g.VCENTER|g.HCENTER,true);
							mFont.tahoma_7_white.drawString(g, c.cName, GameScreen.xstart + 73, GameScreen.ystart+yBGFriend+8 , 0,true);
							g.drawImage(LoadImageInterface.imgOnline[1], GameScreen.xstart +180, GameScreen.ystart+yBGFriend+20,g.VCENTER|g.HCENTER,true);
							
						}
							
						else 
						{
							g.drawImage(LoadImageInterface.imgName, GameScreen.xstart+100, GameScreen.ystart+yBGFriend+15,g.VCENTER|g.HCENTER,true);
							mFont.tahoma_7_white.drawString(g, c.cName, GameScreen.xstart + 73, GameScreen.ystart +yBGFriend+8, 0,true);
							g.drawImage(LoadImageInterface.imgOnline[0], GameScreen.xstart +180, GameScreen.ystart+yBGFriend+20,g.VCENTER|g.HCENTER,true);
						}
						
//						try {
//							Part ph2 = GameScreen.parts[c.head];
//							SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id, 
//									GameScreen.xstart + 44,GameScreen.ystart +yBGFriend+20, 0, mGraphics.VCENTER|mGraphics.HCENTER);
//						} catch (Exception e) {
//							// TODO: handle exception
//						}
						friendCount++;
						yBGFriend+=50;
				}
				resetTranslate(g);
				if(btnChat!=null)
				{
					btnChat.paint(g);
					btnUnfriend.paint(g);
				}
			} else {
				mFont.tahoma_7_white.drawString(g, mResources.NO_FRIEND, popupX + popupW / 2,popupY + 40,
						mFont.CENTER);
			}
			GameScreen.resetTranslate(g);
			//paint name box 
			Paint.PaintBoxName("DANH SÁCH BẠN BÈ",popupX+55,popupY,130,g);
	}

	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case Constants.UN_FRIEND:
			if(indexRow>=0&&indexRow<Char.myChar().vFriend.size()){
				
				Char charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
				GameCanvas.startYesNoDlg("Bạn có muốn tuyệt giao với "+charF.cName,
						new Command("Có", this, 13, null), new Command("Không", GameCanvas.instance, GameCanvas.cEndDgl, null));;
//				Service.gI().deleteFriend((byte)Friend.UNFRIEND,(short)Char.myChar().charID ,(short)charF.CharidDB);
//
//				indexRow = -1;
			}
			break;
		case Constants.CHAT_PRIVATE: // mo giao dien chat rieng
//			ChatTab currentTab = ChatManager.gI().getCurrentChatTab();
//			currentTab.type = 2;
//			indexRow = -1;
//			isPaintFriend = false;
//			left = null;
//			center = null;
//			openUIChatTab();
			GameScreen.gI().guiMain.menuIcon.cmdClose.performAction();
			if(indexRow>=0&&indexRow<Char.myChar().vFriend.size()){
				Char charFf = (Char) Char.myChar().vFriend.elementAt(indexRow);
				if(charFf!=null){
					Cout.println(getClass(), "CHAT_PRIVATE charFf  "+charFf.cName);
					boolean isAdded = false;
					for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++) {
						OtherChar other =(OtherChar)ChatPrivate.vOtherchar.elementAt(i);
						if(other.name.equals(charFf.cName)||other.id==charFf.charID){
							isAdded = true;
							break;
						}
						
					}
					if(!isAdded)
						ChatPrivate.AddNewChater((short)charFf.charID, charFf.cName);
				}
			}
			indexRow = -1;
			left = null;
			center = null;
			TabChat.gI().switchToMe();
//			isLockKey = true;
			GameScreen.setPopupSize(175, 200);
			left = center = null;
			
			break;
		case 13: //ok unfrien
			if(indexRow>=0&&indexRow<Char.myChar().vFriend.size()){
				Char charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
				Service.gI().deleteFriend((byte)Friend.UNFRIEND,(short)Char.myChar().charID ,(short)charF.CharidDB);

				indexRow = -1;
			}

			GameCanvas.endDlg();
			break;
		}
		
	}
}
