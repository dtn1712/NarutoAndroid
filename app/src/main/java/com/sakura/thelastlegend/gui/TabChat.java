package com.sakura.thelastlegend.gui;

import screen.GameScreen;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.NodeChat;
import Objectgame.OtherChar;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;

public class TabChat extends Screen implements IActionListener{

	public static TabChat instance;
	public Scroll scrListActor;
	//icon chat
	public static Iconchat iconChat;
	public static Command cmdchat,cmdClose;
	public static Command cmdIconChat;
	public static TField tfCharFriend;
	public int heightGui = 220,popupY,popw=100,popupW=220,popx;
	public int indexRow;
	public Boolean isPaintListIcon = false;	
	public  int indexSize = 28;
	
	public static TabChat gI()
	{
		if(instance==null)
			instance = new TabChat();
		return instance;
	}
	public TabChat()
	{
		cmdchat = new Command("Gửi", Constants.BUTTON_SEND);
		cmdIconChat = new Command("", Constants.BUTTON_ICON_CHAT);
	    tfCharFriend = new TField();
	    scrListActor = new Scroll();
	}
	
	private void initTfied(){

		  tfCharFriend.width = popupW - 64;
		  tfCharFriend.height = ITEM_HEIGHT + 2;
		  popx = GameCanvas.w/2-(popw+popupW)/2-5;
		  tfCharFriend.x = popx+popw + 8;
		  popupY = GameCanvas.h/2 - heightGui/2;
		  tfCharFriend.y = popupY + heightGui- 30;
		  Cout.println(getClass(), (GameCanvas.h/2)+" tfCharFriend.y   "+ tfCharFriend.y );
		  tfCharFriend.isFocus =false;
		  tfCharFriend.setIputType(TField.INPUT_TYPE_ANY);
		  cmdIconChat.setPos( popx+ GameScreen.gI().popw+5+tfCharFriend.width+5, tfCharFriend.y, LoadImageInterface.imgEmo[7], LoadImageInterface.imgEmo[7]);
		  cmdIconChat.w = 15;
		  cmdIconChat.h = 20;
		  cmdchat.setPos(cmdIconChat.x+cmdIconChat.w+2, tfCharFriend.y-5, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		  cmdchat.w = 30;
		  cmdchat.h = 20;

		  tfCharFriend.y = popupY + heightGui- 32;
//		  tfCharFriend.name = "Chat riêng";
//		  tfCharFriend.m = GameMidlet.instance;
//		  tfCharFriend.c = MotherCanvas.instance;
//		  tfCharFriend.color = 0xffffff;
	}
	
	@Override
	public void switchToMe() {
		// TODO Auto-generated method stub
		initTfied();
		cmdClose = new Command("", this, 20, null);
//		GameScreen.xGui+5, GameScreen.yGui+5,popupW-20
		cmdClose.setPos(popx+(popw+popupW)- LoadImageInterface.closeTab.height/2, popupY, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
		scrListActor.selectedItem = -1;
		super.switchToMe();
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		if(isPaintListIcon){
			iconChat.Updatecm();
			iconChat.updateKeySelectIconChat();
			if(iconChat.indexSelect>=0)
			{
				tfCharFriend.setText(tfCharFriend.getText()+NodeChat.maEmo[iconChat.indexSelect]) ; 
				iconChat.indexSelect=-1;
				isPaintListIcon = false;
			}
		}else{
			scrListActor.updatecm();
			ScrollResult s1 = scrListActor.updateKey();
			if(scrListActor.selectedItem>-1)
			indexRow = scrListActor.selectedItem;
			if(indexRow>=0&&indexRow<ChatPrivate.vOtherchar.size()&&
					((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow))!=null)
			{
				Char c = GameScreen.findCharInMap(((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).id);
				if(c!=null){
				Char.toCharChat = c;

				ChatPrivate.nTinChuaDoc -= ((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).nTinChuaDoc;
				((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).nTinChuaDoc = 0;
				}
				((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).update();
			}
		}
		GameScreen.gI().update();
		super.update();
	}

	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		updatePointer();
		scrListActor.updateKey();
		if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(cmdClose)) {
			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
					cmdClose.performAction();
			}
		}
		if(indexRow<ChatPrivate.vOtherchar.size()&&indexRow>=0&&((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow))!=null&&Char.toCharChat!=null)
		{
			Char.toCharChat.CharidDB = ((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).id;
		}
		super.updateKey();
	}

	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		GameScreen.gI().paint(g);
		resetTranslate(g);
		paintListFiendChat(g);
		resetTranslate(g);
		tfCharFriend.paint(g);
		Paint.PaintBoxName("Chat Riêng",popx+popw+popupW/2-55,popupY,popupW-110,g);
		//paint content of chat friend
		if(indexRow<ChatPrivate.vOtherchar.size()&&indexRow>=0&&((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow))!=null)
		{
			((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).Paint(g, GameScreen.xGui+5+popw/2, GameScreen.yGui+5,popupW-20,tfCharFriend.y- GameScreen.yGui-7);
		}
		resetTranslate(g);
		cmdchat.paintW(g);
		cmdIconChat.paint(g);
		if(isPaintListIcon&&iconChat!=null)
		{
			 iconChat.paint(g);
		}

		cmdClose.paint(g,true);
		super.paint(g);
	}

	@Override
	public void updatePointer() {
		// TODO Auto-generated method stub
		if(isPaintListIcon){
			if(GameCanvas.isPointerClick&&!GameCanvas.isPoint(iconChat.scrMain.xPos,
					iconChat.scrMain.yPos, iconChat.scrMain.width, iconChat.scrMain.height)){
				isPaintListIcon = false;
				Cout.println("--------------dongoooooooooooo1  2222");
				GameCanvas.isPointerClick = false;
			}
		}else{
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(cmdchat)) {
				if (cmdchat!= null) {
					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdchat != null)
						cmdchat.performAction();
				}
			}
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(cmdIconChat)) {
				if (cmdIconChat!= null) {
					GameCanvas.isPointerJustRelease = false;
//					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (cmdIconChat != null)
						cmdIconChat.performAction();
				}
			}
			tfCharFriend.update();
		}
		super.updatePointer();
	}

	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 20:
			GameScreen.gI().switchToMe();
			break;

		default:
			break;
		}
		
	}
	public void paintListFiendChat(mGraphics g) {
//			GameCanvas.paint.paintFrame(popx + 70, popy +30, popw, poph + 20, g);//paint list chater
			GameCanvas.paint.paintFrameNaruto(popx-8,popupY, popw, heightGui, g);//paint list chater
			GameCanvas.paint.paintFrameNaruto(popx+popw, popupY,popupW,heightGui, g);//paint frame chat
			if (ChatPrivate.vOtherchar.size() > 0) {
//				xstart = popupX + 5;
//				ystart = popupY + 40;
				GameScreen.xstart = popx-3;
				GameScreen.ystart = popupY+5;
//				g.setColor(0x001919);
//				g.fillRect(xstart - 2, ystart - 2, popw - 6, indexSize * 5 + 8);
	
				resetTranslate(g);
				
				scrListActor.setStyle(ChatPrivate.vOtherchar.size(), indexSize, popx-8,popupY, popw , heightGui, true, 1);
				scrListActor.setClip(g, popx-8, popupY, popw, heightGui);
	
//				indexRowMax = ChatPrivate.vOtherchar.size();
				int friendCount = 0;
				for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++)
				{
					OtherChar c = (OtherChar) ChatPrivate.vOtherchar.elementAt(i);
//	
					if (indexRow == i) 
					{
						g.setColor(Paint.COLORLIGHT);
						g.fillRect(GameScreen.xstart + 4, GameScreen.ystart + (indexRow * GameScreen.indexSize) + 4, popw - 19, indexSize - 8);
						g.setColor(0xffffff);
						g.drawRect(GameScreen.xstart + 4, GameScreen.ystart + (indexRow * GameScreen.indexSize) + 4, popw - 19, indexSize - 8);
								
					} 
//					else
//					{
//						g.setColor(Paint.COLORBACKGROUND);
//						g.fillRect(xstart + 2, ystart + (i * indexSize) + 2, popw - 15, indexSize - 4);
//						g.setColor(0xd49960);
//						g.drawRect(xstart + 2, ystart + (i * indexSize) + 2, popw - 15, indexSize - 4);
//					}
							
					mFont.tahoma_7_red.drawString(g, c.name, GameScreen.xstart + 8, GameScreen.ystart + i * indexSize + indexSize / 2 - 6, 0);
		
					friendCount++;
				}
				
//				paintNumCount(g);
			} 
	}
	private void paintNumCount(mGraphics g) {
//		resetTranslate(g);
//		int curRow = indexRow;
//		if (isPaintAuctionBuy)
//			curRow = indexSelect;
//		if (curRow >= 0 && indexRowMax > 0) {
//			int row = ((curRow + 1) < indexRowMax) ? (curRow + 1) : indexRowMax;
//			
//			mFont.tahoma_7b_white.drawString(g, row + "/" + indexRowMax, popx + popw / 2, popy + poph - 20, 2);
//		}
	}
}
