package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.mFont;

public class AvMain {
    public Command left, right, center;

    public static void Font3dWhite(mGraphics g, String str, int x, int y, int ar) {
        mFont.tahoma_7b_red.drawString(g, str, x + 1, y + 1, ar);
        mFont.tahoma_7b_white.drawString(g, str, x, y, ar);
    }

}
