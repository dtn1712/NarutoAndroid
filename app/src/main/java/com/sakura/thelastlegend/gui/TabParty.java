package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Char;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.FontSys;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Key;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Part;
import com.sakura.thelastlegend.domain.model.Party;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.domain.model.SmallImage;
import com.sakura.thelastlegend.domain.model.Type_Party;
import com.sakura.thelastlegend.domain.model.mResources;
import com.sakura.thelastlegend.real.Service;
import screen.GameScreen;

public class TabParty extends Screen implements IActionListener {
	Command kich,giaitan,roi;
	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		if(kich!=null&&Party.gI().isLeader&&Party.vCharinParty.size()>=2)
		{
			if(getCmdPointerLast(kich)){
				kich.performAction();	
			}
		}
		if(giaitan!=null&&Party.gI().isLeader&&Party.vCharinParty.size()>=2)
		{
			if(getCmdPointerLast(giaitan)){
				giaitan.performAction();	
			}
		}
		if(roi!=null&&Party.vCharinParty.size()>=2)
		{
			if(getCmdPointerLast(roi)){
				roi.performAction();	
			}
		}
		
		ScrollResult s1 = scrMain.updateKey();
		super.updateKey();
	}
	public static Scroll scrMain = new Scroll();
	int x,y;
	public  int xstart, ystart, popupW = 163, popupH = 160, cmySK, cmtoYSK, cmdySK, cmvySK, cmyLimSK;
	public  int popupY, popupX, isborderIndex, indexRow=-1,widthGui=235,heightGui=240,indexSize=50,indexRowMax;
	public boolean isLead = false;
	@Override
	public void update() {
		// TODO Auto-generated method stub
//		if (hParty.size() == 0)
//		Service.gI().requestinfoPartynearME(Type_Party.GET_INFOR_NEARCHAR, (short)Char.myChar().charID);
	if (GameCanvas.keyPressed[Key.NUM8]) {
		indexRow++;
		if (indexRow >= GameScreen.hParty.size())
			indexRow = GameScreen.hParty.size() - 1;
		scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
	} else if (GameCanvas.keyPressed[Key.NUM2]) {
		indexRow--;
		if (indexRow < 0)
			indexRow = 0;
		scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
	}
	scrMain.updatecm();

	if(GameScreen.hParty.size() == 0&&scrMain.selectedItem!=-1){
		indexRow = scrMain.selectedItem;
		scrMain.selectedItem = -1;
//		if(GameScreen.hParty.size()==0){
//			short PartyId = 0;
//			short CharLeaderid = Char.myChar().idParty;
//			byte membersize = 5;
//			short[] charIDmeber =  new short[membersize];
//			for(int i = 0; i < membersize; i++){
//				charIDmeber[i] = (short)i;
//			}
//			GameScreen.hParty.contains(PartyId+"");
//			GameScreen.hParty.put(PartyId+"",(Object)new Party(PartyId, charIDmeber, CharLeaderid));
//		
//		}
//		Cout.println(getClass(), " iconContact.performAction() ");
//		GameScreen.gI().guiMain.menuIcon.iconContact.performAction();
	}
	else 
		if(GameScreen.hParty.size() != 0&&scrMain.selectedItem!=-1){
		indexRow = scrMain.selectedItem;
		scrMain.selectedItem = -1;
		setPartyCommand();
	}
		super.update();
	}
	public TabParty(int x,int y){
		this.x=GameCanvas.w/2-widthGui/2;//GameCanvas.w/2-GameScreen.widthGui/2;
		this.y=y;
		this.popupX = x-widthGui/2;
		this.popupY = GameCanvas.h/2-heightGui/2;
		
	}
private void setPartyCommand() {
		
		Party party = (Party) GameScreen.hParty.get(Char.myChar().idParty+"");
//		if(party == null)
//			return;
		if (indexRow == -1 )
			return;
		if(party != null){
					if(Char.myChar().charID == party.idleader){
						final Char c = (Char) party.vCharinParty.elementAt(indexRow);
						if(c != null){
							kich = new Command( "Kích",this,  270192, null, 0, 0);
							kich.setPos(popupX + popupW+ LoadImageInterface.btnTab.getWidth()-10, popupY+ LoadImageInterface.btnTab.getHeight()*2, LoadImageInterface.btnTab, LoadImageInterface.btnTab);
							giaitan = new Command( "Giải tán",this,  111111, null, 0, 0);
							giaitan.setPos(popupX + popupW+ LoadImageInterface.btnTab.getWidth()-10, popupY+ LoadImageInterface.btnTab.getHeight()*2+ (Screen.cmdH * 4), LoadImageInterface.btnTab, LoadImageInterface.btnTab);
							roi = new Command( "Rời",this,  231291, null, 0, 0);
							roi.setPos(popupX + popupW+ LoadImageInterface.btnTab.getWidth()-10, popupY+ LoadImageInterface.btnTab.getHeight()*2 + Screen.cmdH*2, LoadImageInterface.btnTab, LoadImageInterface.btnTab);
							giaitan.img = roi.img = kich.img = LoadImageInterface.btnTab;
							giaitan.imgFocus = roi.imgFocus = kich.imgFocus = LoadImageInterface.btnTabFocus;
							giaitan.w = roi.w = kich.w = Image.getWidth(LoadImageInterface.btnTab);
							giaitan.h = roi.h = kich.h = Image.getHeight(LoadImageInterface.btnTabFocus);
						}
					}else{
						kich = new Command( "Kích",this,  270192, null, 0, 0);
						kich.setPos(popupX + popupW+ LoadImageInterface.btnTab.getWidth()-10, popupY+ LoadImageInterface.btnTab.getHeight()*2, LoadImageInterface.btnTab, LoadImageInterface.btnTab);
						giaitan = new Command( "Giải tán",this,  111111, null, 0, 0);
						giaitan.setPos(popupX + popupW+ LoadImageInterface.btnTab.getWidth()-10, popupY+ LoadImageInterface.btnTab.getHeight()*2+ (Screen.cmdH * 4), LoadImageInterface.btnTab, LoadImageInterface.btnTab);
						roi = new Command( "Rời",this,  231291, null, 0, 0);
						roi.setPos(popupX + popupW+ LoadImageInterface.btnTab.getWidth()-10, popupY+ LoadImageInterface.btnTab.getHeight()*2 + Screen.cmdH*2, LoadImageInterface.btnTab, LoadImageInterface.btnTab);
						giaitan.img = roi.img = kich.img = LoadImageInterface.btnTab;
						giaitan.imgFocus = roi.imgFocus = kich.imgFocus = LoadImageInterface.btnTabFocus;
						giaitan.w = roi.w = kich.w = Image.getWidth(LoadImageInterface.btnTab);
						giaitan.h = roi.h = kich.h = Image.getHeight(LoadImageInterface.btnTabFocus);
					}
				//}
				
			} 
			else {
//				if(indexRow<GameScreen.charnearByme.size()){
//					final Char c = (Char) GameScreen.charnearByme.elementAt(indexRow);
//	//				if (c.charID != Char.myChar().charID) {
//						center = new Command(mResources.SELECT, 12009);
//	//				}
//				}
//			}
		}
		
	}
	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub

		g.setColor(0xff000000,GameCanvas.opacityTab);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		GameCanvas.paint.paintFrameNaruto(popupX, popupY,widthGui,heightGui, g,true);

		xstart = popupX + 5;
		ystart = popupY + 40;

		if (GameScreen.hParty.size() == 0){
//			if(GameScreen.charnearByme.size() == 0){
//
//				for(int i = 0; i < 15; i++){
//					Char ch = new Char();
//					ch.charID = (short)i;
//					ch.cName = (i%2==0?"pkpro":"can1doithu")+" "+i;
//					ch.idParty = (short)-1;
//					GameScreen.charnearByme.addElement(ch);
//
//				}
//				
				mFont.tahoma_7_white.drawString(g, mResources.NOT_TEAM, popupX + widthGui / 2, popupY + 40, FontSys.CENTER);
//			}else{
//				g.setColor(0x001919);
//				g.fillRect(xstart - 2, ystart - 2, widthGui - 6, indexSize * 5 + 8);

//				resetTranslate(g);
//				scrMain.setStyle(GameScreen.charnearByme.size(), indexSize, xstart, ystart, widthGui - 3,heightGui-50, true, 1);
//				scrMain.setClip(g, xstart, ystart, widthGui - 3, heightGui-50);
//				indexRowMax = GameScreen.charnearByme.size();
//
//				int yBGFriend =0;
//				for(int i = 0; i < GameScreen.charnearByme.size(); i++){
//					Char c = (Char) GameScreen.charnearByme.elementAt(i);
//					Paint.PaintBGListQuest(xstart +35,ystart+yBGFriend,160,g);//new quest	
//					if (indexRow == i) {
//						Paint.PaintBGListQuestFocus(xstart +35,ystart+yBGFriend,160,g);
////						g.setColor(Paint.COLORLIGHT);
////						g.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 20, indexSize - 4);
////						g.setColor(0xffffff);
////						g.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 20, indexSize - 4);
//					} 
////					else {
////						g.setColor(Paint.COLORBACKGROUND);
////						g.fillRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 20, indexSize - 4);
////						g.setColor(0xd49960);
////						g.drawRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 20, indexSize - 4);
//
//					yBGFriend+=50;
////					}
//					if(c.idParty == -1){
//						mFont.tahoma_7_white.drawString(g, c.cName, xstart + widthGui/2, ystart + i * indexSize + indexSize / 2 - 6, 2,true);
//					}else{
//						mFont.tahoma_7_red.drawString(g, c.cName, xstart + widthGui/2, ystart + i * indexSize + indexSize / 2 - 6, 2,true);
//					}
//				}
//				GameCanvas.resetTrans(g);
//			}
		}
		else {
//			g.setColor(0x001919);
//			g.fillRect(xstart - 2, ystart - 2, widthGui - 6, indexSize * 5 + 8);

			resetTranslate(g);
			scrMain.setStyle(Party.vCharinParty.size(), 50, xstart, ystart, widthGui - 3, heightGui-50, true, 1);
			scrMain.setClip(g, xstart, ystart, widthGui - 3, heightGui-50);
//			Party party = (Party) hParty.get(Char.myChar().idParty+"");
//			if(party != null){
			int yBGFriend=0;
				indexRowMax = Party.vCharinParty.size();
				for(int i = 0; i < Party.vCharinParty.size(); i++){
					Char c = (Char) Party.vCharinParty.elementAt(i); 
//					if (indexRow == i) {
//						g.setColor(Paint.COLORLIGHT);
//						g.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 20, indexSize - 4);
//						g.setColor(0xffffff);
//						g.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 20, indexSize - 4);
//					} else {
//						g.setColor(Paint.COLORBACKGROUND);
//						g.fillRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 20, indexSize - 4);
//						g.setColor(0xd49960);
//						g.drawRect(xstart + 5, ystart + (i * indexSize) + 2, widthGui - 20, indexSize - 4);
//					}
					Paint.PaintBGListQuest(xstart +35,ystart+yBGFriend,160,g);//new quest	
					if (indexRow == i) {
//						g.setColor(Paint.COLORLIGHT);
//						g.fillRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
//						g.setColor(0xffffff);
//						g.drawRect(xstart + 5, ystart + (indexRow * indexSize) + 2, widthGui - 15, indexSize - 4);
//						btnChat.paint(g);
//						btnUnfriend.paint(g);
					
						Paint.PaintBGListQuestFocus(xstart +35,ystart+yBGFriend,160,g);
					}
					 g.drawImage(LoadImageInterface.charPic,xstart +45,ystart+yBGFriend+20,g.VCENTER|g.HCENTER);
						mFont.tahoma_7_white.drawString(g,"Level: "+c.clevel, xstart + 73, ystart+yBGFriend+24 , 0,true);
					if(c.isOnline)
					{
						g.drawImage(LoadImageInterface.imgName,xstart+100,ystart+yBGFriend+15,g.VCENTER|g.HCENTER,true);
						mFont.tahoma_7_white.drawString(g, c.cName, xstart + 73, ystart+yBGFriend+8 , 0,true);
					}
						
					else 
					{
						g.drawImage(LoadImageInterface.imgName,xstart+100,ystart+yBGFriend+15,g.VCENTER|g.HCENTER,true);
						mFont.tahoma_7_white.drawString(g, c.cName, xstart + 73,ystart +yBGFriend+8, 0,true);	
					}

					if(c != null){
						if(c.isLeader)
							mFont.tahoma_7_yellow.drawString(g, c.cName, xstart + 73,ystart +yBGFriend+8, 0);
						try {
							Part ph2 = GameScreen.parts[c.head];
							SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id, 
									xstart + 44,ystart +yBGFriend+20, 0, mGraphics.VCENTER|mGraphics.HCENTER);
						} catch (Exception e) {
							// TODO: handle exception
						}
//						else
//							mFont.tahoma_7_red.drawString(g, "Hoahoa"/*c.cName*/, xstart + widthGui/2, ystart + i * indexSize + 20, 2);
					}

					yBGFriend+=50;
				}
			}

		GameCanvas.resetTrans(g);
		if(indexRow!=-1&&Party.vCharinParty.size()>=2){
			if(Party.gI().isLeader){
				if(kich!=null) kich.paint(g);
				if(giaitan!=null) giaitan.paint(g);
			}
			if(roi!=null) roi.paint(g);
		}
			
		//paintNumCount(g);
		
//	}
	
		//paint name box 
		Paint.PaintBoxName("DANH SÁCH NHÓM",popupX+widthGui/2 - 50,popupY+10,100,g);
//	}
		super.paint(g);
	}

	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		Cout.println(getClass(), " iidAction "+idAction);
		switch (idAction) {
		case 111111: // giai tan
			Service.gI().removeParty(Type_Party.DISBAND_PARTY);
			break;
		case 270192: // kich ra khoi nhom
			Party party = (Party) GameScreen.hParty.get(Char.myChar().idParty+"");
			if (((Char) party.vCharinParty.elementAt(0)).charID == Char.myChar().charID) {
				if(indexRow<party.vCharinParty.size()){
					final Char c = (Char) party.vCharinParty.elementAt(indexRow);
					if (c.charID != Char.myChar().charID) {
						Service.gI().kickPlayeleaveParty((byte)Type_Party.KICK_OUT_PARTY, (short)c.charID);
	//					center = new Command(mResources.SELECT, 11080);
					}
				}
			}
			break;
		case 231291: // tu roi nhom
			Service.gI().leaveParty((byte)Type_Party.OUT_PARTY,(short)Char.myChar().charID);
			break;

		default:
			break;
		}
		
	}
	public void SetPosClose(Command cmdClose) {
		// TODO Auto-generated method stub
		scrMain.selectedItem = -1;
		cmdClose.setPos(popupX+widthGui-Image.getWidth(LoadImageInterface.closeTab), popupY, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
		
	}
}
