package com.sakura.thelastlegend.gui;


import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.real.Service;
import Objectgame.Char;
import Objectgame.Item;
import Objectgame.ItemTemplates;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;

/*
 * Paint and execute shop com.sakura.thelastlegend.gui
 */

public class ShopMain extends Screen implements IActionListener{

	public static Scroll scrMain = new Scroll();
	
	public mVector VecTabScreen = new mVector();
	int x,y,width=180,height=232,widthSub=170,heightSub=230,wKhoangCach=4;
	int xsub;
	int indexSize,xBegin,yBegin;
	int wScollReceived=100,hScollReceived=60;
	int indexRow=-1,indexRowUnRecive,witem = 34;
	public static int idMenu[],indexidmenu;
	public static String nameMenu[];
	public static String dis[];
	public static Command cmdShop[];
	public static Command cmdMua;
	public static Item itemshop[][];
	public TField tfsl;
	public static short idItemtemplate[];
	public mBitmap imgCoins1,imgCoins2;
	int xMenu,yMenu;
	int wMenu,hMenu;
	private mBitmap[] arrayBGShop=new mBitmap[25];
	public int selectTab = 0;
	int coutFc;
	
	int xM;//diem nam giua frame
	public ShopMain(int x, int y)
	{
		int wAll = width+widthSub+wKhoangCach+Image.getWidth(LoadImageInterface.btnTab);
//		this.x=(GameCanvas.w/2-wAll/2);
		this.y=(GameCanvas.h-y-heightSub<0?GameCanvas.h-heightSub:y);
		y= this.y;
//		this.xM=this.x+width/2;
//		x = this.x;

		xsub = (GameCanvas.w/2-wAll/2)<2?2:(GameCanvas.w/2-wAll/2);
		
//		xMenu=this.x+width-25;
		yMenu=y+40;
		int xdem = this.x;
		this.x = xsub+widthSub+wKhoangCach+5;
		xMenu=this.x+width-5;
//		xsub= x+width+wKhoangCach;
		wMenu=Image.getWidth(LoadImageInterface.btnTab);
		hMenu=Image.getHeight(LoadImageInterface.btnTab);
		
		indexSize=mFont.tahoma_7_yellow.getHeight()+2;
		
		ShopWeapon sWeapon= new ShopWeapon();
		VecTabScreen.addElement(sWeapon);
		
		ShopWeapon sWeapon1= new ShopWeapon();
		VecTabScreen.addElement(sWeapon1);
		
		ShopWeapon sWeapon2= new ShopWeapon();
		VecTabScreen.addElement(sWeapon2);
//		getItemList(0,idItemtemplate);
		LoadImage();
		//name[i], this,i+2, null, 0, 0
		creatMenuShop(nameMenu, cmdShop);
		cmdMua = new Command("Mua ngay",this, 1, null, 0, 0);
//		xsub,y,widthSub,heightSub
		cmdMua.setPos( xsub+widthSub/2- LoadImageInterface.img_use.getWidth()/2+7,y+heightSub-2* LoadImageInterface.img_use.getHeight()-2, LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		xBegin = xsub-wKhoangCach/2+4;
		yBegin = y+heightSub-5;
		tfsl = new TField();
		tfsl.width = LoadImageInterface.img_use.getWidth();
		tfsl.x = cmdMua.x;
		tfsl.y = cmdMua.y-25;
	}
	
	public void LoadImage()
	{
		for(int i=0;i<25;i++)
		{
			arrayBGShop[i]=GameCanvas.loadImage("/GuiNaruto/shop/shell_"+(i+1)+".png");
		}
	}
	public void update(){
		if(GameCanvas.gameTick%4 ==0){
			coutFc++;
			if(coutFc>2)
				coutFc =0;
		}
		if(imgCoins1==null){
			imgCoins1 = GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
			imgCoins2 = GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
		}
	}
	public void updateKey() {
		// TODO Auto-generated method stub
		scrMain.updatecm();
		ScrollResult s1 =  scrMain.updateKey();
		if(scrMain.selectedItem>=itemshop[indexidmenu].length)
			scrMain.selectedItem = -1;
		if(Screen.getCmdPointerLast(cmdMua))
			cmdMua.performAction();
		if(cmdShop!=null)
		for (int i = 0; i < cmdShop.length; i++) {
			if(Screen.getCmdPointerLast(cmdShop[i]))
			{
				cmdShop[i].performAction();
				GameCanvas.clearPointerEvent();
			}
		}
//		tfsl.update();
	}
	
	public void updatePointer() {

		int ySelect=yMenu;
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			Cout.println(getClass(), "   updatePointer    1  ");
			selectTab=0;
			GameCanvas.isPointerClick= false;
			
		}
		ySelect+=(5+hMenu);
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			selectTab=1;
			GameCanvas.isPointerClick= false;
			
		}
		ySelect+=(5+hMenu);
		if (GameCanvas.isPointer(xMenu, ySelect,wMenu,hMenu)) {
			selectTab=2;
			GameCanvas.isPointerClick= false;
		}
		
	}
	
	//paint bg human
	private void PaintBGShell(int x,int y,mGraphics g)
	{
		int wImage=Image.getWidth(arrayBGShop[0]);
		int index=0;
	
		for(int j=1;j<5;j++)
		{
			g.drawImage(arrayBGShop[index==3?4:index],x+j*wImage,y,0,true);
			index++;
		}
	}
	
	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		cmd.setPos(x+width-Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
		
	}
	
	public void paint(mGraphics g)
	{
//		GameScreen.resetTranslate(g);
		//tab
//		for(int i=0;i<VecTabScreen.size();i++)
//		{
//			g.drawImage(LoadImageInterface.btnTab, xMenu+9,yMenu+(5+Image.getHeight(LoadImageInterface.btnTab))*i, 0);
//		}
		
//		for(int i=0;i<VecTabScreen.size();i++)
//		{
//			mFont.tahoma_7b_white.drawString(g,nameMenu[i],xMenu+40,yMenu+6+(5+Image.getHeight(LoadImageInterface.btnTab))*i,g.HCENTER|g.VCENTER);
//		}
		//ShopMain.idMenu[i]
		g.setColor(0xff000000,GameCanvas.opacityTab);
		g.fillRect(0,0,GameCanvas.w,GameCanvas.h);
		g.disableBlending();
		if(cmdShop!=null)
		for(int i = 0; i < cmdShop.length; i++){
			if(cmdShop[i]==null) continue;
			if(i!=indexidmenu){
				cmdShop[idMenu[i]].paint(g);
			}
//			else cmdShop[i].paintFocus(g,true);
		}
		Shop questMain= (Shop) VecTabScreen.elementAt(selectTab);
		
		GameCanvas.paint.paintFrameNaruto(this.x,this.y, this.width,this.height,g);
		
		Paint.PaintBoxName(nameMenu[idMenu[indexidmenu]],x+55,20,width-110,g);
		
		Paint.SubFrame(xsub,y,widthSub,heightSub, g);//sub
		if(itemshop==null) return;
		if(scrMain.selectedItem>=0&&scrMain.selectedItem<itemshop[indexidmenu].length){
//		mFont.tahoma_7_white.drawString(g, "Số lượng: ", xsub+10,cmdMua.y-24, 0);
			if(cmdMua!=null)
				cmdMua.paint(g);
			//money
			g.drawImage(imgCoins1, xsub,yBegin-Image.getHeight(imgCoins1)-1,g.TOP|g.LEFT);
			if(itemshop!=null&&itemshop[indexidmenu]!=null&&scrMain.selectedItem!=-1
					&&itemshop[indexidmenu][scrMain.selectedItem]!=null)
//			mFont.tahoma_7b_white.drawString(g,itemshop[indexidmenu][scrMain.selectedItem].template.gia+"",
//					xsub+33,yBegin-Image.getHeight(imgCoins1)/2-2, 0);
				mFont.tahoma_7b_white.drawString(g,Char.myChar().xu+"",
				xsub+25,yBegin-Image.getHeight(imgCoins1)/2-2, 0);
			
			//gold
			g.drawImage(imgCoins2, xsub+(Image.getWidth(imgCoins2)+6),yBegin-Image.getHeight(imgCoins2)-1,g.TOP|g.LEFT);
			mFont.tahoma_7b_white.drawString(g, Char.myChar().luong+"",
					xsub+(Image.getWidth(imgCoins2)+6)+27,yBegin-Image.getHeight(imgCoins1)/2-2,0);
		}
//		paintNameItem(g, x+width+50 + 10,(y-30+ GameCanvas.h / 5) + 20, 170, name, colorName);
//		paintNameItem(g, x+width+50 + 10,(y-30+ GameCanvas.h / 5) + 30, longwidth, "Tấn công: 1000", colorName);
//		paintNameItem(g, x+width+50 + 10,(y-30+ GameCanvas.h / 5) + 40, longwidth, "Độ bền: 10", colorName);
//		paintNameItem(g, x+width+50 + 10,(y-30+ GameCanvas.h / 5) + 50, longwidth, "tắng sát thương vật lý 10%", colorName);
		//content
//		GameScreen.resetTranslate(g);
		scrMain.setStyleWH(itemshop[indexidmenu].length/4+(itemshop[indexidmenu].length%4!=0?1:0),witem, 50, 
				x+25, y+50, width-55, height-70, true, 4);
		scrMain.setClip(g,x, y+50, width, height-70);
		//new quest
		for (int i = 0; i < (itemshop[indexidmenu].length/4+(itemshop[indexidmenu].length%4!=0?1:0)); i++) {
			PaintBGShell(x-30,y+50*(i+1)+20,g);
		}
//		PaintBGShell(x-23,y+70,g);
//		PaintBGShell(x-23,y+120,g);
//		PaintBGShell(x-23,y+170,g);
		for (int i = 0; i < itemshop[indexidmenu].length; i++) {
			
			
//			TField.paintInputTf(g,false,
//					x+30+(i%4)*witem, y+(i/4+1)*50+28,
//					witem, 30, 
//					x+40+(i%4)*witem+14,y+(i/4+1)*50+16, "", "", LoadImageInterface.imgTf0, witem);
//			if(itemshop[indexidmenu][i].template.id != -1 && itemshop[indexidmenu][i].template.name != null){
//				mFont.tahoma_7_white.drawString(g, itemshop[indexidmenu][i].template.name,x+30+(i%4)*witem+17, y+(i/4+1)*50+witem-4,2);
//			}
			g.drawImage(LoadImageInterface.ImgItem,x+20+(i%4)*witem+17, y+(i/4+1)*50+14, mGraphics.VCENTER | mGraphics.HCENTER);
			
			if(itemshop[indexidmenu][i] != null&&itemshop[indexidmenu][i]!=null){
				itemshop[indexidmenu][i].paintItem(g,x+20+(i%4)*witem+17, y+(i/4+1)*50+14); 
			}
			
//			if(i==scrMain.selectedItem){
//				Paint.paintFocus(g,x+20+(i%4)*witem+17-LoadImageInterface.ImgItem.getWidth()/4,
//						y+(i/4+1)*50+14-LoadImageInterface.ImgItem.getWidth()/4
//						,LoadImageInterface.ImgItem.getWidth()-8,LoadImageInterface.ImgItem.getWidth()-8,coutFc);
//				if(GameCanvas.gameTick % 10 == 0)
//					g.drawImage(LoadImageInterface.imgFocusSelectItem1, x+20+(i%4)*witem+17, y+(i/4+1)*50+14,mGraphics.VCENTER|mGraphics.HCENTER);
//				else
//					g.drawImage(LoadImageInterface.imgFocusSelectItem0, x+20+(i%4)*witem+17, y+(i/4+1)*50+14,mGraphics.VCENTER|mGraphics.HCENTER);
//			}
		}
		if(scrMain.selectedItem>=0)
		Paint.paintFocus(g,
				x+20+(scrMain.selectedItem%4)*witem+14- LoadImageInterface.ImgItem.getWidth()/4,
				y+(scrMain.selectedItem/4+1)*50+11- LoadImageInterface.ImgItem.getWidth()/4
				, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
//		paintItem(g);
		
		
//		if (idSelect > -1 && Focus == INFO) {//focus
//			setPaintInfo();
//			
//			g.drawImage(LoadImageInterface.imgFocusSelectItem, (xBegin + ((idSelect % numW)*(Image.getWidth(LoadImageInterface.ImgItem)+2)))+2 , yBegin
//					+ (idSelect / numW) * (Image.getHeight(LoadImageInterface.ImgItem) + 2) +4 , 0);
//			Paint.SubFrame(xTab-widthSubFrame,yTab-30+ GameCanvas.h / 5,widthSubFrame,heightSubFrame, g);
//			paint.paintItemInfo(g, itemFocus, xTab-widthSubFrame + 10,yTab-20+ GameCanvas.h / 5);
//		
//
//		}
		GameCanvas.resetTrans(g);
		for(int i = 0; i < cmdShop.length; i++){
			if(cmdShop[i]==null) continue;
			if(i!=indexidmenu){
//				cmdShop[i].paint(g);
			}else cmdShop[idMenu[i]].paintFocus(g,true);
		}
		if(scrMain.selectedItem>=0&&scrMain.selectedItem<itemshop[indexidmenu].length){
			Paint.paintItemInfo(g, itemshop[indexidmenu][scrMain.selectedItem], xsub+10,y+10,true);
		}
		//tab focus
//		g.drawImage(LoadImageInterface.btnTabFocus,xMenu+9,yMenu+(5+Image.getHeight(LoadImageInterface.btnTabFocus))*selectTab, 0);
//		mFont.tahoma_7b_white.drawString(g,nameMenu[selectTab],xMenu+40,yMenu+6+(5+Image.getHeight(LoadImageInterface.btnTab))*selectTab,g.HCENTER|g.VCENTER);
//		if(scrMain.selectedItem>=0&&scrMain.selectedItem<itemshop[indexidmenu].length)
//		tfsl.paint(g);
	}
	
	public void creatMenuShop(String name[], Command menubtn[]){
		if(name!=null)
		for(int i = 0; i < name.length; i++){
			menubtn[i] = new Command(name[i], this,i+2, null, 0, 0);
			menubtn[i].setPos( xMenu-8,yMenu+(5+Image.getHeight(LoadImageInterface.btnTab))*i, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
		}
	}

	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		Cout.println("idAction  "+idAction);
		if(idAction!=1&&idAction!=100)
		{
			if(idAction-2<=nameMenu.length&&itemshop[idMenu[idAction-2]]==null)
				Service.gI().requestShop(idMenu[idAction-2]);
			else if(itemshop[idMenu[idAction-2]]!=null){
				indexidmenu = idMenu[idAction-2];
			}
		}
		switch(idAction){
			case 1: // mua item
				if(scrMain.selectedItem!=-1&&scrMain.selectedItem<itemshop[indexidmenu].length)
				{
					String textsl = tfsl.getText();
					GameCanvas.startYesNoDlg("Bạn có muốn mua "+itemshop[indexidmenu][scrMain.selectedItem].template.name
							+" với giá "+itemshop[indexidmenu][scrMain.selectedItem].template.gia+(itemshop[indexidmenu][scrMain.selectedItem].template.typeSell==0?"xu":"gold")+" ?",
							new Command("Có", this, 100, null), new Command("Không", GameCanvas.instance, 8882, null));
//					Service.gI().BuyItemShop(((byte)indexidmenu),(short)scrMain.selectedItem/*(short)itemshop[indexidmenu][scrMain.selectedItem].itemId*/);
				}
				break;
			case 100:
				//GameCanvas.startOKDlg("Đang mua "+itemshop[indexidmenu][scrMain.selectedItem].template.name);
				Service.gI().BuyItemShop(((byte)indexidmenu),(short)scrMain.selectedItem/*(short)itemshop[indexidmenu][scrMain.selectedItem].itemId*/);
				GameCanvas.endDlg();
				break;
		}

//		scrMain.selectedItem = -1;
	}
	
	public static void getItemList(int idmenu,short itemtemplate[]){
		if(itemtemplate==null) return;
		if(itemshop==null)
			itemshop = new Item[nameMenu.length][];
		itemshop[idmenu] = new Item[itemtemplate.length];
		for(int i = 0; i < itemtemplate.length; i++){
			itemshop[idmenu][i] = new Item(); 
			itemshop[idmenu][i].itemId = (short)itemtemplate[i];
			itemshop[idmenu][i].template = ItemTemplates.get((short)itemtemplate[i]);
		}
	}
	
	public void paintItem(mGraphics g){
		int wImage=24;
		int index=0;
		
		for(int i = 0; i < itemshop[indexidmenu].length; i++){
			if(itemshop[indexidmenu][i] != null){
				for(int j = 1; j < 5; j++){
					itemshop[indexidmenu][i].paintItem(g, x+j*wImage + (j * 20),y + 65);
				}
			}
		}
//		for(int j=1;j<4;j++)
//		{
//			g.drawImage(arrayBGShop[index],x+j*wImage - 24,y,0);
//			index++;
//		}
	}
}
