package com.sakura.thelastlegend.gui;

public interface IAction {
	  public void perform();
}
