package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.GameCanvas;

import com.sakura.thelastlegend.domain.model.ChatTextField;
import com.sakura.thelastlegend.domain.model.FrameImage;
import com.sakura.thelastlegend.domain.model.Screen;



public class iCommand {
	public String caption;
	public String[] subCaption;
	public IAction action;
	public Screen Pointer;
	public byte indexMenu, subIndex = -1;
	public boolean isSelect = false;
	int nframe, beginframe;
	FrameImage fraImgCaption, fraImageCmd;
	int wimgCaption, himgCaption, wstr, wimgCmd, himgCmd;
	public int xCmd = -1, yCmd = -1, frameCmd, xdich, ydich;
	public static int wButtonCmd = 70, hButtonCmd = 24;


	public void setPos(int x, int y, FrameImage fra, String caption) {
		this.caption = caption;
		xCmd = x;
		yCmd = y;
		fraImageCmd = fra;
		if (fraImageCmd != null) {
			wimgCmd = fraImageCmd.frameWidth;
			himgCmd = fraImageCmd.frameHeight;
			if (wimgCmd < 28)
				wimgCmd = 28;
			if (himgCmd < 28)
				himgCmd = 28;
		} else {
			wimgCmd = 70;
			himgCmd = hButtonCmd;
		}
	}



	public void perform() {
		if (action != null) {
//			action.perform();
//			GameCanvas.isPointerSelect = false;
			GameCanvas.clearKeyHold();
			GameCanvas.clearKeyPressed();
		} else {
			if (Pointer != null) {
				Pointer.commandPointer(indexMenu, subIndex);
			} else {
				if (GameCanvas.currentDialog != null) {
//					GameCanvas.currentDialog.commandTab(indexMenu, subIndex);
					GameCanvas.isPointerClick = false;
					GameCanvas.clearKeyHold();
					GameCanvas.clearKeyPressed();
					return;
				}
				if (ChatTextField.gI().isShow) {
//					ChatTextField.gI().commandTab(indexMenu, subIndex);
					return;
				}
//				}else if (GameCanvas.subDialog != null) {
//					GameCanvas.subDialog.commandTab(indexMenu, subIndex);
//					return;
				}
			}
			GameCanvas.isPointerClick = false;
			GameCanvas.clearKeyHold();
			GameCanvas.clearKeyPressed();
		}

	

	public void update() { }


	public void updatePointer() {
		if (isPosCmd()) {
			if (GameCanvas.isPointerDown /*|| GameCanvas.isPointerMove*/) {
				if (GameCanvas.isPoint(xCmd - wimgCmd / 2 - 5, yCmd - himgCmd
						/ 2 - 5, wimgCmd + 10, himgCmd + 10)) {
					frameCmd = 1;
					perform();
					GameCanvas.isPointerClick = false;
				} else{
					frameCmd = 0;
				}
			} else {
				frameCmd = 0;
			}
		}
	}
	
	

	public void updatePointer(int cmx, int cmy) {
		if (isPosCmd()) {
			if (GameCanvas.isPointerDown /*|| GameCanvas.isPointerMove*/) {
				if (GameCanvas.isPoint(xCmd - wimgCmd / 2 - 3 - cmx, yCmd
						- himgCmd / 2 - 3 - cmy, wimgCmd + 6, himgCmd + 6)) {
					frameCmd = 1;
				} else
					frameCmd = 0;
			} else {
				frameCmd = 0;
			}
			if (GameCanvas.isPointSelect(xCmd - wimgCmd / 2 - 3 - cmx, yCmd
					- himgCmd / 2 - 3 - cmy, wimgCmd + 6, himgCmd + 6)) {
				perform();
				GameCanvas.isPointerClick = false;
				frameCmd = 0;
			}
		}
	}

	public boolean isPosCmd() {
		return xCmd > 0 && yCmd > 0;
	}
}
