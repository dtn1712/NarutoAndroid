package com.sakura.thelastlegend.gui;


import java.util.Enumeration;

import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mHashtable;
import com.sakura.thelastlegend.lib.mImage;



public class ImageEffect {
	public static mHashtable hashImageEff = new mHashtable();
	long timeRemove;
	int IdImage;
	mBitmap img;

	public ImageEffect(int Id) {
		this.IdImage = Id;
		mImage imgg = mImage.createImage("/eff/g" + Id + ".png");
		if(imgg!=null) img = imgg.image;

//		timeRemove = GameCanvas.timeNow;
	}

	public static mBitmap setImage(int Id) {
		ImageEffect img = (ImageEffect) hashImageEff.get("" + Id);
		if (img == null) {
			img = new ImageEffect(Id);
			if(img.img!=null)
			hashImageEff.put("" + Id, img);
		} 
//		else
//			img.timeRemove = GameCanvas.timeNow;
		return img.img;
	}

	public static void SetRemove() {
		Enumeration k = hashImageEff.keys();
		while (k.hasMoreElements()) {
			String keyset = (String) k.nextElement();
			ImageEffect img = (ImageEffect) hashImageEff.get(keyset);
//			if ((GameCanvas.timeNow - img.timeRemove) / 1000 > 300) {
//				hashImageEff.remove(keyset);
//			}
		}
	}

	public static void SetRemoveAll() {
		hashImageEff.clear();
	}
}
