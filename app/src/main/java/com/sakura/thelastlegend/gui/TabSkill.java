package com.sakura.thelastlegend.gui;


import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.real.Service;
import screen.GameScreen;

import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import Objectgame.Char;
import Objectgame.Skill;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mBitmap;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.QuickSlot;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Scroll;
import com.sakura.thelastlegend.domain.model.ScrollResult;
import com.sakura.thelastlegend.domain.model.SmallImage;

public class TabSkill  extends MainTabNew implements IActionListener{

	

	public mBitmap imgBg,imgBgText,imgskill;
	public int ypaintText;
	public int[][] xyListItem = new int[15][];
	public Command cmdGan,cmdNangCap,cmdKhaiMo;
	public int winfoSkill = 185,hinfo=60;
	public Scroll scrInfo = new Scroll();
	public int indexKill = -1,coutFc;
	public static boolean isDaHoc = true;
	public static SkillTemplate skillIndex;
	
	public Char demo;
	
	public TabSkill(String string) {
		// TODO Auto-generated constructor stub
		for (int i = 0; i < xyListItem.length; i++) {
			xyListItem[i] = new int[2];
			xyListItem[i][0] = xTab+14+(i%5)*((widthFrame-80-Image.getWidth(LoadImageInterface.ImgItem)*5)/6)
					+(i%5)*Image.getWidth(LoadImageInterface.ImgItem);
			xyListItem[i][1] = yTab+(i/5)*(Image.getWidth(LoadImageInterface.ImgItem)+
				(widthFrame-50-Image.getWidth(LoadImageInterface.ImgItem)*6)/7)+35;
		}
		ypaintText = xyListItem[xyListItem.length-1][1]
				+Image.getWidth(LoadImageInterface.ImgItem)
				+5;
		
		cmdGan = new Command("Gán", this, 2000, null);
		cmdGan.setPos(xTab+wtab2/2-5* LoadImageInterface.img_use.getWidth()/4,
				yTab+heightFrame- LoadImageInterface.img_use.getHeight()-8,
				LoadImageInterface.img_use , LoadImageInterface.img_use_focus);

		cmdNangCap = new Command("Nâng cấp", this, 2001, null);
		cmdNangCap.setPos(xTab+wtab2/2+ LoadImageInterface.img_use.getWidth()/4,
				yTab+heightFrame- LoadImageInterface.img_use.getHeight()-8,
				LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		

		cmdKhaiMo = new Command("Khai mở", this, 2002, null);
		cmdKhaiMo.setPos(xTab+wtab2/2- LoadImageInterface.img_use.getWidth()/2,
				yTab+heightFrame- LoadImageInterface.img_use.getHeight()-8,
				LoadImageInterface.img_use , LoadImageInterface.img_use_focus);
		
		skillIndex = (SkillTemplate)SkillTemplates.hSkilltemplate.get(new Short((short) 0));
		demo = new Char();
		demo.head = Char.myChar().head;
		demo.body = Char.myChar().body;
		demo.leg = Char.myChar().leg;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		if(imgBg==null){
			imgBg = GameCanvas.loadImage("/GuiNaruto/skill/bg.png");
			imgBgText = GameCanvas.loadImage("/GuiNaruto/skill/bgtext.png");
			imgskill = GameCanvas.loadImage("/GuiNaruto/createChar/itemCreateC.png");
		}
		if(GameCanvas.gameTick%4 ==0){
			coutFc++;
			if(coutFc>2)
				coutFc =0;
		}
//		Char.myChar().update();
		if(skillIndex!=null&&GameCanvas.gameTick%53==0&&demo.indexSkill<=0){
			demo.statusMe = Char.A_ATTK;
			demo.setSkillPaint(GameScreen.sks[skillIndex.ideff], Skill.ATT_STAND);
		}
		demo.update();
	}

	@Override
	public void updateKey() {
		// TODO Auto-generated method stub
		ScrollResult s1 = scrInfo.updateKey();
		scrInfo.updatecm();
		for (int i = 0; i < xyListItem.length; i++) {
			if(i<SkillTemplates.hSkilltemplate.size()&&GameCanvas.isPointerClick&&GameCanvas.isPoint(xyListItem[i][0], xyListItem[i][1],
					LoadImageInterface.ImgItem.getWidth(), LoadImageInterface.ImgItem.getHeight())){
				GameCanvas.clearPointerEvent();
				indexKill = i;

				skillIndex = (SkillTemplate)SkillTemplates.hSkilltemplate.get(""+indexKill);
//				Cout.println(" indexKill "+indexKill+" skillIndex "+skillIndex.ideff);
				demo.setSkillPaint(GameScreen.sks[skillIndex.ideff], Skill.ATT_STAND);
				if(skillIndex!=null) isDaHoc = skillIndex.level>=0?true:false;
				if(isDaHoc){

					if(skillIndex.level>=skillIndex.nlevelSkill-1){
						cmdGan.x = cmdKhaiMo.x;
					}else 
						cmdGan.x = xTab+wtab2/2-5* LoadImageInterface.img_use.getWidth()/4;
					
				}
			}
		}
		if(skillIndex!=null){
			if(skillIndex.level==-1) {
				if(Screen.getCmdPointerLast(cmdKhaiMo)){
					cmdKhaiMo.performAction();
					GameCanvas.clearPointerEvent();
					Service.gI().HocSkill((byte)0, skillIndex.id);
				}
			}else{
				if(Screen.getCmdPointerLast(cmdGan)){
					cmdGan.performAction();
					GameCanvas.clearPointerEvent();
				}

				if(skillIndex.level<skillIndex.nlevelSkill-1)
					if(Screen.getCmdPointerLast(cmdNangCap)){
						cmdNangCap.performAction();
						GameCanvas.clearPointerEvent();
						Service.gI().HocSkill((byte)1, skillIndex.id);
					}
			}
		}
	}

	@Override
	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
//		xTab-widthSubFrame,yTab,widthSubFrame,heightSubFrame
		GameCanvas.resetTrans(g);
		g.drawImage(imgBg, xTab-widthSubFrame/2-10, yTab+heightFrame/2, mGraphics.VCENTER|mGraphics.HCENTER);
		
		for (int i = 0; i < xyListItem.length; i++) {
			g.drawImage(LoadImageInterface.ImgItem, xyListItem[i][0], xyListItem[i][1], mGraphics.TOP|mGraphics.LEFT);
			
		}
		for (int i = 0; i < SkillTemplates.hSkilltemplate.size(); i++) {
			SkillTemplate skillIndexx = (SkillTemplate)SkillTemplates.hSkilltemplate.get(""+i);
			if(skillIndexx!=null){
				SmallImage.drawSmallImage(g,skillIndexx.iconId, xyListItem[i][0]+ LoadImageInterface.ImgItem.getWidth()/2,
						xyListItem[i][1]+ LoadImageInterface.ImgItem.getHeight()/2, 0, mGraphics.VCENTER | mGraphics.HCENTER,true);
//				if(i==indexKill)
//				if(GameCanvas.gameTick%10 == 0)
//					g.drawImage(LoadImageInterface.imgFocusSelectItem1,
//							xyListItem[i][0]+LoadImageInterface.ImgItem.getWidth()/2,
//							xyListItem[i][1]+LoadImageInterface.ImgItem.getHeight()/2,  mGraphics.VCENTER|mGraphics.HCENTER);
//				else
//					g.drawImage(LoadImageInterface.imgFocusSelectItem0,
//							xyListItem[i][0]+LoadImageInterface.ImgItem.getWidth()/2,
//							xyListItem[i][1]+LoadImageInterface.ImgItem.getHeight()/2,  mGraphics.VCENTER|mGraphics.HCENTER);
				if(skillIndexx.level==-1){
					g.setColor(0xff000000,50);
					g.fillRect(xyListItem[i][0]+2,
						xyListItem[i][1]+2, 
						LoadImageInterface.ImgItem.getWidth()-4,
						LoadImageInterface.ImgItem.getHeight()-4);
					g.disableBlending();
				}
			}
		}
		if(indexKill>=0)
		Paint.paintFocus(g,
				xyListItem[indexKill][0]+4,
				xyListItem[indexKill][1]+4
				, LoadImageInterface.ImgItem.getWidth()-9, LoadImageInterface.ImgItem.getWidth()-9,coutFc);
		if(imgBgText!=null)
		g.drawImage(imgBgText, xTab+wtab2/2, ypaintText+imgBgText.getHeight()/2,  mGraphics.VCENTER|mGraphics.HCENTER);
		if(imgskill!=null)
		g.drawImage(imgskill,
				xTab+wtab2/2-imgBgText.getWidth()/2+4+imgskill.getWidth()/2, 
				ypaintText+4+imgskill.getHeight()/2, mGraphics.VCENTER|mGraphics.HCENTER);
//		if(GameCanvas.gameTick % 10 == 0)
//			g.drawImage(LoadImageInterface.imgFocusSelectItem1,
//					xTab+wtab2/2-imgBgText.getWidth()/2+4+imgskill.getWidth()/2, 
//					ypaintText+4+imgskill.getHeight()/2,  mGraphics.VCENTER|mGraphics.HCENTER);
//		else
//			g.drawImage(LoadImageInterface.imgFocusSelectItem0,
//					xTab+wtab2/2-imgBgText.getWidth()/2+4+imgskill.getWidth()/2, 
//					ypaintText+4+imgskill.getHeight()/2,  mGraphics.VCENTER|mGraphics.HCENTER);
		
		if(skillIndex!=null){
			if(isDaHoc){
				cmdGan.paint(g);
				if(skillIndex.level<skillIndex.nlevelSkill-1)
				cmdNangCap.paint(g);
			}else cmdKhaiMo.paint(g);
			SmallImage.drawSmallImage(g,skillIndex.iconId, xTab+wtab2/2-imgBgText.getWidth()/2+4+imgskill.getWidth()/2, 
					ypaintText+4+imgskill.getHeight()/2, 0, mGraphics.VCENTER | mGraphics.HCENTER,true);
			mFont.tahoma_7_yellow.drawString(g, skillIndex.name,
					xTab+wtab2/2-imgBgText.getWidth()/2+6+imgskill.getWidth(),
					ypaintText+4, 0,true);
			mFont.tahoma_7_blue1.drawString(g, "Cấp: "+((skillIndex.level<0?0:skillIndex.level)+1),
					xTab+wtab2/2-imgBgText.getWidth()/2+6+imgskill.getWidth(),
					ypaintText+16, 0,true);
			scrInfo.setStyle(skillIndex.description.length+2+(skillIndex.level+1<skillIndex.nlevelSkill&&skillIndex.id!=0?3:0), 10, xTab+wtab2/2-imgBgText.getWidth()/2+4+imgskill.getWidth()/2, 
					ypaintText+30, imgBgText.getWidth()-8, imgBgText.getHeight()-32, true, 1);
			scrInfo.setClip(g, xTab+wtab2/2-imgBgText.getWidth()/2+4, 
					ypaintText+30, imgBgText.getWidth()-8, imgBgText.getHeight()-32);
			try {
				mFont.tahoma_7_white.drawString(g,"Sát thương: "+skillIndex.dame[skillIndex.level<0?0:skillIndex.level],
						xTab+wtab2/2-imgBgText.getWidth()/2+6,
						ypaintText+30, 0,true);
				mFont.tahoma_7_white.drawString(g,"Phạm vi: "+skillIndex.rangelv[(skillIndex.level<0?1:skillIndex.level)],
						xTab+wtab2/2-imgBgText.getWidth()/2+6,
						ypaintText+40, 0,true);
			} catch (Exception e) {
				// TODO: handle exception
				//Cout.println(skillIndex.level+" skillIndex.dame  "+skillIndex.dame.length+"  "+skillIndex.nlevelSkill);
			}
			
			
			for (int i = 0; i < skillIndex.description.length; i++) {
				mFont.tahoma_7_white.drawString(g, skillIndex.description[i],
						xTab+wtab2/2-imgBgText.getWidth()/2+6,
						ypaintText+50+i*10, 0,true);
			}
			if(skillIndex.level+1<skillIndex.nlevelSkill){
				mFont.tahoma_7_yellow.drawString(g,"Cấp:"+((skillIndex.level<0?0:skillIndex.level)+2),
						xTab+wtab2/2-imgBgText.getWidth()/2+6,
						ypaintText+50+10*skillIndex.description.length, 0,true);
				try {
					mFont.tahoma_7_white.drawString(g,"Sát thương: "+skillIndex.dame[(skillIndex.level<0?0:skillIndex.level)+1],
							xTab+wtab2/2-imgBgText.getWidth()/2+6,
							ypaintText+60+10*skillIndex.description.length, 0,true);
					mFont.tahoma_7_white.drawString(g,"Phạm vi: "+skillIndex.rangelv[(skillIndex.level<0?0:skillIndex.level)+1],
							xTab+wtab2/2-imgBgText.getWidth()/2+6,
							ypaintText+70+10*skillIndex.description.length, 0,true);
				
				} catch (Exception e) {
					// TODO: handle exception
					Cout.println(skillIndex.nlevelSkill+" "+skillIndex.dame.length+" skillIndex.dame "+((skillIndex.level<0?0:skillIndex.level)+1));
					
				}
			}
		}
		GameCanvas.resetTrans(g);
//		g.fillRect(xTab, yTab, widthFrame, heightFrame);
		if (demo.skillPaint != null ) {
			int dir = demo.cdir;
			demo.cdir = 1;
			demo.paintCharWithSkill(g, demo.indexSkill,xTab-widthSubFrame/2-5, yTab+3*heightFrame/4+5);
			demo.cdir = dir;
		} else {
			demo.paintChar(g,xTab-widthSubFrame/2-5, yTab+3*heightFrame/4+30);
		}
		super.paint(g);
	}

	@Override
	public void updatePointer() {
		// TODO Auto-generated method stub
	}
	@Override
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case 2000:
			mVector listKey = new mVector();
			for (int i = 0; i < 5; i++) {
				Command cmd = new Command("Key "+(i+1), this, 2010+i, null);
				listKey.add(cmd);
			}
			GameCanvas.menu.startAt(listKey, 0);
			break;
		case 2010:
			Char.myChar().mQuickslot[0].setIsSkill(skillIndex.id, skillIndex.typebuffSkill!=Skill.SKILL_CLICK_USE_BUFF?false:true);
			QuickSlot.SaveRmsQuickSlot();
			break;
		case 2011:
			Cout.println("setSkill  "+idAction);
			Char.myChar().mQuickslot[1].setIsSkill(skillIndex.id, skillIndex.typebuffSkill!=Skill.SKILL_CLICK_USE_BUFF?false:true);
			QuickSlot.SaveRmsQuickSlot();
			break;
		case 2012:
			Cout.println("setSkill  "+idAction);
			Char.myChar().mQuickslot[2].setIsSkill(skillIndex.id, skillIndex.typebuffSkill!=Skill.SKILL_CLICK_USE_BUFF?false:true);
			QuickSlot.SaveRmsQuickSlot();
			break;
		case 2013:
			Cout.println("setSkill  "+idAction);
			Char.myChar().mQuickslot[3].setIsSkill(skillIndex.id, skillIndex.typebuffSkill!=Skill.SKILL_CLICK_USE_BUFF?false:true);
			QuickSlot.SaveRmsQuickSlot();
			break;
		case 2014:
			Cout.println("setSkill  "+idAction);
			Char.myChar().mQuickslot[4].setIsSkill(skillIndex.id, skillIndex.typebuffSkill!=Skill.SKILL_CLICK_USE_BUFF?false:true);
			QuickSlot.SaveRmsQuickSlot();
			break;
		}
		super.perform(idAction, p);
	}
}
