package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.InfoItem;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.mFont;
import screen.GameScreen;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.Sprite;

public class QShortQuest implements IActionListener{
	
	int width=100,height=100;
	int x=GameCanvas.w-width ,y=GameCanvas.h/2-height/2-35;//x,y
	
	int xMove,limMove=70;
	Command bntOpen;
	boolean moveClose,moveOpen;
	int v;//gia toc tang toc do move
	
	public QShortQuest()
	{
		bntOpen= new Command("", this, Constants.BUTTON_OPEN, null, 0, 0);
		bntOpen.setPos(x, y+height/2-Image.getHeight(LoadImageInterface.imgShortQuest)/2, LoadImageInterface.imgShortQuest_Close, LoadImageInterface.imgShortQuest_Close);
	}
	
	public void Update()
	{
		if(moveClose)
		{
			if(xMove>limMove)
			{
				v=0;
				xMove +=GameCanvas.w - ( bntOpen.x + Image.getWidth(LoadImageInterface.imgShortQuest));
			}
			else
			{
				xMove+=v;
				v++;
			}
		}else
		{
			
			if(xMove<=0)
			{
				v=0;
			}
			else
			{
				xMove+=v;
				v--;
			}
		}
	}
	public void UpdateKey()
	{
			if (/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(bntOpen)) {
				if (bntOpen != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					if (bntOpen != null)
						bntOpen.performAction();
				}
			}
	}
	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		switch (idAction) {
		case Constants.BUTTON_OPEN:
			if(!moveClose)
			{
				moveClose=true;
//				bntOpen.setPos(x, y+height/2-LoadImageInterface.imgShortQuest.getHeight()/2 + 27, LoadImageInterface.imgShortQuest_Close, LoadImageInterface.imgShortQuest_Close);
				bntOpen.setPos(x, y+height/2-Image.getHeight(LoadImageInterface.imgShortQuest)/2, LoadImageInterface.imgShortQuest, LoadImageInterface.imgShortQuest_Close);
			}
			else
			{
				moveClose=false;
				bntOpen.setPos(x, y+height/2-Image.getHeight(LoadImageInterface.imgShortQuest)/2, LoadImageInterface.imgShortQuest_Close, LoadImageInterface.imgShortQuest_Close);
			}
			break;

		default:
			break;
		}
	}
	public void Paint (mGraphics g) 
	{
		GameScreen.resetTranslate(g);
		PaintFrameChat(g,GuiQuest.listNhacNv.size()*2-1);
//		g.setColor(0x001919);
//		g.fillRect(x+xMove+Image.getWidth(LoadImageInterface.imgShortQuest),y, width,height);
		for (int i = 0; i < GuiQuest.listNhacNv.size(); i++) {
			InfoItem infonv = (InfoItem)GuiQuest.listNhacNv.elementAt(i);
			if(infonv!=null)
				mFont.tahoma_7_white.drawString(g, infonv.s, x+4+xMove+Image.getWidth(LoadImageInterface.imgShortQuest),y+4+i*(Screen.ITEM_HEIGHT+2), 0);
		}
		bntOpen.x=x+xMove;
		bntOpen.y=bntOpen.y;
		bntOpen.paint(g,2);//paint button open-close
		
	}

	private void PaintFrameChat(mGraphics g,int h)
	{
		int xpaint = x+Image.getWidth(LoadImageInterface.imgShortQuest)-1;
		int w = 10;
		h = h<8?8:h;
		//first row
				g.drawImage(LoadImageInterface.imgChatConner, xpaint+xMove, y, mGraphics.TOP | mGraphics.LEFT);
				for(int i=1;i<w;i++)
				{
					g.drawImage(LoadImageInterface.imgChatRec, xpaint+i*(Image.getWidth(LoadImageInterface.imgChatRec))+xMove, y, 0,true);
				}
				g.drawRegion(LoadImageInterface.imgChatConner, 0, 0,Image.getWidth(LoadImageInterface.imgChatConner),
						Image.getHeight(LoadImageInterface.imgChatConner), Sprite.TRANS_MIRROR, xpaint +w*(Image.getWidth(LoadImageInterface.imgChatConner))+xMove, y,mGraphics.TOP | mGraphics.LEFT);
				
				//paint all rac of frame
				for(int i=1;i<h;i++)
					for(int j=0;j<w;j++)
					{
						g.drawImage(LoadImageInterface.imgChatRec, xpaint+j*(Image.getWidth(LoadImageInterface.imgChatRec))+xMove, y+Image.getHeight(LoadImageInterface.imgChatRec)*i, 0,true);
					}
				
				
				//g.drawImage(LoadImageInterface.imgChatConner, xpaint+xMove, y+Image.getHeight(LoadImageInterface.imgChatRec)*(h), mGraphics.TOP | mGraphics.LEFT);
				g.drawRegion(LoadImageInterface.imgChatConner, 0, 0, Image.getHeight(LoadImageInterface.imgChatRec), Image.getHeight(LoadImageInterface.imgChatRec),
				1,xpaint+xMove, y+Image.getHeight(LoadImageInterface.imgChatRec)*(h), mGraphics.TOP | mGraphics.LEFT);
				for(int i=1;i<w;i++)
				{
					g.drawImage(LoadImageInterface.imgChatRec, xpaint+i*(Image.getWidth(LoadImageInterface.imgChatRec))+xMove, y+Image.getHeight(LoadImageInterface.imgChatRec)*(h), 0,true);
				}
				
				g.drawRegion(LoadImageInterface.imgChatRec, 0, 0,Image.getWidth(LoadImageInterface.imgChatRec),
						Image.getHeight(LoadImageInterface.imgChatRec), Sprite.TRANS_MIRROR, xpaint +w*(Image.getWidth(LoadImageInterface.imgChatRec))+xMove,y+Image.getHeight(LoadImageInterface.imgChatRec)*(h),mGraphics.TOP | mGraphics.LEFT);
			}
	


}
