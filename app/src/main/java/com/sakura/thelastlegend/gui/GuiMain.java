package com.sakura.thelastlegend.gui;


import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.lib.LoadImageInterface;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.mFont;
import com.sakura.thelastlegend.lib.mSystem;
import com.sakura.thelastlegend.lib.mVector;
import com.sakura.thelastlegend.domain.model.IActionListener;
import com.sakura.thelastlegend.domain.model.Info;
import com.sakura.thelastlegend.domain.model.MsgDlg;
import com.sakura.thelastlegend.domain.model.Part;
import com.sakura.thelastlegend.domain.model.QuickSlot;
import com.sakura.thelastlegend.domain.model.Screen;
import com.sakura.thelastlegend.domain.model.SmallImage;
import com.sakura.thelastlegend.domain.model.Sprite;
import com.sakura.thelastlegend.domain.model.Type_Chat;
import com.sakura.thelastlegend.real.Service;
import screen.GamePad;
import screen.GameScreen;
import screen.LoginScreen;
import Objectgame.Char;
import Objectgame.ChatPrivate;
import Objectgame.Item;
import Objectgame.ItemMap;
import Objectgame.ItemThucAn;
import Objectgame.Mob;
import Objectgame.Npc;
import Objectgame.OtherChar;
import Objectgame.SkillTemplate;
import Objectgame.SkillTemplates;
import Objectgame.TileMap;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

public class GuiMain extends Screen implements IActionListener{
	public static int cmdBarX, cmdBarY, cmdBarW, cmdBarLeftW, cmdBarRightW, cmdBarCenterW, hpBarX, hpBarY, hpBarW, mpBarW, expBarW, lvPosX,
	moneyPosX, hpBarH, girlHPBarY;
	public static int xL, yL; // left
	public static int xC, yC;
	public static int xR, yR; // right
	public static int xF, yF; // fire
	public static int xU, yU; // up
	public static int xCenter, yCenter;
	Command btnchangeSkill;
	Command btnBatTestSkill;
	Command btnChangeFocus;
	Command btnUseHp,btnUseMp;
	Command bntCharBoard;
	Command btnChatMap;
	public Command bntAttack;
	public Command bntAttack_1;
	public Command bntAttack_2;
	public Command bntAttack_3;
	public Command bntAttack_4;
	public static GamePad gamePad;
	public static boolean isAnalog = false;
	
	int xMove,limMove=45;
	public boolean moveClose,moveOpen;
	int v;//gia toc tang toc do move
	int y=GameCanvas.h-43;
	int timeoff=10;//30s
	long timestart;
	public static boolean isTestSkill,isPaintOjectMap=true;
	public static int indexSkillTest =0;
	public int[][] xySkill = new int[5][];
	public static mVector vecItemOther = new mVector();
	public static TField tfchatMap;
	boolean isPayment;
	
	//menu icon
	public MenuIcon menuIcon= new MenuIcon(GameCanvas.hw-215,GameCanvas.h-50);
	
	public GuiMain()
	{
		int xAttack=GameCanvas.w-50/*LoadImageInterface.imgAttack_1.getWidth()*/-10;
		int yAttack=GameCanvas.h - 50/*LoadImageInterface.imgAttack_1.getHeight()*/-10;
		
		for (int i = 0; i < xySkill.length; i++) {
			xySkill[i] = new int[2];
		}
		bntCharBoard= new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
		bntCharBoard.setPos(1,1, LoadImageInterface.imgCharacter_info, LoadImageInterface.imgCharacter_info);
		
		btnchangeSkill= new Command("ChangeKill", this, 20, null, 0, 0);
		btnchangeSkill.setPos(GameCanvas.w-60,1, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
		
		btnBatTestSkill= new Command("BatTest", this, 21, null, 0, 0);
		btnBatTestSkill.setPos(GameCanvas.w-120,1, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
		
		btnUseHp =  new Command("", this, 31, null, 0, 0);
		btnUseHp.setPos(GameCanvas.w- LoadImageInterface.btnUseHP[0].getHeight()*2-10,GameCanvas.h-115, LoadImageInterface.btnUseHP[0], LoadImageInterface.btnUseHP[1]);
		
		btnUseMp =  new Command("", this, 32, null, 0, 0);
		btnUseMp.setPos(GameCanvas.w- LoadImageInterface.btnUseMP[0].getHeight()-5,GameCanvas.h-115, LoadImageInterface.btnUseMP[0], LoadImageInterface.btnUseMP[1]);
		
		
		
		btnChangeFocus =  new Command("", this, 30, null, 0, 0);
		btnChangeFocus.setPos(GameCanvas.w- LoadImageInterface.btnUseHP[0].getHeight()*2-15- LoadImageInterface.changeFocus[0].getHeight(),GameCanvas.h-115, LoadImageInterface.changeFocus[0], LoadImageInterface.changeFocus[1]);
		//0, 68, 30, 30
		btnChatMap=  new Command("", this, 33, null, 0, 0);
		btnChatMap.setPos(35,
				73- LoadImageInterface.btnChatMap[0].getHeight()/2, LoadImageInterface.btnChatMap[0], LoadImageInterface.btnChatMap[1]);
		
		
		xySkill[0][0] = xAttack+ LoadImageInterface.imgAttack_1.getWidth()/2;xySkill[0][1] = yAttack+ LoadImageInterface.imgAttack_1.getHeight()/2;
		bntAttack= new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
		bntAttack.setPos(xAttack,yAttack, LoadImageInterface.imgAttack_1, LoadImageInterface.imgAttack_1);
		
		xAttack=xAttack-Image.getWidth(LoadImageInterface.imgAttack);
		yAttack=yAttack+Image.getHeight(LoadImageInterface.imgAttack_1)/2;
		xySkill[1][0] = xAttack+ LoadImageInterface.imgAttack.getWidth()/2;xySkill[1][1] = yAttack+ LoadImageInterface.imgAttack.getHeight()/2;
		bntAttack_1= new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
		bntAttack_1.setPos(xAttack,yAttack, LoadImageInterface.imgAttack, LoadImageInterface.imgAttack);
		
		xAttack=xAttack+5;
		yAttack=yAttack-Image.getHeight(LoadImageInterface.imgAttack)-5;
		xySkill[2][0] = xAttack+ LoadImageInterface.imgAttack.getWidth()/2;xySkill[2][1] = yAttack+ LoadImageInterface.imgAttack.getHeight()/2;
		bntAttack_2= new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
		bntAttack_2.setPos(xAttack,yAttack, LoadImageInterface.imgAttack, LoadImageInterface.imgAttack);
		
		xAttack=xAttack+Image.getWidth(LoadImageInterface.imgAttack)/3+10;
		yAttack=yAttack-Image.getHeight(LoadImageInterface.imgAttack)+5;
		xySkill[3][0] = xAttack+ LoadImageInterface.imgAttack.getWidth()/2;xySkill[3][1] = yAttack+ LoadImageInterface.imgAttack.getHeight()/2;
		bntAttack_3= new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
		bntAttack_3.setPos(xAttack,yAttack, LoadImageInterface.imgAttack, LoadImageInterface.imgAttack);
		
		xAttack=xAttack+Image.getWidth(LoadImageInterface.imgAttack)+5;
		yAttack=yAttack;
		xySkill[4][0] = xAttack+ LoadImageInterface.imgAttack.getWidth()/2;xySkill[4][1] = yAttack+ LoadImageInterface.imgAttack.getHeight()/2;
		bntAttack_4= new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
		bntAttack_4.setPos(xAttack,yAttack, LoadImageInterface.imgAttack, LoadImageInterface.imgAttack);
		

		tfchatMap = new TField();
		tfchatMap.x = GameCanvas.w/2-40;
		tfchatMap.y = GameCanvas.h-25;
		tfchatMap.isFocus = false;
		
		tfchatMap.width = 80;
		tfchatMap.height = ITEM_HEIGHT + 2;
//		tfPoint.doChangeToTextBox();
		tfchatMap.setMaxTextLenght(40);

	}
	public static int indexshow;
	public void update()
	{
//		updateKey();
//		updatePointer();
//		if(GameScreen.isShowFocus){
//			if(GameScreen.ypaintFocus<0)
//			indexshow++;
//			GameScreen.ypaintFocus+=indexshow;
//			if(GameScreen.ypaintFocus>0) GameScreen.ypaintFocus = 0;
//		}else {
//			if(GameScreen.ypaintFocus>-LoadImageInterface.imgfocusActor.getHeight())
//			indexshow++;
//			GameScreen.ypaintFocus-=indexshow;
//			if(GameScreen.ypaintFocus<-LoadImageInterface.imgfocusActor.getHeight()) GameScreen.ypaintFocus = -LoadImageInterface.imgfocusActor.getHeight();
//		}
		for (int i = 0; i < vecItemOther.size(); i++) {
			ItemThucAn item = (ItemThucAn)vecItemOther.elementAt(i);
			item.update();
		}
		if(moveClose)
		{
			if(xMove>limMove)
			{
				v=0;
			}
			else
			{
				xMove+=v;
				v++;
			}
			
			
		}else
		{
			
			if(xMove<=0)
			{
				v=0;
			}
			else
			{
				xMove+=v;
				v--;
			}
			if((mSystem.currentTimeMillis()-timestart)/1000>timeoff){
				moveClose = true;
				if(menuIcon.subMenu!=null) menuIcon.cmdClose.performAction();
			}
		}
		if(tfchatMap.isFocus){
			tfchatMap.update();
		}
		if(tfchatMap.isOKReturn){
			tfchatMap.isOKReturn = false;
			try {
				Service.gI().chat(tfchatMap.getText(),Type_Chat.CHAT_MAP);
//				Service.gI().requestADD_BASE_POINT((byte)indexFocus, (short)diem);
				tfchatMap.setText("");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if(Char.myChar().statusMe==Char.A_DEAD||Char.myChar().statusMe==Char.A_DEADFLY)
			return;
		if(GameCanvas.gameScreen !=null&&GameCanvas.gameScreen.guiChatClanWorld!=null&&!GameCanvas.gameScreen.guiChatClanWorld.moveClose)
		menuIcon.update();
		
	}
	
	public void UpdateKey()
	{
		if(tfchatMap.isFocus)
		tfchatMap.update();
		if(Char.myChar().statusMe==Char.A_DEAD||Char.myChar().statusMe==Char.A_DEADFLY)
			return;
		boolean isPress = false;
		if(!MenuIcon.isShowTab&&!GameScreen.gI().guiChatClanWorld.moveClose&&moveClose){
			
			isPress = updateKeyTouchControl();
//			updateRightCOntrol();
		}
		if (!GameCanvas.gameScreen.guiChatClanWorld.moveClose&&menuIcon.indexpICon==0&&/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(bntCharBoard)) {

			if (bntCharBoard != null) {
				if(Char.myChar().statusMe==Char.A_DEAD||Char.myChar().statusMe==Char.A_DEADFLY){
				GameCanvas.startCommandDlg("Bạn có muốn hồi sinh tại chỗ (1 gold)?", 
						new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
						new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
				return;
				}
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				isPress = true;
				if (bntCharBoard != null)
				{
					bntCharBoard.performAction();	
				}
			}
		}
		if (!MenuIcon.isShowTab&&Screen.getCmdPointerLast(btnChangeFocus)) {
			indexLoopFind = typeNextFocus;
			if (btnChangeFocus != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				isPress = true;
				if (btnChangeFocus != null)
				{
					btnChangeFocus.performAction();	
				}
			}
		}
		if (!MenuIcon.isShowTab&Screen.getCmdPointerLast(btnUseHp)) {

			if (btnUseHp != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				isPress = true;
				if (btnUseHp != null)
				{
					btnUseHp.performAction();	
				}
			}
		}
		if (!MenuIcon.isShowTab&Screen.getCmdPointerLast(btnUseMp)) {

			if (btnUseMp != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				isPress = true;
				if (btnUseMp != null)
				{
					btnUseMp.performAction();	
				}
			}
		}
		if(LoginScreen.isTest){
		if (menuIcon.indexpICon==0&&/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnchangeSkill)) {

			if (bntCharBoard != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				isPress = true;
				if (btnchangeSkill != null)
				{
					btnchangeSkill.performAction();	
				}
			}
		}
			if (menuIcon.indexpICon==0&&/*GameCanvas.keyPressed[5] ||*/ Screen.getCmdPointerLast(btnBatTestSkill)) {
	
				if (bntCharBoard != null) {
					GameCanvas.isPointerJustRelease = false;
					GameCanvas.keyPressed[5] = false;
					Screen.keyTouch = -1;
					isPress = true;
					if (btnBatTestSkill != null)
					{
						btnBatTestSkill.performAction();	
					}
				}
			}
		}
		if (!MenuIcon.isShowTab& Screen.getCmdPointerLast(btnChatMap)) {

			if (btnChatMap != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				isPress = true;
				if (btnChatMap != null)
				{
					btnChatMap.performAction();	
				}
			}
		}
        if (!GameCanvas.gameScreen.guiChatClanWorld.moveClose && (!moveClose||MenuIcon.lastTab.size()>0))
		menuIcon.updateKey();
		if (!isPress&&GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick&&!moveClose){
			moveClose = true;
			if(menuIcon.subMenu!=null) menuIcon.cmdClose.performAction();
		}
//		GameCanvas.clearKeyPressed();
	}
	public void Paint(mGraphics g)
	{
		//GameScreen.paintCmdBar(g);
		int iMppaint = (int)((Char.myChar().cMP*(mGraphics.getImageWidth(LoadImageInterface.imgMp)-19))//phần dư ko paint
				/Char.myChar().cMaxMP);
		g.drawRegion(LoadImageInterface.imgMp, 0, 0,
				iMppaint+19,
				mGraphics.getImageHeight(LoadImageInterface.imgMp),
				0, bntCharBoard.x+36, bntCharBoard.y+12, mGraphics.TOP|mGraphics.LEFT, false);
		
		int hppaint = (((Char.myChar().cHP>Char.myChar().cMaxHP?Char.myChar().cMaxHP:Char.myChar().cHP)*mGraphics.getImageWidth(LoadImageInterface.imgBlood))/Char.myChar().cMaxHP);
		g.drawRegion(LoadImageInterface.imgBlood, 0, 0,
				hppaint,
				mGraphics.getImageHeight(LoadImageInterface.imgBlood),
				0,  bntCharBoard.x+56, bntCharBoard.y+21, mGraphics.TOP|mGraphics.LEFT, false);
		int Exppaint = (int)((Char.myChar().cEXP*mGraphics.getImageWidth(LoadImageInterface.imgExp))/Char.myChar().cMaxEXP);
		g.drawRegion(LoadImageInterface.imgExp, 0, 0,
				Exppaint,
				mGraphics.getImageHeight(LoadImageInterface.imgExp),
				0,  bntCharBoard.x+54, bntCharBoard.y+35, mGraphics.TOP|mGraphics.LEFT, false);
			g.drawRegion(LoadImageInterface.icn_Mail, 0, 0,
					21,
					21,
					0,  10, 65, mGraphics.TOP|mGraphics.LEFT, false);
			if(ChatPrivate.nTinChuaDoc>0){
				
			mFont.tahoma_7_red.drawStringBorder(g,ChatPrivate.nTinChuaDoc+"", 28,
					68, 1);
		}
//		g.setColor(0xff00);
//		g.fillRect( GameCanvas.w-60,38, 60, 20);
		if(TileMap.mapName!=null){
			int www=mFont.tahoma_7_yellow.getWidth(TileMap.mapName)+8;
			g.drawImage(MsgDlg.imgpopup[0], GameCanvas.w-(www<40?40:www),0, mGraphics.TOP|mGraphics.LEFT );
			mFont.tahoma_7_yellow.drawString(g,TileMap.mapName, GameCanvas.w-4,10, 1);
		}
		
//		g.drawImage(LoadImageInterface.imgMp, bntCharBoard.x+36, bntCharBoard.y+12);
		//		g.drawImage(LoadImageInterface.imgBlood, bntCharBoard.x+56, bntCharBoard.y+21);
//		g.drawImage(LoadImageInterface.imgExp, bntCharBoard.x+54, bntCharBoard.y+35);

		if(LoginScreen.isTest){
			btnchangeSkill.paint(g);
			btnBatTestSkill.paint(g);
		}
		bntCharBoard.paint(g);
		Part ph = GameScreen.parts[Char.myChar().head];
		SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id, 
				30, 30, 0, mGraphics.VCENTER|mGraphics.HCENTER);
		
		if(Char.myChar()!=null&&Char.myChar().cName!=null)
		mFont.tahoma_7b_white.drawString(g,Char.myChar().cName,  bntCharBoard.x+60, bntCharBoard.y+45, 0);
		mFont.tahoma_7_white.drawString(g, Char.myChar().cHP+"/"+Char.myChar().cMaxHP,
				 bntCharBoard.x+95, bntCharBoard.y+20, 2);
		mFont.tahoma_7_white.drawString(g, Char.myChar().cMP+"/"+Char.myChar().cMaxMP,
				 bntCharBoard.x+95, bntCharBoard.y+9, 2);
		mFont.tahoma_7_white.drawString(g, Char.myChar().cEXP+"%",
				 bntCharBoard.x+95, bntCharBoard.y+31, 2);
		mFont.tahoma_7_white.drawString(g, Char.myChar().clevel+"",
				 bntCharBoard.x+49, bntCharBoard.y+44, 2);
		
		if(moveClose&&!MenuIcon.isShowTab)//khi menu hien thi
		{
			if(isAnalog){
				GameScreen.resetTranslate(g);
				gamePad.paint(g);
			}
			else paintTouchControl(g);//3 mui ten
			bntAttack.paint(g);
			bntAttack_1.paint(g);
			bntAttack_2.paint(g);
			bntAttack_3.paint(g);
			bntAttack_4.paint(g);
			btnChangeFocus.paint(g);
			btnUseHp.paint(g);
			btnUseMp.paint(g);
			btnChatMap.paint(g);
			for (int i = 0; i < Char.myChar().mQuickslot.length; i++) {
				QuickSlot ql=  Char.myChar().mQuickslot[i];
				if(ql.quickslotType!=-1){
					SkillTemplate skillIndexx = (SkillTemplate)SkillTemplates.hSkilltemplate.get(""+ql.idSkill);
					if(skillIndexx!=null){
//						gettimeCooldown
						if(ql.canUse())
						SmallImage.drawSmallImage(g,skillIndexx.iconTron,
								xySkill[i][0],
								xySkill[i][1], 0, mGraphics.VCENTER | mGraphics.HCENTER,true);
						else{
							SmallImage.drawSmallImage(g,skillIndexx.iconTron,
									xySkill[i][0],
									xySkill[i][1], 0, mGraphics.VCENTER | mGraphics.HCENTER,true,50);
							mFont.tahoma_7_white.drawString(g, ql.gettimeCooldown(),xySkill[i][0],
									xySkill[i][1]-6,2, true);
						}
					}
				}
			}
		}

		GameScreen.resetTranslate(g);
		if((Char.myChar()!=null&&Char.myChar().npcFocus!=null&&Char.myChar().npcFocus.template!=null&&Char.myChar().npcFocus.template.typeKhu!=-1)||Char.myChar().mobFocus!=null||Char.myChar().charFocus!=null
				||Char.myChar().itemFocus!=null){

			int hppaintFocus = 0;
			if(Char.myChar().npcFocus!=null) hppaintFocus = mGraphics.getImageWidth(LoadImageInterface.imgBlood);
			else if(Char.myChar().charFocus!=null) {
				hppaintFocus = (((Char.myChar().charFocus.cHP>Char.myChar().charFocus.cMaxHP?Char.myChar().charFocus.cMaxHP:Char.myChar().charFocus.cHP)
						*mGraphics.getImageWidth(LoadImageInterface.imgBlood))/Char.myChar().charFocus.cMaxHP);
//				
//				hppaintFocus = Char.myChar().charFocus.cHP*mGraphics.getImageWidth(LoadImageInterface.imgBlood)/Char.myChar().charFocus.cMaxHP;
			}
			else if(Char.myChar().mobFocus!=null) {
				Char.myChar().mobFocus.hp = (Char.myChar().mobFocus.hp<0?0:Char.myChar().mobFocus.hp);
				hppaintFocus = (((Char.myChar().mobFocus.hp>Char.myChar().mobFocus.maxHp?Char.myChar().mobFocus.maxHp:Char.myChar().mobFocus.hp)
						*mGraphics.getImageWidth(LoadImageInterface.imgBlood))/Char.myChar().mobFocus.maxHp);
//				hppaintFocus = Char.myChar().mobFocus.hp*mGraphics.getImageWidth(LoadImageInterface.imgBlood)/Char.myChar().mobFocus.maxHp;
			}
			g.drawRegion(LoadImageInterface.imgBlood, 0, 0,
					hppaintFocus,
					mGraphics.getImageHeight(LoadImageInterface.imgBlood),
					0,   GameCanvas.w/2+24, LoadImageInterface.imgfocusActor.getHeight()/2-6/*+GameScreen.ypaintFocus*/, mGraphics.TOP|mGraphics.LEFT, false);
			
			g.drawImage(LoadImageInterface.imgfocusActor, GameCanvas.w/2+43, LoadImageInterface.imgfocusActor.getHeight()/2/*+GameScreen.ypaintFocus*/, mGraphics.VCENTER|mGraphics.HCENTER);
			if(Char.myChar().npcFocus!=null&&Char.myChar().npcFocus.template.name!=null&&Char.myChar().npcFocus.template.typeKhu!=-1){
				mFont.tahoma_7b_white.drawString(g, Char.myChar().npcFocus.template.name,  GameCanvas.w/2+28, LoadImageInterface.imgfocusActor.getHeight()/2+10/*+GameScreen.ypaintFocus*/, 0);
				
				try {
					Part ph2 = GameScreen.parts[Char.myChar().npcFocus.template.headId];
					SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id, 
							GameCanvas.w/2, 
							LoadImageInterface.imgfocusActor.getHeight()/2/*+GameScreen.ypaintFocus*/, 0, mGraphics.VCENTER|mGraphics.HCENTER);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
				else if(Char.myChar().charFocus!=null&&Char.myChar().charFocus.cName!=null){
				mFont.tahoma_7b_white.drawString(g, Char.myChar().charFocus.cName,  GameCanvas.w/2+28, LoadImageInterface.imgfocusActor.getHeight()/2+10/*+GameScreen.ypaintFocus*/, 0);
				mFont.tahoma_7_white.drawString(g, Char.myChar().charFocus.cHP+"/"+Char.myChar().charFocus.cMaxHP,
						GameCanvas.w/2+68, LoadImageInterface.imgfocusActor.getHeight()/2-7/*+GameScreen.ypaintFocus*/, 2);
				mFont.tahoma_7b_white.drawString(g, Char.myChar().charFocus.clevel+"",  GameCanvas.w/2+17, 
						LoadImageInterface.imgfocusActor.getHeight()/2+11/*+GameScreen.ypaintFocus*/,2);
				try {
					Part ph2 = GameScreen.parts[Char.myChar().charFocus.head];
					SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id, 
							GameCanvas.w/2, 
							LoadImageInterface.imgfocusActor.getHeight()/2/*+GameScreen.ypaintFocus*/, 0, mGraphics.VCENTER|mGraphics.HCENTER);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			else if(Char.myChar().mobFocus!=null&&Char.myChar().mobFocus.mobName!=null){
				mFont.tahoma_7_white.drawString(g, Char.myChar().mobFocus.hp+"/"+Char.myChar().mobFocus.maxHp,
						GameCanvas.w/2+68, LoadImageInterface.imgfocusActor.getHeight()/2-7/*+GameScreen.ypaintFocus*/,2);
				try {
					mFont.tahoma_7b_white.drawString(g, Char.myChar().mobFocus.mobName,  GameCanvas.w/2+28, LoadImageInterface.imgfocusActor.getHeight()/2+10/*+GameScreen.ypaintFocus*/, 0);
					mFont.tahoma_7b_white.drawString(g, Char.myChar().mobFocus.level+"",  GameCanvas.w/2+17, 
							LoadImageInterface.imgfocusActor.getHeight()/2+11/*+GameScreen.ypaintFocus*/,2);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			else if(Char.myChar().itemFocus!=null&&Char.myChar().itemFocus.template!=null){
				mFont.tahoma_7b_white.drawString(g, Char.myChar().itemFocus.template.name,  GameCanvas.w/2+28, LoadImageInterface.imgfocusActor.getHeight()/2+10/*+GameScreen.ypaintFocus*/, 0);
				
			}
			
		}
		for (int i = 0; i < vecItemOther.size(); i++) {
			ItemThucAn item = (ItemThucAn)vecItemOther.elementAt(i);
			item.paint(g, 20*(i)+14, bntCharBoard.y+85);
		}
		menuIcon.y=this.y+xMove;
		menuIcon.paint(g);
		
	}

	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		switch (idAction) {
		case Constants.CHAR_BROAD:
			
			if(!moveClose)
			{
				moveClose=true;
			}
			else
			{
				moveClose=false;
			}
			if(!moveClose) 
				timestart = mSystem.currentTimeMillis();
			break;
		case 20:
			indexSkillTest=(indexSkillTest+1)% GameScreen.sks.length;
			isPaintOjectMap = !isPaintOjectMap;
			break;
		case 21:
			isPayment = !isPayment;
//			isTestSkill = !isTestSkill;
			break;
		case 30:
			int minx,miny,maxx,maxy;
			int rangeMax  = 0;
			try {
				rangeMax = Char.myChar().getdxSkill();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			if(rangeMax<10) rangeMax = 30;
			minx = Char.myChar().cx - 3*rangeMax/2;
			maxx = Char.myChar().cx + 3*rangeMax/2;
			miny = Char.myChar().cy - rangeMax - 20;
			maxy = Char.myChar().cy + rangeMax + 40;
			switch (typeNextFocus) {
			case 0:
				int dem = (indexNext< GameScreen.vCharInMap.size()?indexNext:0);
				boolean isFinded = false;
				for (int i = dem; i < GameScreen.vCharInMap.size(); i++) {
					Char c = (Char) GameScreen.vCharInMap.elementAt(i);
					if(c.isNhanban()||c.equals(Char.myChar())||c.equals(Char.myChar().charFocus))
						continue;
					if (c.statusMe == Char.A_HIDE || c.isInvisible)
						continue;
					if (c.charID >= -1)
						continue;
					if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy&&!c.equals(Char.myChar())) {
						
						Char.myChar().clearAllFocus();
						Char.myChar().charFocus = c;
							indexNext++;
							if(indexNext>= GameScreen.vCharInMap.size())
								typeNextFocus= (typeNextFocus+1)%4;
							isFinded = true;
							break;
					}
				}
				if(!isFinded){
					typeNextFocus= (typeNextFocus+1)%4;
					if(typeNextFocus==indexLoopFind)
						return;
					indexNext = 0;
					if (btnChangeFocus != null)
					{
						btnChangeFocus.performAction();	
					}
				}
				break;
			case 1:
				Cout.println2222("Mob.isBossAppear   "+Mob.isBossAppear);
				int demMob = (indexNext< GameScreen.vMob.size()?indexNext:0);
				boolean isFindedMob = false;
				for (int i = demMob; i < GameScreen.vMob.size(); i++) {
					Mob c = (Mob) GameScreen.vMob.elementAt(i);
					if (c.status == Mob.MA_INJURE|| c.status == Mob.MA_DEADFLY||c.hp<=0||c.equals(Char.myChar().mobFocus))
						continue;
					if (minx <= c.x && c.x <= maxx && miny <= c.y && c.y <= maxy) {
						if(c.isBoss&&!Mob.isBossAppear) continue;
							Char.myChar().clearAllFocus();
							Char.myChar().mobFocus = c;
							indexNext++;
							if(indexNext>= GameScreen.vMob.size())
								typeNextFocus= (typeNextFocus+1)%4;
							isFindedMob = true;
							break;
					}
				}
				if(!isFindedMob){
					typeNextFocus= (typeNextFocus+1)%4;
					if(typeNextFocus==indexLoopFind)
						return;
					indexNext = 0;
					if (btnChangeFocus != null)
					{
						btnChangeFocus.performAction();	
					}
				}
				break;
			case 2:
				int demItem = (indexNext< GameScreen.vItemMap.size()?indexNext:0);
				boolean isFindedItem = false;
				for (int i = demItem; i < GameScreen.vItemMap.size(); i++) {
					ItemMap c = (ItemMap) GameScreen.vItemMap.elementAt(i);
					if(Char.myChar().itemFocus!=null&&c.equals(Char.myChar().itemFocus)) continue;
					if (minx <= c.x && c.x <= maxx && miny <= c.y && c.y <= maxy) {
						Char.myChar().clearAllFocus();
							Char.myChar().itemFocus = c;
							indexNext++;
							if(indexNext>= GameScreen.vItemMap.size())
								typeNextFocus= (typeNextFocus+1)%4;
							isFindedItem = true;
							break;
					}
				}
				if(!isFindedItem){
					typeNextFocus= (typeNextFocus+1)%4;
					if(typeNextFocus==indexLoopFind)
						return;
					indexNext = 0;
					if (btnChangeFocus != null)
					{
						btnChangeFocus.performAction();	
					}
				}
				break;
			case 3:
				int demNpc = (indexNext< GameScreen.vNpc.size()?indexNext:0);
				boolean isFindedNpc = false;
				for (int i = demNpc; i < GameScreen.vNpc.size(); i++) {
					Npc c = (Npc) GameScreen.vNpc.elementAt(i);
					if(Char.myChar().mobFocus!=null&&c.equals(Char.myChar().npcFocus)) continue;
					if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy) {
						Char.myChar().clearAllFocus();	
						Char.myChar().npcFocus = c;
							indexNext++;
							if(indexNext>= GameScreen.vNpc.size())
								typeNextFocus= (typeNextFocus+1)%4;
							isFindedNpc = true;
							break;
					}
				}
				if(!isFindedNpc){
					typeNextFocus= (typeNextFocus+1)%4;
					if(typeNextFocus==indexLoopFind)
						return;
					indexNext = 0;
					if (btnChangeFocus != null)
					{
						btnChangeFocus.performAction();	
					}
				}
				break;
			}
			break;
		case 31:
//			if(true)
//			{
//
//				GameCanvas.startYesNoDlg("Moi vao bang",new Command("Yes", GameScreen.gI(), 1000, "1,1"),
//						new Command("No", GameScreen.gI(), 1001,  "2,2"));
//				return ;
//			}
			if(Char.myChar().cHP<Char.myChar().cMaxHP)
			{
				if( Char.myChar().arrItemBag!=null){
					boolean isNoExitHp = true;
					for(int i = 0; i < Char.myChar().arrItemBag.length; i++){	
						Item it = Char.myChar().arrItemBag[i];
						if(it!=null&&Item.isHP(it.template.type))
						{
							isNoExitHp = false;
							if(Char.myChar().arrItemBag!=null)
								Service.gI().useItem((byte)/*Char.myChar().arrItemBag[*/i/*].itemId*/, Char.myChar().arrItemBag[i]);

							break;
						}
					}
					if(isNoExitHp)
						GameCanvas.StartDglThongBao("Bạn đã hết bình Hp");
				}
				
			}else {
				GameCanvas.StartDglThongBao("Hp đang đầy");
			}
			break;
		case 32:
			if(Char.myChar().cMP<Char.myChar().cMaxMP)
			{
				if( Char.myChar().arrItemBag!=null){
					boolean isNoExitMp = true;
					for(int i = 0; i < Char.myChar().arrItemBag.length; i++){	
						Item it = Char.myChar().arrItemBag[i];
						if(it!=null&&Item.isMP(it.template.type))
						{
							if(Char.myChar().arrItemBag!=null)
								Service.gI().useItem((byte)/*Char.myChar().arrItemBag[*/i/*].itemId*/, Char.myChar().arrItemBag[i]);
							isNoExitMp = false;
							break;
						}
					}
					if(isNoExitMp)
						GameCanvas.StartDglThongBao("Bạn đã hết bình Charka");
						
				}
			}else {
				GameCanvas.StartDglThongBao("Charka đang đầy");
			}
			break;
		case 33:
			tfchatMap.setTextBox();
			GameCanvas.isPointerClick = false;
			break;
		}
	}
	public int typeNextFocus = 0;//char = 0 , mob = 1, item = 2, npc = 3;
	public int indexNext = 0,indexLoopFind;
	public static void paintCmdBar(mGraphics g) {
		Char.myChar().cHP  = 100;
		Char.myChar().cMaxHP = 100;
		Char.myChar().cMP = 50;
		Char.myChar().cMaxMP = 50;
		
		int exp=2;
		int expMax=100;
	
		g.setClip(0, cmdBarY - 4, GameCanvas.w, 100);

		// // PAINT HP
		int hpWidth = Char.myChar().cHP * hpBarW / Char.myChar().cMaxHP;
		Cout.println(" paint  "+ LoadImageInterface.imgBlood);
		//hp
		if (hpWidth > hpBarW)
			hpWidth = 0;
		g.drawRegion(LoadImageInterface.imgBlood,0,0,hpWidth,Image.getHeight(LoadImageInterface.imgBlood), 0,hpBarX + 4, hpBarY - 2, 0);

		//mp
		hpWidth=Char.myChar().cMP * Image.getWidth(LoadImageInterface.imgExp) / Char.myChar().cMaxMP;
		g.drawRegion(LoadImageInterface.imgExp,0,0,hpWidth,Image.getHeight(LoadImageInterface.imgExp), 0,hpBarX + 4, hpBarY -12, 0);

		//exp
		hpWidth=exp * Image.getWidth(LoadImageInterface.imgExp) / expMax;
		g.drawRegion(LoadImageInterface.imgExp,0,0,hpWidth,Image.getHeight(LoadImageInterface.imgExp), 0,hpBarX + 4, hpBarY +12, 0);

	}
	
	public static void loadCmdBar() {
//		if (imgCmdBar == null) {
//			imgCmdBar = new Image[2];
//			for (int i = 0; i < 2; i++)
//				imgCmdBar[i] = GameCanvas.loadImage("/u/c" + i + ".png");
//		}
//		cmdBarLeftW = mGraphics.getImageWidth(imgCmdBar[0]);
//		cmdBarRightW = mGraphics.getImageWidth(imgCmdBar[1]);
//		cmdBarCenterW = gW - cmdBarLeftW - cmdBarRightW + 1;

		hpBarX = 78 - 15 + 10;
		hpBarY = cmdBarY;
		hpBarW = GameScreen.gW - 84 - 30;
		expBarW = GameScreen.gW - 44 - 4;
		hpBarH = 20;

//		if (GameCanvas.w > 176) {
//			cmdBarCenterW -= 50;
//			hpBarW -= 50;
//			expBarW -= 50;
//			hpBarX += 15;
//			hpBarW -= 15;
//		}
		loadInforBar();
	}
	
	public static void loadInforBar() {
		if (!GameCanvas.isTouch)
			return;
		hpBarW = 83;
		mpBarW = 57;
		hpBarX = 52;
		hpBarY = 10 + Info.hI;
		expBarW = GameScreen.gW - 61;
	}
	
	public static void paintTouchControl(mGraphics g) {
		try{
			GameScreen.resetTranslate(g);
//			// ---CHAT
//			g.drawImage(GameScreen.imgChat, xC + 17, yC + 17, mGraphics.HCENTER | mGraphics.VCENTER);
//			
			
			// ---LEFT
//			g.drawImage(imgButton, xL, yL, 0);
			g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal), Image.getHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR, xL, yL,
					mGraphics.HCENTER | mGraphics.VCENTER);
			if (keyTouch == 4) {
//				g.drawImage(imgButton2, xL, yL, 0);
				g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal), Image.getHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_ROT180, xL, yL, mGraphics.HCENTER | mGraphics.VCENTER);
			}

			// ----RIGHT
//			g.drawImage(imgButton, xR, yR, 0);
			g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal),  Image.getHeight(LoadImageInterface.imgMoveNormal), 0, xR, yR, mGraphics.HCENTER
					| mGraphics.VCENTER);
			if (keyTouch == 6) {
//				g.drawImage(imgButton2, xR, yR, 0);
				g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0,Image.getWidth(LoadImageInterface.imgMoveNormal), Image.getHeight(LoadImageInterface.imgMoveNormal), 0, xR , yR,
						mGraphics.HCENTER | mGraphics.VCENTER);
			}

			// ----UP
//			g.drawImage(imgButton, xU, yU, 0);
			g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal), Image.getHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR_ROT90, xU ,
					yU , mGraphics.HCENTER | mGraphics.VCENTER);
			if (keyTouch == 3) {
//				g.drawImage(imgButton2, xU, yU, 0);
				g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(LoadImageInterface.imgMoveFocus), Image.getHeight(LoadImageInterface.imgMoveFocus), Sprite.TRANS_MIRROR_ROT90, xU ,
						yU, mGraphics.HCENTER | mGraphics.VCENTER);
			}
			// CENTER
			g.drawImage(LoadImageInterface.imgMoveCenter, xCenter    , yCenter,  mGraphics.HCENTER | mGraphics.VCENTER);

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
//		if (!GameCanvas.isTouch || (GameCanvas.menu.showMenu && GameCanvas.isTouchControlSmallScreen))
//			return;
//		if (GameCanvas.currentDialog != null || ChatPopup.currentMultilineChatPopup != null || GameCanvas.menu.showMenu || isPaintPopup())
//			return;
		
	}
	
	public static boolean updateKeyTouchControl() {
		boolean isPress = false;
		keyTouch = -1;
//		if (GameCanvas.isPointerHoldIn(TileMap.posMiniMapX, TileMap.posMiniMapY, TileMap.wMiniMap, TileMap.hMiniMap)) {
//			if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease) {
//				doShowMap();
//				isPress = true;
//			}
//		}

//		if ( isNotPaintTouchControl())
//			return;

		if(isAnalog)
			gamePad.update();
		else{
			if (GameCanvas.isPointerHoldIn(xU-Image.getHeight(LoadImageInterface.imgMoveNormal)/2, yU-Image.getWidth(LoadImageInterface.imgMoveNormal)/2,
					Image.getHeight(LoadImageInterface.imgMoveNormal), Image.getWidth(LoadImageInterface.imgMoveNormal)/2)) {
				keyTouch = 3;
				GameCanvas.keyHold[2] = true;
//				resetAuto();
				isPress = true;
			} else if (GameCanvas.isPointerDown) {
				GameCanvas.keyHold[2] = false;
			}

			if (GameCanvas.isPointerHoldIn(xU -Image.getHeight(LoadImageInterface.imgMoveNormal)/2-Image.getWidth(LoadImageInterface.imgMoveNormal)/2,
					yU-Image.getWidth(LoadImageInterface.imgMoveNormal)/2,
					Image.getWidth(LoadImageInterface.imgMoveNormal)-Image.getHeight(LoadImageInterface.imgMoveNormal)/2,
					Image.getWidth(LoadImageInterface.imgMoveNormal)-Image.getHeight(LoadImageInterface.imgMoveNormal)/2)) {

				GameCanvas.keyHold[1] = true;
//				resetAuto();
				isPress = true;
			} else if (GameCanvas.isPointerDown)
				GameCanvas.keyHold[1] = false;

			if (GameCanvas.isPointerHoldIn(xU + Image.getHeight(LoadImageInterface.imgMoveNormal)/2,
					yU-Image.getWidth(LoadImageInterface.imgMoveNormal)/2,
					Image.getWidth(LoadImageInterface.imgMoveNormal)-Image.getHeight(LoadImageInterface.imgMoveNormal)/2,
					Image.getWidth(LoadImageInterface.imgMoveNormal)-Image.getHeight(LoadImageInterface.imgMoveNormal)/2)) {

				GameCanvas.keyHold[3] = true;
//				resetAuto();
				isPress = true;
			} else if (GameCanvas.isPointerDown)
				GameCanvas.keyHold[3] = false;

			if (GameCanvas.isPointerHoldIn(xL-Image.getWidth(LoadImageInterface.imgMoveNormal)/2, yL-Image.getHeight(LoadImageInterface.imgMoveNormal)/2,
					Image.getWidth(LoadImageInterface.imgMoveNormal), Image.getHeight(LoadImageInterface.imgMoveNormal))) {
				keyTouch = 4;
				GameCanvas.keyHold[4] = true;
//				resetAuto();
				isPress = true;
			} else if (GameCanvas.isPointerDown)
				GameCanvas.keyHold[4] = false;

			if (GameCanvas.isPointerHoldIn(xR - Image.getWidth(LoadImageInterface.imgMoveNormal)/2, yR-Image.getHeight(LoadImageInterface.imgMoveNormal)/2,
					Image.getWidth(LoadImageInterface.imgMoveNormal), Image.getHeight(LoadImageInterface.imgMoveNormal))) {
				keyTouch = 6;
				GameCanvas.keyHold[6] = true;
//				resetAuto();
				isPress = true;
			} else if (GameCanvas.isPointerDown){
				GameCanvas.keyHold[6] = false;
			}
		} 
		


		if (GameCanvas.isPointerHoldIn(xF, yF, 54, 54)) {
			keyTouch = 5;
			if (GameCanvas.isPointerJustRelease) {
				GameCanvas.keyPressed[5] = true;
				isPress = true;
			}
		}
		
		if(GameCanvas.isPointerClick&&GameCanvas.isPoint(0, 68, 30, 30)){
			GameCanvas.isPointerClick = false;
			TabChat.gI().switchToMe();
			if(ChatPrivate.nTinChuaDoc>0){
				for (int i = 0; i < ChatPrivate.vOtherchar.size(); i++) {
					OtherChar other = (OtherChar)ChatPrivate.vOtherchar.elementAt(i);
					if(other.nTinChuaDoc>0){
						TabChat.gI().indexRow = i;
						ChatPrivate.nTinChuaDoc-=other.nTinChuaDoc;
						ChatPrivate.nTinChuaDoc = (ChatPrivate.nTinChuaDoc<0?0:ChatPrivate.nTinChuaDoc);
						other.nTinChuaDoc = 0;
						break;
					}
				}
			}
		}
		

		if (GameCanvas.isPointerJustRelease&&!isAnalog) {
			GameCanvas.keyHold[1] = false;
			GameCanvas.keyHold[2] = false;
			GameCanvas.keyHold[3] = false;
			GameCanvas.keyHold[4] = false;
			GameCanvas.keyHold[6] = false;

			// GameCanvas.keyHold[5] = false;

		}
		return isPress;

	}
	
	public void updateRightCOntrol(){
//		boolean isPress = false;
		if(Screen.getCmdPointerLast(bntAttack)){
			Cout.println("PAIT SKILL");
			keyTouch = 5;
//			if (GameCanvas.isPointerJustRelease) {
				GameCanvas.keyPressed[5] = true;
//				System.out.println("ATTACK");
//				isPress = true;
//			}
		}
	}
	@Override
	public void keyPress(int keyCode) {
		// TODO Auto-generated method stub
		if (tfchatMap.isFocus)
			tfchatMap.keyPressed(keyCode);
		super.keyPress(keyCode);
	}
}
