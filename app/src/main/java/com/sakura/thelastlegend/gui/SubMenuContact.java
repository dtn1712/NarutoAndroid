package com.sakura.thelastlegend.gui;

import com.sakura.thelastlegend.Constants;
import com.sakura.thelastlegend.GameCanvas;
import com.sakura.thelastlegend.mGraphics;

import com.sakura.thelastlegend.lib.LoadImageInterface;
import Objectgame.Char;
import Objectgame.Friend;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Cout;
import com.sakura.thelastlegend.lib.Image;
import com.sakura.thelastlegend.real.Service;
import screen.GameScreen;

import com.sakura.thelastlegend.domain.model.Paint;
import com.sakura.thelastlegend.domain.model.Screen;

public class SubMenuContact extends MenuIcon {

	public SubMenuContact(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
		
		this.x=x;
		this.y=y;
		InitComand();
	}

	public static int width2=72;
	int height=50;
	Command iconSubFriend,iconSubParty;//sun contact
	

	
	
	@Override
	public void SetPosClose(Command cmd) {
		// TODO Auto-generated method stub
		//Image.getWidth(LoadImageInterface.imgSubMenu[0])
		Cout.println(getClass(), "submenu setposclose");
		cmd.setPos(x+Image.getWidth(LoadImageInterface.imgSubMenu[0])*7-Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
	}
	public void InitComand() {
		// TODO Auto-generated method stub
		int xx=0;
		//trade
		xx=width2+this.x-37;
		iconSubFriend= new Command("", this, Constants.ICON_SUB_FRIEND, null, 0, 0);
		iconSubFriend.setPos(xx,y, LoadImageInterface.imgFriendIcon, LoadImageInterface.imgFriendIcon);
		
		xx=width2+this.x+3;
		iconSubParty= new Command("", this, Constants.ICON_SUB_TEAM, null, 0, 0);
		iconSubParty.setPos(xx,y, LoadImageInterface.imgTeamIcon, LoadImageInterface.imgTeamIcon);
		
	}
	public void update() {
		// TODO Auto-generated method stub
		super.update();
	}
	public void updateKey() {
		// TODO Auto-generated method stub
		//click sun friend
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconSubFriend)) {
			
			if (iconSubFriend != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (iconSubFriend != null)
				{
					iconSubFriend.performAction();	
				}
			}
		}
		
		//click sub party
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(iconSubParty)) {

			if (iconSubParty != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (iconSubParty != null)
				{
					iconSubParty.performAction();	
				}
			}
		}
		
		//click sub party
		if (GameCanvas.keyPressed[5] || Screen.getCmdPointerLast(cmdClose)) {

			if (cmdClose != null) {
				GameCanvas.isPointerJustRelease = false;
				GameCanvas.keyPressed[5] = false;
				Screen.keyTouch = -1;
				if (cmdClose != null)
				{
					cmdClose.performAction();	
				}
			}
		}
		GameCanvas.clearKeyPressed();
	}
	public void updatePointer() {
		// TODO Auto-generated method stub
		super.updatePointer();
		if (GameCanvas.isPointerDown) {
			if (!GameCanvas.isPointSelect(this.x,this.y,width2*2,height)) {
				MenuIcon.isCloseSub=true;
			}
			
		}
	}

	public void perform(int idAction, Object p) {
		// TODO Auto-generated method stub
//		super.perform(idAction, p);
		Cout.println(getClass(),"SubMenuContact"+	MenuIcon.lastTab.size()+ " idAction "+idAction);
		switch (idAction) {
		
		case Constants.ICON_SUB_FRIEND:

			GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
			MenuIcon.lastTab.add(""+ Constants.ICON_SUB_FRIEND);
			
			Service.gI().requestFriendList((byte)Friend.REQUEST_FRIEND_LIST, (short)Char.myChar().charID);
			GameCanvas.gameScreen.guiMain.menuIcon.friend =new GuiFriend();
			GameCanvas.gameScreen.guiMain.menuIcon.indexpICon = Constants.ICON_SUB_FRIEND;
			GameCanvas.gameScreen.guiMain.menuIcon.friend.SetPosClose(GameCanvas.gameScreen.guiMain.menuIcon.cmdClose);
			GameCanvas.gameScreen.guiMain.menuIcon.paintButtonClose=true;
				MenuIcon.isShowTab = true;
//			GameScreen.gI().guiMain.menuIcon.cmdClose.performAction();
//			GameScreen.gI().guiMain.menuIcon.subMenu = null;
//			GameScreen.isPaintFriend = true;
//			MenuIcon.isCloseSub=true;
			break;
		case Constants.ICON_SUB_TEAM:

			GameCanvas.gameScreen.guiMain.menuIcon.cmdClose.performAction();
			GameCanvas.gameScreen.guiMain.menuIcon.indexpICon = Constants.ICON_TEAM;
			if(GameCanvas.gameScreen.guiMain.menuIcon.party==null)
				GameCanvas.gameScreen.guiMain.menuIcon.party=new TabParty(GameCanvas.hw, 20);

			MenuIcon.lastTab.add(""+ Constants.ICON_TEAM);

			MenuIcon.isShowTab = true;
			GameCanvas.gameScreen.guiMain.menuIcon.paintButtonClose=true;
			GameCanvas.gameScreen.guiMain.menuIcon.party.SetPosClose(GameCanvas.gameScreen.guiMain.menuIcon.cmdClose);
//			GameScreen.gI().guiMain.menuIcon.cmdClose.performAction();
//			GameScreen.gI().guiMain.menuIcon.subMenu = null;
//			GameScreen.gI().guiMain.menuIcon.indexpICon = 0;
//			MenuIcon.isCloseSub=true;
//			GameScreen.isPaintTeam = true;
			break;
			
		case Constants.CMD_TAB_CLOSE:

			GameScreen.gI().guiMain.menuIcon.indexpICon = 0;
			GameScreen.isPaintTeam=false;
			GameScreen.isPaintFriend=false;
			break;
		}
	}

	public void paint(mGraphics g) {
		// TODO Auto-generated method stub
		GameScreen.resetTranslate(g);
		Paint.PaintBGSubIcon(x,y,2,g);
		
		iconSubFriend.paint(g);
		iconSubParty.paint(g);
		
		
	}
}
