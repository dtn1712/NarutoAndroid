package com.sakura.thelastlegend;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.android.vending.billing.AndroidInappBilling;

import java.util.Vector;

import Objectgame.Char;
import com.sakura.thelastlegend.lib.Command;
import com.sakura.thelastlegend.lib.Music;
import com.sakura.thelastlegend.lib.Session_ME;
import com.sakura.thelastlegend.lib.TField;
import com.sakura.thelastlegend.lib.myEditText;
import com.sakura.thelastlegend.domain.model.PlayerInfo;
import com.sakura.thelastlegend.real.Controller;

public class GameMidlet extends Activity implements Runnable {

    // ---VIETNAM
    public static String IPS1 = "112.213.84.18";

    //public static String IPS1 = "192.168.1.3";
    public static boolean isEngVersion = false;
    public static byte indexClient = 0;
    public static String IP = "192.168.1.99";
    public static int PORT = 14444;
    public static String VERSION = "0.1.2";
    public static byte userProvider = 0; //x DO NOT SET HERE
    public static String clientAgent;

    public static PlayerInfo myInfo;
    public static GameCanvas gameCanvas;
    public static GameMidlet instance;
    public static boolean isVibrate = true;
    public static boolean isChaCha;
    public static Char mychar;
    public static int muzic = -1;
    public static AssetManager asset = null;
    // Main view
    public static RelativeLayout mainView;
    public static ScrollView scrollView;
    public static RelativeLayout subview;

    public static int nextID = 0;
    public static Vector<TField> allField = new Vector<TField>();
    public static String Model = "";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        instance = this;
        asset = getAssets();
        // ============
        Session_ME.gI().setHandler(Controller.gI());

        if (gameCanvas == null)
            gameCanvas = new GameCanvas(this);

        Model = Build.MODEL;
        if (Model.indexOf("HTC ChaCha") != -1) {
            isChaCha = true;
        }
        mainView = new RelativeLayout(this);

        mainView.addView(gameCanvas);

        scrollView = new ScrollView(this);
        subview = new RelativeLayout(this);
        scrollView.addView(subview);
        mainView.addView(scrollView);

        setContentView(mainView);

        // disable lock screen when press power key
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        lock.disableKeyguard();

        // check signal
        getDeviceSignal();

        //--PROVIDER & AGENT
//		clientAgent = readFileText("agent");
//		userProvider = Byte.parseByte(readFileText("provider"));
//		
//		System.err.println("AGENT: "+clientAgent+", PROVIDER: "+userProvider);
//
//		try
//		{
//			AndroidInappBilling.instance().initService(this);
//		} catch (Exception exx) {
//			exx.printStackTrace();
//		}
    }

    // control and update UI
    public static int action = 0;
    public static myEditText currentEditText;

    public void update(int act, myEditText t) {
        action = act;
        currentEditText = t;
        Thread thread = new Thread(this);
        thread.start();
    }

    public static int getID() {
        nextID++;
        return nextID;
    }

    public static String tempPro;

    // For get signal
    static Boolean isSignalEnable = true;

    public void getDeviceSignal() {
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
            case TelephonyManager.SIM_STATE_UNKNOWN:
                isSignalEnable = false;
                break;
            case TelephonyManager.SIM_STATE_READY:
                isSignalEnable = true;
                break;
        }

    }


    public static void sendSMS(final String data, final String to, final Command successAction, final Command failAction) {
//		if (isSignalEnable) {
//			if (isSend) {
//				isSend = false;
//				String SENT = "SMS_SENT";
//				PendingIntent sentPI = PendingIntent.getBroadcast(GameCanvas.Context, 0, new Intent(SENT), 0);
//
//				GameCanvas.Context.registerReceiver(new BroadcastReceiver() {
//					@Override
//					public void onReceive(Context arg0, Intent arg1) {
//						switch (getResultCode()) {
//						case Activity.RESULT_OK:
//							isSend = true;
//							successAction.performAction();
//							break;
//
//						case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//						case SmsManager.RESULT_ERROR_NO_SERVICE:
//						case SmsManager.RESULT_ERROR_NULL_PDU:
//						case SmsManager.RESULT_ERROR_RADIO_OFF:
//							isSend = true;
//							failAction.performAction();
//							break;
//
//						}
//
//					}
//				}, new IntentFilter(SENT));
//				String sendTo = String.copyValueOf(to.toCharArray(), 6,	to.length() - 6);
//				SmsManager sms = SmsManager.getDefault();
//				sms.sendTextMessage(sendTo, null, data, sentPI, null);
//			}
//		} else {
//			// if (GameCanvas.currentScreen == GameCanvas.loginScreen) {
//			// String sendTo = String.copyValueOf(to.toCharArray(), 6,
//			// to.length() - 6);
//			// GameCanvas.startOKDlg("Soạn Tin nhắn: " + data + " gửi tới "
//			// + sendTo + " để đăng ký tài khoản!");
//			// } else {
//			// GameCanvas.startOKDlg("Vui lòng kiểm tra lại sim hoặc sóng mạng!");
//			// }
//		}
        OpenMess(to, data);

    }

    public static void OpenMess(String to, String data) {
        Intent defineIntent = new Intent(Intent.ACTION_VIEW);
        defineIntent.setData(Uri.parse("smsto:" + to));
        defineIntent.putExtra("sms_body", data);
        defineIntent.putExtra("compose_mode", true);
        GameMidlet.instance.startActivity(defineIntent);
    }


    @Override
    public void onPause() {
        Music.stopAll();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Music.ResumMusic();
    }

    @Override
    public void onStop() {
        System.out.println("stop App");
        Music.stopAll();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (gameCanvas != null) {
            // canvas.stopGame();
            gameCanvas = null;
        }

        GameCanvas.bRun = false;

        GameMidlet.instance.finish();
        // kill process
        android.os.Process.killProcess(android.os.Process.myPid());

        super.onDestroy();
    }

    public void exit() {
        GameCanvas.bRun = false;
        finish();
    }

    public void run() {
        Looper.prepare();
        handler.sendEmptyMessage(0);
    }



    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            updateGUIState();
        }
    };

    public void updateGUIState() {
        if (action != 0) {
            switch (action) {
                case 1:
                    subview.addView(currentEditText);
                    action = 0;

                    currentEditText.requestFocus();
                    InputMethodManager imm = (InputMethodManager) GameCanvas.Context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(currentEditText, 0);
                    break;
                case 2:
                    subview.removeView(currentEditText);
                    action = 0;
                    currentEditText = null;
                    break;
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AndroidInappBilling.PURCHASE_ACTIVITY_CODE) {
            AndroidInappBilling.instance().notifyPurchase(resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


}