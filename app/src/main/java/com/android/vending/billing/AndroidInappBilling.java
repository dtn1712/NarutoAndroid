package com.android.vending.billing;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

public class AndroidInappBilling  implements InappInterface {
	
	public static final int BILLING_RESPONSE_RESULT_OK = 0;
	public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
	public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
	public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
	public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
	public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
	public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
	public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;
	
	public static final int IAP_BILLING_VERSION = 3;
	public static final String IAP_PURCHASE_TYPE = "inapp";
	public static final int PURCHASE_ACTIVITY_CODE = 1002;
	String packageName = "";
	Activity mActivity = null;

	
	IInAppBillingService mService;
	ServiceConnection mServiceConn;
	
	
	static AndroidInappBilling _instance = null;
	public static AndroidInappBilling instance() {
		if (_instance == null) _instance = new AndroidInappBilling();
		return _instance;
	}
	
	@Override
	public void initService(final Activity activity) {
		mServiceConn = new ServiceConnection() {
			   @Override
			   public void onServiceDisconnected(ComponentName name) {
			       mService = null;
			   }

			   @Override
			   public void onServiceConnected(ComponentName name, IBinder service) {
			       mService = IInAppBillingService.Stub.asInterface(service);
			   }
			};

		activity.bindService(new 
	        Intent("com.android.vending.billing.InAppBillingService.BIND"),
	        	mServiceConn, Context.BIND_AUTO_CREATE);
		this.packageName = activity.getPackageName();
		this.mActivity = activity;
	}

	@Override
	public String[] loadProductIDs() {
		return new String[] { "avatar_gold_10" };
	}

	@Override
	public void getProductDetail(String productId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void purchase(String productId) {
		try {
			
			Bundle buyIntentBundle = mService.getBuyIntent(IAP_BILLING_VERSION, this.packageName, productId, IAP_PURCHASE_TYPE, "");
			int response = buyIntentBundle.getInt("RESPONSE_CODE");
			

			switch (response) {
			case BILLING_RESPONSE_RESULT_OK:
				PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
				this.mActivity.startIntentSenderForResult(pendingIntent.getIntentSender(),
						PURCHASE_ACTIVITY_CODE, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
						   Integer.valueOf(0));
				break;
			case BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED:
				handlePuchase(productId);
				break;
			default:
				break;
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (SendIntentException e) {
			e.printStackTrace();
		}
	}
	
	public void handlePuchase(final String productId) {
		Bundle ownedItems;
		try {
			ownedItems = mService.getPurchases(IAP_BILLING_VERSION, packageName, IAP_PURCHASE_TYPE, null);

			if (ownedItems.getInt("RESPONSE_CODE") == 0) {
			   ArrayList<String>  purchaseDataList =
			      ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
			   
			   for (int i = 0; i < purchaseDataList.size(); ++i) {
				   String purchase = purchaseDataList.get(i);
				   JSONObject json;
				   try {
					   json = new JSONObject(purchase);
					   if (json.getString("productId").equals(productId)) {
						   this.servicePurchaseServerSide(json.getString("productId"), json.getString("purchaseToken"));
						   break;
					   }
				   } catch (JSONException e) {
					   e.printStackTrace();
				   }
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void notifyPurchase(int resultCode, Intent data) {
		String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
		
		//System.out.println("----------------- purchaseData        " + purchaseData);
		
		if (resultCode == Activity.RESULT_OK) {
			try {
			      JSONObject json = new JSONObject(purchaseData);
			      servicePurchaseServerSide(json.getString("productId"), json.getString("purchaseToken"));
			}
			catch (JSONException e) {
				System.out.println("Failed to parse purchase data.");
				e.printStackTrace();
			}
		}
	}

	@Override
	public void consumePurchase(final String productId) {
		new Thread(new Runnable() {
			public void run() {
				Bundle ownedItems;
				try {
					ownedItems = mService.getPurchases(IAP_BILLING_VERSION, packageName, IAP_PURCHASE_TYPE, null);

					if (ownedItems.getInt("RESPONSE_CODE") == 0) {
					   ArrayList<String>  purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
					   
					   for (int i = 0; i < purchaseDataList.size(); ++i) {
						   String purchase = purchaseDataList.get(i);
						   JSONObject json;
						   try {
							   json = new JSONObject(purchase);
							   if (json.getString("productId").equals(productId)) {
								    int response = mService.consumePurchase(IAP_BILLING_VERSION, packageName, json.getString("purchaseToken"));
									if (response != 0) {
										System.out.println("Consume purchase product " + productId + " failed. Error code: " + response);
									}
								    break;
							   }
						   } catch (JSONException e) {
							   e.printStackTrace();
						   }
						}
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	@Override
	public void stopService(Activity activity) {
		if (mService != null) {
			activity.unbindService(mServiceConn);
		}
	}
	
	@Override
	public void handleAllPurchases() {
		try {
			Bundle ownedItems = mService.getPurchases(IAP_BILLING_VERSION, packageName, IAP_PURCHASE_TYPE, null);
			int response = ownedItems.getInt("RESPONSE_CODE");
			if (response == 0) {
				
			   ArrayList<String>  purchaseDataList =
			      ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
			   
			   for (int i = 0; i < purchaseDataList.size(); ++i) {
					String purchaseData = purchaseDataList.get(i);
					JSONObject json;
					try {
						json = new JSONObject(purchaseData);
						this.servicePurchaseServerSide(json.getString("productId"), json.getString("purchaseToken"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
			   }
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void servicePurchaseServerSide(final String productId, final String token) {
		System.out.println(">> process purchase: " + productId + " token: " + token);
//		Service.gI().sendPurchaseInfo(productId, token);
		
		
	}
	
	public class ProductDetail
	{
		public String orderId; 
		public String packageName;
		public String productId;
		public long purchaseTime;
		public int purchaseState; //0 (purchased), 1 (canceled), or 2 (refunded).
		public String developerPayload;
		public String purchaseToken;
	}
}
