package com.android.vending.billing;

import android.app.Activity;
import android.content.Intent;

public interface InappInterface {
	/**
	 * Khoi tao service (bat buoc)
	 * @param activity
	 */
	public void initService(Activity activity);
	/**
	 * Load chuoi product Ids (chua xai)
	 * @return
	 */
	public String[] loadProductIDs();
	/**
	 * Lay thong tin 1 product (chua can)
	 * @param productId
	 */
	public void getProductDetail(String productId);
	/**
	 * Mua 1 product tu tren store (bat buoc)
	 * B1: mo dialog mua
	 * B2: Mua hoac cancel
	 * @param productId
	 */
	public void purchase(String productId);
	/**
	 * Xu ly tat ca cac purchase da thuc hien va con gia tri (bat buoc)
	 */
	public void handleAllPurchases();
	/**
	 * Tra va tu B2 cua {@code}purchase , ket qua la Mua (bat buoc)
	 * @param resultCode 
	 * @param data
	 */
	public void notifyPurchase(int resultCode, Intent data);
	/**
	 * Gui productId va token len cho server kiem tra (bat buoc)
	 * @param data
	 * @param signature
	 */
	public void servicePurchaseServerSide(final String data, final String signature);
	/**
	 * Huy 1 purchase, neu nhan duoc SMS_SERVER_LOAD tu server (bat buoc)
	 * @param productId
	 */
	public void consumePurchase(final String productId);
	/**
	 * Huy servive de tranh memory leak (bat buoc)
	 * @param activity
	 */
	public void stopService(Activity activity);
}
